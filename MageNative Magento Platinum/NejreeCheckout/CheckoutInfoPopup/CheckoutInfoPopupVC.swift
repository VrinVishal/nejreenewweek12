//
//  CheckoutInfoPopupVC.swift
//  MageNative Magento Platinum
//
//  Created by vishal.n on 11/02/21.
//  Copyright © 2021 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class CheckoutInfoPopupVC: UIViewController {
    
    @IBOutlet weak var btnBG: UIButton!
    
    @IBOutlet weak var viewPopup: UIView!
    
    @IBOutlet weak var lblMessage: UILabel!
    
    @IBOutlet weak var btnOk: UIButton!
    
    var strMessage = ""

    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.viewPopup.round(redius: 10.0)
        self.btnOk.round(redius: 3.0)

        self.lblMessage.text = self.strMessage
        
        self.lblMessage.font = UIFont(name: "Cairo-Regular", size: 15)!

        self.btnOk.setTitle(APP_LBL().ok.uppercased(), for: .normal)
        self.btnOk.titleLabel?.font = UIFont(name: "Cairo-Regular", size: 16)!
    }
    

    @IBAction func btnBGAction(_ sender: UIButton) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnOkAction(_ sender: UIButton) {
        
        self.dismiss(animated: true, completion: nil)
    }
}
