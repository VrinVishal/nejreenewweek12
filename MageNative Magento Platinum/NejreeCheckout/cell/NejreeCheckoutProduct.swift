//
//  NejreeCheckoutProduct.swift
//  MageNative Magento Platinum
//
//  Created by vishal.n on 13/08/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class NejreeCheckoutProduct: UIView {

    @IBOutlet weak var viewImageBorder: UIView!
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var lblGiftPrice: UILabel!
    @IBOutlet weak var lblGiftPriceSAR: UILabel!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var quantityLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var sizeValue: UILabel!

}
