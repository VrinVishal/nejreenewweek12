//
//  NejreeCheckoutVC.swift
//  MageNative Magento Platinum
//
//  Created by vishal.n on 13/08/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit
import FirebaseAnalytics
import TamaraSDK

// TAMARA PAYMENT CONTANTS



struct finalPAY {
    
    var title: String?
    var value: String?
    var descr: String? = ""
    var tamara_min: String?
    var tamara_max: String?


}


struct PaymentTotal {
    
    var amounttopay: String?
    var shipping_amount: String?
    var discount_amount: String?
    var tax_amount: String?
    var grandtotal: String?
    var grandtotal_without_currency: String?
    var coupon: String?
    var is_discount: String?
     var shipping_charge_new: String?
    var tax_amount_without_currency: Double?
    var free_shipping_remaining: String?
}

class NejreeCheckoutVC: UIViewController {

    //MARK:- OUTLET
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var imgBackBG: UIImageView!
    
    @IBOutlet weak var lblItem: UILabel!
    
    @IBOutlet weak var carousel: iCarousel!
    
    @IBOutlet weak var scrolVIEW: UIScrollView!
    @IBOutlet weak var stackBottom: UIStackView!
    
    @IBOutlet weak var viewPaymentMethod: UIView!
    @IBOutlet weak var lblTitleChooseePaymentMethod: UILabel!
    @IBOutlet weak var lblOffer: UILabel!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var htTbl: NSLayoutConstraint!
    
    @IBOutlet weak var lblTitleYourAddress: UILabel!
    @IBOutlet weak var viewAddress: UIView!
    @IBOutlet weak var lblAddCountry: UILabel!
    @IBOutlet weak var lblAddCity: UILabel!
    @IBOutlet weak var lblAddNeighbourhood: UILabel!
    @IBOutlet weak var lblAddPhone: UILabel!
    @IBOutlet weak var lblAddName: UILabel!
    @IBOutlet weak var btnChangeAddress: UIButton!
    
    @IBOutlet weak var viewPromoCode: UIView!
    @IBOutlet weak var txtPromoCode: UITextField!
    @IBOutlet weak var btnApplyPromoCode: UIButton!
    
    @IBOutlet weak var viewStoreCredit: UIView!
    @IBOutlet weak var lblStoreCreditApply: UILabel!
    @IBOutlet weak var btnStoreCredit: UIButton!
    
    @IBOutlet weak var viewTotal: UIView!
    @IBOutlet weak var viewCOD: UIView!
    
    
    @IBOutlet weak var lblSubTotal: UILabel!
    @IBOutlet weak var lblSubTotalValue: UILabel!
    @IBOutlet weak var lblShiping: UILabel!
    @IBOutlet weak var lblShipingValue: UILabel!
    @IBOutlet weak var lblDiscount: UILabel!
    @IBOutlet weak var lblDiscountValue: UILabel!
    @IBOutlet weak var lblStoreCredit: UILabel!
    @IBOutlet weak var lblStoreCreditValue: UILabel!
    
    @IBOutlet weak var lblTaxes: UILabel!
         @IBOutlet weak var lblTaxesValue: UILabel!
    
    @IBOutlet weak var lblCodCharge: UILabel!
    @IBOutlet weak var lblCodChargeValue: UILabel!
    @IBOutlet weak var lblTotalValue: UILabel!
    @IBOutlet weak var lblTaxesFees: UILabel!
    
    @IBOutlet weak var btnSubmit: UIButton!
    
    var selectedPaymentMethod = ""
    var jsonData = [String:String]()
    
    var arrOffers = [String]()
    
    
    @IBOutlet weak var viewFreeShipping: UIView!
    @IBOutlet weak var lblRequiredFreeShipping: UILabel!
    @IBOutlet weak var progressView: UIProgressView!
    
    //MARK:- STATIC
    let role = "USER"
    var nejreeColor = UIColor.init(hexString: "#FBAD18")
    
    //MARK:- CHEKCOUT
    var isExistGiftCard = false
    var arrProduct = [CartProduct]()
    var viewCartJSON = JSON()
    var arrFinalPAY : [finalPAY] = []
    
    //MARK:- STORE CREDIT
    var isStoreCreditApplied = false
    var isFreeOrderUsingStoreCredit = false
    var availableStoreCreditInt : Int = 0
    var availableStoreCreditString : String = ""
    var appliedStoreCreditString : String = ""
    var store_creditwithoutcurrency = "0"
    
    //MARK:- ADDRESS
    var isSelectCustomAddress = false
    var addressOBJ = [String:String]()
    
    //MARK:- TOTAL
    var total = PaymentTotal()
    
    //MARK:- PAYMENT METHOD
    var paymentMethod = ""
    var paymentMethodStoreCreditPartner = ""
    var paymentMethodName = ""
    var shippingMethod = "freeshipping_freeshipping"
    var isExtraFeeAmount = false
    var isCountryCodEnabled = true
    
    //MARK:- ORDER STATUS
    var orderStatusData = [String:String]()
    
    //MARK:- PAYMENT GATEWAY - BRAINTREE
    var clientToken = ""
    var previousMethods = [[String:String]]()
    
    //MARK:- PAYMENT GATEWAY - PAYFORT
    let paycontroller = PayFortController(enviroment: KPayFortEnviromentSandBox)
    var applePaySuccessCheck = false
    var tokenstr = ""
    
    //MARK:- PAYMENT GATEWAY - CHECKOUT
    var strPublicKey = ""
    var strSecretKey = ""
    var AmountFinal : Double!
    var strCurrentCheckoutMode = ""
    
    var jsonResponseDictTemp = NSDictionary()
    var responsePassApplePay : Any!
    
    //MARK:- PAYMENT GATEWAY - STC
    var STC_MerchantNote = "STC Payment from application"
    var STC_RefNum = "STC_REF"
    var STC_OtpReference = ""
    var STC_STCPayPmtReference = ""
    var STC_BillNumber = ""
    var grandTotalInt : Int = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.scrolVIEW.isHidden = true
        self.stackBottom.isHidden = true
        
        
        if let tempAddres = UserDefaults.standard.object(forKey: "selectedAddress") as? [String: String] {
            
            self.addressOBJ["name"] = tempAddres["name"] ?? ""
            self.addressOBJ["country"] = tempAddres["country"] ?? ""
            self.addressOBJ["city"] = tempAddres["city"] ?? ""
            self.addressOBJ["phone"] = tempAddres["phone"] ?? ""
            self.addressOBJ["street"] = tempAddres["street"] ?? ""
            self.addressOBJ["neighbour"] = tempAddres["neighbour"] ?? ""
            self.addressOBJ["entity_id"] = tempAddres["entity_id"] ?? ""
        }
        
        self.isGiftCardExist()
                
        self.setUI()

        self.getShippingPayament()
        
        self.getRequiredFields()
        
    }
    
    func getRequiredFields() {
            
            let c_id = UserDefaults.standard.value(forKey: "userInfoDict") as! [String:String]
            
            API().callAPI(endPoint: "mobiconnect/customer/getRequiredFields/\(c_id["customerId"]!)", method: .GET, param: [:]) { (json_res, err) in
                
              //  DispatchQueue.main.async {
                    
                    if err == nil {
                        
                        self.jsonData["firstname"] = json_res[0]["firstname"].stringValue
                        self.jsonData["lastname"] = json_res[0]["lastname"].stringValue
                        
//                        for address in json_res[0]["data"].arrayValue {
//
//                            if address["is_default"].stringValue == "1"{
//
//
//
//
//                            }
//                        }
//
//
//
//                        if let temp = json_res[0]["data"].arrayValue.last, temp["value"].stringValue != ""{
//
//                            UserDefaults.standard.set(temp["value"].stringValue, forKey: "mobile_number")
//                        }
//                        else
//                        {
//                            UserDefaults.standard.removeObject(forKey: "mobile_number")
//                        }
                        
                        UserDefaults.standard.synchronize()
                    }
               // }
            }
        }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
         UIApplication.shared.statusBarStyle = .default
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.tabBarController?.tabBar.isHidden = true
        
        self.tblView.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        self.tblView.removeObserver(self, forKeyPath: "contentSize")
         UIApplication.shared.statusBarStyle = .lightContent
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?)
    {
        self.tblView.layer.removeAllAnimations()

        self.htTbl.constant = self.tblView.contentSize.height
        UIView.animate(withDuration: 0.5) {
            self.updateViewConstraints()
            self.loadViewIfNeeded()
        }
    }
    
    func setUI() {
        
        //MARK:- FOR ENABLE PROMO CODE IN VIEW
        self.viewPromoCode.superview?.isHidden = true
        
        //MARK:- FOR ENABLE STORE CREDIT IN VIEW
        self.viewStoreCredit.superview?.isHidden = true
        
        self.tblView.register(UINib(nibName: "NejreeCheckoutCell", bundle: nil), forCellReuseIdentifier: "NejreeCheckoutCell")
        self.tblView.delegate = self
        self.tblView.dataSource = self
                
        lblTitleChooseePaymentMethod.font  = UIFont(name: "Cairo-Regular", size: 15)!
        lblTitleChooseePaymentMethod.text = APP_LBL().choose_payment_method.uppercased();
        
        viewPaymentMethod.round(redius: 10)
        viewPaymentMethod.layer.borderColor = UIColor.lightGray.cgColor
        viewPaymentMethod.layer.borderWidth = 0.3
                
        lblHeader.text = APP_LBL().checkout.uppercased();
        
        if APP_DEL.selectedLanguage == Arabic{
            txtPromoCode.textAlignment = .right
            lblTitleYourAddress.textAlignment = .right
            lblTitleChooseePaymentMethod.textAlignment = .right
            lblAddCountry.textAlignment = .right
            lblAddCity.textAlignment = .right
            lblAddNeighbourhood.textAlignment = .right
            lblAddPhone.textAlignment = .right
            lblAddName.textAlignment = .right
            lblStoreCreditApply.textAlignment = .right
            lblSubTotal.textAlignment = .right
            lblShiping.textAlignment = .right
            lblDiscount.textAlignment = .right
            lblTaxes.textAlignment = .right
            lblStoreCredit.textAlignment = .right
            lblCodCharge.textAlignment = .right

            
            imgBackBG.image = UIImage(named: "icon_back_white_Arabic")
            lblItem.textAlignment = .right
            
        } else {
            
            lblItem.textAlignment = .left
            lblTitleYourAddress.textAlignment = .left
            lblTitleChooseePaymentMethod.textAlignment = .left
            lblAddCountry.textAlignment = .left
            lblAddCity.textAlignment = .left
            lblAddNeighbourhood.textAlignment = .left
            lblAddPhone.textAlignment = .left
            lblAddName.textAlignment = .left
            lblStoreCreditApply.textAlignment = .left
            lblSubTotal.textAlignment = .left
            lblShiping.textAlignment = .left
            lblDiscount.textAlignment = .left
             lblTaxes.textAlignment = .left
            lblStoreCredit.textAlignment = .left
            lblCodCharge.textAlignment = .left

            txtPromoCode.textAlignment = .left
            imgBackBG.image = UIImage(named: "icon_back_white_round")
        }
        
        lblItem.text = APP_LBL().items.uppercased();
        lblItem.font  = UIFont(name: "Cairo-Regular", size: 15)!
        
        lblAddCountry.font  = UIFont(name: "Cairo-Bold", size: 18)!
        lblAddCity.font =  UIFont(name: "Cairo-Regular", size: 12)!
        lblAddNeighbourhood.font  = UIFont(name: "Cairo-Regular", size: 12)!
        lblAddPhone.font  = UIFont(name: "Cairo-Regular", size: 12)!
        lblAddName.font  = UIFont(name: "Cairo-Regular", size: 12)!
        
        lblTitleYourAddress.font  = UIFont(name: "Cairo-Regular", size: 15)!
        lblTitleYourAddress.text = APP_LBL().your_address.uppercased();
        
        viewAddress.round(redius: 10)
        viewAddress.layer.borderColor = UIColor.lightGray.cgColor
        viewAddress.layer.borderWidth = 0.3
                
        viewPromoCode.round(redius: 0)
        viewPromoCode.layer.borderColor = UIColor.lightGray.cgColor
        viewPromoCode.layer.borderWidth = 0.3
        txtPromoCode.placeholder = APP_LBL().code.uppercased()
        txtPromoCode.font = UIFont(name: "Cairo-Regular", size: 15)!
        
        btnApplyPromoCode.backgroundColor = nejreeColor
        btnApplyPromoCode.setTitleColor(UIColor.black, for: .normal)
        btnApplyPromoCode.setTitle(APP_LBL().apply.uppercased(), for: .normal)
        btnApplyPromoCode.titleLabel?.font = UIFont(name: "Cairo-Regular", size: 15)!
        
        viewStoreCredit.round(redius: 7)
        
        self.updateStoreCredit(isForUpdate: false, value: false)
        
        viewTotal.round(redius: 7)
        self.lblSubTotal.text = APP_LBL().subtotal.uppercased()
        self.lblShiping.text = APP_LBL().shipping.uppercased()
        self.lblDiscount.text = APP_LBL().discount.uppercased()
        self.lblTaxes.text = APP_LBL().tax_price.uppercased()
        self.lblStoreCredit.text = APP_LBL().store_credit.uppercased()
        self.lblCodCharge.text = APP_LBL().cod_charges.uppercased()
        self.lblCodChargeValue.text = (APP_DEL.selectedCountry.getCurrencySymbol() + " " + "0").uppercased()

        
        lblTotalValue.font = UIFont(name: "Cairo-Bold", size: 20)!
        lblTaxesFees.font = UIFont(name: "Cairo-Regular", size: 13)!
        
        self.manageSubmit()
        
        self.manageCarousel()
    }
    
    func manageSubmit() {
        
       // self.btnSubmit.round(redius: 5)
        self.btnSubmit.setTitle(APP_LBL().submit.uppercased(), for: .normal)
        self.btnSubmit.titleLabel?.font = UIFont(name: "Cairo-Bold", size: 17.5)!
        
        if isValid().res == false {
            
            self.btnSubmit.setTitleColor(UIColor.white, for: .normal)
            self.btnSubmit.backgroundColor = UIColor.black
            self.btnSubmit.layer.borderColor = UIColor.white.cgColor
            self.btnSubmit.layer.borderWidth = 0.8
            
        } else {
            
            self.btnSubmit.setTitleColor(UIColor.black, for: .normal)
            self.btnSubmit.backgroundColor = nejreeColor
            self.btnSubmit.layer.borderColor = nejreeColor?.cgColor
            self.btnSubmit.layer.borderWidth = 0.0
        }
    }
    
    //MARK:- ACTION
    @IBAction func btnChangeAddressAction(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true);
    }
    
    @IBAction func btnOfferAction(_ sender: UIButton) {
        
        let vc = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "NejreeOffersVC") as! NejreeOffersVC
        vc.arrOffer = self.arrOffers
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func btnApplyStoreCreditAction(_ sender: UIButton) {
        
        if self.isStoreCreditApplied {
            
            self.removeStoreCredit()
            
        } else {
            
            self.applyStoreCredit()
        }
    }
    
    @IBAction func btnApplyPromoCodeAction(_ sender: UIButton) {
        
        if (self.total.coupon != "") && (self.total.is_discount == "true") {
            self.removePromoCode()
        } else {
            self.applyPromoCode()
        }
    }
    
    @IBAction func btnSubmitAction(_ sender: UIButton) {

        if isValid().res == false {
            
            self.view.makeToast(isValid().message, duration: 1.0, position: .center)
            return
        }
        
        self.saveShippingPayament()
    }
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- VALIDATION
    func isValid() -> (res: Bool, message: String) {
        
        if self.isStoreCreditApplied && !self.isFreeOrderUsingStoreCredit && self.paymentMethodStoreCreditPartner == "" {
            
            return (res: false, message: APP_LBL().please_select_some_payment_method_first)
            
        } else if paymentMethod == "" {
            
            return (res: false, message: APP_LBL().please_select_some_payment_method_first)
        }
        
        return (res: true, message: "")
    }
    
    //MARK:- IS GIFT CARD EXIST
        func isGiftCardExist() {
            
            for prod in self.arrProduct {
                
               // let filt = APP_DEL.arrGftCardData.first(where: { (item) -> Bool in ((item.product_id ?? "") == (prod.product_id ?? ""))})
                
                let filt = APP_DEL.arrGftCardData.first(where: { (item) -> Bool in ((item.child_product_id ?? "") == (prod.child_product_id ?? ""))})
                
                if filt != nil {
                    self.isExistGiftCard = true
                    break;
                }
            }
        }
    
    //MARK:- UPDATE STORE CREDIT
    func updateStoreCredit(isForUpdate: Bool, value: Bool) {
        
        if isForUpdate {
            
            if value == true {
                
                self.isStoreCreditApplied = true
                UserDefaults.standard.set(true, forKey: "isStoreCredit")
                
            } else {
                
                self.isStoreCreditApplied = false
                UserDefaults.standard.set(false, forKey: "isStoreCredit")
            }
        }
        
        if isStoreCreditApplied {
            
            self.lblStoreCreditValue.text = self.appliedStoreCreditString
            btnStoreCredit.setBackgroundImage(UIImage(named: "StoreSwitchOn"), for: .normal)
            
        } else {
            
            self.appliedStoreCreditString = "\(APP_DEL.selectedCountry.getCurrencySymbol().uppercased()) 0"
            
            self.lblStoreCreditValue.text = "0"
            btnStoreCredit.setBackgroundImage(UIImage(named: "StoreSwitchOff"), for: .normal)
        }
        
        if self.store_creditwithoutcurrency == "0" {
            
            lblStoreCreditApply.text = String(format: "%@ (\(APP_DEL.selectedCountry.getCurrencySymbol().uppercased()) 0)", APP_LBL().use_store_credit)
            
        } else {
            
            lblStoreCreditApply.text = String(format: "%@ (%@)", APP_LBL().use_store_credit, viewCartJSON["data"]["store_credit"].stringValue)
        }
    }
    
    //MARK:- UPDATE CART LIST
    func updateCartList() {
        
        self.arrProduct.removeAll()
                
        self.isCountryCodEnabled = (viewCartJSON["data"]["is_cod_enable_for_country"].stringValue == "1")

        if (viewCartJSON["success"].stringValue.lowercased() == "false".lowercased()) {
            UserDefaults.standard.removeObject(forKey: "cartId")
        }
        
        do {
            
            let catData = try viewCartJSON["data"]["products"].rawData()
            self.arrProduct = try JSONDecoder().decode([CartProduct].self, from: catData)
            
        } catch {
            
        }
        
        
        self.total = PaymentTotal(amounttopay: viewCartJSON["data"]["total"][0]["amounttopay"].stringValue,
                                  shipping_amount: viewCartJSON["data"]["total"][0]["shipping_amount"].stringValue,
                                  discount_amount: viewCartJSON["data"]["total"][0]["discount_amount"].stringValue,
                                  tax_amount: viewCartJSON["data"]["total"][0]["tax_amount"].stringValue,
                                  grandtotal: viewCartJSON["data"]["grandtotal"].stringValue,
                                  grandtotal_without_currency: viewCartJSON["data"]["grandtotal_without_currency"].stringValue,
                                  coupon: viewCartJSON["data"]["coupon"].stringValue,
                                  is_discount: viewCartJSON["data"]["is_discount"].stringValue,
                                  shipping_charge_new: viewCartJSON["data"]["shipping_charge_new"].stringValue,
                                  tax_amount_without_currency: viewCartJSON["data"]["total"][0]["tax_amount_without_currency"].doubleValue,
                                  free_shipping_remaining: viewCartJSON["data"]["free_shipping_remaining"].stringValue)

                            
        self.store_creditwithoutcurrency = viewCartJSON["data"]["store_creditwithoutcurrency"].stringValue
        
        self.isGiftCardExist()
        
        self.updateDetail()
    }
    
    func manageCarousel() {
        
        self.carousel.dataSource = self
        self.carousel.delegate = self
        
        self.carousel.type = iCarouselType.linear
        self.carousel.isPagingEnabled = true
        self.carousel.centerItemWhenSelected = true
        self.carousel.bounces = false
        self.carousel.currentItemIndex = 0
        self.carousel.decelerationRate = 0.6
        self.carousel.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.carousel.reloadData()
    }
    
    //MARK:- UPDATE DETAIL
    func updateDetail() {
        
        self.lblOffer.text = "\(self.arrOffers.count) \(APP_LBL().offers_available.uppercased())"
        if (self.arrOffers.count) > 0 {
            self.lblOffer.superview?.isHidden = false
        } else {
            self.lblOffer.superview?.isHidden = true
        }
        
        self.tblView.reloadData()
        
        self.carousel.reloadData()
        
        
        if APP_DEL.selectedLanguage == Arabic{
            self.lblAddCountry.text = "المملكة العربية السعودية"
        }
        else{
            self.lblAddCountry.text = "Saudi Arabia"
        }
       
        self.lblAddCountry.text =  self.addressOBJ["country"] ?? "Saudi Arabia"
        
        self.lblAddCity.text = addressOBJ["city"] ?? ""
        self.lblAddNeighbourhood.text = addressOBJ["neighbour"] ?? ""
        self.lblAddPhone.text = "\(APP_DEL.selectedCountry.phone_code ?? "+966") \(addressOBJ["phone"] ?? "")"
        self.lblAddName.text = addressOBJ["name"] ?? ""

        btnChangeAddress.setTitle(APP_LBL().change.uppercased(), for: .normal)


        if (self.total.coupon != "") && (self.total.is_discount == "true") {

            self.btnApplyPromoCode.setTitle(APP_LBL().remove.uppercased(), for: UIControl.State.normal);
            self.txtPromoCode.text = self.total.coupon

        } else {

            self.btnApplyPromoCode.setTitle(APP_LBL().apply.uppercased(), for: UIControl.State.normal);
            self.txtPromoCode.text = ""
        }

        self.updateStoreCredit(isForUpdate: false, value: false)


        progressView.layer.cornerRadius = 5.0
        progressView.clipsToBounds = true
        
        self.lblRequiredFreeShipping.font  = UIFont(name: "Cairo-Regular", size: 16)!
        
        if self.total.free_shipping_remaining == "0"{
            
            self.viewFreeShipping.isHidden = false
            self.lblRequiredFreeShipping.text = APP_LBL().your_order_qualify_for_free_shipping
            progressView.progress = 1.0
            progressView.setProgress(progressView.progress, animated: true)
            
            progressView.trackTintColor = UIColor.black
            progressView.progressTintColor = UIColor.init(hexString: "#FCB015")
            
        }
        else if self.total.free_shipping_remaining == "-1"
        {
            self.viewFreeShipping.isHidden = true
            self.lblRequiredFreeShipping.text = ""
            progressView.progress = 0.0
            progressView.setProgress(progressView.progress, animated: true)
        }
        else
        {
            
            progressView.trackTintColor = UIColor.black
            progressView.progressTintColor = UIColor.red
            
            self.viewFreeShipping.isHidden = false
            let stringFreeShipping = APP_LBL().free_shipping_remaining as String
            let strAddMoreAmount = APP_DEL.selectedCountry.getCurrencySymbol() + " " + (self.total.free_shipping_remaining ?? "") as String
            self.lblRequiredFreeShipping.text = stringFreeShipping.replacingOccurrences(of: "*", with: strAddMoreAmount)
            
            
            let FreeShippingAmount = Double(self.total.free_shipping_remaining ?? "0.0") ?? 0.0
            let minAmoutShipping = Double(APP_DEL.extrafee_minimum_order_amount) ?? 0.0
            let FinalProgress =  (FreeShippingAmount * 100.0) / minAmoutShipping
            
            progressView.progress = Float((100.0 - FinalProgress)/100.0)
            progressView.setProgress(progressView.progress, animated: true)
            
            let range = (self.lblRequiredFreeShipping.text! as NSString).range(of: strAddMoreAmount)
            let mutableAttributedString = NSMutableAttributedString.init(string: self.lblRequiredFreeShipping.text ?? "")
            mutableAttributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.init(hexString: "#FCB015") ?? UIColor.black, range: range)
            self.lblRequiredFreeShipping.attributedText = mutableAttributedString
            
        }
        
        if self.total.shipping_charge_new != "0" {
            
            
            self.lblShipingValue.text = (APP_DEL.selectedCountry.getCurrencySymbol().uppercased() + " " + (self.total.shipping_charge_new ?? "0"))
        }
        else
        {
             self.lblShipingValue.text = APP_LBL().free
        }
        
        
        if (total.discount_amount == "SAR 0") {
            self.lblDiscountValue.text = "0"
        } else {
            self.lblDiscountValue.text = self.total.discount_amount ?? ""
        }

        if (total.tax_amount == "SAR 0") {
            self.lblTaxesValue.text = "0"
        } else {
            self.lblTaxesValue.text = self.total.tax_amount
        }

        
        
        self.lblSubTotalValue.text = total.amounttopay
        self.lblTotalValue.text = self.total.grandtotal
//        self.lblTaxesFees.text = "+ " + (self.total.tax_amount ?? "") + " " + APP_LBL().taxes_and_fees.uppercased()
         self.lblTaxesFees.text = APP_LBL().taxes_and_fees.uppercased()
        

        self.btnSubmit.setTitle(APP_LBL().submit.uppercased(), for: .normal)
        
        self.scrolVIEW.isHidden = false
        self.stackBottom.isHidden = false
        
        let GrandTotalWithputCurrency = self.total.grandtotal_without_currency
        grandTotalInt = Int(GrandTotalWithputCurrency ?? "0") ?? 0
        
    }
    
}

//MARK:- PAYMENT GATEWAY - TAMARA PAYMENT GATEWAY
extension NejreeCheckoutVC {
//    let MERCHANT_USER = "tamara"
//    let MERCHANT_PASS = "l&MSVSTlfaao"
//    let HOST = "https://api-sandbox.tamara.co"
//    let MERCHANT_TOKEN = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhY2NvdW50SWQiOiIxZjdmYWU4Ni03NzM5LTQ3ZjItOGMxZC00MThlMThhZjMwMjIiLCJ0eXBlIjoibWVyY2hhbnQiLCJzYWx0IjoiMmM3ZTBhODk2NzI2MWZiNDVjYjk2OTllYzcyODNjYjUiLCJpYXQiOjE1ODg3MTQzMTQsImlzcyI6IlRhbWFyYSJ9.iuUjhJhv59AYZUtpsktGL8O3NAR7Qk9X5mYxHA2y92u4XMTDQyWl50otX0qIO2turq4aIIs5jpcyLQWWXtZtItGY4V8280KtnL3eeZ5WKyV6yufwoaHILCTeU6Qoc-Dxwc0oX7J-TP5Wib1Cb5bu6MPBQB6d45UUaXfvaOKYBzvrHolW8OeSJRaGWnf6bajzEg9d2lN621ptusyIU7GO3wjg0NuSfnSjLWUWsOkA2AJvvY_sPQatrgT6Qgofjq564k5TrjXSLgFmPb8JcHFa9AYGcCFmcvYmPh6q07Gn0t-VV8_x6qlc1yAx6PhRGEViPQbN0gZnp0A-Bm9oCE77Cg"
//    product_type = "Digital"
    func checkout(host : String, merchant_token: String, product_type : String, successUrl : String, failedUrl : String, cancelUrl : String, notificationUrl : String, paymentType : String, locale : String) {
        let currency = "SAR"
        
        let countryCode = "SA"
        // check mobile number and skip next if mobile number is not there
        if let MobileNo = UserDefaults.standard.object(forKey: "mobile_number"){
            
            print("Mobile Number is there")
            print(MobileNo)
        }
        else
        {
            let showTitle = APP_LBL().confirmation
            let showMsg = APP_LBL().please_add_phone_number
            
            let confirmationAlert = UIAlertController(title: showTitle, message: showMsg, preferredStyle: UIAlertController.Style.alert);
            
            confirmationAlert.addAction(UIAlertAction(title: APP_LBL().ok, style: .default, handler: { (action: UIAlertAction!) in
                
                let vc = UIStoryboard(name: "cedMageAccounts", bundle: nil).instantiateViewController(withIdentifier: "cedMageProfileChangeNumber") as! cedMageProfileChangeNumber
                vc.jsonData = self.jsonData
                self.navigationController?.pushViewController(vc, animated: true)
                
            }));
            
            self.present(confirmationAlert, animated: true, completion: nil)
            
            return
        }
        
        let tamaraCheckout = TamaraCheckout(endpoint: host, token: merchant_token)
        
        //self.appState.isLoading = true
        
        let totalAmountObject = TamaraAmount(amount:self.total.grandtotal_without_currency!, currency: currency)
        
        let taxAmountObject = TamaraAmount(amount: String(format:"%f", self.total.tax_amount_without_currency!), currency: currency)
        
        let shippingAmountObject = TamaraAmount(amount: String(format:"%f", self.total.shipping_charge_new!), currency: currency)
        
        var itemList: [TamaraItem] = []
        
        
        self.arrProduct.forEach { (item) in
            itemList.append(TamaraItem(
                //referenceID: UUID().uuidString,
                referenceID: self.orderStatusData["orderId"] ?? UUID().uuidString,
                type: product_type,
                name: item.product_name ?? "",
                sku: item.child_product_id ?? "", // temp
                quantity: item.quantity ?? 1,
                unitPrice: TamaraAmount(amount: String(format:"%f", item.subtotal_price_without_currency ?? 0.0), currency: currency),
                discountAmount: TamaraAmount(amount: String(format:"%f", 0.0), currency: currency),
                taxAmount: TamaraAmount(amount: String(format:"%f", 0.0), currency: currency),
                totalAmount: TamaraAmount(amount: String(format:"%f", item.subtotal_price_without_currency ?? 0.0), currency: currency)
            ))
        }
        
        let shippingAddress = TamaraAddress(
            firstName: self.addressOBJ["name"] ?? "",
            lastName: "",
            line1: self.addressOBJ["street"] ?? "" ,
            line2: self.addressOBJ["neighbour"] ?? "",
            region: "",
            city: self.addressOBJ["city"] ?? "" ,
            countryCode: "SA",
            phoneNumber: self.addressOBJ["phone"] ?? ""
        )
        
        let billingAddress = TamaraAddress(
            firstName: self.addressOBJ["name"] ?? "",
            lastName: "",
            line1: self.addressOBJ["street"] ?? "" ,
            line2: self.addressOBJ["neighbour"] ?? "",
            region: self.addressOBJ["neighbour"] ?? "",
            city: self.addressOBJ["city"] ?? "" ,
            countryCode: "SA",
            phoneNumber: self.addressOBJ["phone"] ?? ""
        )
        
        let merchantUrl = TamaraMerchantURL(
            success: successUrl,
            failure: failedUrl,
            cancel: cancelUrl,
            notification: notificationUrl
        )
        let defaults = UserDefaults.standard
      //  let mobile = defaults.value(forKey: "mobile_number") as? String  ?? ""
        
        

        // let email = defaults.value(forKey: "EmailId") as? String  ?? ""
                let userInfoDict = UserDefaults.standard.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]()

                let name = defaults.value(forKey: "name") as? String  ?? ""
                _ = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
                let consumer = TamaraConsumer(
                    firstName: name,
                    lastName: "",
                    phoneNumber: UserDefaults.standard.object(forKey: "mobile_number") as! String,
                    email: userInfoDict["email"] ?? "",
                    nationalID: "",
                    dateOfBirth: "",
                    isFirstOrder: true
                )
        
        
        
        let requestBody = TamaraCheckoutRequestBody(
            orderReferenceID: self.orderStatusData["orderId"] ?? UUID().uuidString,
            totalAmount: totalAmountObject,
            description: "description",
            countryCode: countryCode,
            paymentType: paymentType,
            locale: locale,
            items: itemList,
            consumer: consumer,
            billingAddress: billingAddress,
            shippingAddress: shippingAddress,
            discount: nil,
            taxAmount: taxAmountObject,
            shippingAmount: shippingAmountObject,
            merchantURL: merchantUrl,
            platform: "iOS",
            isMobile: true,
            riskAssessment: nil
        )
        
                tamaraCheckout.processCheckout(body: requestBody, checkoutComplete: { (checkoutSuccess) in
                   
                    DispatchQueue.main.async {
            
                        guard let url = checkoutSuccess?.checkoutUrl else {return}
                        print("TAMARA =======> Checkout URL :=> \(url)")
                        
                        if URL(string: url) != nil {
                            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PaymentWebVC") as! PaymentWebVC
                            vc.successUrl = successUrl
                            vc.failUrl = failedUrl
                            vc.notificationUrl = notificationUrl
                            vc.payUrl = url
                            vc.deleagteCheckoutFail = self
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                    }
                    
                }, checkoutFailed: { (checkoutFailed) in
                    //Handle failed case
                    print(checkoutFailed?.message ?? "")
                    
                    ///Do something when have error.
                })
        
        
//
//        tamaraCheckout.processCheckout(body: requestBody, checkoutComplete: { (checkoutUrl) in
//
//
//            DispatchQueue.main.async {
//
//                guard let url = checkoutUrl else {return}
//                print("TAMARA =======> Checkout URL :=> \(url)")
//                //                self.appState.viewModel = TamaraSDKCheckoutViewModel(url: url, merchantURL: merchantUrl)
//                //                self.appState.currentPage = AppPages.Checkout
//
//                if URL(string: url) != nil {
//                    let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PaymentWebVC") as! PaymentWebVC
//        //            let recCheckout = CheckoutOrder(failUrl: failedUrl, successUrl: successUrl)
//                    vc.successUrl = successUrl
//                    vc.failUrl = failedUrl
//                    vc.notificationUrl = notificationUrl
//                    vc.payUrl = url
//                    vc.deleagteCheckoutFail = self
//                    self.navigationController?.pushViewController(vc, animated: true)
//                }
//            }
//
//        }, checkoutFailed: { (error) in
//            print(error)
//            DispatchQueue.main.async {
//                //                self.appState.isLoading = false
//                //                self.appState.orderSuccessed = false
//                //                self.appState.currentPage = AppPages.Success
//            }
//        })
    }
    

    //MARK:- GET CHECKOUT TOKEN FROM SERVER
    func getTamaraPaymentTokenFromServer() {
        var postData = [String:String]()
        
        if let storeID =  UserDefaults.standard.value(forKey: "storeId") as? String {
            postData["store_id"] = storeID
        }
        
        if let userInfoDict = UserDefaults.standard.object(forKey: "userInfoDict") as? [String: String] {
            
            postData["hashkey"] = userInfoDict["hashKey"]
            postData["customer_id"] = userInfoDict["customerId"]
        }
        
       // postData["device_id"] = paycontroller?.getUDID() ?? ""
        postData["type"] = self.paymentMethod.lowercased()
        if let  appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            postData["ios_version"] = appVersion
        }
        postData["android_version"] = ""
        postData["order_id"] = self.orderStatusData["orderId"]
        
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);
        //https://dev05.nejree.com/rest/V1/mobiconnect/checkout/gettamarakeys
        API().callAPI(endPoint: "mobiconnect/checkout/gettamarakeys", method: .POST, param: postData) { (json, err) in
            
            //   DispatchQueue.main.async {
            
            cedMageLoaders.removeLoadingIndicator(me: self);
            
            let json_0 = json[0]
            
            if err == nil {
                
                
                if json_0["checkout_url"].stringValue == ""{
                    self.checkout(host: json_0["host"].stringValue, merchant_token: json_0["merchant_token"].stringValue, product_type: json_0["product_type"].stringValue, successUrl: json_0["success_url"].stringValue, failedUrl: json_0["failed_url"].stringValue, cancelUrl: json_0["cancel_url"].stringValue, notificationUrl: json_0["notification_url"].stringValue, paymentType: json_0["payment_type"].stringValue, locale: json_0["locale"].stringValue)
                }
                else{
                    let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PaymentWebVC") as! PaymentWebVC
                    vc.successUrl = json_0["success_url"].stringValue
                    vc.failUrl = json_0["failed_url"].stringValue
                    vc.notificationUrl = json_0["notification_url"].stringValue
                    vc.payUrl = json_0["checkout_url"].stringValue
                    vc.deleagteCheckoutFail = self
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                
                
                
                

            }
            //  }
        }
    }
    
}


//MARK:- PAYMENT GATEWAY - BRAINTREE
extension NejreeCheckoutVC {
    
    //MARK:- GENERATE BRAINTREE TOKEN POST
    func generateBraintreeTokenPOST() {
     
        var postData = [String:String]()

        if let userInfoDict = UserDefaults.standard.object(forKey: "userInfoDict") as? [String: String] {
            
            postData["customer_id"] = userInfoDict["customerId"]
        }

        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);
        
        API().callAPI(endPoint: "mobiconnect/mobibrain/generatetokenpost", method: .POST, param: postData) { (json, err) in
            
         //   DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);

                let json_0 = json[0]
                
                if err == nil {

                    if json_0["success"].stringValue == "true" {
                        
                        let clientToken = json["message"].stringValue
                        
                        if json["creditCards"].arrayValue.count > 0 {
                            
                            for creditCard in json["creditCards"].arrayValue {
                                
                                let maskedNumber = creditCard["maskedNumber"].stringValue
                                let imgUrl = creditCard["imageUrl"].stringValue
                                let token  = creditCard["token"].stringValue
                                
                                self.previousMethods.append(["maskedNumber":maskedNumber,"imgUrl":imgUrl,"token":token])
                            }
                        }
                        
                        self.clientToken = clientToken
                        
                    }
                }
         //   }
        }
    }
}

//MARK:- PAYMENT GATEWAY - STC
extension NejreeCheckoutVC: StcMobileVerified {
    
    //MARK:- stcSetSTCVarify - 0
    func stcSetSTCVarify() {
        
        var postData = [String:String]()
        
//        if let addressDynamic =  UserDefaults.standard.value(forKey: "selectedAddress") as? [String:Any] {
//
//            postData["mobile_number"] = addressDynamic["phone"] as? String
//        }
        
        if let userInfoDict = UserDefaults.standard.object(forKey: "userInfoDict") as? [String: String] {
            
            if let customId = userInfoDict["customerId"] {
                postData["customer_id"] = customId
            }
            
            if let MobileNo = UserDefaults.standard.object(forKey: "mobile_number"){
          
                postData["mobile_number"] = MobileNo as? String
            }
            else
            {
                   let showTitle = APP_LBL().confirmation
                   let showMsg = "Please verify the phone number"
           
                   let confirmationAlert = UIAlertController(title: showTitle, message: showMsg, preferredStyle: UIAlertController.Style.alert);
                  
                   confirmationAlert.addAction(UIAlertAction(title: APP_LBL().ok, style: .default, handler: { (action: UIAlertAction!) in
           
                       
                   }));
           
                   self.present(confirmationAlert, animated: true, completion: nil)
                     
                   return
            }
            
        }
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);
        
        API().callAPI(endPoint: "stc/setstcvarify", method: .POST, param: postData) { (json, err) in
            
          //  DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                if err == nil {
                    
                    self.STC_BillNumber = API().randomAlphaNumericString(length: 6)
                    
                    if json[0]["success"].stringValue == "true" {
                        
                        
                        self.stcDirectPayment(tokenid: json[0]["tokenid"].stringValue, BillNumber: self.STC_BillNumber, MerchantNote: self.STC_MerchantNote)
                        
                    } else {
                        
                        self.stcDirectPaymentAuthorize(RefNum: self.STC_RefNum, BillNumber: self.STC_BillNumber, MerchantNote: self.STC_MerchantNote)
                        
                    }
                }
          //  }
        }
    }
    
    func stcMobileVerified(res: Bool, STC_OtpReference: String, STC_STCPayPmtReference: String, STC_TokenID: String) {
        
        if res {
            
            self.STC_OtpReference = STC_OtpReference
            self.STC_STCPayPmtReference = STC_STCPayPmtReference
            
            self.stcDirectPayment(tokenid: STC_TokenID, BillNumber: self.STC_BillNumber, MerchantNote: self.STC_MerchantNote)
            
        } else {
            
            self.additionalINFO(additional_info: "", failure: "true", order_id: self.orderStatusData["orderId"]!)
        }
    }
    
    //MARK:- stcDirectPaymentAuthorize - 1
    func stcDirectPaymentAuthorize(RefNum: String, BillNumber: String, MerchantNote: String) {
        
        var postData = [String:String]()
        
        postData["BillNumber"] = BillNumber
        postData["MerchantNote"] = MerchantNote
        postData["RefNum"] = RefNum
        postData["Amount"] = self.total.grandtotal ?? ""
        

        if let MobileNo = UserDefaults.standard.object(forKey: "mobile_number"){
        
              postData["mobile_number"] = MobileNo as? String
          }
        
        
        if let userInfoDict = UserDefaults.standard.object(forKey: "userInfoDict") as? [String: String] {
            
            if let customId = userInfoDict["customerId"] {
                postData["customer_id"] = customId
            }
        }
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);
        
        API().callAPI(endPoint: "stc/directpaymentauthorize", method: .POST, param: postData) { (json, err) in
            
          //  DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                if err == nil {
                    
                    if json[0]["OtpReference"].stringValue != "" && json[0]["STCPayPmtReference"].stringValue != ""{
                      
                        
                        self.STC_OtpReference = json[0]["OtpReference"].stringValue
                        self.STC_STCPayPmtReference = json[0]["STCPayPmtReference"].stringValue
                        
                        let vc = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "MobileVerifyVC") as! MobileVerifyVC
                        vc.hidesBottomBarWhenPushed = true
                        
                        vc.STC_Amount = self.total.grandtotal ?? ""
                        vc.STC_MerchantNote = self.STC_MerchantNote
                        vc.STC_OtpReference = self.STC_OtpReference
                        vc.STC_STCPayPmtReference = self.STC_STCPayPmtReference
                        vc.STC_RefNum = RefNum
                        vc.STC_BillNumber = MerchantNote
                        vc.customerId = postData["customer_id"]!
                        
                        vc.delegateStcMobileVerified = self
                        vc.modalPresentationStyle = .overFullScreen
                        self.present(vc, animated: true, completion: nil)
                        //self.navigationController?.pushViewController(vc, animated: true)
                        
                    } else {
                     
                        cedMageHttpException.showAlertView(me: self, msg: json[0]["message"].stringValue, title: APP_LBL().error)
                    }
                }
          //  }
        }
    }
    
    //MARK:- stcDirectPayment - 3
    func stcDirectPayment(tokenid: String, BillNumber: String, MerchantNote: String) {
        
        var postData = [String:String]()
        
        postData["BillNumber"] = BillNumber
        postData["MerchantNote"] = MerchantNote
        postData["TokenId"] = tokenid
        postData["Amount"] = self.total.grandtotal ?? ""
        
        if let addressDynamic =  UserDefaults.standard.value(forKey: "selectedAddress") as? [String:Any] {
            
            postData["mobile_number"] = addressDynamic["phone"] as? String
        }
        
        if let userInfoDict = UserDefaults.standard.object(forKey: "userInfoDict") as? [String: String] {
            
            if let customId = userInfoDict["customerId"] {
                postData["customer_id"] = customId
            }
        }
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);
        
        API().callAPI(endPoint: "stc/directpayment", method: .POST, param: postData) { (json, err) in
            
        //    DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                if err == nil {
                    
                    if json[0]["success"].stringValue == "true" {
                      
                        // success view
                    } else {
                        
                        cedMageHttpException.showAlertView(me: self, msg: json[0]["message"].stringValue, title: APP_LBL().error)
                    }
                }
          //  }
        }
    }
}

//MARK:- PAYMENT GATEWAY - PAYFORT & CHECKOUT
extension NejreeCheckoutVC: PKPaymentAuthorizationViewControllerDelegate, PaymentFail {
    
    
    //MARK:- GET PAYFORT TOKEN FROM SERVER
    func getPayfortTokenFromServer() {
        
        var postData = [String:String]()
        
        if let storeID =  UserDefaults.standard.value(forKey: "storeId") as? String {
            postData["store_id"] = storeID
        }
        
        if let userInfoDict = UserDefaults.standard.object(forKey: "userInfoDict") as? [String: String] {
            
            postData["hashkey"] = userInfoDict["hashKey"]
            postData["customer_id"] = userInfoDict["customerId"]
        }
        
        postData["device_id"] = paycontroller?.getUDID() ?? ""
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);
        
        API().callAPI(endPoint: "mobiconnect/catalog/payfort/", method: .POST, param: postData) { (json, err) in
            
           // DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                let json_0 = json[0]
                
                if err == nil {
                    
                    guard let userInfoDict = UserDefaults.standard.object(forKey: "userInfoDict") as? [String: String] else {
                        return;
                    }
                    
                    self.createPayfortPaymentApiRequest(response: json_0.rawValue, amount: self.total.grandtotal ?? "", email: userInfoDict["email"] ?? "", orderID: self.orderStatusData["orderId"] ?? "")
                }
           // }
        }
    }
    
    //MARK:- GET CHECKOUT TOKEN FROM SERVER
    func getCheckoutPaymentTokenFromServer() {
        
        
        var postData = [String:String]()
        
        if let storeID =  UserDefaults.standard.value(forKey: "storeId") as? String {
            postData["store_id"] = storeID
        }
        
        if let userInfoDict = UserDefaults.standard.object(forKey: "userInfoDict") as? [String: String] {
            
            postData["hashkey"] = userInfoDict["hashKey"]
            postData["customer_id"] = userInfoDict["customerId"]
        }
        
        postData["device_id"] = paycontroller?.getUDID() ?? ""
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);
        
        API().callAPI(endPoint: "checkoutkeys/", method: .GET, param: postData) { (json, err) in
            
         //   DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                let json_0 = json[0]
                
                if err == nil {
                    
                    self.strPublicKey = json_0["isPublicKey"].stringValue
                    self.strSecretKey = json_0["isSecretKey"].stringValue
                    
                    
                    self.strCurrentCheckoutMode = json_0["mode"].stringValue
                    
                    if (self.paymentMethod.lowercased() == "checkoutcom_card_payment") {
                        
                        let storyboard = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil);
                        let viewController = storyboard.instantiateViewController(withIdentifier: "AddCardVC") as! AddCardVC;
                        viewController.strGrandTotal = self.total.grandtotal ?? ""
                        viewController.strOrderId = self.orderStatusData["orderId"]!
                        viewController.strPublicKey = json_0["isPublicKey"].stringValue
                        viewController.strSecretKey = json_0["isSecretKey"].stringValue
                        
                        viewController.PaymentSuccessURL = json_0["success_url"].stringValue
                        viewController.PaymentFailURL = json_0["fail_url"].stringValue
                        
                        viewController.deleagteCheckoutFail = self
                        
                        viewController.strCurrentCheckoutMode = json_0["mode"].stringValue
                        viewController.productsData = self.arrProduct;
                        viewController.strOrderDate = self.orderStatusData["order_date"]!
                        viewController.hidesBottomBarWhenPushed = true
                        
                        self.navigationController?.pushViewController(viewController, animated: true)
                        
                    } else if (self.paymentMethod.lowercased() == "applepayfort" || self.paymentMethod.lowercased() == "applepaycheckout") {
                        
                        guard let userInfoDict = UserDefaults.standard.object(forKey: "userInfoDict") as? [String: String] else {
                            return;
                        }
                        
                        self.createPaymentApiRequest(response: self.responsePassApplePay, amount: self.total.grandtotal ?? "", email: userInfoDict["email"] ?? "", orderID: self.orderStatusData["orderId"]!)
                        
                    }
                }
          //  }
        }
    }
    
    func PaymentFailForCheckout(index: Int){
        
        if index == 0 {
        
            //Order Fail
            self.additionalINFO(additional_info: "", failure: "true", order_id: self.orderStatusData["orderId"]!)
            
        } else {
            
            //Order Success
            self.additionalINFO(additional_info: "", failure: "false", order_id: self.orderStatusData["orderId"]!)
        }
    }
    
    func createPayfortPaymentApiRequest(response: Any?, amount: String, email: String, orderID: String) {
        
        self.applePaySuccessCheck = true;
        
        if (response != nil) {
            
            let responseDict = response as! NSDictionary
            self.tokenstr = responseDict["sdk_token"] as! String
            
            let payloadDict = NSMutableDictionary.init()
          
            let AmountFinal = getTotal(floatValue: Double(amount)!)
            
            payloadDict.setValue(self.tokenstr , forKey: "sdk_token")
            payloadDict.setValue(AmountFinal, forKey: "amount")
            // payloadDict.setValue("AUTHORIZATION", forKey: "command")
            payloadDict.setValue("PURCHASE", forKey: "command")
            payloadDict.setValue("SAR", forKey: "currency")
            payloadDict.setValue(email, forKey: "customer_email")
            
            if APP_DEL.selectedLanguage == Arabic{
                payloadDict.setValue(Arabic, forKey: "language")
            }
            else{
                payloadDict.setValue(English, forKey: "language")
            }
            let orderd = orderID
            payloadDict.setValue(orderd, forKey: "merchant_reference")
            // payloadDict.setValue("VISA" ,forKey: "payment_option")
            
            paycontroller?.modalPresentationStyle = .fullScreen
            
            paycontroller?.callPayFort(withRequest: payloadDict, currentViewController: self,
                                       success: { (requestDic, responeDic) in
                                        
                                        print("Success:\(String(describing: responeDic))")
                                        self.additionalINFO(additional_info: "", failure: "false", order_id: orderID)
                                        
                                        
            },
                                       canceled: { (requestDic, responeDic) in
                                        print("Canceled:\(String(describing: responeDic))")
                                        self.additionalINFO(additional_info: "", failure: "true", order_id: orderID)
                                        
            },
                                       faild: { (requestDic, responeDic, message) in
                                        
                                        print("Failure:\(String(describing: responeDic))")
                                        print("Failure message:\(String(describing: message))")
                                        self.additionalINFO(additional_info: "", failure: "true", order_id: orderID)
                                        
            })
        }
    }
    
    func createPaymentApiRequest(response:Any?,amount:String,email:String,orderID:String) {
        
        if (response != nil) {
            
            jsonResponseDictTemp = response as! NSDictionary

            AmountFinal = getTotalForApplePay(floatValue: Double(amount)!)
            
            let request = PKPaymentRequest()
            
            request.merchantIdentifier = "merchant.com.nejree.CheckoutApp"
            //request.merchantIdentifier = "merchant.com.nejree.ourapp"
//            request.merchantIdentifier = "merchant.com.nejree.applepay"
            if #available(iOS 12.1.1, *) {
                request.supportedNetworks = [PKPaymentNetwork.visa, PKPaymentNetwork.masterCard, PKPaymentNetwork.amex, PKPaymentNetwork.mada]
            } else {
                // Fallback on earlier versions
                request.supportedNetworks = [PKPaymentNetwork.visa, PKPaymentNetwork.masterCard, PKPaymentNetwork.amex]
            }
            request.merchantCapabilities = PKMerchantCapability.capability3DS
            request.countryCode = "SA"
            request.currencyCode = "SAR"
            
            
//            let userInfoDict = UserDefaults.standard.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
//
//
//            let contact = PKContact()
//            var name = PersonNameComponents()
//            name.givenName = UserDefaults.standard.value(forKey: "name") as? String
//            name.familyName = ""
//            contact.name = name
//            contact.emailAddress = userInfoDict["email"]!
//
//            contact.phoneNumber = CNPhoneNumber.init(stringValue: (UserDefaults.standard.value(forKey: "mobile_number") as? String)!)
//
//            print(UserDefaults.standard.value(forKey: "selectedAddress") as Any)
//            let addressDynamic =  UserDefaults.standard.value(forKey: "selectedAddress") as! NSDictionary
//
//
//
//
//            let address = CNMutablePostalAddress()
//            address.street = addressDynamic.value(forKey: "street") as! String
//            address.city = addressDynamic.value(forKey: "city") as! String
//            address.state = ""
//            address.postalCode = ""
//            address.country = "SAUDI ARABIA"
//            address.isoCountryCode = ""
//            contact.postalAddress = address
            
//            request.shippingContact = contact
//            request.requiredShippingContactFields = [.emailAddress, .name, .phoneNumber, .postalAddress]
            
            request.paymentSummaryItems = [
                PKPaymentSummaryItem(label: "Nejree", amount: NSDecimalNumber(value: AmountFinal))
            ]
            let applePayController = PKPaymentAuthorizationViewController(paymentRequest: request)
            applePayController?.delegate = self
            self.present(applePayController!, animated: true, completion: nil)
            
        }
    }
    
    func paymentAuthorizationViewControllerDidFinish(_ controller: PKPaymentAuthorizationViewController) {
        
        if self.applePaySuccessCheck == false {
            self.additionalINFO(additional_info: "", failure: "true", order_id: self.orderStatusData["orderId"]!)
        } else {
            self.additionalINFO(additional_info: "", failure: "false", order_id: self.orderStatusData["orderId"]!)
        }
        
        controller.dismiss(animated: true, completion: nil)
    }
    
    func paymentAuthorizationViewController(_ controller: PKPaymentAuthorizationViewController, didAuthorizePayment payment: PKPayment, handler completion: @escaping (PKPaymentAuthorizationResult) -> Void) {
        
        self.getTokenCheckout(payment: payment) { (isFinished,strToken)  in
            
            if  (isFinished!) {
                
                self.applePaySuccessCheck = true
                
                completion(PKPaymentAuthorizationResult(status: PKPaymentAuthorizationStatus.success, errors: []))
                
            } else {
                
                self.applePaySuccessCheck = false
                
                completion(PKPaymentAuthorizationResult(status: PKPaymentAuthorizationStatus.failure, errors: []))
            }
        }
    }
    
    // Checkout
    func getTokenCheckout(payment : PKPayment, handler:@escaping (_ isFinished:Bool?, _ strToken : String?)-> Void) {
                
        let encryptedPaymentData = payment.token.paymentData
//        let paymentMethod = payment.token.paymentMethod
//        let txId = payment.token.transactionIdentifier
//        let decryptedPaymentData:NSString! = NSString(data: encryptedPaymentData, encoding: String.Encoding.utf8.rawValue)

        do {
            // make sure this JSON is in the format we expect
            if let json = try JSONSerialization.jsonObject(with: encryptedPaymentData, options: []) as? [String: Any] {
                // try to read out a string array
                
                let headerDict = json["header"] as! NSDictionary
                                
                let parameters = "{\n  \"type\": \"applepay\",\n  \"token_data\": {\n    \"version\": \"\(json["version"] ?? "")\",\n    \"data\": \"\(json["data"] ?? "")\",\n    \"signature\": \"\(json["signature"] ?? "")\",\n    \"header\": {\n      \"ephemeralPublicKey\": \"\(headerDict["ephemeralPublicKey"] ?? "")\",\n      \"publicKeyHash\":  \"\(headerDict["publicKeyHash"] ?? "")\",\n      \"transactionId\": \"\(headerDict["transactionId"] ?? "")\"\n    }\n  }\n}"
                
                print(parameters)
                
                let postData = parameters.data(using: .utf8)
                
                var strCheckoutBaseURL = ""
                
                if strCurrentCheckoutMode == "1" {
                    //Sandbox Mode
                    strCheckoutBaseURL = "https://api.sandbox.checkout.com/tokens"
                } else {
                    strCheckoutBaseURL = "https://api.checkout.com/tokens"
                }
                
                
                var request = URLRequest(url: URL(string: strCheckoutBaseURL)!,timeoutInterval: Double.infinity)
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                request.addValue(strPublicKey, forHTTPHeaderField: "Authorization")
                
                request.httpMethod = "POST"
                request.httpBody = postData
                
                let task = URLSession.shared.dataTask(with: request) { data, response, error in
                    
                    guard let data = data else {
                        
                        print(String(describing: error))
                        handler(false, "")
                        return
                    }
                    
                    print(String(data: data, encoding: .utf8)!)
                    let strResponse = String(data: data, encoding: .utf8)!
                    let strPostData = strResponse.data(using: .utf8)
                    
                    var token = ""
                    //            do {
                    let json = try? JSONSerialization.jsonObject(with: strPostData!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                    
                    print(json as Any)
                    
                    
                    // Parse JSON data
                    if (json?["token"] != nil) {
                        
                        token = json?["token"] as! String
                        print(token)
                        
                        DispatchQueue.main.async {
                            
                            self.callPaymentRequest((json?["token"] as! String)) { (isFinished) in
                                
                                handler(isFinished, (json?["token"] as! String))
                            }
                        }
                        
                    } else {
                        
                        handler(false, "")
                    }
                }
                
                task.resume()
            }
            
        } catch let error as NSError {
            
            print("Failed to load: \(error.localizedDescription)")
        }
    }
    
    func callPaymentRequest(_ strToken :String, responseHandler: @escaping (_ isFinished:Bool?) -> Void) {
        
        let FinalTotal = (Int((Double(self.total.grandtotal ?? "0.0") ?? 0.0) * 100))
        
        guard let userInfoDict = UserDefaults.standard.object(forKey: "userInfoDict") as? [String: String] else {
            return;
        }
        
        let parameters = "{\n  \"source\": {\n    \"type\": \"token\",\n    \"token\": \"\(strToken)\"\n  }, \n  \"customer\": {\n\"email\": \"\(userInfoDict["email"]!)\",\n\"name\": \"\(UserDefaults.standard.value(forKey: "name") as? String ?? "")\" \n} , \n  \"amount\": \(FinalTotal),\n  \"currency\": \"SAR\"\n,\"reference\": \"\(self.orderStatusData["orderId"]!)\"}"
        
                
        let postData = parameters.data(using: .utf8)
        
        var strCheckoutBaseURL = ""
        
        if strCurrentCheckoutMode == "1" {
            //Sandbox Mode
            strCheckoutBaseURL = "https://api.sandbox.checkout.com/payments"
        } else {
            strCheckoutBaseURL = "https://api.checkout.com/payments"
        }
        
        
        var request = URLRequest(url: URL(string: strCheckoutBaseURL)!,timeoutInterval: Double.infinity)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue(strSecretKey, forHTTPHeaderField: "Authorization")
        print("parameters = \(parameters)")
        
        request.httpMethod = "POST"
        request.httpBody = postData
        let taskNew = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data else {
                print(String(describing: error))
                responseHandler(false)
                return
            }
            print(String(data: data, encoding: .utf8)!)
            
            let strResponse = String(data: data, encoding: .utf8)!
            let strPostData = strResponse.data(using: .utf8)
            
            var isApproved = false
            
            let json = try? JSONSerialization.jsonObject(with: strPostData!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
            
            // Parse JSON data
            if (json?["approved"] != nil) {
                isApproved = json?["approved"] as! Bool
                print(isApproved)
                responseHandler(isApproved)
                
            } else {
                responseHandler(false)
            }
        }
        
        taskNew.resume()
    }
    
    func getTotal(floatValue: Double) -> Int {
        return Int(Double(100) * floatValue)
    }
}



//MARK:- API CALL
extension NejreeCheckoutVC {
    
    //MARK:- GET SHIPPING PAYMENT
    func getShippingPayament() {
        
        var postData = [String:String]()
                
        postData["Role"] = role
        postData["Checkout_payment_enabled_for_app"] = "1"
        postData["Payfort_payment_enabled_for_app"] = "1"
        
        if let cartID =  UserDefaults.standard.value(forKey: "cartId") as? String {
            postData["cart_id"] = cartID
        } else {
            postData["cart_id"] = "0"
        }
        
        if let storeID =  UserDefaults.standard.value(forKey: "storeId") as? String {
            postData["store_id"] = storeID
        }
        
        if let userInfoDict = UserDefaults.standard.object(forKey: "userInfoDict") as? [String: String] {
            postData["email"] = userInfoDict["email"]
            postData["hashkey"] = userInfoDict["hashKey"]
            postData["customer_id"] = userInfoDict["customerId"]
        }
        
        if let storeID =  UserDefaults.standard.value(forKey: "storeId") as? String {
            postData["store_id"] = storeID
        }
        
        
        if let  appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            postData["ios_version"] = appVersion
        }
        postData["android_version"] = ""
        postData["country_id"] = APP_DEL.selectedCountry.country_code ?? "SA"

        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);
        
        API().callAPI(endPoint: "mobiconnect/checkout/getshippingpayament", method: .POST, param: postData) { (json, err) in
            
        //    DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                let json_0 = json[0]
                
                if err == nil {

                    if json_0["success"].stringValue == "true" {
                        
                        self.arrFinalPAY = []
                        
                        self.availableStoreCreditInt = json_0["credit_value"].intValue
                        self.availableStoreCreditString = json_0["store_credit"].stringValue
                        
                        if json_0["payments"]["payfort_method_enabled"].stringValue == "true" {
                            self.arrFinalPAY.append(finalPAY(title: "Credit card/mada bank card", value: "payfort"))
                        }
                        
                        if json_0["payments"]["checkoutcom_apple_pay_enabled"].stringValue == "true" {
                            self.arrFinalPAY.append(finalPAY(title: APP_LBL().pay_with_apple_pay, value: "applepaycheckout",descr: json_0["payments"]["apple_promotional_text"].stringValue))
                        }
                      

                                                
                        let tempMethod = json_0["payments"]["methods"]
                        
                        if (tempMethod.stringValue.lowercased()) != ("No Payment Method Availabile.".lowercased()) {
                            
                            for (_, result) in tempMethod {
                                
                                if (result["value"].stringValue == "cashondelivery") {
                                    
                                    if self.isCountryCodEnabled == true {
                                        self.arrFinalPAY.append(finalPAY(title: result["label"].stringValue, value: result["value"].stringValue, descr: result["additional_data"].stringValue, tamara_min: result["tamara_min"].stringValue, tamara_max: result["tamara_max"].stringValue))
                                    }
                                    else
                                    {
                                       
                                        self.viewCOD.isHidden = true
                                        
                                    }
                                    
                                }
                                
                                else {
                                    
                                    self.arrFinalPAY.append(finalPAY(title: result["label"].stringValue, value: result["value"].stringValue, descr: result["additional_data"].stringValue, tamara_min: result["tamara_min"].stringValue, tamara_max: result["tamara_max"].stringValue))
                                }
                            }
                            
                        } else {
                            
                        }
                        
                        self.arrFinalPAY = self.arrFinalPAY.sorted { $0.title! < $1.title! }
                        
                        self.arrOffers.removeAll()
                        for off in json_0["payments"]["all_offers"].arrayValue {
                            self.arrOffers.append(off.stringValue)
                        }
                        
                        self.updateDetail()
                    }
                }
          //  }
        }
    }
    
    //MARK:- SAVE SHIPPING PAYMENT
    func saveShippingPayament(isForSelction: Bool = false) {
            
            var postData = [String:String]()
            
            postData["Role"] = role
            
            if let cartID =  UserDefaults.standard.value(forKey: "cartId") as? String {
                postData["cart_id"] = cartID
            }
            
            if let storeID =  UserDefaults.standard.value(forKey: "storeId") as? String {
                postData["store_id"] = storeID
            }
            
            if let userInfoDict = UserDefaults.standard.object(forKey: "userInfoDict") as? [String: String] {
                
                postData["hashkey"] = userInfoDict["hashKey"]
                postData["customer_id"] = userInfoDict["customerId"]
                postData["email"] = userInfoDict["email"]
            }
            
            if self.paymentMethod.lowercased() == "Braintree".lowercased() {
                
                postData["payment_method"] = "apppayment";
                
            } else if self.paymentMethod.lowercased() == "payfort".lowercased() {
                
                postData["payment_method"] = "apppayment";
                
            } else if self.paymentMethod.lowercased() == "applepayfort" {
                
                postData["payment_method"] = "apppayment";
                
            } else if self.paymentMethod.lowercased() == "applepaycheckout" {
                
                postData["payment_method"] = "checkoutcom_apple_pay";
                
            } else {
                
                postData["payment_method"] = paymentMethod;
            }
            
        
        selectedPaymentMethod = postData["payment_method"] ?? ""
            
            postData["shipping_method"] = shippingMethod;
            
            
            cedMageLoaders.removeLoadingIndicator(me: self);
            cedMageLoaders.addDefaultLoader(me: self);
            
            API().callAPI(endPoint: "mobiconnect/checkout/saveshippingpayament", method: .POST, param: postData) { (json, err) in
                
              //  DispatchQueue.main.async {
                    
                    cedMageLoaders.removeLoadingIndicator(me: self);
                    
                    if err == nil {
                        if isForSelction == true {
                            self.getCartList()
                        } else {
                            self.saveOrder()
                        }
                    }
              //  }
            }
        }
    
    //MARK:- SAVE ORDER
    func saveOrder() {
        
        var postData = [String:String]()
        
        if let cartID =  UserDefaults.standard.value(forKey: "cartId") as? String {
            postData["cart_id"] = cartID
        }
        
        if let userInfoDict = UserDefaults.standard.object(forKey: "userInfoDict") as? [String: String] {
            
            postData["hashkey"] = userInfoDict["hashKey"]
            postData["customer_id"] = userInfoDict["customerId"]
            postData["email"] = userInfoDict["email"]
            
        }
        
        let defaults = UserDefaults.standard
        
        if let store_id = defaults.object(forKey: "storeId") as? String {
            postData["store_id"] = store_id;
        }
        
      
        if selectedPaymentMethod == "stcpayment" || self.paymentMethod.lowercased() == "tamara_pay_later" || self.paymentMethod.lowercased() == "tamara_pay_by_instalments"{
            if let MobileNo = UserDefaults.standard.object(forKey: "mobile_number"){
            
                 print("Mobile Number is there")
              }
              else
              {
                     let showTitle = APP_LBL().confirmation
                    let showMsg = APP_LBL().please_add_phone_number
             
                     let confirmationAlert = UIAlertController(title: showTitle, message: showMsg, preferredStyle: UIAlertController.Style.alert);
                    
                     confirmationAlert.addAction(UIAlertAction(title: APP_LBL().ok, style: .default, handler: { (action: UIAlertAction!) in
             
                         let vc = UIStoryboard(name: "cedMageAccounts", bundle: nil).instantiateViewController(withIdentifier: "cedMageProfileChangeNumber") as! cedMageProfileChangeNumber
                        vc.jsonData = self.jsonData
                                self.navigationController?.pushViewController(vc, animated: true)
                        
                     }));
             
                     self.present(confirmationAlert, animated: true, completion: nil)
                       
                     return
              }
        }
        
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);
        
        API().callAPI(endPoint: "mobiconnect/checkout/saveorder", method: .POST, param: postData) { (json, err) in
            
          //  DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                let json_0 = json[0]
                
                if err == nil {

                    if (json_0["success"].stringValue == "true") {
                        
                        self.orderStatusData["orderId"] = json_0["order_id"].stringValue
                        self.orderStatusData["orderStatus"] = json_0["success"].stringValue
                        self.total.grandtotal = json_0["grandtotal"].stringValue
                        self.orderStatusData["order_date"] = json_0["order_date"].stringValue

                        var arrTempBlocks : [ItemBlock] = []
                        for bl in self.arrProduct {
                            arrTempBlocks.append(ItemBlock(productId: bl.product_id, productName: bl.product_name, productPrice: bl.sub_total, productQuantity: "\(bl.quantity ?? 0)"))
                        }
                        let finalRes = self.getItemBlock(itemBlocks: arrTempBlocks)
                        
                        if self.paymentMethod.lowercased() == "Braintree".lowercased() {
                            
                            self.generateBraintreeTokenPOST()
                            
                        } else if (self.paymentMethod.lowercased() == "payfort") {
                            
                            self.getPayfortTokenFromServer()
                            
                        } else if(self.paymentMethod.lowercased() == "checkoutcom_card_payment") {
                            
                            self.getCheckoutPaymentTokenFromServer()
                            
                        } else if(self.paymentMethod.lowercased() == "applepayfort" || self.paymentMethod.lowercased() == "applepaycheckout") {
                            
                            self.responsePassApplePay = json_0.rawValue
                            
                            self.getCheckoutPaymentTokenFromServer()
                            
                            
                        }
                        else if (self.paymentMethod.lowercased() == "stcpayment"){
                            self.stcSetSTCVarify()
                        }
                        
                        else if (self.paymentMethod.lowercased() == "tamara_pay_later"){
                           // Call payment for details
                            self.getTamaraPaymentTokenFromServer()
                        }
                        else if (self.paymentMethod.lowercased() == "tamara_pay_by_instalments"){
                           // Call payment for details
                            self.getTamaraPaymentTokenFromServer()
                            //tamara_pay_by_instalments
                        }
                            
                        else{
                            
                            let detail : [AnalyticKey:String] = [
                                .Value : self.total.grandtotal ?? "",
                                .Currency : APP_DEL.selectedCountry.getCurrencySymbol().uppercased(),
                                .Coupon : self.total.coupon ?? "",
                                .Tax : self.lblTaxesValue.text ?? "",
                                .ItemID : finalRes.jsonString,
                                .TransactionID : self.orderStatusData["orderId"] ?? "",
                                .PriceWithoutCurrency : self.total.grandtotal_without_currency ?? ""
                            ]
                                
                            API().setEvent(eventName: .Purchase, eventDetail: detail) { (json, err) in }
                            
                            Analytics.logEvent(AnalyticsEventPurchase, parameters: [
                                AnalyticsParameterValue : self.total,
                                AnalyticsParameterCurrency : APP_DEL.selectedCountry.getCurrencySymbol().uppercased(),
                                AnalyticsParameterCoupon : self.total.coupon ?? "",
                                AnalyticsParameterTax : "",
                                AnalyticsParameterItemID : finalRes.jsonString,
                                AnalyticsParameterTransactionID : self.orderStatusData["orderId"] as Any,

                            ])
                            
                            UserDefaults.standard.removeObject(forKey: "guestEmail");
                            UserDefaults.standard.removeObject(forKey: "cartId");
                            UserDefaults.standard.removeObject(forKey: "appliedCoupon");
                            UserDefaults.standard.setValue("0", forKey: "items_count")
                            
                            APP_DEL.isComesFromOrder = true
                            
                            
                            var arrOrderItem = [orderItems]()
                                                
                            arrOrderItem.removeAll()
                            
                            for i in 0..<self.arrProduct.count{
                                                       
                            
                            let dictdata = self.arrProduct[i]
                                                       
                            arrOrderItem.append(orderItems(product_type: dictdata.product_type,
                                                           product_name: dictdata.product_name,
                                                           product_price: dictdata.sub_total,
                                                           product_id: dictdata.product_id,
                                                           rowsubtotal: dictdata.sub_total,
                                                           product_qty: "\(dictdata.quantity ?? 0)",
                                                           product_image: dictdata.product_image,
                                                           selected: false,
                                                           itemId: dictdata.item_id,
                                                           optionSize: (dictdata.options_selected?.first?.value ?? "")))
                                                       
                            }
                            
                            
                            let storyboard = UIStoryboard(name: "Main", bundle: nil);
                            let viewController = storyboard.instantiateViewController(withIdentifier: "cedMagePaymentSuccess") as! cedMagePaymentSuccess;
                            viewController.order_id = self.orderStatusData["orderId"]!
                            
                            viewController.orderProduct = arrOrderItem
                            viewController.order_date = self.orderStatusData["order_date"]!
                            self.navigationController?.viewControllers = [viewController];
                        }
                        
                    } else {
                        cedMageHttpException.showAlertView(me: self, msg: json["message"].stringValue, title: APP_LBL().error)
                    }
                }
          //  }
        }
    }
    
    
    
    //MARK:- SAVE BILLING SHIPPING
    func saveBillingShipping() {
        
        var postData = [String:String]()
        
        if let cartID =  UserDefaults.standard.value(forKey: "cartId") as? String {
            postData["cart_id"] = cartID
        }
        
        if let userInfoDict = UserDefaults.standard.object(forKey: "userInfoDict") as? [String: String] {
            postData["hashkey"] = userInfoDict["hashKey"]
            postData["customer_id"] = userInfoDict["customerId"]
        }
        
        postData["address_id"] = addressOBJ["entity_id"] ?? "";
        
        let defaults = UserDefaults.standard
        
        if let store_id = defaults.object(forKey: "storeId") as? String {
            postData["store_id"] = store_id;
        }
        
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);
        
        API().callAPI(endPoint: "mobiconnect/checkout/savebillingshipping", method: .POST, param: postData) { (json, err) in
            
         //   DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                if err == nil {

                    //self.selectPaymentMethod()
                }
          //  }
        }
    }
    
    //MARK:- GET CART LIST
    func getCartList() {
        
        var postData = [String:String]()
                
        if let userInfoDict = UserDefaults.standard.object(forKey: "userInfoDict") as? [String: String] {
            postData["hashkey"] = userInfoDict["hashKey"]
            postData["customer_id"] = userInfoDict["customerId"]
        }
        
        if let storeID =  UserDefaults.standard.value(forKey: "storeId") as? String {
            postData["store_id"] = storeID
        }
        
        if let cartID =  UserDefaults.standard.value(forKey: "cartId") as? String {
            postData["cart_id"] = cartID
        }
        postData["country_id"] = APP_DEL.selectedCountry.country_code ?? "SA"
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);
        
        API().callAPI(endPoint: "mobiconnect/checkout/viewcart", method: .POST, param: postData) { (json, err) in
            
           // DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                let json_0 = json[0]
                
                if err == nil {

                   
//                    if let success =  json[0][0]["success"].stringValue as? String {
//                        if success == "invalid_country"{
//                            // redirect to splash screen
//                            UserDefaults.standard.removeObject(forKey: Constants().USERDEFAULT_KEY_SELECTED_COUNTRY);
//
//                            let test = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SplashVC") as! SplashVC
//                            APP_DEL.window?.rootViewController = UINavigationController (rootViewController: test)
//                            return
//                        }
//                    }
                    
                    self.viewCartJSON = json_0
                    self.updateCartList()
                }
           // }
        }
    }
    
    //MARK:- ADDITIONAL INFO
    func additionalINFO(additional_info: String, failure: String, order_id: String = "") {

        var postData = [String:String]()
        postData["order_id"] = order_id
        postData["additional_info"] = self.paymentMethod.lowercased()
        postData["failure"] = failure
        
        if let storeID =  UserDefaults.standard.value(forKey: "storeId") as? String {
            postData["store_id"] = storeID
        }
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);
        
        API().callAPI(endPoint: "mobiconnect/checkout/additionalinfo", method: .POST, param: postData) { (json, err) in
            
         //   DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                let json_0 = json[0]

                if err == nil {
                    
                    if json_0["success"].stringValue != "false" {
                        
                        UserDefaults.standard.removeObject(forKey: "guestEmail");
                        UserDefaults.standard.removeObject(forKey: "cartId");
                        UserDefaults.standard.setValue("0", forKey: "items_count")
                        
                        var arrTempBlocks : [ItemBlock] = []
                        for bl in self.arrProduct {
                            arrTempBlocks.append(ItemBlock(productId: bl.product_id, productName: bl.product_name, productPrice: bl.sub_total, productQuantity: "\(bl.quantity ?? 0)"))
                        }
                        let finalRes = self.getItemBlock(itemBlocks: arrTempBlocks)

                        let strCoupon : String = self.total.coupon ?? ""
                        
                        let detail : [AnalyticKey:String] = [
                            .Value : self.total.grandtotal ?? "",
                            .Currency : APP_DEL.selectedCountry.getCurrencySymbol().uppercased(),
                            .Coupon : self.total.coupon ?? "",
                            .Tax : self.lblTaxesValue.text ?? "",
                            .ItemID : finalRes.jsonString,
                            .TransactionID : self.orderStatusData["orderId"] ?? "",
                            .PriceWithoutCurrency : self.total.grandtotal_without_currency ?? ""
                        ]
                        
                        API().setEvent(eventName: .Purchase, eventDetail: detail) { (json, err) in }
                        
                        Analytics.logEvent(AnalyticsEventPurchase, parameters: [
                            AnalyticsParameterValue : self.total.grandtotal ?? "",
                            AnalyticsParameterCurrency : APP_DEL.selectedCountry.getCurrencySymbol().uppercased(),
                            AnalyticsParameterCoupon : strCoupon,
                            AnalyticsParameterTax : self.lblTaxesValue.text ?? "",
                            AnalyticsParameterItemID : finalRes.jsonString,
                            AnalyticsParameterTransactionID : self.orderStatusData["orderId"] as Any
                        ])
                        
                        APP_DEL.arrGftCardData = []
                        var arrOrderItem = [orderItems]()
                                                    
                        
                        APP_DEL.isAccountPageRefreshAfterOrder = true
                        
                        arrOrderItem.removeAll()
                                                             
                        for i in 0..<self.arrProduct.count {
                            
                            let dictdata = self.arrProduct[i]
                            
                            
                            arrOrderItem.append(orderItems(product_type: dictdata.product_type,
                                                           product_name: dictdata.product_name,
                                                           product_price: dictdata.sub_total,
                                                           product_id: dictdata.product_id,
                                                           rowsubtotal: dictdata.sub_total,
                                                           product_qty: "\(dictdata.quantity)",
                                                           product_image: dictdata.product_image,
                                                           selected: false,
                                                           itemId: dictdata.item_id,
                                                           optionSize: (dictdata.options_selected?.first?.value ?? "")))
                            
                        }
                            

                        
                        
                        self.view.makeToast(APP_LBL().payment_successful, duration: 2, position: .center, title: nil, image: nil, style: nil, completion: {
                            Void in
                            
                            let storyboard = UIStoryboard(name: "Main", bundle: nil);
                            let viewController = storyboard.instantiateViewController(withIdentifier: "cedMagePaymentSuccess") as! cedMagePaymentSuccess;
                            viewController.order_id = self.orderStatusData["orderId"]!
                            viewController.order_date = self.orderStatusData["order_date"]!
                            viewController.orderProduct = arrOrderItem
                            
                            self.navigationController?.viewControllers = [viewController];

                         
                        })
                        
                    } else if json_0["success"].stringValue == "false" {

                        
                        self.view.makeToast(APP_LBL().order_payment_failure, duration: 2, position: .center, title: nil, image: nil, style: nil, completion: {
                            Void in
                            //self.removeCoupon()

                        })
                    }
                }
          //  }
        }
    }
    
    //MARK:- APPLY STORE CREDIT
    func applyStoreCredit() {
               
        if isExistGiftCard {
            
            self.view.isUserInteractionEnabled = false;
            self.view.makeToast(APP_LBL().you_can_not_apply_store_credit_for_giftcard, duration: 2, position: .center, title: nil, image: nil, style: nil, completion: {
                Void in
                
                self.view.isUserInteractionEnabled = true;
            })
            
            return;
        }
        
        if self.store_creditwithoutcurrency == "0" {
            
            self.view.isUserInteractionEnabled = false;
            self.view.makeToast(APP_LBL().store_credit_not_available, duration: 2, position: .center, title: nil, image: nil, style: nil, completion: {
                Void in
                
                self.view.isUserInteractionEnabled = true;
            })
            
            return;
        }
        
        var postData = [String:String]()
        
        if let storeID =  UserDefaults.standard.value(forKey: "storeId") as? String {
            postData["store_id"] = storeID
        }

        if let userInfoDict = UserDefaults.standard.object(forKey: "userInfoDict") as? [String: String] {
            
            postData["hashkey"] = userInfoDict["hashKey"]
            postData["customer_id"] = userInfoDict["customerId"]
            
        } else {
            
            self.getCartList()
            return;
        }

        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);
        
        API().callAPI(endPoint: "mobiconnect/setStoreCredit", method: .POST, param: postData) { (json, err) in
            
           // DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                if err == nil {

                    self.appliedStoreCreditString = json["applied_amout"].stringValue
                    self.updateStoreCredit(isForUpdate: true, value: true)
                    
                    self.getCartList()
                }
          //  }
        }
    }
    
    //MARK:- REMOVE STORE CREDIT
    func removeStoreCredit() {
        
        var postData = [String:String]()

        if let userInfoDict = UserDefaults.standard.object(forKey: "userInfoDict") as? [String: String] {
            
            postData["hashkey"] = userInfoDict["hashKey"]
            postData["customer_id"] = userInfoDict["customerId"]
            
        } else {
            
            self.getCartList()
            return;
        }
        
        if let storeID =  UserDefaults.standard.value(forKey: "storeId") as? String {
            postData["store_id"] = storeID
        }
        
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);
        
        API().callAPI(endPoint: "mobiconnect/removeStoreCredit", method: .POST, param: postData) { (json, err) in
            
          //  DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                if err == nil {
                    
                    let json_0 = json[0]

                    if json_0["success"].stringValue == "true" {
                        self.updateStoreCredit(isForUpdate: true, value: false)
                    }
                    
                    self.getCartList()
                }
          //  }
        }
    }
    
    //MARK:- APPLY PROMO CODE
    func applyPromoCode() {
        
        if (self.txtPromoCode.text == "") {
            
            self.view.makeToast(APP_LBL().please_enter_coupon, duration: 2.0, position: .center, title: nil, image: nil, style: nil, completion: nil);
            return;
        }
        
        var postData = [String:String]()

        if let userInfoDict = UserDefaults.standard.object(forKey: "userInfoDict") as? [String: String] {
            
            postData["hashkey"] = userInfoDict["hashKey"]
            postData["customer_id"] = userInfoDict["customerId"]
            
        } else {
            
            self.getCartList()
            return;
        }
        
        if let storeID =  UserDefaults.standard.value(forKey: "storeId") as? String {
            postData["store_id"] = storeID
        }
        
        if let cartID =  UserDefaults.standard.value(forKey: "cartId") as? String {
            postData["cart_id"] = cartID
        }
        
        postData["coupon_code"] = self.txtPromoCode.text!;
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);
        
        API().callAPI(endPoint: "mobiconnect/checkout/coupon", method: .POST, param: postData) { (json, err) in
            
           // DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                if err == nil {
                    
                    let json_0 = json[0]

                    if json_0["cart_id"]["success"].stringValue == "true" {
                        
                        self.view.makeToast("\(APP_LBL().you_can_use_the_discount_coupon_code) \(self.txtPromoCode.text!).", duration: 1.0, position: .center)
                        
                    } else {
                        
                        if APP_DEL.selectedLanguage == Arabic{
                            cedMageHttpException.showAlertView(me: self, msg: json["cart_id"]["message"].stringValue, title: APP_LBL().error)
                        }
                        else{
                            cedMageHttpException.showAlertView(me: self, msg: "\(APP_LBL().the_discount_coupan_code) \(self.txtPromoCode.text!) \(APP_LBL().is_invalid)", title: APP_LBL().error)
                        }
                    }
                    
                    self.getCartList()
                }
          //  }
        }
    }
    
    //MARK:- REMOVE PROMO CODE
    func removePromoCode() {
        
        if (self.txtPromoCode.text == "") {
            
            self.view.makeToast(APP_LBL().please_enter_coupon, duration: 2.0, position: .center, title: nil, image: nil, style: nil, completion: nil);
            return;
        }
        
        var postData = [String:String]()

        if let userInfoDict = UserDefaults.standard.object(forKey: "userInfoDict") as? [String: String] {
            
            postData["hashkey"] = userInfoDict["hashKey"]
            postData["customer_id"] = userInfoDict["customerId"]
            
        } else {
            
            self.getCartList()
            return;
        }
        
        if let storeID =  UserDefaults.standard.value(forKey: "storeId") as? String {
            postData["store_id"] = storeID
        }
        
        if let cartID =  UserDefaults.standard.value(forKey: "cartId") as? String {
            postData["cart_id"] = cartID
        }
        
        postData["remove"] = "1";
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);
        
        API().callAPI(endPoint: "mobiconnect/checkout/coupon", method: .POST, param: postData) { (json, err) in
            
          //  DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                if err == nil {
                    
                    let json_0 = json[0]
                    
                    if (json_0["cart_id"]["success"].stringValue == "true") {
                        
                        if APP_DEL.selectedLanguage == Arabic{
                            self.view.makeToast(json["cart_id"]["message"].stringValue, duration: 1.0, position: .center)
                        }
                        else{
                            self.view.makeToast(APP_LBL().you_canceled_the_discount_coupon_code, duration: 1.0, position: .center)
                        }
                    }
                    
                    self.getCartList()
                }
          //  }
        }
    }
    
}

extension NejreeCheckoutVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        //MARK:- For Enable STORE CREDIT
        //return self.arrFinalPAY.count + 1
        
        return self.arrFinalPAY.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "NejreeCheckoutCell") as! NejreeCheckoutCell
        cell.selectionStyle = .none
        
        cell.lblDesc.textColor = UIColor.lightGray//.lightGray
        
        if indexPath.row == arrFinalPAY.count {
            
            if self.store_creditwithoutcurrency == "0" {
                cell.lblPaymentMethod.textColor = .lightGray
            }
            
            cell.lblPaymentMethod.text = APP_LBL().store_credit + " ( \(self.availableStoreCreditString) ) "
            cell.imgPaymentMethod.image = UIImage(named: "Store credit icon")
            
            if self.isStoreCreditApplied {
                
                cell.imgSelect.image = UIImage(named: "checkout_check");
                cell.imgSelect.isHidden = false;
                
            } else {
                
                cell.imgSelect.isHidden = true
                cell.imgSelect.image = nil;
            }
            
        } else {
            
            cell.lblPaymentMethod.text = self.arrFinalPAY[indexPath.row].title!
            cell.lblPaymentMethod.textColor = .black
            
            if self.isStoreCreditApplied && self.isFreeOrderUsingStoreCredit {
                
                cell.imgSelect.image = nil;
                cell.imgSelect.isHidden = true;
                
            } else if self.isStoreCreditApplied && !self.isFreeOrderUsingStoreCredit {
                
                if self.arrFinalPAY[indexPath.row].value! == self.paymentMethodStoreCreditPartner {
                    
                    if (self.arrFinalPAY[indexPath.row].value! == "cashondelivery") && ((isExistGiftCard) || (grandTotalInt >= codTotalToDiableOption)) {

                        cell.imgSelect.image = UIImage(named: "information_button")
                        cell.imgSelect.isHidden = false;
                        
                         self.paymentMethodName = cell.lblPaymentMethod.text!
                        
                    } else {
                        
                        cell.imgSelect.image = UIImage(named: "checkout_check");
                        cell.imgSelect.isHidden = false;
                        
                         self.paymentMethodName = cell.lblPaymentMethod.text!
                    }
                    
                } else {
                    cell.imgSelect.image = nil;
                    cell.imgSelect.isHidden = true;
                }
                
            } else  if self.arrFinalPAY[indexPath.row].value! == self.paymentMethod {
                
                if (self.arrFinalPAY[indexPath.row].value! == "cashondelivery") && ((isExistGiftCard) || (grandTotalInt >= codTotalToDiableOption)) {

                    cell.imgSelect.image = UIImage(named: "information_button")
                    cell.imgSelect.isHidden = false;
                    
                } else {
                    
                    cell.imgSelect.image = UIImage(named: "checkout_check");
                    cell.imgSelect.isHidden = false;
                    
                    self.paymentMethodName = cell.lblPaymentMethod.text!
                }
                
            } else {
                
                if (self.arrFinalPAY[indexPath.row].value! == "cashondelivery") && ((isExistGiftCard) || (grandTotalInt >= codTotalToDiableOption)) {

                    cell.imgSelect.image = UIImage(named: "information_button")
                    cell.imgSelect.isHidden = false;
                    
                } else {
                    
                    cell.imgSelect.image = UIImage(named: "PaymentMethodNotSelected")
                    cell.imgSelect.isHidden = false;
                }
            }
            
            if self.arrFinalPAY[indexPath.row].value! == "applepayfort" {
                
                cell.imgPaymentMethod.image = UIImage(named: "newapple-pay")
                cell.lblPaymentMethod.text = APP_LBL().pay_with_apple_pay
                cell.lblDesc.text = self.arrFinalPAY[indexPath.row].descr ?? ""
                
            } else if self.arrFinalPAY[indexPath.row].value! == "payfort" {
                
                cell.imgPaymentMethod.image = UIImage(named: "mada")
                cell.lblPaymentMethod.text = APP_LBL().credit_card_bank
                cell.lblDesc.text = self.arrFinalPAY[indexPath.row].descr ?? ""
                
            } else if self.arrFinalPAY[indexPath.row].value! == "cashondelivery" {

                cell.imgPaymentMethod.image = UIImage(named: "cod")
                cell.lblPaymentMethod.text = APP_LBL().cash_on_delivery
                cell.lblDesc.text = self.arrFinalPAY[indexPath.row].descr ?? ""
                
                
                if isExistGiftCard {
                
                    cell.lblPaymentMethod.textColor = UIColor.lightGray
                    cell.lblDesc.textColor = UIColor.lightGray
                    cell.imgPaymentMethod.alpha = 0.3
                    
                    cell.imgSelect.image = UIImage(named: "information_button")
                    cell.lblDesc.text = ""//APP_LBL().gift_card_is_added_on_cart
                                                
                } else if grandTotalInt >= codTotalToDiableOption {

                    cell.lblPaymentMethod.textColor = UIColor.lightGray
                    cell.lblDesc.textColor = UIColor.lightGray
                    cell.imgPaymentMethod.alpha = 0.3

                    cell.imgSelect.image = UIImage(named: "information_button")
                    cell.lblDesc.text = ""//APP_LBL().cod_disable_due_to_over_limit
                }
                
            } else if self.arrFinalPAY[indexPath.row].value! == "stcpayment" {
                
                cell.imgPaymentMethod.image = UIImage(named: "mada")
                cell.lblPaymentMethod.text = self.arrFinalPAY[indexPath.row].title!
                cell.lblDesc.text = self.arrFinalPAY[indexPath.row].descr ?? ""
                
            } else if self.arrFinalPAY[indexPath.row].value! == "onlinepayment" {
                
                cell.imgPaymentMethod.image = UIImage(named: "newapple-pay")
                cell.lblPaymentMethod.text = self.arrFinalPAY[indexPath.row].title!
                cell.lblDesc.text = self.arrFinalPAY[indexPath.row].descr ?? ""
                
            } else if self.arrFinalPAY[indexPath.row].value! == "applepaycheckout" {
                
                cell.imgPaymentMethod.image = UIImage(named: "newapple-pay")
                cell.lblPaymentMethod.text = self.arrFinalPAY[indexPath.row].title!
                cell.lblDesc.text = self.arrFinalPAY[indexPath.row].descr ?? ""
                
            } else if self.arrFinalPAY[indexPath.row].value! == "checkoutcom_card_payment" {
                
                cell.imgPaymentMethod.image = UIImage(named: "mada")
                cell.lblPaymentMethod.text = APP_LBL().credit_card_bank
                cell.lblDesc.text = self.arrFinalPAY[indexPath.row].descr ?? ""
                
            } else if self.arrFinalPAY[indexPath.row].value! == "tamara_pay_later" {
                cell.imgPaymentMethod.image = UIImage(named: "tamara_pay_by_instalments")
                cell.lblPaymentMethod.text = APP_LBL().tamara_pay
                cell.lblDesc.text = self.arrFinalPAY[indexPath.row].descr ?? ""
                //tamara_pay_by_instalments
                if ((Double(self.arrFinalPAY[indexPath.row].tamara_min ?? "")! > Double(total.grandtotal_without_currency ?? "")!) || (Double(total.grandtotal_without_currency ?? "")! > Double(self.arrFinalPAY[indexPath.row].tamara_max ?? "")!) || (isExistGiftCard)) {
                    cell.lblPaymentMethod.textColor = UIColor.lightGray
                    cell.lblDesc.textColor = UIColor.lightGray
                    cell.imgPaymentMethod.alpha = 0.3
                    cell.imgPaymentMethod.image = UIImage(named: "tamara_logo_unselected")
                    cell.imgSelect.image = UIImage(named: "information_button")
//                    cell.lblDesc.text = ""//APP_LBL().gift_card_is_added_on_cart
                    cell.imgSelect.isHidden = false;
                }
            } else if self.arrFinalPAY[indexPath.row].value! == "tamara_pay_by_instalments" {
                cell.imgPaymentMethod.image = UIImage(named: "tamara_pay_by_instalments")
                cell.lblPaymentMethod.text = APP_LBL().tamara_pay_instalments
                cell.lblDesc.text = self.arrFinalPAY[indexPath.row].descr ?? ""
                //tamara_pay_by_instalments
                if ((Double(self.arrFinalPAY[indexPath.row].tamara_min ?? "")! > Double(total.grandtotal_without_currency ?? "")!) || (Double(total.grandtotal_without_currency ?? "")! > Double(self.arrFinalPAY[indexPath.row].tamara_max ?? "")!) || (isExistGiftCard)) {
                    cell.lblPaymentMethod.textColor = UIColor.lightGray
                    cell.lblDesc.textColor = UIColor.lightGray
                    cell.imgPaymentMethod.alpha = 0.3
                    cell.imgPaymentMethod.image = UIImage(named: "tamara_logo_unselected")
                    cell.imgSelect.isHidden = false;

                    cell.imgSelect.image = UIImage(named: "information_button")
//                    cell.lblDesc.text = ""//APP_LBL().gift_card_is_added_on_cart
                }
            } else {
                
                cell.imgPaymentMethod.image = UIImage(named: "mada")
                cell.lblPaymentMethod.text = self.arrFinalPAY[indexPath.row].title ?? ""
                cell.lblDesc.text = self.arrFinalPAY[indexPath.row].descr ?? ""
            }
            
            
            
            
            
            

            
        }
        
        if APP_DEL.selectedLanguage == Arabic {
            cell.lblPaymentMethod.textAlignment = .right
            cell.lblDesc.textAlignment = .right
        } else {
            cell.lblPaymentMethod.textAlignment = .left
            cell.lblDesc.textAlignment = .left
        }
        
        return cell;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == self.arrFinalPAY.count {
                        
            if self.isStoreCreditApplied {
                
                self.removeStoreCredit()
                
            } else {
                
                self.applyStoreCredit()
            }
            
        } else {
                            
            if self.isFreeOrderUsingStoreCredit {
                
            } else {
                
                if isExistGiftCard && self.arrFinalPAY[indexPath.row].value! == "cashondelivery" {
                    
                    //display popup here
                    let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CheckoutInfoPopupVC") as! CheckoutInfoPopupVC
                    vc.modalPresentationStyle = .overFullScreen
                    vc.modalTransitionStyle = .crossDissolve
                    vc.strMessage = APP_LBL().gift_card_is_added_on_cart
                    self.present(vc, animated: true, completion: nil)
                    
                    return;
                }
                
                if grandTotalInt >= codTotalToDiableOption && self.arrFinalPAY[indexPath.row].value! == "cashondelivery" {
                    
                    //display popup here
                    let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CheckoutInfoPopupVC") as! CheckoutInfoPopupVC
                    vc.modalPresentationStyle = .overFullScreen
                    vc.modalTransitionStyle = .crossDissolve
                    vc.strMessage = APP_LBL().cod_disable_due_to_over_limit
                    self.present(vc, animated: true, completion: nil)
                    
                    return;
                }
                
                if (self.arrFinalPAY[indexPath.row].value! == "tamara_pay_later"){
                    if isExistGiftCard && self.arrFinalPAY[indexPath.row].value! == "tamara_pay_later" {
                        
                        //display popup here
                        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CheckoutInfoPopupVC") as! CheckoutInfoPopupVC
                        vc.modalPresentationStyle = .overFullScreen
                        vc.modalTransitionStyle = .crossDissolve
                        vc.strMessage = APP_LBL().gift_card_is_added_on_cart_tamara
                        self.present(vc, animated: true, completion: nil)
                        
                        return;
                    }
                    if (((Double(self.arrFinalPAY[indexPath.row].tamara_min ?? "")! > Double(total.grandtotal_without_currency ?? "")!) || (Double(total.grandtotal_without_currency ?? "")! > Double(self.arrFinalPAY[indexPath.row].tamara_max ?? "")!))) {
                        //display popup here
                        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CheckoutInfoPopupVC") as! CheckoutInfoPopupVC
                        vc.modalPresentationStyle = .overFullScreen
                        vc.modalTransitionStyle = .crossDissolve
                        
                        var strMessageAlertText = APP_LBL().grand_total_limit_of_tamara_is_min_x_max_y
                        strMessageAlertText = strMessageAlertText.replacingOccurrences(of: "Z*", with: APP_LBL().tamara_pay)
                        strMessageAlertText = strMessageAlertText.replacingOccurrences(of: "X*", with: "SAR \(self.arrFinalPAY[indexPath.row].tamara_min ?? "")")
                        strMessageAlertText = strMessageAlertText.replacingOccurrences(of: "Y*", with: "SAR \(self.arrFinalPAY[indexPath.row].tamara_max ?? "")")
                        vc.strMessage = strMessageAlertText
                        
                        self.present(vc, animated: true, completion: nil)
                        
                        return;
                    }
                }
                
                if (self.arrFinalPAY[indexPath.row].value! == "tamara_pay_by_instalments") {
                    if isExistGiftCard && self.arrFinalPAY[indexPath.row].value! == "tamara_pay_by_instalments" {
                        
                        //display popup here
                        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CheckoutInfoPopupVC") as! CheckoutInfoPopupVC
                        vc.modalPresentationStyle = .overFullScreen
                        vc.modalTransitionStyle = .crossDissolve
                        vc.strMessage = APP_LBL().gift_card_is_added_on_cart_tamara
                        self.present(vc, animated: true, completion: nil)
                        
                        return;
                    }
                    if ((((Double(self.arrFinalPAY[indexPath.row].tamara_min ?? "")! > Double(total.grandtotal_without_currency ?? "")!) || (Double(total.grandtotal_without_currency ?? "")! > Double(self.arrFinalPAY[indexPath.row].tamara_max ?? "")!)))) {
                        //display popup here
                        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CheckoutInfoPopupVC") as! CheckoutInfoPopupVC
                        vc.modalPresentationStyle = .overFullScreen
                        vc.modalTransitionStyle = .crossDissolve
                        
                        var strMessageAlertText = APP_LBL().grand_total_limit_of_tamara_is_min_x_max_y
                        strMessageAlertText = strMessageAlertText.replacingOccurrences(of: "Z*", with: APP_LBL().tamara_pay_instalments)
                        strMessageAlertText = strMessageAlertText.replacingOccurrences(of: "X*", with: "SAR \(self.arrFinalPAY[indexPath.row].tamara_min ?? "")")
                        strMessageAlertText = strMessageAlertText.replacingOccurrences(of: "Y*", with: "SAR \(self.arrFinalPAY[indexPath.row].tamara_max ?? "")")
                        vc.strMessage = strMessageAlertText
                        
                        self.present(vc, animated: true, completion: nil)
                        
                        return;
                    }
                }
                
                self.paymentMethodStoreCreditPartner = self.arrFinalPAY[indexPath.row].value ?? ""
                self.paymentMethod = self.arrFinalPAY[indexPath.row].value ?? ""
                
                if (self.arrFinalPAY[indexPath.row].value! == "cashondelivery") {
                    
                    self.lblCodChargeValue.text = APP_DEL.selectedCountry.getCurrencySymbol().uppercased() + " " + APP_DEL.strCodCharge
                    UserDefaults.standard.setValue(true, forKey: APP_DEL.key_isCOD)
                    UserDefaults.standard.setValue(false, forKey: APP_DEL.key_isCard)
                    
                } else {
                    
                    self.lblCodChargeValue.text = APP_DEL.selectedCountry.getCurrencySymbol().uppercased() + " " + "0"
                    UserDefaults.standard.setValue(false, forKey: APP_DEL.key_isCOD)
                    
                    if (self.arrFinalPAY[indexPath.row].value! == "checkoutcom_card_payment"){
                        
                        UserDefaults.standard.setValue(true, forKey: APP_DEL.key_isCard)
                    }
                    else
                    {
                        UserDefaults.standard.setValue(false, forKey: APP_DEL.key_isCard)
                    }
                }
                
                self.saveShippingPayament(isForSelction: true)
                
            }
        }
        
        if isExistGiftCard && self.arrFinalPAY[indexPath.row].value! == "cashondelivery" {
            return;
        }
        
        self.tblView.reloadData()
        
        self.manageSubmit()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension//50;
    }
}

extension NejreeCheckoutVC: iCarouselDelegate, iCarouselDataSource {
    
    func numberOfItems(in carousel: iCarousel) -> Int {
        
        return self.arrProduct.count
    }
    
    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView {
        
        let w = (self.carousel.frame.size.width - 80.0)
        let h = self.carousel.frame.size.height
        let carView = UIView(frame: CGRect(x: 0, y: 0, width: w, height: h))
        
        let vi = Bundle.main.loadNibNamed("NejreeCheckoutProduct", owner: self, options: nil)?.first as! NejreeCheckoutProduct
        vi.frame = carView.bounds
      
        vi.productName.text = arrProduct[index].product_name
        vi.quantityLabel.text = APP_LBL().quantity_semicolon + " " + "\(arrProduct[index].quantity ?? 0)"
        vi.priceLabel.text = arrProduct[index].sub_total ?? ""
        vi.productImage.sd_setImage(with: URL(string: arrProduct[index].product_image!), placeholderImage:nil)
        
        let filt = APP_DEL.arrGftCardData.first(where: { (item) -> Bool in ((item.child_product_id ?? "") == (arrProduct[index].child_product_id ?? ""))})
        
        if filt != nil {
            
            var one = ""

            if APP_DEL.selectedLanguage == Arabic {

                one = (filt?.description ?? "")
                one = " " + (filt?.phone ?? "") + " | " + one
                one = (filt?.name ?? "") + " :" + APP_LBL().to + one

            } else {
                                                
                one = APP_LBL().to + ": " + (filt?.name ?? "")
                one = one + " | " + (filt?.phone ?? "") + " "
                one = one + (filt?.description ?? "")
            }
            
            vi.sizeValue.text = one
            if APP_DEL.selectedLanguage == Arabic {
                vi.productImage.sd_setImage(with: URL(string: arrProduct[index].product_image!), placeholderImage:UIImage(named: "gift_card_ar"))
            } else {
                vi.productImage.sd_setImage(with: URL(string: arrProduct[index].product_image!), placeholderImage:UIImage(named: "gift_card_eng"))
            }
            
            let comp = arrProduct[index].sub_total?.components(separatedBy: " ")
            vi.lblGiftPrice.text = String(format: "%.2f", Double(comp?[1] ?? "0.0")!)
            
            vi.lblGiftPrice.isHidden = false
            vi.lblGiftPriceSAR.isHidden = false
            vi.lblGiftPriceSAR.text = APP_DEL.selectedCountry.getCurrencySymbol()
            
        } else {
            
            vi.sizeValue.text = APP_LBL().size.uppercased() + ": " + (arrProduct[index].options_selected?.first?.value ?? "")
            vi.lblGiftPrice.isHidden = true
            vi.lblGiftPriceSAR.isHidden = true
            vi.lblGiftPriceSAR.text = ""
        }
        
        if APP_DEL.selectedLanguage == Arabic {
            vi.productName.textAlignment = .right
            vi.quantityLabel.textAlignment = .right
            vi.sizeValue.textAlignment = .right
        } else {
            vi.productName.textAlignment = .left
            vi.quantityLabel.textAlignment = .left
            vi.sizeValue.textAlignment = .left
        }
        
        carView.layer.borderColor = UIColor.lightGray.cgColor
        carView.layer.borderWidth = 0.3
        carView.round(redius: 10)
        
        vi.viewImageBorder.layer.borderColor = UIColor.lightGray.cgColor
        vi.viewImageBorder.layer.borderWidth = 0.3
        vi.viewImageBorder.round(redius: 10)
        
        vi.layoutSubviews()
        vi.layoutIfNeeded()
        carView.addSubview(vi)
        
        return carView
    }
    
    func carousel(_ carousel: iCarousel, didSelectItemAt index: Int) {
        
        let filt = APP_DEL.arrGftCardData.first(where: { (item) -> Bool in ((item.child_product_id ?? "") == (arrProduct[index].child_product_id ?? ""))})
        
        if filt != nil {
            return;
        }
        
        
        
        let vc = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "productViewController") as! productViewController
        vc.product_id = arrProduct[index].product_id!
        APP_DEL.productIDglobal = arrProduct[index].product_id!
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
        
        
    }
    
    func carousel(_ carousel: iCarousel, valueFor option: iCarouselOption, withDefault value: CGFloat) -> CGFloat {
        
        switch (option) {
            
            case .spacing:
                return (1.0 + (20.0 / carousel.itemWidth))
            
            default:
                return value
        }
    }
}

extension NejreeCheckoutVC {
    
    func getTotalForApplePay(floatValue: Double) -> Double {

        let formatted = String(format: "%.2f", Double(round(100*floatValue)/100))
        return Double(formatted)!
    }
    
    func getItemBlock(itemBlocks: [ItemBlock]) -> (json: [[String:String]], jsonString: String) {
        
        do {
            let data = try JSONEncoder().encode(itemBlocks)
            let json : [[String : String]] = try JSONSerialization.jsonObject(with: data, options: []) as! [[String : String]]
            return (json: json, jsonString: String(data: data, encoding: .utf8)!)
        } catch {
            print("ERROR.")
            return (json: [], jsonString: "")
        }
    }
}

