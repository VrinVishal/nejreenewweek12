//
//  nejreeCategoryController.swift
//  MageNative Magento Platinum
//
//  Created by MacMini on 28/08/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit
import FirebaseAnalytics


let SCREEN_WIDTH = UIScreen.main.bounds.size.width
let SCREEN_HEIGHT = UIScreen.main.bounds.size.height

//let RatioProductDetailImage = 0.60

struct singleCategoryProduct {
    var review: String?
    var regular_price: String?
    var stock_status: String?
    var product_image: String?
    var product_id: String?
    var special_price: String?
    var offer: String?
    var product_name: String?
    var product_url: String?
    var wishlist_item_id: String?
    var description: String?
    var Inwishlist: String?
    var type: String?
    var brands_name: String?
    var imgProductCache: UIImage?
    
}

struct singleCategory {
    var category_image: String?
    var category_name: String?
    var category_id: String?
    var has_child: String?
}


class nejreeCategoryController: MagenativeUIViewController {
    
    
    @IBOutlet weak var collView: UICollectionView!
    
    var task: URLSessionDownloadTask!
    var session: URLSession!
    var cache:NSCache<AnyObject, AnyObject>!
    
    var categoryId = String()
    var isChild = "false"
    var categoryName = String()
    var categoryImage = String()
    var currentPage = 1
    var products = [singleCategoryProduct]()
    var subcategories = [singleCategory]()
    var catNameArr = [String]()
    
    var parentController = categoryMenuBarController()
    var filterNames = [filter]()
    var dataofFilter = [filterData]()
    var selectedFilters = [String:[String]]()
    var applyFilter = false
    var filterString = String()
    var loading = true;
    var noProductCheck = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        session = URLSession.shared
        task = URLSessionDownloadTask()
        self.cache = NSCache()
        
        let layout = self.collView.collectionViewLayout as? UICollectionViewFlowLayout
        layout?.sectionHeadersPinToVisibleBounds = true
        
        self.collView.register(UINib(nibName: "CategoryBannerCollCell", bundle: nil), forCellWithReuseIdentifier: "CategoryBannerCollCell")
        //self.collView.register(UINib(nibName: "SubCategoryBannerCollCell", bundle: nil), forCellWithReuseIdentifier: "SubCategoryBannerCollCell")
        self.collView.register(UINib(nibName: "CategoryProductCollCell", bundle: nil), forCellWithReuseIdentifier: "CategoryProductCollCell")
        
        self.collView.register(UINib(nibName: "FilterButtonCollReusableView", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "FilterButtonCollReusableView")
        
        self.collView.delegate = self
        self.collView.dataSource = self
        
        
        
                
        self.navigationItem.title = APP_LBL().categories
        cedMageLoaders.removeLoadingIndicator(me: parentController)
        collView.isHidden = true
        
        if !applyFilter
        {
            self.sendRequest(url: "mobiconnect/catalog/productwithoutattribute/", params: ["id":categoryId, "page":String(currentPage),"theme":"1", "country_id" : APP_DEL.selectedCountry.country_code ?? "SA"])
            
        }
        
        Analytics.logEvent(AnalyticsEventViewItemList, parameters: [
            AnalyticsParameterItemCategory : categoryId,
        ])
    }
    
    override func didReceiveMemoryWarning() {
        
        if self.products.count > 0 {
                    
            let forCount = (((self.products.count % 2) == 0) ? self.products.count : self.products.count - 1)
            
            for index in 0..<forCount {
                
                if let url = self.products[index].product_image {
                    
                    if (self.cache.object(forKey: (url as AnyObject)) != nil) {
                        
                        self.cache.removeObject(forKey: (url as AnyObject))
                    }
                }
            }
        }
    }
    
    override func recieveResponse(data: Data?, requestUrl: String?, response: URLResponse?) {
        guard let data = data else {return}
        do {
            var json = try JSON(data:data)
            
            json = json[0]
            print(json)
            
            if json.stringValue == "NO_PRODUCTS" {
                cedMageLoaders.removeLoadingIndicator(me: parentController);
                
                if(currentPage == 1)
                {
                    parentController.pageMenu?.view.makeToast(APP_LBL().no_result_found, duration: 1.0, position: .center)
                }
                
                self.noProductCheck = true;
                
            }
            
            if(currentPage == 1)
            {
                for cat in json["data"]["sub_category"].arrayValue {
                    
                    let temp = singleCategory.init(category_image: cat["category_image"].stringValue,
                                                   category_name: cat["category_name"].stringValue,
                                                   category_id: cat["category_id"].stringValue,
                                                   has_child: cat["has_child"].stringValue)
                    
                    self.subcategories.append(temp)
                    
                }
            }
            
            
            for pro in json["data"]["products"].arrayValue {
                
                let temp = singleCategoryProduct.init(review: pro["review"].stringValue,
                                                      regular_price: pro["regular_price"].stringValue,
                                                      stock_status: pro["stock_status"].stringValue,
                                                      product_image: pro["product_image"].stringValue,
                                                      product_id: pro["product_id"].stringValue,
                                                      special_price: pro["special_price"].stringValue,
                                                      offer: pro["offer"].stringValue,
                                                      product_name: pro["product_name"].stringValue,
                                                      product_url: pro["product-url"].stringValue,
                                                      wishlist_item_id: pro["wishlist_item_id"].stringValue,
                                                      description: pro["description"].stringValue,
                                                      Inwishlist: pro["Inwishlist"].stringValue,
                                                      type: pro["type"].stringValue,
                                                      brands_name: pro["brands_name"].stringValue,
                                                      imgProductCache : nil
                )
                
                self.products.append(temp)
                
            }
            
           
            
            
            if(json["data"]["products"].arrayValue.count == 0)
            {
                self.loading = false;
                cedMageLoaders.removeLoadingIndicator(me: parentController)
            }
            
            cedMageLoaders.removeLoadingIndicator(me: parentController)
            
            
            self.collView.isHidden = false
            self.collView.reloadData()
            
            if(currentPage == 1)
           {
               if self.products.count != 0{
                  self.collView.setContentOffset(CGPoint(x:0,y:0), animated: true)
               }
               
           }
            
            
            if(currentPage != 1){

                self.loading = true
            }
            
            
        } catch let error {
            print(error.localizedDescription)
        }
    }
    
     func reloadData() {
        
        self.loading = true;
        self.subcategories.removeAll()
        self.products.removeAll()
        self.currentPage = 1;
        
        if applyFilter {
            
            self.sendRequest(url: "mobiconnect/catalog/productwithoutattribute/", params: ["id":categoryId,"theme":"1", "store_id":UserDefaults.standard.value(forKey: "storeId") as! String, "page":String(currentPage), "multi_filter":filterString, "country_id" : APP_DEL.selectedCountry.country_code ?? "SA"])
            
        } else {
            
            print("Apply false")
            self.sendRequest(url: "mobiconnect/catalog/productwithoutattribute/", params: ["id":categoryId, "page":String(currentPage),"theme":"1", "country_id" : APP_DEL.selectedCountry.country_code ?? "SA"])
        }
    }
}

extension nejreeCategoryController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        switch section {
            
            case 0:
                return 1
            case 1:
                return self.products.count;
            default:
                return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        switch indexPath.section {
            
            case 0:
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryBannerCollCell", for: indexPath) as! CategoryBannerCollCell
                cell.categoryName.text = self.categoryName.uppercased()
                cell.categoryName.textColor = .white
                cell.categoryName.isHidden = true
                cell.Categoryimage.sd_setImage(with: URL(string: categoryImage), placeholderImage: nil)
                return cell
            
            default:
                
                if indexPath.row == self.products.count - 3 {
                    self.checkPagination()
                }
                
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryProductCollCell", for: indexPath) as! CategoryProductCollCell
                
                
//                cell.insideView.productCardView() // ashish comment for remove card view
                
//                if products[indexPath.item].offer != "" {
//                    cell.offerLabel.text = products[indexPath.item].offer! + APP_LBL().percent_off.uppercased()
//                    cell.offerLabel.isHidden = false
//                } else {
//                    cell.offerLabel.isHidden = true
//                }

                if (APP_DEL.isDiscountTagEnable == "0"){
                    cell.offerLabel.isHidden = true
                }
                else
                {
                    if products[indexPath.item].offer != "" {
                           //cell.offerLabel.text = products[indexPath.item].offer! + APP_LBL().percent_off.uppercased()
                        let offer =
                        """
                        \(products[indexPath.item].offer!) %
                        \(APP_LBL().off.uppercased())
                        """
                        
                        cell.offerLabel.text = offer
                           cell.offerLabel.isHidden = false
                       } else {
                           cell.offerLabel.isHidden = true
                       }
                }
                
               cell.offerLabel.round(redius: 18)
                
                let name = products[indexPath.item].product_name?.components(separatedBy: "-")
                cell.productName.text = products[indexPath.item].product_name
                if let names = name {
                    
                    var newName = names[0]
                    if newName.first == " " {
                        newName.removeFirst()
                    }
                    
                    if(names.count>1) {
                        cell.productColor.text = names[1]
                    }
                }
                
                cell.productColor.text = ""
                
                let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: products[indexPath.item].regular_price!)
                attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 1, range: NSMakeRange(0, attributeString.length))
                                
                cell.lblBrandName.text = products[indexPath.item].brands_name
                cell.lblBrandName.textAlignment = .center
                
                if products[indexPath.item].special_price != "no_special" {
                    
                    cell.productPrice.text = products[indexPath.item].special_price;
                    cell.regularPrice.isHidden = false
                    cell.regularPrice.attributedText = attributeString
                    
                } else {

                    cell.productPrice.text = products[indexPath.item].regular_price
                    cell.regularPrice.isHidden = true
                    
                }
                
                if let rating = Float(products[indexPath.item].review!) {
                    
                     cell.ratingView.rating = rating
                }
                
                
                if let downloadURL = SDImageCache.shared.imageFromCache(forKey: products[indexPath.item].product_image!){
                     cell.productImage!.image = downloadURL
                } else {
                    cell.productImage!.sd_setImage(with: URL(string: products[indexPath.item].product_image!), placeholderImage: nil)
                }
                
//                if (self.cache.object(forKey: self.products[indexPath.item].product_image as AnyObject) != nil) {
//
//                    print("Cached image used, no need to download it")
//                    cell.productImage!.image = self.cache.object(forKey: self.products[indexPath.item].product_image as AnyObject) as? UIImage
//
//                } else {
//
//                    let url:URL! = URL(string: self.products[indexPath.item].product_image!)
//                    task = session.downloadTask(with: url, completionHandler: { (location, response, error) -> Void in
//
//                        if let data = try? Data(contentsOf: url) {
//
//                            DispatchQueue.main.async(execute: { () -> Void in
//
//                                if let updateCell = self.collView.cellForItem(at: indexPath) as? CategoryProductCollCell {
//
//                                    if let dtImg = UIImage(data: data) {
//
//                                        IPH.createThumbnail(from: dtImg, fillingSize: dtImg.size, completion: { (newImg) in
//
//                                            let finImage = UIImage(data: newImg.jpegData(compressionQuality: 0.5)!)
//                                            updateCell.productImage!.image = finImage
//                                            self.cache.setObject(finImage!, forKey: response?.url as AnyObject)
//                                        })
//                                    }
//                                }
//                            })
//                        }
//                    })
//                    task.resume()
//                }
                
                
                
                return cell
            }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        switch indexPath.section {
        case 0:
            return CGSize(width: SCREEN_WIDTH, height: SCREEN_WIDTH / 2.0)
        
        default:
        
            return CGSize(width: ((SCREEN_WIDTH - 11.0) / 2.0), height: 365.0)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        if (kind == UICollectionView.elementKindSectionHeader) && (indexPath.section == 1) {
            
            let reusView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "FilterButtonCollReusableView", for: indexPath) as! FilterButtonCollReusableView
            reusView.filterButton.addTarget(self, action: #selector(filterTapped(_:)), for: .touchUpInside)
            reusView.filterButton.roundCorners()
            reusView.filterButton.setBorder()
            reusView.filterButton.setTitle(APP_LBL().filter.uppercased(), for: .normal)
            return reusView;
        } else {
            return UICollectionReusableView(frame: CGRect(x: 0, y: SCREEN_WIDTH, width: 0, height: 0))
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: SCREEN_WIDTH, height: (section == 1) ? 50.0 : 0.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if indexPath.section == 1 {

            let vc = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "productViewController") as! productViewController
            vc.product_id = products[indexPath.row].product_id ?? ""
             APP_DEL.productIDglobal = products[indexPath.row].product_id ?? ""
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
   
    
    @objc func filterTapped(_ sender: UIButton) {
        let vc = UIStoryboard(name: "categorylayouts", bundle: nil).instantiateViewController(withIdentifier: "nejreeFilterController") as! nejreeFilterController
        vc.categoryId = self.categoryId
//        vc.isCategoryFilter = true
        vc.isCategoryFilter = true
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func checkPagination() {
    
        if noProductCheck {
           return
        }
        
        if isChild == "true" {
            return
        }
        
        if (loading) {
            
            let index =  parentController.pageMenu?.currentPageIndex
          
            if index != 0 {
                cedMageLoaders.addDefaultLoader(me: parentController)
            }
            
            currentPage += 1
            self.loading = false
            
            if !applyFilter {
                
                self.sendRequest(url: "mobiconnect/catalog/productwithoutattribute/", params: ["id":categoryId, "page":String(currentPage),"theme":"1", "country_id" : APP_DEL.selectedCountry.country_code ?? "SA"])
            } else {
                
                self.sendRequest(url: "mobiconnect/catalog/productwithoutattribute/", params: ["id":categoryId,"theme":"1", "store_id":UserDefaults.standard.value(forKey: "storeId") as! String, "page":String(currentPage), "multi_filter":filterString, "country_id" : APP_DEL.selectedCountry.country_code ?? "SA"])
            }
        }
    }
    
}

extension nejreeCategoryController: filterResultDelegate {
    
    func filterResult(data: String, applyFilter: Bool) {
        
        print(data)
        self.filterString = data
        self.applyFilter = applyFilter
        if !applyFilter{
            self.noProductCheck = false
        }
        self.noProductCheck = false
        collView.isHidden = true
        self.reloadData()
    }
    
    
}

