//
//  subcategoryMenuBarController.swift
//  MageNative Magento Platinum
//
//  Created by Manohar Singh Rawat on 02/09/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class subcategoryMenuBarController: cedMageViewController {
    
    var pageMenu : CAPSPageMenu?
    var controllerArray : [UIViewController] = []
    var categories = [category]()
    var catId = [String]()
    var catIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = APP_LBL().categories
        createMenuBar()
        
        
        // Do any additional setup after loading the view.
    }
    
    func createMenuBar() {
        
        let parameters: [CAPSPageMenuOption] = [
           .scrollMenuBackgroundColor(UIColor.black),
            .viewBackgroundColor(UIColor.black),
            .selectionIndicatorColor(cedMage.UIColorFromRGB(colorCode: "#FBAD18")),
            .bottomMenuHairlineColor(UIColor.white),
            .menuItemFont(UIFont(name: "Poppins", size: 12.0)!),
            .menuHeight(40.0),
            //            .menuItemWidth(60),
            .centerMenuItems(true),
            .addBottomMenuShadow(true),
            .menuShadowColor(UIColor.black),
            .menuItemMargin(0),
            .menuItemWidthBasedOnTitleTextWidth(true),
            .menuShadowRadius(4),.selectedMenuItemLabelColor(UIColor.init(hexString: "#FBAD18")!),.unselectedMenuItemLabelColor(UIColor.lightGray)
        ]
        
        // Initialize scroll menu
        let rect = CGRect(origin: CGPoint(x: 0,y :0), size: CGSize(width: self.view.frame.width, height: self.view.frame.height))
        pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: rect, pageMenuOptions: parameters)
        
        self.addChild(pageMenu!)
        self.view.addSubview(pageMenu!.view)
        
        
    }
    
    
    
    
    func didMoveToPage(controller: UIViewController, index: Int){
        catIndex = index;
    }
    
    
    
    
    
    
}
