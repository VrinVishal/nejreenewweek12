//
//  CategoryProductCollCell.swift
//  MageNative Magento Platinum
//
//  Created by Jayesh Dabhi on 10/02/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class CategoryProductCollCell: UICollectionViewCell {

    @IBOutlet weak var offerLabel: UILabel!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var productColor: UILabel!
    @IBOutlet weak var productPrice: UILabel!
    @IBOutlet weak var ratingView: FloatRatingView!
    @IBOutlet weak var insideView: UIView!
    @IBOutlet weak var regularPrice: UILabel!
    
    weak var imgProduct: UIImage!
    
    @IBOutlet weak var lblBrandName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
//        insideView.layer.cornerRadius = 2.0
//        insideView.layer.borderColor = UIColor.lightGray.cgColor
//        insideView.layer.borderWidth = 0.2
//        insideView.layer.shadowColor = UIColor(red: 225.0 / 255.0, green: 228.0 / 255.0, blue: 228.0 / 255.0, alpha: 1.0).cgColor
//        insideView.layer.shadowOpacity = 2.0
//        insideView.layer.shadowRadius = 2.0
//        insideView.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        
        
        // Initialization code
    }

}
