//
//  CategoryBannerCollCell.swift
//  MageNative Magento Platinum
//
//  Created by Jayesh Dabhi on 10/02/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class CategoryBannerCollCell: UICollectionViewCell {

    @IBOutlet weak var Categoryimage: UIImageView!
    @IBOutlet weak var categoryName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
