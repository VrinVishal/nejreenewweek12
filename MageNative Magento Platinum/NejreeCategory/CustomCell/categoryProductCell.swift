//
//  categoryProductCell.swift
//  MageNative Magento Platinum
//
//  Created by MacMini on 28/08/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class categoryProductCell: UITableViewCell {

    
    @IBOutlet weak var categoryCollection: UICollectionView!
    
    var products = [singleCategoryProduct]()
    var parent = UIViewController()
    override func awakeFromNib() {
        super.awakeFromNib()
        
        categoryCollection.delegate = self
        categoryCollection.dataSource = self
        
        // Initialization code
    }
    
    func reloadData(){
        
        categoryCollection.reloadData();
    }

    

}

extension categoryProductCell: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return products.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "nejreeCategoryCollectionCell", for: indexPath) as! nejreeCategoryCollectionCell
        cell.insideView.cardView()
        
        if products[indexPath.item].offer != "" {
            //cell.offerLabel.text = products[indexPath.item].offer! + APP_LBL().percent_off.uppercased()
            let offer =
            """
            \(products[indexPath.item].offer!) %
            \(APP_LBL().off.uppercased())
            """
            
            cell.offerLabel.text = offer
            cell.offerLabel.isHidden = false
        } else {
            cell.offerLabel.isHidden = true
        }

        cell.offerLabel.round(redius: 18)
        
        //cell.productImage.sd_setImage(with: URL(string: products[indexPath.item].product_image!), placeholderImage: (UIImage(named: "placeholder")), options: SDWebImageOptions.continueInBackground, context: nil)
        
       //  cell.productImage.sd_setImage(with: URL(string:String(format: "%@",products[indexPath.item].product_image!)), placeholderImage: UIImage(named: "placeholder"))
        
//        if let downloadURL = SDImageCache.shared.imageFromCache(forKey: products[indexPath.item].product_image!){
//             cell.productImage!.image = downloadURL
//        }
//        else{
//        cell.productImage!.sd_setImage(with: URL(string: products[indexPath.item].product_image!), placeholderImage: UIImage(named: "placeholder"))
           // cell.productImage!.sd_setImage(with: URL(string: products[indexPath.item].product_image!), placeholderImage: nil)
          
        //}
        
//        DispatchQueue.global(qos: .background).async {
//
//            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
//
//                cell.productImage!.sd_setImage(with: URL(string: self.products[indexPath.item].product_image!), placeholderImage: nil)
//
//
//            }
//        }
        
        
       // cell.productImage!.sd_setImage(with: URL(string: self.products[indexPath.item].product_image!), placeholderImage: nil)
        
        cell.productImage.image = nil
        
//        if let downloadURL = SDImageCache.shared.imageFromCache(forKey: products[indexPath.item].product_image!){
//             cell.productImage!.image = downloadURL
//        }
//        else{
            cell.productImage!.sd_setImage(with: URL(string: products[indexPath.item].product_image!), placeholderImage: nil)
          
        //}
        
        let name = products[indexPath.item].product_name?.components(separatedBy: "-")
         cell.productName.text = products[indexPath.item].product_name
        if let names = name{
            
            var newName = names[0]
            if newName.first == " " {
                newName.removeFirst()
            }
            
          //  cell.productName.text = newName//nam?.first
            
            //cell.productName.text = names[0]
            if(names.count>1){
                cell.productColor.text = names[1]
            }
            
        }
        
        cell.productColor.text = ""
        
        let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: products[indexPath.item].regular_price!)
        attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 1, range: NSMakeRange(0, attributeString.length))
        
        
        cell.lblBrandName.text = products[indexPath.item].brands_name
        cell.lblBrandName.textAlignment = .center
        
        
        if products[indexPath.item].special_price != "no_special"{
            cell.productPrice.text = products[indexPath.item].special_price;
            cell.regularPrice.isHidden = false
            cell.regularPrice.attributedText = attributeString
        }else {
            cell.productPrice.text = products[indexPath.item].regular_price
            cell.regularPrice.isHidden = true
        }
        if let rating = Float(products[indexPath.item].review!) {
             cell.ratingView.rating = rating
        }
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "productViewController") as! productViewController
        vc.product_id = products[indexPath.row].product_id!
        APP_DEL.productIDglobal = products[indexPath.row].product_id!
         vc.hidesBottomBarWhenPushed = true
        parent.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: frame.width/2, height: 365)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    
    
}
