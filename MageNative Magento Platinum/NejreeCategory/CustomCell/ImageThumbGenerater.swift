//
//  ImageThumbGenerater.swift
//  MageNative Magento Platinum
//
//  Created by Jayesh Dabhi on 07/02/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import Foundation

let IPH = ImagePeakHeap.shared
class ImagePeakHeap {
    
    private init() { }
    static let shared = ImagePeakHeap()

    func createThumbnail(from image: UIImage, fillingSize size: CGSize, completion: @escaping (_ image: UIImage) -> Void) {

        let scale = max(size.width / image.size.width, size.height / image.size.height)
        let width = image.size.width * scale
        let height = image.size.height * scale
        let thumbnailRect = CGRect(
            x: (size.width - width) / 2,
            y: (size.height - height) / 2,
            width: width,
            height: height)
        UIGraphicsBeginImageContext(size)
        image.draw(in: thumbnailRect)
        let thumbnail = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        completion(thumbnail!)
    }
//
//    func downSample(imageAt imageURL: URL, to pointSize: CGSize, completion: @escaping (_ image: UIImage?) -> Void) {
//
//        let imageSourceOptions = [kCGImageSourceShouldCache: false] as CFDictionary
//
//        guard let imageSource = CGImageSourceCreateWithURL(imageURL as CFURL, imageSourceOptions) else {
//
//            completion(nil);
//            return;
//        }
//
//        let maxDimentionInPixels = max(pointSize.width, pointSize.height) * UIScreen.main.scale
//
//        let downsampledOptions = [kCGImageSourceCreateThumbnailFromImageAlways: true,
//                                  kCGImageSourceShouldCacheImmediately: true,
//                                  kCGImageSourceCreateThumbnailWithTransform: true,
//                                  kCGImageSourceThumbnailMaxPixelSize: maxDimentionInPixels] as CFDictionary
//
//        guard let downsampledImage = CGImageSourceCreateThumbnailAtIndex(imageSource, 0, downsampledOptions) else {
//            completion(nil);
//            return;
//        }
//
//        return completion(UIImage(cgImage: downsampledImage))
//    }
    
}

