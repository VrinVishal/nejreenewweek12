//
//  FilterButtonCollReusableView.swift
//  MageNative Magento Platinum
//
//  Created by Jayesh Dabhi on 10/02/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class FilterButtonCollReusableView: UICollectionReusableView {

    @IBOutlet weak var filterButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}
