//
//  categoryMenuBarController.swift
//  MageNative Magento Platinum
//
//  Created by MacMini on 28/08/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import IQKeyboardManager
import FirebaseAnalytics


struct category {
    var categoryName: String?
    var categoryId: String?
    var categoryImage: String?
    var hasChild: String?
    var subCategory: [subCategory]?
    var sort_order: String?
}
struct subCategory {
    var subCategoryName: String?
    var subCategoryId: String?
    var subCategoryImage: String?
    var hasChild: String?
}


class categoryMenuBarController: cedMageViewController, FilterApply {
    
    
    @IBOutlet weak var viewSearch: UIView!
    @IBOutlet weak var viewTxtSearch: UIView!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var viewNotification: UIView!
    @IBOutlet weak var lblNotificCount: UILabel!
    @IBOutlet weak var btnClear: UIButton!
    
    @IBOutlet weak var viewFilter: UIView!
    @IBOutlet weak var btnFilter: UIButton!
    
    @IBOutlet weak var viewSearchList: UIView!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var htTbl: NSLayoutConstraint!
    
    @IBOutlet weak var viewSubVC: UIView!
    
    @IBOutlet weak var btnSearch: UIButton!
    
    var searchText = ""
    var suggentionsArray = [[String:String]]()
    
    
    
    
    var pageMenu : CAPSPageMenu?
    var controllerArray : [UIViewController] = []
    var categories = [category]()
    var catId = [String]()
    var catIndex = 0
    var applyFilter = false
    var filterString = String()
    var catnameAndId = [[String:String]]()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //self.navigationItem.title = "Categories".localized
        
        //self.getAllCategory()
        
        // Do any additional setup after loading the view.
        //setUI()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
           super.viewWillAppear(animated)
           
           self.navigationController?.setNavigationBarHidden(true, animated: true)
           
           IQKeyboardManager.shared().isEnabled = false
           IQKeyboardManager.shared().isEnableAutoToolbar = false
           
//           self.tblView.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
       }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        IQKeyboardManager.shared().isEnabled = true
        IQKeyboardManager.shared().isEnableAutoToolbar = true
    }
    
    
    var filterCategoryID = ""
    @IBAction func btnFilterAction(_ sender: Any) {
        
//        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NejreeCategoryFilterVC") as! NejreeCategoryFilterVC
//        vc.hidesBottomBarWhenPushed = true
//        if let temp = self.controllerArray[self.pageMenu!.currentPageIndex] as? CategoryVC {
//            vc.categoryId = temp.categoryId
//            vc.filterDT = temp.filterDT
//        }
//        vc.isCategoryFilter = true
//
//        vc.delegateFilterApply = self
//        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func filterApply(filter: String, filterDT: FilterData, isForShowFilterView: Bool) {

//        if let temp = self.controllerArray[self.pageMenu!.currentPageIndex] as? CategoryVC {
//
//            temp.filterApply(filter: filter, filterDT: filterDT)
//        }
    }


    
}

//MARK:- SEARCH CODE
//extension categoryMenuBarController {
//
//    @IBAction func btnSearchAction(_ sender: Any) {
//
//        if self.viewTxtSearch.isHidden == true {
//
//            self.viewTxtSearch.isHidden = true
//            //self.viewSearch.isHidden = false
//            self.txtSearch.becomeFirstResponder()
//
//            UIView.animate(withDuration: 0.3, delay: 0.0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.7, options: [.curveLinear], animations: { () -> Void in
//
//                self.viewTxtSearch.isHidden = false
//                //self.viewSearch.isHidden = true
//                self.view.layoutIfNeeded()
//
//            }, completion: nil)
//
//        } else {
//
//            self.view.endEditing(true)
//
//            Analytics.logEvent(AnalyticsEventSearch, parameters: [AnalyticsParameterSearchTerm : searchText])
//
//            if let viewController =  UIStoryboard(name: "categorylayouts", bundle: nil).instantiateViewController(withIdentifier: "cedMageDefaultCollection") as?  cedMageDefaultCollection {
//
//                viewController.searchString = searchText
//                viewController.hidesBottomBarWhenPushed = true
//
//                if let temp = self.controllerArray[self.pageMenu!.currentPageIndex] as? CategoryVC {
//                    viewController.selectedCategory = temp.categoryId
//                }
//
//                if (self.txtSearch.text != "") {
//
//                    self.navigationController?.pushViewController(viewController, animated: true)
//                }
//            }
//        }
//
//
//    }
//
//    @IBAction func btnClearAction(_ sender: Any) {
//
//        if self.txtSearch.text == "" {
//
//            self.view.endEditing(true)
//
//            self.viewTxtSearch.isHidden = false
//            self.viewSearch.isHidden = true
//            UIView.animate(withDuration: 0.3, delay: 0.0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.7, options: [.curveLinear], animations: { () -> Void in
//
//                self.viewTxtSearch.isHidden = true
//                self.viewSearch.isHidden = false
//                self.view.layoutIfNeeded()
//
//            }, completion: nil)
//
//        } else {
//
//            self.txtSearch.text = ""
//            self.displayClear(res: false)
//            self.searchText(text: "")
//        }
//    }
//
//    func displayClear(res: Bool) {
//
//        //self.btnClear.isHidden = !res
//    }
//
//    func searchText(text: String) {
//
//        searchText = text
//
//        if text == "" {
//
//            self.htTbl.constant = 4.0
//            self.viewSearchList.isHidden = true
//            self.tblView.isHidden = true
//
//        } else {
//
//            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(getHint), object: nil)
//            self.perform(#selector(getHint), with: nil, afterDelay: 1)
//        }
//
//        //checkNoData()
//    }
//
//
//    @objc func getHint() {
//
//        if(searchText.count >= 3) {
//
//            searchText = searchText.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
//            getAutocomplete()
//        }
//    }
//
//    func getAutocomplete() {
//
//        guard let storeId = UserDefaults.standard.value(forKey: "storeId") as? String else {return}
//
//        var postData = [String:String]()
//        postData["page"] = "1"
//        postData["store_id"] = storeId
//        postData["keyword"] = searchText
//
//        cedMageLoaders.removeLoadingIndicator(me: self);
//        cedMageLoaders.addDefaultLoader(me: self);
//
//        API().callAPI(endPoint: "mobiconnectadvcart/search/autocomplete", method: .POST, param: postData) { (json, err) in
//
//            DispatchQueue.main.async {
//
//                cedMageLoaders.removeLoadingIndicator(me: self);
//
//                let json_0 = json[0]
//
//                if err == nil {
//
//                    if (json_0["data"]["status"].stringValue != "empty")
//                    {
//                        self.suggentionsArray.removeAll()
//                        for (_,value) in json_0["data"]["suggestion"]
//                        {
//
//                            var data = [String:String]()
//                            data["product_id"] = value["product_id"].stringValue
//                            data["product_name"] = value["product_name"].stringValue
//                            data["product_image"] = value["product_image"].stringValue
//                            self.suggentionsArray.append(data)
//                        }
//
//                        self.tblView.reloadData()
//                        self.viewSearchList.isHidden = false
//                        self.tblView.isHidden = false
//                    }
//                    else
//                    {
//                        self.htTbl.constant = 4.0
//                        self.viewSearchList.isHidden = true
//                        self.tblView.isHidden = true
//                    }
//
//                }
//            }
//        }
//    }
//
//}
//
//extension categoryMenuBarController: UITextFieldDelegate {
//
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//
//        if let str = textField.text, let swtRange = Range(range, in: str) {
//
//            let fullString = str.replacingCharacters(in: swtRange, with: string)
//            self.displayClear(res: (fullString.count > 0))
//            searchText(text: fullString)
//        }
//
//        return true;
//    }
//
//    func textFieldDidEndEditing(_ textField: UITextField) {
//
//        self.displayClear(res: ((textField.text ?? "").count > 0))
//    }
//
//    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//
//       // filter(text: textField.text ?? "")
//        self.view.endEditing(true)
//
//        Analytics.logEvent(AnalyticsEventSearch, parameters: [AnalyticsParameterSearchTerm : searchText])
//
//        if let viewController =  UIStoryboard(name: "categorylayouts", bundle: nil).instantiateViewController(withIdentifier: "cedMageDefaultCollection") as?  cedMageDefaultCollection {
//
//            viewController.searchString = searchText
//
//            if (self.txtSearch.text != "") {
//
//                self.navigationController?.pushViewController(viewController, animated: true)
//            }
//        }
//
//        return true
//    }
//}
//
//extension categoryMenuBarController: UITableViewDelegate, UITableViewDataSource {
//
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return suggentionsArray.count
//    }
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//
//        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchProductCell") as! SearchProductCell
//        cell.selectionStyle = .none
//
//        cell.imgView.sd_setImage(with: URL(string: suggentionsArray[indexPath.row]["product_image"] ?? ""), placeholderImage: nil)
//
//        cell.lblTitle.text = suggentionsArray[indexPath.row]["product_name"]
//
//        return cell;
//    }
//
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//
//        let vc = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "productViewController") as! productViewController
//        APP_DEL.productIDglobal = suggentionsArray[indexPath.row]["product_id"]!
//        vc.product_id = suggentionsArray[indexPath.row]["product_id"]!
//        self.navigationController?.pushViewController(vc, animated: true)
//
//        self.btnClearAction(self.btnClear)
//    }
//
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//
//        return UITableView.automaticDimension
//    }
//
//}
