//
//  nejreeAddAddressController.swift
//  MageNative Magento Platinum
//
//  Created by MacMini on 22/08/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

struct countryData :  Codable{
    var label: String? = ""
    var label_ar: String? = ""
    var is_region_visible: String? = ""
    var value: String? = ""
    var phone_code: String? = ""
    var currency_code_en: String? = ""
    var currency_code_ar: String? = ""
    var country_code: String? = ""
    var phone_number_limit: String? = ""
    var country_flag_image: String? = ""
    var CODDisableAfterTotal: Int? = 1000
    var extrafee_minimum_order_amount: Int? = 1000
    init(){
        
    }
    
    func syncronize() {
        
        do {
            APP_DEL.arrCategoriesCat.removeAll()
            let json : [String : Any] = try JSONSerialization.jsonObject(with: JSONEncoder().encode(self), options: []) as! [String : Any]
            APP_UDS.set(json, forKey: Constants().USERDEFAULT_KEY_SELECTED_COUNTRY)
            APP_UDS.synchronize()
            codTotalToDiableOption = Int(self.CODDisableAfterTotal ?? 1200)
            APP_DEL.extrafee_minimum_order_amount = "\(Int(self.extrafee_minimum_order_amount ?? 0) )"
        } catch {
            print("ERROR.JD")
        }
    }
    
    func getCurrencySymbol() -> String {
        if (APP_DEL.selectedLanguage == Arabic){
            return currency_code_ar ?? "SAR_ENGLISH"
        } else {
            return currency_code_en ?? "SAR_ARABIC"
        }
        
    }
    
    func getPhoneCodeWithPlus() -> String {
            return ("+" + (phone_code ?? "966"))
        
    }
    
    func getCountryName() -> String {
        
        if (APP_DEL.selectedLanguage == Arabic){
            return label_ar ?? (label ?? "")
        } else {
            return label ?? ""
        }
    }
    
}

struct cityData {
    var label: String?
    var value: String?
}

struct neighbourData {
    var label: String?
    var value: String?
    var arabic_value: String?
}

class nejreeAddAddressController: MagenativeUIViewController {
    
    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var addressTable: UITableView!
    var viewToShow = UIView()
    var countryTableView = UITableView()
    var cityTableView = UITableView()
    var neighbourTableView = UITableView()
    var tapGesture = UITapGestureRecognizer()
    var countries = [countryData]()
    var tempCountries = [countryData]()
    var cities = [cityData]()
    var tempCities = [cityData]()
    var neighbours = [neighbourData]()
    var tempNeighbours = [neighbourData]()
    var selectedLocation = [String:String]()
    var mainvc: UIViewController?
    var selectedCountry : countryData?

    var isDefaultAddress = false
    
    var isFromEditAddress = false
    
    var requestedString = ""
    var FirstName = ""
    var LastName = ""
    var Country = ""
    var CountryCode = ""
    var City = ""
    var Neighbourhood = ""
    var Postcode = ""
    var Address_ID = ""
    var phone_number = ""
    var is_default = ""

    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addressTable.separatorStyle = .none
        
        self.updateEditDetail()
        
        
        if (phone_number == ""){
            if let mobile = defaults.value(forKey: "mobile_number") as? String {
                
                phone_number = mobile
            } else {
                //cell.phoneNumberLabel.text = "xxxxxxxxx"
                phone_number = ""
            }
        }
        
        isDefaultAddress = (is_default == "1")
        
        // Do any additional setup after loading the view.
        tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissPopup(_:)))
        tapGesture.delegate = self
          
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        if navigationController?.responds(to: #selector(getter: UINavigationController.interactivePopGestureRecognizer)) ?? false {
                          
                          self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
                          self.navigationController?.interactivePopGestureRecognizer?.delegate = self
                      }
      
    }
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        
        return true
    }
    
    
    func updateEditDetail() {
        
        
        self.addressTable.delegate = self
        self.addressTable.dataSource = self
        self.addressTable.reloadData()
        
//        self.selectedLocation["country"] = "SA"
//        if APP_DEL.selectedLanguage == Arabic {
//            cell.countryTextfield.text = "المملكة العربية السعودية"
//        } else {
//            cell.countryTextfield.text = "Saudi Arabia"
//        }
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);
        
        let storeID = UserDefaults.standard.value(forKey: "storeId") as! String?
        var postData = [String:String]()
        postData["store_id"] = "2"
        API().callAPI(endPoint: "mobiconnect/getcountrybystore", method: .POST, param: postData) { (json_res, err) in

                cedMageLoaders.removeLoadingIndicator(me: self);

                if err == nil {

                    let cell = self.addressTable.cellForRow(at: IndexPath(row: 0, section: 1)) as! addNewAddressCell
                    
                    let json = json_res[0]
                    
                    self.countries = []
                    self.tempCountries = []
                    
                    var countryValue = ""
                    let catData = try! json["country"].rawData()
                    self.countries = try! JSONDecoder().decode([countryData].self, from: catData)
                    self.tempCountries  = try! JSONDecoder().decode([countryData].self, from: catData)
                    for recCountryData in self.tempCountries {
                        
                        if ((recCountryData.country_code ?? "").uppercased() == self.CountryCode.uppercased()){
                            
                            if APP_DEL.selectedLanguage == Arabic{
                                cell.countryTextfield.text = (recCountryData.label_ar ?? "")
                            }
                            else{
                                cell.countryTextfield.text = (recCountryData.label ?? "")
                            }
                            
                            
                            self.selectedLocation["country"] =  (recCountryData.country_code ?? "")
                            countryValue = (recCountryData.country_code ?? "")
                            self.selectedCountry = recCountryData
                        }
             
                        
                    }
                    
                    self.addressTable.reloadData()
                    
                    
                    if (self.isFromEditAddress || self.CountryCode != "") {
                        
                        let getCityEndPoint = "mobiconnect/module/getstorewisecity/\(countryValue)/store/\(storeID ?? "1")"
                        
                        cedMageLoaders.removeLoadingIndicator(me: self);
                        cedMageLoaders.addDefaultLoader(me: self);
                        
                        API().callAPI(endPoint: getCityEndPoint, method: .GET, param: [:]) { (json_2, err) in
                            
                            cedMageLoaders.removeLoadingIndicator(me: self);
                            
                            if err == nil {
                                
                                self.cities = []
                                self.tempCities = []
                                
                                var cityValue = ""
                                
                                for city in json_2[0]["city"].arrayValue {
                                    
                                    if city["label"].stringValue == self.City {
                                        
                                        cell.cityTextfield.text = city["label"].stringValue
                                        self.selectedLocation["city"] =  city["label"].stringValue
                                        self.selectedLocation["city_id"] =  city["value"].stringValue

                                        cityValue = city["value"].stringValue
                                    }
                                    
                                    let temp = cityData(label: city["label"].stringValue, value: city["value"].stringValue)
                                    self.cities.append(temp)
                                    self.tempCities.append(temp)
                                }
                                
                                let _ = self.isValidDetail()
                                
                                cedMageLoaders.removeLoadingIndicator(me: self);
                                cedMageLoaders.addDefaultLoader(me: self);
                                
                                API().callAPI(endPoint: "mobiconnect/module/getneighbours/\(cityValue)", method: .GET, param: [:]) { (json_3, err) in
                                    
                                    cedMageLoaders.removeLoadingIndicator(me: self);
                                    
                                    if err == nil {
                                        
                                        self.neighbours = []
                                        self.tempNeighbours = []
                                        
                                        for neigh in json_3[0]["neighbours"].arrayValue {
                                            let temp = neighbourData(label: neigh["label"].stringValue, value: neigh["value"].stringValue, arabic_value: neigh["arabic_label"].stringValue)
                                            
                                            self.neighbours.append(temp)
                                            self.tempNeighbours.append(temp)
                                            
                                            if self.Neighbourhood == neigh["arabic_label"].stringValue || self.Neighbourhood == neigh["label"].stringValue {
                                                
                                                if APP_DEL.selectedLanguage == Arabic{
                                                    
                                                    cell.neighborhoodTextfield.text = neigh["arabic_label"].stringValue
                                                    self.selectedLocation["neighbour"] = neigh["arabic_label"].stringValue
                                                    self.selectedLocation["neighbour_id"] = neigh["value"].stringValue

                                                } else {
                                                    
                                                    cell.neighborhoodTextfield.text = neigh["label"].stringValue
                                                    self.selectedLocation["neighbour"] = neigh["label"].stringValue
                                                    self.selectedLocation["neighbour_id"] = neigh["value"].stringValue
                                                }
                                            }
                                        }
                                    } else {
                                        
                                        cedMageLoaders.removeLoadingIndicator(me: self);
                                    }
                                    let _ = self.isValidDetail()

                                    
                                }
                                
                            } else {
                                
                                cedMageLoaders.removeLoadingIndicator(me: self);
                            }
                        }
                        
                    } else {
                        
                        cedMageLoaders.removeLoadingIndicator(me: self);
                    }
                    
                    
                } else {
                    
                    cedMageLoaders.removeLoadingIndicator(me: self);
                }
        }
    }

    func getCity() {

        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);
        let storeID = UserDefaults.standard.value(forKey: "storeId") as! String?
               
        let getCityEndPoint = "mobiconnect/module/getstorewisecity/\(self.selectedLocation["country"] ?? "")/store/\(storeID ?? "1")"
        
        
        API().callAPI(endPoint: getCityEndPoint, method: .GET, param: [:]) { (json_res, err) in
            
           // DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                if err == nil {
                    
                    let json = json_res[0]
                    
                    if json["success"].stringValue == "true" {
                        
                        let cell = self.addressTable.cellForRow(at: IndexPath(row: 0, section: 1)) as! addNewAddressCell
                        cell.cityTextfield.text = ""
                        cell.neighborhoodTextfield.text = ""
                        
                        self.cities.removeAll()
                        self.tempCities.removeAll()
                        self.neighbours.removeAll()
                        
                            for city in json["city"].arrayValue {
                                
                                let temp = cityData.init(label: city["label"].stringValue,
                                                         value: city["value"].stringValue)
                                self.cities.append(temp)
                                self.tempCities.append(temp)
                            }
                     
                    }
                }
        }
    }

    func getNeighbours(city: String) {
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);
        
        API().callAPI(endPoint: "mobiconnect/module/getneighbours/\(city)", method: .GET, param: [:]) { (json_res, err) in

         //   DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);

                if err == nil {
                    
                    let json = json_res[0]
                    
                    let cell = self.addressTable.cellForRow(at: IndexPath(row: 0, section: 1)) as! addNewAddressCell
                    cell.neighborhoodTextfield.text = ""
                    self.neighbours = [neighbourData]()
                    self.tempNeighbours = [neighbourData]()
                    
                    for neigh in json["neighbours"].arrayValue {
                        let temp = neighbourData.init(label: neigh["label"].stringValue,
                                                      value: neigh["value"].stringValue,
                                                      arabic_value: neigh["arabic_label"].stringValue)
                        
                        self.neighbours.append(temp)
                        self.tempNeighbours.append(temp)
                    }

                }
         //   }
        }
    }
    
    @objc func btnClearTapped(btn: UIButton) {
        
        if ((btn.accessibilityIdentifier ?? "") == "localSearchCountry") {
            
            self.txtSearch.text = ""
            self.countries = self.tempCountries
            self.btnSearchClear.isHidden = true
            self.countryTableView.reloadData()
            
        } else if ((btn.accessibilityIdentifier ?? "") == "localSearchCity") {
            
            self.txtSearch.text = ""
            self.cities = self.tempCities
            self.btnSearchClear.isHidden = true
            self.cityTableView.reloadData()
            
        } else if ((btn.accessibilityIdentifier ?? "") == "localSearchNeighbours") {
            
            self.txtSearch.text = ""
            self.neighbours = self.tempNeighbours
            self.btnSearchClear.isHidden = true
            self.neighbourTableView.reloadData()
        }
    }
    
    var btnSearchClear = UIButton()
    var txtSearch = UITextField()
    func displayClear(res: Bool) {
        
        self.btnSearchClear.isHidden = !res
    }
    
    func logout() {
           
           var newPram = [String: String]()
           
           if let userInfoDict = UserDefaults.standard.object(forKey: "userInfoDict") as? [String: String] {
               
               newPram["hashkey"] = userInfoDict["hashKey"]
               newPram["customer_id"] = userInfoDict["customerId"]
           }
           
           if let storeID =  UserDefaults.standard.value(forKey: "storeId") as? String {
               newPram["store_id"] = storeID
           }
           
           cedMageLoaders.removeLoadingIndicator(me: self);
           cedMageLoaders.addDefaultLoader(me: self);
           
           API().callAPI(endPoint: "mobiconnect/customer/logout", method: .POST, param: newPram) { (json_res, err) in
               
             //  DispatchQueue.main.async {
                   
                   cedMageLoaders.removeLoadingIndicator(me: self);
                   
                   if err == nil {
                       
                       self.defaults.set(false, forKey: "isLogin");
                       self.defaults.removeObject(forKey: "userInfoDict");
                       self.defaults.removeObject(forKey: "selectedAddressId");
                       self.defaults.setValue("Magenative App For Magento 2", forKey: "name")
                       UserDefaults.standard.set(nil,forKey: "LIAccessToken")
                       UserDefaults.standard.synchronize();
                       self.defaults.synchronize()
                       self.defaults.removeObject(forKey: "selectedAddressDescription");
                       
                       tempAddressObjApplePay = nil
                       
                       self.defaults.removeObject(forKey: "cartId");
                       self.defaults.removeObject(forKey: "cart_summary");
                       self.defaults.removeObject(forKey: "guestEmail");
                       self.defaults.set("0", forKey: "items_count");
                       self.defaults.removeObject(forKey: "name")
                       //self.defaults.removeObject(forKey: "mobile_number")
                       self.setCartCount(view: self, items: "0")
                       self.navigationController?.navigationBar.isHidden = false
                       
                       UserDefaults.standard.set(0, forKey: "NejreeUnreadCount")
                       UserDefaults.standard.set("0", forKey: "wishlist_item_count")
                       
                        APP_DEL.isLogoutFromAddAddress = true
                    
                       APP_DEL.changeLanguage()
                       
                      
                   }
             //  }
           }
       }
    
    
    @objc func saveButtonTapped(_ sender: UIButton) {
        
        let cell = addressTable.cellForRow(at: IndexPath(row: 0, section: 1)) as! addNewAddressCell
        
//        if cell.nameTextfield.text == "" || cell.cityTextfield.text == "" || cell.neighborhoodTextfield.text == "" {
//            self.view.makeToast("Enter details to save".localized, duration: 1.0, position: .center)
//            return
//        }
        
        if (cell.nameTextfield.text ?? "") == "" {
            self.view.makeToast(APP_LBL().please_enter_first_name, duration: 1.0, position: .center)
            return
        }
        
        if (cell.lName.text ?? "") == "" {
           self.view.makeToast(APP_LBL().please_enter_last_name, duration: 1.0, position: .center)
           return
       }
        
        if (cell.txtPhoneNumber.text ?? "") == "" {
            cell.phoneNumberView.layer.borderColor = UIColor.init(hexString: "#AF1F45")?.cgColor
            self.view.makeToast(APP_LBL().please_enter_phone_no, duration: 1.0, position: .center)
            return;
        }
        
       
        if (cell.countryTextfield.text ?? "") == "" {
            cell.countryView.layer.borderColor = UIColor.init(hexString: "#AF1F45")?.cgColor
            self.view.makeToast(APP_LBL().please_select_country, duration: 1.0, position: .center)
            return
        }
        
        if ((cell.txtPhoneNumber.text ?? "").count) != (Int(selectedCountry?.phone_number_limit ?? "") ?? 9) {
            cell.phoneNumberView.layer.borderColor = UIColor.init(hexString: "#AF1F45")?.cgColor
            self.view.makeToast(APP_LBL().please_enter_valid_phone_no, duration: 1.0, position: .center)
            return;
        }
        
        
        if (cell.cityTextfield.text ?? "") == "" {
            cell.cityView.layer.borderColor = UIColor.init(hexString: "#AF1F45")?.cgColor
            self.view.makeToast(APP_LBL().please_select_city, duration: 1.0, position: .center)
            return
        }
        
        if (cell.neighborhoodTextfield.text ?? "") == "" {
            cell.neighborhoodView.layer.borderColor = UIColor.init(hexString: "#AF1F45")?.cgColor
            self.view.makeToast(APP_LBL().please_select_neighborhood, duration: 1.0, position: .center)
            return
        }
        
        if (cell.postcodeTextfield.text ?? "") == "" {
        //            cell.postcodeView.layer.borderColor = UIColor.init(hexString: "#AF1F45")?.cgColor
        //            self.view.makeToast(APP_LBL().please_enter_postal_code, duration: 1.0, position: .center)
        //            return
                } else {
                    if (cell.postcodeTextfield.text ?? "").count < 5 || (cell.postcodeTextfield.text ?? "").count > 10 {
                        cell.postcodeView.layer.borderColor = UIColor.init(hexString: "#AF1F45")?.cgColor
                        self.view.makeToast(APP_LBL().please_enter_valid_postal_code, duration: 1.0, position: .center)
                        return
                    }
                }
        
        
        var postData = [String:String]()
        let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
        
        if userInfoDict["customerId"] == ""{
            
            let showTitle = APP_LBL().error
            let showMsg = APP_LBL().add_address_login_required
            
            let confirmationAlert = UIAlertController(title: showTitle, message: showMsg, preferredStyle: UIAlertController.Style.alert);
            confirmationAlert.addAction(UIAlertAction(title: APP_LBL().please_login_first, style: .default, handler: { (action: UIAlertAction!) in
                
                self.logout()
                
            }));
            
            self.present(confirmationAlert, animated: true, completion: nil)
            
            //add_address_login_required
            return;
        }
        
        

        postData["hashkey"] = userInfoDict["hashKey"]
        postData["customer_id"] = userInfoDict["customerId"]
        postData["country_id"] = selectedLocation["country"]
        postData["city"] = selectedLocation["city"]
        postData["city_id"] = selectedLocation["city_id"]
        postData["neighbour_name"] = selectedLocation["neighbour"]
        postData["neighbour_id"] = selectedLocation["neighbour_id"]
        postData["firstname"] = cell.nameTextfield.text!
        postData["lastname"] = cell.lName.text!
        postData["email"] = "z@gmail.com"
        postData["street"] = "Street"
        //postData["telephone"] = cell.phoneNumberLabel.text
        postData["telephone"] = cell.txtPhoneNumber.text
        postData["store_id"] = UserDefaults.standard.value(forKey: "storeId") as! String?
        postData["is_default"] = (self.isDefaultAddress == true) ? "1" : "0"
        
        if isFromEditAddress {
            postData["address_id"] = Address_ID
        }
        postData["postcode"] = cell.postcodeTextfield.text ?? ""
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);
        
        self.view.isUserInteractionEnabled = false;
        
        API().callAPI(endPoint: "mobiconnect/customer/saveaddressbycountry", method: .POST, param: postData) { (json, err) in
            
          //  DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                if err == nil {
                    
                    self.view.isUserInteractionEnabled = true;
                    var msg = json[0]["data"]["customer"][0]["message"].stringValue;
                    if (json[0]["data"]["customer"][0]["status"].stringValue == "success") {
                        
                        
                        if self.isFromEditAddress{
                            msg = APP_LBL().address_edited_successfully;
                        }
                        else
                        {
                            msg = APP_LBL().address_added_successfully;
                        }
                        
                        self.view.isUserInteractionEnabled = false;
                        self.view.makeToast(msg, duration: 2, position: .center, title: nil, image: nil, style: nil, completion: {
                            Void in
                            
                            self.view.isUserInteractionEnabled = true;
                            //self.navigationController!.popViewController(animated: true);
                            self.navigationController?.popViewController(animated: true);
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "newAddressAddedId"), object: nil);
                        })
                        
                    } else {
                        self.view.makeToast(msg, duration: 2.0, position: .center, title: nil, image: nil, style: nil, completion: nil);
                    }
                }
         //   }
        }
    }
    
    @IBAction func btnDefaultAddressAction(_ sender: Any) {
        
        let cell = addressTable.cellForRow(at: IndexPath(row: 0, section: 1)) as! addNewAddressCell
        
        isDefaultAddress = !isDefaultAddress
        
        if isDefaultAddress {
            cell.imgDefaultAddress.image = UIImage(named: "checkout_check")
        } else {
            cell.imgDefaultAddress.image = UIImage(named: "PaymentMethodNotSelected")
        }
    }
    
    @objc func dismissButtonTapped(_ sender: UIButton) {
     
        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    func isValidDetail() -> Bool {
        
        guard let cell = addressTable.cellForRow(at: IndexPath(row: 0, section: 1)) as? addNewAddressCell else {
            return false
        }
        
        var isValid = true
        var isValidPhoneNumbner = true
        var isValidCountry = true
        var isValidCity = true
        var isValidNeighbour = true
        var isValidPostcode = true

        if (cell.nameTextfield.text ?? "") == "" {
             isValid = false
         }
        if (cell.lName.text ?? "") == "" {
            isValid = false
        }
        if (cell.txtPhoneNumber.text ?? "") == "" {
            isValidPhoneNumbner = false
             isValid = false
        }
        if ((cell.txtPhoneNumber.text ?? "").count) != (Int(APP_DEL.selectedCountry.phone_number_limit ?? "9") ?? 9) {
            isValidPhoneNumbner = false
             isValid = false
         }
        if (cell.countryTextfield.text ?? "") == "" {
            isValidCountry = false
             isValid = false
         }
        if (cell.cityTextfield.text ?? "") == "" {
            isValidCity = false
             isValid = false
         }
        if (cell.neighborhoodTextfield.text ?? "") == "" {
            isValidNeighbour = false
             isValid = false
         }
//        if (cell.postcodeTextfield.text ?? "") == "" {
//            isValidPostcode = false
//             isValid = false
//         }

        if (isValidPhoneNumbner){
            cell.phoneNumberView.layer.borderColor = nejreeColor?.cgColor
        } else {
            cell.phoneNumberView.setBorderBlack()
        }
        
        if (isValidCountry){
            cell.countryView.layer.borderColor = nejreeColor?.cgColor
        } else {
            cell.countryView.setBorderBlack()
        }
        
        if (isValidCity){
            cell.cityView.layer.borderColor = nejreeColor?.cgColor
        } else {
            cell.cityView.setBorderBlack()
        }
        
        if (isValidNeighbour){
            cell.neighborhoodView.layer.borderColor = nejreeColor?.cgColor
        } else {
            cell.neighborhoodView.setBorderBlack()
        }
        
//        if (isValidPostcode){
//            cell.postcodeView.layer.borderColor = nejreeColor?.cgColor
//        } else {
//            cell.postcodeView.setBorderBlack()
//        }
        
        if isValid {
            cell.saveButton.layer.borderColor = nejreeColor?.cgColor
            cell.saveButton.backgroundColor = UIColor.black
            cell.saveButton.setTitleColor(UIColor.white, for: .normal)
            
        } else {
            
            cell.saveButton.layer.borderColor = UIColor.gray.cgColor
            cell.saveButton.backgroundColor = UIColor.gray
            cell.saveButton.setTitleColor(UIColor.white, for: .normal)
        }
        
        return isValid
    }
}

extension nejreeAddAddressController: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch tableView {
        case addressTable:
            return 1
        case countryTableView:
            return countries.count
        case cityTableView:
            return cities.count
        case neighbourTableView:
            return neighbours.count
        default:
            return 0
        }
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        switch tableView {
               case addressTable:
                   return 2
               case countryTableView:
                   return 1
               case cityTableView:
                   return 1
               case neighbourTableView:
                   return 1
               default:
                   return 0
        }
    }
    
    @objc func BtnCountryClick(btn : UIButton) {
        self.viewToShow.removeFromSuperview()

        if (self.countries.count <= 0){
            self.countries = self.tempCountries
            return
        }
         let cell = addressTable.cellForRow(at: IndexPath(row: 0, section: 1)) as! addNewAddressCell
        
        addressTable.endEditing(true)
        
        cell.countryTextfield.endEditing(true)
        cell.nameTextfield.endEditing(true)
        cell.lName.endEditing(true)
        self.view.endEditing(true)
        createTable(tableName: "Country");
        
    }
    
    @objc func BtnCityClick(btn : UIButton) {
        self.viewToShow.removeFromSuperview()

        if (self.cities.count <= 0){
            self.cities = self.tempCities
            return
        }
         let cell = addressTable.cellForRow(at: IndexPath(row: 0, section: 1)) as! addNewAddressCell
        
        addressTable.endEditing(true)
        
        cell.countryTextfield.endEditing(true)
        cell.nameTextfield.endEditing(true)
        cell.lName.endEditing(true)
        self.view.endEditing(true)
        createTable(tableName: "City");
        
    }
    @objc func BtnNeighborClick(btn : UIButton) {
        self.viewToShow.removeFromSuperview()

        if (self.neighbours.count <= 0){
            self.neighbours = self.tempNeighbours
            return
        }
            let cell = addressTable.cellForRow(at: IndexPath(row: 0, section: 1)) as! addNewAddressCell
           
           addressTable.endEditing(true)
           
           cell.countryTextfield.endEditing(true)
           cell.nameTextfield.endEditing(true)
           cell.lName.endEditing(true)
           self.view.endEditing(true)
           createTable(tableName: "Neighbourhood");
           
       }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch tableView {
            
        case addressTable:
            if (indexPath.section == 0){
                let cell = tableView.dequeueReusableCell(withIdentifier: "nejreeNewAddressImageCell", for: indexPath) as! nejreeLoginImageCell
                cell.btnBack.addTarget(self, action: #selector(dismissButtonTapped(_:)), for: .touchUpInside)

                if (APP_DEL.isLoginFromWishList) {
                    cell.btnBack.isHidden = true
                } else {
                    cell.btnBack.isHidden = false
                }

               
                if APP_DEL.selectedLanguage == Arabic {
                    cell.btnBack.setImage(UIImage(named: "icon_back_white_Arabic"), for: .normal)
                } else {
                    cell.btnBack.setImage(UIImage(named: "icon_back_white_round"), for: .normal)
                }

                
                cell.lblSignIn.textAlignment = .left
                
                if isFromEditAddress {
                
                    cell.lblSignIn.text = APP_LBL().edit_address.uppercased()
                }
                else
                {
                    cell.lblSignIn.text = APP_LBL().add_new_address.uppercased()
                }
                
               // cell.lblSignIn.text = APP_LBL().add_new_address.uppercased()

                return cell
                
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "addNewAddressCell", for: indexPath) as! addNewAddressCell
            
                cell.countryCodeLabel.text = "+" + (selectedCountry?.phone_code ?? "966")
                cell.nameImage.image = UIImage(named: "")
                cell.lNameValidImage.image = UIImage(named: "")
   
                cell.nameTextfield.text = FirstName
                cell.postcodeTextfield.text = Postcode
                cell.lName.text = LastName
                
                cell.nameTextfield.delegate = self
                cell.postcodeTextfield.delegate = self
                cell.cityTextfield.delegate = self
                cell.neighborhoodTextfield.delegate = self
                cell.countryTextfield.delegate = self
                cell.lName.delegate = self
                cell.txtPhoneNumber.delegate = self
                cell.txtPhoneNumber.accessibilityIdentifier = "txtPhoneNumber"
                cell.postcodeTextfield.keyboardType = .numbersAndPunctuation
                
                cell.nameView.setBorderBlack()
                cell.postcodeView.setBorderBlack()
                cell.countryView.setBorderBlack()
                cell.cityView.setBorderBlack()
                cell.neighborhoodView.setBorderBlack()
                cell.phoneNumberView.setBorderBlack()
                cell.lNameView.setBorderBlack()
                
                
                cell.btnCountry.addTarget(self, action: #selector(BtnCountryClick(btn:)), for: .touchUpInside)
                cell.btnCity.addTarget(self, action: #selector(BtnCityClick(btn:)), for: .touchUpInside)
                cell.btnNeighbor.addTarget(self, action: #selector(BtnNeighborClick(btn:)), for: .touchUpInside)
                
                cell.countryTextfield.ignoreSwitchingByNextPrevious = true
                cell.cityView.ignoreSwitchingByNextPrevious = true
                
                cell.phoneNumberView.semanticContentAttribute = .forceLeftToRight
                cell.phoneNumberLabel.isHidden = true
                cell.txtPhoneNumber.semanticContentAttribute = .forceLeftToRight
                cell.txtPhoneNumber.textAlignment = .left
                cell.txtPhoneNumber.keyboardType = .asciiCapableNumberPad
                cell.countryTextfield.tag = indexPath.row
                
                cell.countryImage.image = UIImage(named: "down_arrow")?.withRenderingMode(.alwaysTemplate)
                cell.countryImage.tintColor = nejreeColor
                cell.cityImage.image = UIImage(named: "down_arrow")?.withRenderingMode(.alwaysTemplate)
                cell.cityImage.tintColor = nejreeColor
                cell.neighborhoodImage.image = UIImage(named: "down_arrow")?.withRenderingMode(.alwaysTemplate)
                cell.neighborhoodImage.tintColor = nejreeColor
                
                cell.txtPhoneNumber.placeholder = "xxxxxxxxx"
                cell.txtPhoneNumber.textColor = UIColor.lightGray
                

                
                cell.txtPhoneNumber.text = phone_number
                
//                if (phone_number != "")
//                {
//                     cell.txtPhoneNumber.text = phone_number
//                }
//                else
//                {
//                    if let mobile = defaults.value(forKey: "mobile_number") as? String {
//                        //cell.phoneNumberLabel.text = mobile
//                        cell.txtPhoneNumber.text = mobile
//                    } else {
//                        //cell.phoneNumberLabel.text = "xxxxxxxxx"
//                        cell.txtPhoneNumber.text = ""
//                    }
//                }
                
                
                
                cell.nameTextfield.textColor = UIColor.gray
//                cell.nameTextfield.placeholderColor = UIColor.gray
//                cell.nameTextfield.titleColor = UIColor.gray
//                cell.nameTextfield.selectedTitleColor = UIColor.gray
                
                cell.lName.textColor = UIColor.gray
//                cell.lName.placeholderColor = UIColor.gray
//                cell.lName.titleColor = UIColor.gray
//                cell.lName.selectedTitleColor = UIColor.gray
                
                cell.countryTextfield.textColor = UIColor.gray
//                cell.countryTextfield.placeholderColor = UIColor.gray
//                cell.countryTextfield.titleColor = UIColor.gray
//                cell.countryTextfield.selectedTitleColor = UIColor.gray
                
                cell.cityTextfield.textColor = UIColor.gray
//                cell.cityTextfield.placeholderColor = UIColor.gray
//                cell.cityTextfield.titleColor = UIColor.gray
//                cell.cityTextfield.selectedTitleColor = UIColor.gray
                
                cell.neighborhoodTextfield.textColor = UIColor.gray
//                cell.neighborhoodTextfield.placeholderColor = UIColor.gray
//                cell.neighborhoodTextfield.titleColor = UIColor.gray
//                cell.neighborhoodTextfield.selectedTitleColor = UIColor.gray
                
                cell.postcodeTextfield.textColor = UIColor.gray
                
                cell.nameTextfield.placeholder = APP_LBL().firstname_star
                cell.lName.placeholder = APP_LBL().lastname
                cell.countryTextfield.placeholder = APP_LBL().country_star
                cell.cityTextfield.placeholder = APP_LBL().city_star
                cell.neighborhoodTextfield.placeholder = APP_LBL().neighborhood_star
                cell.postcodeTextfield.placeholder = APP_LBL().postal_code
                
    //            cell.nameSideImage.roundImageAndBorderColor()
                cell.nameSideImage.layer.cornerRadius = 15.0
                cell.lNameSideImage.layer.cornerRadius = 15.0
                 if #available(iOS 11.0, *) {
                    cell.nameSideImage.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMaxYCorner]
                    cell.nameSideImage.layer.borderColor = self.nejreeColor?.cgColor
                    cell.nameSideImage.layer.borderWidth = 1.0
                    
                    cell.lNameSideImage.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMaxYCorner]
                    cell.lNameSideImage.layer.borderColor = self.nejreeColor?.cgColor
                    cell.lNameSideImage.layer.borderWidth = 1.0
                       } else {
                           // Fallback on earlier versions
                       }
              //  cell.saveButton.layer.cornerRadius = 5.0
                cell.saveButton.layer.masksToBounds = true
//                cell.saveButton.setBorder()
                
                if isFromEditAddress{
                    
                     cell.saveButton.setTitle(APP_LBL().edit_address.uppercased(), for: .normal)
                }
                else
                {
                     cell.saveButton.setTitle(APP_LBL().add_new_address.uppercased(), for: .normal)
                }
                
                //cell.saveButton.setTitle(APP_LBL().add_new_address.uppercased(), for: .normal)
                cell.saveButton.addTarget(self, action: #selector(saveButtonTapped(_:)), for: .touchUpInside)

                let _ = self.isValidDetail()
                
                if APP_DEL.selectedLanguage == Arabic {
                    cell.nameTextfield.textAlignment = .right
                    cell.countryTextfield.textAlignment = .right
                    cell.cityTextfield.textAlignment = .right
                    cell.neighborhoodTextfield.textAlignment = .right
                    cell.postcodeTextfield.textAlignment = .right
                    cell.lName.textAlignment = .right
                } else {
                    cell.nameTextfield.textAlignment = .left
                    cell.countryTextfield.textAlignment = .left
                    cell.cityTextfield.textAlignment = .left
                    cell.neighborhoodTextfield.textAlignment = .left
                    cell.postcodeTextfield.textAlignment = .left
                    cell.lName.textAlignment = .left
                }
                
                if isDefaultAddress {
                    cell.imgDefaultAddress.image = UIImage(named: "checkout_check")
                } else {
                    cell.imgDefaultAddress.image = UIImage(named: "PaymentMethodNotSelected")
                }
                
                cell.lblDefaultAddress.text = APP_LBL().set_as_default_address
                cell.btnDefaultAddress.addTarget(self, action: #selector(self.btnDefaultAddressAction(_:)), for: .touchUpInside)
                
                
                if cell.nameTextfield.text == "" {
                    cell.nameImage.image = UIImage(named: "wrongname")
                    cell.nameSideImage.tintColor = UIColor.init(hexString: "#AF1F45")
                    cell.nameView.layer.borderColor = UIColor.init(hexString: "#AF1F45")?.cgColor
                }
                else
                {
                    cell.nameImage.isHidden = false
                    let decimalCharacters = CharacterSet.decimalDigits
                    let decimalRange = cell.nameTextfield.text?.rangeOfCharacter(from: decimalCharacters)
                    if decimalRange != nil {
                        cell.nameImage.image = UIImage(named: "wrongname")
                        cell.nameSideImage.tintColor = UIColor.init(hexString: "#AF1F45")
                        cell.nameView.layer.borderColor = UIColor.init(hexString: "#AF1F45")?.cgColor
                    } else {
                        cell.nameImage.image = UIImage(named: "verified")
                        cell.nameSideImage.tintColor = self.nejreeColor
                        cell.nameView.layer.borderColor = nejreeColor?.cgColor
                    }
                }
                
                if cell.lName.text == "" {
                    cell.lNameValidImage.image = UIImage(named: "wrongname")
                    cell.lNameSideImage.tintColor = UIColor.init(hexString: "#AF1F45")
                    cell.lNameView.layer.borderColor = UIColor.init(hexString: "#AF1F45")?.cgColor
                }
                else
                {
                    let decimalCharacters = CharacterSet.decimalDigits
                    let decimalRange = cell.lName.text?.rangeOfCharacter(from: decimalCharacters)
                    if decimalRange != nil {
                        cell.lNameValidImage.image = UIImage(named: "wrongname")
                        cell.lNameSideImage.tintColor = UIColor.init(hexString: "#AF1F45")
                        cell.lNameView.layer.borderColor = UIColor.init(hexString: "#AF1F45")?.cgColor
                    } else {
                        cell.lNameValidImage.image = UIImage(named: "verified")
                        cell.lNameSideImage.tintColor = self.nejreeColor
                        cell.lNameView.layer.borderColor = nejreeColor?.cgColor
                    }
                }
                
                if cell.postcodeTextfield.text == "" {
                    cell.postcodeValidImage.image = nil//UIImage(named: "wrongname")
                    cell.postcodeSideImage.tintColor = UIColor.init(hexString: "#AF1F45")
                    //cell.postcodeView.layer.borderColor = UIColor.clear.cgColor
                    
                }
                else
                {
                    cell.postcodeValidImage.image = UIImage(named: "verified")
                    cell.postcodeSideImage.tintColor = self.nejreeColor
                    cell.postcodeView.layer.borderColor = nejreeColor?.cgColor
                }
                
                let _ = self.isValidDetail()
                
                
                cell.selectionStyle = .none
                return cell
            }
            
        case countryTableView:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "tableCell", for: indexPath)
            
            if APP_DEL.selectedLanguage == Arabic {
                //cell.textLabel?.text = "المملكة العربية السعودية"
                cell.textLabel?.textAlignment = .right
                cell.textLabel?.text = countries[indexPath.row].label_ar
            } else {
                //cell.textLabel?.text = "Saudi Arabia"
                cell.textLabel?.textAlignment = .left
                cell.textLabel?.text = countries[indexPath.row].label
            }
            
            if self.selectedCountry?.country_code == countries[indexPath.row].country_code{
                
                cell.textLabel?.textColor = UIColor.init(hexString: "#FCB215")//.white
            }
            else{
                cell.textLabel?.textColor = UIColor.black
            }
            
            
            
            
            cell.textLabel?.font = UIFont(name: "Cairo-Regular", size: 16)
            return cell
        case cityTableView:
            let cell = tableView.dequeueReusableCell(withIdentifier: "tableCell", for: indexPath)
            cell.textLabel?.text = cities[indexPath.row].label
            
            if APP_DEL.selectedLanguage == Arabic {
                cell.textLabel?.textAlignment = .right
            } else {
                cell.textLabel?.textAlignment = .left
            }
            cell.textLabel?.font = UIFont(name: "Cairo-Regular", size: 16)
            return cell
        case neighbourTableView:
            let cell = tableView.dequeueReusableCell(withIdentifier: "NtableCell", for: indexPath)

            if APP_DEL.selectedLanguage == Arabic {
                cell.textLabel?.textAlignment = .right
                 cell.textLabel?.text = neighbours[indexPath.row].arabic_value
            } else {
                cell.textLabel?.textAlignment = .left
                 cell.textLabel?.text = neighbours[indexPath.row].label
            }
            cell.textLabel?.font = UIFont(name: "Cairo-Regular", size: 16)
            return cell
        default:
            return UITableViewCell()
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch tableView {
        case addressTable:
            if (indexPath.section == 0){
                return UITableView.automaticDimension
            } else {
                return 560
            }
        case countryTableView:
            return 50
        case cityTableView:
            return 50
        case neighbourTableView:
            return 50
        default:
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
//        if (indexPath.section == 0){
//            return;
//        }
        
        let cell = addressTable.cellForRow(at: IndexPath(row: 0, section: 1)) as! addNewAddressCell
        switch tableView {
            
        case countryTableView:

            self.viewToShow.removeFromSuperview()
  
            
            if APP_DEL.selectedLanguage == Arabic{
                cell.countryTextfield.text = countries[indexPath.row].label_ar
            }
            else{
                cell.countryTextfield.text = countries[indexPath.row].label
            }
            
            
            phone_number = ""
            
            self.viewToShow.removeFromSuperview()
            self.selectedLocation["country"] = countries[indexPath.row].country_code
            self.selectedCountry = countries[indexPath.row]
            self.addressTable.reloadData()
            self.cities.removeAll()
            self.neighbours.removeAll()
            self.getCity()
            self.countries = self.tempCountries
            let _ = self.isValidDetail()
            
        case cityTableView:
            
            self.viewToShow.removeFromSuperview()
  
            cell.cityTextfield.text = cities[indexPath.row].label
            self.selectedLocation["city"] =  cities[indexPath.row].label
            self.selectedLocation["city_id"] =  cities[indexPath.row].value

            self.getNeighbours(city: cities[indexPath.row].value!)
            
            cities = tempCities
            
            let _ = self.isValidDetail()
            
        case neighbourTableView:
            //cell.neighborhoodTextfield.text = neighbours[indexPath.row].label
            self.viewToShow.removeFromSuperview()
  
            if APP_DEL.selectedLanguage == Arabic {
                cell.neighborhoodTextfield.text = neighbours[indexPath.row].arabic_value
            } else {
                cell.neighborhoodTextfield.text = neighbours[indexPath.row].label
            }
            self.selectedLocation["neighbour"] = neighbours[indexPath.row].label
            self.selectedLocation["neighbour_id"] =  neighbours[indexPath.row].value

            neighbours = tempNeighbours
            
            let _ = self.isValidDetail()
            
        default:
            print("hdsfghasdfghasdgfhasdgfhsadgfads")
        }
    }
}

extension nejreeAddAddressController: UITextFieldDelegate {
    
    
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    
        
        let cell = addressTable.cellForRow(at: IndexPath(row: 0, section: 1)) as! addNewAddressCell
        
        
        if (textField.accessibilityIdentifier ?? "") == "txtPhoneNumber" {
            
            guard !string.isEmpty else {

                return true
            }
            
            if (CharacterSet(charactersIn: "0123456789").isSuperset(of: CharacterSet(charactersIn: string))) {

                if let text = textField.text, let range = Range(range, in: text) {

                    let proposedText = text.replacingCharacters(in: range, with: string)

                    if (selectedCountry?.country_code?.uppercased() == "SA"){
                        if proposedText.first == "5" && proposedText.count <= (Int(selectedCountry?.phone_number_limit ?? "") ?? 9) {
                            return true
                        }
                    } else {
                        if proposedText.count <= (Int(selectedCountry?.phone_number_limit ?? "") ?? 9) {
                            return true
                        }
                    }
                }
            }

            return false
        }
        else if (textField == cell.nameTextfield || textField == cell.lName){
            
           if let x = string.rangeOfCharacter(from: NSCharacterSet.decimalDigits) {
              return false
           } else {
           return true
           }

        } else if ((textField.accessibilityIdentifier ?? "") == "localSearchCountry") {
            
            if let str = textField.text, let swtRange = Range(range, in: str) {
                
                let fullString = str.replacingCharacters(in: swtRange, with: string)
                self.displayClear(res: (fullString.count > 0))
            
                if fullString == "" {
                    self.countries = self.tempCountries
                } else {
                    if APP_DEL.selectedLanguage == Arabic{
                        self.countries = self.tempCountries.filter { (($0.label_ar ?? "")).range(of: fullString, options: [.diacriticInsensitive, .caseInsensitive]) != nil }
                    }
                    else
                    {
                        self.countries = self.tempCountries.filter { (($0.label ?? "")).range(of: fullString, options: [.diacriticInsensitive, .caseInsensitive]) != nil }
                    }
                }
                
                self.countryTableView.reloadData()
            }
            
        } else if ((textField.accessibilityIdentifier ?? "") == "localSearchCity") {
            
            if let str = textField.text, let swtRange = Range(range, in: str) {
                
                let fullString = str.replacingCharacters(in: swtRange, with: string)
                self.displayClear(res: (fullString.count > 0))
            
                if fullString == "" {
                    self.cities = self.tempCities
                } else {
                    self.cities = self.tempCities.filter { (($0.label ?? "")).range(of: fullString, options: [.diacriticInsensitive, .caseInsensitive]) != nil }
                }
                
                self.cityTableView.reloadData()
            }
            
        } else if ((textField.accessibilityIdentifier ?? "") == "localSearchNeighbours") {
                        
            if let str = textField.text, let swtRange = Range(range, in: str) {
                
                let fullString = str.replacingCharacters(in: swtRange, with: string)
                self.displayClear(res: (fullString.count > 0))
            
                if fullString == "" {
                    self.neighbours = self.tempNeighbours
                } else {
                    
                    if APP_DEL.selectedLanguage == Arabic {
                        self.neighbours = self.tempNeighbours.filter { (($0.arabic_value ?? "")).range(of: fullString, options: [.diacriticInsensitive, .caseInsensitive]) != nil }
                    } else {
                        self.neighbours = self.tempNeighbours.filter { (($0.label ?? "")).range(of: fullString, options: [.diacriticInsensitive, .caseInsensitive]) != nil }
                    }
                                        
                }
                
                self.neighbourTableView.reloadData()
            }
        }
        else if (textField == cell.postcodeTextfield) {
                    if let text = textField.text, let range = Range(range, in: text) {

                        let proposedText = text.replacingCharacters(in: range, with: string)
                        if (proposedText.count > 10) {
                            return false
                        }
                    }
                    
        //           if (CharacterSet(charactersIn: "0123456789").isSuperset(of: CharacterSet(charactersIn: string))) {
        //              return true
        //           } else {
        //           return false
        //           }

                }
        
        return true
    }
    
    
    
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        let cell = addressTable.cellForRow(at: IndexPath(row: 0, section: 1)) as! addNewAddressCell
        let str = textField.text!
        
        switch textField {
        case cell.nameTextfield:
            
            if cell.nameTextfield.text == "" {
                cell.nameImage.image = UIImage(named: "wrongname")
                cell.nameSideImage.tintColor = UIColor.init(hexString: "#AF1F45")
                cell.nameView.layer.borderColor = UIColor.init(hexString: "#AF1F45")?.cgColor
                return}
            
            cell.nameImage.isHidden = false
            FirstName = cell.nameTextfield.text ?? ""
            let decimalCharacters = CharacterSet.decimalDigits
            let decimalRange = str.rangeOfCharacter(from: decimalCharacters)
            if decimalRange != nil {
                cell.nameImage.image = UIImage(named: "wrongname")
                cell.nameSideImage.tintColor = UIColor.init(hexString: "#AF1F45")
                cell.nameView.layer.borderColor = UIColor.init(hexString: "#AF1F45")?.cgColor
            } else {
                cell.nameImage.image = UIImage(named: "verified")
                cell.nameSideImage.tintColor = self.nejreeColor
                cell.nameView.layer.borderColor = nejreeColor?.cgColor
            }
            
        case cell.lName:
            if cell.lName.text == "" {
                cell.lNameValidImage.image = UIImage(named: "wrongname")
                cell.lNameSideImage.tintColor = UIColor.init(hexString: "#AF1F45")
                cell.lNameView.layer.borderColor = UIColor.init(hexString: "#AF1F45")?.cgColor
                return}
            LastName = cell.lName.text ?? ""
            
            let decimalCharacters = CharacterSet.decimalDigits
            let decimalRange = str.rangeOfCharacter(from: decimalCharacters)
            if decimalRange != nil {
                cell.lNameValidImage.image = UIImage(named: "wrongname")
                cell.lNameSideImage.tintColor = UIColor.init(hexString: "#AF1F45")
                cell.lNameView.layer.borderColor = UIColor.init(hexString: "#AF1F45")?.cgColor
            } else {
                cell.lNameValidImage.image = UIImage(named: "verified")
                cell.lNameSideImage.tintColor = self.nejreeColor
                cell.lNameView.layer.borderColor = nejreeColor?.cgColor
            }
            
        case cell.postcodeTextfield:
            
            if cell.postcodeTextfield.text == "" {
                cell.postcodeValidImage.image = nil//UIImage(named: "wrongname")
                cell.postcodeSideImage.tintColor = UIColor.init(hexString: "#AF1F45")
                cell.postcodeView.layer.borderColor = UIColor.init(hexString: "#AF1F45")?.cgColor
            }
            else
            {
                cell.postcodeValidImage.image = UIImage(named: "verified")
                cell.postcodeSideImage.tintColor = self.nejreeColor
                cell.postcodeView.layer.borderColor = nejreeColor?.cgColor
                Postcode = cell.postcodeTextfield.text ?? ""
            }
        
        case cell.txtPhoneNumber:
        
            let _ = self.isValidDetail()
            
        case cell.countryTextfield:
        
            let _ = self.isValidDetail()
            
        case cell.cityTextfield:
            
            let _ = self.isValidDetail()
            
        case cell.neighborhoodTextfield:
            
            let _ = self.isValidDetail()
            
        case cell.postcodeTextfield:
            
            let _ = self.isValidDetail()
            
        default:
            print(textField.text!)
        }
        
        
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        let cell = addressTable.cellForRow(at: IndexPath(row: 0, section: 1)) as! addNewAddressCell
        if textField == cell.cityTextfield {
            if cell.countryTextfield.text != "" {
            createTable(tableName: "City");textField.endEditing(true);
            }
        }
        if textField == cell.neighborhoodTextfield
        {
            createTable(tableName: "Neighbourhood");
            textField.endEditing(true);
            
        }
        if textField == cell.countryTextfield {
            
             let cell = addressTable.cellForRow(at: IndexPath(row: 0, section: 1)) as! addNewAddressCell
           // let cellLastName = addressTable.cellForRow(at: IndexPath(row: 1, section: 0)) as! addNewAddressCell
            
            addressTable.endEditing(true)
            
            cell.countryTextfield.endEditing(true)
            cell.nameTextfield.endEditing(true)
            cell.lName.endEditing(true)
            
            textField.resignFirstResponder()
            self.view.endEditing(true)
            createTable(tableName: "Country");
            textField.endEditing(true);
            
        }
    }
}

extension nejreeAddAddressController: UIGestureRecognizerDelegate {
    
    
    func createTable(tableName: String) {
        self.viewToShow.removeFromSuperview()

        switch tableName {
            
        case "Country":
            
            viewToShow = UIView.init(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height))
            viewToShow.backgroundColor = UIColor.black.withAlphaComponent(0.5)
            self.view.addSubview(viewToShow)
            countryTableView = UITableView.init(frame: CGRect(x: 0, y: 0, width: viewToShow.frame.width - 80, height: 350))
            countryTableView.register(UITableViewCell.self, forCellReuseIdentifier: "tableCell")
            countryTableView.delegate = self
            countryTableView.dataSource = self
            countryTableView.center = self.viewToShow.center
            countryTableView.layer.cornerRadius = 10.0
            let headerView = UIView.init(frame: CGRect(x: 0, y: 0, width: countryTableView.frame.width, height: 100))
            headerView.backgroundColor = UIColor.black//UIColor.init(hexString: "#FCB215")
            let label = UILabel.init(frame: CGRect(x: 0, y: 0, width: headerView.frame.width, height: 50))
            label.text = APP_LBL().select_your_country
            label.textAlignment = .center
            label.textColor = UIColor.init(hexString: "#FCB215")//.white
            label.font = UIFont(name: "Cairo-Bold", size: 18)
            headerView.addSubview(label)
            
            let searchView = UIView(frame: CGRect(x: 15, y: label.frame.height + 8, width: countryTableView.frame.width - 30, height: 40))
            searchView.backgroundColor = .white
            searchView.layer.cornerRadius = 20
            searchView.clipsToBounds = true
            searchView.layer.borderColor = UIColor.darkGray.cgColor
            searchView.layer.borderWidth = 0.5
            txtSearch = UITextField(frame: CGRect(x: 10, y: 0, width: searchView.frame.width - 20, height: searchView.frame.height))
            txtSearch.autocorrectionType = .no
            txtSearch.accessibilityIdentifier = "localSearchCountry"
            txtSearch.borderStyle = .none
            txtSearch.font = UIFont(name: "Cairo-Regular", size: 16)

            if APP_DEL.selectedLanguage == Arabic {
            txtSearch.textAlignment = .right
              btnSearchClear = UIButton(frame: CGRect(x: 10, y: 0, width: txtSearch.frame.height, height: txtSearch.frame.height))

            }else{
                txtSearch.textAlignment = .left
              btnSearchClear = UIButton(frame: CGRect(x: txtSearch.frame.width - txtSearch.frame.height + 10, y: 0, width: txtSearch.frame.height, height: txtSearch.frame.height))

            }
            
            txtSearch.placeholder = APP_LBL().search
            txtSearch.delegate = self
            //btnSearchClear = UIButton(frame: CGRect(x: txtSearch.frame.width - txtSearch.frame.height + 10, y: 0, width: txtSearch.frame.height, height: txtSearch.frame.height))
            btnSearchClear.isHidden = true
            btnSearchClear.setImage(UIImage(named: "close"), for: .normal)
            btnSearchClear.contentEdgeInsets = UIEdgeInsets(top: 13, left: 13, bottom: 13, right: 13)
            btnSearchClear.accessibilityIdentifier = "localSearchCountry"
            btnSearchClear.addTarget(self, action: #selector(self.btnClearTapped(btn:)), for: .touchUpInside)
            let whiteView = UIView(frame: CGRect(x: 0, y: label.frame.height, width: countryTableView.frame.width, height: 50))
            whiteView.backgroundColor = .white
            headerView.addSubview(whiteView)
            searchView.addSubview(txtSearch)
            searchView.addSubview(btnSearchClear)
            headerView.addSubview(searchView)
            
            countryTableView.tableHeaderView = headerView
            headerView.layoutIfNeeded()
            viewToShow.addSubview(countryTableView)
            viewToShow.addGestureRecognizer(tapGesture)
            
        case "City":
            viewToShow = UIView.init(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height))
            viewToShow.backgroundColor = UIColor.black.withAlphaComponent(0.5)
            self.view.addSubview(viewToShow)
            cityTableView = UITableView.init(frame: CGRect(x: 0, y: 0, width: viewToShow.frame.width - 80, height: 350))
            cityTableView.register(UITableViewCell.self, forCellReuseIdentifier: "tableCell")
            cityTableView.delegate = self
            cityTableView.dataSource = self
            cityTableView.center = self.viewToShow.center
            cityTableView.layer.cornerRadius = 10.0
            let headerView = UIView.init(frame: CGRect(x: 0, y: 0, width: cityTableView.frame.width, height: 100))
            headerView.backgroundColor = UIColor.black//UIColor.init(hexString: "#FCB215")
            let label = UILabel.init(frame: CGRect(x: 0, y: 0, width: headerView.frame.width, height: 50))
            label.text = APP_LBL().select_city
            label.textAlignment = .center
            label.textColor = UIColor.init(hexString: "#FCB215")//.white
            label.font = UIFont(name: "Cairo-Bold", size: 18)
            headerView.addSubview(label)
            
            let searchView = UIView(frame: CGRect(x: 15, y: label.frame.height + 8, width: cityTableView.frame.width - 30, height: 40))
            searchView.backgroundColor = .white
            searchView.layer.cornerRadius = 20
            searchView.clipsToBounds = true
            searchView.layer.borderColor = UIColor.darkGray.cgColor
            searchView.layer.borderWidth = 0.5
            txtSearch = UITextField(frame: CGRect(x: 10, y: 0, width: searchView.frame.width - 20, height: searchView.frame.height))
            txtSearch.autocorrectionType = .no
            txtSearch.accessibilityIdentifier = "localSearchCity"
            txtSearch.borderStyle = .none
            txtSearch.font = UIFont(name: "Cairo-Regular", size: 16)

            if APP_DEL.selectedLanguage == Arabic {
            txtSearch.textAlignment = .right
              btnSearchClear = UIButton(frame: CGRect(x: 10, y: 0, width: txtSearch.frame.height, height: txtSearch.frame.height))

            }else{
                txtSearch.textAlignment = .left
              btnSearchClear = UIButton(frame: CGRect(x: txtSearch.frame.width - txtSearch.frame.height + 10, y: 0, width: txtSearch.frame.height, height: txtSearch.frame.height))

            }
            
            txtSearch.placeholder = APP_LBL().search
            txtSearch.delegate = self
            //btnSearchClear = UIButton(frame: CGRect(x: txtSearch.frame.width - txtSearch.frame.height + 10, y: 0, width: txtSearch.frame.height, height: txtSearch.frame.height))
            btnSearchClear.isHidden = true
            btnSearchClear.setImage(UIImage(named: "close"), for: .normal)
            btnSearchClear.contentEdgeInsets = UIEdgeInsets(top: 13, left: 13, bottom: 13, right: 13)
            btnSearchClear.accessibilityIdentifier = "localSearchCity"
            btnSearchClear.addTarget(self, action: #selector(self.btnClearTapped(btn:)), for: .touchUpInside)
            let whiteView = UIView(frame: CGRect(x: 0, y: label.frame.height, width: cityTableView.frame.width, height: 50))
            whiteView.backgroundColor = .white
            headerView.addSubview(whiteView)
            searchView.addSubview(txtSearch)
            searchView.addSubview(btnSearchClear)
            headerView.addSubview(searchView)
            
            cityTableView.tableHeaderView = headerView
            headerView.layoutIfNeeded()
            viewToShow.addSubview(cityTableView)
            viewToShow.addGestureRecognizer(tapGesture)
            
        case "Neighbourhood":
            viewToShow = UIView.init(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height))
            viewToShow.backgroundColor = UIColor.black.withAlphaComponent(0.5)
            self.view.addSubview(viewToShow)
            neighbourTableView = UITableView.init(frame: CGRect(x: 0, y: 0, width: viewToShow.frame.width - 80, height: 350))
            neighbourTableView.register(UITableViewCell.self, forCellReuseIdentifier: "NtableCell")
            neighbourTableView.delegate = self
            neighbourTableView.dataSource = self
            neighbourTableView.center = self.viewToShow.center
            neighbourTableView.layer.cornerRadius = 10.0
            let headerView = UIView.init(frame: CGRect(x: 0, y: 0, width: neighbourTableView.frame.width, height: 100))
            headerView.backgroundColor = UIColor.black//UIColor.init(hexString: "#FCB215")
            let label = UILabel.init(frame: CGRect(x: 0, y: 0, width: headerView.frame.width, height: 50))
            label.text = APP_LBL().select_neighborhood
            label.textAlignment = .center
            label.textColor = UIColor.init(hexString: "#FCB215")//.white
            label.font = UIFont(name: "Cairo-Bold", size: 18)
            headerView.addSubview(label)
            
            let searchView = UIView(frame: CGRect(x: 15, y: label.frame.height + 8, width: neighbourTableView.frame.width - 30, height: 40))
            searchView.backgroundColor = .white
            searchView.layer.cornerRadius = 20
            searchView.clipsToBounds = true
            searchView.layer.borderColor = UIColor.darkGray.cgColor
            searchView.layer.borderWidth = 0.5
            txtSearch = UITextField(frame: CGRect(x: 10, y: 0, width: searchView.frame.width - 20, height: searchView.frame.height))
            txtSearch.autocorrectionType = .no
            txtSearch.accessibilityIdentifier = "localSearchNeighbours"
            txtSearch.borderStyle = .none
            txtSearch.font = UIFont(name: "Cairo-Regular", size: 16)

            if APP_DEL.selectedLanguage == Arabic {
            txtSearch.textAlignment = .right
              btnSearchClear = UIButton(frame: CGRect(x: 10, y: 0, width: txtSearch.frame.height, height: txtSearch.frame.height))

            }else{
                txtSearch.textAlignment = .left
              btnSearchClear = UIButton(frame: CGRect(x: txtSearch.frame.width - txtSearch.frame.height + 10, y: 0, width: txtSearch.frame.height, height: txtSearch.frame.height))

            }
            
            txtSearch.placeholder = APP_LBL().search
            txtSearch.delegate = self
            //btnSearchClear = UIButton(frame: CGRect(x: txtSearch.frame.width - txtSearch.frame.height + 10, y: 0, width: txtSearch.frame.height, height: txtSearch.frame.height))
            btnSearchClear.isHidden = true
            btnSearchClear.setImage(UIImage(named: "close"), for: .normal)
            btnSearchClear.contentEdgeInsets = UIEdgeInsets(top: 13, left: 13, bottom: 13, right: 13)
            btnSearchClear.accessibilityIdentifier = "localSearchNeighbours"
            btnSearchClear.addTarget(self, action: #selector(self.btnClearTapped(btn:)), for: .touchUpInside)
            let whiteView = UIView(frame: CGRect(x: 0, y: label.frame.height, width: neighbourTableView.frame.width, height: 50))
            whiteView.backgroundColor = .white
            headerView.addSubview(whiteView)
            searchView.addSubview(txtSearch)
            searchView.addSubview(btnSearchClear)
            headerView.addSubview(searchView)
            
            neighbourTableView.tableHeaderView = headerView
            headerView.layoutIfNeeded()
            viewToShow.addSubview(neighbourTableView)
            viewToShow.addGestureRecognizer(tapGesture)
            
        default:
            print("ghasdfhagsdajdghs")
        }
    }
    
    @objc func dismissPopup(_ sender: UITapGestureRecognizer) {
        
        self.countries = self.tempCountries
        cities = tempCities
        neighbours = tempNeighbours
        
        viewToShow.removeFromSuperview()
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if (touch.view?.isDescendant(of: cityTableView))! || (touch.view?.isDescendant(of: neighbourTableView))! || (touch.view?.isDescendant(of: countryTableView))!{
            return false
        }
        return true
    }

}

extension UIView {
    func roundCornersWithShadow() {
        self.layer.masksToBounds = false
        //        self.clipsToBounds = true
        self.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.3
        self.layer.cornerRadius = 15.0
        self.layer.borderWidth = 1.0
        self.layer.borderColor = UIColor.init(hexString: "#FBAD18")?.cgColor
        if #available(iOS 11.0, *) {
            self.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMinYCorner]
        } else {
            // Fallback on earlier versions
        }
    }
}
