//
//  addNewAddressCell.swift
//  MageNative Magento Platinum
//
//  Created by MacMini on 22/08/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class addNewAddressCell: UITableViewCell {

  
    
    @IBOutlet weak var newAddressLabel: UILabel!
    
    @IBOutlet weak var nameView: UIView!
    @IBOutlet weak var nameTextfield: UITextField!
    @IBOutlet weak var nameImage: UIImageView!
    @IBOutlet weak var nameSideImage: UIImageView!
    
    
    @IBOutlet weak var phoneNumberView: UIView!
    @IBOutlet weak var countryCodeLabel: UILabel!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    @IBOutlet weak var txtPhoneNumber: UITextField!
    
    @IBOutlet weak var lNameView: UIView!
    @IBOutlet weak var lName: UITextField!
    @IBOutlet weak var lNameValidImage: UIImageView!
    @IBOutlet weak var lNameSideImage: UIImageView!
    
    
    @IBOutlet weak var countryView: UIView!
    @IBOutlet weak var countryTextfield: UITextField!
    @IBOutlet weak var countryImage: UIImageView!
    
    
    @IBOutlet weak var cityView: UIView!
    @IBOutlet weak var cityTextfield: UITextField!
    @IBOutlet weak var cityImage: UIImageView!
    
    @IBOutlet weak var neighborhoodView: UIView!
    @IBOutlet weak var neighborhoodTextfield: UITextField!
    @IBOutlet weak var neighborhoodImage: UIImageView!
    
    @IBOutlet weak var postcodeView: UIView!
    @IBOutlet weak var postcodeTextfield: UITextField!
    @IBOutlet weak var postcodeValidImage: UIImageView!
    @IBOutlet weak var postcodeSideImage: UIImageView!
    
    @IBOutlet weak var saveButton: UIButton!
    
    @IBOutlet weak var btnCountry: UIButton!
    @IBOutlet weak var btnCity: UIButton!
    @IBOutlet weak var btnNeighbor: UIButton!
    
    @IBOutlet weak var imgDefaultAddress: UIImageView!
    @IBOutlet weak var lblDefaultAddress: UILabel!
    @IBOutlet weak var btnDefaultAddress: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        lblDefaultAddress.font =  UIFont(name: "Cairo-Regular", size: 14)
        
        
        txtPhoneNumber.keyboardType = .asciiCapableNumberPad
    }

    

}
