//
//  NejreeBannerVC.swift
//  MageNative Magento Platinum
//
//  Created by vishal.n on 01/07/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit
import IQKeyboardManager
import FirebaseAnalytics
import SKPhotoBrowser
import StoreKit

let APP_ID = "1454057624"

let MENU_HT : CGFloat = 40.0
var IS_FEATURED_ENABLED: Bool = true
var minimumReviewWorthyActionCount = 4
var codTotalToDiableOption : Int = 800

enum LayerType : String {
    
    case None = ""
    case Image = "1"
    case Text = "2"
    case Multiple = "3"
    case Counter = "4"
    case ProductScroll = "5"
}

enum LayerOption : String {
    
    case None = ""
    case NoLayer = "0"
    case FullImage = "1"
    case SpecificProduct = "2"
    case SpecificCategory = "3"
    case GiftCard = "4"
    case ShopInShop = "6"
    case Product = "7"
    case NoClickableFullImage = "8"
    case ScrollableFullImage = "9"
    case productWithCategory = "11"
}


struct BannerLayer : Codable {
    
    let layer_id : String?
    let layer_name : String?
    let store_id : String?
    let category_id : String?
    let layer_ratio : String?
    let layer_sort_order : String?
    let layer_timer : String?
    let layer_type : String?
    let layer_option : String?
    let layer_height : String?
    let layer_width : String?
    let layer_item_space : String?
    let layer_top_space : String?
    let layer_bottom_space  : String?
    let server_time : String?
    let start_date : String?
    let cat_id : String?
    let end_date : String?
    let count : Int?
    let vcategory_id : String?
    let limit : String?
    let category : String?
    let product_data : [Product_data]?
    let layer_data : [Layer_data]?
    
    var elapsedTime : Int = 0
    
    enum CodingKeys: String, CodingKey {
        
        case layer_id = "layer_id"
        case layer_name = "layer_name"
        case store_id = "store_id"
        case category_id = "category_id"
        case layer_ratio = "layer_ratio"
        case layer_sort_order = "layer_sort_order"
        case layer_timer = "layer_timer"
        case layer_type = "layer_type"
        case layer_option = "layer_option"
        case layer_height = "layer_height"
        case layer_width = "layer_width"
        case layer_item_space = "layer_item_space"
        case layer_top_space = "layer_top_space"
        case layer_bottom_space  = "layer_bottom_space "
        case server_time = "server_time"
        case start_date = "start_date"
        case cat_id = "cat_id"
        case end_date = "end_date"
        case count = "count"
        case product_data = "product_data"
        case layer_data = "layer_data"
        case vcategory_id = "vcategory_id"
        case limit = "limit"
        case category = "category"
        
        
        
        
        
    }
}

struct Layer_data : Codable {
    
    let layer_type : String?
    let layer_option : String?
    let banner_image : String?
    let full_image : String?
    let category_id : String?
    let category : String?
    let product_id : String?
    let text_description : String?
    let tags : String?
    
    enum CodingKeys: String, CodingKey {
        
        case layer_type = "layer_type"
        case layer_option = "layer_option"
        case banner_image = "banner_image"
        case full_image = "full_image"
        case category_id = "category_id"
        case product_id = "product_id"
        case text_description = "text_description"
        case tags = "tags"
        case category = "category"
    }
}

struct Product_data : Codable {
    
    let product_id : String?
    let product_name : String?
    let product_image : String?
    let brands_name : String?
    let special_price : String?
    let regular_price : String?
    
    let description : String?
    let product_url : String?
    let offer : String?
    let wishlist_item_id : String?
    let Inwishlist : String?
    let source_id : String?
    let position : String?
    let stock_status : String?
    let type : String?
    let tag : String?
    
    
    
    
    enum CodingKeys: String, CodingKey {
        
        case product_id = "product_id"
        case product_name = "product_name"
        case product_image = "product_image"
        case brands_name = "brands_name"
        case special_price = "special_price"
        case regular_price = "regular_price"
        
        
        case description = "description"
        case product_url = "product-url"
        case offer = "offer"
        case wishlist_item_id = "wishlist_item_id"
        case Inwishlist = "Inwishlist"
        case source_id = "source_id"
        case position = "position"
        case stock_status = "stock_status"
        case type = "type"
        case tag = "tag"
    }
}

protocol DidSelectBanner {
    func didSelectBanner(index: IndexPath)
}

class NejreeBannerVC: UIViewController, UpdateHome, DidSelectBanner {
    
    @IBOutlet weak var viewNotificationBottom: UIView!
    @IBOutlet weak var lblNotificCountBottom: UILabel!

    @IBOutlet weak var viewCategory: UIView!
    @IBOutlet weak var collCategory: UICollectionView!
    
    @IBOutlet weak var viewSubVC: UIView!
    @IBOutlet weak var collView: UICollectionView!
    @IBOutlet weak var lblNodata: UILabel!
    
    @IBOutlet weak var viewSearchBottom: UIView!
    @IBOutlet weak var stackBottomHT: NSLayoutConstraint!
    
    var suggentionsArray = [[String:String]]()
    
    var isLOGIN = false
    var categoryImage = ""
    var isFirstTime = true
    var lastCategoryID = ""
    var homeCategoryId = ""

    
    var arrLayer : [BannerLayer] = []
    
    var arrProducts: [product] = []
    var noProduct = false
    var currentPage = 1
    
    var arrCounterSection : [Int] = []
    var timer: Timer?
    
    var activityIndi = UIActivityIndicatorView()
    var isSearchOpen : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        APP_DEL.delegateUpdateHome = self
        
        if UserDefaults.standard.object(forKey: KEY_GENDER) == nil {
            
            let window = UIApplication.shared.keyWindow!
            let test = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DummySplashVC") as! DummySplashVC
            dummySplash = test.view!
            dummySplash.frame = window.bounds
            window.addSubview(dummySplash);
        }
        
        if UserDefaults.standard.bool(forKey: "isLogin") {
            isLOGIN = false
        }
        
        setUI()
        
        self.getAllCategory()
        
        
        activityIndi.frame = CGRect(x: 0.0, y: 0.0, width: 40.0, height: 40.0)
        activityIndi.center = self.view.center
        activityIndi.hidesWhenStopped = true
        activityIndi.style =
            UIActivityIndicatorView.Style.gray
        //  self.view.addSubview(activityIndi)
        // activityIndi.startAnimating()
        activityIndi.isHidden = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(checkDeeplinkFromFirebase), name: NSNotification.Name(rawValue: "checkDeeplinkFromFirebase"), object: nil)
        APP_DEL.isAppLaunch = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(checkDeeplinkFromFirebase), name: NSNotification.Name(rawValue: "checkDeeplinkFromFirebaseCategory"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(checkNotificationUpdate), name: NSNotification.Name(rawValue: "checkForNotificationUpdate"), object: nil)
        
        setupHideKeyboardOnTap()
    }
    
    func requestReviewIfAppropriate() {
        let defaults = UserDefaults.standard
        let actionCount = defaults.integer(forKey: "appOpenCount")
        guard actionCount >= minimumReviewWorthyActionCount else {
            return
        }
        
        
        if actionCount >= minimumReviewWorthyActionCount{
            SKStoreReviewController.requestReview()
            defaults.set(0, forKey: "appOpenCount")
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.tabBarController?.tabBar.isHidden = false
        self.requestReviewIfAppropriate()
        
        
        self.updateNotificationCount()
        
        if UserDefaults.standard.bool(forKey: "isLogin") {
            isLOGIN = true
            
            //
            //            APP_DEL.getUnreadCount { (res) in
            //
            //               self.updateNotificationCount()
            //           }
            
            
        }
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.tabBarController?.tabBar.isHidden = false
        
        self.stackBottomHT.constant = (44 + 15)
        
        IQKeyboardManager.shared().isEnabled = false
        IQKeyboardManager.shared().isEnableAutoToolbar = false
        
        if isFromWishList != "" {
            
            let vc = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "productViewController") as! productViewController
            vc.product_id = isFromWishList
            APP_DEL.productIDglobal = isFromWishList
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        
        
        if APP_DEL.strDeepLinkData != ""{
            
            
            APP_DEL.DeeplinkForward(APP_DEL.strDeepLinkData)
            
            
        }
        
        if (APP_DEL.strSignUpfromGiftcard != ""){
            
            
            
            let vc = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "NejreeGiftcardVC") as! NejreeGiftcardVC
            vc.category_id = APP_DEL.strSignUpfromGiftcard
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
            
            APP_DEL.strSignUpfromGiftcard = ""
            
        }
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(willEnterForeground), name: UIApplication.willEnterForegroundNotification, object: nil)
        
        willEnterForeground()
        
        
        
    }
    
    @objc func checkNotificationUpdate(){
        
        self.updateNotificationCount()
    }
    
    @objc func checkDeeplinkFromFirebase(){
        
        if isDisplayHomeCategory {
            APP_DEL.delegateUpdateCategory?.updateCategory(res: true)
        }
        
        
        self.arrCounterSection = []
        self.checkNoData()
        
        self.lblNodata.isHidden = true
        
        self.collView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        
        
        //self.getAllCategory()
    }
    
    
    @objc func willEnterForeground() {
        
        if isCheckForUpdate && isForcefullyUpdate {
            
            let alert = UIAlertController(title: "Nejree", message: APP_LBL().please_update_new_version_from_the_store, preferredStyle: .alert)
            let ok = UIAlertAction(title: APP_LBL().update_now, style: .default) { (ok) in
                
                alert.dismiss(animated: true, completion: nil)
                
                DispatchQueue.main.async {
                    
                    if let url = URL(string: "itms-apps://itunes.apple.com/app/id" + APP_ID )
                    {
                        if #available(iOS 10.0, *) {
                            
                            UIApplication.shared.open(url, options: [:]) { (opend) in
                                print("version Popup:- ",opend)
                            }
                            
                        } else {
                            
                            if UIApplication.shared.canOpenURL(url as URL) {
                                UIApplication.shared.openURL(url as URL)
                            } else {
                                print("Can not open")
                            }
                        }
                        
                    } else {
                        
                        //Just check it on phone not simulator!
                        print("Can not open")
                    }
                }
            }
            alert.addAction(ok)
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        //  self.navigationController?.setNavigationBarHidden(false, animated: false)
        
        IQKeyboardManager.shared().isEnabled = true
        IQKeyboardManager.shared().isEnableAutoToolbar = true
        
        //self.tblView.removeObserver(self, forKeyPath: "contentSize", context: nil)
    }
    
    func setUI() {
        viewSearchBottom.superview?.isHidden = true // forcefully hide search button for now

        lblNotificCountBottom.round(redius: 7.5)
        
        self.lblNodata.text = APP_LBL().no_data_found
        self.lblNodata.isHidden = true
        
            
        self.viewCategory.isHidden = !(isDisplayHomeCategory)
        self.viewCategory.alpha = (isDisplayHomeCategory ? 1 : 0)
        
        collCategory.register(UINib(nibName: "LayerHeaderCell", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "LayerHeaderCell")
        collCategory.register(UINib(nibName: "LayerHeaderWhiteCell", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "LayerHeaderWhiteCell")
        collCategory.register(UINib(nibName: "LayerHeaderWhiteCell", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: "LayerHeaderWhiteCell")
        collCategory.register(UINib(nibName: "CategoryMainCell", bundle: nil), forCellWithReuseIdentifier: "CategoryMainCell")
        collCategory.delegate = self
        collCategory.dataSource = self
        
        
        
        collView.register(UINib(nibName: "CatProductCell", bundle: nil), forCellWithReuseIdentifier: "CatProductCell")
        collView.register(UINib(nibName: "LayerHeaderCell", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "LayerHeaderCell")
        collView.register(UINib(nibName: "LayerHeaderWhiteCell", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "LayerHeaderWhiteCell")
        collView.register(UINib(nibName: "LayerHeaderWhiteCell", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: "LayerHeaderWhiteCell")
        collView.register(UINib(nibName: "LayerHeaderViewAllCell", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "LayerHeaderViewAllCell")
                
        collView.register(UINib(nibName: "LayerCell", bundle: nil), forCellWithReuseIdentifier: "LayerCell")
        collView.register(UINib(nibName: "LayerTextCell", bundle: nil), forCellWithReuseIdentifier: "LayerTextCell")
        collView.register(UINib(nibName: "LayerBrandCell", bundle: nil), forCellWithReuseIdentifier: "LayerBrandCell")
        collView.register(UINib(nibName: "LayerCounterCell", bundle: nil), forCellWithReuseIdentifier: "LayerCounterCell")
        collView.register(UINib(nibName: "LayerProductCell", bundle: nil), forCellWithReuseIdentifier: "LayerProductCell")
        collView.delegate = self
        collView.dataSource = self
        
        collView.keyboardDismissMode = .onDrag
        
        collView.reloadData()
        
    }
    
    func updateNotificationCount() {
        
        if APP_DEL.notificationStatus && APP_DEL.arrNotification.count > 0 {
            
            self.viewNotificationBottom.superview?.isHidden = false
            
        } else {
            
            self.viewNotificationBottom.superview?.isHidden = true
        }
    }
    
    func updateHome(res: Bool) {

        if APP_DEL.arrCategoriesCat.count > 0 {
            self.getBannerLayers()
        } else {
            self.getAllCategory()
        }
    }
    
    func getAllCategory() {
        
        guard let storeId = UserDefaults.standard.value(forKey: "storeId") as? String else {
            return
        }
        
        var postData = [String:String]()
        postData["store_id"] = storeId
        postData["country_id"] = APP_DEL.selectedCountry.country_code ?? "SA"
        
        API().callAPI(endPoint: "mobiconnectadvcart/category/getallcategorieslistnew", method: .POST, param: postData) { (json, err) in
            
            
            if let success =  json[0]["success"].stringValue as? String {
                if success == "invalid_country"{
                    // redirect to splash screen
                    UserDefaults.standard.removeObject(forKey: Constants().USERDEFAULT_KEY_SELECTED_COUNTRY);

                    let test = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SplashVC") as! SplashVC
                    APP_DEL.window?.rootViewController = UINavigationController (rootViewController: test)
                    return
                }
            }
            if (json[0]["status"].stringValue == "success")
            {
                let catData = try! json[0]["data"]["categories"].rawData()
                let tempCat = try! JSONDecoder().decode([CategoryLayer].self, from: catData)
                
                let tempCatSorted = tempCat.sorted { (one, two) -> Bool in
                    
                    if (one.sort_order == "" || one.sort_order == nil || two.sort_order == "" || two.sort_order == nil) {
                        return false;
                    } else {
                        return ((Int(one.sort_order ?? "0")!) < (Int(two.sort_order ?? "0")!));
                    }
                }
                
                APP_DEL.arrCategoriesCat = tempCatSorted
            }
            
            if APP_DEL.arrCategoriesCat.count > 0 {
                
//                let tempSelected = APP_DEL.arrCategoriesCat.firstIndex { (layer) -> Bool in
//                    return (layer.selected ?? false)
//                }
//
//                if let preSelected = tempSelected {
//                    APP_DEL.isFromHomeBannerID = APP_DEL.arrCategoriesCat[preSelected].main_category_id ?? ""
//                    self.homeCategoryId = APP_DEL.arrCategoriesCat[preSelected].main_category_id ?? ""
//                }
                self.homeCategoryId = home_layer_category_id
                self.getBannerLayers()
                
            } else {
                
                if IS_FEATURED_ENABLED {
                    
                    if self.arrProducts.count == 0 {
                        
                        self.getFeaturedProduct()
                        
                    } else {
                        
                        self.removeDummySplash()

                        self.activityIndi.isHidden = true
                        self.activityIndi.stopAnimating()
                    }
                    
                } else {
                    
                    self.removeDummySplash()
                }
            }
        }
    }
    
    func getBannerLayers() {
        
        if APP_DEL.isFromHomeBannerID == "" {

            APP_DEL.isFromHomeBannerID = APP_DEL.arrCategoriesCat.first?.main_category_id ?? ""
            self.categoryImage = APP_DEL.arrCategoriesCat.first?.main_category_image ?? ""

        } else {

            let index_row = APP_DEL.arrCategoriesCat.firstIndex { (item_category) -> Bool in
                return (item_category.main_category_id == APP_DEL.isFromHomeBannerID)
            }

            if let tempIndexRow = index_row {

                APP_DEL.isFromHomeBannerID = APP_DEL.arrCategoriesCat[tempIndexRow].main_category_id ?? ""
                self.categoryImage = APP_DEL.arrCategoriesCat[tempIndexRow].main_category_image ?? ""

            } else {

                APP_DEL.isFromHomeBannerID = APP_DEL.arrCategoriesCat.first?.main_category_id ?? ""
                self.categoryImage = APP_DEL.arrCategoriesCat.first?.main_category_image ?? ""
            }

        }
        
        guard let storeId = UserDefaults.standard.value(forKey: "storeId") as? String else {return}
        
        var postData = [String:String]()
        postData["store_id"] = storeId
        postData["category_id"] = self.homeCategoryId
//        postData["category_id"] = "119"
        postData["country_id"] = APP_DEL.selectedCountry.country_code ?? "SA"
        
        if let genderID =  UserDefaults.standard.object(forKey: KEY_GENDER) as? String {
            postData["gender_id"] = genderID
        }
        
        let localDT = LocalBanner().getBanner(cat_id: self.homeCategoryId, store_id: storeId)
        
        if localDT != nil {
            
            self.arrLayer = localDT?.banner_layer ?? []
            
            self.callReload()
            
            return;
            
        } else {
            
            // cedMageLoaders.removeLoadingIndicator(me: self);
            
            if isFirstTime == true {
                
                isFirstTime = false
                
            } else {
                
                activityIndi.isHidden = false
                activityIndi.startAnimating()
            }
            
            API().callAPI(endPoint: "layerdataoptimize", method: .POST, param: postData) { (json, err) in
                
                if err == nil {
                    
                    do {
                        if let success = try json[0]["success"].stringValue as? String {
                            if success == "invalid_country"{
                                // redirect to splash screen
                                UserDefaults.standard.removeObject(forKey: Constants().USERDEFAULT_KEY_SELECTED_COUNTRY);

                                let test = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SplashVC") as! SplashVC
                                APP_DEL.window?.rootViewController = UINavigationController (rootViewController: test)
                                return
                            }
                        }
                        
                        let catData = try json[0]["result"].rawData()
                        
                        self.arrLayer = try JSONDecoder().decode([BannerLayer].self, from: catData)
                        
                        LocalBanner().setBanner(banner: LocalBannerLayer(cat_id: postData["category_id"], store_id: postData["store_id"], expire_second: APP_DEL.category_record_seconds, store_time: DATE().getDate(date: Date()), banner_layer: self.arrLayer))
                        
                    } catch {
                        
                        print(error.localizedDescription)
                    }
                    
                }
                
                self.callReload()
            }
        }
    }
    
    func callReload() {
        
        DispatchQueue.main.async {
            
            self.collCategory.reloadData()
            
            self.collView.reloadData()
            
            self.startValidCounter()
        }
        
        self.checkNoData()
    }
    
    func startValidCounter() {
        
        arrCounterSection = []
        var arrRemoveCounterSection : [Int] = []
        for i in 0..<(self.arrLayer.count) {
            
            if self.arrLayer.count > i{
                
                let layer = self.arrLayer[i]
                
                //            let type = LayerType(rawValue: layer.layer_type ?? "") ?? LayerType.None
                
                //            if type == .Counter {
                
                let serverDate = layer.server_time ?? "" //"2020-07-01 01:00:00"
                let endDate = layer.end_date ?? "" //"2020-07-01 01:00:20"
                
                if serverDate != "" && endDate != "" {
                    
                    let serverDT = DATE().getDate(string: DATE().getDate(date: Date()))
                    let endDT = DATE().getDate(string: endDate)
                    
                    let endDateCompRes = serverDT.compare(endDT)
                    
                    if (endDateCompRes != .orderedDescending) {
                        
                        let elapsedTime = Int(endDT.timeIntervalSince(serverDT))
                        
                        if elapsedTime > 0 {
                            
                            arrCounterSection.append(i)
                            self.arrLayer[i].elapsedTime = elapsedTime
                            
                        } else {
                            
                            arrRemoveCounterSection.append(i)
                        }
                        
                    } else {
                        
                        arrRemoveCounterSection.append(i)
                    }
                    
                } else {
                    
                    arrRemoveCounterSection.append(i)
                }
                //            }
            }
            
            
            
        }
        
        if arrRemoveCounterSection.count > 0 {
            
            for index in 0..<arrRemoveCounterSection.count {
                
                if self.arrLayer.count > index {
                    self.arrLayer.remove(at: index)
                }
            }
        }
        
        if arrCounterSection.count > 0 {
            
            self.timer?.invalidate()
            self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.fireTimer), userInfo: nil, repeats: true)
        }
        
        
        self.collView.reloadData()
        
        if IS_FEATURED_ENABLED {
            
            if self.arrProducts.count == 0 {
                
                self.getFeaturedProduct()
                
            } else {
                
                self.activityIndi.isHidden = true
                self.activityIndi.stopAnimating()
                
                // cedMageLoaders.removeLoadingIndicator(me: self);
            }
            
            
        } else {
            
            self.removeDummySplash()
        }
        
    }
    
    lazy var DF: DateFormatter = {
        
        let DF = DateFormatter()
        DF.calendar = Calendar.current
        DF.timeZone = TimeZone.current
        return DF
    }()
    
    @objc func fireTimer() {
        
        DispatchQueue.main.async {
            
            let tempArrLayer = self.arrLayer
            for k in 0..<tempArrLayer.count {
                
                let type = LayerType(rawValue: tempArrLayer[k].layer_type ?? "") ?? LayerType.None
                //                if type == .Counter {
                if tempArrLayer[k].elapsedTime > 0 {
                    
                    if self.arrLayer.count > k {
                        self.arrLayer[k].elapsedTime = self.arrLayer[k].elapsedTime - 1
                    }
                    
                    if self.collView.numberOfSections > k {
                        
                        if type == .Counter {
                            self.collView.reloadItems(at: [IndexPath(item: 0, section: k)])
                        }
                    }
                    
                } else {
                    
                    if self.arrLayer.count > k {
                        
                        let storeId = UserDefaults.standard.value(forKey: "storeId") as! String
                        LocalBanner().removeLayer(cat_id: APP_DEL.isFromHomeBannerID, layer_id: self.arrLayer[k].layer_id ?? "", store_id: storeId)
                        
                        self.arrLayer.remove(at: k)
                        self.collView.reloadData()
                    }
                }
                //                }
            }
        }
        
    }
    
    func removeDummySplash() {
        
        if UserDefaults.standard.object(forKey: KEY_GENDER) == nil {
            
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "GenderVC") as! GenderVC
            vc.modalPresentationStyle = .fullScreen
            vc.modalTransitionStyle = .crossDissolve
            self.present(vc, animated: true, completion: {
                
                UIView.animate(withDuration: 0.5, animations: { [weak self] in
                    dummySplash.alpha = 0.0
                }) { [weak self] _ in
                    dummySplash.removeFromSuperview()
                }
            })
            
        } else {
            
            UIView.animate(withDuration: 0.5, animations: { [weak self] in
                dummySplash.alpha = 0.0
            }) { [weak self] _ in
                dummySplash.removeFromSuperview()
            }
        }
    }
    
    func getFeaturedProduct() {
            
            guard let storeId = UserDefaults.standard.value(forKey: "storeId") as? String else {return}
            
            // cedMageLoaders.removeLoadingIndicator(me: self);
            if currentPage != 1 {
                
                activityIndi.isHidden = false
                activityIndi.startAnimating()
                
                cedMageLoaders.addDefaultLoader_withlockedbackground(me: self)
            }

//        https://dev05.nejree.com/rest/V1/mobiconnect/catalog/homeFeatureProduct
//        {
//        "parameters":{
//        "page":"1",
//        "storeId":"1",
//        "country_id":"SA"
//        }
//        }
        
        var postData = [String:String]()
        postData["page"] = "\(currentPage)"
        postData["store_id"] = storeId
//        postData["category_id"] = "119"
        postData["country_id"] = APP_DEL.selectedCountry.country_code ?? "SA"
            API().callAPI(endPoint: "mobiconnect/catalog/homeFeatureProduct", method: .POST, param: postData) { (json_res, err) in
                
                let json = json_res[0]
                
                if json.stringValue == "NO_PRODUCTS" {
                    self.noProduct = true;
                    cedMageLoaders.removeLoadingIndicator(me: self);
                    self.activityIndi.isHidden = true
                    self.activityIndi.stopAnimating()
                   
                }
                else{
                    if json["success"].stringValue == "true" {
                        for prod in json["featured_products"].arrayValue {
                            let temp = product(offer: prod["offer"].stringValue,
                                               product_name: prod["product_name"].stringValue,
                                               stock_status: prod["stock_status"].stringValue,
                                               product_id: prod["product_id"].stringValue,
                                               type: prod["type"].stringValue,
                                               special_price: prod["special_price"].stringValue,
                                               product_url: prod["product-url"].stringValue,
                                               product_image: prod["product_image"].stringValue,
                                               wishlist_item_id: prod["wishlist_item_id"].stringValue,
                                               Inwishlist: prod["Inwishlist"].stringValue,
                                               description: prod["description"].stringValue,
                                               review: prod["review"].stringValue,
                                               regular_price: prod["regular_price"].stringValue,
                                               brands_name: prod["brands_name"].stringValue,
                                               tag: prod["tag"].stringValue)
                            
                            self.arrProducts.append(temp)
                        }
                    }
                }
                
               
                
                let indexSet = IndexSet(integer: self.arrLayer.count)
                self.collView.reloadSections(indexSet)
                
                self.activityIndi.isHidden = true
                self.activityIndi.stopAnimating()
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                self.checkNoData()
                
                self.removeDummySplash()
            }
        }

    @IBAction func btnSearchBottomAction(_ sender: Any) {
        
        if let viewController =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "cedMageSearchPage") as?  cedMageSearchPage {
            
            viewController.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(viewController, animated: true)
            
        }
    }
    
    @IBAction func btnNotificationAction(_ sender: Any) {
        
        //        let obj = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NotificationNewVC") as! NotificationNewVC
        //        obj.hidesBottomBarWhenPushed = true
        //        self.navigationController?.pushViewController(obj, animated: true)
        
        let obj = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
        obj.modalPresentationStyle = .overFullScreen
        self.present(obj, animated: false, completion: nil)
    }
    
    func checkNoData() {
        
        if ((self.arrLayer.count) + (self.arrProducts.count)) == 0 {
            self.collView.isHidden = true
            self.lblNodata.isHidden = false
        } else {
            self.collView.isHidden = false
            self.lblNodata.isHidden = true
        }
    }
    
    
    
    func getTagJsonString(tagString: String) -> String {
        
        var dict : [String:String] = [:]
        
        if tagString != "" {
            
            for temp in (tagString.components(separatedBy: ",")) {
                
                let temp_2 = temp.components(separatedBy: "/")
                
                if temp_2.count > 1 {
                    dict[temp_2[0]] = temp_2[1]
                }
            }
        }
        
        if dict == [:] {
            
            return ""
            
        } else {
            
            let data = try! JSONSerialization.data(withJSONObject: ["tag":dict], options: [])
            let jsonSTR = String(data: data, encoding: .utf8)!;
            
            return jsonSTR//"{\"tag\":\"\(jsonSTR)\"}";
        }
    }
    
    @objc func btnViewAllAction(_ sender: UIButton) {
        
        let section = sender.tag
        
        let layer = self.arrLayer[section]
        didSelectBanner(index: IndexPath(row: layer.product_data?.count ?? 0, section: section))
    }
    
    func didSelectBanner(index: IndexPath) {
        
        if (index.section != (self.arrLayer.count)) {
            
            self.view.endEditing(true);
            self.view.endEditing(true);
            
            let layer = self.arrLayer[index.section]
            let type = LayerType(rawValue: layer.layer_type ?? "") ?? LayerType.None
            var option: LayerOption = .None
            if (layer.layer_data?.count ?? 0) > 0 {
                option = LayerOption(rawValue: layer.layer_data?[index.row].layer_option ?? (layer.layer_option ?? "")) ?? LayerOption.None
            } else {
                option = LayerOption(rawValue: layer.layer_option ?? "") ?? LayerOption.None
            }
        
            
            if (type == .ProductScroll) // Product Layer
            {
                if (index.row == layer.product_data?.count) {
                    
                    if let viewController =  UIStoryboard(name: "categorylayouts", bundle: nil).instantiateViewController(withIdentifier: "cedMageDefaultCollection") as?  cedMageDefaultCollection {
                        
                        viewController.strTitleLayer = layer.layer_name ?? ""
                        //viewController.searchString = layer.layer_data?[index.row].tags ?? ""
                        viewController.hidesBottomBarWhenPushed = true
                        viewController.isFilterDisplay = false
                        viewController.isComesFromProductLayer = true
                        viewController.selectedCategory = layer.cat_id ?? ""
                        self.navigationController?.pushViewController(viewController, animated: true)
                    }
                    
                } else {
                    
                    let vc = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "productViewController") as! productViewController
                    vc.product_id = layer.product_data?[index.row].product_id ?? ""
                    APP_DEL.productIDglobal = layer.product_data?[index.row].product_id ?? ""
                    vc.hidesBottomBarWhenPushed = true
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
                
            else if option == .productWithCategory{
                
                if let viewController =  UIStoryboard(name: "categorylayouts", bundle: nil).instantiateViewController(withIdentifier: "cedMageDefaultCollection") as?  cedMageDefaultCollection {
                    
                    viewController.strTitleLayer = layer.layer_name ?? ""
                    viewController.hidesBottomBarWhenPushed = true
                    viewController.isFilterDisplay = false
                    viewController.isComesFromProductLayer = true
                    viewController.selectedCategory = layer.layer_data?[index.row].category_id ?? ""
                    self.navigationController?.pushViewController(viewController, animated: true)
                }
                
            }
                
            else if option == .GiftCard
            {
                
                let vc = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "NejreeGiftcardVC") as! NejreeGiftcardVC
                vc.category_id = layer.layer_data?[index.row].category_id ?? ""
                vc.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else if (option == .FullImage){
                
                SKPhotoBrowserOptions.displayAction = false
                var arrImages = [SKPhoto]()
                
                let photo = SKPhoto.photoWithImageURL(layer.layer_data?[index.item].full_image ?? "")
                photo.shouldCachePhotoURLImage = true
                arrImages.append(photo)
                
                let browser = SKPhotoBrowser(photos: arrImages)
                browser.modalPresentationStyle = .fullScreen
                browser.modalTransitionStyle = .crossDissolve
                self.present(browser, animated: true, completion: {})
                
            }
            else if (option == .SpecificProduct)
            {
                let vc = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "productViewController") as! productViewController
                vc.product_id = layer.layer_data?[index.row].product_id ?? ""
                APP_DEL.productIDglobal = layer.layer_data?[index.row].product_id ?? ""
                vc.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else if (option == .SpecificCategory)
            {
                APP_DEL.isFromHomeBannerID = layer.layer_data?[index.row].category_id ?? ""
              //  if isDisplayHomeCategory {
                APP_DEL.isCategoryFromHomeLayer = true
                    APP_DEL.delegateUpdateCategory?.updateCategory(res: false)
                  //  self.updateHome(res: true)
              //  }
                self.tabBarController?.selectedIndex = 0
            }
            else if (option == .ScrollableFullImage)
            {
                APP_DEL.isFromHomeBannerID = layer.layer_data?[index.row].category_id ?? ""
              //  if isDisplayHomeCategory {
                APP_DEL.isCategoryFromHomeLayer = true
                APP_DEL.delegateUpdateCategory?.updateCategory(res: false)
               //     self.updateHome(res: true)
              //  }
                self.tabBarController?.selectedIndex = 0
                
            }
            else if (option == .ShopInShop)
            {
                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HomeBrandVC") as! HomeBrandVC
                vc.categoryImage = categoryImage
                //vc.categoryId = layer.category ?? ""
                vc.categoryId = layer.layer_data?[index.row].category ?? ""
                vc.hidesBottomBarWhenPushed = true
                vc.strTitle = layer.layer_name ?? ""
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else if (option == .Product)
            {
                if let viewController =  UIStoryboard(name: "categorylayouts", bundle: nil).instantiateViewController(withIdentifier: "cedMageDefaultCollection") as?  cedMageDefaultCollection {
                    
                    let res = self.getTagJsonString(tagString: layer.layer_data?[index.row].tags ?? "")
                    UserDefaults.standard.set(res, forKey: "filtersToSend");
                    
                    viewController.strTitleLayer = layer.layer_name ?? ""
                    //viewController.searchString = layer.layer_data?[index.row].tags ?? ""
                    viewController.hidesBottomBarWhenPushed = true
                    viewController.isFilterDisplay = false
                    viewController.selectedCategory = self.homeCategoryId
                    self.navigationController?.pushViewController(viewController, animated: true)
                }
            }
            
        } else {
            
            
        }
        
    }
    
    
    
    func getDateFromString(strDate: String, formate: String) -> Date {
        
        DF.dateFormat = formate
        return DF.date(from: strDate)!
    }
    
    func checkPagination() {
        
        if noProduct {
            return
        }
        
        currentPage += 1
        
        self.getFeaturedProduct()
    }
}

extension NejreeBannerVC: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if scrollView == self.collView {
            
            if scrollView.panGestureRecognizer.translation(in: scrollView).y < 0 {
                
                //scrolling down
                changeTabBar(hidden: true, animated: true)
                
            } else {
                
                //scrolling up
                changeTabBar(hidden: false, animated: true)
            }
        }
    }

    func changeTabBar(hidden:Bool, animated: Bool) {
        
        let tabBar = self.tabBarController?.tabBar
        let offset = (hidden ? UIScreen.main.bounds.size.height : UIScreen.main.bounds.size.height - (tabBar?.frame.size.height)! )
        if offset == tabBar?.frame.origin.y { return }
        print("changing origin y position")
        
        let duration:TimeInterval = (animated ? 0.5 : 0.0)
        UIView.animate(withDuration: duration, animations: {
            
            tabBar!.frame.origin.y = offset
            self.stackBottomHT.constant = (hidden ? (10) : (44 + 10))
            
        }, completion:nil)
    }
}

extension NejreeBannerVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        if collectionView == self.collView {
            
            if IS_FEATURED_ENABLED {
                return self.arrLayer.count + 1;
            } else {
                return self.arrLayer.count;
            }
            
        } else if collectionView == self.collCategory {
            
            return 1;
        }
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == self.collView {
            
            if IS_FEATURED_ENABLED && (section == self.arrLayer.count) {
                
                return arrProducts.count;
                
            } else {
                
                if section < self.arrLayer.count {
                    
                    let layer = self.arrLayer[section]
                    let type = LayerType(rawValue: layer.layer_type ?? "") ?? LayerType.None
                    let option = LayerOption(rawValue: layer.layer_option ?? "") ?? LayerOption.None
                    
                    if type == .Image {
                        return 1
                    } else if type == .Image && option == .FullImage {
                        return 1
                    } else if type == .Image && option == .NoClickableFullImage {
                        return 1
                    } else if type == .Image && option == .ScrollableFullImage {
                        return 1
                    } else if type == .Image && (option == .SpecificProduct || option == .SpecificCategory || option == .GiftCard || option == .ShopInShop || option == . Product) {
                        return self.arrLayer[section].layer_data?.count ?? 0
                    }
                    else if type == .Image && option == .productWithCategory {
                        return 1
                    }
                        
                    else if type == .Text {
                        return 1
                    } else if type == .Multiple {
                        return 1
                    } else if type == .Counter {
                        return 1
                    } else if type == .ProductScroll {
                        return 1
                    }
                }
            }
            
        } else if collectionView == self.collCategory {
            
            return APP_DEL.arrCategoriesCat.count;
        }
        
        return 0;
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == self.collView {
            
            if IS_FEATURED_ENABLED && (indexPath.section == self.arrLayer.count) {
                
                if indexPath.row == arrProducts.count - 3 {
                    self.checkPagination()
                }
                
                
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CatProductCell", for: indexPath) as! CatProductCell
                
                if (APP_DEL.isDiscountTagEnable == "0"){
                    cell.offerLabel.isHidden = true
                }
                else
                {
                    let offerString = arrProducts[indexPath.item].offer ?? ""
                    
                    if offerString != "" {
                        
                        let offer = " \(offerString)% \(APP_LBL().off.uppercased()) "
                        
                        cell.offerLabel.text = offer
                        cell.offerLabel.isHidden = false
                    } else {
                        cell.offerLabel.isHidden = true
                    }
                }
                
                cell.lblProductName.text = arrProducts[indexPath.item].product_name
                
                let tagString = arrProducts[indexPath.item].tag ?? ""
                if tagString != "" {
                    
                    
                    cell.lblTags.text = " " + tagString + " "
                    cell.lblTags.isHidden = false
                } else {
                    cell.lblTags.isHidden = true
                }
                
                let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: arrProducts[indexPath.item].regular_price!)
                attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 1, range: NSMakeRange(0, attributeString.length))
                
                cell.lblBrandName.text = arrProducts[indexPath.item].brands_name
                
                if arrProducts[indexPath.item].special_price != "no_special" {
                    
                    cell.lblPrice.text = arrProducts[indexPath.item].special_price;
                    
                    cell.lblRegularPrice.attributedText = attributeString
                    
                    cell.lblRegularPrice.isHidden = false
                    cell.lblRegularPrice.alpha = 1
                    
                } else {
                    
                    cell.lblPrice.text = arrProducts[indexPath.item].regular_price
                    
                    cell.lblRegularPrice.isHidden = true
                    cell.lblRegularPrice.alpha = 0
                }
                
                
                //            if products[indexPath.item].special_price != "no_special" {
                //
                //                cell.lblPrice.text = products[indexPath.item].special_price;
                //
                //            } else {
                //
                //                cell.lblPrice.text = products[indexPath.item].regular_price
                //            }
                
                cell.imgProduct.image = nil
                
                //                if let downloadURL = SDImageCache.shared.imageFromCache(forKey: arrProducts[indexPath.item].product_image!){
                //                    cell.imgProduct!.image = downloadURL
                //                } else {
                cell.imgProduct!.sd_setImage(with: URL(string: arrProducts[indexPath.item].product_image!), placeholderImage: nil)
                //        }
                

                
                return cell
                
            } else {
                
                if indexPath.section < self.arrLayer.count {
                    
                    let layer = self.arrLayer[indexPath.section]
                    let type = LayerType(rawValue: layer.layer_type ?? "") ?? LayerType.None
                    let option = LayerOption(rawValue: layer.layer_option ?? "") ?? LayerOption.None
                    
                    // Temp condition for all image side by side
                    if type == .Image {
                        
                        let cell = self.collView.dequeueReusableCell(withReuseIdentifier: "LayerCell", for: indexPath) as! LayerCell
                        cell.tag = indexPath.section
                        cell.isSingleLayer = -1
                        cell.delegateDidSelectBanner = self
                        
                        let imgCount = layer.layer_data?.count ?? 0
                        cell.pager.numberOfPages = imgCount
                        cell.pager.currentPage = 0
                        cell.pager.isHidden = (imgCount <= 1)
                        
                        cell.arrImages = layer.layer_data ?? []
                        
                        let res = getGeometry(ratio: layer.layer_ratio ?? "")
                        cell.itemSize = res.itemSize
                        cell.imageSize = res.imageSize
                        cell.timerSecond(second: Int(layer.layer_timer ?? "5")!)
                        
                        cell.collView.reloadData()
                        
                        return cell
                        
                    }
                    
                    if type == .Image && option == .FullImage {
                        
                        let cell = self.collView.dequeueReusableCell(withReuseIdentifier: "LayerCell", for: indexPath) as! LayerCell
                        cell.tag = indexPath.section
                        cell.isSingleLayer = -1
                        cell.delegateDidSelectBanner = self
                        
                        let imgCount = layer.layer_data?.count ?? 0
                        cell.pager.numberOfPages = imgCount
                        cell.pager.currentPage = 0
                        cell.pager.isHidden = (imgCount <= 1)
                        
                        cell.arrImages = layer.layer_data ?? []
                        
                        let res = getGeometry(ratio: layer.layer_ratio ?? "")
                        cell.itemSize = res.itemSize
                        cell.imageSize = res.imageSize
                        cell.timerSecond(second: Int(layer.layer_timer ?? "5")!)
                        
                        cell.collView.reloadData()
                        
                        return cell
                        
                    }
                    
                    if type == .Image && option == .productWithCategory {
                        
                        let cell = self.collView.dequeueReusableCell(withReuseIdentifier: "LayerCell", for: indexPath) as! LayerCell
                        cell.tag = indexPath.section
                        cell.isSingleLayer = -1
                        cell.delegateDidSelectBanner = self
                        
                        let imgCount = layer.layer_data?.count ?? 0
                        cell.pager.numberOfPages = imgCount
                        cell.pager.currentPage = 0
                        cell.pager.isHidden = (imgCount <= 1)
                        
                        cell.arrImages = layer.layer_data ?? []
                        
                        let res = getGeometry(ratio: layer.layer_ratio ?? "")
                        cell.itemSize = res.itemSize
                        cell.imageSize = res.imageSize
                        cell.timerSecond(second: Int(layer.layer_timer ?? "5")!)
                        
                        cell.collView.reloadData()
                        
                        return cell
                        
                    }
                        
                    else if type == .Image && option == .NoClickableFullImage {
                        
                        let cell = self.collView.dequeueReusableCell(withReuseIdentifier: "LayerCell", for: indexPath) as! LayerCell
                        cell.tag = indexPath.section
                        cell.isSingleLayer = -1
                        cell.delegateDidSelectBanner = self
                        
                        let imgCount = layer.layer_data?.count ?? 0
                        cell.pager.numberOfPages = imgCount
                        cell.pager.currentPage = 0
                        cell.pager.isHidden = (imgCount <= 1)
                        
                        cell.arrImages = layer.layer_data ?? []
                        
                        let res = getGeometry(ratio: layer.layer_ratio ?? "")
                        cell.itemSize = res.itemSize
                        cell.imageSize = res.imageSize
                        cell.timerSecond(second: Int(layer.layer_timer ?? "5")!)
                        
                        cell.collView.reloadData()
                        
                        return cell
                        
                    }
                    else if type == .Image && option == .ScrollableFullImage {
                        
                        let cell = self.collView.dequeueReusableCell(withReuseIdentifier: "LayerCell", for: indexPath) as! LayerCell
                        cell.tag = indexPath.section
                        cell.isSingleLayer = -1
                        cell.delegateDidSelectBanner = self
                        
                        let imgCount = layer.layer_data?.count ?? 0
                        cell.pager.numberOfPages = imgCount
                        cell.pager.currentPage = 0
                        cell.pager.isHidden = (imgCount <= 1)
                        
                        cell.arrImages = layer.layer_data ?? []
                        
                        let res = getGeometry(ratio: layer.layer_ratio ?? "")
                        cell.itemSize = res.itemSize
                        cell.imageSize = res.imageSize
                        cell.timerSecond(second: Int(layer.layer_timer ?? "5")!)
                        
                        cell.collView.reloadData()
                        
                        return cell
                        
                    } else if type == .Image && (option == .SpecificProduct || option == .SpecificCategory || option == .GiftCard || option == .ShopInShop || option == .Product) {
                        
                        let cell = self.collView.dequeueReusableCell(withReuseIdentifier: "LayerCell", for: indexPath) as! LayerCell
                        cell.tag = indexPath.section
                        cell.isSingleLayer = indexPath.row
                        cell.delegateDidSelectBanner = self
                        
                        cell.arrImages = [layer.layer_data?[indexPath.row]]
                        
                        cell.pager.numberOfPages = 1
                        cell.pager.currentPage = 0
                        cell.pager.isHidden = true
                        
                        let res = getGeometry(ratio: layer.layer_ratio ?? "")
                        cell.itemSize = res.itemSize
                        cell.imageSize = res.imageSize
                        
                        cell.collView.reloadData()
                        
                        return cell
                        
                    } else if type == .Text {
                        
                        let cell = self.collView.dequeueReusableCell(withReuseIdentifier: "LayerTextCell", for: indexPath) as! LayerTextCell
                        cell.tag = indexPath.section
                        
                        cell.lblTitle.text = layer.layer_name ?? ""
                        
                        return cell
                        
                    } else if type == .Multiple {
                        
                        let cell = self.collView.dequeueReusableCell(withReuseIdentifier: "LayerBrandCell", for: indexPath) as! LayerBrandCell
                        cell.tag = indexPath.section
                        cell.delegateDidSelectBanner = self
                        
                        cell.itemSize = CGSize(width: CGFloat(Float(layer.layer_width ?? "0.0") ?? 0.0), height: CGFloat(Float(layer.layer_height ?? "0.0") ?? 0.0))
                        cell.itemSpace = CGFloat(Float(layer.layer_item_space ?? "0.0") ?? 0.0)
                        
                        cell.collView.contentInset = UIEdgeInsets(top: 0, left: CGFloat(Float(layer.layer_item_space ?? "0.0") ?? 0.0), bottom: 0, right: CGFloat(Float(layer.layer_item_space ?? "0.0") ?? 0.0))
                        
                        cell.arrImages = layer.layer_data ?? []
                        
                        cell.collView.reloadData()
                        
                        return cell
                        
                    } else if type == .Counter {
                        
                        let cell = self.collView.dequeueReusableCell(withReuseIdentifier: "LayerCounterCell", for: indexPath) as! LayerCounterCell
                        cell.tag = indexPath.section
                        
                        cell.setTimer(elapsedTime: layer.elapsedTime)
                        
                        return cell
                        
                    } else if type == .ProductScroll {
                        
                        let cell = self.collView.dequeueReusableCell(withReuseIdentifier: "LayerProductCell", for: indexPath) as! LayerProductCell
                        cell.tag = indexPath.section
                        cell.delegateDidSelectBanner = self
                        
                        //cell.collView.contentInset = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
                        
                        
                        //let limitGet = Int(layer.limit ?? "0") ?? 0
                        cell.isDisplayViewAll = false//((layer.count ?? 0) > limitGet)
                        cell.setProductData(product: layer.product_data ?? [])
                        
                        //                        if self.lastCategoryID != APP_DEL.isFromHomeBannerID {
                        //                            self.lastCategoryID = APP_DEL.isFromHomeBannerID
                        
                        cell.collView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .init(), animated: false)
                        //       }
                        
                        return cell
                    }
                }
            }
            
        } else if collectionView == self.collCategory {
            
            let cell = self.collCategory.dequeueReusableCell(withReuseIdentifier: "CategoryMainCell", for: indexPath) as! CategoryMainCell
            
            let cat = APP_DEL.arrCategoriesCat[indexPath.row]
            
            cell.lblTitle.text = (cat.main_category_name ?? "").uppercased()
            
            if APP_DEL.isFromHomeBannerID == (cat.main_category_id ?? "") {
                
                cell.lblTitle.font = UIFont(name: "Cairo-Bold", size: 17)!
                cell.lblDevider.isHidden = false
                cell.lblTitle.textColor = UIColor(hex: "#FBAD18")!
                
                APP_DEL.strHomeBannerImage = cat.main_category_image ?? ""
                
            } else {
                
                cell.lblTitle.font = UIFont(name: "Cairo-Regular", size: 17)!
                cell.lblDevider.isHidden = true
                cell.lblTitle.textColor = UIColor.white
            }
            
            //cell.lblTitle.textColor = UIColor.black
            cell.lblTitle.superview?.backgroundColor = UIColor.black//UIColor.init(hexString: "#FBAD18")!
            cell.lblDevider.backgroundColor = UIColor(hex: "#FBAD18")!//UIColor.black
            
            return cell;
        }
        
        
        
        
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == self.collView {
            
            if indexPath.section == self.arrLayer.count {
                
                let vc = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "productViewController") as! productViewController
                vc.product_id = arrProducts[indexPath.item].product_id!
                APP_DEL.productIDglobal = arrProducts[indexPath.item].product_id!
                vc.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
        } else if collectionView == self.collCategory {
            
            let cat = APP_DEL.arrCategoriesCat[indexPath.row]
            
            if APP_DEL.isFromHomeBannerID != (cat.main_category_id ?? "") {
                
                APP_DEL.isFromHomeBannerID = (cat.main_category_id ?? "")
                self.categoryImage = (cat.main_category_image ?? "")
                
                APP_DEL.delegateUpdateCategory?.updateCategory(res: true)
                
                self.arrCounterSection = []
                self.checkNoData()
                
                self.collCategory.reloadData()
                
                self.lblNodata.isHidden = true
                
                self.collView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
                
                self.getBannerLayers()
            }
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        if collectionView == self.collView {
            
            if IS_FEATURED_ENABLED && (indexPath.section == self.arrLayer.count) {
                
                guard let headerView = self.collView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "LayerHeaderCell", for: indexPath) as? LayerHeaderCell
                    else {
                        fatalError("Invalid view type")
                }
                
                headerView.backgroundColor = .white
                headerView.lblTitle.text = APP_LBL().featured_products.uppercased()
                
                return headerView
                
            } else {
                
                if indexPath.section < self.arrLayer.count {
                    
                    let layer = self.arrLayer[indexPath.section]
                    let type = LayerType(rawValue: layer.layer_type ?? "") ?? LayerType.None
                    let option = LayerOption(rawValue: layer.layer_option ?? "") ?? LayerOption.None
                    
                    switch kind {
                        
                    case UICollectionView.elementKindSectionHeader:
                        
                        if type == .Image && option == .FullImage {
                            
                            guard let headerView = self.collView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "LayerHeaderWhiteCell", for: indexPath) as? LayerHeaderWhiteCell
                                else {
                                    fatalError("Invalid view type")
                            }
                            
                            return headerView
                            
                        } else if type == .Image && option == .NoClickableFullImage {
                            
                            guard let headerView = self.collView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "LayerHeaderWhiteCell", for: indexPath) as? LayerHeaderWhiteCell
                                else {
                                    fatalError("Invalid view type")
                            }
                            
                            return headerView
                            
                        }  else if type == .Image && option == .ScrollableFullImage {
                            
                            guard let headerView = self.collView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "LayerHeaderWhiteCell", for: indexPath) as? LayerHeaderWhiteCell
                                else {
                                    fatalError("Invalid view type")
                            }
                            
                            return headerView
                            
                        }
                            
                        else if type == .Image && (option == .SpecificProduct || option == .SpecificCategory || option == .GiftCard || option == .ShopInShop || option == . Product || option == .productWithCategory) {
                            
                            guard let headerView = self.collView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "LayerHeaderWhiteCell", for: indexPath) as? LayerHeaderWhiteCell
                                else {
                                    fatalError("Invalid view type")
                            }
                            
                            return headerView
                            
                        } else if type == .Text {
                            
                            guard let headerView = self.collView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "LayerHeaderWhiteCell", for: indexPath) as? LayerHeaderWhiteCell
                                else {
                                    fatalError("Invalid view type")
                            }
                            
                            return headerView
                            
                        } else if type == .Multiple {
                            
                            guard let headerView = self.collView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "LayerHeaderWhiteCell", for: indexPath) as? LayerHeaderWhiteCell
                                else {
                                    fatalError("Invalid view type")
                            }
                            
                            return headerView
                            
                        } else if type == .Counter {
                            
                            guard let headerView = self.collView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "LayerHeaderWhiteCell", for: indexPath) as? LayerHeaderWhiteCell
                                else {
                                    fatalError("Invalid view type")
                            }
                            
                            return headerView
                            
                        } else if type == .ProductScroll {
                            
                            //                            guard let headerView = self.collView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "LayerHeaderWhiteCell", for: indexPath) as? LayerHeaderWhiteCell
                            //                                else {
                            //                                    fatalError("Invalid view type")
                            //                            }
                            
                            guard let headerView = self.collView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "LayerHeaderViewAllCell", for: indexPath) as? LayerHeaderViewAllCell
                                else {
                                    fatalError("Invalid view type")
                            }
                            
                            headerView.lblTitle.text = (layer.layer_name ?? "").uppercased()
                            headerView.btnViewAll.tag = indexPath.section
                            headerView.btnViewAll.addTarget(self, action: #selector(self.btnViewAllAction(_:)), for: .touchUpInside)
                            
                            return headerView
                            
                        } else {
                            
                            guard let headerView = self.collView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "LayerHeaderWhiteCell", for: indexPath) as? LayerHeaderWhiteCell
                                else {
                                    fatalError("Invalid view type")
                            }
                            
                            return headerView
                        }
                        
                    case UICollectionView.elementKindSectionFooter:
                        
                        if type == .Image && option == .FullImage {
                            
                            guard let headerView = self.collView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "LayerHeaderWhiteCell", for: indexPath) as? LayerHeaderWhiteCell
                                else {
                                    fatalError("Invalid view type")
                            }
                            
                            return headerView
                            
                        } else if type == .Image && option == .NoClickableFullImage {
                            
                            guard let headerView = self.collView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "LayerHeaderWhiteCell", for: indexPath) as? LayerHeaderWhiteCell
                                else {
                                    fatalError("Invalid view type")
                            }
                            
                            return headerView
                            
                        } else if type == .Image && option == .ScrollableFullImage {
                            
                            guard let headerView = self.collView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "LayerHeaderWhiteCell", for: indexPath) as? LayerHeaderWhiteCell
                                else {
                                    fatalError("Invalid view type")
                            }
                            
                            return headerView
                            
                        } else if type == .Image && (option == .SpecificProduct || option == .SpecificCategory || option == .GiftCard) {
                            
                            guard let headerView = self.collView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "LayerHeaderWhiteCell", for: indexPath) as? LayerHeaderWhiteCell
                                else {
                                    fatalError("Invalid view type")
                            }
                            
                            return headerView
                            
                        } else if type == .Text {
                            
                            guard let headerView = self.collView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "LayerHeaderWhiteCell", for: indexPath) as? LayerHeaderWhiteCell
                                else {
                                    fatalError("Invalid view type")
                            }
                            
                            return headerView
                            
                        } else if type == .Multiple {
                            
                            guard let headerView = self.collView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "LayerHeaderWhiteCell", for: indexPath) as? LayerHeaderWhiteCell
                                else {
                                    fatalError("Invalid view type")
                            }
                            
                            return headerView
                            
                        } else if type == .Counter {
                            
                            guard let headerView = self.collView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "LayerHeaderWhiteCell", for: indexPath) as? LayerHeaderWhiteCell
                                else {
                                    fatalError("Invalid view type")
                            }
                            
                            return headerView
                            
                        } else if type == .ProductScroll {
                            
                            guard let headerView = self.collView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "LayerHeaderWhiteCell", for: indexPath) as? LayerHeaderWhiteCell
                                else {
                                    fatalError("Invalid view type")
                            }
                            
                            return headerView
                            
                        } else {
                            
                            guard let headerView = self.collView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "LayerHeaderWhiteCell", for: indexPath) as? LayerHeaderWhiteCell
                                else {
                                    fatalError("Invalid view type")
                            }
                            
                            return headerView
                        }
                        
                        
                    default:
                        
                        fatalError("Unexpected element kind")
                    }
                    
                } else {
                    
                    guard let headerView = self.collView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "LayerHeaderWhiteCell", for: indexPath) as? LayerHeaderWhiteCell
                        else {
                            fatalError("Invalid view type")
                    }
                    
                    return headerView
                }
            }
            
        } else if collectionView == self.collCategory {
            
            switch kind {
                
            case UICollectionView.elementKindSectionHeader:
                
                guard let headerView = self.collCategory.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "LayerHeaderWhiteCell", for: indexPath) as? LayerHeaderWhiteCell
                    else {
                        fatalError("Invalid view type")
                }
                
                headerView.backgroundColor = UIColor.black
                
                return headerView
                
            case UICollectionView.elementKindSectionFooter:
                
                guard let headerView = self.collCategory.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "LayerHeaderWhiteCell", for: indexPath) as? LayerHeaderWhiteCell
                    else {
                        fatalError("Invalid view type")
                }
                
                headerView.backgroundColor = UIColor.black
                
                return headerView
                
                
            default:
                
                fatalError("Unexpected element kind")
            }
        }
        
        return UICollectionReusableView()
    }
    
    func getSafeAreaHT() -> CGFloat {
        
        let verticalSafeAreaInset: CGFloat
        if #available(iOS 11.0, *) {
            verticalSafeAreaInset = self.view.safeAreaInsets.bottom + self.view.safeAreaInsets.top
        } else {
            verticalSafeAreaInset = 0.0
        }
        let safeAreaHeight = self.view.frame.height - verticalSafeAreaInset
        
        return (safeAreaHeight - 44.0 + MENU_HT)
    }
    
    func getGeometry(ratio: String) -> (itemSize: CGSize, imageSize: CGSize) {
        
        let temp = ratio.components(separatedBy: ":")
        
        if temp.count >= 2 {
            
            if (temp[0] != "*" && temp[1] != "*") {
                
                let WR : CGFloat = CGFloat(Double(temp[0])!)
                let HR : CGFloat = CGFloat(Double(temp[1])!)
                
                if WR >= 1.0 {
                    
                    let wR : CGFloat = (1.0)
                    let hR : CGFloat = (HR / WR)
                    
                    return (itemSize: CGSize(width: SCREEN_WIDTH * wR, height: SCREEN_WIDTH * hR),
                            imageSize: CGSize(width: SCREEN_WIDTH * wR, height: SCREEN_WIDTH * hR))
                    
                } else {
                    
                    let width : CGFloat = (SCREEN_WIDTH * WR)
                    let height : CGFloat = (SCREEN_WIDTH * HR)
                    
                    return (itemSize: CGSize(width: SCREEN_WIDTH, height: height),
                            imageSize: CGSize(width: width, height: height))
                    
                }
                
            } else if (temp[0] == "*" && temp[1] != "*") {
                
                let HR : CGFloat = CGFloat(Double(temp[1])!)
                
                let height : CGFloat = (SCREEN_WIDTH * HR)
                
                return (itemSize: CGSize(width: SCREEN_WIDTH, height: height),
                        imageSize: CGSize(width: SCREEN_WIDTH, height: height))
                
            } else if (temp[0] != "*" && temp[1] == "*") {
                
                let WR : CGFloat = CGFloat(Double(temp[0])!)
                
                let width : CGFloat = (SCREEN_WIDTH * WR)
                
                return (itemSize: CGSize(width: width, height: getSafeAreaHT()),
                        imageSize: CGSize(width: width, height: getSafeAreaHT()))
                
            } else if (temp[0] == "*" && temp[1] == "*") {
                
                return (itemSize: CGSize(width: SCREEN_WIDTH, height: getSafeAreaHT()),
                        imageSize: CGSize(width: SCREEN_WIDTH, height: getSafeAreaHT()))
                
            }
        }
        
        return (itemSize: CGSize.zero,
                imageSize: CGSize.zero)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == self.collView {
            
            if IS_FEATURED_ENABLED && (indexPath.section == self.arrLayer.count) {
                
                return CGSize(width: (collView.frame.size.width/2)-3, height: (collView.frame.size.width/2)+70);
                
            } else {
                
                if indexPath.section < self.arrLayer.count {
                    
                    let layer = self.arrLayer[indexPath.section]
                    let type = LayerType(rawValue: layer.layer_type ?? "") ?? LayerType.None
                    let option = LayerOption(rawValue: layer.layer_option ?? "") ?? LayerOption.None
                    
                    if type == .Image && option == .FullImage {
                        
                        return getGeometry(ratio: layer.layer_ratio ?? "").itemSize
                        
                    } else if type == .Image && option == .NoClickableFullImage {
                        
                        return getGeometry(ratio: layer.layer_ratio ?? "").itemSize
                        
                    } else if type == .Image && option == .ScrollableFullImage {
                        
                        return getGeometry(ratio: layer.layer_ratio ?? "").itemSize
                        
                    } else if type == .Image && (option == .SpecificProduct || option == .SpecificCategory || option == .GiftCard || option == .ShopInShop || option == . Product || option == . productWithCategory) {
                        
                        return getGeometry(ratio: layer.layer_ratio ?? "").itemSize
                        
                    } else if type == .Text {
                        
                        return CGSize(width: SCREEN_WIDTH, height: 35.0)
                        
                    } else if type == .Multiple {
                        
                        return CGSize(width: SCREEN_WIDTH, height: CGFloat(Float(layer.layer_height ?? "0.0") ?? 0.0))
                        
                    } else if type == .Counter {
                        
                        //return CGSize(width: SCREEN_WIDTH, height: 135.0) -- hide remaining time text
                        return CGSize(width: SCREEN_WIDTH, height: 80)
                    } else if type == .ProductScroll {
                        
                        return CGSize(width: SCREEN_WIDTH, height: 240.0)
                    }
                }
                
            }
            
        } else if collectionView == self.collCategory {
            
            return CGSize(width: (SCREEN_WIDTH / CGFloat(APP_DEL.arrCategoriesCat.count)), height: 40.0)
        }
        
        return CGSize.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        
        if collectionView == self.collView {
            
            if IS_FEATURED_ENABLED && (section == self.arrLayer.count) {
                
                return CGSize(width: SCREEN_WIDTH, height: 35)
                
            } else {
                
                if section < self.arrLayer.count {
                    
                    let layer = self.arrLayer[section]
                    let type = LayerType(rawValue: layer.layer_type ?? "") ?? LayerType.None
                    let option = LayerOption(rawValue: layer.layer_option ?? "") ?? LayerOption.None
                    
                    if type == .Image && option == .FullImage {
                        
                        return CGSize(width: SCREEN_WIDTH, height: CGFloat(Float(layer.layer_top_space ?? "0.0") ?? 0.0))
                        
                    } else if type == .Image && option == .NoClickableFullImage {
                        
                        return CGSize(width: SCREEN_WIDTH, height: CGFloat(Float(layer.layer_top_space ?? "0.0") ?? 0.0))
                        
                    } else if type == .Image && option == .ScrollableFullImage {
                        
                        return CGSize(width: SCREEN_WIDTH, height: CGFloat(Float(layer.layer_top_space ?? "0.0") ?? 0.0))
                        
                    } else if type == .Image && (option == .SpecificProduct || option == .SpecificCategory || option == .GiftCard || option == .ShopInShop || option == . Product || option == . productWithCategory) {
                        
                        return CGSize(width: SCREEN_WIDTH, height: CGFloat(Float(layer.layer_top_space ?? "0.0") ?? 0.0))
                        
                    } else if type == .Text {
                        
                        return CGSize(width: SCREEN_WIDTH, height: CGFloat(Float(layer.layer_top_space ?? "0.0") ?? 0.0))
                        
                    } else if type == .Multiple {
                        
                        return CGSize(width: SCREEN_WIDTH, height: CGFloat(Float(layer.layer_top_space ?? "0.0") ?? 0.0))
                        
                    } else if type == .Counter {
                        
                        return CGSize(width: SCREEN_WIDTH, height: CGFloat(Float(layer.layer_top_space ?? "0.0") ?? 0.0))
                        
                    } else if type == .ProductScroll {
                        
                        //return CGSize(width: SCREEN_WIDTH, height: 0.0)
                        
                        let limitGet = Int(layer.limit ?? "0") ?? 0
                        return CGSize(width: SCREEN_WIDTH, height: ((layer.count ?? 0) > limitGet) ? 44.0 : 0.0)
                    }
                }
                
            }
            
        } else {
            
            return CGSize(width: 0.0, height: 0.0)
        }
        
        return CGSize(width: SCREEN_WIDTH, height: 0.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        
        if collectionView == self.collView {
            
            if IS_FEATURED_ENABLED && (section == self.arrLayer.count) {
                
                return CGSize(width: SCREEN_WIDTH, height: 0.0)
                
            } else {
                
                if section < self.arrLayer.count {
                    
                    let layer = self.arrLayer[section]
                    let type = LayerType(rawValue: layer.layer_type ?? "") ?? LayerType.None
                    let option = LayerOption(rawValue: layer.layer_option ?? "") ?? LayerOption.None
                    
                    if type == .Image && option == .FullImage {
                        
                        return CGSize(width: SCREEN_WIDTH, height: CGFloat(Float(layer.layer_bottom_space ?? "0.0") ?? 0.0))
                        
                    } else if type == .Image && option == .NoClickableFullImage {
                        
                        return CGSize(width: SCREEN_WIDTH, height: CGFloat(Float(layer.layer_bottom_space ?? "0.0") ?? 0.0))
                        
                    } else if type == .Image && option == .ScrollableFullImage {
                        
                        return CGSize(width: SCREEN_WIDTH, height: CGFloat(Float(layer.layer_bottom_space ?? "0.0") ?? 0.0))
                        
                    } else if type == .Image && (option == .SpecificProduct || option == .SpecificCategory || option == .GiftCard || option == .ShopInShop || option == . Product || option == . productWithCategory) {
                        
                        return CGSize(width: SCREEN_WIDTH, height: CGFloat(Float(layer.layer_bottom_space ?? "0.0") ?? 0.0))
                        
                    } else if type == .Text {
                        
                        return CGSize(width: SCREEN_WIDTH, height: CGFloat(Float(layer.layer_bottom_space ?? "0.0") ?? 0.0))
                        
                    } else if type == .Multiple {
                        
                        return CGSize(width: SCREEN_WIDTH, height: CGFloat(Float(layer.layer_bottom_space ?? "0.0") ?? 0.0))
                        
                    } else if type == .Counter {
                        
                        return CGSize(width: SCREEN_WIDTH, height: CGFloat(Float(layer.layer_bottom_space ?? "0.0") ?? 0.0))
                        
                    } else if type == .ProductScroll {
                        
                        return CGSize(width: SCREEN_WIDTH, height: 0.0)
                    }
                }
                
            }
            
        } else {
            
            return CGSize(width: 0.0, height: 0.0)
        }
        
        
        
        return CGSize(width: SCREEN_WIDTH, height: 0.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        if collectionView == self.collView {
            
            if IS_FEATURED_ENABLED && (section == self.arrLayer.count) {
                
                return 0.0
                
            } else {
                
                if section < self.arrLayer.count {
                    
                    let layer = self.arrLayer[section]
                    let type = LayerType(rawValue: layer.layer_type ?? "") ?? LayerType.None
                    let option = LayerOption(rawValue: layer.layer_option ?? "") ?? LayerOption.None
                    
                    if type == .Image && (option == .SpecificProduct || option == .SpecificCategory || option == .GiftCard || option == .ShopInShop || option == . Product || option == . productWithCategory) {
                        
                        return CGFloat(Float(layer.layer_item_space ?? "0.0") ?? 0.0)
                        
                    }
                }
            }
            
        } else {
            
            return 0.0
        }
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        if collectionView == self.collView {
            
            return 0
            
        } else {
            
            return 0
        }
    }
}

extension UIView {
    
    func round(redius: CGFloat? = nil) {
        
        self.layer.cornerRadius = ((redius == nil) ? (self.frame.size.height / 2.0) : (redius ?? 0.0))
        self.clipsToBounds = true
    }
}
