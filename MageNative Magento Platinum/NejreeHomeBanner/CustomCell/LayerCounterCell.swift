//
//  LayerCounterCell.swift
//  MageNative Magento Platinum
//
//  Created by vishal.n on 04/07/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class LayerCounterCell: UICollectionViewCell {

    
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var viewDays: UIView!
    @IBOutlet weak var lblDays: UILabel!
    @IBOutlet weak var lblDaysTitle: UILabel!
    
    @IBOutlet weak var viewHours: UIView!
    @IBOutlet weak var lblHours: UILabel!
    @IBOutlet weak var lblHoursTitle: UILabel!
    
    @IBOutlet weak var viewMinutes: UIView!
    @IBOutlet weak var lblMinutes: UILabel!
    @IBOutlet weak var lblMinutesTitle: UILabel!
    
    @IBOutlet weak var viewSeconds: UIView!
    @IBOutlet weak var lblSeconds: UILabel!
    @IBOutlet weak var lblSecondsTitle: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        
        
        
        let itemWidth : CGFloat = 40.0

        self.lblTitle.text = ""//APP_LBL().time_remaining.uppercased()
        
        self.viewDays.layer.cornerRadius = (itemWidth / 2.0)
        self.viewDays.clipsToBounds = true
        self.viewDays.layer.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        self.viewDays.layer.borderWidth = 1.5
        self.lblDaysTitle.text = APP_LBL().timer_days.uppercased()
        
        self.viewHours.layer.cornerRadius = (itemWidth / 2.0)
        self.viewHours.clipsToBounds = true
        self.viewHours.layer.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        self.viewHours.layer.borderWidth = 1.5
        self.lblHoursTitle.text = APP_LBL().timer_hours.uppercased()
        
        self.viewMinutes.layer.cornerRadius = (itemWidth / 2.0)
        self.viewMinutes.clipsToBounds = true
        self.viewMinutes.layer.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        self.viewMinutes.layer.borderWidth = 1.5
        self.lblMinutesTitle.text = APP_LBL().timer_minutes.uppercased()
        
        self.viewSeconds.layer.cornerRadius = (itemWidth / 2.0)
        self.viewSeconds.clipsToBounds = true
        self.viewSeconds.layer.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        self.viewSeconds.layer.borderWidth = 1.5
        self.lblSecondsTitle.text = APP_LBL().timer_seconds.uppercased()
        
        ////
    }

    
    func getHmsFromSecond(second: Int) -> (d: String, h: String, m: String, s: String, secInt: Int) {
        
        
        
        let d = (second / 86400);
        let h = ((second % 86400) / 3600);
        let m = ((second % 3600) / 60);
        let s = ((second % 3600) % 60);
        
        return (d: String(format: "%.02d", d),
                h: String(format: "%.02d", h),
                m: String(format: "%.02d", m),
                s: String(format: "%.02d", s),
                secInt: second)
    }
    
    
    func setTimer(elapsedTime: Int) {
        
        if elapsedTime > 0 {
                        
            let res = self.getHmsFromSecond(second: elapsedTime)
            
            if (Int(res.d) ?? 0) > 0 {
                self.lblDays.text = res.d
                self.viewDays.superview?.isHidden = false
            } else {
                self.viewDays.superview?.isHidden = true
            }
            
            if (((Int(res.h) ?? 0) > 0) || ((Int(res.d) ?? 0) > 0)) {
                self.lblHours.text = res.h
                self.viewHours.superview?.isHidden = false
            } else {
                self.viewHours.superview?.isHidden = true
            }
            
            if (((Int(res.m) ?? 0) > 0) || ((Int(res.h) ?? 0) > 0) || ((Int(res.d) ?? 0) > 0)) {
                self.lblMinutes.text = res.m
                self.viewMinutes.superview?.isHidden = false
            } else {
                self.viewMinutes.superview?.isHidden = true
            }
            
            if (((Int(res.s) ?? 0) > 0) || ((Int(res.m) ?? 0) > 0) || ((Int(res.h) ?? 0) > 0) || ((Int(res.d) ?? 0) > 0)) {
                self.lblSeconds.text = res.s
                self.viewSeconds.superview?.isHidden = false
            } else {
                self.viewSeconds.superview?.isHidden = true
            }
            
            
        } else {
            
            self.lblDays.text = "00"
            self.lblHours.text = "00"
            self.lblMinutes.text = "00"
            self.lblSeconds.text = "00"
        }
        
    }
}
