//
//  LayerBrandCell.swift
//  MageNative Magento Platinum
//
//  Created by vishal.n on 03/07/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class LayerBrandCell: UICollectionViewCell {
    
    @IBOutlet weak var collView: UICollectionView!
    
    var arrImages: [Layer_data] = []
    var itemSize : CGSize = CGSize.zero
    var itemSpace : CGFloat = 0.0
    
    var delegateDidSelectBanner: DidSelectBanner?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        collView.delegate = self
        collView.dataSource = self
        collView.register(UINib(nibName: "ImageCell", bundle: nil), forCellWithReuseIdentifier: "ImageCell")
        collView.reloadData()
        
    }

}


extension LayerBrandCell: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return arrImages.count;
    }
    

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = self.collView.dequeueReusableCell(withReuseIdentifier: "ImageCell", for: indexPath) as! ImageCell

        let data = arrImages[indexPath.item]
        
        cell.consWidth.constant = itemSize.width
        cell.consHeight.constant = itemSize.height
        
       // cell.imgView.sd_setImage(with: URL(string: data.banner_image ?? ""), placeholderImage: nil)
        
         cell.imgView.image = nil
        
//        if let downloadURL = SDImageCache.shared.imageFromCache(forKey: data.banner_image ?? ""){
//             cell.imgView.image = downloadURL
//        } else {
            cell.imgView.sd_setImage(with: URL(string: data.banner_image ?? ""), placeholderImage: nil)
        //}
        
        
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        self.delegateDidSelectBanner?.didSelectBanner(index: IndexPath(item: indexPath.item, section: self.tag))
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return itemSize
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return itemSpace
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}
