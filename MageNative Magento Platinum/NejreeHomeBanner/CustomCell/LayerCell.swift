//
//  LayerCell.swift
//  MageNative Magento Platinum
//
//  Created by vishal.n on 02/07/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class LayerCell: UICollectionViewCell {

    @IBOutlet weak var collView: UICollectionView!
    @IBOutlet weak var pager: UIPageControl!
    
    var arrImages: [Layer_data?] = []
    var itemSize: CGSize = .zero
    var imageSize: CGSize = .zero
        
    var timer: Timer?
    var timer_second: Int = 5
    var currentIndex: Int = 0

    var isSingleLayer = -1
    
    var delegateDidSelectBanner: DidSelectBanner?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        collView.delegate = self
        collView.dataSource = self
        collView.register(UINib(nibName: "ImageCell", bundle: nil), forCellWithReuseIdentifier: "ImageCell")
        collView.reloadData()
    }

    func timerSecond(second: Int) {
        
        self.timer_second = second
        self.timer?.invalidate()
        self.timer = Timer.scheduledTimer(timeInterval: TimeInterval(timer_second), target: self, selector: #selector(self.fireTimer), userInfo: nil, repeats: true)
    }
    
    @objc func fireTimer() {

        if arrImages.count > 1 && timer_second != 0 {
            
            if currentIndex == (arrImages.count - 1) {
                
                currentIndex = 0
                self.collView.scrollToItem(at: IndexPath(item: currentIndex, section: 0), at: .left, animated: true)
                                
            } else {
                
                currentIndex = currentIndex + 1
                self.collView.scrollToItem(at: IndexPath(item: currentIndex, section: 0), at: .left, animated: true)

            }
            
            pager.currentPage = currentIndex
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        if (scrollView == self.collView) {
            
            let page = Int(CGFloat(scrollView.contentOffset.x / scrollView.frame.size.width).rounded())
            currentIndex = page
            pager.currentPage = page
        }
    }
}

extension LayerCell: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return arrImages.count;
    }
    

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = self.collView.dequeueReusableCell(withReuseIdentifier: "ImageCell", for: indexPath) as! ImageCell

        cell.consWidth.constant = imageSize.width
        cell.consHeight.constant = imageSize.height
        
        let data = arrImages[indexPath.item]
       // cell.imgView.sd_setImage(with: URL(string: data?.banner_image ?? ""), placeholderImage: nil)
        
        cell.imgView.image = nil
        
//        if let downloadURL = SDImageCache.shared.imageFromCache(forKey: data?.banner_image ?? ""){
//             cell.imgView.image = downloadURL
//        } else {
            cell.imgView.sd_setImage(with: URL(string: data?.banner_image ?? ""), placeholderImage: nil)
      //  }
        
        
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        self.delegateDidSelectBanner?.didSelectBanner(index: IndexPath(item: ((isSingleLayer == -1) ? indexPath.item : isSingleLayer), section: self.tag))
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return itemSize
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}
