//
//  ImageCell.swift
//  MageNative Magento Platinum
//
//  Created by vishal.n on 02/07/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class ImageCell: UICollectionViewCell {

    
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var consWidth: NSLayoutConstraint!
    @IBOutlet weak var consHeight: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
