//
//  LayerHeaderViewAllCell.swift
//  MageNative Magento Platinum
//
//  Created by vishal.n on 28/01/21.
//  Copyright © 2021 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class LayerHeaderViewAllCell: UICollectionReusableView {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnViewAll: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        btnViewAll.setTitle(APP_LBL().view_all.uppercased(), for: .normal)
        
        let value=UserDefaults.standard.value(forKey: "AppleLanguages") as! [String]
        
        if value[0] == "ar" {
            lblTitle.textAlignment = .right
        } else {
            lblTitle.textAlignment = .left
        }
    }
    
}
