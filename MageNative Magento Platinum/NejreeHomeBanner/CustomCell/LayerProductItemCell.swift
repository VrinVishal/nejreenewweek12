//
//  LayerProductItemCell.swift
//  MageNative Magento Platinum
//
//  Created by vishal.n on 16/09/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class LayerProductItemCell: UICollectionViewCell {

    @IBOutlet weak var offerLabel: UILabel!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var productColor: UILabel!
    @IBOutlet weak var productPrice: UILabel!
    @IBOutlet weak var ratingView: FloatRatingView!
    @IBOutlet weak var insideView: UIView!
    @IBOutlet weak var regularPrice: UILabel!
    @IBOutlet weak var lblBrandName: UILabel!
      @IBOutlet weak var lblTags: UILabel!
    
    weak var imgProduct: UIImage!
    
    @IBOutlet weak var viewAll: UIView!
    @IBOutlet weak var lblViewAll: UILabel!
    @IBOutlet weak var imgViewAll: UIImageView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        
        
        offerLabel.round(redius: 5)
       // lblTags.round(redius: 5)
        
//        insideView.layer.cornerRadius = 2.0
//        insideView.layer.borderColor = UIColor.lightGray.cgColor
//        insideView.layer.borderWidth = 0.2
//        insideView.layer.shadowColor = UIColor(red: 225.0 / 255.0, green: 228.0 / 255.0, blue: 228.0 / 255.0, alpha: 1.0).cgColor
//        insideView.layer.shadowOpacity = 2.0
//        insideView.layer.shadowRadius = 2.0
//        insideView.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        
        lblBrandName.setFont(fontFamily: "Cairo-Bold", fontSize: 13)
        productName.setFont(fontFamily: "Cairo-Regular", fontSize: 13)
        productPrice.setFont(fontFamily: "Cairo-Regular", fontSize: 13)
        regularPrice.setFont(fontFamily: "Cairo-Regular", fontSize: 13)
        
        
        lblViewAll.text = APP_LBL().view_all.uppercased()
        lblViewAll.textColor = UIColor.black
        
        let value=UserDefaults.standard.value(forKey: "AppleLanguages") as! [String]
        lblBrandName.textAlignment = .center
        productName.textAlignment = .center
        productPrice.textAlignment = .center
        regularPrice.textAlignment = .center
        productColor.textAlignment = .center

    }

}
