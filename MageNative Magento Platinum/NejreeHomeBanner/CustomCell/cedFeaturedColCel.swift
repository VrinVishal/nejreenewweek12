//
//  cedFeaturedColCel.swift
//  MageNative Magento Platinum
//
//  Created by cedcoss on 22/08/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class cedFeaturedColCel: UICollectionViewCell {
    
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var starRatingView: FloatRatingView!
    @IBOutlet weak var offerLabel: UILabel!
    @IBOutlet weak var offerView: UIView!
    @IBOutlet weak var wishAddButton: UIButton!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var wrapperView: UIView!
    @IBOutlet weak var colorLabel: UILabel!
    
    
    @IBOutlet weak var productPrice: UILabel!
    
    @IBOutlet weak var lblBrand: UILabel!
    
    @IBOutlet weak var oldPrice: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
       //   offerView.backgroundColor = UIColor(red:0.69, green:0.12, blue:0.27, alpha:1)
         wrapperView.productCardView()
    }
}
