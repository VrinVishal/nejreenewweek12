//
//  NejreeOffersVC.swift
//  MageNative Magento Platinum
//
//  Created by vishal.n on 23/02/21.
//  Copyright © 2021 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class NejreeOffersVC: UIViewController {

    
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var tblViewHT: NSLayoutConstraint!
    
    @IBOutlet weak var lblCancel: UILabel!
    
    var arrOffer = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setUI()
    }
    
    
    

    @IBAction func btnCancelAction(_ sender: UIButton) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.tblView.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
    }

    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?)
    {
        if let tbl = object as? UITableView {
            
            if tbl == self.tblView {
             
                self.tblView.layer.removeAllAnimations()
                self.tblViewHT.constant = self.tblView.contentSize.height
                
            }
        }
        
        UIView.animate(withDuration: 0.5) {
            self.updateViewConstraints()
            self.loadViewIfNeeded()
        }
    }

    func setUI() {
                
        lblTitle.text = APP_LBL().available_offers.uppercased()
        lblTitle.font = UIFont(name: "Cairo-Regular", size: 17)!
        lblCancel.text = APP_LBL().cancel.uppercased()
        lblCancel.font = UIFont(name: "Cairo-Regular", size: 15)!
        
        if APP_DEL.selectedLanguage == Arabic {
            lblTitle.textAlignment = .right
        } else {
            lblTitle.textAlignment = .left
        }
        
        tblView.rowHeight = UITableView.automaticDimension
        tblView.estimatedRowHeight = UITableView.automaticDimension
        tblView.register(UINib(nibName: "NejreeOfferCell", bundle: nil), forCellReuseIdentifier: "NejreeOfferCell")
        tblView.delegate = self
        tblView.dataSource = self
        tblView.reloadData()
    }

}

extension NejreeOffersVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrOffer.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "NejreeOfferCell") as! NejreeOfferCell
        cell.selectionStyle = .none
        
        cell.lblTitle.font = UIFont(name: "Cairo-Regular", size: 16)!
        
        cell.lblTitle.text = self.arrOffer[indexPath.row]
        cell.lblTitle.textColor = UIColor.darkGray.withAlphaComponent(0.9)
        
        if APP_DEL.selectedLanguage == Arabic {
            cell.lblTitle.textAlignment = .right
        } else {
            cell.lblTitle.textAlignment = .left
        }
        
        cell.lblDevider.isHidden = ((self.arrOffer.count - 1) == indexPath.row)
        cell.lblDevider.backgroundColor = UIColor.lightGray.withAlphaComponent(0.5)
        
        return cell;
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension;
    }
}
