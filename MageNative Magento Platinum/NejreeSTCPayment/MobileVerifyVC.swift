//
//  MobileVerifyVC.swift
//  MageNative Magento Platinum
//
//  Created by pratima on 06/05/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

protocol StcMobileVerified {
    func stcMobileVerified(res: Bool, STC_OtpReference: String, STC_STCPayPmtReference: String, STC_TokenID: String)
}

class MobileVerifyVC: UIViewController, UITextFieldDelegate {
    
    
    @IBOutlet weak var lblMobileverification: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblNoCode: UILabel!
    @IBOutlet weak var lblResend: UILabel!
    @IBOutlet weak var btnResend: UIButton!
    @IBOutlet weak var btnVerify: UIButton!
    @IBOutlet weak var mobileView: UIView!
    @IBOutlet weak var txtMobile: UITextField!
    @IBOutlet weak var btntouch: UIButton!

    var STC_MerchantNote = ""
    var STC_RefNum = ""
    var STC_OtpReference = ""
    var STC_STCPayPmtReference = ""
    var STC_Amount = ""
    var STC_BillNumber = ""
    
    var customerId = ""
    
    var delegateStcMobileVerified: StcMobileVerified?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnVerify.setTitle(APP_LBL().verify.uppercased(), for: .normal)
        lblResend.text = APP_LBL().resend_code
        lblNoCode.text = APP_LBL().no_text_message_received
        lblMobileverification.text = APP_LBL().mobile_verification.uppercased()
        lblTitle.text = APP_LBL().we_need_to_verify_your_mobile_phone_number
//        mobileView.roundCorners()
        mobileView.setBorder()
        mobileView.layer.borderColor = UIColor.lightGray.cgColor
        mobileView.round(redius: 2)
        //btnVerify.roundCorners()
        
        txtMobile.placeholder = APP_LBL().enter_your_otp
        txtMobile.delegate = self
        txtMobile.keyboardType = .asciiCapableNumberPad
        
        mobileView.superview?.round(redius: 8)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.tabBarController?.tabBar.isHidden = true
    }
 
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField == self.txtMobile {
            
            btnVerify.setTitleColor(((textField.text ?? "") == "") ? UIColor.lightGray : UIColor.white , for: .normal)
        }
    }
    
    @IBAction func btnVerifyAction(_ sender:UIButton){
         
        if !txtMobile.hasText {
            
            cedMageHttpException.showAlertView(me: self, msg: APP_LBL().please_enter_six_digit_otp, title: APP_LBL().error)
            return
            
        } else {
            
            //, TokenReference: String
            self.stcDirectPaymentConfirm(OtpReference: STC_OtpReference, StcpaypmtReference: STC_STCPayPmtReference, OtpValue: txtMobile.text!)
        }
        
     }
    
    @IBAction func btnResendAction(_ sender:UIButton){
         
        self.stcDirectPaymentAuthorize(RefNum: self.STC_RefNum, BillNumber: self.STC_BillNumber, MerchantNote: self.STC_MerchantNote)
     }

    @IBAction func btnTouchAction(_ sender:UIButton){
        
        
        self.delegateStcMobileVerified?.stcMobileVerified(res: false, STC_OtpReference: "", STC_STCPayPmtReference: "", STC_TokenID: "")
        self.dismiss(animated: true, completion: nil)
        
        //self.navigationController?.popViewController(animated: true)
    }
  
    //MARK:- stcDirectPaymentAuthorize - 1
    //https://stagingapp.nejree.com/rest/V1/stc/directpaymentauthorize
    func stcDirectPaymentAuthorize(RefNum: String, BillNumber: String, MerchantNote: String) {
        
        var postData = [String:String]()
        
        postData["BillNumber"] = BillNumber
        postData["MerchantNote"] = MerchantNote
        postData["RefNum"] = RefNum
        postData["Amount"] = STC_Amount
        
        if let addressDynamic =  UserDefaults.standard.value(forKey: "selectedAddress") as? [String:Any] {
            
            postData["mobile_number"] = addressDynamic["phone"] as? String
        }
        
        postData["customer_id"] = customerId
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);
        
        API().callAPI(endPoint: "stc/directpaymentauthorize", method: .POST, param: postData) { (json, err) in
            
            DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                if err == nil {
                    
                    if json[0]["OtpReference"].stringValue != "" && json[0]["STCPayPmtReference"].stringValue != ""{
                      
                        
                        self.STC_OtpReference = json[0]["OtpReference"].stringValue
                        self.STC_STCPayPmtReference = json[0]["STCPayPmtReference"].stringValue
                       
                        cedMageHttpException.showAlertView(me: self, msg: APP_LBL().otp_resend_success, title: APP_LBL().error)
                        
                    } else {
                        
                        cedMageHttpException.showAlertView(me: self, msg: json[0]["message"].stringValue, title: APP_LBL().error)
                    }
                }
            }
        }
    }
    
    //MARK:- stcDirectPaymentConfirm - 2
    //https://stagingapp.nejree.com/rest/V1/stc/directpaymentconfirm
    //, TokenReference: String
    func stcDirectPaymentConfirm(OtpReference: String, StcpaypmtReference: String, OtpValue: String) {
        
        var postData = [String:String]()
        
        postData["OtpReference"] = OtpReference
        postData["StcpaypmtReference"] = StcpaypmtReference
        postData["OtpValue"] = OtpValue
        //postData["TokenReference"] = TokenReference
        
        if let addressDynamic =  UserDefaults.standard.value(forKey: "selectedAddress") as? [String:Any] {
            
            postData["mobile_number"] = addressDynamic["phone"] as? String
        }
        
        postData["customer_id"] = customerId
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);
        
        API().callAPI(endPoint: "stc/directpaymentconfirm", method: .POST, param: postData) { (json, err) in
            
            DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                if err == nil {
                    
                    if json[0]["Code"].stringValue == "2003" {
                      
                        //cedMageHttpException.showAlertView(me: self, msg: json[0]["message"].stringValue, title: APP_LBL().error)
                        
                        let showTitle = APP_LBL().error
                       let showMsg = json[0]["message"].stringValue
                
                        let confirmationAlert = UIAlertController(title: showTitle, message: showMsg, preferredStyle: UIAlertController.Style.alert);
                       
                        confirmationAlert.addAction(UIAlertAction(title: APP_LBL().ok, style: .default, handler: { (action: UIAlertAction!) in
                
                            self.delegateStcMobileVerified?.stcMobileVerified(res: false, STC_OtpReference: "", STC_STCPayPmtReference: "", STC_TokenID: "")
                            self.dismiss(animated: true, completion: nil)
                           
                        }));
                
                        self.present(confirmationAlert, animated: true, completion: nil)
                        
                        
                      
                        
                        
                    } else if json[0]["success"].stringValue == "true" {
                        
                        //MARK:- Please update of STC_OtpReference & STC_STCPayPmtReference & STC_TokenID from this responce
                        self.delegateStcMobileVerified?.stcMobileVerified(res: true, STC_OtpReference: self.STC_OtpReference, STC_STCPayPmtReference: self.STC_STCPayPmtReference, STC_TokenID: "Pass token here")
                        
                         self.dismiss(animated: true, completion: nil)
                        //self.navigationController?.popViewController(animated: true)
                    }
                }
            }
        }
    }
    
}
