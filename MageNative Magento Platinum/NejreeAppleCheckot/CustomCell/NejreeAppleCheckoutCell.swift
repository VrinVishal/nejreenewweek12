//
//  NejreeAppleCheckoutCell.swift
//  MageNative Magento Platinum
//
//  Created by vishal.n on 23/07/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class NejreeAppleCheckoutCell: UITableViewCell {

    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var imgSizeArrow: UIImageView!
    @IBOutlet weak var imgQtyArrow: UIImageView!
    @IBOutlet weak var qtyNotAvailable: UILabel!


    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productName: UILabel!
    
    @IBOutlet weak var lblGiftPrice: UILabel!
    @IBOutlet weak var lblGiftPriceSAR: UILabel!
    @IBOutlet weak var lblGiftDescr: UILabel!
    
    @IBOutlet weak var sizeValue: UILabel!
    @IBOutlet weak var btnSize: UIButton!
    @IBOutlet weak var quantityLabel: UILabel!
    @IBOutlet weak var btnQty: UIButton!
    @IBOutlet weak var sizeNotLabel: UILabel!
//    @IBOutlet weak var lblOutOfStock: UILabel!
    
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var priceRegular: UILabel!
    
    @IBOutlet weak var btnWish: UIButton!
    
    @IBOutlet weak var btnDelete: UIButton!
    
    @IBOutlet weak var btnEditGiftCard: UIButton!
    
    
    @IBOutlet weak var btnProductRedirctionImg: UIButton!
    @IBOutlet weak var btnProductRedirctionTitle: UIButton!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        outerView.layer.cornerRadius = 6.0
        outerView.clipsToBounds = true
        outerView.layer.borderColor = UIColor.lightGray.withAlphaComponent(0.6).cgColor
        outerView.layer.borderWidth = 0.5
        
//        productImage.superview?.layer.cornerRadius = 6.0
        productImage.superview?.clipsToBounds = true
//        productImage.superview?.layer.borderColor = UIColor.lightGray.withAlphaComponent(0.6).cgColor
//        productImage.superview?.layer.borderWidth = 0.5
        
        btnSize.superview?.layer.cornerRadius = 6.0
        sizeValue.superview?.clipsToBounds = true
        
        btnQty.superview?.layer.cornerRadius = 6.0
        quantityLabel.superview?.clipsToBounds = true
                
        sizeNotLabel?.superview?.layer.cornerRadius = 6.0
        sizeNotLabel?.superview?.clipsToBounds = true
        sizeNotLabel?.superview?.layer.borderColor = UIColor(hex: "#EF4B4B")?.withAlphaComponent(0.6).cgColor
        sizeNotLabel?.superview?.layer.borderWidth = 0.5
        sizeNotLabel?.text = APP_LBL().size_not_available
                

//        lblOutOfStock?.superview?.layer.cornerRadius = 6.0
//        lblOutOfStock?.superview?.clipsToBounds = true
//        lblOutOfStock?.superview?.layer.borderColor = UIColor(hex: "#EF4B4B")?.withAlphaComponent(0.6).cgColor
//        lblOutOfStock?.superview?.layer.borderWidth = 0.5
//        lblOutOfStock?.text = APP_LBL().item_out_of_stock
        
//        btnDelete.setTitle(APP_LBL().remove.uppercased(), for: .normal)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
