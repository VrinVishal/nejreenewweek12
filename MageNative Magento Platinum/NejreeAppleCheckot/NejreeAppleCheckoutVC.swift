//
//  NejreeAppleCheckoutVC.swift
//  MageNative Magento Platinum
//
//  Created by vishal.n on 23/07/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit
import FirebaseAnalytics


var addressObjApplePay : [String: Any]? = nil
var tempAddressObjApplePay : [String: Any]? = nil

class NejreeAppleCheckoutVC: MagenativeUIViewController, UIGestureRecognizerDelegate,RemoveCartProduct, SelectGiftcardOption {

    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var imgBackBG: UIImageView!
    
    @IBOutlet weak var lblItem: UILabel!
    
    @IBOutlet weak var scrolVIEW: UIScrollView!
    @IBOutlet weak var viewApplePay: UIView!
    @IBOutlet weak var lblNoData: UILabel!
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var htTbl: NSLayoutConstraint!
    
    @IBOutlet weak var lblTitleYourAddress: UILabel!
    @IBOutlet weak var viewAddress: UIView!
    @IBOutlet weak var lblAddCountry: UILabel!
    @IBOutlet weak var lblAddCity: UILabel!
    @IBOutlet weak var lblAddNeighbourhood: UILabel!
    @IBOutlet weak var lblAddPhone: UILabel!
    @IBOutlet weak var lblAddName: UILabel!
    @IBOutlet weak var btnChangeAddress: UIButton!
    
    @IBOutlet weak var viewPromoCode: UIView!
    @IBOutlet weak var txtPromoCode: UITextField!
    @IBOutlet weak var btnApplyPromoCode: UIButton!
    
    @IBOutlet weak var viewStoreCredit: UIView!
    @IBOutlet weak var lblStoreCreditApply: UILabel!
    @IBOutlet weak var btnStoreCredit: UIButton!
    
    @IBOutlet weak var viewTotal: UIView!
    @IBOutlet weak var lblSubTotal: UILabel!
    @IBOutlet weak var lblSubTotalValue: UILabel!
    @IBOutlet weak var lblShipping: UILabel!
    @IBOutlet weak var lblShippingValue: UILabel!
    @IBOutlet weak var lblDiscount: UILabel!
    @IBOutlet weak var lblDiscountValue: UILabel!
    @IBOutlet weak var lblStoreCredit: UILabel!
    @IBOutlet weak var lblStoreCreditValue: UILabel!
    
    @IBOutlet weak var lblTaxes: UILabel!
    @IBOutlet weak var lblTaxesValue: UILabel!
    
    @IBOutlet weak var lblTotal: UILabel!
    @IBOutlet weak var lblTotalValue: UILabel!
    
    @IBOutlet weak var btnApplePay: UIButton!
    
    var selectedEditGiftIndex = 0
    var isSelectCustomAddress = false
    
    
    var isGiftCardThereInCart : Bool = false
   
    var appliedStoreCreditString = "\(APP_DEL.selectedCountry.getCurrencySymbol().uppercased()) 0"
    var isStoreCreditApplied = false
    var store_creditwithoutcurrency = "0"
    var products = [CartProduct]()
    var total = [String:String]()
    var viewCartJson = JSON()
    
    var isExtraFeeAmount = false
    
    var selectedCartItem : CartProduct?
    
    var orderStatusData = [String:String]()
    var responsePassApplePay : Any!
    var strPublicKey = ""
    var strSecretKey = ""
    var strCurrentCheckoutMode = ""
    var orderEmail = "";
    var jsonResponseDictTemp = NSDictionary()
    var applePaySuccessCheck = false;
    var AmountFinal : Double!
    var orderId = ""
    var isStoreCreditApplyManually : Bool = false
    
    let clientTokenUrl  =     "mobiconnect/mobibrain/generatetokenpost"
    let trasactionUrl   =     "mobiconnect/mobibrain/transaction"
    let finalOrderCheck =     "mobiconnect/checkout/additionalinfo"
    let saveOrder       =     "mobiconnect/checkout/saveorder"
   
    @IBOutlet weak var viewFreeShipping: UIView!
    @IBOutlet weak var lblRequiredFreeShipping: UILabel!
    @IBOutlet weak var progressView: UIProgressView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.scrolVIEW.isHidden = true
        self.viewApplePay.isHidden = true
        self.lblNoData.isHidden = true
        
        setUI()
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    override func viewWillAppear(_ animated: Bool) {
        
         self.isStoreCreditApplyManually = false
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.tblView.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
        self.tabBarController?.tabBar.isHidden = true
        
        isGiftCardThereInCart = false
        
//        if (self.defaults.object(forKey: "userInfoDict") != nil) {
//            self.removeStoreCredit()
//        } else {
//            self.getCartList()
//        }
        
        let isCOD : Bool = (UserDefaults.standard.value(forKey: APP_DEL.key_isCOD) as? Bool) ?? false
        
        let isCardSelected : Bool = (UserDefaults.standard.value(forKey: APP_DEL.key_isCard) as? Bool) ?? false
        if isCOD == true || isCardSelected == true {

            UserDefaults.standard.setValue(false, forKey: APP_DEL.key_isCOD)
            UserDefaults.standard.setValue(false, forKey: APP_DEL.key_isCard)
            
            self.saveShippingPayamentForClearCOD()

        } else {
            
            self.updateCartList()
        }
        
        
        if navigationController?.responds(to: #selector(getter: UINavigationController.interactivePopGestureRecognizer)) ?? false {
            
            self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
            self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        }
        
    }
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        
        return true
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?)
    {
        self.tblView.layer.removeAllAnimations()

        self.htTbl.constant = self.tblView.contentSize.height
        UIView.animate(withDuration: 0.5) {
            self.updateViewConstraints()
            self.loadViewIfNeeded()
        }
    }
    
    func setUI() {
        
        let value=UserDefaults.standard.value(forKey: "AppleLanguages") as! [String]
        
        self.tblView.register(UINib(nibName: "NejreeAppleCheckoutCell", bundle: nil), forCellReuseIdentifier: "NejreeAppleCheckoutCell")
        self.tblView.delegate = self
        self.tblView.dataSource = self
        
        lblNoData.text = APP_LBL().your_box_looks_empty.uppercased();
        
        lblHeader.text = APP_LBL().apple_pay.uppercased();
        
        if value[0] == "ar" {
            lblRequiredFreeShipping.textAlignment = .right
            txtPromoCode.textAlignment = .right
            lblTitleYourAddress.textAlignment = .right
            lblAddCountry.textAlignment = .right
            lblAddCity.textAlignment = .right
            lblAddNeighbourhood.textAlignment = .right
            lblAddPhone.textAlignment = .right
            lblAddName.textAlignment = .right
            lblStoreCreditApply.textAlignment = .right
            lblSubTotal.textAlignment = .right
            lblShipping.textAlignment = .right
            lblDiscount.textAlignment = .right
              lblTaxes.textAlignment = .right
            lblStoreCredit.textAlignment = .right
            lblTotal.textAlignment = .right
            
            imgBackBG.image = UIImage(named: "icon_back_white_Arabic")
            lblItem.textAlignment = .right
        } else {
            lblRequiredFreeShipping.textAlignment = .left
            lblItem.textAlignment = .left
            lblTitleYourAddress.textAlignment = .left
            lblAddCountry.textAlignment = .left
            lblAddCity.textAlignment = .left
            lblAddNeighbourhood.textAlignment = .left
            lblAddPhone.textAlignment = .left
            lblAddName.textAlignment = .left
            lblStoreCreditApply.textAlignment = .left
            lblSubTotal.textAlignment = .left
            lblShipping.textAlignment = .left
            lblDiscount.textAlignment = .left
             lblTaxes.textAlignment = .left
            lblStoreCredit.textAlignment = .left
            lblTotal.textAlignment = .left
            
            txtPromoCode.textAlignment = .left
            imgBackBG.image = UIImage(named: "icon_back_white_round")
        }
        
        lblItem.text = APP_LBL().items.uppercased();
        
        lblItem.font  = UIFont(name: "Cairo-Regular", size: 13)!
        
        lblAddCountry.font  = UIFont(name: "Cairo-Regular", size: 16)!
        lblAddCity.font =  UIFont(name: "Cairo-Regular", size: 12)!
        lblAddNeighbourhood.font  = UIFont(name: "Cairo-Regular", size: 12)!
        lblAddPhone.font  = UIFont(name: "Cairo-Regular", size: 12)!
        lblAddName.font  = UIFont(name: "Cairo-Regular", size: 12)!
        
        lblTitleYourAddress.font  = UIFont(name: "Cairo-Regular", size: 15)!
        
        
        lblTitleYourAddress.text = APP_LBL().your_address.uppercased();
        
        viewAddress.round(redius: 10)
        viewAddress.layer.borderColor = UIColor.lightGray.cgColor
        viewAddress.layer.borderWidth = 0.3
        
        btnChangeAddress.setTitle(APP_LBL().change.uppercased(), for: .normal)
        btnChangeAddress.backgroundColor = UIColor.white
        btnChangeAddress.layer.borderColor = nejreeColor?.cgColor
        btnChangeAddress.layer.borderWidth = 1.0
        btnChangeAddress.setTitleColor(nejreeColor, for: .normal)
      //  btnChangeAddress.layer.cornerRadius = 5
        btnChangeAddress.clipsToBounds = true
        btnChangeAddress.titleLabel?.font = UIFont(name: "Cairo-Regular", size: 15)!
        
        viewPromoCode.round(redius: 0)
        viewPromoCode.layer.borderColor = UIColor.lightGray.cgColor
        viewPromoCode.layer.borderWidth = 0.3
        txtPromoCode.placeholder = APP_LBL().code.uppercased()
        txtPromoCode.font = UIFont(name: "Cairo-Regular", size: 15)!
        
        btnApplyPromoCode.backgroundColor = nejreeColor
        btnApplyPromoCode.setTitleColor(UIColor.black, for: .normal)
        btnApplyPromoCode.setTitle(APP_LBL().apply.uppercased(), for: .normal)
        btnApplyPromoCode.titleLabel?.font = UIFont(name: "Cairo-Regular", size: 15)!
        
        viewStoreCredit.round(redius: 7)
        
        self.updateStoreCredit(isForUpdate: false, value: false)
        
        viewTotal.round(redius: 7)
        self.lblSubTotal.text = APP_LBL().subtotal.uppercased()
        self.lblShipping.text = APP_LBL().shipping.uppercased()
        self.lblDiscount.text = APP_LBL().discount.uppercased()
         self.lblTaxes.text = APP_LBL().tax_price.uppercased()
        self.lblStoreCredit.text = APP_LBL().store_credit.uppercased()
        self.lblTotal.text = APP_LBL().total.uppercased()
        
        self.lblTotalValue.textColor = nejreeColor
        
        viewFreeShipping.round(redius: 7)
        
        btnApplePay.setTitle(" Pay", for: .normal)
        btnApplePay.round(redius: 5)
        
        progressView.layer.cornerRadius = 5.0
        progressView.clipsToBounds = true
        
        updateDetail()
    }
    
   
    
    @IBAction func btnChangeAddressAction(_ sender: UIButton) {
        
        let vc = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "nejreeAddressBookController") as! nejreeAddressBookController
        vc.delegateSelectAppleCheckoutAddress = self
        vc.isFromSelectAppleCheckoutAddress = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnApplyStoreCreditAction(_ sender: UIButton) {
        
         if UserDefaults.standard.bool(forKey: "isLogin") {
        
        
            self.isStoreCreditApplyManually = true
            
                if self.isStoreCreditApplied {
                    self.removeStoreCredit()
                } else {
                    
                    if self.store_creditwithoutcurrency == "0" {
                        
                        self.view.isUserInteractionEnabled = false;
                        self.view.makeToast(APP_LBL().store_credit_not_available, duration: 2, position: .center, title: nil, image: nil, style: nil, completion: {
                            Void in
                            
                            self.view.isUserInteractionEnabled = true;
                        })
                        
                        return;
                    }
                    self.setStoreCredit()
                }
        
        }
        else
         {
            self.view.isUserInteractionEnabled = false;
            self.view.makeToast(APP_LBL().store_credit_not_available, duration: 2, position: .center, title: nil, image: nil, style: nil, completion: {
                Void in
                
                self.view.isUserInteractionEnabled = true;
            })
        }
        
        
    }
    
    @IBAction func btnApplyPromoCodeAction(_ sender: UIButton) {
        
        if (self.total["coupon"] != "") && (self.total["is_discount"] == "true") {
            self.removePromoCode()
        } else {
            self.applyPromoCode()
        }
    }
    
    @IBAction func btnApplePayAction(_ sender: UIButton) {
        
        if addressObjApplePay == nil {
          
            self.view.makeToast(APP_LBL().Please_Choose_address_Before_checkout, duration: 1.0, position: .center)
            return;
        }
        
        for pro in self.products {
            
            //if pro.item_error != "" {
            if ((pro.item_error?.first?.type ?? "") != "") {
                
                self.view.makeToast(APP_LBL().some_of_your_products_are_out_of_stock, duration: 1.0, position: .center)
                return
            }
        }
        
        
        self.saveBillingAddress()
    }
    
    @IBAction func btnDismissAction(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
        APP_DEL.getCartCount()
    }
    
    @objc func btnWishProductAction(_ sender: UIButton) {
        
        if self.products[sender.tag].is_wishlist == "0" {
            self.addToWishList(index: sender.tag)
        } else {
            self.removeFromWishList(index: sender.tag)
        }
    }
    @objc func btnEditGiftCardAction(_ sender: UIButton) {
        
        selectedEditGiftIndex = sender.tag
        
        let filt = APP_DEL.arrGftCardData.first(where: { (item) -> Bool in ((item.child_product_id ?? "") == (products[sender.tag].child_product_id ?? ""))})
        if filt != nil {

            let comp = products[sender.tag].sub_total?.components(separatedBy: " ")
            let giftPrice = String(format: "%.2f", Double(comp?[1] ?? "0.0")!)
            
            let vc = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "NejreeGiftcardConfirmVC") as! NejreeGiftcardConfirmVC
            vc.delegateSelectGiftcardOption = self
            vc.name = filt?.name ?? ""
            vc.mobile = filt?.phone ?? ""
            vc.message = filt?.description ?? ""
            vc.price = giftPrice
//            vc.imageURL = self.arrGiftcard[self.carousel.currentItemIndex].product_image ?? ""
            vc.modalPresentationStyle = .overFullScreen
            vc.modalTransitionStyle = .crossDissolve
            self.present(vc, animated: true, completion: nil)
            
        }
        
        
        
    }
    
    func selectGiftcardOption(confirm: Bool, name: String, phone: String, message: String) {
        
        self.view.endEditing(true)
        
        if confirm == false {
            return;
        }
        
     
        
        guard let storeId = UserDefaults.standard.value(forKey: "storeId") as? String else {return}
        var postData = [String:String]()
        postData["store_id"] = storeId

 
        postData["product_id"] = products[selectedEditGiftIndex].product_id
        postData["child_product_id"] = products[selectedEditGiftIndex].child_product_id
        postData["qty"] = "1"
        postData["po_number"] = phone
        postData["item_id"] = products[selectedEditGiftIndex].item_id
        if let cart_id = self.defaults.object(forKey: "cartId") as? String {
            postData["cart_id"] = cart_id;
        }
        
        
        print("\(products[selectedEditGiftIndex].options_selected)")
        
      
        
        
        

        
        let filt = APP_DEL.arrGftCardData.first(where: { (item) -> Bool in ((item.child_product_id ?? "") == (products[selectedEditGiftIndex].child_product_id ?? ""))})
        if filt != nil {
            
            let nameAttributeID = filt?.name_option_attribute_id ?? "79"
            let phoneAttributeID = filt?.phone_option_attribute_id ?? "80"
            let descAttributeID = filt?.description_option_attribute_id ?? "81"
            
            postData["Custom"] = "{\"options\":{\"\(nameAttributeID)\":\"\(name)\",\"\(phoneAttributeID)\":\"\(phone)\",\"\(descAttributeID)\":\"\(message.encodeEmoji)\"}}"
            
            var super_attribute = "{";
            super_attribute += "\""+("\(products[selectedEditGiftIndex].options_selected?.first?.option_id ?? 0)")+"\":";
            super_attribute += "\""+(products[selectedEditGiftIndex].options_selected?.first?.option_value ?? "")+"\",";

            if (super_attribute.last! == ",") {

                super_attribute = super_attribute.substring(to: super_attribute.index(before: super_attribute.endIndex));
            }
            super_attribute += "}";

            postData["super_attribute"] = super_attribute
        }
    
        if (UserDefaults.standard.object(forKey: "userInfoDict") != nil) {

            let userInfoDict = UserDefaults.standard.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
            postData["customer_id"] = userInfoDict["customerId"]!;
            postData["email"] = userInfoDict["email"]!;
        }

        postData["message"] = message.encodeEmoji
        postData["name"] = name
        postData["price"] = "\(products[selectedEditGiftIndex].subtotal_price_without_currency ?? 0.0)"

        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);

        API().callAPI(endPoint: "mobiconnect/checkout/editgiftcard", method: .POST, param: postData) { (json_res, err) in

            DispatchQueue.main.async {

                cedMageLoaders.removeLoadingIndicator(me: self);

                if err == nil {

                    let value=UserDefaults.standard.value(forKey: "AppleLanguages") as! [String]

                    if json_res[0]["success"].stringValue.uppercased() == "TRUE" {

                        APP_DEL.getGiftCardData { (res) in
                            print("getGiftCardData:- ", res)
                            self.getCartList()
                        }

                    }
                    else {

                        cedMageHttpException.showAlertView(me: self, msg: APP_LBL().gift_quantity_not_available, title: APP_LBL().error)
                    }
                }
            }
        }
    }
    
    
    @objc func btnDeleteProductAction(_ sender: UIButton) {
        
        removeProductIndex = sender.tag
               
       let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RemoveProductPopupVC") as! RemoveProductPopupVC
       vc.modalPresentationStyle = .overFullScreen
       vc.modalTransitionStyle = .crossDissolve
       vc.delagateRemoveCartProduct = self
       self.present(vc, animated: true, completion: nil)
        
//        let showTitle = APP_LBL().confirmation
//        let showMsg = APP_LBL().operation_cant_be_undone
//
//        let confirmationAlert = UIAlertController(title: showTitle, message: showMsg, preferredStyle: UIAlertController.Style.alert);
//
//        confirmationAlert.addAction(UIAlertAction(title: APP_LBL().done_c, style: .default, handler: { (action: UIAlertAction!) in
//
//            self.removeProduct(index: sender.tag)
//        }));
//
//        confirmationAlert.addAction(UIAlertAction(title: APP_LBL().cancel, style: .default, handler: { (action: UIAlertAction!) in
//        }));
//
//        present(confirmationAlert, animated: true, completion: nil)
    }
    func removeCartProduct(res: Bool) {
        
        if res {
            
            self.removeProduct(index: removeProductIndex)
        }
    }
    
    func updateStoreCredit(isForUpdate: Bool, value: Bool) {
        
        if isForUpdate {
            
            if value == true {
                
                self.isStoreCreditApplied = true
                UserDefaults.standard.set(true, forKey: "isStoreCredit")
                
            } else {
                
                self.isStoreCreditApplied = false
                UserDefaults.standard.set(false, forKey: "isStoreCredit")
            }
        }
        
        if isStoreCreditApplied {
            
            self.lblStoreCreditValue.text = self.appliedStoreCreditString
            
            btnStoreCredit.setBackgroundImage(UIImage(named: "StoreSwitchOn"), for: .normal)
            
        } else {
            
            self.appliedStoreCreditString = "\(APP_DEL.selectedCountry.getCurrencySymbol().uppercased()) 0"
            self.lblStoreCreditValue.text = "0"
            
            btnStoreCredit.setBackgroundImage(UIImage(named: "StoreSwitchOff"), for: .normal)
        }
        
        if self.store_creditwithoutcurrency == "0"
        {
            lblStoreCreditApply.text = String(format: "%@ (\(APP_DEL.selectedCountry.getCurrencySymbol().uppercased()) 0)", APP_LBL().use_store_credit)
        }
        else
        {
            lblStoreCreditApply.text = String(format: "%@ (%@)", APP_LBL().use_store_credit,viewCartJson["data"]["store_credit"].stringValue)
        }
                
    }
    
    //MARK:- APPLY PROMO CODE
    func applyPromoCode() {
        
        if (self.txtPromoCode.text == "") {
            
            self.view.makeToast(APP_LBL().please_enter_coupon, duration: 2.0, position: .center, title: nil, image: nil, style: nil, completion: nil);
            return;
        }
        
        var postData = [String:String]()

        let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
        postData["hashkey"] = userInfoDict["hashKey"]!
        postData["customer_id"] = userInfoDict["customerId"]!;
        postData["store_id"] = UserDefaults.standard.value(forKey: "storeId") as? String
        
        if let cart_id = self.defaults.object(forKey: "cartId") as? String {
            postData["cart_id"] = cart_id;
        }
        
        postData["coupon_code"] = self.txtPromoCode.text!;
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);
        
        API().callAPI(endPoint: "mobiconnect/checkout/coupon", method: .POST, param: postData) { (json_res, err) in
            
            DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                if err == nil {
                    
                    let json = json_res[0]
                    
                    let value=UserDefaults.standard.value(forKey: "AppleLanguages") as! [String]
                    
                    if(json["cart_id"]["success"].stringValue == "true"){
                        
                        self.view.makeToast("\(APP_LBL().you_can_use_the_discount_coupon_code) \(self.txtPromoCode.text!).", duration: 1.0, position: .center)
                    }
                    else{
                        
                        if value[0]=="ar" {
                            
                            cedMageHttpException.showAlertView(me: self, msg: json["cart_id"]["message"].stringValue, title: APP_LBL().error)
                            
                        } else {
                            
                            cedMageHttpException.showAlertView(me: self, msg: "\(APP_LBL().the_discount_coupan_code) \(self.txtPromoCode.text!) \(APP_LBL().is_invalid)", title: APP_LBL().error)
                        }
                      
                    }
                    
                    self.getCartList()
                }
            }
        }
    }
    
    //MARK:- REMOVE PROMO CODE
    func removePromoCode() {
        
        if (self.txtPromoCode.text == "") {
            
            self.view.makeToast(APP_LBL().please_enter_coupon, duration: 2.0, position: .center, title: nil, image: nil, style: nil, completion: nil);
            return;
        }
        
        var postData = [String:String]()

        let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
        postData["hashkey"] = userInfoDict["hashKey"]!
        postData["customer_id"] = userInfoDict["customerId"]!;
        postData["store_id"] = UserDefaults.standard.value(forKey: "storeId") as? String
        
        if let cart_id = self.defaults.object(forKey: "cartId") as? String {
            postData["cart_id"] = cart_id;
        }
        
        postData["remove"] = "1";
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);
        
        API().callAPI(endPoint: "mobiconnect/checkout/coupon", method: .POST, param: postData) { (json_res, err) in
            
            DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                if err == nil {
                    
                    let json = json_res[0]
                    
                    let value=UserDefaults.standard.value(forKey: "AppleLanguages") as! [String]
                    
                    if (json["cart_id"]["success"].stringValue == "true") {
                        
                        if value[0]=="ar" {
                            
                            self.view.makeToast(json["cart_id"]["message"].stringValue, duration: 1.0, position: .center)
                            
                        } else {
                            
                            self.view.makeToast(APP_LBL().you_canceled_the_discount_coupon_code, duration: 1.0, position: .center)
                        }
                    }
                    
                    self.getCartList()
                }
            }
        }
    }
    
    //MARK:- SET STORE CREDIT
    func setStoreCredit() {
       
        
        if isGiftCardThereInCart {
            
            self.view.makeToast(APP_LBL().you_can_not_apply_store_credit_for_giftcard, duration: 2, position: .center, title: nil, image: nil, style: nil, completion: {
                Void in
                
            })
            
            return;
        }
        
        
        let currentStoreCredit = viewCartJson["data"]["store_creditwithoutcurrency"].stringValue
        if (currentStoreCredit == "0") {
         
            self.view.isUserInteractionEnabled = false;
            self.view.makeToast(APP_LBL().store_credit_not_available, duration: 2, position: .center, title: nil, image: nil, style: nil, completion: {
                Void in
                
                self.view.isUserInteractionEnabled = true;
            })
            
            return;
        }
        
        var postData = [String:String]()

        if(self.defaults.object(forKey: "userInfoDict") != nil){
            
            let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
            postData["hashkey"] = userInfoDict["hashKey"]!
            postData["customer_id"] = userInfoDict["customerId"]!;
            postData["store_id"] = UserDefaults.standard.value(forKey: "storeId") as? String
            
        } else {
            
            self.getCartList()
            return;
        }
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);
        
        API().callAPI(endPoint: "mobiconnect/setStoreCredit", method: .POST, param: postData) { (json_res, err) in
            
            DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                if err == nil {
                    
                    let json = json_res[0]
                    
                    self.appliedStoreCreditString = json["applied_amout"].stringValue
                    self.updateStoreCredit(isForUpdate: true, value: true)
                    
                    self.getCartList()
                }
            }
        }
    }
    
    //MARK:- REMOVE STORE CREDIT
    func removeStoreCredit() {
        
        var postData = [String:String]()

        if (self.defaults.object(forKey: "userInfoDict") != nil) {
            
            let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
            postData["hashkey"] = userInfoDict["hashKey"]!
            postData["customer_id"] = userInfoDict["customerId"]!;
            postData["store_id"] = UserDefaults.standard.value(forKey: "storeId") as? String
            
        } else {
            
            self.getCartList()
            return;
        }
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);
        
        API().callAPI(endPoint: "mobiconnect/removeStoreCredit", method: .POST, param: postData) { (json_res, err) in
            
            DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                if err == nil {
                    
                    let json = json_res[0]
                    
                    if json["success"].stringValue == "true" {
                        self.updateStoreCredit(isForUpdate: true, value: false)
                    }
                    
                    self.getCartList()
                }
            }
        }
    }
    
    //MARK:- CART LIST
    func getCartList() {
        
        var postData = [String:String]()
        
        let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
        postData["hashkey"] = userInfoDict["hashKey"]!
        postData["customer_id"] = userInfoDict["customerId"]!;
        postData["store_id"] = UserDefaults.standard.value(forKey: "storeId") as? String
        
        if let cart_id = self.defaults.object(forKey: "cartId") as? String {
            postData["cart_id"] = cart_id;
        }
        
        postData["country_id"] = APP_DEL.selectedCountry.country_code ?? "SA"
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);
        
        API().callAPI(endPoint: "mobiconnect/checkout/viewcart", method: .POST, param: postData) { (json_res, err) in
            
            DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                if err == nil {
                    
                    self.viewCartJson = json_res[0]
                    self.updateCartList()
                }
            }
        }
    }
    
    func getEditCart(isUpdateQty:Bool? = false, selectedSize: String, selectedQty : String, selectedSizeValue: String = "") {
        
        // Note A: Check is same size product is selected then remove old one and change size. Not application on qty change cases
        if (products.count > 0 && !(isUpdateQty ?? false)){
            for indexProduct in 0..<products.count {
                let recProduct =  products[indexProduct]
                let filt = APP_DEL.arrGftCardData.first(where: { (item) -> Bool in ((item.child_product_id ?? "") == (recProduct.child_product_id ?? ""))})
                if filt == nil {
                    if (recProduct.product_id == selectedCartItem?.product_id){
                        if (recProduct.options_selected?.first?.value == selectedSizeValue){
                            // remove product here and on suceess callback of remove api we will call again Edit cart
                            self.removeProduct(index: indexProduct, isFromEditCart: true, selectedSize: selectedSize,selectedSizeValue: selectedSizeValue)
                            return
                        }
                    }
                }
            }
        }
        
        var postData = [String:String]()
        
        if let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] {
            
            postData["hashkey"] = userInfoDict["hashKey"]!
            postData["customer_id"] = userInfoDict["customerId"]!;
        } else {
            postData["customer_id"] = "0";
        }
        if let store_id = self.defaults.object(forKey: "storeId") as? String {
            postData["store_id"] = store_id;
        }
        
        if let cart_id = self.defaults.object(forKey: "cartId") as? String {
            postData["cart_id"] = cart_id;
        }
        
        
        if let item_id = self.selectedCartItem?.item_id as? String {
            postData["item_id"] = item_id;
        }
        
        if var qty = Int(selectedQty) {
//            if (Int(selectedCartItem?.options_selected?.first?.max_qty ?? "0") ?? 0 < qty){
//                qty = Int(selectedCartItem?.options_selected?.first?.max_qty ?? "0") ?? 0
//            }
            var maxQty = 0
            if let optionsSizeArray = selectedCartItem?.options_size {
                for rec in optionsSizeArray {
                    if ((rec.value ?? "") == selectedSizeValue){
                        maxQty = Int(rec.max_qty ?? "1") ?? 1
                    }
                }
            }
            qty = min(maxQty, qty) // if size change then we need to set minimum available qty
            qty = min(Int(max_qty_all_products) ?? 1, qty) // if size change then we need to set minimum available qty

            postData["qty"] = "\(qty)";
        }
        
//        var super_attribute = "{";
//        super_attribute += "\""+("\(self.selectedCartItem?.options_selected?.first?.option_id ?? 0)")+"\":";
//        super_attribute += "\""+(self.selectedCartItem?.options_selected?.first?.option_value ?? "")+"\",";
//
//        if (super_attribute.last! == ",") {
//
//            super_attribute = super_attribute.substring(to: super_attribute.index(before: super_attribute.endIndex));
//        }
//        super_attribute += "}";
        postData["super_attribute"] = selectedSize;
        
//        "item_id" : "4678078",
//               "qty" : "2",
//               "customer_id" : "47743",
//               "super_attribute" : "{\"169\":\"50\"}",
//               "store_id" : "1",
//               "hashkey" : "lIYzHM25oQFdq2svJumtoaWCRXiDaC1T",
//               "cart_id" : "5885821"
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader_withlockedbackground(me: self);
//        https://dev05.nejree.com/rest/V1/mobiconnect/checkout/editcart
        API().callAPI(endPoint: "mobiconnect/checkout/editcart", method: .POST, param: postData) { (json_res, err) in
            
            DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                if err == nil {
                    if (json_res[0]["success"].stringValue.lowercased() == "true".lowercased()) {
                        self.removeStoreCredit()
                    } else {
                        // show alert
                        self.selectedCartItem = nil
//                        var strAlertMsg = APP_LBL().cart_product_requested_qty_not_available
//                        strAlertMsg = strAlertMsg.replacingOccurrences(of: "X*", with: self.selectedCartItem?.options_selected?.first?.value ?? "")
                        
                        let strAlertMsg = APP_LBL().you_can_not_add_more_than_5_quantity
                        
                        cedMageHttpException.showAlertView(me: self, msg: strAlertMsg, title: APP_LBL().error)
                    }
                } else {
                    self.selectedCartItem = nil
                    // internet or server issue
                }
            }
        }
    }
    
    func updateCartList() {
        
        self.products.removeAll()
                
        if (viewCartJson["success"].stringValue.lowercased() == "false".lowercased()) {
            self.defaults.removeObject(forKey: "cartId")
        }
        
        if self.isSelectCustomAddress != true {
         
//            if let addressObject = viewCartJson["data"]["default_address"].object as? [String : Any] {
//               addressObjApplePay = addressObject
//            } else {
               addressObjApplePay = tempAddressObjApplePay
            //}
        }
             
        do {
            
            let catData = try viewCartJson["data"]["products"].rawData()
            self.products = try JSONDecoder().decode([CartProduct].self, from: catData)
            if let cart_id = try viewCartJson["data"]["cart_id"].stringValue as? String {
                self.defaults.setValue(cart_id, forKey: "cartId")
            }
        } catch {
            
        }
        
        
        
        self.total.removeAll()
        
        self.total["amounttopay"] = viewCartJson["data"]["total"][0]["amounttopay"].stringValue
        self.total["shipping_amount"] = viewCartJson["data"]["total"][0]["shipping_amount"].stringValue
        self.total["discount_amount"] = viewCartJson["data"]["total"][0]["discount_amount"].stringValue
        self.total["tax_amount"] = viewCartJson["data"]["total"][0]["tax_amount"].stringValue
        self.total["grandtotal"] = viewCartJson["data"]["grandtotal"].stringValue
        self.total["grandtotal_without_currency"] = viewCartJson["data"]["grandtotal_without_currency"].stringValue
        self.total["coupon"] = viewCartJson["data"]["coupon"].stringValue
        self.total["is_discount"] = viewCartJson["data"]["is_discount"].stringValue
                            
        self.total["shipping_charge_new"] = viewCartJson["data"]["shipping_charge_new"].stringValue
          self.total["free_shipping_remaining"] = viewCartJson["data"]["free_shipping_remaining"].stringValue
        
        self.store_creditwithoutcurrency = viewCartJson["data"]["store_creditwithoutcurrency"].stringValue
        
        self.checkNoData()
        
        self.updateDetail()
    }
    func checkNoData() {
           
           if self.products.count == 0 {
               self.scrolVIEW.isHidden = true
               self.viewApplePay.isHidden = true
               self.lblNoData.isHidden = false
           } else {
               self.scrolVIEW.isHidden = false
               self.viewApplePay.isHidden = false
               self.lblNoData.isHidden = true
           }
       }
       
       func updateDetail() {
           
           self.tblView.reloadData()
           
           self.isGiftCardExist()
           
           if addressObjApplePay == nil {
               
               self.lblAddCountry.text = ""
               self.lblAddCity.text = ""
               self.lblAddNeighbourhood.text = ""
               self.lblAddPhone.text = ""
               self.lblAddName.text = ""
               
               self.btnChangeAddress.setTitle(APP_LBL().add.uppercased(), for: .normal)
               
           } else {
               
              // self.lblAddCountry.text = self.addressObj?["country_id"] as? String ?? ""
               
               let value=UserDefaults.standard.value(forKey: "AppleLanguages") as! [String]
               if value[0] == "ar" {
                   
                   self.lblAddCountry.text = "المملكة العربية السعودية"
                   
               } else {
                   
                   self.lblAddCountry.text = "Saudi Arabia"
               }
               
            self.lblAddCountry.text =  addressObjApplePay?["country"] as? String ?? ""
            
            self.lblAddCity.text = addressObjApplePay?["city"] as? String ?? ""
            self.lblAddNeighbourhood.text = addressObjApplePay?["neighbour"] as? String ?? ""
             self.lblAddPhone.text = "\(APP_DEL.selectedCountry.phone_code ?? "+966") \(addressObjApplePay?["phone"] as? String ?? "")"
           
               var tempAddName = ""
               tempAddName = addressObjApplePay?["firstname"] as? String ?? ""
               if tempAddName == "" {
                   tempAddName = (addressObjApplePay?["lastname"] as? String ?? "")
               } else {
                   tempAddName = (tempAddName + " " + (addressObjApplePay?["lastname"] as? String ?? ""))
               }
               self.lblAddName.text = tempAddName
               
               var selectedAddress = [String:String]()
               selectedAddress["name"] = addressObjApplePay?["firstname"] as? String ?? ""
               selectedAddress["country"] = self.lblAddCountry.text
               selectedAddress["city"] = addressObjApplePay?["city"] as? String ?? ""
               selectedAddress["phone"] = addressObjApplePay?["telephone"] as? String ?? ""
               selectedAddress["street"] = addressObjApplePay?["street"] as? String ?? ""
               selectedAddress["neighbour"] = addressObjApplePay?["neighbour_name"] as? String ?? ""
               defaults.setValue(selectedAddress, forKey: "selectedAddress")
               defaults.synchronize()
               
               
               btnChangeAddress.setTitle(APP_LBL().change.uppercased(), for: .normal)
           }
           
           
           if (self.total["coupon"] != "") && (self.total["is_discount"] == "true") {
               
               self.btnApplyPromoCode.setTitle(APP_LBL().remove.uppercased(), for: UIControl.State.normal);
               self.txtPromoCode.text = self.total["coupon"];
               
           } else {
               
               self.btnApplyPromoCode.setTitle(APP_LBL().apply.uppercased(), for: UIControl.State.normal);
               self.txtPromoCode.text = ""
           }
           
           self.updateStoreCredit(isForUpdate: false, value: false)
           
           if (total["discount_amount"] == "SAR 0") {
               self.lblDiscountValue.text = "0"
           } else {
               self.lblDiscountValue.text = self.total["discount_amount"]
           }

           if (total["tax_amount"] == "SAR 0") {
               self.lblTaxesValue.text = "0"
           } else {
               self.lblTaxesValue.text = self.total["tax_amount"]
           }
           
           
        self.lblRequiredFreeShipping.font  = UIFont(name: "Cairo-Regular", size: 16)!
        
           if self.total["free_shipping_remaining"] == "0"{
               
            self.viewFreeShipping.superview?.isHidden = false
               
               self.lblRequiredFreeShipping.text = APP_LBL().your_order_qualify_for_free_shipping
               
               progressView.progress = 1.0
                          
               progressView.setProgress(progressView.progress, animated: true)
            progressView.trackTintColor = UIColor.black
            progressView.progressTintColor = UIColor.init(hexString: "#FCB015")
            
           }
           else if self.total["free_shipping_remaining"] == "-1"
           {
               self.viewFreeShipping.superview?.isHidden = true
               self.lblRequiredFreeShipping.text = ""
               progressView.progress = 0.0
               progressView.setProgress(progressView.progress, animated: true)
           }
           else
           {
            
            
            progressView.trackTintColor = UIColor.black
            progressView.progressTintColor = UIColor.red
            
               self.viewFreeShipping.superview?.isHidden = false
               let stringFreeShipping = APP_LBL().free_shipping_remaining as String
               let strAddMoreAmount = APP_DEL.selectedCountry.getCurrencySymbol() + " " + (self.total["free_shipping_remaining"] ?? "") as String
               self.lblRequiredFreeShipping.text = stringFreeShipping.replacingOccurrences(of: "*", with: strAddMoreAmount)
               
           
               let FreeShippingAmount = Double(self.total["free_shipping_remaining"] ?? "0.0") ?? 0.0
               let minAmoutShipping = Double(APP_DEL.extrafee_minimum_order_amount) ?? 0.0
               let FinalProgress =  (FreeShippingAmount * 100.0) / minAmoutShipping
               
               progressView.progress = Float((100.0 - FinalProgress)/100.0)
               progressView.setProgress(progressView.progress, animated: true)
            
                let range = (self.lblRequiredFreeShipping.text! as NSString).range(of: strAddMoreAmount)
                let mutableAttributedString = NSMutableAttributedString.init(string: self.lblRequiredFreeShipping.text ?? "")
                mutableAttributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.init(hexString: "#FCB015") ?? UIColor.black, range: range)
                self.lblRequiredFreeShipping.attributedText = mutableAttributedString
           }

            if self.total["shipping_charge_new"] != "0" {
                
                
                self.lblShippingValue.text = (APP_DEL.selectedCountry.getCurrencySymbol().uppercased() + " " + (self.total["shipping_charge_new"] ?? "0"))
            }
            else
            {
                self.lblShippingValue.text = APP_LBL().free
            }
        
           
           self.lblSubTotalValue.text = total["amounttopay"]
           self.lblTotalValue.text = self.total["grandtotal"]
           
           btnApplePay.setTitle(" Pay \(self.total["grandtotal"] ?? "")", for: .normal)
           
        if isGiftCardThereInCart == false && APP_DEL.auto_apply_storecredit == "1" && self.store_creditwithoutcurrency != "0" && self.appliedStoreCreditString == "\(APP_DEL.selectedCountry.getCurrencySymbol().uppercased()) 0" && self.products.count != 0 && self.isStoreCreditApplyManually == false {
            
            
            if (self.store_creditwithoutcurrency != "") && (total["grandtotal"] != "\(APP_DEL.selectedCountry.getCurrencySymbol().uppercased()) 0") {
                            self.setStoreCredit()
                        }
            
            
        }
        
       }
       
       func isGiftCardExist() {
           
           var res = false
           
           for pro in products {
                           
               let filt = APP_DEL.arrGftCardData.first(where: { (item) -> Bool in ((item.child_product_id ?? "") == (pro.child_product_id ?? ""))})
               
               if filt != nil {
                   
                   res = true
                   
                   break;
               }
           }
           
           isGiftCardThereInCart = res
       }
    
    
    //MARK:- REMOVE PRODUCT
    var removeProductIndex = -1
    func removeProduct(index: Int, isFromEditCart:Bool? = false,selectedSize: String = "",selectedSizeValue: String = "") {
        
        var postData = [String:String]()
        
        if UserDefaults.standard.bool(forKey: "isLogin") {
            
            let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
            postData["hashkey"] = userInfoDict["hashKey"]!
            postData["customer_id"] = userInfoDict["customerId"]!;
        }
        
      
        postData["store_id"] = UserDefaults.standard.value(forKey: "storeId") as? String
        
        guard (try? products[index].product_id!) != nil else {
           
            return;
        }
        
        
        postData["product_id"]=products[index].product_id!;
        postData["item_id"]=products[index].item_id!
        
        if let cart_id = self.defaults.object(forKey: "cartId") as? String {
            postData["cart_id"] = cart_id;
        }
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader_withlockedbackground(me: self);
        
        API().callAPI(endPoint: "mobiconnect/checkout/delete/", method: .POST, param: postData) { (json_res, err) in
            
            DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                if err == nil {
                    
                    let json = json_res[0]
                    
                    if (json["success"].stringValue == "removed_successfully") {
                        
                        let filt = APP_DEL.arrGftCardData.first(where: { (item) -> Bool in ((item.child_product_id ?? "") == (self.products[index].child_product_id ?? ""))})
                        
                        if filt != nil {
                            
                            let tempIndex = APP_DEL.arrGftCardData.firstIndex { (cat) -> Bool in
                                ((self.products[index].child_product_id ?? "") == (cat.child_product_id ?? ""))
                            }
                            
                            if tempIndex != nil {
                                APP_DEL.arrGftCardData.remove(at: tempIndex!)
                            }
                        }
                    }
                    
                    if (isFromEditCart ?? false){
                        self.products.remove(at: index)
                        self.getEditCart(selectedSize: selectedSize, selectedQty: "\(self.selectedCartItem?.quantity ?? 0)",selectedSizeValue: selectedSizeValue)
                    } else {
                        self.removeStoreCredit()
                    }
                }
            }
        }
    }
    
    
    
    //MARK:- ADD TO WISHLIST
    func addToWishList(index: Int) {
        
        var postData = [String:String]()
        
        let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
        postData["hashkey"] = userInfoDict["hashKey"]!
        postData["customer_id"] = userInfoDict["customerId"]!;
        postData["product_id"] = self.products[index].product_id!
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);
        
        if let storeID =  UserDefaults.standard.value(forKey: "storeId") as? String {
            postData["store_id"] = storeID
        }
        
        API().callAPI(endPoint: "wishlist/addwishlist", method: .POST, param: postData) { (json_res, err) in
            
            DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                if err == nil {
                    
                    let json = json_res[0]["data"][0]
                    
                    let status = json["status"].stringValue;
                    let msg = json["message"].stringValue;
                    
                    var toastMessg = ""
                    cedMageLoaders.removeLoadingIndicator(me: self);
                    
                    if status == "true" {
                        
                        toastMessg = "\(self.products[index].product_name ?? "") " + APP_LBL().is_successfully_added_to_the_wishlist
                        
                        
                        Analytics.logEvent(AnalyticsEventAddToWishlist, parameters: [
                            AnalyticsParameterCurrency : APP_DEL.selectedCountry.getCurrencySymbol().uppercased(),
                            AnalyticsParameterItemCategory : "Shoes",
                            AnalyticsParameterItemID : self.products[index].product_id as Any,
                            AnalyticsParameterQuantity : "1",
                            AnalyticsParameterItemName : self.products[index].product_name ?? "",
                            AnalyticsParameterPrice : self.products[index].sub_total ?? "",
                            AnalyticsParameterValue : "",
                        ])
                        
                        let detail : [AnalyticKey:String] = [
                            .Currency : APP_DEL.selectedCountry.getCurrencySymbol().uppercased(),
                            .ItemCategory : "Shoes",
                            .ItemID : self.products[index].product_id ?? "",
                            .Quantity : "1",
                            .ItemName : self.products[index].product_name ?? "",
                            .Price : self.products[index].sub_total ?? "",
                            .PriceWithoutCurrency : "\(self.products[index].subtotal_price_without_currency ?? 0.0)"
                        ]
                        API().setEvent(eventName: .AddToWishlist, eventDetail: detail) { (json, err) in }
                        
                        if self.products.count > index {
                            
                            self.products[index].is_wishlist = (((self.products[index].is_wishlist ?? "") == "0") ? "1" : "0")
                            self.tblView.reloadData()
                        }
                        
                        APP_DEL.isWishListShouldReload = true
                        
                    } else {
                        toastMessg = msg
                    }
                    
                    self.view.isUserInteractionEnabled = false;
                    self.view.makeToast(toastMessg, duration: 2, position: .center, title: nil, image: nil, style: nil, completion: {
                        Void in
                        
                        self.view.isUserInteractionEnabled = true;
                    })
                }
                
                APP_DEL.getCartCount()
            }
        }
    }
    
    //MARK:- REMOVE FROM WISHLIST
    func removeFromWishList(index: Int) {
        
        var postData = [String:String]()
        
        let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
        postData["hashkey"] = userInfoDict["hashKey"]!
        postData["customer_id"] = userInfoDict["customerId"]!;
        postData["product_id"] = self.products[index].product_id!
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);
        
        API().callAPI(endPoint: "wishlist/removewishlist", method: .POST, param: postData) { (json_res, err) in
            
            DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                if err == nil {
                    
                    if self.products.count > index {
                        
                        self.products[index].is_wishlist = (((self.products[index].is_wishlist ?? "") == "0") ? "1" : "0")
                        
                         APP_DEL.isWishListShouldReload = true
                        
                        self.tblView.reloadData()
                    }
                }
                
                APP_DEL.getCartCount()
            }
        }
    }
    
    //MARK:- SAVE BILLING ADDRESS
    func saveBillingAddress() {
        
        var postData = [String:String]()
        
        let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
        postData["hashkey"] = userInfoDict["hashKey"]!
        postData["customer_id"] = userInfoDict["customerId"]!;
        
        if let cart_id = self.defaults.object(forKey: "cartId") as? String {
            postData["cart_id"] = cart_id;
        }
        
        if let store_id = self.defaults.object(forKey: "storeId") as? String {
            postData["store_id"] = store_id;
        }
        
        postData["address_id"] = addressObjApplePay?["entity_id"] as? String ?? "";
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);
        
        API().callAPI(endPoint: "mobiconnect/checkout/savebillingshipping", method: .POST, param: postData) { (json_res, err) in
            
            DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                if err == nil {
                    
                    let json = json_res[0]
                    
                    if json["success"].stringValue == "true" {
                        
                        self.saveShippingPayament()
                    }
                    else
                    {
                        APP_DEL.showAlertWithOkButton(parentVC: self, strTitle: APP_LBL().error, strMsg: APP_LBL().country_not_available_for_shipping)
                    }
                    
                   
                }
            }
        }
    }
    
    //MARK:- SAVE SHIPPING PAYMENT
    func saveShippingPayament()  {

        var postData = [String:String]()
        if let cart_id = self.defaults.object(forKey: "cartId") as? String {
            postData["cart_id"] = cart_id;
        }
        
        if total["grandtotal_without_currency"] == "0" {
            
             postData["payment_method"] = "free";
        }
        else
        {
            postData["payment_method"] = "checkoutcom_apple_pay";
        }
        
        postData["shipping_method"] = "freeshipping_freeshipping"

        postData["Role"] = "USER";
        postData["email"] = "test@test.com";
        
        guard let storeId = defaults.value(forKey: "storeId") as? String else {return}
        postData["store_id"] = storeId
        
        let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
        postData["hashkey"] = userInfoDict["hashKey"]!
        postData["customer_id"] = userInfoDict["customerId"]!;
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);
        
        API().callAPI(endPoint: "mobiconnect/checkout/saveshippingpayament", method: .POST, param: postData) { (json_res, err) in
            
            DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                if err == nil {
                    
                    var tempParameterItemID = ""
                    
                    for prod in self.products {
                        if (prod.item_id ?? "") != "" {
                            tempParameterItemID = tempParameterItemID + ((tempParameterItemID == "") ? "\(prod.item_id ?? "")" : ",\(prod.item_id ?? "")")
                        }
                    }
                                        
                    print(tempParameterItemID)
                    
                    Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
                        AnalyticsParameterContentType : "Checkout ApplePay",
                        AnalyticsParameterItemID : tempParameterItemID,
                    ])
                    
                    let detail : [AnalyticKey:String] = [
                        .ContentType : "Checkout ApplePay",
                        .ItemID : tempParameterItemID
                    ]
                    API().setEvent(eventName: .SelectContent, eventDetail: detail) { (json, err) in }
                                        
                    self.saveAppleOrder()
                }
            }
        }
    }
    
    //MARK:- SAVE SHIPPING PAYMENT FOR CLEAR COD
    func saveShippingPayamentForClearCOD() {
        
        var postData = [String:String]()
        
        postData["Role"] = "USER";
        
        if let cartID =  UserDefaults.standard.value(forKey: "cartId") as? String {
            postData["cart_id"] = cartID
        }
        
        if let storeID =  UserDefaults.standard.value(forKey: "storeId") as? String {
            postData["store_id"] = storeID
        }
        
        if let userInfoDict = UserDefaults.standard.object(forKey: "userInfoDict") as? [String: String] {
            
            postData["hashkey"] = userInfoDict["hashKey"]
            postData["customer_id"] = userInfoDict["customerId"]
            postData["email"] = userInfoDict["email"]
        }
        
        postData["payment_method"] = "checkoutcom_apple_pay";
        
        postData["shipping_method"] = "freeshipping_freeshipping";
        
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);
        
        API().callAPI(endPoint: "mobiconnect/checkout/saveshippingpayament", method: .POST, param: postData) { (json, err) in
            
            DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                self.getCartList()
            }
        }
    }
    
    //MARK:- SAVE APPLE ORDER
    func saveAppleOrder() {
        
        var strCouponCode = ""
        strCouponCode = self.total["coupon"] ?? ""
        
        Analytics.logEvent(AnalyticsEventBeginCheckout, parameters: [
            AnalyticsParameterCurrency : APP_DEL.selectedCountry.getCurrencySymbol().uppercased(),
            AnalyticsParameterCoupon : strCouponCode,
            AnalyticsParameterItemID : getCoded().product_id,
            AnalyticsParameterQuantity : getCoded().quantity,
            AnalyticsParameterPrice : getCoded().subTotal,
            AnalyticsParameterItemName : getCoded().productName,
            AnalyticsParameterValue : total["grandtotal"]!
        ])
        
        let detail : [AnalyticKey:String] = [
            .Currency : APP_DEL.selectedCountry.getCurrencySymbol().uppercased(),
            .Coupon : strCouponCode,
            .ItemID : getCoded().product_id,
            .Quantity : getCoded().quantity,
            .Price : getCoded().subTotal,
            .ItemName : getCoded().productName,
            .Value : total["grandtotal"] ?? "",
            .PriceWithoutCurrency : self.total["grandtotal_without_currency"] ?? ""
        ]
        API().setEvent(eventName: .BeginCheckout, eventDetail: detail) { (json, err) in }
        
        var postData = [String:String]()

        if let cart_id = self.defaults.object(forKey: "cartId") as? String {
            postData["cart_id"] = cart_id;
        } else {
            postData["cart_id"] = "0";
        }
        
        if let email = self.defaults.object(forKey: "EmailId") as? String {
            postData["email"] = email;
        }

        if let userInfoDict = UserDefaults.standard.object(forKey: "userInfoDict") as? [String: String] {
            
            postData["hashkey"] = userInfoDict["hashKey"]
            postData["customer_id"] = userInfoDict["customerId"]
        }
        
        if let store_id = self.defaults.object(forKey: "storeId") as? String {
            postData["store_id"] = store_id;
        }
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);
        
        API().callAPI(endPoint: "mobiconnect/checkout/saveorder", method: .POST, param: postData) { (json_res, err) in
            
            DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                if err == nil {
                    
                    let json = json_res[0]
                    
                    if (json["success"].stringValue == "true") {
                        
                        self.orderStatusData["orderId"] = json["order_id"].stringValue
                        self.orderStatusData["orderStatus"] = json["success"].stringValue
                        self.total["grandtotal"] = json["grandtotal"].stringValue
                        self.orderStatusData["order_date"] = json["order_date"].stringValue
                        
                        
                        print(self.total)
                        
                        var arrTempBlocks : [ItemBlock] = []
                        for bl in self.products {
                            arrTempBlocks.append(ItemBlock(productId: bl.product_id, productName: bl.product_name, productPrice: bl.sub_total, productQuantity: "\(bl.quantity ?? 0)"))
                        }
                        let finalRes = self.getItemBlock(itemBlocks: arrTempBlocks)
                        
                        print(finalRes.jsonString)
                        print(finalRes.json)
                        
                        
                        if self.total["grandtotal_without_currency"] == "0" {
                                   
                               let strCoupon : String = self.total["coupon"] ?? ""
                                                         
                             Analytics.logEvent(AnalyticsEventPurchase, parameters: [
                                 AnalyticsParameterValue : self.total,
                                 AnalyticsParameterCurrency : APP_DEL.selectedCountry.getCurrencySymbol().uppercased(),
                                 AnalyticsParameterCoupon : strCoupon,
                                 AnalyticsParameterTax : "",
                                 AnalyticsParameterItemID : finalRes.jsonString,
                                 AnalyticsParameterTransactionID : self.orderStatusData["orderId"] as Any,

                             ])
                            
                            let detail : [AnalyticKey:String] = [
                                .Value : self.total["grandtotal"] ?? "",
                                .Currency : APP_DEL.selectedCountry.getCurrencySymbol().uppercased(),
                                .Coupon : strCoupon,
                                .Tax : "",
                                .ItemID : finalRes.jsonString,
                                .TransactionID : self.orderStatusData["orderId"] ?? "",
                                .PriceWithoutCurrency : self.total["grandtotal_without_currency"] as? String ?? ""
                            ]
                            API().setEvent(eventName: .Purchase, eventDetail: detail) { (json, err) in }
                             
                             self.defaults.removeObject(forKey: "guestEmail");
                             self.defaults.removeObject(forKey: "cartId");
                             self.defaults.removeObject(forKey: "appliedCoupon");
                             self.defaults.setValue("0", forKey: "items_count")
                            
                            APP_DEL.isAccountPageRefreshAfterOrder = true
                            
                            self.sendGiftCardSMS(order_id: self.orderStatusData["orderId"] ?? "")
                       }
                       else
                       {
                            APP_DEL.isAccountPageRefreshAfterOrder = true
                        
                            self.responsePassApplePay = json.rawValue
                                                 
                            self.getCheckoutPaymentTokenFromServer()
                       }
                    }
                    
                } else {
                    
                    let strCoupon : String = self.total["coupon"] ?? ""//self.defaults.value(forKey: "appliedCoupon") as? String ?? ""
                    
                    print("\(self.products)")
                    
                    var arrTempBlocks : [ItemBlock] = []
                    for bl in self.products {
                        arrTempBlocks.append(ItemBlock(productId: bl.product_id, productName: bl.product_name, productPrice: bl.sub_total, productQuantity: "\(bl.quantity ?? 0)"))
                    }
                    let finalRes = self.getItemBlock(itemBlocks: arrTempBlocks)
                    
                    print(finalRes.jsonString)
                    print(finalRes.json)
                    
                    
                    let Parameters = ["COUPON" : strCoupon as Any,
                                      "CURRENCY" : APP_DEL.selectedCountry.getCurrencySymbol().uppercased() as Any,
                                      "ITEMS" : finalRes.jsonString,
                                      "VALUE" : "",
                    ]

                    Analytics.logEvent("failed_checkout", parameters: Parameters)
                    
                    let detail : [AnalyticKey:String] = [
                        .Coupon : strCoupon,
                        .Currency : APP_DEL.selectedCountry.getCurrencySymbol().uppercased(),
                        .Items : finalRes.jsonString,
                        .Value : ""
                    ]
                    API().setEvent(eventName: .FailedCheckout, eventDetail: detail) { (json, err) in }
                }
            }
        }
    }
    
    func sendGiftCardSMS(order_id: String) {

        guard let storeId = UserDefaults.standard.value(forKey: "storeId") as? String else {return}
        
        var postData = [String:String]()
        postData["store_id"] = storeId
        postData["order_id"] = order_id
        
        if let addressDynamic =  UserDefaults.standard.value(forKey: "selectedAddress") as? [String:Any] {
            
            postData["po_number"] = addressDynamic["phone"] as? String
        }
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);
        
        API().callAPI(endPoint: "mobiconnect/checkout/sendSmsGiftcard", method: .POST, param: postData) { (json_res, err) in
            
            DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                if err == nil {
                    
                    var arrOrderItem = [orderItems]()
                                                                       
                    arrOrderItem.removeAll()
                        
                        for i in 0..<self.products.count{
                                                   
                        
                        let dictdata = self.products[i]
                                                   
                        arrOrderItem.append(orderItems(product_type: dictdata.product_type,
                                                       product_name: dictdata.product_name,
                                                       product_price: dictdata.sub_total,
                                                       product_id: dictdata.product_id,
                                                       rowsubtotal: dictdata.sub_total,
                                                       product_qty: "\(dictdata.quantity ?? 0)",
                                                       product_image: dictdata.product_image,
                                                       selected: false,
                                                       itemId: dictdata.item_id,
                                                       optionSize: (dictdata.options_selected?.first?.value ?? "")))
                                                   
                        }
                        
                        
                    let storyboard = UIStoryboard(name: "Main", bundle: nil);
                    let viewController = storyboard.instantiateViewController(withIdentifier: "cedMagePaymentSuccess") as! cedMagePaymentSuccess;
                    viewController.order_id = self.orderStatusData["orderId"]!
                     viewController.orderProduct = arrOrderItem
                    viewController.order_date = self.orderStatusData["order_date"]!
                    self.navigationController?.viewControllers = [viewController];
                }
            }
        }
    }
    
    func getCheckoutPaymentTokenFromServer() {
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);
        
        API().callAPI(endPoint: "checkoutkeys/", method: .GET, param: [:]) { (json_res, err) in
            
            DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                if err == nil {
                    
                    let json = json_res[0]

                    self.strPublicKey = json["isPublicKey"].stringValue
                    self.strSecretKey = json["isSecretKey"].stringValue
                    
                    self.strCurrentCheckoutMode = json["mode"].stringValue
                    
                    self.createPaymentApiRequest(response: self.responsePassApplePay, amount: self.total["grandtotal"]!, email: self.orderEmail, orderID: self.orderStatusData["orderId"]!)
                }
            }
        }
    }
    
    override func recieveResponse(data: Data?, requestUrl: String?, response: URLResponse?) {
        guard let data = data else {return}
        do {
            
            let jsonTemp = try JSON(data: data)
            var json = jsonTemp[0]
            
            print(json)
            if requestUrl == finalOrderCheck {
                if json["success"].stringValue != "false" {
                    self.defaults.removeObject(forKey: "guestEmail");
                    self.defaults.removeObject(forKey: "cartId");
                    self.defaults.setValue("0", forKey: "items_count")
                    
                    var arrTempBlocks : [ItemBlock] = []
                    for bl in self.products {
                        arrTempBlocks.append(ItemBlock(productId: bl.product_id, productName: bl.product_name, productPrice: bl.sub_total, productQuantity: "\(bl.quantity ?? 0)"))
                    }
                    let finalRes = self.getItemBlock(itemBlocks: arrTempBlocks)
                    
                    print(finalRes.jsonString)
                    print(finalRes.json)
                    
                    
                    let strCoupon : String = self.total["coupon"] ?? ""//self.defaults.value(forKey: "appliedCoupon") as? String ?? ""
                    
                    Analytics.logEvent(AnalyticsEventPurchase, parameters: [
                        AnalyticsParameterValue : self.total,
                        AnalyticsParameterCurrency : APP_DEL.selectedCountry.getCurrencySymbol().uppercased(),
                        AnalyticsParameterCoupon : strCoupon,
                        AnalyticsParameterTax : "",
                        AnalyticsParameterItemID : finalRes.jsonString,
                        AnalyticsParameterTransactionID : self.orderStatusData["orderId"] as Any,

                    ])
                    
                    let detail : [AnalyticKey:String] = [
                        .Value : self.total["grandtotal"] ?? "",
                        .Currency : APP_DEL.selectedCountry.getCurrencySymbol().uppercased(),
                        .Coupon : strCoupon,
                        .Tax : "",
                        .ItemID : finalRes.jsonString,
                        .TransactionID : self.orderStatusData["orderId"] ?? "",
                        .PriceWithoutCurrency : self.total["grandtotal_without_currency"] as? String ?? ""
                    ]
                    API().setEvent(eventName: .Purchase, eventDetail: detail) { (json, err) in }
                    
                    APP_DEL.arrGftCardData = []
                    
                    self.view.makeToast(APP_LBL().payment_successful, duration: 2, position: .center, title: nil, image: nil, style: nil, completion: {
                        Void in
                        let storyboard = UIStoryboard(name: "Main", bundle: nil);

                        let viewController = storyboard.instantiateViewController(withIdentifier: "cedMagePaymentSuccess") as! cedMagePaymentSuccess;
                        viewController.order_id = self.orderStatusData["orderId"]!
                        viewController.order_date = self.orderStatusData["order_date"]!
                        
                        viewController.mainvc = self
                        self.navigationController?.viewControllers = [viewController];

                     
                    })
                }else if json["success"].stringValue == "false"{

                    
                    self.view.makeToast(APP_LBL().order_payment_failure, duration: 2, position: .center, title: nil, image: nil, style: nil, completion: {
                        Void in
                        //self.removeCoupon()


                    })
                }

            }
                       
        } catch {print("error occured in catch section")}
    }
    
    func getCoded() -> (json: [[String:String]], jsonString: String, product_id : String, productName : String, subTotal : String, quantity : String) {
            
        var arrCoded : [AnalyticCartProduct] = []
        
        for d in self.products {
            arrCoded.append(AnalyticCartProduct(productId: d.product_id, productName: d.product_name, productPrice: d.sub_total?.components(separatedBy: " ").first, productQuantity: "\(d.quantity ?? 0)"))
        }
        
        var product_id : [String] = []
        var productName : [String] = []
        var subTotal : [String] = []
        var quantity : [String] = []
        
        for d in self.products {
            
            product_id.append(d.product_id ?? "")
            productName.append(d.product_name ?? "")
            subTotal.append(d.sub_total?.components(separatedBy: " ").first ?? "")
            quantity.append("\(d.quantity ?? 0)")
        }
        
        do {
            let data = try JSONEncoder().encode(arrCoded)
            let json : [[String : String]] = try JSONSerialization.jsonObject(with: data, options: []) as! [[String : String]]
            return (json: json,
                    jsonString: String(data: data, encoding: .utf8)!,
                    product_id : product_id.joined(separator: ","),
                    productName : productName.joined(separator: ","),
                    subTotal : subTotal.joined(separator: ","),
                    quantity : quantity.joined(separator: ","))
        } catch {
            print("ERROR...")
            return (json: [],
                    jsonString: "",
                    product_id : "",
                    productName : "",
                    subTotal : "",
                    quantity : "")
        }
    }
    
    func getItemBlock(itemBlocks: [ItemBlock]) -> (json: [[String:String]], jsonString: String) {
        
        do {
            let data = try JSONEncoder().encode(itemBlocks)
            let json : [[String : String]] = try JSONSerialization.jsonObject(with: data, options: []) as! [[String : String]]
            return (json: json, jsonString: String(data: data, encoding: .utf8)!)
        } catch {
            print("ERROR.")
            return (json: [], jsonString: "")
        }
    }
    
    var popupSize = FFPopup()
    @objc func btnSizeAction(_ sender: UIButton) {
        if (allow_size_selection_incart ?? "0" != "1"){
            return
        }
        if let sizeArray = products[sender.tag].options_size {
            if (sizeArray.count > 0){
                selectedCartItem = products[sender.tag]
                let vi = Bundle.main.loadNibNamed("ProductSizePopup", owner: self, options: nil)?.first as! ProductSizePopup
                vi.parentVC = self
                vi.arrSize = sizeArray
                vi.setContent()
                vi.btnDone.addTarget(self, action: #selector(self.btnSizeDoneAction(_:)), for: .touchUpInside)
                vi.frame = CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: SCREEN_HEIGHT)
                
                self.popupSize = FFPopup(contetnView: vi, showType: .slideInFromBottom, dismissType: .slideOutToBottom, maskType: .dimmed, dismissOnBackgroundTouch: true, dismissOnContentTouch: false)
                self.popupSize.showInDuration = 0.3
                
                let layout = FFPopupLayout(horizontal: .left, vertical: .bottom)
                self.popupSize.show(layout: layout)
            }
        }
    }
    
    @objc func btnSizeDoneAction(_ sender: UIButton) {
        
        print("Size Done")
        popupSize.dismiss(animated: true)
    }
    
    var popupQty = FFPopup()
    @objc func btnQtyAction(_ sender: UIButton) {
        if (allow_size_selection_incart ?? "0" != "1"){
            return
        }
        if let optionsSizeArray = products[sender.tag].options_size {
            for rec in optionsSizeArray {
                if ((rec.value ?? "") == (products[sender.tag].options_selected?.first?.value ?? "")){
                    if (Int(rec.max_qty ?? "0") ?? 0 > 0){
                        selectedCartItem = products[sender.tag]
                        selectedCartItem?.options_selected?[0].max_qty = rec.max_qty ?? "0"
                        let vi = Bundle.main.loadNibNamed("ProductQtyPopup", owner: self, options: nil)?.first as! ProductQtyPopup
                        vi.parentVC = self
                        vi.maxQty = Int(rec.max_qty ?? "0") ?? 0
                        vi.setContent()
                        vi.btnDone.addTarget(self, action: #selector(self.btnQtyDoneAction(_:)), for: .touchUpInside)
                        vi.frame = CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: SCREEN_HEIGHT)
                        
                        self.popupQty = FFPopup(contetnView: vi, showType: .slideInFromBottom, dismissType: .slideOutToBottom, maskType: .dimmed, dismissOnBackgroundTouch: true, dismissOnContentTouch: false)
                        self.popupQty.showInDuration = 0.3
                        
                        let layout = FFPopupLayout(horizontal: .left, vertical: .bottom)
                        self.popupQty.show(layout: layout)
                    }
                }
            }
        }
        
    }
    
    @objc func btnQtyDoneAction(_ sender: UIButton) {
        
        print("Qty Done")
        popupQty.dismiss(animated: true)
    }
    
    @objc func ActionProductDetailRedirection(_ sender: UIButton) {
        let filt = APP_DEL.arrGftCardData.first(where: { (item) -> Bool in ((item.child_product_id ?? "") == (products[sender.tag].child_product_id ?? ""))})
        
        if filt != nil {
            return;
        }
        
//        //if products[indexPath.row].item_error != "" {
//        if ((products[indexPath.row].item_error?.first?.type ?? "") != "") {
//            self.view.makeToast(APP_LBL().product_is_out_of_stock, duration: 1.0, position: .center)
//            return
//        }
        
        let vc = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "productViewController") as! productViewController
        APP_DEL.productIDglobal = products[sender.tag].product_id ?? ""
        vc.product_id = products[sender.tag].product_id ?? ""
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
}





extension NejreeAppleCheckoutVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return products.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "NejreeAppleCheckoutCell", for: indexPath) as! NejreeAppleCheckoutCell
        cell.selectionStyle = .none
        
        cell.btnProductRedirctionImg.tag = indexPath.row
        cell.btnProductRedirctionImg.addTarget(self, action: #selector(ActionProductDetailRedirection(_:)), for: .touchUpInside)
        
        cell.btnProductRedirctionTitle.tag = indexPath.row
        cell.btnProductRedirctionTitle.addTarget(self, action: #selector(ActionProductDetailRedirection(_:)), for: .touchUpInside)
        
        cell.btnSize.tag = indexPath.row
        cell.btnSize.addTarget(self, action: #selector(btnSizeAction(_:)), for: .touchUpInside)
        
        cell.btnQty.tag = indexPath.row
        cell.btnQty.addTarget(self, action: #selector(btnQtyAction(_:)), for: .touchUpInside)
        
        cell.btnWish.tag = indexPath.row
        cell.btnWish.addTarget(self, action: #selector(btnWishProductAction(_:)), for: .touchUpInside)
        
        cell.btnDelete.tag = indexPath.row
        cell.btnDelete.addTarget(self, action: #selector(btnDeleteProductAction(_:)), for: .touchUpInside)
        
        cell.btnEditGiftCard.tag = indexPath.row
        cell.btnEditGiftCard.addTarget(self, action: #selector(btnEditGiftCardAction(_:)), for: .touchUpInside)
        
        
        let value = UserDefaults.standard.value(forKey: "AppleLanguages") as! [String]
        if value[0] == "ar" {
            cell.semanticContentAttribute = .forceRightToLeft
            cell.quantityLabel.textAlignment = .right
            cell.productName.textAlignment = .right
            cell.sizeValue.textAlignment = .right
        } else {
            cell.semanticContentAttribute = .forceLeftToRight
            cell.quantityLabel.textAlignment = .left
            cell.productName.textAlignment = .left
            cell.sizeValue.textAlignment = .left
        }
        
        let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: products[indexPath.row].total_regular_price!)
        attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 1, range: NSMakeRange(0, attributeString.length))
        
        cell.priceRegular.isHidden = true
        if products[indexPath.row].total_special_price != "no_special" {
            cell.priceLabel.text = products[indexPath.row].total_special_price;
            cell.priceRegular.attributedText = attributeString
            cell.priceRegular.isHidden = false
            cell.priceRegular.alpha = 1
            
        } else {
            cell.priceLabel.text = products[indexPath.row].total_regular_price
            cell.priceRegular.isHidden = true
            cell.priceRegular.alpha = 0
        }
        
        
        cell.btnSize.isUserInteractionEnabled = false
        cell.btnQty.isUserInteractionEnabled = false
        cell.imgQtyArrow.isHidden = true
        cell.imgSizeArrow.isHidden = true
        var productMaxQty = 0
        if (allow_size_selection_incart == "1"){
            if (products[indexPath.row].options_size?.count ?? 0 > 1){
                cell.imgSizeArrow.isHidden = false
                cell.btnSize.isUserInteractionEnabled = true
            } else  if (products[indexPath.row].options_size?.count ?? 0 == 1){
                if ((products[indexPath.row].options_size?.first?.value ?? "av") != (products[indexPath.row].options_selected?.first?.value ?? "asd")){
                    cell.imgSizeArrow.isHidden = false
                    cell.btnSize.isUserInteractionEnabled = true
                }
            }
//            else if ((Int(products[indexPath.row].options_selected?.first?.max_qty ?? "0") ?? 0) > 1) {
//                cell.imgQtyArrow.isHidden = false
//                cell.btnQty.isUserInteractionEnabled = true
//            }
                if let optionSizeArray = products[indexPath.row].options_size {
                    for rec in optionSizeArray {
                        if ((rec.value ?? "") == (products[indexPath.row].options_selected?.first?.value ?? "")){
                            productMaxQty = Int(rec.max_qty ?? "0") ?? 0
                            if ((Int(rec.max_qty ?? "0") ?? 0) > 1){
                                cell.imgQtyArrow.isHidden = false
                                cell.btnQty.isUserInteractionEnabled = true
                            } else if ((Int(rec.max_qty ?? "0") ?? 0) == 1){ // if max qty is one and selected is more than max qty
                                if ((Int(rec.max_qty ?? "0") ?? 0) < (products[indexPath.row].quantity ?? 0)){
                                    cell.imgQtyArrow.isHidden = false
                                    cell.btnQty.isUserInteractionEnabled = true
                                }
                            }
                        }
                    }
                }
        }
        
        cell.productName.text = products[indexPath.row].product_name ?? ""
        cell.quantityLabel.text = APP_LBL().qty.uppercased() + " " + "\(products[indexPath.row].quantity ?? 0)"
        cell.productImage.sd_setImage(with: URL(string: products[indexPath.row].product_image!), placeholderImage:nil)
        cell.btnEditGiftCard.isHidden = true
        
        
        let filt = APP_DEL.arrGftCardData.first(where: { (item) -> Bool in ((item.child_product_id ?? "") == (products[indexPath.row].child_product_id ?? ""))})
        if filt != nil {
            //Note A:Come here is product is gift
            var one = ""
            
            if value[0]=="ar" {
                
                one = (filt?.description ?? "")
                one = " " + (filt?.phone ?? "") + " | " + one
                one = (filt?.name ?? "") + " :" + APP_LBL().to + one
                
            } else {
                
                one = APP_LBL().to + ": " + (filt?.name ?? "")
                one = one + " | " + (filt?.phone ?? "") + " "
                one = one + (filt?.description ?? "")
            }
            
            cell.btnEditGiftCard.isHidden = false
            
            cell.lblGiftDescr.text = one
            cell.lblGiftDescr.superview?.isHidden = false
            cell.btnSize.superview?.isHidden = true
            cell.sizeNotLabel.superview?.isHidden = true
            
            if value[0] == "ar" {
                cell.productImage.sd_setImage(with: URL(string: products[indexPath.row].product_image!), placeholderImage:UIImage(named: "gift_card_ar"))
            } else {
                cell.productImage.sd_setImage(with: URL(string: products[indexPath.row].product_image!), placeholderImage:UIImage(named: "gift_card_eng"))
            }
            
            let comp = products[indexPath.row].sub_total?.components(separatedBy: " ")
            cell.lblGiftPrice.text = String(format: "%.2f", Double(comp?[1] ?? "0.0")!)
            
            cell.lblGiftPrice.isHidden = false
            cell.lblGiftPriceSAR.isHidden = false
            cell.imgQtyArrow.isHidden = true
            cell.btnQty.isUserInteractionEnabled = false
            cell.btnSize.isUserInteractionEnabled = false
            cell.lblGiftPriceSAR.text = APP_DEL.selectedCountry.getCurrencySymbol().uppercased()
            
        } else {
            
            cell.sizeValue.text = APP_LBL().size.uppercased() + ": " + (products[indexPath.row].options_selected?.first?.value ?? "")
            cell.lblGiftPrice.isHidden = true
            cell.lblGiftPriceSAR.isHidden = true
            cell.lblGiftPriceSAR.text = ""
            
            cell.lblGiftDescr.text = ""
            cell.lblGiftDescr.superview?.isHidden = true
            cell.btnSize.superview?.isHidden = false
            cell.btnQty.superview?.isHidden = false
            cell.btnQty.isUserInteractionEnabled = true
            cell.btnSize.isUserInteractionEnabled = true
            cell.sizeNotLabel.superview?.isHidden = true
        }
        
        if defaults.bool(forKey: "isLogin") == true {
            
            let filt = APP_DEL.arrGftCardData.first(where: { (item) -> Bool in ((item.child_product_id ?? "") == (products[indexPath.row].child_product_id ?? ""))})
            
            if filt != nil {
                cell.btnWish.isHidden = true
            } else {
                cell.btnWish.isHidden = false
            }
            
        } else {
            cell.btnWish.isHidden = false
        }
        
        if products[indexPath.row].is_wishlist == "0" {
            cell.btnWish.setImage(UIImage(named:"icon_wishlist_unselected"), for: .normal)
        } else {
            cell.btnWish.setImage(UIImage(named:"icon_wishlist_selected"), for: .normal)
        }
        
        cell.qtyNotAvailable.backgroundColor = .clear
        if ((products[indexPath.row].item_error?.first?.type ?? "") != "") {
            
            cell.btnSize.superview?.isHidden = false
            
            if (productMaxQty <= 0){
                cell.btnQty.superview?.isHidden = true
                cell.sizeNotLabel.superview?.isHidden = false
                cell.sizeNotLabel?.text = APP_LBL().size_not_available
            } else {
                cell.btnQty.isUserInteractionEnabled = true
            }
            
            if (products[indexPath.row].options_size?.count ?? 0 > 0){
//                cell.sizeNotLabel?.text = APP_LBL().size_not_available
                cell.qtyNotAvailable.backgroundColor = .black
            } else {
                cell.sizeNotLabel.superview?.isHidden = false
                cell.sizeNotLabel?.text = APP_LBL().item_out_of_stock
            }
//            cell.lblOutOfStock?.superview?.isHidden = true
            
        } else {
            cell.sizeNotLabel.superview?.isHidden = true
//            cell.lblOutOfStock?.superview?.isHidden = true
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
         return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        return true
    }
    
//    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
//
//
//        let swipeActionConfig = UISwipeActionsConfiguration(actions: [])
//        swipeActionConfig.performsFirstActionWithFullSwipe = false
//        return swipeActionConfig
//
//
//    }
    
    
    
//    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
//
//        print("Trailling")
//
//        let CartAction = UIContextualAction(style: .normal, title: nil) { (action, sourceView, completionHandler) in
//
//            if self.products[indexPath.row].is_wishlist == "0" {
//                self.addToWishList(index: indexPath.row)
//            } else {
//                self.removeFromWishList(index: indexPath.row)
//            }
//
//            completionHandler(true)
//        }
//
//        if products[indexPath.row].is_wishlist == "0" {
//            CartAction.image = UIImage(named: "wish_unselected")!
//
//        } else {
//            CartAction.image = UIImage(named: "WishListSelectedCart")!
//        }
//
//        CartAction.backgroundColor = UIColor(red: 250/255, green: 174/255, blue: 23/255, alpha: 1)
//
//
//        let deleteAction = UIContextualAction(style: .normal, title: nil) { (action, sourceView, completionHandler) in
//            print("index path of delete: \(indexPath)")
//
//            let showTitle = APP_LBL().confirmation
//            let showMsg = APP_LBL().operation_cant_be_undone
//
//            let confirmationAlert = UIAlertController(title: showTitle, message: showMsg, preferredStyle: UIAlertController.Style.alert);
//
//            confirmationAlert.addAction(UIAlertAction(title: APP_LBL().done_c, style: .default, handler: { (action: UIAlertAction!) in
//
//                self.removeProduct(index: indexPath.row)
//            }));
//
//            confirmationAlert.addAction(UIAlertAction(title: APP_LBL().cancel, style: .default, handler: { (action: UIAlertAction!) in
//            }));
//
//            self.present(confirmationAlert, animated: true, completion: nil)
//
//            completionHandler(true)
//        }
//
//
//        deleteAction.image = UIImage(named: "MyCartNewTrash")!
//        deleteAction.backgroundColor = UIColor.black
//
//        if defaults.bool(forKey: "isLogin") == true {
//
//            let filt = APP_DEL.arrGftCardData.first(where: { (item) -> Bool in ((item.child_product_id ?? "") == (products[indexPath.row].child_product_id ?? ""))})
//
//            if filt != nil {
//
//                let swipeActionConfig = UISwipeActionsConfiguration(actions: [deleteAction])
//                swipeActionConfig.performsFirstActionWithFullSwipe = false
//
//                return swipeActionConfig
//
//            } else {
//
//                let swipeActionConfig = UISwipeActionsConfiguration(actions: [deleteAction,CartAction])
//                swipeActionConfig.performsFirstActionWithFullSwipe = false
//
//                return swipeActionConfig
//            }
//
//        } else {
//
//            let swipeActionConfig = UISwipeActionsConfiguration(actions: [deleteAction])
//            swipeActionConfig.performsFirstActionWithFullSwipe = false
//
//            return swipeActionConfig
//        }
//    }

}

extension NejreeAppleCheckoutVC: SelectAppleCheckoutAddress {
    
    func selectAppleCheckoutAddress(dict: [String:Any]) {
        
        self.isSelectCustomAddress = true
        addressObjApplePay = dict
        self.updateDetail()
    }
}
