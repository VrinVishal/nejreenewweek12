/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */

import UIKit

class cedMageOrdrProductCell: UITableViewCell {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var productImage: UIImageView!
    
    @IBOutlet weak var productTitle: UIButton!
    
    @IBOutlet weak var productDesc: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        let colorString = cedMage.getInfoPlist(fileName:"cedMage",indexString: "themeColor") as! String
//     let color = cedMage.UIColorFromRGB(colorCode: colorString)
////        productTitle.setTitleColor(color, for: UIControlState.normal)
        productTitle.fontColorTool()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
