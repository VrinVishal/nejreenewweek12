//
//  oldUserController.swift
//  MageNative Magento Platinum
//
//  Created by MacMini on 02/09/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit
import FirebaseAuth

class oldUserController: MagenativeUIViewController,UITextFieldDelegate {

  
    @IBOutlet weak var dismissButton: UIButton!
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var numberTextfield: SkyFloatingLabelTextField!
    @IBOutlet weak var sideView: UIView!
    @IBOutlet weak var validNumberImage: UIImageView!
    @IBOutlet weak var sendOtpButton: UIButton!
    @IBOutlet weak var phoneSideImage: UIImageView!
    @IBOutlet weak var outerView: UIView!
 
    @IBOutlet weak var phoneNumberHeading: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        dismissButton.isHidden = false
        
        contentLabel.text = APP_LBL().you_will_need_to_add_your_phone_number_in_order_to_complete_your_account
        contentLabel.numberOfLines = 0
        numberTextfield.delegate = self
        numberTextfield.keyboardType = .asciiCapableNumberPad
        validNumberImage.isHidden = true
        sideView.backgroundColor = .black
        sendOtpButton.setTitle(APP_LBL().continue_c.uppercased(), for: .normal)
        sendOtpButton.backgroundColor = .black
       // sendOtpButton.roundCorners()
      //  outerView.roundCorners()
        outerView.setBorder()
        sendOtpButton.layer.borderColor = UIColor.lightGray.cgColor
        sendOtpButton.layer.borderWidth = 1.0
        sendOtpButton.addTarget(self, action: #selector(continueButtonTapped(_:)), for: .touchUpInside)
      //  phoneSideImage.roundImageAndBorderColor()
        sendOtpButton.setTitleColor(.lightGray, for: .normal)
        
        numberTextfield.placeholder = APP_LBL().phone_star.uppercased()
        phoneNumberHeading.text = APP_LBL().phone_number.uppercased()
        
        if APP_DEL.selectedLanguage == Arabic{
            numberTextfield.isLTRLanguage = true
            dismissButton.setImage(UIImage(named: "BackArrowNewArabic"), for: .normal)
        } else {
            numberTextfield.isLTRLanguage = true
            dismissButton.setImage(UIImage(named: "BackArrowNew"), for: .normal)
        }
        
        dismissButton.addTarget(self, action: #selector(dismissButtonTapepd(_:)), for: .touchUpInside)
        self.outerView.semanticContentAttribute = .forceLeftToRight
      //  numberTextfield.textAlignment = .left
        
        
        numberTextfield.semanticContentAttribute = .forceLeftToRight
        phoneSideImage.semanticContentAttribute = .forceRightToLeft
        
        // Do any additional setup after loading the view.
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
       // if textField.text == "" {self.view.makeToast("Please Enter mobile number".localized, duration: 1.0, position: .center);return}
        
        let str = textField.text!
        
        if str.count > 10 || str.count < 9 {
            validNumberImage.image = UIImage(named: "wrongname")
            validNumberImage.isHidden = false
          //  outerView.layer.borderColor = UIColor.init(hexString: "#AF1F45")?.cgColor
            phoneSideImage.tintColor = UIColor.init(hexString: "#AF1F45")
            sideView.backgroundColor = .black
            
            sendOtpButton.titleLabel?.textColor = UIColor.white
            sendOtpButton.backgroundColor = UIColor.clear
            
           // sendOtpButton.layer.borderColor = UIColor.lightGray.cgColor
        } else {
            validNumberImage.image = UIImage(named: "verified")
            validNumberImage.isHidden = false
            sideView.backgroundColor = nejreeColor
            sendOtpButton.setTitleColor(.white, for: .normal)
            //sendOtpButton.setBorder()
            outerView.setBorder()
            phoneSideImage.tintColor = nejreeColor
            
            sendOtpButton.titleLabel?.textColor = UIColor.black
            sendOtpButton.backgroundColor = UIColor(red: 251/255, green: 173/255, blue: 24/255, alpha: 1.0)
            
        }
        
    }
    
    @objc func continueButtonTapped(_ sender: UIButton) {
        
        
        
        let mobileNumber = numberTextfield.text
        if mobileNumber == "" {
            self.view.makeToast(APP_LBL().please_enter_mobile_number, duration: 2.0, position: .center);
            return
            
        }
        
        if mobileNumber!.count > (Int(APP_DEL.selectedCountry.phone_number_limit ?? "") ?? 9) {
            
            self.view.makeToast(APP_LBL().mobile_number_should_be_of_xyz_digits_only, duration: 2.0, position: .center);
            return
            
        }
        
        cedMageLoaders.addDefaultLoader(me: self)
        
        
        let number = "+" + (APP_DEL.selectedCountry.phone_code ?? "966") + (mobileNumber!).getArToEnDigit()
        
        let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
                                    let hashKey = userInfoDict["hashKey"]!;
                                       let customerId = userInfoDict["customerId"]!;

        var lang_value = ""
        if APP_DEL.selectedLanguage == Arabic{
                   lang_value = "2"
               } else {
                   lang_value = "1"
               }
        
      
        
        let params = ["telephone": number, "hashkey": hashKey,"customer_id" : customerId, "store_id" : lang_value, "country_id" : (APP_DEL.selectedCountry.country_code ?? "SA")]

                    let url = "mobiconnect/customer/otpdata"

                    var httpUrl = cedMage.getInfoPlist(fileName:"cedMage",indexString: "cedBaseUrl") as! String
                    let reqUrl = httpUrl+url
        

                     var postString=Dictionary<String,Dictionary<String,String>>()
                     var postString1=""
                     print(reqUrl)
                     var makeRequest = URLRequest(url: URL(string: reqUrl)!)

                     _ = cedMage.getInfoPlist(fileName:"cedMage",indexString: "requestheader") as! String
                     let storeId = defaults.value(forKey: "storeId") as? String

                     if(params != nil){
                         makeRequest.httpMethod = "POST"

                             postString=["parameters":[:]]
                             for (key,value) in params
                             {
                                 _ = postString["parameters"]?.updateValue(value ?? "", forKey:key)
                             }

                             if storeId != nil {
                                 _ = postString["parameters"]?.updateValue(storeId!, forKey:"store_id")
                             }

                             postString1=postString.convtToJson() as String
                             makeRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
                         makeRequest.setValue(API().getAuthorization(), forHTTPHeaderField: "Authorization")

                     }

                     print(reqUrl)
                     print(postString)
                     makeRequest.httpBody = postString1.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))

                     let task = URLSession.shared.dataTask(with: makeRequest, completionHandler: {data,response,error in
                         //cedMageLoaders.removeLoadingIndicator(me: self)
                   //      print(NSString(data: data!, encoding: String.Encoding.utf8.rawValue)!)
                         // check for http errors
                         if let httpStatus = response as? HTTPURLResponse , httpStatus.statusCode != 200
                         {

                             DispatchQueue.main.async
                                 {
                                     cedMageLoaders.removeLoadingIndicator(me: self)



                                     print("poststring=\(postString1)")
                                     print(NSString(data: data!, encoding: String.Encoding.utf8.rawValue) ?? "")
                                     print("statusCode should be 200, but is \(httpStatus.statusCode)")

                                     print("response = \(response)")

                             }
                             return;
                         }

                         // code to fetch values from response :: start


                         guard error == nil && data != nil else
                         {
                             DispatchQueue.main.async
                                 {

                                     cedMageLoaders.removeLoadingIndicator(me: self)
                                     cedMageHttpException.showAlertView(me: self, msg: error?.localizedDescription, title: APP_LBL().error )
                                     print("error=\(error)")

                             }
                             return ;
                         }

                         DispatchQueue.main.async
                             {
                                 cedMageLoaders.removeLoadingIndicator(me: self)

                                 print("response = \(response)")
                                guard let data = data else {return}
                                //var json = try! JSON(data: data)
                                guard var json = try? JSON(data: data) else {
                                    return;
                                }
                                print(json)

                                if json[0]["code"] == 0 {
                                     cedMageLoaders.removeLoadingIndicator(me: self)
                                    self.view.makeToast(APP_LBL().please_enter_valid_phone_no, duration: 1.0, position: .center)
                                }
                                else
                                {

                                    let vc = UIStoryboard(name: "cedMageLogin", bundle: nil).instantiateViewController(withIdentifier: "nejreeOTPController") as! nejreeOTPController
                                    vc.isOldUser = true
                                    vc.mobileNumber = mobileNumber!
                                    cedMageLoaders.removeLoadingIndicator(me: self)
                                    //vc.modalPresentationStyle = .fullScreen
                                    vc.hidesBottomBarWhenPushed = true
                                    self.navigationController?.pushViewController(vc, animated: true)
                                    //self.present(vc, animated: true)

                                }




                                /// self.recieveResponse(data: data, requestUrl: url, response: response)

                         }
                     })

                     task.resume()
        
        

//        PhoneAuthProvider.provider().verifyPhoneNumber(number, uiDelegate: nil) { (secretKey, error) in
//            if error == nil {
//                guard let verificationKey = secretKey else {return}
//                self.defaults.set(verificationKey, forKey: "mobileVerificationId")
//                self.defaults.synchronize()
//
//                let vc = UIStoryboard(name: "cedMageLogin", bundle: nil).instantiateViewController(withIdentifier: "nejreeOTPController") as! nejreeOTPController
//                vc.isOldUser = true
//                vc.mobileNumber = mobileNumber!
//                cedMageLoaders.removeLoadingIndicator(me: self)
//                vc.modalPresentationStyle = .fullScreen
//                self.present(vc, animated: true)
//            }else {
//                self.view.makeToast((error?.localizedDescription)!, duration: 2.0, position: .center)
//                cedMageLoaders.removeLoadingIndicator(me: self)
//            }
//
//        }

        
        
    }
    
    @objc func dismissButtonTapepd(_ sender: UIButton) {
        if(defaults.bool(forKey: "isLogin") == true)
        {
            self.logout()
        }
        else
        {
          //  self.dismiss(animated: true, completion: nil)
            
            self.navigationController?.popViewController(animated: true)
        }
        
    }
    
    func logout() {
        
        var newPram = [String: String]()
        
        if let userInfoDict = UserDefaults.standard.object(forKey: "userInfoDict") as? [String: String] {
            
            newPram["hashkey"] = userInfoDict["hashKey"]
            newPram["customer_id"] = userInfoDict["customerId"]
        }
        
        if let storeID =  UserDefaults.standard.value(forKey: "storeId") as? String {
            newPram["store_id"] = storeID
        }
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);
        
        API().callAPI(endPoint: "mobiconnect/customer/logout", method: .POST, param: newPram) { (json_res, err) in
            
         //   DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                if err == nil {
                    
                    self.defaults.set(false, forKey: "isLogin");
                     self.defaults.removeObject(forKey: "userInfoDict");
                     self.defaults.removeObject(forKey: "selectedAddressId");
                     self.defaults.setValue("Magenative App For Magento 2", forKey: "name")
                     UserDefaults.standard.set(nil,forKey: "LIAccessToken")
                     UserDefaults.standard.synchronize();
                     self.defaults.synchronize()
                     self.defaults.removeObject(forKey: "selectedAddressDescription");
                     
                     self.defaults.removeObject(forKey: "cartId");
                     self.defaults.removeObject(forKey: "cart_summary");
                     self.defaults.removeObject(forKey: "guestEmail");
                     self.defaults.set("0", forKey: "items_count");
                     self.defaults.removeObject(forKey: "name")
                     self.setCartCount(view: self, items: "0")
                     
                     self.navigationController?.popViewController(animated: true)
                }
          //  }
        }
    }

}
