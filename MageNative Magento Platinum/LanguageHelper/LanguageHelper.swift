//
//  LanguageHelper.swift
//  WonderWomen
//
//  Created by jayesh.d on 24/09/19.
//  Copyright © 2019 Vrinsoft. All rights reserved.
//

import Foundation
import CoreData


struct CD_Key {
    
    static let key : String = "key"
    static let english : String = "english"
    static let arabic : String = "arabic"
}

struct LBL : Codable {
    
    var key: String
    var en: String
    var ar: String
    
    init(key: String, en: String, ar: String)
    {
        self.key = key
        self.en = en
        self.ar = ar
    }
}

let APP_LANG = LanguageHelper.shared
class LanguageHelper {
    
    
    private init() { }
    static let shared = LanguageHelper()
    
    // MARK: - Core Data stack
    private var dbLanguage: NSPersistentContainer = {
        
        let db = NSPersistentContainer(name: "LanguageHelper")
        db.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return db
    }()
    
    private var dbContext: NSManagedObjectContext {
        return dbLanguage.viewContext
    }
    
    
    
    // MARK: - Tabels
    
    private var tblLanguage : NSEntityDescription {
        
        return NSEntityDescription.entity(forEntityName: "Language", in: dbContext)!
    }
    
    
    
    
    // MARK: - Core Data Saving support
    private func saveContext() {
        let context = dbLanguage.viewContext
        if context.hasChanges {
            
            do {
                
                try context.save()
            } catch {
                
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    func save() {
        self.saveContext()
    }
    
    func manageLabelList(list: [LBL]) {
        
        for lbl in list {
            
            if self.isExist(label: lbl.key) {
                self.update(label: lbl)
            } else {
                self.insert(label: lbl)
            }
        }
    }
    
    
    
    private func insert(label: LBL) {
        
        let lbl = Language(entity: tblLanguage, insertInto: dbContext)
        lbl.key = label.key
        lbl.english = label.en
        lbl.arabic = label.ar

        print("CD LANG:- INSERT: \(label.key) => DONE")
        self.saveContext()
    }
    
    private func update(label: LBL) {
        
        let req: NSFetchRequest<Language> = Language.fetchRequest()
        req.predicate = NSPredicate(format: "\(CD_Key.key) = %@", "\(label.key)")
        
        do {
            let language = try dbContext.fetch(req)
            if language.count > 0 {
                
                let lbl = language[0]
                lbl.english = label.en
                lbl.arabic = label.ar
                
                print("CD LANG:- UPDATE: \(label.key) => DONE")
                self.saveContext()
            }
        } catch {
            
        }
    }
    
    
    private func isExist(label: String) -> Bool {
        
        var isExist = false
        
        let req: NSFetchRequest<Language> = Language.fetchRequest()
        req.predicate = NSPredicate(format: "\(CD_Key.key) = %@", "\(label)")
        
        do {
            let language = try dbContext.fetch(req)
            if language.count > 0 {
                isExist = true
            }
        } catch {
            
        }
        return isExist
    }
    
    
    func retrive(label: String) -> String {
            
            //========== GET FROM CORE DATA ==========//

            let req: NSFetchRequest<Language> = Language.fetchRequest()
            req.predicate = NSPredicate(format: "\(CD_Key.key) = %@", "\(label)")
    
            var LANGUAGE = 2
            let arrValue = UserDefaults.standard.value(forKey: "AppleLanguages") as! [String]
            if arrValue.count > 0 {
                if arrValue[0] == "ar" {
                    LANGUAGE = 2
                } else {
                    LANGUAGE = 1
                }
            } else {
                LANGUAGE = 2
            }
    
            do {
                let language = try dbContext.fetch(req)
                if language.count > 0 {
    
                    if LANGUAGE == 1 {
                        return language[0].english ?? self.prepareExceptionLabel(label: label)
                    } else if LANGUAGE == 2 {
                        return language[0].arabic ?? self.prepareExceptionLabel(label: label)
                    } else {
                        return self.prepareExceptionLabel(label: label)
                    }
                }
            } catch {
    
            }
            return self.prepareExceptionLabel(label: label)
        }
    
    private func prepareExceptionLabel(label: String) -> String {
        
        let lb = label.replacingOccurrences(of: "_", with: " ")
        return lb.capitalizingFirstLetter()
    }
    
    private func retriveAll() -> [Language] {

        let req: NSFetchRequest<Language> = Language.fetchRequest()

        do {
            let arrLanguage = try dbContext.fetch(req)
            return arrLanguage

        } catch {

        }

        return []
    }
    
    
    
    private func delete(label: String) {

        let req: NSFetchRequest<Language> = Language.fetchRequest()
        req.predicate = NSPredicate(format: "\(CD_Key.key) = %@", "\(label)")

        do {
            let language = try dbContext.fetch(req)
            if language.count > 0 {
                print("CD LANG:- DELETE: \(label) => DONE")
                self.dbContext.delete(language[0])
                self.saveContext()
            }
        } catch {

        }
    }



    private func deleteAll() {

        let reqFetch = NSFetchRequest<NSFetchRequestResult>(entityName: self.tblLanguage.name!)
        let reqDelete: NSBatchDeleteRequest = NSBatchDeleteRequest(fetchRequest: reqFetch)

        do {
            try self.dbContext.execute(reqDelete)
            print("CD LANG:- DELETE ALL DONE")
            self.saveContext()
        } catch {

        }
    }
    
    
    
    
    
    
    
    private let UD = UserDefaults.standard
    private let key_updatedAt = "UpdatedAt"
    
    func setLanguageID(updated_date: String) {
        
        UD.set(updated_date, forKey: key_updatedAt)
        UD.synchronize()
    }
    
    func getLanguageInfo() -> String {
        
        let update_date = UD.value(forKey: key_updatedAt) as? String ?? ""

        return update_date
    }
    
    
    private func getCurrentDate() -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        return dateFormatter.string(from: Date())
    }
    
}



extension String
{
    func capitalizingFirstLetter() -> String {
        return prefix(1).capitalized + dropFirst()
    }

    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
}

