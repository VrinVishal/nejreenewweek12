//
//  NejreeReturnReasonVC.swift
//  MageNative Magento Platinum
//
//  Created by vishal.n on 23/02/21.
//  Copyright © 2021 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

protocol DidSelectReturnReason {
    func didSelectReturnReason(isOther: Bool, index: Int)
}

class NejreeReturnReasonVC: UIViewController {
    
    
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var tblViewHT: NSLayoutConstraint!
    
    @IBOutlet weak var lblCancel: UILabel!
    
    var delegateDidSelectReturnReason: DidSelectReturnReason?
    
    var arrReason = [ReturnReason]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setUI()
    }
    
    
    

    @IBAction func btnCancelAction(_ sender: UIButton) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.tblView.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
    }

    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?)
    {
        if let tbl = object as? UITableView {
            
            if tbl == self.tblView {
             
                self.tblView.layer.removeAllAnimations()
                self.tblViewHT.constant = self.tblView.contentSize.height
                
            }
        }
        
        UIView.animate(withDuration: 0.5) {
            self.updateViewConstraints()
            self.loadViewIfNeeded()
        }
    }

    func setUI() {
                
        lblTitle.text = APP_LBL().choose_reason.uppercased()
        lblTitle.font = UIFont(name: "Cairo-Regular", size: 17)!
        lblCancel.text = APP_LBL().cancel.uppercased()
        lblCancel.font = UIFont(name: "Cairo-Regular", size: 15)!
        
        if APP_DEL.selectedLanguage == Arabic {
            lblTitle.textAlignment = .right
        } else {
            lblTitle.textAlignment = .left
        }
        
        tblView.rowHeight = UITableView.automaticDimension
        tblView.estimatedRowHeight = UITableView.automaticDimension
        tblView.register(UINib(nibName: "NejreeReturnReasonCell", bundle: nil), forCellReuseIdentifier: "NejreeReturnReasonCell")
        tblView.delegate = self
        tblView.dataSource = self
        tblView.reloadData()
    }

}

extension NejreeReturnReasonVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrReason.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "NejreeReturnReasonCell") as! NejreeReturnReasonCell
        cell.selectionStyle = .none
        
        cell.lblTitle.font = UIFont(name: "Cairo-Regular", size: 16)!
        cell.lblTitle.textColor = UIColor.darkGray.withAlphaComponent(0.9)
        cell.lblDevider.backgroundColor = UIColor.lightGray.withAlphaComponent(0.5)
        
        if APP_DEL.selectedLanguage == Arabic {
            cell.lblTitle.textAlignment = .right
        } else {
            cell.lblTitle.textAlignment = .left
        }
        
        cell.lblTitle.text = self.arrReason[indexPath.row].label ?? ""
        
        if indexPath.row == (self.arrReason.count - 1) {
            cell.lblDevider.isHidden = true
        } else {
            cell.lblDevider.isHidden = false
        }
        
        return cell;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
//        if indexPath.row == self.arrReason.count - 1 {
//            self.delegateDidSelectReturnReason?.didSelectReturnReason(isOther: true, index: -1)
//        } else {
//            self.delegateDidSelectReturnReason?.didSelectReturnReason(isOther: false, index: indexPath.row)
//        }
        
        self.delegateDidSelectReturnReason?.didSelectReturnReason(isOther: (self.arrReason[indexPath.row].id == "6") , index: indexPath.row)
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension;
    }
}
