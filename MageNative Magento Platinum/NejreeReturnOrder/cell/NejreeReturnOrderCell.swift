//
//  NejreeReturnOrderCell.swift
//  MageNative Magento Platinum
//
//  Created by vishal.n on 23/02/21.
//  Copyright © 2021 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class NejreeReturnOrderCell: UITableViewCell {
    
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var imgQtyArrow: UIImageView!
    
    @IBOutlet weak var btnSelect: UIButton!
    @IBOutlet weak var imgSelect: UIImageView!


    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productName: UILabel!
    
    @IBOutlet weak var lblGiftDescr: UILabel!
    
    @IBOutlet weak var sizeValue: UILabel!
    @IBOutlet weak var quantityLabel: UILabel!
    @IBOutlet weak var btnQty: UIButton!
    
    @IBOutlet weak var priceLabel: UILabel!
    
    @IBOutlet weak var btnProductRedirctionImg: UIButton!
    @IBOutlet weak var btnProductRedirctionTitle: UIButton!
    
    
    @IBOutlet weak var lblReturnReason: UILabel!
    @IBOutlet weak var lblSelectedReason: UILabel!
    @IBOutlet weak var btnSelectedReason: UIButton!
    @IBOutlet weak var textReason: UITextView!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        if APP_DEL.selectedLanguage == Arabic {
            lblReturnReason.textAlignment = .right
            lblSelectedReason.textAlignment = .right
            textReason.textAlignment = .right
        } else {
            lblReturnReason.textAlignment = .left
            lblSelectedReason.textAlignment = .left
            textReason.textAlignment = .left
        }
        
        lblReturnReason.text = APP_LBL().return_reason.uppercased()
        lblReturnReason.font = UIFont(name: "Cairo-Regular", size: 16)!
                
        lblSelectedReason.font = UIFont(name: "Cairo-Regular", size: 16)!
        lblSelectedReason.superview?.layer.cornerRadius = 4
        lblSelectedReason.superview?.clipsToBounds = true
        lblSelectedReason.superview?.layer.borderWidth = 0.5
        lblSelectedReason.superview?.layer.borderColor = UIColor.black.cgColor
        
        textReason.text = APP_LBL().Write_the_reason
        textReason.font = UIFont(name: "Cairo-Regular", size: 15)!
        textReason.superview?.layer.cornerRadius = 4
        textReason.superview?.clipsToBounds = true
        textReason.superview?.layer.borderWidth = 0.5
        textReason.superview?.layer.borderColor = UIColor.black.cgColor
        
        
        outerView.layer.cornerRadius = 6.0
        outerView.clipsToBounds = true
        outerView.layer.borderColor = UIColor.lightGray.withAlphaComponent(0.6).cgColor
        outerView.layer.borderWidth = 0.5
        
//        productImage.superview?.layer.cornerRadius = 6.0
        productImage.superview?.clipsToBounds = true
//        productImage.superview?.layer.borderColor = UIColor.lightGray.withAlphaComponent(0.6).cgColor
//        productImage.superview?.layer.borderWidth = 0.5
        
        sizeValue.superview?.clipsToBounds = true
        
        btnQty.superview?.layer.cornerRadius = 6.0
        quantityLabel.superview?.clipsToBounds = true

}

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
