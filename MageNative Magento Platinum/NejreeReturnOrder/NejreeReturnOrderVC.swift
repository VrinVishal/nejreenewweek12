//
//  NejreeReturnOrderVC.swift
//  MageNative Magento Platinum
//
//  Created by vishal.n on 22/02/21.
//  Copyright © 2021 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

struct ReturnProduct : Codable {
    
    var isReturn: Bool? = false
    var isKeep: Bool? = false
    var returnQuantity : Int?
    var returnReason : ReturnReason?
    var returnOtherReason : String?
    
    let finalprice : String?
    let name : String?
    let options : [ReturnProductOptions]?
    let product_id : String?
    let image : String?
    let size : String?
    let qty : Int?
    
}

struct ReturnProductOptions : Codable {
    
    let option_value : String?
    let value : String?
    let option_id : Int?
    let label : String?
}

struct ReturnReason : Codable {
    
    let id : String?
    let label : String?
}
class NejreeReturnOrderVC: UIViewController, DidSelectReturnReason {
    
    @IBOutlet weak var scrlView: UIScrollView!
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgBackBG: UIImageView!
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var tblViewHT: NSLayoutConstraint!
    
    @IBOutlet weak var lblEasyReturn: UILabel!
    @IBOutlet weak var lblRerturnPolicy: UILabel!
    
    @IBOutlet weak var btnContinue: UIButton!
    
    let placeholder = APP_LBL().Write_the_reason
    var orderId = ""
    var products = [ReturnProduct]()
    var arrReason = [ReturnReason]()
    
    var selectedCartItem : Int = -1
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        scrlView.isHidden = true
        btnContinue.isHidden = true

        setUI()
        
        getReasons()
        getReturnItem()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.tblView.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        //self.tblView.removeObserver(self, forKeyPath: "contentSize")
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?)
    {
        self.tblView.layer.removeAllAnimations()
        self.tblViewHT.constant = self.tblView.contentSize.height
        
        UIView.animate(withDuration: 0.5) {
            self.updateViewConstraints()
            self.loadViewIfNeeded()
        }
    }
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnReturnPolicyAction(_ sender: UIButton) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NejreeCmsVC") as! NejreeCmsVC
        vc.contentText = "returns"
        vc.strTitle = APP_LBL().return_policy.uppercased()
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnContinueAction(_ sender: UIButton) {
        
        self.view.endEditing(true)
        self.submitReturnOrder()
    }
    
    var selectedReasonIndex = -1
    @objc func btnReasonAction(_ sender: UIButton) {
        
        selectedReasonIndex = sender.tag
        let vc = UIStoryboard(name: "cedMageAccounts", bundle: nil).instantiateViewController(withIdentifier: "NejreeReturnReasonVC") as! NejreeReturnReasonVC
        vc.delegateDidSelectReturnReason = self
        vc.arrReason = self.arrReason
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    func didSelectReturnReason(isOther: Bool, index: Int) {
        
        products[selectedReasonIndex].returnReason = self.arrReason[index]
        
        if self.arrReason[index].id == "6" {
            
        } else {

            products[selectedReasonIndex].returnOtherReason = ""
        }
        
        
        
        self.tblView.reloadData()
    }
    
    
    @objc func ActionProductDetailRedirection(_ sender: UIButton) {
        
    }
    
    @objc func btnSelectAction(_ sender: UIButton) {
        
        products[sender.tag].isReturn = !(products[sender.tag].isReturn ?? false)
        self.tblView.reloadData()
//        if let cell = self.tblView.cellForRow(at: IndexPath(item: sender.tag, section: 0)) as? NejreeReturnOrderCell {
//
//            if products[sender.tag].isReturn {
//                cell.imgSelect.image = UIImage(named: "checkboxFilled")
//            } else {
//                cell.imgSelect.image = UIImage(named: "checkboxEmpty")
//            }
//        }
    }
    
    //MARK:- CART LIST
    func getEditCart(isUpdateQty:Bool? = false, selectedSize: String, selectedQty : String, selectedSizeValue: String = "") {
        
        
        if let qty = Int(selectedQty), (self.selectedCartItem != -1) {
            self.products[selectedCartItem].returnQuantity = qty
        }
        
        self.tblView.reloadData()
    }
    
    var popupQty = FFPopup()
    @objc func btnQtyAction(_ sender: UIButton) {
        if (allow_size_selection_incart != "1"){
            return
        }
        if let qua = products[sender.tag].qty {
            
            selectedCartItem = sender.tag
            let vi = Bundle.main.loadNibNamed("ProductQtyPopup", owner: self, options: nil)?.first as! ProductQtyPopup
            vi.parentVC = self
            vi.maxQty = qua
            vi.setContent()
            vi.btnDone.addTarget(self, action: #selector(self.btnQtyDoneAction(_:)), for: .touchUpInside)
            vi.frame = CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: SCREEN_HEIGHT)
            
            self.popupQty = FFPopup(contetnView: vi, showType: .slideInFromBottom, dismissType: .slideOutToBottom, maskType: .dimmed, dismissOnBackgroundTouch: true, dismissOnContentTouch: false)
            self.popupQty.showInDuration = 0.3
            
            let layout = FFPopupLayout(horizontal: .left, vertical: .bottom)
            self.popupQty.show(layout: layout)
        }
        
    }
    
    @objc func btnQtyDoneAction(_ sender: UIButton) {
        
        print("Qty Done")
        popupQty.dismiss(animated: true)
    }
    
    func getReturnItem() {
        
        var postData = [String:String]()

        if let storeID =  UserDefaults.standard.value(forKey: "storeId") as? String {
            postData["store_id"] = storeID
        }
        
        postData["order_id"] = orderId
        postData["country_id"] = APP_DEL.selectedCountry.country_code ?? "SA"
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);
        
        API().callAPI(endPoint: "mobiconnect/awRmaRequest/returnOrderItems", method: .POST, param: postData) { (json_res, err) in
            
            cedMageLoaders.removeLoadingIndicator(me: self);
            
            
            do {
                
                let json_res = json_res[0]
                let catData = try json_res["data"]["orderItems"].rawData()
                self.products = try JSONDecoder().decode([ReturnProduct].self, from: catData)
                
                self.returnType = json_res["data"]["type"]["reutrn"].stringValue
                self.mobile_number = json_res["data"]["mobile_number"].stringValue
                
            } catch {
                
            }
            
            if self.products.count > 0 {
                
                self.scrlView.isHidden = false
                self.btnContinue.isHidden = false
                
            } else {
                
                let confirmationAlert = UIAlertController(title: "", message: APP_LBL().no_product_available_for_return, preferredStyle: UIAlertController.Style.alert);
                
                confirmationAlert.addAction(UIAlertAction(title: APP_LBL().ok, style: .default, handler: { (action: UIAlertAction!) in
                    
                    self.navigationController?.popViewController(animated: true)
                }));
                
                self.present(confirmationAlert, animated: true, completion: nil)
            }
            
            
            self.tblView.reloadData()
        }
    }
    
    func getReasons() {
        
        var postData = [String:String]()

        if let storeID =  UserDefaults.standard.value(forKey: "storeId") as? String {
            postData["store_id"] = storeID
        }
        
        //cedMageLoaders.removeLoadingIndicator(me: self);
        //cedMageLoaders.addDefaultLoader(me: self);
        
        API().callAPI(endPoint: "mobiconnect/awRmaRequest/returnReasons", method: .POST, param: postData) { (json_res, err) in
            
            //cedMageLoaders.removeLoadingIndicator(me: self);

            do {
                let data = try! json_res.rawData()
                self.arrReason = try JSONDecoder().decode([ReturnReason].self, from: data)
            } catch {
                
            }
        }
    }
    
    var returnType = ""
    var mobile_number = ""
    func submitReturnOrder() {
        
        var dict = [String:[String:Any]]()
        var index = 1
        for pro in products {
            
            if pro.isReturn ?? false {
                    
                if pro.returnReason == nil {
                        
                    APP_DEL.showAlertWithOkButton(parentVC: self, strTitle: "", strMsg: APP_LBL().select_your_reason_for_return)
                    break
                    
                } else if pro.returnReason?.id == "6" && (pro.returnOtherReason ?? "" == "") {
                    
                    APP_DEL.showAlertWithOkButton(parentVC: self, strTitle: "", strMsg: APP_LBL().select_your_reason_for_return)
                    break
                }
                
                var tempItem = [String:Any]()
                tempItem["product_id"] = pro.product_id ?? ""
                tempItem["quantity"] = "\(pro.returnQuantity ?? 0)"
                tempItem["reason_id"] = pro.returnReason?.id ?? ""
                tempItem["reason_text"] = ((pro.returnReason?.id == "6") ? (pro.returnOtherReason ?? "") : (pro.returnReason?.label ?? ""))
                tempItem["size"] = (pro.options?.first?.option_value ?? "")
                
                dict["product\(index)"] = tempItem
                
                index = (index + 1)
            }
        }
        
        if dict.count <= 0 {
            
            return;
        }
        
        var postData = [String:String]()

        if let storeID =  UserDefaults.standard.value(forKey: "storeId") as? String {
            postData["store_id"] = storeID
        }
        postData["order_id"] = orderId
        postData["country_id"] = APP_DEL.selectedCountry.country_code ?? "SA"
        postData["type"] = returnType
        postData["mobileno"] = mobile_number//UserDefaults.standard.object(forKey: "mobile_number") as? String ?? ""

        let data = try! JSONSerialization.data(withJSONObject: dict, options: [.prettyPrinted])
        let str = String(data: data, encoding: .utf8)
        postData["order_items"] = str
        
        print(postData)
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);

        API().callAPI(endPoint: "mobiconnect/awRmaRequest/submitReturn", method: .POST, param: postData) { (json_res, err) in

            cedMageLoaders.removeLoadingIndicator(me: self);
            
            if json_res[0]["data"]["success"].stringValue == "true" {
                
                let confirmationAlert = UIAlertController(title: "", message: APP_LBL().your_return_order_requested_successfully, preferredStyle: UIAlertController.Style.alert);
                
                confirmationAlert.addAction(UIAlertAction(title: APP_LBL().ok, style: .default, handler: { (action: UIAlertAction!) in
                    
                    self.navigationController?.popViewController(animated: true)
                }));
                
                self.present(confirmationAlert, animated: true, completion: nil)
                
            } else {
                
                APP_DEL.showAlertWithOkButton(parentVC: self, strTitle: "", strMsg: json_res[0]["data"]["message"].stringValue)
            }
        }
    }
    
    func setUI() {
        
        self.tblView.register(UINib(nibName: "NejreeReturnOrderCell", bundle: nil), forCellReuseIdentifier: "NejreeReturnOrderCell")
        self.tblView.delegate = self
        self.tblView.dataSource = self
   
        
        lblTitle.text = APP_LBL().returned_items.uppercased();
        
        if APP_DEL.selectedLanguage == Arabic {
            
            //lblItem.textAlignment = .right
            
            imgBackBG.image = UIImage(named: "icon_back_white_Arabic")
            
        } else {
            
            //lblItem.textAlignment = .left
            
            imgBackBG.image = UIImage(named: "icon_back_white_round")
        }
        
        lblEasyReturn.text = APP_LBL().nejree_gives_easy_return
        lblEasyReturn.font  = UIFont(name: "Cairo-Regular", size: 15)!
        
        lblRerturnPolicy.text = APP_LBL().return_policy.uppercased();
        lblRerturnPolicy.font  = UIFont(name: "Cairo-Bold", size: 15)!
        
        
        self.btnContinue.setTitle(APP_LBL().continue_c.uppercased(), for: .normal)
        self.btnContinue.titleLabel?.font = UIFont(name: "Cairo-Regular", size: 16)!

    }
}

extension NejreeReturnOrderVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return products.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "NejreeReturnOrderCell", for: indexPath) as! NejreeReturnOrderCell
        cell.selectionStyle = .none
        
        cell.btnProductRedirctionImg.tag = indexPath.row
        cell.btnProductRedirctionImg.addTarget(self, action: #selector(ActionProductDetailRedirection(_:)), for: .touchUpInside)

        cell.btnProductRedirctionTitle.tag = indexPath.row
        cell.btnProductRedirctionTitle.addTarget(self, action: #selector(ActionProductDetailRedirection(_:)), for: .touchUpInside)

        cell.btnQty.tag = indexPath.row
        cell.btnQty.addTarget(self, action: #selector(btnQtyAction(_:)), for: .touchUpInside)
        
        cell.btnSelect.tag = indexPath.row
        cell.btnSelect.addTarget(self, action: #selector(self.btnSelectAction(_:)), for: .touchUpInside)
        
        cell.btnSelectedReason.tag = indexPath.row
        cell.btnSelectedReason.addTarget(self, action: #selector(self.btnReasonAction(_:)), for: .touchUpInside)
        
        cell.textReason.tag = indexPath.row
        
        if (products[indexPath.row].isReturn ?? false) {
            cell.imgSelect.image = UIImage(named: "checkboxFilled")
        } else {
            cell.imgSelect.image = UIImage(named: "checkboxEmpty")
        }
        
        if (products[indexPath.row].isReturn ?? false) {
                
            cell.lblReturnReason?.superview?.isHidden = false
            
            if products[indexPath.row].returnReason == nil {
                cell.lblSelectedReason.text = APP_LBL().choose_reason.uppercased()
                cell.lblSelectedReason.superview?.superview?.isHidden = false
                cell.textReason.superview?.superview?.isHidden = true
            } else if products[indexPath.row].returnReason?.id == "6" {
                cell.lblSelectedReason.text = (products[indexPath.row].returnReason?.label ?? "")//APP_LBL().other.uppercased()
                cell.textReason.superview?.superview?.isHidden = false
            } else {
                cell.lblSelectedReason.text = products[indexPath.row].returnReason?.label ?? ""
                cell.textReason.superview?.superview?.isHidden = true
            }
            
        } else {
            
            cell.lblReturnReason?.superview?.isHidden = true
            cell.lblSelectedReason.superview?.superview?.isHidden = true
            cell.textReason.superview?.superview?.isHidden = true
            
        }
        
        
        let value = UserDefaults.standard.value(forKey: "AppleLanguages") as! [String]
        if value[0] == "ar" {
            cell.semanticContentAttribute = .forceRightToLeft
            cell.quantityLabel.textAlignment = .right
            cell.productName.textAlignment = .right
            cell.sizeValue.textAlignment = .right
        } else {
            cell.semanticContentAttribute = .forceLeftToRight
            cell.quantityLabel.textAlignment = .left
            cell.productName.textAlignment = .left
            cell.sizeValue.textAlignment = .left
        }
        
//        let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: products[indexPath.row].total_regular_price!)
//        attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 1, range: NSMakeRange(0, attributeString.length))
//        attributeString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.red, range: NSMakeRange(0, attributeString.length))

//        if products[indexPath.row].total_special_price != "no_special" {
//            cell.priceLabel.text = products[indexPath.row].total_special_price;
//
//        } else {
//            cell.priceLabel.text = products[indexPath.row].total_regular_price
//        }
        
        cell.priceLabel.text = products[indexPath.row].finalprice ?? "";
        
        
        cell.btnQty.isUserInteractionEnabled = false
        cell.imgQtyArrow.isHidden = true
        
        cell.productName.text = products[indexPath.row].name ?? ""
        //cell.quantityLabel.text = APP_LBL().qty.uppercased() + " " + "\(products[indexPath.row].quantity ?? 0)"
        if products[indexPath.row].returnQuantity == nil {
            cell.quantityLabel.text = APP_LBL().qty.uppercased() + " " + "\(products[indexPath.row].qty ?? 0)"
        } else {
            cell.quantityLabel.text = APP_LBL().qty.uppercased() + " " + "\(products[indexPath.row].returnQuantity ?? 0)"
        }
        cell.productImage.sd_setImage(with: URL(string: products[indexPath.row].image!), placeholderImage:nil)
        
        
        cell.sizeValue.text = APP_LBL().size.uppercased() + ": " + (products[indexPath.row].options?.first?.value ?? "")
        
        cell.lblGiftDescr.text = ""
        cell.lblGiftDescr.superview?.isHidden = true
        cell.btnQty.superview?.isHidden = false
        cell.imgQtyArrow.isHidden = false
        cell.btnQty.isUserInteractionEnabled = true
        
        cell.textReason.delegate = self
        if (products[indexPath.row].returnOtherReason ?? "") == "" {
            cell.textReason.text = placeholder
            cell.textReason.textColor = UIColor.lightGray
        } else {
            cell.textReason.text = (products[indexPath.row].returnOtherReason ?? "")
            cell.textReason.textColor = UIColor.black
        }
        
        
        cell.textReason.selectedTextRange = cell.textReason.textRange(from: cell.textReason.beginningOfDocument, to: cell.textReason.beginningOfDocument)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
         return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        return true
    }
    
}


extension NejreeReturnOrderVC: UITextViewDelegate {
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        let currentText: NSString = textView.text! as NSString
        let updatedText = currentText.replacingCharacters(in: range, with:text)
        
        if updatedText.isEmpty {
            textView.text = placeholder
            textView.textColor = UIColor.lightGray
            textView.selectedTextRange = textView.textRange(from: textView.beginningOfDocument, to: textView.beginningOfDocument)
            return false
        }
        else if textView.textColor == UIColor.lightGray && !text.isEmpty {
            textView.text = nil
            textView.textColor = UIColor.black
        }
        
        return true
    }
    
    func textViewDidChangeSelection(_ textView: UITextView) {
        if self.view.window != nil {
            if textView.textColor == UIColor.lightGray {
                textView.selectedTextRange = textView.textRange(from: textView.beginningOfDocument, to: textView.beginningOfDocument)
            }
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if products.count > textView.tag {
            if textView.text == placeholder {
                products[textView.tag].returnOtherReason = ""
            } else {
                products[textView.tag].returnOtherReason = textView.text
            }
        }
    }
}
