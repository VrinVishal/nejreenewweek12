//
//  DummySplashVC.swift
//  MageNative Magento Platinum
//
//  Created by vishal.n on 06/08/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class DummySplashVC: UIViewController {

    @IBOutlet weak var lblVersionName : UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

       let appVersion = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as? String ?? "3.1.8"
       lblVersionName.text =  appVersion
        
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
