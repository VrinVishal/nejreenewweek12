//
//  SplashVC.swift
//  MageNative Magento Platinum
//
//  Created by vishal.n on 04/04/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit
import SwiftOverlayShims
import NVActivityIndicatorView

var dummySplash = UIView()
let KEY_GENDER = "Nejree_Gender"

var isDisplayHomeCategory = false
var max_qty_all_products = ""
var cat_bg_default_color_code = ""
var gender_gif_url = ""
var allow_size_selection_incart = ""
var home_layer_category_id = ""
var is_country_selection_allow = ""
var English = "en"
var Arabic = "ar"
var allow_otp_non_saudi = ""
var is_return_order_allow = ""

class SplashVC: UIViewController {
    
    let defaults = UserDefaults.standard
     @IBOutlet weak var lblVersionName : UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
          self.navigationController?.setNavigationBarHidden(true, animated: false)

        let appVersion = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as? String ?? "3.1.8"
        lblVersionName.text = appVersion
        
        if let storeID = UserDefaults.standard.value(forKey: "storeId") as? String {
            
            print("STORE ID:-")
            print(storeID)
            
        } else {

            UserDefaults.standard.setValue("2", forKey: "storeId")
            UserDefaults.standard.synchronize()
        }
        
        
        self.getLabel()
    }
       
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func getLabel() {
        
        var postData = [String:String]()
        postData["language_id"] = ""
        postData["updated_datetime"] = APP_LANG.getLanguageInfo() //""

//        API().callAPI(endPoint: "getlabel", method: .POST, param: postData) { (json_res, err) in
//
//            if err == nil {
//
//                let json = json_res[0]
//
//                strTopicID  = json["topic_id"].stringValue
//                strTopicName = json["topic_name"].stringValue
//                APP_DEL.strCodCharge = json["cod"].stringValue
//
//                IS_FEATURED_ENABLED = (json["home_featured_product"].stringValue == "true")
//
//                isForcefullyUpdate =  (json["ios_force_update"].stringValue == "1")
//
//                APP_DEL.isDiscountTagEnable = json["discount_tags"].stringValue
//
//                APP_DEL.home_featured_category = json["home_featured_category"].stringValue.components(separatedBy: ",")
//
//                APP_DEL.extrafee_status = json["extrafee_status"].stringValue
//                APP_DEL.extrafee_minimum_order_amount = json["extrafee_minimum_order_amount"].stringValue
//
//
//                let appVersion = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as? String
//
//                let tempWebVersion = json["ios_app_version"].stringValue
//                if let vers = appVersion, tempWebVersion != "" {
//
//                    print(vers)
//                    print(json["ios_app_version"].stringValue)
//
//                    let intAppVersion = Int(vers.replacingOccurrences(of: ".", with: ""))!
//                    let intWebVersion = Int((json["ios_app_version"].stringValue).replacingOccurrences(of: ".", with: ""))!
//
//                    print("APP VERSION:- ",intAppVersion)
//                    print("WEB VERSION:- ",intWebVersion)
//
//                    if intAppVersion < intWebVersion {
//                        isCheckForUpdate = true
//                    } else {
//                        isCheckForUpdate = false
//                    }
//
//                }
//
//
//
//
//                //let arrLabel = json["result"].arrayValue
//
//                APP_DEL.arrLBL = try! JSONDecoder().decode([LBL].self, from: json["result"].rawData())
//
//                //                    var arrLB : [LBL] = []
//                //                    for tempLB in arrLabel {
//                //                        let lb = LBL(key: tempLB["key"].stringValue, en: tempLB["en"].stringValue, ar: tempLB["ar"].stringValue)
//                //                        arrLB.append(lb)
//                //                    }
//
//                print("LBL COUNT:-")
//                print(APP_DEL.arrLBL.count)
//                //                    APP_LANG.manageLabelList(list: arrLB)
//
//                APP_LANG.setLanguageID(updated_date: json["updated_datetime"].stringValue)
//
//                if let _ = self.defaults.string(forKey: "isAppAlreadyLaunchedOnce") {
//
//                    // app is launched already not first time
//
//                } else {
//
//                    // first Time launched
//
//                    self.defaults.set(true, forKey: "isAppAlreadyLaunchedOnce")
//                    UserDefaults.standard.set(["ar","en","fr"], forKey: "AppleLanguages")
//                }
//
//
//                let window = UIApplication.shared.keyWindow!
//                let test = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DummySplashVC") as! DummySplashVC
//                dummySplash = test.view!
//                dummySplash.frame = window.bounds
//                window.addSubview(dummySplash);
//
//
//                APP_DEL.changeLanguage()
//
//
//                APP_DEL.getCartCount()
//
//                if self.defaults.bool(forKey: "isLogin") {
//
//                    self.getAddress()
//                }
//            }
//        }
        
        
        API().callDataAPI(endPoint: "getlabel", method: .POST, param: postData) { (data, err) in

            if err == nil && data != nil {

                var json = [String:Any]()

                do {

                    let temp = try (JSONSerialization.jsonObject(with: data!, options: []) as? [[String:Any]] ?? [])

                    if ((temp.count) > 0) {
                        json = temp[0]
                    }

                } catch {
                    
                    print(String(reflecting: error))
                    print(error.localizedDescription)
                }

//                strTopicID  = json["topic_id"] as? String ?? ""
//                strTopicName = json["topic_name"] as? String ?? ""
                APP_DEL.strCodCharge = json["cod"] as? String ?? ""
                IS_FEATURED_ENABLED = (json["home_featured_product"] as? String ?? "" == "true")
                isForcefullyUpdate =  (json["ios_force_update"] as? String ?? "" == "1")
                APP_DEL.isDiscountTagEnable = json["discount_tags"] as? String ?? ""
                APP_DEL.home_featured_category = (json["home_featured_category"] as? String ?? "").components(separatedBy: ",")
                APP_DEL.extrafee_status = json["extrafee_status"] as? String ?? ""
                APP_DEL.extrafee_minimum_order_amount = json["extrafee_minimum_order_amount"] as? String ?? ""
                APP_DEL.auto_apply_storecredit = json["auto_apply_storecredit"] as? String ?? ""
                APP_DEL.category_record_seconds = json["category_reload_seconds"] as? String ?? "300"
                
                isDisplayHomeCategory = ((json["show_subcategory_home"] as? String ?? "") == "1")
                cat_bg_default_color_code = (json["color_code"] as? String ?? "")
                max_qty_all_products = (json["max_qty_all_products"] as? String ?? "")
                gender_gif_url = (json["gender_gif_url"] as? String ?? "")
                allow_size_selection_incart = (json["allow_size_selection_incart"] as? String ?? "")
                home_layer_category_id = (json["home_layer_category_id"] as? String ?? "")
                allow_otp_non_saudi = (json["allow_otp_non_saudi"] as? String ?? "")
                is_return_order_allow = (json["is_return_order_allow"] as? String ?? "")
                
                arrGender.removeAll()
                var genderCount = 0
                for gen in (json["gender_list"] as? [[String:String]] ?? []) {
                    if genderCount < 4 {
                        arrGender.append(Gender(id: gen["id"], title: gen["title"], title_ar : gen["title_ar"]))
                        genderCount = (genderCount + 1)
                    } else {
                        break;
                    }
                }
                
                
                let tempStringCharLimit = json["sms_char_limit"] as? String ?? "140"
                APP_DEL.SMSCharLimit =  Int(tempStringCharLimit) ?? 140
                APP_DEL.isApplePayEnable = json["apple_pay_enable"] as? String ?? ""
                
                let minActionCount = json["minimumReviewActionCount"] as? String ?? ""
                minimumReviewWorthyActionCount = Int(minActionCount) ?? 10
                
                let codTotalValidation = json["CODDisableAfterTotal"] as? String ?? ""
                codTotalToDiableOption = Int(codTotalValidation) ?? 1200
                
                let appVersion = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as? String

                let tempWebVersion = json["ios_app_version"] as? String ?? ""
                if let vers = appVersion, tempWebVersion != "" {

                    print(vers)
                    print(json["ios_app_version"] as? String ?? "")

                    let intAppVersion = Int(vers.replacingOccurrences(of: ".", with: ""))!
                    let intWebVersion = Int((json["ios_app_version"] as? String ?? "").replacingOccurrences(of: ".", with: ""))!

                    print("APP VERSION:- ",intAppVersion)
                    print("WEB VERSION:- ",intWebVersion)

                    if intAppVersion < intWebVersion {
                        isCheckForUpdate = true
                    } else {
                        isCheckForUpdate = false
                    }

                }

                

                let lblData = try! JSONSerialization.data(withJSONObject: json["result"] as? [[String:Any]] ?? [], options: [])
                APP_DEL.arrLBL = try! JSONDecoder().decode([LBL].self, from: lblData)

                APP_LANG.manageLabelList(list: APP_DEL.arrLBL)
                APP_LANG.setLanguageID(updated_date: json["updated_datetime"] as? String ?? "")

                if let _ = self.defaults.string(forKey: "isAppAlreadyLaunchedOnce") {

                    // app is launched already not first time

                } else {

                    // first Time launched

                    self.defaults.set(true, forKey: "isAppAlreadyLaunchedOnce")
                    UserDefaults.standard.set(["ar","en","fr"], forKey: "AppleLanguages")
                }

                is_country_selection_allow = json["is_country_selection_allow"] as? String ?? ""
                
//                if APP_DEL.getCountryFromUserDefault() {
//                    APP_DEL.setTabBarAfterAPIsCalling()
////                    self.getAllCountryAPI()
//                } else {
                    //is_country_selection_allow
                    if let is_country_selection_allow = json["is_country_selection_allow"] as? String {
                        if is_country_selection_allow == "1"{
                            if APP_DEL.getCountryFromUserDefault() {
                                self.getAllCountryAPI()
                            } else {
                                let test = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: Constants().SCREEN_NAME_GenderVC) as! GenderVC
                                self.navigationController?.pushToViewController(test, animated: false, completion: {
                                    
                                })
                            }
                            return;
                        } else {
                            if APP_DEL.getCountryFromUserDefault() {
                                if (APP_DEL.selectedCountry.country_code == "SA"){
                                    APP_DEL.setTabBarAfterAPIsCalling()
                                } else {
                                    self.getAllCountryAPI()
                                }
                            } else {
                                self.getAllCountryAPI()
                            }
                        }
                    }
                    
//                    self.getAllCountryAPI()
//                }
                
                

                
            }
        }
        
    }
    
    
    func getAllCountryAPI() {
        
        let storeID = UserDefaults.standard.value(forKey: "storeId") as! String?
        var postData = [String:String]()
        postData["store_id"] = "2"
        API().callAPI(endPoint: "mobiconnect/getcountrybystore", method: .POST, param: postData) { (json_res, err) in



            let catData = try! json_res[0]["country"].rawData()
            let countryList = try! JSONDecoder().decode([countryData].self, from: catData)

            var isCountryFound = false
            
            for rec in countryList {
                
                if APP_DEL.getCountryFromUserDefault() {
                    
                    if APP_DEL.selectedCountry.country_code == rec.country_code && is_country_selection_allow == "1"{
                        
                        APP_DEL.selectedCountry = rec
                        APP_DEL.selectedCountry.syncronize()
                        
                        isCountryFound = true
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                            // your code here
                            APP_DEL.setTabBarAfterAPIsCalling()
                        }
                    }
                    
                } else {
                    
                    if (rec.country_code?.uppercased() == "SA"){
                        APP_DEL.selectedCountry = rec
                        APP_DEL.selectedCountry.syncronize()
                        isCountryFound = true
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                            // your code here
                            APP_DEL.setTabBarAfterAPIsCalling()
                        }
                    }
                }
                
            }
            
            
            if !isCountryFound{
                if is_country_selection_allow == "1"{
                    let test = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: Constants().SCREEN_NAME_GenderVC) as! GenderVC
                    self.navigationController?.pushToViewController(test, animated: false, completion: {
                        
                    })
                    return
                }
                for rec in countryList {
                    
                    if (rec.country_code?.uppercased() == "SA"){
                        APP_DEL.selectedCountry = rec
          
                        APP_DEL.selectedCountry.syncronize()
                        isCountryFound = true
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                            // your code here
                            APP_DEL.setTabBarAfterAPIsCalling()
                        }
                    }
                    
                }
            }
        
        }
//        if let path = Bundle.main.path(forResource: "CountryList", ofType: "json") {
//            do {
//                  let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
//                guard let json_res = try? JSON(data: data) else {
//                    return
//                }
//                  let catData = try! json_res[0]["Country"].rawData()
//                 self.countryList = try! JSONDecoder().decode([countryData].self, from: catData)
//                 self.setUI()
//              } catch {
//                   // handle error
//              }
//        }
    }
    

}
