//NEW
//  cedMageDefaultCollection.swift
//  MageNative Magento Platinum
//
//  Created by CEDCOSS Technologies Private Limited on 18/01/17.
//  Copyright © 2017 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit
import FirebaseAnalytics
import IQKeyboardManager

struct SortType : Codable {
        
    let id : String?
    let label_ar : String?
    let code : String?
    let label_en : String?
    let order : String?
}

class cedMageDefaultCollection: MagenativeUIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UIScrollViewDelegate, UIGestureRecognizerDelegate, FilterApply, DidSelectSortType {

    @IBOutlet weak var viewSortFilter: UIView!
    @IBOutlet weak var lblSort: UILabel!
    @IBOutlet weak var lblFilter: UILabel!
    
    @IBOutlet weak var imgBack: UIImageView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var searchLabel: UILabel!
    @IBOutlet weak var filterButton: UIButton!
    @IBOutlet weak var searchHeading: UILabel!
    @IBOutlet weak var viewSearch: UIView!
    @IBOutlet weak var viewTxtSearch: UIView!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var viewNotification: UIView!
    @IBOutlet weak var lblNotificCount: UILabel!
    @IBOutlet weak var btnClear: UIButton!

    @IBOutlet weak var viewFilter: UIView!
    @IBOutlet weak var btnFilter: UIButton!
    
    @IBOutlet weak var viewSearchList: UIView!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var htTbl: NSLayoutConstraint!
    @IBOutlet weak var btnSearch: UIButton!
    var searchText = ""
    var suggentionsArray = [[String:String]]()
    var loading = true;
    var selectedCategory = String()
    var tagDict: [String:Any]? = nil
    var filDict : [String:Any]? = nil
    var subCateGories = [[String:JSON]]()
   // var products  = [[String:String]]()
    var products = [CategoryProduct]()
    var arrFilter : [Filter] = []
    @IBOutlet weak var lblNoData: UILabel!
      var isFilterApplied : Bool = false
    
    var sortByArray = [String:String]();
    var currentView = "grid"
    var searchString = ""; // variable to handle search case
    var jsonResponse:JSON?
    var loadMoreData = true
    var currentpage = 1
    //var curr_page=1
    var flag=false
    var imagView = UIImageView()
    var no_pro_check=false
    var homePage = Bool()
    var eanCode = String()
    var subCatflag=false
    var applyFilter = false
    var filterString = String()
    var filterDT = FilterData()
    var isFilterDisplay = true
    var isComesFromProductLayer = false
    var strTitleLayer = ""
    var activityIndi = UIActivityIndicatorView()
    var isSearchOpen : Bool = false
    private var cache = NSCache<AnyObject, AnyObject>()
    @IBOutlet weak var lblAppliedSortTop: UILabel!
    @IBOutlet weak var lblAppliedFilter: UILabel!
    @IBOutlet weak var lblAppliedFilterTop: UILabel!
    
    var arrSort: [SortType] = [SortType]()
    
    //MARK:- ViewController Life Cycle
    override func viewDidLoad() {
        
        searchLabel.setFont(fontFamily: "Cairo-Bold", fontSize: 22)

        
        activityIndi.frame = CGRect(x: 0.0, y: 0.0, width: 40.0, height: 40.0)
        activityIndi.center = self.view.center
        activityIndi.hidesWhenStopped = true
        activityIndi.style =
            UIActivityIndicatorView.Style.gray
        //self.view.addSubview(activityIndi)
        // activityIndi.startAnimating()
        activityIndi.isHidden = true
        self.isFilterApplied = false
        setUI()
        
        NotificationCenter.default.addObserver(self, selector: #selector(cedMageDefaultCollection.reloadData(_:)), name: NSNotification.Name(rawValue: "loadProductsAgainId"), object: nil);
        
        self.collectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "Cell")
        let nib3 = UINib(nibName: "CatProductCell", bundle: nil)
        self.collectionView.register(nib3, forCellWithReuseIdentifier: "CatProductCell")
        
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        
        filterButton.addTarget(self, action: #selector(filterButtonTapped(_:)), for: .touchUpInside)
        filterButton.setTitle(APP_LBL().filter, for: .normal)
        filterButton.roundCorners()
        filterButton.setBorder()
         self.lblNoData.text = APP_LBL().no_product_found.uppercased()
        self.lblNoData.isHidden = true
        self.filterButton.superview?.isHidden = true//!self.isFilterDisplay
        
        lblAppliedFilter.layer.cornerRadius = lblAppliedFilter.frame.size.height/2
        lblAppliedFilter.clipsToBounds = true
        self.lblAppliedFilter.isHidden = true
        
        lblAppliedFilterTop.layer.cornerRadius = lblAppliedFilterTop.frame.size.height/2
        lblAppliedFilterTop.clipsToBounds = true
        lblAppliedFilterTop.isHidden = true
        
        lblAppliedSortTop.layer.cornerRadius = lblAppliedSortTop.frame.size.height/2
        lblAppliedSortTop.clipsToBounds = true
        lblAppliedSortTop.isHidden = true
        
        
        if strTitleLayer != "" {
            searchHeading.text = strTitleLayer
            searchLabel.text = strTitleLayer.uppercased()
        } else {
            searchHeading.text = APP_LBL().search_results + " "
            searchLabel.text = self.searchString.uppercased()
        }
        
        if(searchString != ""){
            
            let detail : [AnalyticKey:String] = [
                .SearchTerm : searchString
            ]
            API().setEvent(eventName: .Search, eventDetail: detail) { (json, err) in }
             Analytics.logEvent(AnalyticsEventSearch, parameters: [AnalyticsParameterSearchTerm : searchString])
            
            self.getCollection(urlToRequest: "mobiconnect/catalog/productsearchwithfilter",dataToPost: ["q":searchString, "page":String(currentpage), "country_id" : APP_DEL.selectedCountry.country_code ?? "SA"]);
        }
        else{
            self.getCollection(urlToRequest: "mobiconnect/catalog/productwithattribute/",dataToPost: ["id":selectedCategory, "page":String(currentpage),"theme":"1", "country_id" : APP_DEL.selectedCountry.country_code ?? "SA"]);
        }
        
        
        let value=UserDefaults.standard.value(forKey: "AppleLanguages") as! [String]

        if APP_DEL.selectedLanguage == Arabic{
            imgBack.image = UIImage(named: "icon_back_white_Arabic")
        }
        else{
            imgBack.image = UIImage(named: "icon_back_white_round")
        }

        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardNotification(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardNotification(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        setupHideKeyboardOnTap()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
      
        
//        self.navigationController?.navigationBar.barTintColor = UIColor.black
//        self.navigationController?.navigationBar.tintColor = .white
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        if navigationController?.responds(to: #selector(getter: UINavigationController.interactivePopGestureRecognizer)) ?? false {
            
            self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
            self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        }

        IQKeyboardManager.shared().isEnabled = false
        IQKeyboardManager.shared().isEnableAutoToolbar = false
        
        self.tblView.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        APP_DEL.strFilterWithDeeplink = ""
        
        IQKeyboardManager.shared().isEnabled = true
        IQKeyboardManager.shared().isEnableAutoToolbar = true
    }
    
    var isKeyboardShowing = false
    @objc func handleKeyboardNotification(notification: NSNotification) {
        
        self.isKeyboardShowing = (notification.name == UIResponder.keyboardWillShowNotification)
        
        if self.isKeyboardShowing {
            
        } else {
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.closeSuggetionView()
            }
        }
    }
    
    @IBAction func btnBackAction(_ sender: UIButton) {
           
           self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSortAction(_ sender: UIButton) {
           
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NejreeSortVC") as! NejreeSortVC
        vc.arrSort = self.arrSort
        vc.preSort = self.sortType
        vc.delegateDidSelectSortType = self
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        self.present(vc, animated: true, completion: nil)
    }
    
//    @IBAction func btnFilterAction(_ sender: UIButton) {
//
//
//    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?)
    {
        self.tblView.layer.removeAllAnimations()

        self.htTbl.constant = self.tblView.contentSize.height
        UIView.animate(withDuration: 0.5) {
            self.updateViewConstraints()
            self.loadViewIfNeeded()
        }
    }
    
    func setUI() {
                
        let value=UserDefaults.standard.value(forKey: "AppleLanguages") as! [String]
        if value[0] == "ar" {
            self.txtSearch.textAlignment = .right
        } else {
            self.txtSearch.textAlignment = .left
        }
        
        viewNotification.round(redius: 16)
        
        lblNotificCount.round(redius: 5)
        
        viewSortFilter.properLightShadow()
//        viewSortFilter.layer.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
//        viewSortFilter.layer.borderWidth = 0.5
//        viewSortFilter.round(redius: 6)
//        viewSortFilter.backgroundColor = .clear
        lblSort.text = APP_LBL().sort.uppercased()
        lblSort.font = UIFont(name: "Cairo-Regular", size: 15)!
        lblFilter.text = APP_LBL().filter.uppercased()
        lblFilter.font = UIFont(name: "Cairo-Regular", size: 15)!
        
        viewSearch.round(redius: 16)
        viewTxtSearch.round(redius: 16)
        viewTxtSearch.layer.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        viewTxtSearch.layer.borderWidth = 0.5
        viewTxtSearch.layer.shadowColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        viewTxtSearch.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        viewTxtSearch.layer.shadowRadius = 4
        viewTxtSearch.layer.shadowOpacity = 0.5
        viewTxtSearch.isHidden = true
        
        viewSearchList.round(redius: 16)

        viewFilter.round(redius: 16)
        
        self.tblView.rowHeight = UITableView.automaticDimension
        self.tblView.estimatedRowHeight = UITableView.automaticDimension
        self.tblView.register(UINib(nibName: "SearchProductCell", bundle: nil), forCellReuseIdentifier: "SearchProductCell")
        self.tblView.dataSource = self
        self.tblView.delegate = self
        self.tblView.reloadData()
        self.tblView.allowsSelection = true
        
        tblView.keyboardDismissMode = .onDrag
        
        self.txtSearch.delegate = self
        self.txtSearch.placeholder = APP_LBL().search
        self.txtSearch.autocorrectionType = .no
        
        self.htTbl.constant = 4.0
        self.viewSearchList.isHidden = true
        self.tblView.isHidden = true
                
    }
    
    
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        
        if scrollView == collectionView {

            
            if self.isSearchOpen{
                
                self.txtSearch.text = ""
                self.searchText(text: "")
                self.view.endEditing(true)
                self.btnClearAction(self.btnClear)
            }
            
        }
        
   
        
    }
    
       func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
           
           return true
       }
    
     //MARK:- Pagination and scrollview delegate
    func checkPagination(){
        
        
     
        
        var baseURL = ""
        if searchString != ""{
            
            if self.currentpage == 1{
                 baseURL = "mobiconnect/catalog/productsearchwithfilter";
            }
            else
            {
                 baseURL = "mobiconnect/catalog/productsearch";
            }
            
           
        }
        else
        {
            if self.currentpage == 1 {
                
                baseURL = "mobiconnect/catalog/productwithattribute/"
            }
            else
            {
                baseURL = "mobiconnect/catalog/productwithoutattribute/"
            }
            
        }
        
        if(searchString != ""){
            
      
            var customer_id = ""
            var hashkey = ""
            
            let defaults = UserDefaults.standard
            if defaults.bool(forKey: "isLogin") {
                
                if let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] {
                    
                    hashkey = userInfoDict["hashKey"] ?? ""
                    customer_id = userInfoDict["customerId"] ?? ""
                }
            }
            
            self.getCollection(urlToRequest: baseURL,dataToPost: ["q":searchString, "page":String(currentpage),"multi_filter":filterString, "customer_id" : customer_id, "hashkey" : hashkey, "country_id" : APP_DEL.selectedCountry.country_code ?? "SA"]);
        }
        else
        {
            if isComesFromProductLayer{
                getCollection(urlToRequest: baseURL, dataToPost: ["multi_filter":filterString,"q":searchString,"id":selectedCategory,"page":String(currentpage),"theme":"1","store_id":UserDefaults.standard.value(forKey: "storeId") as! String, "country_id" : APP_DEL.selectedCountry.country_code ?? "SA"])
            }
            else
            {
                getCollection(urlToRequest: baseURL, dataToPost: ["multi_filter":filterString,"q":searchString,"id":selectedCategory,"page":String(currentpage),"theme":"1","store_id":UserDefaults.standard.value(forKey: "storeId") as! String, "country_id" : APP_DEL.selectedCountry.country_code ?? "SA"])
            }
        }

        
    }
       
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if scrollView == self.collectionView{
            if  scrollView.contentOffset.y + scrollView.frame.size.height >= (scrollView.contentSize.height - 500) {
            
            if no_pro_check == false && self.loading == false {
                self.loading = true
                   currentpage += 1
                self.checkPagination()
                }
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
   
    
    func clearViewAndVariables(){
        self.showHttpErorImage(me: self, img: "Empty")
        products = [];
        sortByArray = [String:String]();
        subCateGories = [[String:JSON]]();
        currentpage = 1;
        loadMoreData = true;
    }
    
    //MARK:- Collectionview Delegate methods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section==0
        {
            return subCateGories.count
        }
        return products.count
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if indexPath.section==0{
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "subcategoriesCell", for: indexPath) as! defaultGridCell
            let gradient: CAGradientLayer = CAGradientLayer()
            gradient.frame = cell.subCategoryView.frame
            gradient.colors = [UIColor(hexString:"#2cb200")?.cgColor ?? "", UIColor(hexString: "#7FB200")?.cgColor ?? ""]
            gradient.locations = [0.0, 0.5, 1.0]
            gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
            gradient.endPoint = CGPoint(x: 1.0, y: 0.5)
            gradient.cornerRadius=5
            //cell.subCategoryView.layer.insertSublayer(gradient, at: 0)
            
            cell.subCategoryLabel.numberOfLines=0
            
            cell.subCategoryLabel.backgroundColor=UIColor.clear
            cell.subCategoryLabel.text=subCateGories[indexPath.row]["category_name"]?.stringValue
            cell.subCategoryLabel.textColor=UIColor.white
            cell.subCategoryLabel.numberOfLines=0
            
            
            return cell
        }
        
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CatProductCell", for: indexPath) as? CatProductCell {
          
            if (APP_DEL.isDiscountTagEnable == "0"){
                cell.offerLabel.isHidden = true
            }
            else
            {
                let offerString = products[indexPath.item].offer ?? ""
                
                if offerString != "" {
                    
                    let offer = " \(offerString)% \(APP_LBL().off.uppercased()) "
                    
                    cell.offerLabel.text = offer
                    cell.offerLabel.isHidden = false
                } else {
                    cell.offerLabel.isHidden = true
                }
            }
            
            cell.lblProductName.text = products[indexPath.item].product_name
            
            let tagString = products[indexPath.item].tag ?? ""
            if tagString != "" {
                
                
                cell.lblTags.text = " " + tagString + " "
                cell.lblTags.isHidden = false
            } else {
                cell.lblTags.isHidden = true
            }
                       
            
            
            let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: products[indexPath.item].regular_price!)
            attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 1, range: NSMakeRange(0, attributeString.length))
            
            cell.lblBrandName.text = products[indexPath.item].brands_name
            cell.lblRegularPrice.isHidden = true
            if products[indexPath.item].special_price != "no_special" {
                
                cell.lblPrice.text = products[indexPath.item].special_price;
                
                cell.lblRegularPrice.attributedText = attributeString
                
                cell.lblRegularPrice.isHidden = false
                cell.lblRegularPrice.alpha = 1
                
            } else {
                
                cell.lblPrice.text = products[indexPath.item].regular_price
                
                cell.lblRegularPrice.isHidden = true
                cell.lblRegularPrice.alpha = 0
            }
              cell.imgProduct.image = nil
            
//            if let downloadURL = SDImageCache.shared.imageFromCache(forKey: products[indexPath.item].product_image!){
//                cell.imgProduct!.image = downloadURL
//            } else {
                cell.imgProduct!.sd_setImage(with: URL(string: products[indexPath.item].product_image!), placeholderImage: nil)
     //       }
            

            
            return cell
            

        }else{
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath)
            return cell;
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.section==0{
            
            let data=subCateGories[indexPath.row]["category_id"]?.stringValue
            if let viewController =  UIStoryboard(name: "categorylayouts", bundle: nil).instantiateViewController(withIdentifier: "cedMageDefaultCollection") as?  cedMageDefaultCollection {
                viewController.selectedCategory = data!
                self.navigationController?.pushViewController(viewController, animated: true)
                return
            }
        }
        
        _ = products[indexPath.row];
        let productview = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "productViewController") as! productViewController;
        productview.product_id = products[indexPath.row].product_id ?? ""
        APP_DEL.productIDglobal = products[indexPath.row].product_id ?? ""
        productview.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(productview
            , animated: true);
        
//        let productview = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "rootPageViewController") as! cedMageProductViewRoot;
//        productview.pageData = products as NSArray;
//        let instance = cedMage.singletonInstance;
//        instance.storeParameterInteger(parameter: indexPath.row);
//        self.navigationController?.pushViewController(productview
//            , animated: true);
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
        
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.section==0{
            return CGSize(width: collectionView.frame.width/3-10, height: 50)
        }
        if self.currentView == "grid" {
             return CGSize(width: (collectionView.frame.size.width/2)-8, height: (collectionView.frame.size.width/2)+70)
        }else{
             return CGSize(width: (collectionView.frame.size.width/2)-3, height: (collectionView.frame.size.width/2)+108)
        }
    }
    
    func showEmptyScreen() {
        
        let pop = emptySearch()
        self.view.addSubview(pop)
        pop.translatesAutoresizingMaskIntoConstraints = false
        pop.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 0).isActive = true
        pop.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0).isActive = true
        pop.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0).isActive = true
        pop.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0).isActive = true
        pop.exploreButton.addTarget(self, action: #selector(exploreTapped(_:)), for: .touchUpInside)
        
        pop.sorryLabel.text = APP_LBL().sorry_there_is_no_match_for_your_search
      //  pop.exploreButton.setBorder()
        pop.exploreButton.setTitle(APP_LBL().explore_our_products.uppercased(), for: .normal)
        //pop.exploreButton.roundCorners()
    }
    
    @objc func exploreTapped(_ sender: UIButton) {
        
        //self.tabBarController?.selectedViewController = self.tabBarController?.viewControllers?[0]
        self.tabBarController?.selectedIndex = 2
        self.navigationController?.popToRootViewController(animated: true)
    }
    
//    func parseData(json: JSON, index: String){
//        let json = json[0]
//
//        if(json.stringValue.lowercased() == "NO_PRODUCTS".lowercased()){
//            no_pro_check=true
//            if(self.products.count == 0){
//                //self.renderNoDataImage(view:self,imageName:"noProduct");
//                showEmptyScreen()
//                return;
//            }
//            else{
//                self.loadMoreData = false;
//            }
//            return
//        }
//
//
//        if json["data"]["status"].stringValue.lowercased() == "EXCEPTION".lowercased() {
//
//            self.clearViewAndVariables()
//            self.collectionView.reloadData()
//
//            return;
//        }
//
//
//        self.jsonResponse = json
//        for (_,result) in json["data"]["products"] {
//
//            let product_id = result["product_id"].stringValue;
//            let regular_price = result["regular_price"].stringValue;
//            let special_price = result["special_price"].stringValue;
//            let product_name = result["product_name"].stringValue;
//            let product_image = result["product_image"].stringValue;
//            let type = result["type"].stringValue;
//            let review = result["review"].stringValue;
//            let show_both_price = result["show-both-price"].stringValue;
//            let Inwishlist = result["Inwishlist"].stringValue
//            let starting_from = result["starting_from"].stringValue;
//            let from_price = result["from_price"].stringValue;
//            let offerText  = result["offer"].stringValue
//            let brandName  = result["brands_name"].stringValue
//
//            var wishlistItemId = "-1";
//            if(result["wishlist-item-id"].stringValue != ""){
//                wishlistItemId = result["wishlist-item-id"].stringValue;
//            }
//            self.products.append(["product_id":product_id,"regular_price":regular_price, "special_price":special_price,"product_name":product_name,"product_image":product_image,"type":type,"review":review,"show-both-price":show_both_price,"starting_from":starting_from,"from_price":from_price,"Inwishlist":Inwishlist,"wishlist-item-id":wishlistItemId,"offerText":offerText,"brands_name" : brandName]);
//        }
//
//        if !subCatflag{
//            subCatflag=true
//            if let sub_category=json["data"]["sub_category"].array{
//                for node in sub_category {
//                    if let subCateData=node.dictionary{
//                        subCateGories.append(subCateData)
//                    }
//                }
//            }
//        }
//
//
//        for (_,val) in json["data"]["sort"]{
//            for (keyInr,valInr) in val{
//                self.sortByArray[keyInr] = valInr[0].stringValue;
//            }
//        }
//        if self.products.count > 0 {
//            self.collectionView.reloadData()
//        }
//        //  stackView.subviews.forEach({ $0.removeFromSuperview() });
//
//    }
    
    var sortType: SortType? = nil
    func didSelectSortType(res: Bool, type: SortType) {
        self.products.removeAll()
        self.collectionView.reloadData()
        
        self.no_pro_check = false
        
        self.currentpage = 1
        if res {
            
            sortType = type
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "loadProductsAgainId"), object: nil)
        }
        
        if ((sortType?.code ?? "") == "default") || ((sortType?.code ?? "") == "") {
            self.lblAppliedSortTop.isHidden = true
        } else {
            self.lblAppliedSortTop.isHidden = false
        }
    }
    
    //MARK:- API CAll
    func getCollection(urlToRequest: String, dataToPost:[String:String]?) {
        
        
        flag = true
        activityIndi.isHidden = false
        activityIndi.startAnimating()
        cedMageLoaders.addDefaultLoader(me: self);
        
        if (defaults.object(forKey: "userInfoDict") != nil) {
            
            userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
        }
        
        
        var postData = [String:String]()
        
        if let data = dataToPost {
            
            for (key,val) in data {
                
                postData[key] = val;
            }
        }
        
        
        if defaults.bool(forKey: "isLogin") {
            
            postData["hashkey"] = userInfoDict["hashKey"]
            postData["customer_id"] = userInfoDict["customerId"]
        }
        
        
        
        if let filtersToSend = defaults.object(forKey: "filtersToSend") as? String {
            
            if tagDict == nil {
                do {
                    tagDict = try (JSONSerialization.jsonObject(with: filtersToSend.data(using: .utf8)!, options: [.fragmentsAllowed]) as? [String:Any] ?? [:])
                } catch { }
            }
            
            defaults.removeObject(forKey: "filtersToSend");
            
            
            
            
        }
        
        if ((postData["multi_filter"] ?? "") != "") {
            
            do {
                let tempDT = postData["multi_filter"]!.data(using: .utf8)!
                filDict = try (JSONSerialization.jsonObject(with: tempDT, options: []) as? [String:Any] ?? [:])
            } catch { }
        } else {
            filDict = nil
        }
        
        var mergedDict : [String:Any] = [String:Any]()
        
        if tagDict != nil {
            mergedDict = mergedDict.merging(tagDict!) { (first, last) -> Any in first }
        }
        
        if filDict != nil {
            mergedDict = mergedDict.merging(filDict!) { (first, last) -> Any in first }
        }
        
        if mergedDict.count > 0 {
            let data = try! JSONSerialization.data(withJSONObject: mergedDict, options: [])
            postData["multi_filter"] = String(data: data, encoding: .utf8)!;
        } else {
            postData["multi_filter"] = ""
        }
        
        if APP_DEL.strFilterWithDeeplink != ""{
            postData["multi_filter"] = APP_DEL.strFilterWithDeeplink
            
        }
        
        let storeId = defaults.value(forKey: "storeId")
        if storeId != nil {
            postData["store_id"] = (storeId as! String)
        }
        self.loading = true
        
        if self.filterString == ""{
            
            self.lblAppliedFilter.isHidden = true
            self.lblAppliedFilterTop.isHidden = true
            
        }
        else
        {
            self.lblAppliedFilter.isHidden = false
            self.lblAppliedFilterTop.isHidden = false
        }
        
        if let sType = sortType {
            postData["sort_type"] = sType.id ?? ""
        } else {
            postData["sort_type"] = "1"
        }
        
        API().callAPI(endPoint: urlToRequest, method: .POST, param: postData) { (json_res, err) in
            
            //   DispatchQueue.main.async {
            
            cedMageLoaders.removeLoadingIndicator(me: self);
            
            if self.currentpage == 1{
                self.products = []
            }
            
            if(json_res.stringValue.lowercased() == "NO_PRODUCTS".lowercased()){
                self.no_pro_check=true
                if(self.products.count == 0){
                    //  self.showEmptyScreen()
                }
            }
            
            if json_res[0].stringValue == "NO_PRODUCTS" {
                if(self.products.count == 0){
                    //    self.showEmptyScreen()
                    
                }
                
                self.no_pro_check = true;
            }
            
            else if json_res.stringValue == "NO_PRODUCTS"
            {
                if(self.products.count == 0){
                    self.showEmptyScreen()
                }
                self.no_pro_check = true;
                if self.currentpage == 1{
                    self.products = []
                }
            }
            
            if (self.currentpage == 1) {
                if self.no_pro_check == false {
                    
                    if !self.isFilterApplied{
                        
                        
                        do {
                            
                            if self.searchString != ""{
                                self.arrFilter = try JSONDecoder().decode([Filter].self, from: json_res[0][1][0]["filter"].rawData())
                                
                                self.arrSort = try JSONDecoder().decode([SortType].self, from: json_res[0][2].rawData())
                            }
                            else
                            {
                                self.arrFilter = try JSONDecoder().decode([Filter].self, from: json_res[1][0]["filter"].rawData())
                                
                                self.arrSort = try JSONDecoder().decode([SortType].self, from: json_res[1][0]["sort_type"].rawData())
                                
                            }
                            
                            
                        } catch { }
                        
                    } else {
                        
                        print("isFilterApplied == true")
                    }
                }
                self.products = []
            }
            
            do {
                
                
                if self.searchString != "" && self.currentpage == 1{
                    if self.currentpage == 1{
                        
                        let catData = try json_res[0][0]["data"]["products"].rawData()
                        let temp = try JSONDecoder().decode([CategoryProduct].self, from: catData)
                        self.products.append(contentsOf: temp as [CategoryProduct])
                        
                        self.arrSort = try! JSONDecoder().decode([SortType].self, from: json_res[0][2].rawData())
                    }
                    else
                    {
                        let catData = try json_res[0]["data"]["products"].rawData()
                        let temp = try JSONDecoder().decode([CategoryProduct].self, from: catData)
                        self.products.append(contentsOf: temp as [CategoryProduct])
                    }
                    
                }
                else{
                    if self.currentpage == 1{
                        
                        let catData = try json_res[0]["data"]["products"].rawData()
                        let temp = try JSONDecoder().decode([CategoryProduct].self, from: catData)
                        self.products.append(contentsOf: temp as [CategoryProduct])
                        
                        self.arrSort = try! JSONDecoder().decode([SortType].self, from: json_res[1][0]["sort_type"].rawData())
                    }
                    else
                    {
                        let catData = try json_res[0]["data"]["products"].rawData()
                        let temp = try JSONDecoder().decode([CategoryProduct].self, from: catData)
                        self.products.append(contentsOf: temp as [CategoryProduct])
                    }
                    
                    
                }
                
                
                
            } catch { }
            
            
            
            
            if self.products.count > 0 {
                self.collectionView.reloadData()
                self.lblNoData.isHidden = true
                self.viewSortFilter.isHidden = false
            }
            else
            {
                self.viewSortFilter.isHidden = true
                self.lblNoData.isHidden = false
            }
            
            self.loading = false;
        }
    }
    
     @objc func reloadData(_ notification: NSNotification){
            clearViewAndVariables()
   
        
        
        if strTitleLayer != "" {
            
            searchHeading.text = strTitleLayer
//            searchLabel.text = ""
            
        }
        else
        {
            searchHeading.text = APP_LBL().search_results + " "
//            searchLabel.text = self.searchString.uppercased()
        }
 
        self.checkPagination()
        
        }
    
  
    func showHttpErorImage(me:UIViewController,img:String){
        
        if img == "Empty"
        {
            imagView.removeFromSuperview()
            return
        }
        
        let bounds = UIScreen.main.bounds
        imagView = UIImageView(frame: bounds)
        imagView.image = UIImage(named: img)
        imagView.contentMode = .scaleAspectFit
        me.view.addSubview(imagView)
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}


    
    
    
//MARK:- SEARCH CODE
extension cedMageDefaultCollection {

    @objc func filterButtonTapped(_ sender: UIButton) {
            let vc = UIStoryboard(name: "categorylayouts", bundle: nil).instantiateViewController(withIdentifier: "nejreeFilterController") as! nejreeFilterController
            vc.searchString = self.searchString
            vc.vc = self
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        func filterApply(filter: String, filterDT: FilterData, isForShowFilterView: Bool) {
            
            //self.viewFilter.isHidden = false
            self.lblAppliedFilter.isHidden = false
            self.lblAppliedFilterTop.isHidden = true
            
            if self.filterString == ""{
                 self.lblAppliedFilter.isHidden = true
                self.lblAppliedFilterTop.isHidden = true
                
            }
            else
             {
                 self.lblAppliedFilter.isHidden = false
                self.lblAppliedFilterTop.isHidden = false
            }
            
            if isForShowFilterView {
                return;
            }
            self.products.removeAll()
            self.collectionView.reloadData()
            
            self.no_pro_check = false
            
            self.currentpage = 1
            
            self.applyFilter = true
            self.filterString = filter
            self.filterDT = filterDT
            
            if self.filterString == ""{
                
                self.isFilterApplied = false
                 
            }
            else
            {
                self.isFilterApplied = true
               
            }
            
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "loadProductsAgainId"), object: nil)
        }
        
    
    @IBAction func btnFilterAction(_ sender: Any) {
                
        self.viewFilter.isHidden = true
         self.lblAppliedFilter.isHidden = true
        self.lblAppliedFilterTop.isHidden = true
        
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NejreeCategoryFilterVC") as! NejreeCategoryFilterVC
        vc.hidesBottomBarWhenPushed = true
        vc.categoryId = self.selectedCategory
        vc.filterDT = self.filterDT
        vc.isCategoryFilter = false
         vc.arrFilter = self.arrFilter
        vc.modalPresentationStyle = .overFullScreen
        vc.delegateFilterApply = self
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func btnSearchBottomAction(_ sender: Any) {
        
        if let viewController =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "cedMageSearchPage") as?  cedMageSearchPage {
            
            viewController.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(viewController, animated: true)
            
        }
    }
    
    
    @IBAction func btnSearchAction(_ sender: Any) {

        if self.viewTxtSearch.isHidden == true {

            self.viewTxtSearch.isHidden = true
            //self.viewSearch.isHidden = false
            self.isSearchOpen = true
            self.txtSearch.becomeFirstResponder()

            UIView.animate(withDuration: 0.3, delay: 0.0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.7, options: [.curveLinear], animations: { () -> Void in

                self.viewTxtSearch.isHidden = false
                //self.viewSearch.isHidden = true
                self.view.layoutIfNeeded()

                
                
            }, completion: nil)

        } else {

            self.view.endEditing(true)

            
            if txtSearch.text == ""{
                                      
                          self.closeSuggetionView()
                          return
                      }
                       
            
            Analytics.logEvent(AnalyticsEventSearch, parameters: [AnalyticsParameterSearchTerm : searchText])
            let detail : [AnalyticKey:String] = [
                .SearchTerm : searchText
            ]
            API().setEvent(eventName: .Search, eventDetail: detail) { (json, err) in }

            self.searchString = searchText
             
            if let viewController =  UIStoryboard(name: "categorylayouts", bundle: nil).instantiateViewController(withIdentifier: "cedMageDefaultCollection") as?  cedMageDefaultCollection {
                
                viewController.hidesBottomBarWhenPushed = true
                viewController.searchString = self.searchString
                viewController.selectedCategory = ""
                
                if (self.txtSearch.text != "") {
                    
                    self.navigationController?.pushViewController(viewController, animated: true)
                }
            }
            
            
//            self.hidesBottomBarWhenPushed = true
//
//            if (self.txtSearch.text != "") {
//
//                self.btnClearAction(self.btnClear)
//
//                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "loadProductsAgainId"), object: nil)
//            }
        }


    }
    func closeSuggetionView() {
        self.isSearchOpen = false
        self.txtSearch.text = ""
       self.searchText(text: "")
       self.view.endEditing(true)
       self.btnClearAction(self.btnClear)
    }
    @IBAction func btnClearAction(_ sender: Any) {

        if self.txtSearch.text == "" {

            self.view.endEditing(true)

            self.viewTxtSearch.isHidden = false
            //self.viewSearch.isHidden = true
            UIView.animate(withDuration: 0.3, delay: 0.0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.7, options: [.curveLinear], animations: { () -> Void in

                self.viewTxtSearch.isHidden = true
                //self.viewSearch.isHidden = false
                self.isSearchOpen = false
                self.view.layoutIfNeeded()

            }, completion: nil)

        } else {

            self.txtSearch.text = ""
            self.searchText(text: "")
        }
    }


    func searchText(text: String) {

        searchText = text

        if text == "" {

            self.htTbl.constant = 4.0
            self.viewSearchList.isHidden = true
            self.tblView.isHidden = true

        } else {

            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(getHint), object: nil)
            self.perform(#selector(getHint), with: nil, afterDelay: 1)
        }

        //checkNoData()
    }


    @objc func getHint() {

        if(searchText.count >= 3) {

            searchText = searchText.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            getAutocomplete()
        }
    }

    func getAutocomplete() {

        guard let storeId = UserDefaults.standard.value(forKey: "storeId") as? String else {return}

        var postData = [String:String]()
        postData["page"] = "1"
        postData["store_id"] = storeId
        postData["q"] = searchText
        postData["autosearch"] = "yes"
        
        let defaults = UserDefaults.standard
        if defaults.bool(forKey: "isLogin") {
            
            if let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] {
                
                postData["hashkey"] = userInfoDict["hashKey"]
                postData["customer_id"] = userInfoDict["customerId"]
            }
        }
        
        postData["country_id"] = APP_DEL.selectedCountry.country_code ?? "SA"
        
        activityIndi.isHidden = false
        activityIndi.startAnimating()

        API().callAPI(endPoint: "mobiconnect/catalog/productsearch", method: .POST, param: postData) { (json, err) in

           // DispatchQueue.main.async {

                self.activityIndi.isHidden = true
                self.activityIndi.stopAnimating()
                
              //  cedMageLoaders.removeLoadingIndicator(me: self);

                let json_0 = json[0]

                if err == nil {

                    if (json[0].stringValue != "NO_PRODUCTS")
                    {
                        self.suggentionsArray.removeAll()
                        for (_,value) in json_0["data"]["suggestion"]
                        {

                            var data = [String:String]()
                            data["product_id"] = value["product_id"].stringValue
                            data["product_name"] = value["product_name"].stringValue
                            data["product_image"] = value["product_image"].stringValue
                            self.suggentionsArray.append(data)
                        }

                        self.tblView.reloadData()
                        self.viewSearchList.isHidden = false
                        self.tblView.isHidden = false
                    }
                    else
                    {
                        self.htTbl.constant = 4.0
                        self.viewSearchList.isHidden = true
                        self.tblView.isHidden = true
                    }

                }
          //  }
        }
    }

}

extension cedMageDefaultCollection: UITextFieldDelegate {

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        if let str = textField.text, let swtRange = Range(range, in: str) {

            let fullString = str.replacingCharacters(in: swtRange, with: string)
            searchText(text: fullString)
        }

        return true;
    }

//    func textFieldDidEndEditing(_ textField: UITextField) {
//
//        self.displayClear(res: ((textField.text ?? "").count > 0))
//    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {

       // filter(text: textField.text ?? "")
        self.view.endEditing(true)

        Analytics.logEvent(AnalyticsEventSearch, parameters: [AnalyticsParameterSearchTerm : searchText])

        self.searchString = searchText
        self.hidesBottomBarWhenPushed = true

        if (self.txtSearch.text != "") {

            Analytics.logEvent(AnalyticsEventSearch, parameters: [AnalyticsParameterSearchTerm : searchText])
            let detail : [AnalyticKey:String] = [
                .SearchTerm : searchText
            ]
            API().setEvent(eventName: .Search, eventDetail: detail) { (json, err) in }
            
            self.searchString = searchText
            
            if let viewController =  UIStoryboard(name: "categorylayouts", bundle: nil).instantiateViewController(withIdentifier: "cedMageDefaultCollection") as?  cedMageDefaultCollection {
                
                viewController.hidesBottomBarWhenPushed = true
                viewController.searchString = self.searchString
                viewController.selectedCategory = ""
                
                if (self.txtSearch.text != "") {
                    
                    self.navigationController?.pushViewController(viewController, animated: true)
                }
            }
                   
            
            //self.btnClearAction(self.btnClear)
            
            //NotificationCenter.default.post(name: NSNotification.Name(rawValue: "loadProductsAgainId"), object: nil)
        }

        return true
    }
}

extension cedMageDefaultCollection: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return suggentionsArray.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchProductCell") as! SearchProductCell
        cell.selectionStyle = .none

        cell.imgView.sd_setImage(with: URL(string: suggentionsArray[indexPath.row]["product_image"] ?? ""), placeholderImage: nil)

        cell.lblTitle.text = suggentionsArray[indexPath.row]["product_name"]
        
        cell.lblDevider.isHidden = (indexPath.row == (suggentionsArray.count - 1))
        
        let value=UserDefaults.standard.value(forKey: "AppleLanguages") as! [String]
        if value[0] == "ar" {
            cell.lblTitle.textAlignment = .right
            cell.imgArrow.image = UIImage(named: "arrows_ar")
        } else {
            cell.lblTitle.textAlignment = .left
            cell.imgArrow.image = UIImage(named: "arrows")
        }

        return cell;
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        self.btnClearAction(self.btnClear)
        
        self.closeSuggetionView()
        
        let vc = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "productViewController") as! productViewController
        APP_DEL.productIDglobal = suggentionsArray[indexPath.row]["product_id"]!
        vc.product_id = suggentionsArray[indexPath.row]["product_id"]!
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        return UITableView.automaticDimension
    }

}
