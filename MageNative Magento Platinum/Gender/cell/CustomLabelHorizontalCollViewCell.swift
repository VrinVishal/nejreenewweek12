//
//  CustomLabelHorizontalCollViewCell.swift
//  MageNative Magento Platinum
//
//  Created by Jayesh Dabhi on 18/02/21.
//  Copyright © 2021 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit



protocol CustomLabelHorizontalCollViewCellDelegate {
    func anyDataTappedCallBack(index:Int, type: CustomLabelHorizontalCollType)
}

enum CustomLabelHorizontalCollType: Int {
    case kLangauge = 0
    case kGender = 1
    case kCountry = 2
}

enum CustomLabelContryCellThemeType: Int {
    case kBlackBorder = 0
    case kNoBorder = 1
}

class CustomLabelHorizontalCollViewCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var collView: UICollectionView!
    @IBOutlet weak var cnstrntCollHeight: NSLayoutConstraint!

    var collectionHeight = CGFloat(60.0)
    
    var arrData = NSMutableArray()
    var strType = -1 // 0 = Country, 1 = language
    var selectedIndex = -1
    var collType = CustomLabelHorizontalCollType(rawValue: 1)
    var delegate : CustomLabelHorizontalCollViewCellDelegate?
    var store_id = "2"
    var theme = -1 // -1 = default, 1 = black border view
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        collView.register(UINib(nibName: "CustomTextBoxCollectionCell", bundle: nil), forCellWithReuseIdentifier: "CustomTextBoxCollectionCell")
        collView.delegate = self
        collView.dataSource = self
        
        self.lblTitle.font = UIFont(name: "Cairo-Regular", size: 17)
        
       

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func reloadWholeCellData() {
        if APP_DEL.selectedLanguage == Arabic {
            lblTitle.textAlignment = .right
        } else {
            lblTitle.textAlignment = .left
        }
        collView.reloadData()
    }
    
}

extension CustomLabelHorizontalCollViewCell : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        arrData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CustomTextBoxCollectionCell", for: indexPath) as! CustomTextBoxCollectionCell
    
        let rec = arrData[indexPath.row]
        if (theme == 1){
            if (selectedIndex == indexPath.row){
                cell.viewBg.backgroundColor = .black
                cell.lblName.textColor = .white
            } else {
                cell.viewBg.backgroundColor = .white
                cell.lblName.textColor = .black
            }
        } else {
            if (selectedIndex == indexPath.row){
                cell.viewBg.backgroundColor = .white
                cell.viewUnselected.isHidden = true
            } else {
                cell.viewBg.backgroundColor = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 0.7)
                cell.viewUnselected.isHidden = false
            }
        }
        
        var strTitle = ""
        if rec is Gender {
            if (store_id == "2"){
                strTitle = (rec as! Gender).title_ar?.uppercased() ?? ""
            } else {
                strTitle = (rec as! Gender).title?.uppercased() ?? ""
            }
            cell.img.image = nil
            if (theme == 1){
                cell.viewUnselected.isHidden = true
//                cell.viewBg.setBorderBlack()
                cell.viewBg.layer.borderColor = UIColor.darkGray.cgColor
                cell.viewBg.layer.borderWidth = 0.5
            }
        } else if rec is countryData {
//            if (APP_DEL.selectedLanguage == Arabic){
//                strTitle = (rec as! countryData).label_ar ?? ""
//            } else {
//                strTitle = (rec as! countryData).label ?? ""
//            }
            strTitle = ""
            let imgStr = ((rec as! countryData).country_flag_image ?? "")
            if let imgUrl = URL(string: (rec as! countryData).country_flag_image ?? ""), imgStr != "" {
                cell.img.sd_setImage(with: imgUrl, placeholderImage: nil)
            } else {
                cell.img.backgroundColor = UIColor(hex: cat_bg_default_color_code)
            }
            cell.viewBg.layer.cornerRadius = 25.0
            cell.viewBg.clipsToBounds = true
            cell.viewBg.backgroundColor = .clear
            if (theme == 1){
                if (selectedIndex == indexPath.row){
                    cell.viewUnselected.isHidden = true
                } else {
                    cell.viewUnselected.isHidden = false
                }
            }
        } else {
            cell.img.image = nil
            strTitle = ((rec as! [String : String])["name"] ?? "").uppercased()
            if (theme == 1){
                cell.viewUnselected.isHidden = true
                cell.viewBg.layer.borderColor = UIColor.darkGray.cgColor
                cell.viewBg.layer.borderWidth = 0.5
            }
        }
        cell.lblName.text = strTitle
        
//        if APP_DEL.selectedLanguage == Arabic{
//        
//            cell.lblName.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
//        }
//        else{
//            cell.lblName.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
//        }
        
        
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let rec = arrData[indexPath.row]
        var strTitle = ""
        if rec is Gender {
//            if (APP_DEL.selectedLanguage == Arabic){
//                strTitle = (rec as! Gender).title_ar ?? ""
//            } else {
//                strTitle = (rec as! Gender).title ?? ""
//            }
//
//            if let font = UIFont(name: "Cairo-Regular", size: 17) {
//                let fontAttributes = [NSAttributedString.Key.font: font]
//               let myText = strTitle
//               let size = (myText as NSString).size(withAttributes: fontAttributes)
            return CGSize(width: collectionView.frame.size.width/CGFloat(2.0) - 10, height: collectionHeight)

//            }
        } else if rec is countryData {
            return CGSize(width: collectionHeight, height: collectionHeight)
        } else {
//            if (APP_DEL.selectedLanguage == Arabic){
//                strTitle = (rec as! [String : String])["name"] ?? ""
//            } else {
//                strTitle = (rec as! [String : String])["name"] ?? ""
//            }
//
//            if let font = UIFont(name: "Cairo-Regular", size: 17) {
//                let fontAttributes = [NSAttributedString.Key.font: font]
//               let myText = strTitle
//               let size = (myText as NSString).size(withAttributes: fontAttributes)
//                return CGSize(width: max(size.width, 100.0), height: 60.0)
//            }
//            return CGSize(width: collectionView.frame.size.width/CGFloat((arrData.count ?? 0)), height: collectionHeight)
            return CGSize(width: collectionView.frame.size.width/CGFloat(2.0) - 10, height: collectionHeight)

            
            
        }
        
        return CGSize.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.anyDataTappedCallBack(index: indexPath.row, type: collType!)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    
}
