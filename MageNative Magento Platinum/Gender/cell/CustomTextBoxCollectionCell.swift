//
//  CustomTextBoxCollectionCell.swift
//  MageNative Magento Platinum
//
//  Created by Jayesh Dabhi on 18/02/21.
//  Copyright © 2021 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class CustomTextBoxCollectionCell: UICollectionViewCell {
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var viewUnselected: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.lblName.font = UIFont(name: "Cairo-Regular", size: 17)
    }

}
