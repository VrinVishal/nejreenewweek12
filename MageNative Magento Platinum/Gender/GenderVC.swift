//
//  GenderVC.swift
//  MageNative Magento Platinum
//
//  Created by vishal.n on 27/01/21.
//  Copyright © 2021 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

struct Gender: Codable {
    
    let id: String?
    let title: String?
    let title_ar: String?

}

var arrGender: [Gender] = []

class GenderVC: UIViewController {

    @IBOutlet weak var imgGIF: UIImageView!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var tblViewHT: NSLayoutConstraint!
    @IBOutlet weak var btnCountinue: UIButton!

    var countryList = [countryData]()
    var selectedGenderIndex = -1
    var selectedCountryIndex = -1
    var selectedLangaugeIndex = -1
    var Stores = [[String:String]]()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // forcefully set arabic langauge
        
        let lg = UserDefaults.standard.value(forKey: "AppleLanguages") as! [String]
        
        if lg[0] == "ar" || lg[0] == "ur" {
            
            APP_DEL.selectedLanguage =  Arabic
            UserDefaults.standard.set(["ar","en","fr"], forKey: "AppleLanguages")
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
            self.view.semanticContentAttribute = .forceRightToLeft
            
            
            APP_DEL.selectedLanguage =  "ar"
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
            Bundle.setLanguage("ar")
        } else {
            
            APP_DEL.selectedLanguage =  "en"
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
            Bundle.setLanguage(lg[0])
            
            APP_DEL.selectedLanguage =  English
            UserDefaults.standard.set(["en","ar","fr"], forKey: "AppleLanguages")
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
            self.view.semanticContentAttribute = .forceRightToLeft
            
        }
        
        
       
        
        self.btnCountinue.titleLabel?.font = UIFont(name: "Cairo-Regular", size: 17)
        self.btnCountinue.setAttributedTitle(NSAttributedString(string: APP_LBL().continue_.uppercased(),
                                                                attributes: [NSAttributedString.Key.foregroundColor: UIColor(hex: "#FBAD18")!, NSAttributedString.Key.underlineStyle: NSUnderlineStyle.thick.rawValue]), for: .normal)
        self.getAllCountryAPI()

        loadGIF()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func loadGIF() {
        
        //"https://media.giphy.com/media/EuwbSy46466Q0/giphy.gif"
//        imgGIF.image = UIImage.gif(url: gender_gif_url)
        if let mediaUrl = URL(string: gender_gif_url) {
            print("URL MEDIA = \(mediaUrl.pathExtension)")
            if mediaUrl.pathExtension == "gif"{
                imgGIF.image = UIImage.gif(url: gender_gif_url)
            } else {
                if let imgUrl = URL(string: gender_gif_url) {
                    imgGIF.sd_setImage(with: imgUrl, placeholderImage: nil)
                } else {
                    imgGIF.backgroundColor = UIColor(hex: cat_bg_default_color_code)
                }
            }
        }
        
    }

    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.tabBarController?.tabBar.isHidden = true
        
        tblView.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        tblView.removeObserver(self, forKeyPath: "contentSize", context: nil)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?)
    {
        
        tblView.layer.removeAllAnimations()
        tblViewHT.constant = tblView.contentSize.height

        UIView.animate(withDuration: 0.1) {
            
            self.updateViewConstraints()
            self.loadViewIfNeeded()
        }
    }
    
    @IBAction func btnCountineClicked(sender : UIButton) {
        print("btnCountineClicked \(selectedGenderIndex)-\(selectedLangaugeIndex)-\(selectedCountryIndex)")
        if (selectedGenderIndex < 0){
            APP_DEL.showAlertWithOkButton(parentVC: self, strTitle: APP_LBL().error, strMsg: APP_LBL().please_select_gender)
        } else if (selectedLangaugeIndex < 0) {
            APP_DEL.showAlertWithOkButton(parentVC: self, strTitle: APP_LBL().error, strMsg: APP_LBL().please_select_language)
        } else if (selectedCountryIndex < 0){
            APP_DEL.showAlertWithOkButton(parentVC: self, strTitle: APP_LBL().error, strMsg: APP_LBL().please_select_country)
        } else {
            self.setValues()
        }
    }
    
    func setValues() {
        
        UserDefaults.standard.setValue((arrGender[self.selectedGenderIndex]).id, forKey: KEY_GENDER)

        APP_DEL.selectedCountry = self.countryList[self.selectedCountryIndex]
        
        APP_DEL.selectedCountry.syncronize()
        self.selectStore(store: self.Stores[self.selectedLangaugeIndex]["store_id"])
        
        
        return;
        var countryAlert = APP_LBL().change_country_alert_description
        countryAlert = countryAlert.replacingOccurrences(of: "XYZ", with: countryList[selectedCountryIndex].getCountryName())
        countryAlert = countryAlert.replacingOccurrences(of: "ABC", with: countryList[selectedCountryIndex].getCurrencySymbol())

        let confirmationAlert = UIAlertController(title: APP_LBL().change_country, message: countryAlert, preferredStyle: UIAlertController.Style.alert);
       
        confirmationAlert.addAction(UIAlertAction(title: APP_LBL().ok, style: .default, handler: { (action: UIAlertAction!) in
            UserDefaults.standard.setValue((arrGender[self.selectedGenderIndex]).id, forKey: KEY_GENDER)

            APP_DEL.selectedCountry = self.countryList[self.selectedCountryIndex]
            APP_DEL.selectedCountry.syncronize()
            self.selectStore(store: self.Stores[self.selectedLangaugeIndex]["store_id"])

//            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
//                // your code here
//                APP_DEL.setTabBarAfterAPIsCalling()
//            }
        }));
        
        confirmationAlert.addAction(UIAlertAction(title: APP_LBL().cancel, style: .destructive, handler: { (action: UIAlertAction!) in

            
        }));

        self.present(confirmationAlert, animated: true, completion: nil)
       
    }
    
    func selectStore(store:String?){
        //let params = ["store_id":store! as String]
        UserDefaults.standard.setValue(store, forKey: "storeId")
        self.setStore(url: "mobiconnectstore/setstore/"+store!)
    }
    
    func setStore(url: String) {
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);
        
        API().callAPI(endPoint: url, method: .GET, param: [:]) { (json_res, err) in
            
            //    DispatchQueue.main.async {
            
            cedMageLoaders.removeLoadingIndicator(me: self);
            
            if err == nil {
                
                let lang = json_res[0]["locale_code"].stringValue
                
                let language=lang.components(separatedBy: "_")
                if language[0]=="ar"
                {
                    UserDefaults.standard.set(["ar","en","fr"], forKey: "AppleLanguages")
                    UIView.appearance().semanticContentAttribute = .forceRightToLeft
                }
                else if language[0]=="en"
                {
                    UserDefaults.standard.set(["en","ar","fr"], forKey: "AppleLanguages")
                    UIView.appearance().semanticContentAttribute = .forceLeftToRight
                }
                
                APP_DEL.changeLanguage()
            }
            // }
        }
    }
    
    func getStoreList() {
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);
        
        API().callAPI(endPoint: "mobiconnectstore/getlist", method: .GET, param: [:]) { (json_res, err) in
            
            //  DispatchQueue.main.async {
            
            cedMageLoaders.removeLoadingIndicator(me: self);
            
            if err == nil {
                
                let json = json_res[0]
                
                self.Stores.removeAll()
                var index = 0
                for store in json["store_data"].arrayValue{
                    
                    let group_id = store["group_id"].stringValue
                    
                    let code = store["code"].stringValue
                    let store_id = store["store_id"].stringValue
                    let name = store["name"].stringValue
                    let is_active = store["is_active"].stringValue
                    let storeobject = ["group_id":group_id,"code":code,"store_id":store_id,"name":name,"is_active":is_active]
                    self.Stores.append(storeobject)
                    
                    
                    print(APP_DEL.selectedLanguage)
                    
                    if APP_DEL.selectedLanguage == Arabic{
                        if (store["store_id"].stringValue == "2"){
                            self.selectedLangaugeIndex = index
                        }
                    }
                    else{
                        if (store["store_id"].stringValue == "1"){
                            self.selectedLangaugeIndex = index
                        }
                    }
                    
                    
                    
                    index = index + 1
                }
                self.setUI()
            }
            //   }
        }
    }
    
    func getAllCountryAPI(){
        APP_DEL.activityIndicator = NVActivityIndicatorView(frame: CGRect(x: (UIScreen.main.bounds.width/2) - 25.0, y: (UIScreen.main.bounds.height/2) - 25.0, width: 50.0, height: 50.0), type: .ballPulse, color: UIColor.init(hexString: "#fcb215"), padding: 0)
        
        APP_DEL.window?.addSubview(APP_DEL.activityIndicator)
        APP_DEL.window?.bringSubviewToFront(APP_DEL.activityIndicator)
        APP_DEL.activityIndicator.isHidden = true
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);
        
        let storeID = UserDefaults.standard.value(forKey: "storeId") as! String?
        var postData = [String:String]()
        postData["store_id"] = "2"
        API().callAPI(endPoint: "mobiconnect/getcountrybystore", method: .POST, param: postData) { (json_res, err) in

            cedMageLoaders.removeLoadingIndicator(me: self);


            let catData = try! json_res[0]["country"].rawData()
            self.countryList = try! JSONDecoder().decode([countryData].self, from: catData)
            
            for i in 0...self.countryList.count-1 {
                if ((self.countryList[i].country_code ?? "") == (APP_DEL.selectedCountry.country_code ?? "")) {
                    self.selectedCountryIndex = i
                }
            }
            self.getStoreList()

        }
//        if let path = Bundle.main.path(forResource: "CountryList", ofType: "json") {
//            do {
//                  let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
//                guard let json_res = try? JSON(data: data) else {
//                    return
//                }
//                  let catData = try! json_res[0]["Country"].rawData()
//                 self.countryList = try! JSONDecoder().decode([countryData].self, from: catData)
//                 self.setUI()
//              } catch {
//                   // handle error
//              }
//        }
    }

}

extension GenderVC {
    
    func setUI() {
        
        tblView.register(UINib(nibName: "CustomLabelHorizontalCollViewCell", bundle: nil), forCellReuseIdentifier: "CustomLabelHorizontalCollViewCell")
        tblView.register(UINib(nibName: "GenderCell", bundle: nil), forCellReuseIdentifier: "GenderCell")
        tblView.rowHeight = UITableView.automaticDimension
        tblView.estimatedRowHeight = UITableView.automaticDimension
        tblView.dataSource = self
        tblView.delegate = self
        tblView.reloadData()
    }
}

extension GenderVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (is_country_selection_allow == "1") {
            return 3
        } else {
            return 2
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomLabelHorizontalCollViewCell") as! CustomLabelHorizontalCollViewCell
        cell.selectionStyle = .none
        cell.collType = CustomLabelHorizontalCollType(rawValue: indexPath.row)
        cell.arrData = NSMutableArray(array: arrGender)
        cell.lblTitle.textColor = .white
        
//        if APP_DEL.selectedLanguage == English{
//
//            cell.lblTitle.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
//
//        }
//        else
//        {
//            cell.lblTitle.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
//        }
        
        switch cell.collType {
            case .kCountry:
                if (is_country_selection_allow == "1") {
                    cell.arrData = NSMutableArray(array:self.countryList)
                    cell.selectedIndex = selectedCountryIndex
                    cell.lblTitle.text = APP_LBL().choose_country.uppercased()
                }
                break;
            case .kGender:
                cell.arrData = NSMutableArray(array: arrGender)
                cell.selectedIndex = selectedGenderIndex
                cell.store_id = self.Stores[selectedLangaugeIndex]["store_id"] ?? "2"
                cell.lblTitle.text = APP_LBL().choose_shopping_for.uppercased()
                break;

            case .kLangauge:
                cell.arrData = NSMutableArray(array: self.Stores)
                cell.selectedIndex = selectedLangaugeIndex
                cell.lblTitle.text = ""
                
                    break;
                
            default:
                break;
        }
        cell.delegate = self
        cell.reloadWholeCellData()
        return cell;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

}

extension GenderVC : CustomLabelHorizontalCollViewCellDelegate {
    func anyDataTappedCallBack(index: Int, type: CustomLabelHorizontalCollType) {
        switch type {
            case .kCountry:
                selectedCountryIndex = index
                break;
                
            case .kGender:
                selectedGenderIndex = index
                break;

            case .kLangauge:
                    selectedLangaugeIndex = index
                var lang = "ar"
                if (self.Stores[selectedLangaugeIndex]["store_id"] == "2"){
                    lang = "ar"
                } else {
                    lang = "en"
                }
                
                let language=lang.components(separatedBy: "_")
                if language[0] == "ar"
                {
                    
//                    self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
//                    self.btnCountinue.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
//                    self.imgGIF.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                    
                    APP_DEL.selectedLanguage =  Arabic
                    UserDefaults.standard.set(["ar","en","fr"], forKey: "AppleLanguages")
//                    UIView.appearance().semanticContentAttribute = .forceRightToLeft
//                    self.view.semanticContentAttribute = .forceRightToLeft
                }
                else if language[0] == "en"
                {
                    
//                    self.view.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
//
//                    self.btnCountinue.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
//                    self.imgGIF.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
                    
                    APP_DEL.selectedLanguage =  English
                    UserDefaults.standard.set(["en","ar","fr"], forKey: "AppleLanguages")
//                    UIView.appearance().semanticContentAttribute = .forceLeftToRight
//                    self.view.semanticContentAttribute = .forceLeftToRight
                }
                self.btnCountinue.setAttributedTitle(NSAttributedString(string: APP_LBL().continue_.uppercased(),
                                                                        attributes: [NSAttributedString.Key.foregroundColor: UIColor(hex: "#FBAD18")!, NSAttributedString.Key.underlineStyle: NSUnderlineStyle.thick.rawValue]), for: .normal)
                
            
                APP_DEL.changeLanguage()
                
                break;
            
            default:
                break;
        }
        tblView.reloadData()
    }
}
