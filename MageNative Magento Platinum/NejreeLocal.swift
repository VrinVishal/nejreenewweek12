//
//  NejreeLocal.swift
//  MageNative Magento Platinum
//
//  Created by vishal.n on 14/07/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import Foundation

struct LocalBannerLayer : Codable {
    
    let cat_id : String?
    let store_id : String?
    let expire_second : String?
    let store_time : String?
    var banner_layer : [BannerLayer]?
}

class LocalBanner {
    
    private let keyLocalBanner: String = "LocalCategoryIDBanner"
    
    func getBanner(cat_id: String, store_id: String) -> LocalBannerLayer? {

        if  (UserDefaults.standard.value(forKey: keyLocalBanner) != nil), let dict  = UserDefaults.standard.value(forKey: keyLocalBanner)
        {
            if let jsonData = try? JSONSerialization.data(withJSONObject: dict, options: [.prettyPrinted])
            {
                do
                {
                    let arrBanner = try JSONDecoder().decode([LocalBannerLayer].self, from: jsonData)

                    let index = arrBanner.firstIndex(where: { (banner) -> Bool in
                        return ((cat_id == banner.cat_id) && (store_id == banner.store_id))
                    })
                    
                    if let finalIndex = index {
                                                
                        
                        let temp_store = arrBanner[finalIndex].store_time!
                        print("Store Time")
                        print(temp_store)
                        
                        let temp_current = DATE().getDate(date: Date())
                        print("Current Time")
                        print(temp_current)
                        
                        let storeDT = DATE().getDate(string: temp_store)
                        let currentDT = DATE().getDate(string: temp_current)
                        
                        let seconds = Int(currentDT.timeIntervalSince(storeDT))
                        print("DURATION")
                        print(seconds)
                        
                        let expire_second = Int(arrBanner[finalIndex].expire_second ?? "0")!
                        let new_expire_second = Int(APP_DEL.category_record_seconds) ?? 0
                        
                        if (seconds > expire_second) || (new_expire_second != expire_second) {
                        
                            print("LAYER EXPIRED")
                            return nil
                            
                        } else {
                            
                            return arrBanner[finalIndex]
                        }
                    }
                    
                } catch { }
            }
        }

        return nil
    }
    
    func setBanner(banner: LocalBannerLayer) {
        
        do {
            
            if  (UserDefaults.standard.value(forKey: keyLocalBanner) != nil), let dict  = UserDefaults.standard.value(forKey: keyLocalBanner)
            {
                if let jsonData = try? JSONSerialization.data(withJSONObject: dict, options: [.prettyPrinted])
                {
                    do
                    {
                        let arrBanner = try JSONDecoder().decode([LocalBannerLayer].self, from: jsonData)
                        print("HomeLayer:-", arrBanner)
                        
                        
                        var arrNewBanner = arrBanner
                        
                        
                        let index = arrNewBanner.firstIndex(where: { (temp_banner) -> Bool in
                            return ((banner.cat_id == temp_banner.cat_id) && (banner.store_id == temp_banner.store_id))
                        })
                        
                        
                        if let finalIndex = index {
                            
                            arrNewBanner[finalIndex] = banner
                            
                        } else {
                            
                            arrNewBanner.append(banner)
                        }
                        
                        let json : [[String : Any]] = try JSONSerialization.jsonObject(with: JSONEncoder().encode(arrNewBanner), options: []) as! [[String : Any]]
                        UserDefaults.standard.set(json, forKey: keyLocalBanner)
                        UserDefaults.standard.synchronize()

                    } catch {
                        
                    }
                    
                }
                
            } else {
                
                let json : [[String : Any]] = try JSONSerialization.jsonObject(with: JSONEncoder().encode([banner]), options: []) as! [[String : Any]]
                UserDefaults.standard.set(json, forKey: keyLocalBanner)
                UserDefaults.standard.synchronize()
            }
            
        } catch {
            
        }
    }
    
    func removeLayer(cat_id: String, layer_id: String, store_id: String) {
        
        do {
            
            if  (UserDefaults.standard.value(forKey: keyLocalBanner) != nil), let dict  = UserDefaults.standard.value(forKey: keyLocalBanner)
            {
                if let jsonData = try? JSONSerialization.data(withJSONObject: dict, options: [.prettyPrinted])
                {
                    do
                    {
                        let arrBanner = try JSONDecoder().decode([LocalBannerLayer].self, from: jsonData)
                        print("HomeLayer:-", arrBanner)
                        
                        
                        var arrNewBanner = arrBanner
                        
                        
                        let index = arrNewBanner.firstIndex(where: { (banner) -> Bool in
                            return ((cat_id == banner.cat_id) && (store_id == banner.store_id))
                        })
                        
                        
                        if let finalIndex = index {
                            
                            var totalLayer = arrNewBanner[finalIndex].banner_layer ?? []
                            
                            let layer_index = totalLayer.firstIndex(where: { (temp_layer) -> Bool in
                                return (layer_id == temp_layer.layer_id)
                            })
                            
                            if let finalLayerIndex = layer_index {
                                
                                totalLayer.remove(at: finalLayerIndex)
                                
                                arrNewBanner[finalIndex].banner_layer = totalLayer
                            }
                            
                            let json : [[String : Any]] = try JSONSerialization.jsonObject(with: JSONEncoder().encode(arrNewBanner), options: []) as! [[String : Any]]
                            UserDefaults.standard.set(json, forKey: keyLocalBanner)
                            UserDefaults.standard.synchronize()
                        }
                        
                    } catch {
                        
                    }
                }
            }
            
        } catch {
            
        }
    }
}



class DATE {
    
    func getDate(date: Date) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone(identifier: "UTC")

        return dateFormatter.string(from: date)
    }
    
    func getDate(string: String) -> Date {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone(identifier: "UTC")

        return dateFormatter.date(from: string) ??  Date()
    }
}
