//
//  PaymentWebVC.swift
//  Tappoo
//
//  Created by vishal nayee on 19/05/20.
//  Copyright © 2020 Yesha. All rights reserved.
//

import UIKit
import WebKit




class PaymentWebVC: UIViewController , WKNavigationDelegate{

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var imgBackViewArrow: UIImageView!

    @IBOutlet weak var webView: WKWebView!
    var dictData = NSDictionary()
    var strResult = String()
    var btnimgBird = UIButton()

    var strTitle = APP_LBL().payment
    
    var failUrl: String = ""
    var payUrl: String = ""
    var successUrl: String = ""
    var notificationUrl: String = ""
    
    var deleagteCheckoutFail: PaymentFail?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblTitle.text = strTitle
        if APP_DEL.selectedLanguage == Arabic{
            imgBackViewArrow.image = UIImage(named: "icon_back_white_Arabic")
        }
        else{
            imgBackViewArrow.image = UIImage(named: "icon_back_white_round")
        }
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);
        webView.navigationDelegate = self
        webLoad()
    }
    
    //MARK:- Action Btn Menu
    @IBAction func actionBtnBack(_ sender: UIButton){
        self.deleagteCheckoutFail?.PaymentFailForCheckout(index: 0)
        self.navigationController?.popViewController(animated: true)
    }
    
    func webLoad() {
        let url = URL(string: payUrl ?? "")
        webView.load(URLRequest(url: url!))
    }

}

extension PaymentWebVC : UIWebViewDelegate {
    
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);

    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        cedMageLoaders.removeLoadingIndicator(me: self);

    }
    
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        cedMageLoaders.removeLoadingIndicator(me: self);

    }


        func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
            
            btnimgBird.setBackgroundImage(UIImage(named: ""), for: .normal)
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: btnimgBird)

            
                if navigationAction.request != nil{

                    let strRedirectUrl = navigationAction.request.url
                    let strUrl = strRedirectUrl?.description
                    
                    if (strUrl?.hasPrefix(self.successUrl))!{
                        self.webView.isHidden = true
                        self.successPayment()
//                        self.strResult = getQueryStringParameter(url: (strUrl)!, param: "result")!
//                        CheckoutPayment(result: self.strResult)
                    }
                    
                    
                    if (strUrl!.hasPrefix(self.failUrl)){
                        self.FailedPayment()
                        self.webView.isHidden = true
//                        self.strResult = getQueryStringParameter(url: (strUrl)!, param: "result")!
//                        CheckoutPayment(result: self.strResult)
                    }
                    
                }

    //        }
            decisionHandler(WKNavigationActionPolicy.allow)

        }
    
    
    func getQueryStringParameter(url: String, param: String) -> String? {
      guard let url = URLComponents(string: url) else { return nil }
      return url.queryItems?.first(where: { $0.name == param })?.value
    }

    func successPayment(){
        self.deleagteCheckoutFail?.PaymentFailForCheckout(index: 1)
        self.navigationController?.popViewController(animated: true)
    }
    
    func FailedPayment(){
        self.deleagteCheckoutFail?.PaymentFailForCheckout(index: 0)
        self.navigationController?.popViewController(animated: true)
    }
}

