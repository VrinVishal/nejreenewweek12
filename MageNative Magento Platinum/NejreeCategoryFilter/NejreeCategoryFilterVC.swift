//
//  NejreeCategoryFilterVC.swift
//  MageNative Magento Platinum
//
//  Created by vishal.n on 31/07/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit
import RangeSeekSlider

enum FilterType: String {

    case none = ""
    case Price = "1"
    case Size = "2"
    case Brands = "3"
    case Types = "4"
    case Gender = "5"
    case Color = "6"
    case Categories = "7"
    case Shop_1 = "8"
    case Shop_2 = "9"
    case Category_1 = "10"
    case Category_2 = "11"
}

struct Filter : Codable {
    
    var isExpand: Bool = false
    var type: FilterType = .none
    
    let att_code : String?
    let att_label : String?
    let filter_type : String?
    let filter_data : [Filter_data]?
    
    enum CodingKeys: String, CodingKey {

        case att_code = "att_code"
        case att_label = "att_label"
        case filter_type = "filter_type"
        case filter_data = "filter_data"
    }

    init(from decoder: Decoder) throws {
        
        let values = try decoder.container(keyedBy: CodingKeys.self)
        att_code = try values.decodeIfPresent(String.self, forKey: .att_code)
        att_label = try values.decodeIfPresent(String.self, forKey: .att_label)
        type = try (FilterType(rawValue: values.decodeIfPresent(String.self, forKey: .filter_type) ?? "") ?? .none)
        filter_type = try values.decodeIfPresent(String.self, forKey: .filter_type)
        filter_data = try values.decodeIfPresent([Filter_data].self, forKey: .filter_data)
    }
    
}

struct Filter_data : Codable {
        
    let filter_label : String?
    let filter_code : String?
    let filter_value : String?
    let filter_img : String?
    let min_price : String?
    let max_price : String?
}



class FilterData: NSObject {
    
    var selected_level_1_code = ""
    var selected_level_2_code = ""
    
    var tempMinPrice: CGFloat = 0.0
    var tempMaxPrice: CGFloat = 5000.0
   
    var isPriceApply = false
    var minPrice: CGFloat = 0.0
    var maxPrice: CGFloat = 0.0
    
    var size: [Int] = []
    var brand: [Int] = []
    var types: [Int] = []
    var shop_1: [Int] = []
    var shop_2: [Int] = []
    var category_1: [Int] = []
    var category_2: [Int] = []
    var gender: [Int] = []
    var color: [Int] = []
    var categories: [Int] = []
    
    var other: [String:[Int]] = [:]
    
    func getJsonString(arrFilter: [Filter]) -> String {
        
        var dict : [String : [String : String]] = [:]
        
        for fil in arrFilter {
            
            let type = fil.type
            
            if type == .Price {
                
                if isPriceApply {
                    
                    let minNew = Int(minPrice)
                    let maxNew = Int(maxPrice)
                    
                    dict[(fil.att_code!)] = ["0.0-0.0" : "\(minNew)-\(maxNew)"]
                }
                
            } else if type == .Size && size.count > 0 {
                                
                //dict[fil.att_code ?? ""] = [fil.filter_data?[size[0]].filter_code ?? "" : fil.filter_data?[size[0]].filter_value ?? ""]
                
                var temp : [String : String] = [:]
                for item in size {
                    temp[fil.filter_data?[item].filter_code ?? ""] = fil.filter_data?[item].filter_value ?? ""
                }
                
                dict[fil.att_code ?? ""] = temp
                
            } else if type == .Brands && brand.count > 0 {
                
                //dict[fil.att_code ?? ""] = [fil.filter_data?[brand[0]].filter_code ?? "" : fil.filter_data?[brand[0]].filter_label ?? ""]
                
                var temp : [String : String] = [:]
                for item in brand {
                    temp[fil.filter_data?[item].filter_code ?? ""] = fil.filter_data?[item].filter_value ?? ""
                }
                
                dict[fil.att_code ?? ""] = temp
                
            } else if type == .Types && types.count > 0 {
                
                //dict[fil.att_code ?? ""] = [fil.filter_data?[types[0]].filter_code ?? "" : fil.filter_data?[types[0]].filter_value ?? ""]
                
                var temp : [String : String] = [:]
                for item in types {
                    temp[fil.filter_data?[item].filter_code ?? ""] = fil.filter_data?[item].filter_value ?? ""
                }
                
                dict[fil.att_code ?? ""] = temp
                
            } else if type == .Shop_1 && shop_1.count > 0 {
                
                dict[fil.att_code ?? ""] = [fil.filter_data?[shop_1[0]].filter_code ?? "" : fil.filter_data?[shop_1[0]].filter_value ?? ""]
                
            } else if type == .Shop_2 && shop_2.count > 0 {
                
                dict[fil.att_code ?? ""] = [fil.filter_data?[shop_2[0]].filter_code ?? "" : fil.filter_data?[shop_2[0]].filter_value ?? ""]
                
            } else if type == .Category_1 && category_1.count > 0 {
                
                dict[fil.att_code ?? ""] = [fil.filter_data?[category_1[0]].filter_code ?? "" : fil.filter_data?[category_1[0]].filter_value ?? ""]
                
            } else if type == .Category_2 && category_2.count > 0 {
                
                dict[fil.att_code ?? ""] = [fil.filter_data?[category_2[0]].filter_code ?? "" : fil.filter_data?[category_2[0]].filter_value ?? ""]
                
            } else if type == .Gender && gender.count > 0 {
                
                //dict[fil.att_code ?? ""] = [fil.filter_data?[gender[0]].filter_code ?? "" : fil.filter_data?[gender[0]].filter_value ?? ""]
                
                var temp : [String : String] = [:]
                for item in gender {
                    temp[fil.filter_data?[item].filter_code ?? ""] = fil.filter_data?[item].filter_value ?? ""
                }
                
                dict[fil.att_code ?? ""] = temp
                
            } else if type == .Color && color.count > 0 {
                
                //dict[fil.att_code ?? ""] = [fil.filter_data?[color[0]].filter_code ?? "" : fil.filter_data?[color[0]].filter_value ?? ""]
                
                var temp : [String : String] = [:]
                for item in color {
                    temp[fil.filter_data?[item].filter_code ?? ""] = fil.filter_data?[item].filter_value ?? ""
                }
                
                dict[fil.att_code ?? ""] = temp
                
            } else if type == .Categories && categories.count > 0 {
                
                dict[fil.att_code ?? ""] = [fil.filter_data?[categories[0]].filter_code ?? "" : fil.filter_data?[categories[0]].filter_value ?? ""]
                
            } else if other.count > 0 {
                
                for (otherType, otherValue) in other {

                    let indexType = arrFilter.firstIndex { (item_section) -> Bool in
                        return (otherType == item_section.filter_type)
                    }
                    
                    if let tempIndexType = indexType {
                        
                        var temp : [String : String] = [:]
                        for subValue in otherValue {

                            temp[arrFilter[tempIndexType].filter_data?[subValue].filter_code ?? ""] = arrFilter[tempIndexType].filter_data?[subValue].filter_value ?? ""
                        }
                        
                        dict[arrFilter[tempIndexType].att_code ?? ""] = temp

                    } else {
                        
                    }
                }
            }
        }
            
        if dict == [:] {
            
            return "";
            
        } else {
            
            let data = try! JSONSerialization.data(withJSONObject: dict, options: [])
            return String(data: data, encoding: .utf8)!
        }
        
    }
}


protocol DidSelectPrice {
    func didSelectPrice(minPrice: CGFloat, maxPrice: CGFloat)
}

protocol FilterApply {
    func filterApply(filter: String, filterDT: FilterData, isForShowFilterView: Bool)
}

class NejreeCategoryFilterVC: UIViewController, DidSelectPrice {

    @IBOutlet weak var lblHeadingTitle: UILabel!
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var btnReset: UIButton!
    @IBOutlet weak var btnApply: UIButton!
    
    @IBOutlet weak var viewMainFilter: UIView!
    @IBOutlet weak var collView: UICollectionView!
    
    var totalPading : CGFloat = 30.0
    let expand_down = "expand_down"
    let expand_up = "expand_up"
    let nejreeColor = UIColor.init(hexString: "#FBAD18")
    
    var categoryId = ""
    var isCategoryFilter = false
    
    var delegateFilterApply: FilterApply?
    
    var arrFilter : [Filter] = []
    var filterDT = FilterData()
    
    
    var isGenderHide: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.getFilter()
    }
    
    func setUI() {
        
        self.lblHeadingTitle.text = APP_LBL().filter.uppercased()
        
        
        viewMainFilter.layer.cornerRadius = 15
        viewMainFilter.clipsToBounds = true
        
        collView.delegate = self
        collView.dataSource = self
        
        collView.register(UINib(nibName: "FilterHeaderCell", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "FilterHeaderCell")
        collView.register(UINib(nibName: "FilterFooterCell", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: "FilterFooterCell")
        
        collView.register(UINib(nibName: "FilterPriceCell", bundle: nil), forCellWithReuseIdentifier: "FilterPriceCell")
        collView.register(UINib(nibName: "FilterColorCell", bundle: nil), forCellWithReuseIdentifier: "FilterColorCell")
        collView.register(UINib(nibName: "FilterSizeCell", bundle: nil), forCellWithReuseIdentifier: "FilterSizeCell")
        collView.register(UINib(nibName: "FilterBrandCell", bundle: nil), forCellWithReuseIdentifier: "FilterBrandCell")
        collView.register(UINib(nibName: "FilterTextCell", bundle: nil), forCellWithReuseIdentifier: "FilterTextCell")
        
        collView.contentInset = UIEdgeInsets(top: 0, left: 15, bottom: 15, right: 15)
        
        collView.reloadData()
                
        updateUI()
    }
    
    func getFilter() {
        
        
        if self.arrFilter.count == 0 {
            
            var postData = [String:String]()
            
            guard let storeId = UserDefaults.standard.value(forKey: "storeId") as? String else {
                return;
            }
            postData["store_id"] = storeId
            postData["id"] = self.categoryId
                
            cedMageLoaders.removeLoadingIndicator(me: self);
            cedMageLoaders.addDefaultLoader(me: self);
            
            //https://dev05.nejree.com/rest/V1/mobiconnect/catalog/productattributelist
            //"mobiconnect/catalog/attributes"
            API().callAPI(endPoint: "mobiconnect/catalog/productattributelist", method: .POST, param: postData) { (json, err) in
                
              //  DispatchQueue.main.async {
                    
                    cedMageLoaders.removeLoadingIndicator(me: self);
                    
                    if err == nil {

                        if json[0]["status"].stringValue == "true" {
                            
                            self.arrFilter = try! JSONDecoder().decode([Filter].self, from: json[0]["filter"].rawData())
                            
                        } else {
                            
                            self.view.makeToast(json[0]["message"].stringValue, duration: 1.0, position: .center)
                        }
                    }
                    
                    self.updateCommonDetail()
               // }
            }
            
        } else {
            
            self.updateCommonDetail()
        }
    }
    
    func updateCommonDetail() {
        
        for temp in self.arrFilter {
            
            let type = temp.type
            
            if type == .Price {
                
                if (self.filterDT.minPrice == 0.0) && (self.filterDT.maxPrice == 0.0) {
                    
                    if (temp.filter_data?.count ?? 0) > 0 {
                        
                        self.filterDT.minPrice = CGFloat(Double(temp.filter_data![0].min_price ?? "0.0")!)
                        self.filterDT.maxPrice = CGFloat(Double(temp.filter_data![0].max_price ?? "0.0")!)
                        
                        self.filterDT.tempMinPrice = CGFloat(Double(temp.filter_data![0].min_price ?? "0.0")!)
                        self.filterDT.tempMaxPrice = CGFloat(Double(temp.filter_data![0].max_price ?? "0.0")!)
                        
                    } else {
                        
                        self.filterDT.minPrice = self.filterDT.tempMinPrice
                        self.filterDT.maxPrice = self.filterDT.tempMaxPrice
                    }
                }
            }
        }
                                
        
//        let temp = self.arrFilter.filter { (fil) -> Bool in
//
//            if (
//                    (fil.type == .Price) ||
//                    (fil.type == .Size) ||
//                    (fil.type == .Brands) ||
//                    (fil.type == .Types) ||
//                    ((fil.type == .Shop_1) && (self.isCategoryFilter == false)) ||
//                    ((fil.type == .Shop_2) && (self.isCategoryFilter == false)) ||
//                    ((fil.type == .Category_1) && (self.isCategoryFilter == true)) ||
//                    ((fil.type == .Category_2) && (self.isCategoryFilter == true)) ||
//                    ((fil.type == .Gender) && (self.isGenderHide == false)) ||
//                    (fil.type == .Color)
//                    //(fil.type == .Categories)
//
//                ) {
//
//                return true
//            }
//
//            return false
//        }
        
//        self.arrFilter = temp
        
        self.preselectedOption()
    }
    
 
    func preselectedOption() {
        
        if self.filterDT.selected_level_1_code != "" {
                
            if isCategoryFilter {
                
                let filt_cat = self.arrFilter.first(where: { (item_category) -> Bool in (item_category.type == .Category_1)})
                if filt_cat != nil {
                    
                    let index_section = self.arrFilter.firstIndex { (item_section) -> Bool in
                        return (filt_cat!.type == item_section.type)
                    }
                    
                    if let tempIndexSection = index_section {
                                            
                        let index_row = self.arrFilter[tempIndexSection].filter_data?.firstIndex { (item_row) -> Bool in
                            
                            return ((item_row.filter_code ?? "") == (self.filterDT.selected_level_1_code))
                        }
                        
                        if let tempIndexRow = index_row {
                            
                            filterDT.category_1 = [tempIndexRow]
                            self.arrFilter[tempIndexSection].isExpand = true
                        }
                    }
                }
                
            } else {
                
                let filt_shop = self.arrFilter.first(where: { (item_shop) -> Bool in (item_shop.type == .Shop_1)})
                if filt_shop != nil {
                    
                    let index_section = self.arrFilter.firstIndex { (item_section) -> Bool in
                        return (filt_shop!.type == item_section.type)
                    }
                    
                    if let tempIndexSection = index_section {
                                            
                        let index_row = self.arrFilter[tempIndexSection].filter_data?.firstIndex { (item_row) -> Bool in
                            
                            return ((item_row.filter_code ?? "") == (self.filterDT.selected_level_1_code))
                        }
                        
                        if let tempIndexRow = index_row {
                            
                            filterDT.shop_1 = [tempIndexRow]
                            self.arrFilter[tempIndexSection].isExpand = true
                        }
                    }
                }
            }
            
        } else {
            
            filterDT.shop_1 = []
            filterDT.category_1 = []
        }
        
        if self.filterDT.selected_level_2_code != "" {
            
            if isCategoryFilter {
                
                let filt_type = self.arrFilter.first(where: { (item_types) -> Bool in (item_types.type == .Category_2)})
                if filt_type != nil {
                    
                    let index_section = self.arrFilter.firstIndex { (item_section) -> Bool in
                        return (filt_type!.type == item_section.type)
                    }
                    
                    if let tempIndexSection = index_section {
                                            
                        let index_row = self.arrFilter[tempIndexSection].filter_data?.firstIndex { (item_row) -> Bool in
                            
                            return ((item_row.filter_code ?? "") == (self.filterDT.selected_level_2_code))
                        }
                        
                        if let tempIndexRow = index_row {
                            
                            filterDT.category_2 = [tempIndexRow]
                            self.arrFilter[tempIndexSection].isExpand = true
                        }
                    }
                }
                
            } else {
                
                let filt_shop = self.arrFilter.first(where: { (item_shop) -> Bool in (item_shop.type == .Shop_2)})
                if filt_shop != nil {
                    
                    let index_section = self.arrFilter.firstIndex { (item_section) -> Bool in
                        return (filt_shop!.type == item_section.type)
                    }
                    
                    if let tempIndexSection = index_section {
                                            
                        let index_row = self.arrFilter[tempIndexSection].filter_data?.firstIndex { (item_row) -> Bool in
                            
                            return ((item_row.filter_code ?? "") == (self.filterDT.selected_level_2_code))
                        }
                        
                        if let tempIndexRow = index_row {
                            
                            filterDT.shop_2 = [tempIndexRow]
                            self.arrFilter[tempIndexSection].isExpand = true
                        }
                    }
                }
            }
            
        } else {
            
            filterDT.shop_2 = []
            filterDT.category_2 = []
        }
        
        if filterDT.size.count > 0 {
            
            let filt_type = self.arrFilter.first(where: { (item_type) -> Bool in (item_type.type == .Size)})
            if filt_type != nil {
                
                let index_section = self.arrFilter.firstIndex { (item_section) -> Bool in
                    return (filt_type!.type == item_section.type)
                }
                
                if let tempIndexSection = index_section {
                                        
                    self.arrFilter[tempIndexSection].isExpand = true
                }
            }
        }
        
        if filterDT.types.count > 0 {
            
            let filt_type = self.arrFilter.first(where: { (item_type) -> Bool in (item_type.type == .Types)})
            if filt_type != nil {
                
                let index_section = self.arrFilter.firstIndex { (item_section) -> Bool in
                    return (filt_type!.type == item_section.type)
                }
                
                if let tempIndexSection = index_section {
                                        
                    self.arrFilter[tempIndexSection].isExpand = true
                }
            }
        }
        
        if ((filterDT.tempMinPrice != filterDT.minPrice) || (filterDT.tempMaxPrice != filterDT.maxPrice)) {
        
            let filt_type = self.arrFilter.first(where: { (item_type) -> Bool in (item_type.type == .Price)})
            if filt_type != nil {
                
                let index_section = self.arrFilter.firstIndex { (item_section) -> Bool in
                    return (filt_type!.type == item_section.type)
                }
                
                if let tempIndexSection = index_section {
                                        
                    self.arrFilter[tempIndexSection].isExpand = true
                }
            }
        }
        
        if filterDT.gender.count > 0 {
            
            let filt_type = self.arrFilter.first(where: { (item_type) -> Bool in (item_type.type == .Gender)})
            if filt_type != nil {
                
                let index_section = self.arrFilter.firstIndex { (item_section) -> Bool in
                    return (filt_type!.type == item_section.type)
                }
                
                if let tempIndexSection = index_section {
                                        
                    self.arrFilter[tempIndexSection].isExpand = true
                }
            }
        }
        
        if filterDT.color.count > 0 {
            
            let filt_type = self.arrFilter.first(where: { (item_type) -> Bool in (item_type.type == .Color)})
            if filt_type != nil {
                
                let index_section = self.arrFilter.firstIndex { (item_section) -> Bool in
                    return (filt_type!.type == item_section.type)
                }
                
                if let tempIndexSection = index_section {
                                        
                    self.arrFilter[tempIndexSection].isExpand = true
                }
            }
        }
        
        if filterDT.brand.count > 0 {
            
            let filt_type = self.arrFilter.first(where: { (item_type) -> Bool in (item_type.type == .Brands)})
            if filt_type != nil {
                
                let index_section = self.arrFilter.firstIndex { (item_section) -> Bool in
                    return (filt_type!.type == item_section.type)
                }
                
                if let tempIndexSection = index_section {
                                        
                    self.arrFilter[tempIndexSection].isExpand = true
                }
            }
        }
        
        if filterDT.other.count > 0 {
            
            for (otherType, otherValue) in filterDT.other {

                let item_section = arrFilter.firstIndex { (item_section) -> Bool in
                    return (otherType == item_section.filter_type)
                }
                
                if let tempIndexSection = item_section {
                    
                    if otherValue.count > 0 {
                        self.arrFilter[tempIndexSection].isExpand = true
                    }

                } else {
                    
                }
            }
        }
        
//        if filterDT.other.count > 0 {
//
//            for (otherType, otherValue) in filterDT.other {
//
//                if otherValue.count > 0 {
//
//                    let filt_type = self.arrFilter.first(where: { (item_type) -> Bool in (item_type.filter_type == otherType)})
//                    if filt_type != nil {
//
//                        let index_section = self.arrFilter.firstIndex { (item_section) -> Bool in
//                            return (filt_type!.type == item_section.type)
//                        }
//
//                        if let tempIndexSection = index_section {
//
//                            self.arrFilter[tempIndexSection].isExpand = true
//                        }
//                    }
//                }
//            }
//        }
        
        self.collView.reloadData()
    }

    
    @IBAction func btnCloseAction(_ sender: UIButton) {
        
//        self.navigationController?.popViewController(animated: true)
        let tempFilter = self.filterDT.getJsonString(arrFilter: self.arrFilter)
        self.delegateFilterApply?.filterApply(filter: tempFilter, filterDT: self.filterDT, isForShowFilterView: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnResetAction(_ sender: UIButton) {
        
        self.filterDT = FilterData()
        self.collView.reloadData()
        self.delegateFilterApply?.filterApply(filter: "", filterDT: FilterData(), isForShowFilterView: false)
//        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)

    }
    
    @IBAction func btnApplyAction(_ sender: UIButton) {
        
        let tempFilter = self.filterDT.getJsonString(arrFilter: self.arrFilter)
        print(tempFilter)
        self.delegateFilterApply?.filterApply(filter: tempFilter, filterDT: self.filterDT, isForShowFilterView: false)
//        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)

    }
    
    func updateUI() {
        
        self.btnReset.backgroundColor = UIColor.black
        self.btnReset.setTitle(APP_LBL().reset.uppercased(), for: .normal)
        self.btnReset.setTitleColor(UIColor.white, for: .normal)
        
        self.btnApply.backgroundColor = UIColor.black
        self.btnApply.setTitle(APP_LBL().apply.uppercased(), for: .normal)
        self.btnApply.setTitleColor(UIColor.white, for: .normal)
    }
    
    @objc func btnExpandAction(_ sender: UIButton) {
        
        self.arrFilter[sender.tag].isExpand = !self.arrFilter[sender.tag].isExpand
        self.collView.reloadSections([sender.tag])
    }
    
    func didSelectPrice(minPrice: CGFloat, maxPrice: CGFloat) {
        
        filterDT.isPriceApply = true
        
        
        
        
        filterDT.minPrice = minPrice
        filterDT.maxPrice = maxPrice
        
    }
}

extension NejreeCategoryFilterVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return arrFilter.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        let filter = arrFilter[section]
        let type = filter.type
        
        if type == .Price {
            
            return (filter.isExpand ? 1 : 0)
            
        } else if type == .Size {
            
            return (filter.isExpand ? (filter.filter_data?.count ?? 0) : 0)
            
        } else if type == .Brands {
            
            return (filter.isExpand ? (filter.filter_data?.count ?? 0) : 0)
            
        } else if type == .Types {
            
            return (filter.isExpand ? (filter.filter_data?.count ?? 0) : 0)
            
        } else if type == .Shop_1 {
            
            return (filter.isExpand ? (filter.filter_data?.count ?? 0) : 0)
            
        } else if type == .Shop_2 {
            
            return (filter.isExpand ? (filter.filter_data?.count ?? 0) : 0)
            
        } else if type == .Category_1 {
            
            return (filter.isExpand ? (filter.filter_data?.count ?? 0) : 0)
            
        } else if type == .Category_2 {
            
            return (filter.isExpand ? (filter.filter_data?.count ?? 0) : 0)
            
        } else if type == .Gender {
            
            return (filter.isExpand ? (filter.filter_data?.count ?? 0) : 0)
            
        } else if type == .Color {
            
            return (filter.isExpand ? (filter.filter_data?.count ?? 0) : 0)
            
        } else if type == .Categories {
            
            return (filter.isExpand ? (filter.filter_data?.count ?? 0) : 0)
            
        } else {
            
            return (filter.isExpand ? (filter.filter_data?.count ?? 0) : 0)
        }
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cellPrice = collectionView.dequeueReusableCell(withReuseIdentifier: "FilterPriceCell", for: indexPath) as! FilterPriceCell
        let cellSize = collectionView.dequeueReusableCell(withReuseIdentifier: "FilterSizeCell", for: indexPath) as! FilterSizeCell
        let cellColor = collectionView.dequeueReusableCell(withReuseIdentifier: "FilterColorCell", for: indexPath) as! FilterColorCell
        let cellBrand = collectionView.dequeueReusableCell(withReuseIdentifier: "FilterBrandCell", for: indexPath) as! FilterBrandCell
        let cellText = collectionView.dequeueReusableCell(withReuseIdentifier: "FilterTextCell", for: indexPath) as! FilterTextCell
        
        let filter = arrFilter[indexPath.section]
        let type = filter.type
        
        if type == .Price {
            
            if (filter.filter_data?.count ?? 0) > 0 {
                cellPrice.slider.minValue = CGFloat(Double(filter.filter_data![0].min_price ?? "0.0")!)
                cellPrice.slider.maxValue = CGFloat(Double(filter.filter_data![0].max_price ?? "0.0")!)
            } else {
                cellPrice.slider.minValue = self.filterDT.tempMinPrice
                cellPrice.slider.maxValue = self.filterDT.tempMaxPrice
            }
                        
            cellPrice.slider.selectedMinValue = filterDT.minPrice
            cellPrice.slider.selectedMaxValue = filterDT.maxPrice
            
            cellPrice.setSliderValue(minValue: filterDT.minPrice, maxValue: filterDT.maxPrice)

            cellPrice.delegateDidSelectPrice = self
            
            cellPrice.slider.layoutIfNeeded()
            cellPrice.slider.layoutSubviews()
            
            cellPrice.layoutIfNeeded()
            
            return cellPrice;
            
        } else if type == .Size {
            
            cellSize.lblTitle.text = filter.filter_data?[indexPath.row].filter_value ?? ""
            cellSize.lblTitle.font  = UIFont(name: "Cairo-Regular", size: 13)!
            cellSize.lblTitle.layer.borderColor = UIColor.black.cgColor
            cellSize.lblTitle.layer.borderWidth = 0.8
            cellSize.lblTitle.layer.cornerRadius = 2.5
            cellSize.lblTitle.clipsToBounds = true
            
            if filterDT.size.contains(indexPath.row) {
                cellSize.lblTitle.backgroundColor = UIColor.black
                cellSize.lblTitle.textColor = nejreeColor
            } else {
                cellSize.lblTitle.backgroundColor = UIColor.clear
                cellSize.lblTitle.textColor = UIColor.black
            }
            
            return cellSize;
            
        } else if type == .Brands {
            
            cellBrand.imgView.sd_setImage(with: URL(string: filter.filter_data?[indexPath.row].filter_img ?? ""), placeholderImage: nil)
            cellBrand.viewBorder.layer.borderWidth = 2.0
            cellBrand.viewBorder.clipsToBounds = true
            
            if filterDT.brand.contains(indexPath.row) {
                cellBrand.viewBorder.layer.borderColor = nejreeColor?.cgColor//UIColor.black.cgColor
            } else {
                cellBrand.viewBorder.layer.borderColor = UIColor.lightGray.cgColor
            }
            
            return cellBrand;
            
        } else if type == .Types {
            
            cellText.lblTitle.text = filter.filter_data?[indexPath.row].filter_value ?? ""
            //cellText.lblTitle.font  = UIFont(name: "Cairo-Regular", size: 13)!
            cellText.viewBorder.layer.borderColor = UIColor.lightGray.cgColor
            cellText.viewBorder.layer.borderWidth = 0.8
            cellText.viewBorder.layer.cornerRadius = 2
            cellText.viewBorder.clipsToBounds = true
            
            if filterDT.types.contains(indexPath.row) {
                cellText.viewBorder.backgroundColor = UIColor.black
                cellText.lblTitle.textColor = nejreeColor
            } else {
                cellText.viewBorder.backgroundColor = UIColor.clear
                cellText.lblTitle.textColor = UIColor.black
            }
            
            return cellText;
            
        } else if type == .Shop_1 {
            
            cellText.lblTitle.text = filter.filter_data?[indexPath.row].filter_value ?? ""
            //cellText.lblTitle.font  = UIFont(name: "Cairo-Regular", size: 13)!
            cellText.viewBorder.layer.borderColor = UIColor.lightGray.cgColor
            cellText.viewBorder.layer.borderWidth = 0.8
            cellText.viewBorder.layer.cornerRadius = 2
            cellText.viewBorder.clipsToBounds = true
            
            if filterDT.shop_1.contains(indexPath.row) {
                cellText.viewBorder.backgroundColor = UIColor.black
                cellText.lblTitle.textColor = nejreeColor
            } else {
                cellText.viewBorder.backgroundColor = UIColor.clear
                cellText.lblTitle.textColor = UIColor.black
            }
            
            return cellText;
            
        } else if type == .Shop_2 {
            
            cellText.lblTitle.text = filter.filter_data?[indexPath.row].filter_value ?? ""
            //cellText.lblTitle.font  = UIFont(name: "Cairo-Regular", size: 13)!
            cellText.viewBorder.layer.borderColor = UIColor.lightGray.cgColor
            cellText.viewBorder.layer.borderWidth = 0.8
            cellText.viewBorder.layer.cornerRadius = 2
            cellText.viewBorder.clipsToBounds = true
            
            if filterDT.shop_2.contains(indexPath.row) {
                cellText.viewBorder.backgroundColor = UIColor.black
                cellText.lblTitle.textColor = nejreeColor
            } else {
                cellText.viewBorder.backgroundColor = UIColor.clear
                cellText.lblTitle.textColor = UIColor.black
            }
            
            return cellText;
            
        } else if type == .Category_1 {
            
            cellText.lblTitle.text = filter.filter_data?[indexPath.row].filter_value ?? ""
            //cellText.lblTitle.font  = UIFont(name: "Cairo-Regular", size: 13)!
            cellText.viewBorder.layer.borderColor = UIColor.lightGray.cgColor
            cellText.viewBorder.layer.borderWidth = 0.8
            cellText.viewBorder.layer.cornerRadius = 2
            cellText.viewBorder.clipsToBounds = true
            
            if filterDT.category_1.contains(indexPath.row) {
                cellText.viewBorder.backgroundColor = UIColor.black
                cellText.lblTitle.textColor = nejreeColor
            } else {
                cellText.viewBorder.backgroundColor = UIColor.clear
                cellText.lblTitle.textColor = UIColor.black
            }
            
            return cellText;
            
        } else if type == .Category_2 {
            
            cellText.lblTitle.text = filter.filter_data?[indexPath.row].filter_value ?? ""
            //cellText.lblTitle.font  = UIFont(name: "Cairo-Regular", size: 13)!
            cellText.viewBorder.layer.borderColor = UIColor.lightGray.cgColor
            cellText.viewBorder.layer.borderWidth = 0.8
            cellText.viewBorder.layer.cornerRadius = 2
            cellText.viewBorder.clipsToBounds = true
            
            if filterDT.category_2.contains(indexPath.row) {
                cellText.viewBorder.backgroundColor = UIColor.black
                cellText.lblTitle.textColor = nejreeColor
            } else {
                cellText.viewBorder.backgroundColor = UIColor.clear
                cellText.lblTitle.textColor = UIColor.black
            }
            
            return cellText;
            
        } else if type == .Gender {
            
            cellText.lblTitle.text = filter.filter_data?[indexPath.row].filter_value ?? ""
            //cellText.lblTitle.font  = UIFont(name: "Cairo-Regular", size: 13)!
            cellText.viewBorder.layer.borderColor = UIColor.lightGray.cgColor
            cellText.viewBorder.layer.borderWidth = 0.8
            cellText.viewBorder.layer.cornerRadius = 2
            cellText.viewBorder.clipsToBounds = true
            
            if filterDT.gender.contains(indexPath.row) {
                cellText.viewBorder.backgroundColor = UIColor.black
                cellText.lblTitle.textColor = nejreeColor
            } else {
                cellText.viewBorder.backgroundColor = UIColor.clear
                cellText.lblTitle.textColor = UIColor.black
            }
            
            return cellText;
            
        } else if type == .Color {
            
            cellColor.lblTitle.text = ""
            cellColor.lblTitle.font  = UIFont(name: "Cairo-Regular", size: 13)!
            cellColor.lblTitle.layer.cornerRadius = 1
            cellColor.lblTitle.clipsToBounds = true
            let colHex = filter.filter_data?[indexPath.row].filter_value ?? ""
            if colHex != "" {
                cellColor.lblTitle.backgroundColor = UIColor.init(hex: colHex)
            } else {
                cellColor.lblTitle.backgroundColor = nejreeColor
            }
                        
            cellColor.viewBorder.layer.borderWidth = 2.0
            cellColor.viewBorder.layer.cornerRadius = 3
            cellColor.viewBorder.clipsToBounds = true
            //cellColor.backgroundColor = UIColor.black
            
            if filterDT.color.contains(indexPath.row) {
                cellColor.viewBorder.layer.borderColor = nejreeColor?.cgColor//UIColor.black.cgColor
            } else {
                cellColor.viewBorder.layer.borderColor = UIColor.lightGray.cgColor
            }
            
            return cellColor;
            
        } else if type == .Categories {
         
            cellText.lblTitle.text = filter.filter_data?[indexPath.row].filter_value ?? ""
            //cellText.lblTitle.font  = UIFont(name: "Cairo-Regular", size: 13)!
            cellText.viewBorder.layer.borderColor = UIColor.lightGray.cgColor
            cellText.viewBorder.layer.borderWidth = 0.8
            cellText.viewBorder.clipsToBounds = true
         
            if filterDT.categories.contains(indexPath.row) {
                cellText.viewBorder.backgroundColor = UIColor.black
                cellText.lblTitle.textColor = nejreeColor
            } else {
                cellText.viewBorder.backgroundColor = UIColor.clear
                cellText.lblTitle.textColor = UIColor.black
            }
            
            return cellText;
            
        } else {
            
            cellText.lblTitle.text = filter.filter_data?[indexPath.row].filter_value ?? ""
            //cellText.lblTitle.font  = UIFont(name: "Cairo-Regular", size: 13)!
            cellText.viewBorder.layer.borderColor = UIColor.lightGray.cgColor
            cellText.viewBorder.layer.borderWidth = 0.8
            cellText.viewBorder.clipsToBounds = true
            
            if let arrTempInt : [Int] = filterDT.other[arrFilter[indexPath.section].filter_type ?? ""] {
                    
                let arrInt = arrTempInt
                
                if arrInt.contains(indexPath.row) {
                    cellText.viewBorder.backgroundColor = UIColor.black
                    cellText.lblTitle.textColor = nejreeColor
                } else {
                    cellText.viewBorder.backgroundColor = UIColor.clear
                    cellText.lblTitle.textColor = UIColor.black
                }
                
            } else {
                cellText.viewBorder.backgroundColor = UIColor.clear
                cellText.lblTitle.textColor = UIColor.black
            }
            
            return cellText;
        }
        
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let type = arrFilter[indexPath.section].type
        
        if type == .Size {
                        
//            if filterDT.size == [indexPath.row] {
//                filterDT.size = []
//            } else {
//                filterDT.size = [indexPath.row]
//            }
            
            if filterDT.size.contains(indexPath.row) {
                
                let index = filterDT.size.firstIndex { (item) -> Bool in
                    return (item == indexPath.row)
                }
                
                if let tempIndex = index {
                    
                    filterDT.size.remove(at: tempIndex)
                }
                                
            } else {
                
                filterDT.size.append(indexPath.row)
            }
            
        } else if type == .Brands {
            
//            if filterDT.brand == [indexPath.row] {
//                filterDT.brand = []
//            } else {
//                filterDT.brand = [indexPath.row]
//            }
            
            if filterDT.brand.contains(indexPath.row) {
                
                let index = filterDT.brand.firstIndex { (item) -> Bool in
                    return (item == indexPath.row)
                }
                
                if let tempIndex = index {
                    
                    filterDT.brand.remove(at: tempIndex)
                }
                                
            } else {
                
                filterDT.brand.append(indexPath.row)
            }
            
        } else if type == .Types {
            
//            if filterDT.types == [indexPath.row] {
//                filterDT.types = []
//            } else {
//                filterDT.types = [indexPath.row]
//            }
            
            if filterDT.types.contains(indexPath.row) {
                
                let index = filterDT.types.firstIndex { (item) -> Bool in
                    return (item == indexPath.row)
                }
                
                if let tempIndex = index {
                    
                    filterDT.types.remove(at: tempIndex)
                }
                                
            } else {
                
                filterDT.types.append(indexPath.row)
            }
            
        } else if type == .Shop_1 {
            
            if filterDT.shop_1 == [indexPath.row] {
                filterDT.shop_1 = []
                self.filterDT.selected_level_1_code = ""
            } else {
                filterDT.shop_1 = [indexPath.row]
                self.filterDT.selected_level_1_code = arrFilter[indexPath.section].filter_data?[indexPath.row].filter_code ?? ""
            }
            
        } else if type == .Shop_2 {
            
            if filterDT.shop_2 == [indexPath.row] {
                filterDT.shop_2 = []
                self.filterDT.selected_level_2_code = ""
            } else {
                filterDT.shop_2 = [indexPath.row]
                self.filterDT.selected_level_2_code = arrFilter[indexPath.section].filter_data?[indexPath.row].filter_code ?? ""
            }
            
        } else if type == .Category_1 {
            
            if filterDT.category_1 == [indexPath.row] {
                filterDT.category_1 = []
                self.filterDT.selected_level_1_code = ""
            } else {
                filterDT.category_1 = [indexPath.row]
                self.filterDT.selected_level_1_code = arrFilter[indexPath.section].filter_data?[indexPath.row].filter_code ?? ""
            }
            
        } else if type == .Category_2 {
            
            if filterDT.category_2 == [indexPath.row] {
                filterDT.category_2 = []
                self.filterDT.selected_level_2_code = ""
            } else {
                filterDT.category_2 = [indexPath.row]
                self.filterDT.selected_level_2_code = arrFilter[indexPath.section].filter_data?[indexPath.row].filter_code ?? ""
            }
            
        } else if type == .Gender {
            
//            if filterDT.gender == [indexPath.row] {
//                filterDT.gender = []
//            } else {
//                filterDT.gender = [indexPath.row]
//            }
            
            if filterDT.gender.contains(indexPath.row) {
                
                let index = filterDT.gender.firstIndex { (item) -> Bool in
                    return (item == indexPath.row)
                }
                
                if let tempIndex = index {
                    
                    filterDT.gender.remove(at: tempIndex)
                }
                                
            } else {
                
                filterDT.gender.append(indexPath.row)
            }
            
        } else if type == .Color {
            
//            if filterDT.color == [indexPath.row] {
//                filterDT.color = []
//            } else {
//                filterDT.color = [indexPath.row]
//            }
            
            if filterDT.color.contains(indexPath.row) {
                
                let index = filterDT.color.firstIndex { (item) -> Bool in
                    return (item == indexPath.row)
                }
                
                if let tempIndex = index {
                    
                    filterDT.color.remove(at: tempIndex)
                }
                                
            } else {
                
                filterDT.color.append(indexPath.row)
            }
            
        } else if type == .Categories {
            
            if filterDT.categories == [indexPath.row] {
                filterDT.categories = []
            } else {
                filterDT.categories = [indexPath.row]
            }
            
        } else {
            
            if let arrTempInt : [Int] = filterDT.other[arrFilter[indexPath.section].filter_type ?? ""] {

                var arrInt = arrTempInt
                
                if arrInt.contains(indexPath.row) {
                    
                    let index = arrInt.firstIndex { (item) -> Bool in
                        return (item == indexPath.row)
                    }
                    
                    if let tempIndex = index {
                        
                        arrInt.remove(at: tempIndex)
                    }
                                    
                } else {
                    
                    arrInt.append(indexPath.row)
                }
                
                if arrInt.count == 0 {
                    
                    filterDT.other.removeValue(forKey: arrFilter[indexPath.section].filter_type ?? "")
                    
                } else {
                    
                    filterDT.other[arrFilter[indexPath.section].filter_type ?? ""] = arrInt
                }
                
                
            } else {
                
                filterDT.other[arrFilter[indexPath.section].filter_type ?? ""] = [indexPath.row]
            }
        }
        
        self.collView.reloadSections([indexPath.section])
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let type = arrFilter[indexPath.section].type
        
        if type == .Price {
            
            return CGSize(width: SCREEN_WIDTH - totalPading, height: 45)
            
        } else if type == .Size {
            
            return CGSize(width: (((collectionView.frame.size.width - totalPading) - (4.0 * 15.0)) / 5.0), height: 35)
            
        } else if type == .Brands {
            
            return CGSize(width: (((collectionView.frame.size.width - totalPading) - (3.0 * 15.0)) / 4.0), height: 50)
            
        } else if type == .Types {
            
            return CGSize(width: (((collectionView.frame.size.width - totalPading) - (2.0 * 15.0)) / 3.0), height: 40)
            
        } else if type == .Shop_1 {
            
            return CGSize(width: (((collectionView.frame.size.width - totalPading) - (2.0 * 15.0)) / 3.0), height: 40)
            
        } else if type == .Shop_2 {
            
            return CGSize(width: (((collectionView.frame.size.width - totalPading) - (2.0 * 15.0)) / 3.0), height: 40)
            
        } else if type == .Category_1 {
            
            return CGSize(width: (((collectionView.frame.size.width - totalPading) - (2.0 * 15.0)) / 3.0), height: 40)
            
        } else if type == .Category_2 {
            
            return CGSize(width: (((collectionView.frame.size.width - totalPading) - (2.0 * 15.0)) / 3.0), height: 40)
            
        } else if type == .Gender {
            
            return CGSize(width: (((collectionView.frame.size.width - totalPading) - (2.0 * 15.0)) / 3.0), height: 40)
            
        } else if type == .Color {
            
            return CGSize(width: 35, height: 35)
            
        } else if type == .Categories {
            
            return CGSize(width: (((collectionView.frame.size.width - totalPading) - (2.0 * 15.0)) / 3.0), height: 40)
            
        } else {
            
            return CGSize(width: (((collectionView.frame.size.width - totalPading) - (2.0 * 15.0)) / 3.0), height: 40)
        }
       
        return CGSize.zero
    }
 
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        let type = arrFilter[section].type
        
        if type == .Price {
            
            return 0
            
        } else if type == .Size {
            
            return 15
            
        } else if type == .Brands {
            
            return 15
            
        } else if type == .Types {
            
            return 15
            
        } else if type == .Shop_1 {
            
            return 15
            
        } else if type == .Shop_2 {
            
            return 15
            
        } else if type == .Category_1 {
            
            return 15
            
        } else if type == .Category_2 {
            
            return 15
            
        } else if type == .Gender {
            
            return 15
            
        } else if type == .Color {
            
            return 15
            
        } else if type == .Categories {
            
            return 15
            
        } else {
            
            return 15
        }
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        let type = arrFilter[section].type
        
        if type == .Price {
            
            return 0
            
        } else if type == .Size {
            
            return 15
            
        } else if type == .Brands {
            
            return 15
            
        } else if type == .Types {
            
            return 15
            
        } else if type == .Shop_1 {
            
            return 15
            
        } else if type == .Shop_2 {
            
            return 15
            
        } else if type == .Category_1 {
            
            return 15
            
        } else if type == .Category_2 {
            
            return 15
            
        } else if type == .Gender {
            
            return 15
            
        } else if type == .Color {
            
            return 15
            
        } else if type == .Categories {
            
            return 15
            
        } else {
            
            return 15
        }
        
        return 0
        
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let filter = arrFilter[indexPath.section]
        
        switch kind {
            
        case UICollectionView.elementKindSectionHeader:
            
            guard let headerView = self.collView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "FilterHeaderCell", for: indexPath) as? FilterHeaderCell
                else {
                    fatalError("Invalid view type")
            }
            
            headerView.lblTitle.text = (filter.att_label ?? "").uppercased()
            
            headerView.btnExpand.tag = indexPath.section
            headerView.btnExpand.addTarget(self, action: #selector(self.btnExpandAction(_:)), for: .touchUpInside)
            headerView.imgExpand.image = UIImage(named: filter.isExpand ? expand_up : expand_down)
            
            return headerView
            
        case UICollectionView.elementKindSectionFooter:
            
            guard let headerView = self.collView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "FilterFooterCell", for: indexPath) as? FilterFooterCell
                else {
                    fatalError("Invalid view type")
            }
            
            return headerView
                
        default:
            
            fatalError("Unexpected element kind")
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        
        return CGSize(width: SCREEN_WIDTH - totalPading, height: 45.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        
        let filter = arrFilter[section]
        return CGSize(width: SCREEN_WIDTH - totalPading, height: filter.isExpand ? 15 : 2)
    }
}


extension UIColor {
    
    public convenience init?(hex: String) {
        let r, g, b, a: CGFloat

        if hex.hasPrefix("#") {
            let start = hex.index(hex.startIndex, offsetBy: 1)
            let hexColor = String(hex[start...])

            if hexColor.count == 8 {
                let scanner = Scanner(string: hexColor)
                var hexNumber: UInt64 = 0

                if scanner.scanHexInt64(&hexNumber) {
                    r = CGFloat((hexNumber & 0xff000000) >> 24) / 255
                    g = CGFloat((hexNumber & 0x00ff0000) >> 16) / 255
                    b = CGFloat((hexNumber & 0x0000ff00) >> 8) / 255
                    a = CGFloat(hexNumber & 0x000000ff) / 255

                    self.init(red: r, green: g, blue: b, alpha: a)
                    return
                }
            } else if hexColor.count == 6 {
                
                let scanner = Scanner(string: hexColor)
                var hexNumber: UInt64 = 0

                if scanner.scanHexInt64(&hexNumber) {
                    
                    r = CGFloat((hexNumber & 0xFF0000) >> 16) / 255.0
                    g = CGFloat((hexNumber & 0x00FF00) >> 8) / 255.0
                    b = CGFloat(hexNumber & 0x0000FF) / 255.0

                    self.init(red: r, green: g, blue: b, alpha: 1)
                    return
                }
            }
        }

        return nil
    }
}
