//
//  FilterPriceCell.swift
//  MageNative Magento Platinum
//
//  Created by vishal.n on 31/07/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit
import RangeSeekSlider

class FilterPriceCell: UICollectionViewCell {

    @IBOutlet weak var slider: RangeSeekSlider!
    @IBOutlet weak var lblRange: UILabel!
    
    var delegateDidSelectPrice: DidSelectPrice?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        slider.delegate = self
        
        self.lblRange.font = UIFont(name: "Cairo-Regular", size: 14)!
       
        if APP_DEL.selectedLanguage == Arabic{
            lblRange.textAlignment = .right
        } else {
            lblRange.textAlignment = .left
        }
    }
    
    func setSliderValue(minValue: CGFloat, maxValue: CGFloat) {
        
        if APP_DEL.selectedLanguage == Arabic{
            self.lblRange.text = "\(Int(maxValue)) \(APP_DEL.selectedCountry.getCurrencySymbol().uppercased()) \(APP_LBL().to_without_colon) \(Int(minValue)) \(APP_DEL.selectedCountry.getCurrencySymbol().uppercased())"
        } else {
            self.lblRange.text = "\(Int(minValue)) \(APP_DEL.selectedCountry.getCurrencySymbol().uppercased()) \(APP_LBL().to_without_colon) \(Int(maxValue)) \(APP_DEL.selectedCountry.getCurrencySymbol().uppercased())"
        }
    }

}

extension FilterPriceCell: RangeSeekSliderDelegate {
    
    func rangeSeekSlider(_ slider: RangeSeekSlider, didChange minValue: CGFloat, maxValue: CGFloat) {
                
        self.delegateDidSelectPrice?.didSelectPrice(minPrice: minValue, maxPrice: maxValue)
        self.setSliderValue(minValue: minValue, maxValue: maxValue)
    }
}
