//
//  FilterHeaderCell.swift
//  MageNative Magento Platinum
//
//  Created by vishal.n on 31/07/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class FilterHeaderCell: UICollectionReusableView {

    
    @IBOutlet weak var viewDot: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgExpand: UIImageView!
    @IBOutlet weak var btnExpand: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        viewDot.round()
        lblTitle.font  = UIFont(name: "Cairo-Bold", size: 14)!
    }
    
}
