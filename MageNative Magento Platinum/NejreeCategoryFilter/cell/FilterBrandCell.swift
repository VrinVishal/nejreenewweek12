//
//  FilterBrandCell.swift
//  MageNative Magento Platinum
//
//  Created by vishal.n on 31/07/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class FilterBrandCell: UICollectionViewCell {

    @IBOutlet weak var viewBorder: UIView!
    @IBOutlet weak var imgView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
