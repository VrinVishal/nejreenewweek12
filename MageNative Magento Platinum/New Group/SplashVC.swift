//
//  SplashVC.swift
//  MageNative Magento Platinum
//
//  Created by vishal.n on 04/04/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class SplashVC: MagenativeUIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.getLabel()
    }
    
    func getLabel() {
        
        var postData = [String:String]()
        postData["language_id"] = ""
        postData["updated_datetime"] = APP_LANG.getLanguageInfo()
        let postString = ["parameters":postData].convtToJson() as String
        
        let baseURL = cedMage.getInfoPlist(fileName:"cedMage",indexString: "cedBaseUrl") as! String;
        //https://stagingapp.nejree.com/rest/V1/getlabel
        let url = baseURL + "getlabel"//"custom/label"
        var request = URLRequest(url: URL(string: url)!)
        request.httpMethod = "POST";
        request.httpBody = postString.data(using: String.Encoding.utf8);
        request = API().setHeader(request: request)
        
        print(url);
        print(postString);
        print(request.allHTTPHeaderFields ?? "--");
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            
            guard error == nil && data != nil else {
                print("error=\(String(describing: error))")
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse , httpStatus.statusCode != 200 {
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = ",response ?? "-")
                return
            }
            
            // code to fetch values from response :: start
            DispatchQueue.main.async {
                
                do {
                    var json = try JSON(data: data!);
                    json = json[0]
                    print(json)
                               
                     IS_FEATURED_ENABLED = (json["home_featured_product"].stringValue == "true")
                    
                    APP_DEL.isDiscountTagEnable = json["discount_tags"].stringValue
                    
                    APP_DEL.home_featured_category = json["home_featured_category"].stringValue.components(separatedBy: ",")
//                    APP_DEL.home_featured_category = json["home_category_banner"].stringValue.components(separatedBy: ",")
                    
                    print(json["updated_datetime"])
                    
                    let arrLabel = json["result"].arrayValue
                    
                    var arrLB : [LBL] = []
                    for tempLB in arrLabel {
                        let lb = LBL(key: tempLB["key"].stringValue, en: tempLB["en"].stringValue, ar: tempLB["ar"].stringValue)
                        
                        if(tempLB["key"].stringValue == "thanks_for_contacting_us"){
                            
                        }
                        
                        arrLB.append(lb)
                    }
                    
                    print("LBL COUNT:-",arrLB.count)
                    APP_LANG.manageLabelList(list: arrLB)
                    
                    APP_LANG.setLanguageID(updated_date: json["updated_datetime"].stringValue)
                    
                     let defaults = UserDefaults.standard
                    
                    if let isAppAlreadyLaunchedOnce = defaults.string(forKey: "isAppAlreadyLaunchedOnce"){
                        // app is launched already not first time
                       (UIApplication.shared.delegate as! AppDelegate).changeLanguage()
                        
                    }else{
                        // first Time launched
                        defaults.set(true, forKey: "isAppAlreadyLaunchedOnce")
//                        let lang=json[0]["locale"].stringValue
//                        let language=lang.components(separatedBy: "_")
//                        if language[0]=="ar"
//                        {
                            UserDefaults.standard.set(["ar","en","fr"], forKey: "AppleLanguages")
                            UIView.appearance().semanticContentAttribute = .forceRightToLeft
                            (UIApplication.shared.delegate as! AppDelegate).changeLanguage()
//                        }
//                        else if language[0]=="en"
//                        {
//                            UserDefaults.standard.set(["en","ar","fr"], forKey: "AppleLanguages")
//                            UIView.appearance().semanticContentAttribute = .forceLeftToRight
//                            (UIApplication.shared.delegate as! AppDelegate).changeLanguage()
//                        }
                    }
                    
                    
                     cedMage().getCartCount()
                    
                    if defaults.bool(forKey: "isLogin") {
                        
                        self.makeRequestToAPI("mobiconnect/customer/address/",dataToPost: [:]);
                    }
                    
                    
                } catch let error {
                    print(error.localizedDescription)
                }
            }
        }
        task.resume()
    }
    
    override func parseResponseReceivedFromAPI(_ jsonResponse: JSON) {
            
        let json = jsonResponse[0]
           
        if (json["data"]["address"].arrayValue.count > 0) {
            
            let address = json["data"]["address"].arrayValue[0]
            
            let addressRecord = addressData(street: address["address"].stringValue,
            city: address["city"].stringValue,
            region: address["region"].stringValue,
            country: address["country"].stringValue,
            lastname: address["lastname"].stringValue,
            firstname: address["firstname"].stringValue,
            phone: address["phone"].stringValue,
            pincode: address["pincode"].stringValue,
            address_id: address["address_id"].stringValue,
            region_id: address["region_id"].stringValue,
            neightbour_name: address["neighbour_name"].stringValue,
            selected: false)
            
            var tempAdd = [String:Any]()
            tempAdd["country_id"] = addressRecord.country ?? ""
            tempAdd["city"] = addressRecord.city ?? ""
            tempAdd["neighbour_name"] = addressRecord.neightbour_name ?? ""
            tempAdd["telephone"] = addressRecord.phone ?? ""
            tempAdd["firstname"] = addressRecord.firstname ?? ""
            tempAdd["lastname"] = addressRecord.lastname ?? ""
            tempAdd["entity_id"] = addressRecord.address_id ?? ""
            
            tempAddressObjApplePay = tempAdd
        }
    }
}
