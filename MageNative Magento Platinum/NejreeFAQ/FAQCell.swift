//
//  FAQCell.swift
//  MageNative Magento Platinum
//
//  Created by MacMini on 29/08/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class FAQCell: UITableViewCell {

   
    @IBOutlet weak var insideView: UIView!
    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var answerLabel: UILabel!
    @IBOutlet weak var arrowImage: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

  

}
