//
//  FAQController.swift
//  MageNative Magento Platinum
//
//  Created by MacMini on 29/08/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit
import WebKit
struct questionnAnswer {
    var question: String?
    var answer: String?
    var isExpanded: Bool?
}

class FAQController: MagenativeUIViewController,UIGestureRecognizerDelegate {

 
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var faqTbale: UITableView!
     @IBOutlet weak var webview: WKWebView!
      @IBOutlet weak var imgFAQ: UIImageView!
    
    
    var QnA = [questionnAnswer]()
//    var isExpanded = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        self.navigationItem.title = APP_LBL().faqs
        faqTbale.separatorStyle = .none
        
        self.getCMS()
        
        // Do any additional setup after loading the view.#imageLiteral(resourceName: "simulator_screenshot_36E34008-420D-477C-8E92-67E482058195.png")
        
        if APP_DEL.selectedLanguage == Arabic{
            
            self.btnBack.setImage(UIImage(named: "BackArrowNewArabic"), for: .normal)
            
        } else {
          
            self.btnBack.setImage(UIImage(named: "BackArrowNew"), for: .normal)
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func getCMS() {
          
          var postData = [String:String]()
          postData["language_id"] = UserDefaults.standard.value(forKey: "storeId") as? String
          
           postData["cms_id"] = "faqs"

          
          cedMageLoaders.removeLoadingIndicator(me: self);
          cedMageLoaders.addDefaultLoader(me: self);
          
          API().callAPI(endPoint: "mobiconnect/cms/getpage/", method: .POST, param: postData) { (json, err) in
              
              //DispatchQueue.main.async {
                  
                  cedMageLoaders.removeLoadingIndicator(me: self);
                  
                  if err == nil {
                      
                      let content = json[0]["data"]["result"][0]["content"].stringValue
                      let image = json[0]["data"]["result"][0]["image"].stringValue
                      
                      let fontName =  "Cairo-Regular"
                     let fontSize = 45
                     let fontSetting = "<span style=\"font-family: \(fontName);font-size: \(fontSize)\"</span>"
                     
                      self.webview.loadHTMLString( fontSetting + content, baseURL: nil)
                      
//
//                     // self.webview.loadHTMLString(content, baseURL: nil)
                      self.imgFAQ.sd_setImage(with: URL(string: image), placeholderImage: nil)
                  }
            //  }
          }
      }
    
    override func viewWillAppear(_ animated: Bool) {
           
           super.viewWillAppear(animated)
          
           self.navigationController?.setNavigationBarHidden(true, animated: true)
           
        if navigationController?.responds(to: #selector(getter: UINavigationController.interactivePopGestureRecognizer)) ?? false {
                   
                   self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
                   self.navigationController?.interactivePopGestureRecognizer?.delegate = self
               }

        
       }
       
       func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
              
              return true
          }
       
       override func viewWillDisappear(_ animated: Bool) {
           self.navigationController?.setNavigationBarHidden(true, animated: true)
       }
    
    
    func QnAforArabic() {
        let temp = questionnAnswer.init(question: "HOW TO PLACE AN ORDER AT NEJREE?".localized, answer: "Visit us on Nejree.com and select the items you like, select your preferred size and quantity and finally go to ‘Add to Shopping Bag’. \n \n Once you add to shopping bag there will be a pop-up, displaying your product and there are options “VIEW CART” and “CHECKOUT” If you wish to continue shopping, simply close the pop-up and continue with your shopping \n \n If you wish to view your products in the cart, simply click the 'VIEW CART' button and check your shoes. If you wish to buy it instantly, simply click the 'CHECK OUT' button and proceed as following. Choose between Register Account / Returning Customer. \n \n If you're registered customer, enter your Login details to proceed/nIf you’re not a registered customer, Click “Register Account” and enter your personal details and create an account for yourself to proceed Choose your shipping method/nChoose your payment method/nEnter if there any coupon / voucher/nFinally confirm the Order".localized, isExpanded: false)
        self.QnA.append(temp)
        let temp2 = questionnAnswer.init(question: "HOW TO REGISTER ACCOUNT?".localized, answer: "Click on the 'Register' button at the top right of the page on desktop and mobiles. Please complete all fields marked with * accurately. Click on continue. Congratulations! You now have an account and are ready to start shopping! \n \n It is very important for you to have an account and login in order to have full access at Nejree. For convenience and security reasons, we need to know your account details in order to allow you to shop with us.\n \n If you still do not have an account, please refer to the above question.".localized, isExpanded: false)
        self.QnA.append(temp2)
        
        let temp3 = questionnAnswer.init(question: "HOW TO CHANGE PASSWORD IN MY ACCOUNT?".localized, answer: "It’s simple, \n \n Go to My Account in our website Login with Email Address and current password \n \n Click on change password link Enter the new password twice and Click on the “CONTINUE” button to change your password  \n \n You will see a message Success: Your password has been successfully updated.".localized, isExpanded: false)
        self.QnA.append(temp3)
        
        let temp4 = questionnAnswer.init(question: "HOW CAN I UNSUBSCRIBE FROM NEJREE NEWSLETTERS?".localized, answer: "You can unsubscribe if you don't want regular updates on our new product launches, exclusive promotions & special offers.\n \n Simply sign in with your registered email id and visit the 'My Account' page Go to 'Newsletter Subscriptions' select 'No' Click 'CONTINUE' button            \n \n There you go, you will not receive Nejree’s newsletters anymore".localized, isExpanded: false)
        self.QnA.append(temp4)
        
        let temp5 = questionnAnswer.init(question: "DO YOU HAVE A SHOWROOM? WHERE IS YOUR STORE?".localized, answer: "We do not have a physical store as we are an online shopping website.\n \n We offer fast and free delivery to your home or office. If you do not like the product or it does not fit, we will pick it up for free if it is an exchange.\n \n Avoid the crowds at the malls and shop from your desk or home!".localized, isExpanded: false)
        self.QnA.append(temp5)
        
        let temp6 = questionnAnswer.init(question: "CAN I COME AND TRY ON THE ITEMS BEFORE I BUY THEM?".localized, answer: "Nejree is an online shopping website. All of our items are stored in a warehouse that is not open to customers.\n \n We offer a free exchange policy, if your item does not fit or you are unhappy with it, you can exchange this for a different size or Nejree Credit free of charge within 14 days of purchase.".localized, isExpanded: false)
        self.QnA.append(temp6)
        
        let temp7 = questionnAnswer.init(question: "WHY DO I HAVE TO VERIFY MY PHONE NUMBER?".localized, answer: "We request verification of your phone number for the security of the account and our customer support team will contact you to confirm the order that you made. \n \n If you provide a false mobile number and we couldn’t reach you, we’ll cancel your order without your concern. \n \n The courier will use this number to call or message you before delivery to ensure we deliver to the right person and on time.".localized, isExpanded: false)
        self.QnA.append(temp7)
        
        let temp8 = questionnAnswer.init(question: "CAN I ORDER ITEMS IN BULK TO RESELL THEM?".localized, answer: "We do not support bulk ordering. The reason is that customs authorities require additional paperwork for bulk shipments.\n \n We would love for you to order anything you like for yourself but we might have to block an order if we think you're ordering to resell.".localized, isExpanded: false)
        self.QnA.append(temp8)
        
        //        let temp9 = questionnAnswer.init(question: "CAN I ORDER ITEMS IN BULK TO RESELL THEM?", answer: "We do not support bulk ordering. The reason is that customs authorities require additional paperwork for bulk shipments. We would love for you to order anything you like for yourself but we might have to block an order if we think you're ordering to resell.", isExpanded: false)
        //        self.QnA.append(temp9)
        
        let temp10 = questionnAnswer.init(question: "HOW DO I USE A COUPON OR VOUCHER CODE?".localized, answer: "Once you’ve finished shopping and proceed to checkout, there will be a field provided for coupons and gift certificate codes. Type your code in here and click the validate button. The correct amount should be removed from your purchase total.".localized, isExpanded: false)
        self.QnA.append(temp10)
        
        let temp21 = questionnAnswer.init(question: "MY ORDER HAS BEEN SHIPPED, WHAT NEXT?".localized, answer: "This means that we have shipped the order from our end through one of our delivery partners. Our delivert partner will contact you once your shipment is out for the delivert. You would have also received an SMS with the tracking link, where you can check the status of your order.".localized, isExpanded: false)
        self.QnA.append(temp21)
        
        
        faqTbale.delegate = self
        faqTbale.dataSource = self
    }
    
    
    

    func getQnA() {
        
        let temp = questionnAnswer.init(question: "HOW TO PLACE AN ORDER AT NEJREE?".localized, answer: "Visit us on Nejree.com and select the items you like, select your preferred size and quantity and finally go to ‘Add to Shopping Bag’. \n \n Once you add to shopping bag there will be a pop-up, displaying your product and there are options “VIEW CART” and “CHECKOUT” If you wish to continue shopping, simply close the pop-up and continue with your shopping \n \n If you wish to view your products in the cart, simply click the 'VIEW CART' button and check your shoes. If you wish to buy it instantly, simply click the 'CHECK OUT' button and proceed as following. Choose between Register Account / Returning Customer. \n \n If you're registered customer, enter your Login details to proceed/nIf you’re not a registered customer, Click “Register Account” and enter your personal details and create an account for yourself to proceed Choose your shipping method/nChoose your payment method/nEnter if there any coupon / voucher/nFinally confirm the Order".localized, isExpanded: false)
        self.QnA.append(temp)
        let temp2 = questionnAnswer.init(question: "HOW TO REGISTER ACCOUNT?".localized, answer: "Click on the 'Register' button at the top right of the page on desktop and mobiles. Please complete all fields marked with * accurately. Click on continue. Congratulations! You now have an account and are ready to start shopping! \n \n It is very important for you to have an account and login in order to have full access at Nejree. For convenience and security reasons, we need to know your account details in order to allow you to shop with us.\n \n If you still do not have an account, please refer to the above question.".localized, isExpanded: false)
        self.QnA.append(temp2)
        
        let temp3 = questionnAnswer.init(question: "HOW TO CHANGE PASSWORD IN MY ACCOUNT?".localized, answer: "It’s simple, \n \n Go to My Account in our website Login with Email Address and current password \n \n Click on change password link Enter the new password twice and Click on the “CONTINUE” button to change your password  \n \n You will see a message Success: Your password has been successfully updated.".localized, isExpanded: false)
        self.QnA.append(temp3)
        
        let temp4 = questionnAnswer.init(question: "HOW CAN I UNSUBSCRIBE FROM NEJREE NEWSLETTERS?".localized, answer: "You can unsubscribe if you don't want regular updates on our new product launches, exclusive promotions & special offers.\n \n Simply sign in with your registered email id and visit the 'My Account' page Go to 'Newsletter Subscriptions' select 'No' Click 'CONTINUE' button            \n \n There you go, you will not receive Nejree’s newsletters anymore".localized, isExpanded: false)
        self.QnA.append(temp4)
        
        let temp5 = questionnAnswer.init(question: "DO YOU HAVE A SHOWROOM? WHERE IS YOUR STORE?".localized, answer: "We do not have a physical store as we are an online shopping website.\n \n We offer fast and free delivery to your home or office. If you do not like the product or it does not fit, we will pick it up for free if it is an exchange.\n \n Avoid the crowds at the malls and shop from your desk or home!".localized, isExpanded: false)
        self.QnA.append(temp5)
        
        let temp6 = questionnAnswer.init(question: "CAN I COME AND TRY ON THE ITEMS BEFORE I BUY THEM?".localized, answer: "Nejree is an online shopping website. All of our items are stored in a warehouse that is not open to customers.\n \n We offer a free exchange policy, if your item does not fit or you are unhappy with it, you can exchange this for a different size or Nejree Credit free of charge within 14 days of purchase.".localized, isExpanded: false)
        self.QnA.append(temp6)
        
        let temp7 = questionnAnswer.init(question: "WHY DO I HAVE TO VERIFY MY PHONE NUMBER?".localized, answer: "We request verification of your phone number for the security of the account and our customer support team will contact you to confirm the order that you made. \n \n If you provide a false mobile number and we couldn’t reach you, we’ll cancel your order without your concern. \n \n The courier will use this number to call or message you before delivery to ensure we deliver to the right person and on time.".localized, isExpanded: false)
        self.QnA.append(temp7)
        
        let temp8 = questionnAnswer.init(question: "CAN I ORDER ITEMS IN BULK TO RESELL THEM?".localized, answer: "We do not support bulk ordering. The reason is that customs authorities require additional paperwork for bulk shipments.\n \n We would love for you to order anything you like for yourself but we might have to block an order if we think you're ordering to resell.".localized, isExpanded: false)
        self.QnA.append(temp8)
        
        let temp10 = questionnAnswer.init(question: "HOW DO I USE A COUPON OR VOUCHER CODE?".localized, answer: "Once you’ve finished shopping and proceed to checkout, there will be a field provided for coupons and gift certificate codes. Type your code in here and click the validate button. The correct amount should be removed from your purchase total.".localized, isExpanded: false)
        self.QnA.append(temp10)
        

        let temp21 = questionnAnswer.init(question: "MY ORDER HAS BEEN SHIPPED, WHAT NEXT?", answer: "This means that we have shipped the order from our end through one of our delivery partners. Our delivert partner will contact you once your shipment is out for the delivert. You would have also received an SMS with the tracking link, where you can check the status of your order.", isExpanded: false)
        self.QnA.append(temp21)
        
        
        faqTbale.delegate = self
        faqTbale.dataSource = self
    }

    
}

extension FAQController: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return QnA.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FAQCell", for: indexPath) as! FAQCell
        cell.questionLabel.text = QnA[indexPath.row].question
        cell.questionLabel.numberOfLines = 0
        if QnA[indexPath.row].isExpanded! {cell.answerLabel.isHidden = false}
        else { cell.answerLabel.isHidden = true }
        
        if QnA[indexPath.row].isExpanded! {UIView.animate(withDuration: 1) {cell.arrowImage.transform=CGAffineTransform(rotationAngle: .pi)}
        }
        else {UIView.animate(withDuration: 1) {cell.arrowImage.transform = CGAffineTransform.identity}}
        
        cell.answerLabel.text = QnA[indexPath.row].answer
        cell.insideView.cardView()
        cell.selectionStyle = .none
        
        if APP_DEL.selectedLanguage == Arabic {
            cell.questionLabel.textAlignment = .right
            cell.answerLabel.textAlignment = .right
        } else {
            cell.questionLabel.textAlignment = .left
            cell.answerLabel.textAlignment = .left
        }
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if QnA[indexPath.row].isExpanded!{ return UITableView.automaticDimension }
        else { return 100 }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        QnA[indexPath.row].isExpanded = !QnA[indexPath.row].isExpanded!
        print(indexPath)
        let ip = IndexPath(row: indexPath.row, section: 0)
        tableView.reloadRows(at: [ip], with: .automatic)
        
    }
    
    
    
}
