//
//  NejreeGiftcardConfirmVC.swift
//  MageNative Magento Platinum
//
//  Created by vishal.n on 08/07/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

protocol SelectGiftcardOption {
    
    func selectGiftcardOption(confirm: Bool, name: String, phone: String, message: String)
}

class NejreeGiftcardConfirmVC: UIViewController {

    @IBOutlet weak var lblMessageCharCount: UILabel!
    @IBOutlet weak var imgGiftCard: UIImageView!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblPriceSAR: UILabel!
    
    @IBOutlet weak var viewName: UIView!
    @IBOutlet weak var txtNameField: UITextField!
    @IBOutlet weak var imgNameVerify: UIImageView!
    
    @IBOutlet weak var viewMobile: UIView!
    @IBOutlet weak var txtMobileField: UITextField!
    @IBOutlet weak var imgMobileVerify: UIImageView!
    @IBOutlet weak var lblPhoneCode: UILabel!

    
    @IBOutlet weak var viewMessage: UIView!
    @IBOutlet weak var textMessageField: UITextView!
    
    @IBOutlet weak var btnConfirm: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    
    
    let nejreeColor = UIColor.init(hexString: "#FBAD18")
    
    var delegateSelectGiftcardOption: SelectGiftcardOption?
    
    var name = ""
    var mobile = ""
    var message = ""
    var price = ""
    var imageURL = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lblPhoneCode.text = "+" + (APP_DEL.selectedCountry.phone_code ?? "966")

        setUI()
        
    }
    

    func setUI() {
        
        self.imgGiftCard.superview?.round(redius: 10.0)
        lblPrice.text = price
        self.imgGiftCard.round(redius: 15.0)
        
        let value = UserDefaults.standard.value(forKey: "AppleLanguages") as! [String]
        
        lblPriceSAR.text = APP_DEL.selectedCountry.getCurrencySymbol().uppercased()
        
        if value[0] == "ar" {
            imgGiftCard.sd_setImage(with: URL(string: imageURL), placeholderImage:UIImage(named: "gift_card_ar"))
        } else {
            imgGiftCard.sd_setImage(with: URL(string: imageURL), placeholderImage:UIImage(named: "gift_card_eng"))
        }
        
        //self.imgGiftCard.sd_setImage(with: URL(string: imageURL), placeholderImage: nil)
        
        txtNameField.attributedPlaceholder = NSAttributedString(string: APP_LBL().name.uppercased(),
        attributes: [NSAttributedString.Key.foregroundColor: UIColor.black])
        
        txtMobileField.attributedPlaceholder = NSAttributedString(string: APP_LBL().phone_number_star.uppercased(),
               attributes: [NSAttributedString.Key.foregroundColor: UIColor.black])
        textMessageField.text = APP_LBL().your_message.uppercased()
        
        txtNameField.font = UIFont(name: "Cairo-Regular", size: 14)!
        txtMobileField.font = UIFont(name: "Cairo-Regular", size: 14)!
        textMessageField.font = UIFont(name: "Cairo-Regular", size: 14)!
        textMessageField.textColor = UIColor.lightGray
        
//        imgNameVerify.image = UIImage(named: "")
//        imgMobileVerify.image = UIImage(named: "")
        
        viewName.setBorderBlack()
        viewMobile.setBorderBlack()
        viewMessage.setBorderBlack()
        
        viewName.setCornerRadius()
        viewMobile.setCornerRadius()
        viewMessage.setCornerRadius()
        
        txtNameField.delegate = self
        txtMobileField.delegate = self
        textMessageField.delegate = self
        
        
        if value[0] == "ar" {
            txtNameField.textAlignment = .right
            textMessageField.textAlignment = .right
        } else {
            txtNameField.textAlignment = .left
            textMessageField.textAlignment = .left
        }
        
        viewMobile.semanticContentAttribute = .forceLeftToRight
        txtMobileField.semanticContentAttribute = .forceLeftToRight
        txtMobileField.textAlignment = .left

        
        btnConfirm.setTitle(APP_LBL().confirm.uppercased(), for: .normal)
        btnConfirm.titleLabel?.font = UIFont(name: "Cairo-Regular", size: 17)!
       
        btnCancel.setTitle(APP_LBL().cancel.uppercased(), for: .normal)
        btnCancel.titleLabel?.font = UIFont(name: "Cairo-Regular", size: 17)!
        
         txtMobileField.keyboardType = .asciiCapableNumberPad
        
        
        self.txtNameField.text = self.name
        self.txtMobileField.text = self.mobile
        self.textMessageField.text = self.message
        
        lblMessageCharCount.text = "\(APP_DEL.SMSCharLimit - (self.message.count - 1))"
        txtMobileField.keyboardType = .asciiCapableNumberPad
        
        // lblMessageCharCount.text = String(APP_DEL.SMSCharLimit)
    }

    
    @IBAction func btnConfirmAction(_ sender: Any) {
        
        if txtNameField.text == "" {
            self.view.makeToast(APP_LBL().please_enter_first_name, duration: 1.0, position: .center)
            return
        }
        
        if txtNameField.text == "" {
            self.view.makeToast(APP_LBL().please_enter_last_name, duration: 1.0, position: .center)
            return
        }
        
        if txtMobileField.text == "" {
            self.view.makeToast(APP_LBL().please_enter_phone_no, duration: 1.0, position: .center)
            return;
        }
        
        if ((txtMobileField.text ?? "").count) != (Int(APP_DEL.selectedCountry.phone_number_limit ?? "") ?? 9) {
            
            self.view.makeToast(APP_LBL().mobile_number_should_be_of_xyz_digits_only, duration: 1.0, position: .center)
            return;
        }
        
        if (textMessageField.text == APP_LBL().your_message.uppercased()) || (textMessageField.text == "") {
            self.view.makeToast(APP_LBL().please_enter_your_message, duration: 1.0, position: .center)
            return;
        }
       
        self.dismiss(animated: true, completion: nil)
        self.delegateSelectGiftcardOption?.selectGiftcardOption(confirm: true, name: txtNameField.text ?? "", phone: txtMobileField.text ?? "", message: textMessageField.text)
    }
    
    @IBAction func btnCancelAction(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
        self.delegateSelectGiftcardOption?.selectGiftcardOption(confirm: false, name: "", phone: "", message: "")
    }
}

extension NejreeGiftcardConfirmVC: UITextViewDelegate {
    
    // Use this if you have a UITextView // ashish and JD change
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        // get the current text, or use an empty string if that failed
        let currentText = textView.text ?? ""

        // attempt to read the range they are trying to change, or exit if we can't
        guard let stringRange = Range(range, in: currentText) else { return false }

        // add their new text to the existing text
        let updatedText = currentText.replacingCharacters(in: stringRange, with: text)
        
        let tempCount = (updatedText.count + updatedText.emojis.count)

        if tempCount <= APP_DEL.SMSCharLimit {
            
            lblMessageCharCount.text = "\(((APP_DEL.SMSCharLimit - tempCount) <= 0) ? 0 : (APP_DEL.SMSCharLimit - tempCount))"
        }
        
        //lblMessageCharCount.text = "\(((APP_DEL.SMSCharLimit - tempCount) <= 0) ? 0 : (APP_DEL.SMSCharLimit - tempCount))"
        
        //(updatedText.count + updatedText.emojis.count)
        
        // make sure the result is under 16 characters
        return tempCount <= APP_DEL.SMSCharLimit
    }
}

extension NejreeGiftcardConfirmVC: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
                
        if (textField) == txtMobileField {
            
            guard !string.isEmpty else {

                return true
            }
            
            if (CharacterSet(charactersIn: "0123456789").isSuperset(of: CharacterSet(charactersIn: string))) {

                if let text = textField.text, let range = Range(range, in: text) {

                    let proposedText = text.replacingCharacters(in: range, with: string)

                    if (APP_DEL.selectedCountry.country_code?.uppercased() == "SA"){
                        if proposedText.first == "5" && proposedText.count <= (Int(APP_DEL.selectedCountry.phone_number_limit ?? "") ?? 9) {
                            return true
                        }
                    } else {
                        if proposedText.count <= (Int(APP_DEL.selectedCountry.phone_number_limit ?? "") ?? 9) {
                            return true
                        }
                    }
                }
            }

            return false
        } else if self.txtNameField == textField {
         
            if let x = string.rangeOfCharacter(from: NSCharacterSet.decimalDigits) {
               return false
            } else {
            return true
            }
        }
        
        return true
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if textView == self.textMessageField {
            
            if (textView.text == APP_LBL().your_message.uppercased()) {
                textView.text = ""
                textView.textColor = UIColor.black //optional
            }
            }
        
       }
    

    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        
        if textView == self.textMessageField {
             
             if (textView.text == APP_LBL().your_message.uppercased()) {
                         textView.text = ""
                         textView.textColor = UIColor.black //optional
             }
         
        }
        
        
        
        return true
    }
    
    
       
       func textViewDidEndEditing(_ textView: UITextView) {
         
        if textView == self.textMessageField {
             
             if (textView.text == "") {
                   textView.text = APP_LBL().your_message.uppercased()
                    textView.textColor = UIColor.lightGray
                }
                
                if textView.text.count <= 0 {
                    textView.text = APP_LBL().your_message.uppercased()
             
                     textView.textColor = UIColor.lightGray //optional

                }
         
        }
               
       }

}
