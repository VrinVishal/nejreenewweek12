//
//  NejreeGiftcardVC.swift
//  MageNative Magento Platinum
//
//  Created by vishal.n on 07/07/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit
import FirebaseAnalytics



struct GiftcarItem : Codable {
    
    let product_id : String?
    let status : String?
    let product_name : String?
    let type : String?
    let description : String?
    let source_id : String?
    let stock_status : String?
    let product_url : String?
    let product_image : String?
    let gift_price : String?
    
    let option_id : String?
    let attribute_id : String?
    let parent_id : String?
    
    
    
    
    

    enum CodingKeys: String, CodingKey {

        case product_id = "product_id"
        case status = "status"
        case product_name = "product_name"
        case type = "type"
        case description = "description"
        case source_id = "source_id"
        case stock_status = "stock_status"
        case product_url = "product-url"
        case product_image = "product_image"
        case gift_price = "gift_price"
        
        case option_id = "option_id"
        case attribute_id = "attribute_id"
        case parent_id = "parent_id"
        
        
        
    }
}

class NejreeGiftcardVC: UIViewController, SelectGiftcardOption, UIGestureRecognizerDelegate {

    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var lblPhoneCode: UILabel!

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblMessageCharCount: UILabel!
    
    @IBOutlet weak var lblTopText: UILabel!
    
    @IBOutlet weak var lblTitleChooseGiftCard: UILabel!
    @IBOutlet weak var lblTitleSendGiftCard: UILabel!
    
    @IBOutlet weak var viewDot_1_1: UIView!
    @IBOutlet weak var viewDot_1_2: UIView!
    @IBOutlet weak var viewDot_2_1: UIView!
    @IBOutlet weak var viewDot_2_2: UIView!
    @IBOutlet weak var viewDot_3_1: UIView!
    @IBOutlet weak var viewDot_3_2: UIView!
    
    @IBOutlet weak var carousel: iCarousel!
        
    @IBOutlet weak var viewEmail: UIView!
    @IBOutlet weak var txtEmailField: UITextField!
    @IBOutlet weak var imgEmailVerify: UIImageView!
    
    @IBOutlet weak var viewPassword: UIView!
    @IBOutlet weak var txtPasswordField: UITextField!
    @IBOutlet weak var imgPasswordVerify: UIImageView!
    
    @IBOutlet weak var btnForgotPasswordButton: UIButton!
    @IBOutlet weak var btnSigninButton: UIButton!
    
    @IBOutlet weak var imgTermsCondi: UIImageView!
    
    @IBOutlet weak var lblForgotPassword: UILabel!
    @IBOutlet weak var lblNewToNejree: UILabel!
    @IBOutlet weak var lblSignUp: UILabel!
    
    
    @IBOutlet weak var viewName: UIView!
    @IBOutlet weak var txtNameField: UITextField!
    @IBOutlet weak var imgNameVerify: UIImageView!
    
    @IBOutlet weak var viewMobile: UIView!
    @IBOutlet weak var txtMobileField: UITextField!
    @IBOutlet weak var imgMobileVerify: UIImageView!
    
    @IBOutlet weak var viewMessage: UIView!
    @IBOutlet weak var textMessageField: UITextView!
    
    @IBOutlet weak var lblAccept: UILabel!
    @IBOutlet weak var lblTermsAndCondition: UILabel!
    
    @IBOutlet weak var btnAddToCart: UIButton!
    @IBOutlet weak var btnApplePay: UIButton!
    
    @IBOutlet weak var viewAddedCard: UIView!
    @IBOutlet weak var constraintTopViewAddedControl: NSLayoutConstraint!
    @IBOutlet weak var lblAddedCart: UILabel!
    
    @IBOutlet weak var viewProductImg: UIView!
    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var lblGiftCardPrice: UILabel!
    @IBOutlet weak var lblGiftCardPriceSAR: UILabel!
    @IBOutlet weak var lblQty: UILabel!
    @IBOutlet weak var lblInfo: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var btnViewCart: UIButton!

    
    @IBOutlet weak var btnBack: UIButton!
    
    
    
    
    
    let nejreeColor = UIColor.init(hexString: "#FBAD18")
    
    
    var category_id = ""
    var giftCardID = ""
    var arrGiftcard: [GiftcarItem] = []
    
    var custNameOptionId = ""
    var msgOptionId = ""
    var mobileOptionId = ""
    
    var attributeID = String()
    var optionID = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = ""
        self.lblPhoneCode.text = "+" + (APP_DEL.selectedCountry.phone_code ?? "966")
        setUI()
        
        lblMessageCharCount.text = String(APP_DEL.SMSCharLimit)
        
        scrollView.isHidden = true
        getGiftCard()
        AddedCartPopupData()
        
        let value=UserDefaults.standard.value(forKey: "AppleLanguages") as! [String]
        if value[0] == "ar" {
          
            
            btnBack.setImage(UIImage(named: "icon_back_white_Arabic"), for: .normal)
            
        } else {
            
            
            btnBack.setImage(UIImage(named: "icon_back_white_round"), for: .normal)
            
        }
        
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
   
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        if navigationController?.responds(to: #selector(getter: UINavigationController.interactivePopGestureRecognizer)) ?? false {
            
            self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
            self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        }

    }
    
    func AddedCartPopupData(){
        viewAddedCard.cardView()
        constraintTopViewAddedControl.constant = -500
        viewProductImg.cardView()
        
        lblAddedCart.text = APP_LBL().added_to_cart.uppercased()
        btnViewCart.setTitle(APP_LBL().view_cart.uppercased(), for: .normal)
    }
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
           
           return true
       }

    @IBAction func btnBackAction(_ sender: Any) {
     
        self.navigationController?.popViewController(animated: true);
    }
    
    @IBAction func actionBtnViewCart(_ sender: UIButton) {
         self.tabBarController?.selectedIndex = 1
    }
    
    func setUI() {
        
        
        lblTitle.text = APP_LBL().gift_card
        lblTitle.font = UIFont(name: "Cairo-Regular", size: 20)!
        
        lblTopText.font = UIFont(name: "Cairo-Regular", size: 14)!
        lblTopText.text = APP_LBL().gift_heading_text
        
        lblTitleChooseGiftCard.font = UIFont(name: "Cairo-Regular", size: 18)!
        lblTitleSendGiftCard.font = UIFont(name: "Cairo-Regular", size: 18)!
        
        lblTitleChooseGiftCard.text = APP_LBL().choose_gift_card.uppercased()
        lblTitleSendGiftCard.text = APP_LBL().send_gift_card.uppercased()
        
        viewDot_1_1.round()
        viewDot_1_2.round()
        viewDot_2_1.round()
        viewDot_2_2.round()
        viewDot_3_1.round()
        viewDot_3_2.round()
        
        
        txtEmailField.attributedPlaceholder = NSAttributedString(string: APP_LBL().email_star.uppercased(),
        attributes: [NSAttributedString.Key.foregroundColor: UIColor.black])
        
        txtPasswordField.attributedPlaceholder = NSAttributedString(string: APP_LBL().password_star.uppercased(),
               attributes: [NSAttributedString.Key.foregroundColor: UIColor.black])
        
        txtEmailField.font = UIFont(name: "Cairo-Regular", size: 14)!
        txtPasswordField.font = UIFont(name: "Cairo-Regular", size: 14)!
        
        imgEmailVerify.image = UIImage(named: "")
        imgPasswordVerify.image = UIImage(named: "")
        
        viewEmail.setBorderBlack()
        viewPassword.setBorderBlack()
        
        viewEmail.setCornerRadius()
        viewPassword.setCornerRadius()
        
        txtEmailField.delegate = self
        txtPasswordField.delegate = self
        
        let value=UserDefaults.standard.value(forKey: "AppleLanguages") as! [String]
        if value[0] == "ar" {
            txtEmailField.textAlignment = .right
            txtPasswordField.textAlignment = .right
            
            lblProductName.textAlignment = .right
            lblQty.textAlignment = .right
            lblInfo.textAlignment = .right
            txtMobileField.textAlignment = .left
        } else {
            txtMobileField.textAlignment = .left

            txtEmailField.textAlignment = .left
            txtPasswordField.textAlignment = .left
            
            lblProductName.textAlignment = .left
            lblQty.textAlignment = .left
            lblInfo.textAlignment = .left
        }
        
        viewMobile.semanticContentAttribute = .forceLeftToRight
        txtMobileField.semanticContentAttribute = .forceLeftToRight

        
        lblForgotPassword.font = UIFont(name: "Cairo-Regular", size: 15)!
        lblNewToNejree.font = UIFont(name: "Cairo-Regular", size: 17)!
        lblSignUp.font = UIFont(name: "Cairo-Regular", size: 20)!
        
        lblForgotPassword.text = APP_LBL().forgot_password.uppercased()
        lblSignUp.text = APP_LBL().sign_up.uppercased()
        lblNewToNejree.text = APP_LBL().new_to_nejree_sign_up.uppercased()
        
        btnSigninButton.setTitle(APP_LBL().sign_in.uppercased(), for: .normal)
        btnSigninButton.setTitleColor(.black, for: .normal)
        btnSigninButton.setCornerRadius()
        btnSigninButton.setBorderBlack()
        btnSigninButton.titleLabel?.font = UIFont(name: "Cairo-Regular", size: 17)!
        
        
        
        txtNameField.attributedPlaceholder = NSAttributedString(string: APP_LBL().name.uppercased(),
        attributes: [NSAttributedString.Key.foregroundColor: UIColor.black])
        
        txtMobileField.attributedPlaceholder = NSAttributedString(string: APP_LBL().phone_number_star.uppercased(),
               attributes: [NSAttributedString.Key.foregroundColor: UIColor.black])
        textMessageField.text = APP_LBL().your_message.uppercased()
        textMessageField.textColor = UIColor.lightGray
        
        txtNameField.font = UIFont(name: "Cairo-Regular", size: 14)!
        txtMobileField.font = UIFont(name: "Cairo-Regular", size: 14)!
        textMessageField.font = UIFont(name: "Cairo-Regular", size: 14)!
        
       // txtMobileField.keyboardType = .asciiCapableNumberPad
       // txtMobileField.keyboardType = .asciiCapable
        txtMobileField.keyboardType = .asciiCapableNumberPad
        
        
        imgNameVerify.image = UIImage(named: "")
        imgMobileVerify.image = UIImage(named: "")
        
        viewName.setBorderBlack()
        viewMobile.setBorderBlack()
        viewMessage.setBorderBlack()
        
        viewName.setCornerRadius()
        viewMobile.setCornerRadius()
        viewMessage.setCornerRadius()
        
        txtNameField.delegate = self
        txtMobileField.delegate = self
        textMessageField.delegate = self
        
        
        if value[0] == "ar" {
            txtNameField.textAlignment = .right
            textMessageField.textAlignment = .right
        } else {
            txtNameField.textAlignment = .left
            textMessageField.textAlignment = .left
        }
        
        
        imgTermsCondi.image = UIImage(named: "icon_checkyellow_un")
        
        lblAccept.font = UIFont(name: "Cairo-Regular", size: 15)!
        lblAccept.text = APP_LBL().accept_terms_condition + " "
        
        lblTermsAndCondition.font = UIFont(name: "Cairo-Regular", size: 15)!
        lblTermsAndCondition.text = APP_LBL().terms_amp_condition.uppercased()
        
        btnAddToCart.setTitle(APP_LBL().add_to_cart.uppercased(), for: .normal)
        btnAddToCart.setTitleColor(.black, for: .normal)
        btnAddToCart.setCornerRadius()
        btnAddToCart.setBorderBlack()
        btnAddToCart.titleLabel?.font = UIFont(name: "Cairo-Regular", size: 17)!
        
        btnApplePay.setTitleColor(.black, for: .normal)
        btnApplePay.setCornerRadius()
        btnApplePay.setBorderBlack()
        btnApplePay.titleLabel?.font = UIFont(name: "Cairo-Regular", size: 17)!

        btnApplePay.isHidden = !(APP_DEL.isApplePayEnable == "1")
        
        if (UserDefaults.standard.bool(forKey: "isLogin")) {
            
            self.viewEmail.superview?.isHidden = true
            self.viewName.superview?.isHidden = false
            
        } else {
            
            self.viewEmail.superview?.isHidden = false
            self.viewName.superview?.isHidden = true
        }
        
        self.view.layoutIfNeeded()
        
        
    }
        
    
    func getGiftCard() {
        
        guard let storeId = UserDefaults.standard.value(forKey: "storeId") as? String else {return}
        
        var postData = [String:String]()
        postData["store_id"] = storeId
        if category_id == "0" || category_id == ""{
            postData["id"] = "93"//category_id //93
        }
        else
        {
            postData["id"] = category_id //93
        }
        postData["country_id"] = APP_DEL.selectedCountry.country_code ?? "SA"

        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);
        
        API().callAPI(endPoint: "mobiconnect/catalog/giftcard", method: .POST, param: postData) { (json_res, err) in
            
            DispatchQueue.main.async { [self] in
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                if err == nil {
                    
                    self.arrGiftcard.removeAll()
                    let catData = try! json_res[0]["result"].rawData()
                    self.arrGiftcard = try! JSONDecoder().decode([GiftcarItem].self, from: catData)
                    
                    self.custNameOptionId = json_res[0]["custNameOptionId"].stringValue
                    self.msgOptionId = json_res[0]["msgOptionId"].stringValue
                    self.mobileOptionId = json_res[0]["mobileOptionId"].stringValue
                    
                    
                    let filtered = self.arrGiftcard.filter { (gift) -> Bool in
                        return ((gift.type ?? "") != "configurable")
                    }
                    self.arrGiftcard = filtered
                    if self.giftCardID != ""{
                        let index_section = self.arrGiftcard.firstIndex { (item_section) -> Bool in
                           return self.giftCardID == item_section.product_id
                            //return (filt_cat!.type == item_section.type)
                        }
                        
                        if let temp = index_section{
                            self.selectedIndex = temp
                        }
                    }
                    
                    
                    self.manageCarousel()
                    
                    self.scrollView.isHidden = false
                }
            }
        }
    }
    
    
    var selectedIndex = 0
    func manageCarousel() {
        
        self.carousel.dataSource = self
        self.carousel.delegate = self
        
        self.carousel.type = iCarouselType.linear
        self.carousel.isPagingEnabled = true
        self.carousel.centerItemWhenSelected = true
        self.carousel.bounces = false
        self.carousel.currentItemIndex = selectedIndex
        self.carousel.decelerationRate = 0.6
        self.carousel.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.carousel.reloadData()
    }
    
    @IBAction func btnTermsCondiAction(_ sender: Any) {
        
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "termsMenuController") as! termsMenuController
        vc.isFromGiftCard = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    var isTermsSelected = false
    @IBAction func btnAcceptAction(_ sender: Any) {
        
        isTermsSelected = !isTermsSelected
        
        if isTermsSelected {
            imgTermsCondi.image = UIImage(named: "icon_checkyellow")
        } else {
            imgTermsCondi.image = UIImage(named: "icon_checkyellow_un")
        }
    }
    
    @IBAction func btnSigninAction(_ sender: Any) {
        
        var email = txtEmailField.text!
        email = email.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
        var password = txtPasswordField.text!
        password = password.trimmingCharacters(in:NSCharacterSet.whitespacesAndNewlines);
        
        
        self.view.endEditing(true)
        let validEmail = EmailVerifier.isValidEmail(testStr: email)
        
        
        if email == ""{
            cedMageHttpException.showAlertView(me: self, msg: APP_LBL().please_enter_email, title: APP_LBL().error)
            return;
        }
        else if !validEmail {
            cedMageHttpException.showAlertView(me: self, msg: APP_LBL().please_enter_valid_email, title: APP_LBL().error)
            return;
        }
        else if password == ""{
            cedMageHttpException.showAlertView(me: self, msg: APP_LBL().please_enter_password, title: APP_LBL().error)
            return;
        }
                
        if email == "" || password == "" {
            self.view.makeToast(APP_LBL().fields_should_not_be_left_empty, duration: 1.0, position: .center);
            return
            
        }
        
        customLoginAction()

    }
    
    func customLoginAction() {
        
        var email = txtEmailField.text!
        email = email.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
        var password = txtPasswordField.text!
        password = password.trimmingCharacters(in:NSCharacterSet.whitespacesAndNewlines);
        
        
        var postData = ["email":email,"password":password]
        
        if let cartID =  UserDefaults.standard.value(forKey: "cartId") as? String {
            postData["cart_id"] = cartID
        }
        
        if let storeID =  UserDefaults.standard.value(forKey: "storeId") as? String {
            postData["store_id"] = storeID
        }
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self)
        
        API().callAPI(endPoint: "mobiconnect/customer/login/", method: .POST, param: postData) { (json_res, err) in
            
            DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                if err == nil {
                    
                    let json = json_res[0]
                    
                    if json["data"]["customer"][0]["status"].stringValue == "success" {
                        
                        if json["data"]["customer"][0]["isConfirmationRequired"].stringValue.lowercased() == "YES".lowercased() {
                            
                            cedMageHttpException.showAlertView(me: self, msg: json["data"]["customer"][0]["message"].stringValue, title: "Error")
                            return
                        }
                        
                        let customer_id = json["data"]["customer"][0]["customer_id"].stringValue;
                        let hashKey = json["data"]["customer"][0]["hash"].stringValue;
                        let cart_summary = json["data"]["customer"][0]["cart_summary"].intValue;
                        let name = json["data"]["customer"][0]["name"].stringValue;
                        let mobile = json["data"]["customer"][0]["mobile_number"].stringValue
                        
                        let dict = ["email": email, "customerId": customer_id, "hashKey": hashKey];
                        
                        UserDefaults.standard.set(name, forKey: "name")
                        UserDefaults.standard.set(dict, forKey: "userInfoDict");
                        UserDefaults.standard.set(mobile, forKey: "mobile_number");
                        UserDefaults.standard.set(String(cart_summary), forKey: "items_count");
                        
                        UserDefaults.standard.set(email, forKey: "EmailId")
                        UserDefaults.standard.set(email, forKey: "EmailId")
                        
                       
                        
                        // checking for old user
                        if mobile == "" {
                            
                            let vc = UIStoryboard(name: "cedMageLogin", bundle: nil).instantiateViewController(withIdentifier: "oldUserController") as! oldUserController
                            vc.modalPresentationStyle = .fullScreen
                            self.present(vc, animated: true)
                            return
                        }
                        
                        let DataLogin = ["METHOD" : "email",
                                         "EMAIL" : email as Any,
                                         "PHONE_NUMBER" : mobile as Any] as [String : Any]
                        Analytics.logEvent(AnalyticsEventLogin, parameters: DataLogin)
                        
                        let detail : [AnalyticKey:String] = [
                            .Email : email,
                            .PhoneNumber : mobile,
                        ]
                        API().setEvent(eventName: .Login, eventDetail: detail) { (json, err) in }
                        
                        UserDefaults.standard.set(true, forKey: "isLogin")
                        
                         APP_DEL.getCartCount()
                        
                        APP_DEL.getGiftCardData { (res) in }
                        
                        APP_DEL.RegisterFirebaseTopic()
                        
                        NotificationCenter.default.post(name: NSNotification.Name("loadDrawerAgain"), object: nil)
                        self.view.makeToast(APP_LBL().login_successful, duration: 1.0, position: .center)
                        
                        self.setUI()

                    } else {
                        
                        cedMageHttpException.showAlertView(me: self, msg: APP_LBL().invalid_login_or_password, title: APP_LBL().error)
                    }
                    
                } else {
                    
                    cedMageHttpException.showAlertView(me: self, msg: APP_LBL().invalid_login_or_password, title: APP_LBL().error)
                }
            }
        }
    }
    
    
    var isForApplePay = false
    @IBAction func btnAddToCartAction(_ sender: Any) {
        
        let send = sender as? UIButton
        if send == self.btnAddToCart {
            isForApplePay = false
        } else if send == self.btnApplePay {
            isForApplePay = true
        }
//        txtNameField.text = txtNameField.text?.trimmingCharacters(in: .whitespaces)
//        textMessageField.text = textMessageField.text?.trimmingCharacters(in: .whitespaces)

        if txtNameField.text == "" {
            self.view.makeToast(APP_LBL().please_enter_first_name, duration: 1.0, position: .center)
            return
        }
        
        if txtNameField.text == "" {
            self.view.makeToast(APP_LBL().please_enter_last_name, duration: 1.0, position: .center)
            return
        }
        
        if txtMobileField.text == "" {
            self.view.makeToast(APP_LBL().please_enter_phone_no, duration: 1.0, position: .center)
            return;
        }
        
        if (((txtMobileField.text ?? "").count) != (Int(APP_DEL.selectedCountry.phone_number_limit ?? "") ?? 9)) {
            
            self.view.makeToast(APP_LBL().please_enter_valid_phone_no, duration: 1.0, position: .center)
            return;
        }
        
        if (textMessageField.text == APP_LBL().your_message.uppercased()) || (textMessageField.text == "") {
            self.view.makeToast(APP_LBL().please_enter_your_message, duration: 1.0, position: .center)
            return;
        }
        
        if isTermsSelected == false {
            
            self.view.makeToast(APP_LBL().please_select_terms_and_conditions, duration: 1.0, position: .center)
            return;
        }
        
        if self.arrGiftcard.count == 0 {
            
            self.view.makeToast(APP_LBL().gift_quantity_not_available, duration: 1.0, position: .center)
            return;
        }
        
        let vc = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "NejreeGiftcardConfirmVC") as! NejreeGiftcardConfirmVC
        vc.delegateSelectGiftcardOption = self
        vc.name = self.txtNameField.text ?? ""
        vc.mobile = self.txtMobileField.text ?? ""
        vc.message = self.textMessageField.text ?? ""
        vc.price = String(format: "%.2f", Double(self.arrGiftcard[self.carousel.currentItemIndex].gift_price ?? "0.0")!)
        vc.imageURL = self.arrGiftcard[self.carousel.currentItemIndex].product_image ?? ""
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        self.present(vc, animated: true, completion: nil)
    }
    
    func clearDetail() {
        
        self.txtNameField.text = ""
        self.txtMobileField.text = ""
        self.textMessageField.text = ""
        self.textViewDidEndEditing(textMessageField)
        self.isTermsSelected = true
        self.btnAcceptAction(UIButton())
    }
    
    func selectGiftcardOption(confirm: Bool, name: String, phone: String, message: String) {
        
        self.view.endEditing(true)
        
        if confirm == false {
            return;
        }
        
        let card = self.arrGiftcard[self.carousel.currentItemIndex]
        
        guard let storeId = UserDefaults.standard.value(forKey: "storeId") as? String else {return}
        
        var postData = [String:String]()
        postData["store_id"] = storeId
        
        postData["product_id"] = card.parent_id ?? ""
        postData["child_product_id"] = card.product_id ?? ""
        postData["type"] = "configurable"
        
        postData["qty"] = "1"
        postData["po_number"] = phone
        
        attributeID = card.attribute_id ?? ""
        optionID = card.option_id ?? ""
        
        var super_attribute = "{";
        super_attribute += "\""+attributeID+"\":";
        super_attribute += "\""+optionID+"\",";

        if (super_attribute.last! == ",") {

            super_attribute = super_attribute.substring(to: super_attribute.index(before: super_attribute.endIndex));
        }
        super_attribute += "}";
       
        postData["super_attribute"] = super_attribute
        
        postData["Custom"] = "{\"options\":{\"\(custNameOptionId)\":\"\(name)\",\"\(mobileOptionId)\":\"\(phone)\",\"\(msgOptionId)\":\"\(message.encodeEmoji)\"}}"
     
        if (UserDefaults.standard.object(forKey: "userInfoDict") != nil) {
            
            let userInfoDict = UserDefaults.standard.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
            postData["customer_id"] = userInfoDict["customerId"]!;
            postData["email"] = userInfoDict["email"]!;
        }
        
        postData["message"] = message.encodeEmoji
        postData["name"] = name
        postData["price"] = card.gift_price ?? ""
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);
        
        API().callAPI(endPoint: "mobiconnect/checkout/addtogift", method: .POST, param: postData) { (json_res, err) in
            
            DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                if err == nil {
                    
                    let value=UserDefaults.standard.value(forKey: "AppleLanguages") as! [String]
                    
                    if json_res[0]["code"].stringValue == "1" {
                        
                        APP_DEL.getGiftCardData { (res) in
                            print("getGiftCardData:- ", res)
                        }
                        
                        UserDefaults.standard.set(json_res[0]["items_count"].stringValue, forKey: "items_count");
                        
                        APP_DEL.getCartCount()
                        
                        self.lblGiftCardPriceSAR.text = APP_DEL.selectedCountry.getCurrencySymbol().uppercased()
                        if value[0] == "ar" {
                            self.imgProduct.sd_setImage(with: URL(string: card.product_image ?? ""), placeholderImage:UIImage(named: "gift_card_ar"))
                        } else {
                            self.imgProduct.sd_setImage(with: URL(string: card.product_image ?? ""), placeholderImage:UIImage(named: "gift_card_eng"))
                        }
                        
                        if (card.gift_price ?? "") != "" {
                            self.lblGiftCardPrice.text = String(format: "%.2f", Double(card.gift_price ?? "0.0")!)
                            self.lblAmount.text = self.lblGiftCardPrice.text
                        }
                        
                        self.lblProductName.text = card.product_name ?? ""
                        self.lblQty.text = APP_LBL().quantity_semicolon + " " + "1"
                        
                        var one = ""
                        
                        if value[0]=="ar" {
                            
                            one = (message)
                            one = " " + (phone) + " | " + one
                            one = (name) + " :" + APP_LBL().to + one
                            
                        } else {
                            
                            one = APP_LBL().to + ": " + (name)
                            one = one + " | " + (phone) + " "
                            one = one + (message)
                        }
                        
                        
                        self.lblInfo.text = one
                        
                        self.clearDetail()
                        
                        if self.isForApplePay {
                            
                            //self.getViewCart()
                            
                            APP_DEL.getGiftCardData { (res) in

                               self.getViewCart()
                            }
                            
                            
                        } else {
                            
                            UIView.animate(withDuration: Double(0.5), animations: {
                                self.constraintTopViewAddedControl.constant = 0
                                self.view.layoutIfNeeded()
                                
                                DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) { // Change `2.0`
                                    UIView.animate(withDuration: Double(0.5), animations: {
                                        self.constraintTopViewAddedControl.constant = -500
                                        
                                        self.view.layoutIfNeeded()
                                        
                                    })
                                }
                            })
                        }
                        
                    } else if json_res[0]["code"].stringValue == "-2" {
                        
                        let showTitle = APP_LBL().error
                        let showMsg = APP_LBL().gift_card_already_added
                        
                        let confirmationAlert = UIAlertController(title: showTitle, message: showMsg, preferredStyle: UIAlertController.Style.alert);
                        confirmationAlert.addAction(UIAlertAction(title: APP_LBL().ok.uppercased(), style: .default, handler: { (action: UIAlertAction!) in
                            
                        }));
                        
                        confirmationAlert.addAction(UIAlertAction(title: APP_LBL().go_to_cart, style: .default, handler: { (action: UIAlertAction!) in
                            self.tabBarController?.selectedIndex = 1;
                            self.navigationController?.popViewController(animated: true)
                            
                        }));
                        
                        self.present(confirmationAlert, animated: true, completion: nil)
                        
                    } else if json_res[0]["code"].stringValue == "-1" {
                        
                        cedMageHttpException.showAlertView(me: self, msg: APP_LBL().gift_quantity_not_available, title: APP_LBL().error)
                    }
                }
            }
        }
    }
    
    func getViewCart() {
        
        var postData = [String:String]()
        
        let userInfoDict = UserDefaults.standard.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
        postData["hashkey"] = userInfoDict["hashKey"]!
        postData["customer_id"] = userInfoDict["customerId"]!;
        postData["store_id"] = UserDefaults.standard.value(forKey: "storeId") as? String
        
        if let cart_id = UserDefaults.standard.object(forKey: "cartId") as? String {
            postData["cart_id"] = cart_id;
        }
        
        postData["country_id"] = APP_DEL.selectedCountry.country_code ?? "SA"

        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);
        
        API().callAPI(endPoint: "mobiconnect/checkout/viewcart", method: .POST, param: postData) { (json, err) in
            
            DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                if err == nil {
                    
                    print(json[0])
                    
                    let temp = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "NejreeAppleCheckoutVC") as! NejreeAppleCheckoutVC
                    temp.viewCartJson = json[0]
                    temp.isStoreCreditApplied = false
                    temp.appliedStoreCreditString = "\(APP_DEL.selectedCountry.getCurrencySymbol().uppercased()) 0"
                    temp.hidesBottomBarWhenPushed = true
                    self.navigationController?.pushViewController(temp, animated: true)
                }
            }
        }
    }
    
    @IBAction func btnForgotPasswordAction(_ sender: Any) {
        
        let vc = UIStoryboard(name: "cedMageLogin", bundle: nil).instantiateViewController(withIdentifier: "nejreeForgetController") as! nejreeForgetController
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true)
    }
    
    @IBAction func btnSignupAction(_ sender: Any) {
        
         if let vc = UIStoryboard(name: "cedMageLogin", bundle: nil).instantiateViewController(withIdentifier: "NejreeSignupVC") as? NejreeSignupVC {
       
                        APP_DEL.isLoginFromWishList = false
            APP_DEL.strSignUpfromGiftcard = category_id;
                        vc.hidesBottomBarWhenPushed = true
                        self.navigationController?.pushViewController(vc, animated: true)
                        
        }
        
//        let vc = UIStoryboard(name: "cedMageLogin", bundle: nil).instantiateViewController(withIdentifier: "NejreeSignupVC") as! NejreeSignupVC
//        vc.hidesBottomBarWhenPushed = true
//        self.present(vc, animated: true)
    }
    
}

extension NejreeGiftcardVC: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
                
        if (textField) == txtMobileField {
            
            guard !string.isEmpty else {

                return true
            }
            
            if (CharacterSet(charactersIn: "0123456789").isSuperset(of: CharacterSet(charactersIn: string))) {

                if let text = textField.text, let range = Range(range, in: text) {

                    let proposedText = text.replacingCharacters(in: range, with: string)

                    if (APP_DEL.selectedCountry.country_code?.uppercased() == "SA"){
                        if proposedText.first == "5" && proposedText.count <= (Int(APP_DEL.selectedCountry.phone_number_limit ?? "") ?? 9) {
                            return true
                        }
                    } else {
                        if proposedText.count <= (Int(APP_DEL.selectedCountry.phone_number_limit ?? "") ?? 9) {
                            return true
                        }
                    }
                }
            }

            return false
        } else if self.txtNameField == textField {
         
            if let x = string.rangeOfCharacter(from: NSCharacterSet.decimalDigits) {
               return false
            } else {
            return true
            }
        }
        
        return true
    }
    
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        switch textField {
        case txtEmailField:
            if isValidEmail(email: txtEmailField.text!) {
                
                txtEmailField.textColor = UIColor.black
                imgEmailVerify.image = UIImage(named: "iconCorrectCheckMark")
            } else {
                
                txtEmailField.textColor = nejreeColor
                imgEmailVerify.image = UIImage(named: "IconIncorrectCheck")
            }
            
        case txtPasswordField:
            if txtPasswordField.text!.count >= 6 {
                
                txtPasswordField.textColor = UIColor.black
                imgPasswordVerify.image = UIImage(named: "iconCorrectCheckMark")
            } else {
                
                txtPasswordField.textColor = nejreeColor
                imgPasswordVerify.image = UIImage(named: "IconIncorrectCheck")
            }
            
        case txtNameField:
            if txtNameField.hasText {
                
                txtNameField.textColor = UIColor.black
                imgNameVerify.image = UIImage(named: "iconCorrectCheckMark")
            } else {
                
                txtNameField.textColor = nejreeColor
                imgNameVerify.image = UIImage(named: "IconIncorrectCheck")
            }
            
        case txtMobileField:
            if txtMobileField.text!.count == (Int(APP_DEL.selectedCountry.phone_number_limit ?? "") ?? 9) {
                
                txtMobileField.textColor = UIColor.black
                imgMobileVerify.image = UIImage(named: "iconCorrectCheckMark")
            } else {
                
                txtMobileField.textColor = nejreeColor
                imgMobileVerify.image = UIImage(named: "IconIncorrectCheck")
            }
            
        default:
            
            break;
        }
        
        
        
        if txtEmailField.text! != "" && txtPasswordField.text! != "" {
            btnSigninButton.setBorderBlack()
            btnSigninButton.setTitleColor(.black, for: .normal)
        } else {
            btnSigninButton.layer.borderColor = UIColor.lightGray.cgColor
            btnSigninButton.setTitleColor(.lightGray, for: .normal)
        }
        
        
        if txtNameField.text! != "" && txtMobileField.text!.count == (Int(APP_DEL.selectedCountry.phone_number_limit ?? "") ?? 9) {
            btnAddToCart.setBorderBlack()
            btnAddToCart.setTitleColor(.black, for: .normal)
        } else {
            btnAddToCart.layer.borderColor = UIColor.lightGray.cgColor
            btnAddToCart.setTitleColor(.lightGray, for: .normal)
        }
        
    }
    
    
}

extension NejreeGiftcardVC: UITextViewDelegate {
    
    // Use this if you have a UITextView // ashish and JD change
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        // get the current text, or use an empty string if that failed
        let currentText = textView.text ?? ""

        // attempt to read the range they are trying to change, or exit if we can't
        guard let stringRange = Range(range, in: currentText) else { return false }

        // add their new text to the existing text
        let updatedText = currentText.replacingCharacters(in: stringRange, with: text)
        
        let tempCount = (updatedText.count + updatedText.emojis.count)

        if tempCount <= APP_DEL.SMSCharLimit {
            
            lblMessageCharCount.text = "\(((APP_DEL.SMSCharLimit - tempCount) <= 0) ? 0 : (APP_DEL.SMSCharLimit - tempCount))"
        }
        
        //lblMessageCharCount.text = "\(((APP_DEL.SMSCharLimit - tempCount) <= 0) ? 0 : (APP_DEL.SMSCharLimit - tempCount))"
        
        //(updatedText.count + updatedText.emojis.count)
        
        // make sure the result is under 16 characters
        return tempCount <= APP_DEL.SMSCharLimit
    }
    
//    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
//
//        if textView == self.textMessageField {
//
////            if let currtext = textView.text, let range = Range(range, in: currtext) {
////
////                let proposedText = currtext.replacingCharacters(in: range, with: text)
////                if proposedText.count < APP_DEL.SMSCharLimit {
////                    return true
////                } else {
////                    return false
////                }
////            }
//
////            if textView.text.count < APP_DEL.SMSCharLimit{
////                if (text == ""){
////                    if(textView.text.count == 0){
////                        lblMessageCharCount.text = "\(APP_DEL.SMSCharLimit)"
////                    } else {
////                        lblMessageCharCount.text = "\(APP_DEL.SMSCharLimit - (textView.text.count - 1))"
////                    }
////                } else {
////                    lblMessageCharCount.text = "\(APP_DEL.SMSCharLimit - (textView.text.count + 1))"
////                }
////                return true
////            } else if textView.text.count == APP_DEL.SMSCharLimit && text != "" {
////                lblMessageCharCount.text = "0"
////                return false
////            } else if (textView.text.count == APP_DEL.SMSCharLimit && text == "" ) {
////                lblMessageCharCount.text = "\(APP_DEL.SMSCharLimit - (textView.text.count - 1))"
////                return true
////            } else {
////                lblMessageCharCount.text = "\(APP_DEL.SMSCharLimit - (textView.text.count - 1))"
////                return false
////            }
//            lblMessageCharCount.text = "\(APP_DEL.SMSCharLimit - (textView.text.count - 1))"
//             return true
//        }
//
//        return false
//    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if textView == self.textMessageField {
            
            if (textView.text == APP_LBL().your_message.uppercased()) {
                textView.text = ""
                textView.textColor = UIColor.black //optional
            }
            }
        
       }
    

    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        
        if textView == self.textMessageField {
             
             if (textView.text == APP_LBL().your_message.uppercased()) {
                         textView.text = ""
                         textView.textColor = UIColor.black //optional
             }
         
        }
        
        
        
        return true
    }
    
    
       
       func textViewDidEndEditing(_ textView: UITextView) {
         
        if textView == self.textMessageField {
             
             if (textView.text == "") {
                   textView.text = APP_LBL().your_message.uppercased()
                    textView.textColor = UIColor.lightGray
                }
                
                if textView.text.count <= 0 {
                    textView.text = APP_LBL().your_message.uppercased()
             
                     textView.textColor = UIColor.lightGray //optional

                }
         
        }
               
       }
}

extension NejreeGiftcardVC: iCarouselDelegate, iCarouselDataSource {
    
    func numberOfItems(in carousel: iCarousel) -> Int {
        return self.arrGiftcard.count
    }
    
    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView {
        
        let w = (self.carousel.frame.size.width - 80.0)
        let h = self.carousel.frame.size.height
        let carView = UIView(frame: CGRect(x: 0, y: 0, width: w, height: h))
        carView.layer.cornerRadius = 20.0
        carView.clipsToBounds = true
        
        let vi = Bundle.main.loadNibNamed("CarouselView", owner: self, options: nil)?.first as! CarouselView
        vi.frame = carView.bounds
        
        let value = UserDefaults.standard.value(forKey: "AppleLanguages") as! [String]
        if value[0] == "ar" {
            vi.imgSel.sd_setImage(with: URL(string: self.arrGiftcard[index].product_image ?? ""), placeholderImage: UIImage(named: "gift_card_ar"))
        } else {
            vi.imgSel.sd_setImage(with: URL(string: self.arrGiftcard[index].product_image ?? ""), placeholderImage: UIImage(named: "gift_card_eng"))
        }
      
        
        if (self.arrGiftcard[index].gift_price ?? "") != "" {
            vi.lblPrice.text = String(format: "%.2f", Double(self.arrGiftcard[index].gift_price ?? "0.0")!)
        }
        
        vi.lblPriceSAR.text = APP_DEL.selectedCountry.getCurrencySymbol().uppercased()
        vi.layoutSubviews()
        vi.layoutIfNeeded()
        carView.addSubview(vi)
        
        return carView
    }
    
    func carousel(_ carousel: iCarousel, valueFor option: iCarouselOption, withDefault value: CGFloat) -> CGFloat {
        
        switch (option) {
            
            case .spacing:
                return (1.0 + (20.0 / carousel.itemWidth))
            
            default:
                return value
        }
    }
}

extension String {
    
    var encodeEmoji: String {
        
        if let encodeStr = NSString(cString: self.cString(using: .nonLossyASCII)!, encoding: String.Encoding.utf8.rawValue) {
            
            return encodeStr as String
        }
        
        return self
    }
    
    var decodeEmoji: String {
        
        let data = self.data(using: String.Encoding.utf8);
        let decodedStr = NSString(data: data!, encoding: String.Encoding.nonLossyASCII.rawValue)
        
        if let str = decodedStr {
            
            return str as String
        }
        return self
    }
}

extension Character {
    
    var isSimpleEmoji: Bool {
        
        guard let firstScalar = unicodeScalars.first else {
            return false
        }
        
        return firstScalar.properties.isEmoji && firstScalar.value > 0x238C
    }
    
    var isCombinedIntoEmoji: Bool {
        unicodeScalars.count > 1 && unicodeScalars.first?.properties.isEmoji ?? false
    }
    
    var isEmoji: Bool { isSimpleEmoji || isCombinedIntoEmoji }
}

extension String {
    
    var isSingleEmoji: Bool {
        return count == 1 && containsEmoji
    }
    
    var containsEmoji: Bool {
        return contains { $0.isEmoji }
    }
    
    var containsOnlyEmoji: Bool {
        return !isEmpty && !contains { !$0.isEmoji }
    }
    
    var emojiString: String {
        return emojis.map { String($0) }.reduce("", +)
    }
    
    var emojis: [Character] {
        return filter { $0.isEmoji }
    }
    
    var emojiScalars: [UnicodeScalar] {
        return filter { $0.isEmoji }.flatMap { $0.unicodeScalars }
    }
}
