//
//  RemoveProductPopupVC.swift
//  MageNative Magento Platinum
//
//  Created by vishal.n on 11/09/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

protocol RemoveCartProduct {
    func removeCartProduct(res: Bool)
}

class RemoveProductPopupVC: UIViewController {

    @IBOutlet weak var btnBG: UIButton!
    
    @IBOutlet weak var viewPopup: UIView!
    
    @IBOutlet weak var lblMessage: UILabel!
    
    @IBOutlet weak var btnRemove: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    
    var isFromDeleteAddress : Bool = false
    
    var delagateRemoveCartProduct: RemoveCartProduct?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.viewPopup.round(redius: 10.0)
        self.btnRemove.round(redius: 0.0)
        self.btnCancel.round(redius: 0.0)

        self.lblMessage.text = APP_LBL().are_you_sure_you_want_to_delete_this
        if isFromDeleteAddress{
            self.lblMessage.text = APP_LBL().are_you_sure_you_want_to_delete_this_address
        }
        
        self.lblMessage.font = UIFont(name: "Cairo-Regular", size: 15)!
        self.btnRemove.setTitle(APP_LBL().remove.uppercased(), for: .normal)
        self.btnRemove.titleLabel?.font = UIFont(name: "Cairo-Regular", size: 16)!
        self.btnCancel.setTitle(APP_LBL().cancel.uppercased(), for: .normal)
        self.btnCancel.titleLabel?.font = UIFont(name: "Cairo-Regular", size: 16)!
    }
    

    @IBAction func btnBGAction(_ sender: UIButton) {
        
        self.dismiss(animated: true, completion: nil)
        self.delagateRemoveCartProduct?.removeCartProduct(res: false)
    }
    
    @IBAction func btnCancelAction(_ sender: UIButton) {
        
        self.dismiss(animated: true, completion: nil)
        self.delagateRemoveCartProduct?.removeCartProduct(res: false)
    }
    
    @IBAction func btnRemoveAction(_ sender: UIButton) {
        
        self.dismiss(animated: true, completion: nil)
        self.delagateRemoveCartProduct?.removeCartProduct(res: true)
    }

}
