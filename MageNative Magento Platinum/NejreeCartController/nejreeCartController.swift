//
//  nejreeCartController.swift
//  MageNative Magento Platinum
//
//  Created by cedcoss on 20/10/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit
import FirebaseAnalytics
//import FFPopup
import Foundation


struct GiftItem : Codable {
    
    let name : String?
    let email : String?
    let phone : String?
    let product_id : String?
    let child_product_id : String?
    let description : String?
    let redemecode : String?
    let redemestatus : String?
    
    let name_option_attribute_id : String?
    let phone_option_attribute_id : String?
    let description_option_attribute_id : String?
    
    enum CodingKeys: String, CodingKey {
        
        case name = "name"
        case email = "email"
        case phone = "phone"
        case product_id = "product_id"
        case child_product_id = "child_product_id"
        case description = "description"
        case redemecode = "redemecode"
        case redemestatus = "redemestatus"
        
        case name_option_attribute_id = "option_0"
        case phone_option_attribute_id = "option_1"
        case description_option_attribute_id = "option_2"
    }
}

struct CartProduct : Codable {
    
    let product_id : String?
    let item_id : String?
    let product_name : String?
    let stock_status : Bool?
    let product_image : String?
    var quantity : Int?
    let sub_total : String?
    let product_type : String?
    var options_selected : [Options_selected]?
    let bundle_options : [String]?
    let review : Int?
    let review_count : Int?
    var is_wishlist : String?
    var child_product_id : String?
    let item_error : [ItemError]?
    let options_size : [Options_selected]?
    var regular_price : String?
    var special_price : String?
    var subtotal_price_without_currency : Double?
    var unit_price_without_currency : Double?
    var total_regular_price : String?
    var total_special_price : String?
    
    enum CodingKeys: String, CodingKey {
        
        case product_id = "product_id"
        case child_product_id = "child_product_id"
        case item_id = "item_id"
        case product_name = "product-name"
        case stock_status = "stock_status"
        case product_image = "product_image"
        case quantity = "quantity"
        case sub_total = "sub-total"
        case product_type = "product_type"
        case options_selected = "options_selected"
        case bundle_options = "bundle_options"
        case review = "review"
        case review_count = "review_count"
        case is_wishlist = "is_wishlist"
        case item_error = "item_error"
        case options_size = "options_size"
        case regular_price = "regular_price"
        case special_price = "special_price"
        case subtotal_price_without_currency = "subtotal_price_without_currency"
        case unit_price_without_currency = "unit_price_without_currency"
        case total_regular_price = "total_regular_price"
        case total_special_price = "total_special_price"
    }
    
    init(from decoder: Decoder) throws {
        
        let values = try decoder.container(keyedBy: CodingKeys.self)
        product_id = try values.decodeIfPresent(String.self, forKey: .product_id)
        
        child_product_id = try values.decodeIfPresent(String.self, forKey: .child_product_id)
        
        item_id = try values.decodeIfPresent(String.self, forKey: .item_id)
        product_name = try values.decodeIfPresent(String.self, forKey: .product_name)
        stock_status = try values.decodeIfPresent(Bool.self, forKey: .stock_status)
        product_image = try values.decodeIfPresent(String.self, forKey: .product_image)
        
        do {
            if let temp = try values.decodeIfPresent(Int.self, forKey: .quantity) {
                quantity = temp
            } else {
                quantity = 1
            }
        } catch {
            quantity = 1
        }
        
        sub_total = try values.decodeIfPresent(String.self, forKey: .sub_total)
        product_type = try values.decodeIfPresent(String.self, forKey: .product_type)
        do { options_selected = try values.decodeIfPresent([Options_selected].self, forKey: .options_selected) } catch { options_selected = nil }
        bundle_options = try values.decodeIfPresent([String].self, forKey: .bundle_options)
        review = try values.decodeIfPresent(Int.self, forKey: .review)
        review_count = try values.decodeIfPresent(Int.self, forKey: .review_count)
        is_wishlist = try values.decodeIfPresent(String.self, forKey: .is_wishlist)
        item_error = try values.decodeIfPresent([ItemError].self, forKey: .item_error)
        
        do { options_size = try values.decodeIfPresent([Options_selected].self, forKey: .options_size) } catch { options_size = nil }
        regular_price = try values.decodeIfPresent(String.self, forKey: .regular_price)
        special_price = try values.decodeIfPresent(String.self, forKey: .special_price)
        subtotal_price_without_currency = try values.decodeIfPresent(Double.self, forKey: .subtotal_price_without_currency)
        unit_price_without_currency = try values.decodeIfPresent(Double.self, forKey: .unit_price_without_currency)
        total_regular_price = try values.decodeIfPresent(String.self, forKey: .total_regular_price)
        total_special_price = try values.decodeIfPresent(String.self, forKey: .total_special_price)

    }
}

struct ItemError : Codable {
    
    let type : String?
    let text : String?
    
    
    

    enum CodingKeys: String, CodingKey {

        case type = "type"
        case text = "text"
        
    }
}

struct Options_selected : Codable {
    
    let label : String?
    let value : String?
    let option_id : Int?
    let option_value : String?
    var max_qty : String? = "0"

    enum CodingKeys: String, CodingKey {

        case label = "label"
        case value = "value"
        case option_id = "option_id"
        case option_value = "option_value"
        case max_qty = "max_qty"
    }
}


class nejreeCartController: MagenativeUIViewController, RemoveCartProduct, SelectGiftcardOption {
   
    // EMPTY CART
    @IBOutlet weak var viewEmptyCart: UIView!
    @IBOutlet weak var lblYourBasket: UILabel!
    @IBOutlet weak var lblIsEmpty: UILabel!
    @IBOutlet weak var btnCountinueShopping: UIButton!
    @IBOutlet weak var tblEmptyCart: UITableView!
    @IBOutlet weak var cnstrntHeightTblEmptyCart: NSLayoutConstraint!

    
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var imgBackBG: UIImageView!
    @IBOutlet weak var btnBack: UIButton!

    @IBOutlet weak var lblItem: UILabel!
    
    @IBOutlet weak var scrolVIEW: UIScrollView!
    @IBOutlet weak var viewPayment: UIView!
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var htTbl: NSLayoutConstraint!
    
    @IBOutlet weak var viewPromoCode: UIView!
    @IBOutlet weak var txtPromoCode: UITextField!
    @IBOutlet weak var btnApplyPromoCode: UIButton!
    
    @IBOutlet weak var viewStoreCredit: UIView!
    @IBOutlet weak var lblStoreCreditApply: UILabel!
    @IBOutlet weak var btnStoreCredit: UIButton!
    
    @IBOutlet weak var viewTotal: UIView!
    @IBOutlet weak var lblSubTotal: UILabel!
    @IBOutlet weak var lblSubTotalValue: UILabel!
    @IBOutlet weak var lblShipping: UILabel!
    @IBOutlet weak var lblShippingValue: UILabel!
    @IBOutlet weak var lblDiscount: UILabel!
    @IBOutlet weak var lblDiscountValue: UILabel!
    @IBOutlet weak var lblStoreCredit: UILabel!
    @IBOutlet weak var lblStoreCreditValue: UILabel!
    
    @IBOutlet weak var lblTaxes: UILabel!
    @IBOutlet weak var lblTaxesValue: UILabel!
    
    @IBOutlet weak var lblTotal: UILabel!
    @IBOutlet weak var lblTotalValue: UILabel!
    
    @IBOutlet weak var btnApplePay: UIButton!
    @IBOutlet weak var btnCheckout: UIButton!
    
    @IBOutlet weak var viewFreeShipping: UIView!
    @IBOutlet weak var lblRequiredFreeShipping: UILabel!
    @IBOutlet weak var progressView: UIProgressView!
    
    var appliedStoreCreditString = "\(APP_DEL.selectedCountry.getCurrencySymbol().uppercased()) 0"
    var isStoreCreditApplied = false
    var store_creditwithoutcurrency = "0"
    var products = [CartProduct]()
    var total = [String:String]()
    var viewCartJson = JSON()
    
    var isExtraFeeAmount = false

    var isGiftCardThereInCart : Bool = false
    var isStoreCreditApplyManually : Bool = false
    
    
    var isPerformTabHide = false
    var selectedCartItem : CartProduct?
    
    var arrRecommendedProducts : [Product_data] = []
    var limitRecommend = 0
    var tagRelated = ""
    
    var selectedEditGiftIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewEmptyCart.isHidden = true
        
        self.scrolVIEW.isHidden = true
        self.viewPayment.isHidden = true
        
        setUI()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        isPerformTabHide = true
        
        self.isStoreCreditApplyManually = false
        isGiftCardThereInCart = false
        
        self.tblView.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.tabBarController?.tabBar.isHidden = true
        
        let isCOD : Bool = (UserDefaults.standard.value(forKey: APP_DEL.key_isCOD) as? Bool) ?? false
        let isCardSelected : Bool = (UserDefaults.standard.value(forKey: APP_DEL.key_isCard) as? Bool) ?? false
        

        if isCOD == true || isCardSelected == true {

            UserDefaults.standard.setValue(false, forKey: APP_DEL.key_isCOD)
            UserDefaults.standard.setValue(false, forKey: APP_DEL.key_isCard)
            
            self.saveShippingPayamentForClearCOD()

        } else {

            self.viewCartFlow()
        }
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        isPerformTabHide = false
        self.tabBarController?.tabBar.isHidden = false
        
        //self.tblView.removeObserver(self, forKeyPath: "contentSize")
    }
    
    
    @IBAction func btnBackAction(_ sender: UIButton) {

        self.navigationController?.popToRootViewController(animated: true)
        self.tabBarController?.selectedIndex = lastCartIndex;
        
        
        APP_DEL.getCartCount()
    }
    
    func viewCartFlow() {

        APP_DEL.getGiftCardData { (res) in

            if (self.defaults.object(forKey: "userInfoDict") != nil) {

                self.removeStoreCredit()

            } else {

                self.getCartList()
            }
        }
    }

    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?)
    {
        self.tblView.layer.removeAllAnimations()

        self.htTbl.constant = self.tblView.contentSize.height
        UIView.animate(withDuration: 0.5) {
            self.updateViewConstraints()
            self.loadViewIfNeeded()
        }
    }
    
    func setUI() {
        
        self.lblYourBasket.font  = UIFont(name: "Cairo-Regular", size: 14)!
        self.lblIsEmpty.font  = UIFont(name: "Cairo-Regular", size: 14)!
        self.btnCountinueShopping.titleLabel?.font = UIFont(name: "Cairo-Regular", size: 14.5)!
        self.lblYourBasket.text = APP_LBL().your_box_looks_empty.uppercased()
        self.lblIsEmpty.text = APP_LBL().what_are_you_waiting_for.uppercased()
        self.btnCountinueShopping.setTitle(APP_LBL().start_shopping.uppercased(), for: .normal)
        self.btnCountinueShopping.addTarget(self, action: #selector(continueShoppingTapped(_:)), for: UIControl.Event.touchUpInside)


        let value=UserDefaults.standard.value(forKey: "AppleLanguages") as! [String]
        
        self.tblView.register(UINib(nibName: "NejreeAppleCheckoutCell", bundle: nil), forCellReuseIdentifier: "NejreeAppleCheckoutCell")
        self.tblView.delegate = self
        self.tblView.dataSource = self
        
        self.tblEmptyCart.register(UINib(nibName: "DetailProductCell", bundle: nil), forCellReuseIdentifier: "DetailProductCell")
        self.tblEmptyCart.delegate = self
        self.tblEmptyCart.dataSource = self
        
        lblHeader.text = APP_LBL().my_box.uppercased();
        
        if value[0] == "ar" {
            
            lblItem.textAlignment = .right
            txtPromoCode.textAlignment = .right
            lblStoreCreditApply.textAlignment = .right
            lblSubTotal.textAlignment = .right
            lblShipping.textAlignment = .right
            lblDiscount.textAlignment = .right
             lblTaxes.textAlignment = .right
            lblStoreCredit.textAlignment = .right
            lblTotal.textAlignment = .right
            lblRequiredFreeShipping.textAlignment = .right
            imgBackBG.image = UIImage(named: "icon_back_white_Arabic")
            
        } else {
            lblRequiredFreeShipping.textAlignment = .left
            lblItem.textAlignment = .left
            txtPromoCode.textAlignment = .left
            lblStoreCreditApply.textAlignment = .left
            lblSubTotal.textAlignment = .left
            lblShipping.textAlignment = .left
            lblDiscount.textAlignment = .left
            lblTaxes.textAlignment = .left
            lblStoreCredit.textAlignment = .left
            lblTotal.textAlignment = .left
            
            imgBackBG.image = UIImage(named: "icon_back_white_round")
        }
        
        lblItem.text = APP_LBL().items.uppercased();
        
        lblItem.font  = UIFont(name: "Cairo-Regular", size: 13)!
        
        
        viewPromoCode.round(redius: 0)
        viewPromoCode.layer.borderColor = UIColor.lightGray.cgColor
        viewPromoCode.layer.borderWidth = 0.3
        txtPromoCode.placeholder = APP_LBL().code.uppercased()
        txtPromoCode.font = UIFont(name: "Cairo-Regular", size: 15)!
        
        btnApplyPromoCode.backgroundColor = nejreeColor
        btnApplyPromoCode.setTitleColor(UIColor.black, for: .normal)
        btnApplyPromoCode.setTitle(APP_LBL().apply.uppercased(), for: .normal)
        btnApplyPromoCode.titleLabel?.font = UIFont(name: "Cairo-Regular", size: 15)!
        
        viewStoreCredit.round(redius: 7)
        
        
        self.updateStoreCredit(isForUpdate: false, value: false)
        
        viewTotal.round(redius: 7)
        self.lblSubTotal.text = APP_LBL().subtotal.uppercased()
        self.lblShipping.text = APP_LBL().shipping.uppercased()
        self.lblDiscount.text = APP_LBL().discount.uppercased()
        self.lblTaxes.text = APP_LBL().tax_price.uppercased()
        self.lblStoreCredit.text = APP_LBL().store_credit.uppercased()
        self.lblTotal.text = APP_LBL().total.uppercased()
        
        self.lblTotalValue.textColor = nejreeColor
        
        viewFreeShipping.round(redius: 7)
        
        self.btnApplePay.setBorder()
        self.btnApplePay.setCornerRadius()
        self.btnApplePay.setTitle(" Pay", for: .normal)
        self.btnApplePay.titleLabel?.font = UIFont(name: "Cairo-Regular", size: 17)!
        self.btnApplePay.setTitleColor(UIColor.black, for: .normal)
        
        self.btnCheckout.setBorder()
        self.btnCheckout.setCornerRadius()
        self.btnCheckout.setTitle(APP_LBL().checkout.uppercased(), for: .normal)
        self.btnCheckout.titleLabel?.font = UIFont(name: "Cairo-Regular", size: 14)!
        self.btnCheckout.setTitleColor(UIColor.white, for: .normal)
        
      
      
     
        updateDetail()
    }
    
   
    
    @IBAction func btnApplyStoreCreditAction(_ sender: UIButton) {
        
         if UserDefaults.standard.bool(forKey: "isLogin") {
            
             self.isStoreCreditApplyManually = true
            if self.isStoreCreditApplied {
               
                      self.removeStoreCredit()
                  } else {
                      
                      if self.store_creditwithoutcurrency == "0" {
                          
                          self.view.isUserInteractionEnabled = false;
                          self.view.makeToast(APP_LBL().store_credit_not_available, duration: 2, position: .center, title: nil, image: nil, style: nil, completion: {
                              Void in
                              
                              self.view.isUserInteractionEnabled = true;
                          })
                          
                          return;
                      }
                      self.setStoreCredit()
                  }
            
        }
        else
         {
            self.view.isUserInteractionEnabled = false;
            self.view.makeToast(APP_LBL().store_credit_not_available, duration: 2, position: .center, title: nil, image: nil, style: nil, completion: {
                Void in
                
                self.view.isUserInteractionEnabled = true;
            })
        }
        
        
        
      
    }
    
    @IBAction func btnApplyPromoCodeAction(_ sender: UIButton) {
        
        if (self.total["coupon"] != "") && (self.total["is_discount"] == "true") {
            self.removePromoCode()
        } else {
            self.applyPromoCode()
        }
    }
    
    @IBAction func btnApplePayAction(_ sender: UIButton) {
        
        if UserDefaults.standard.bool(forKey: "isLogin") {
            
            for pro in self.products {
                
                //if pro.item_error != "" {
                if ((pro.item_error?.first?.type ?? "") != "") {
                    
                    self.view.makeToast(APP_LBL().some_of_your_products_are_out_of_stock, duration: 1.0, position: .center)
                    return
                }
            }
            
            let temp = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "NejreeAppleCheckoutVC") as! NejreeAppleCheckoutVC
            temp.viewCartJson = viewCartJson
            temp.isStoreCreditApplied = self.isStoreCreditApplied
            temp.appliedStoreCreditString = self.appliedStoreCreditString
            temp.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(temp, animated: true)
            
        } else {
            
            if let vc = UIStoryboard(name: "cedMageLogin", bundle: nil).instantiateViewController(withIdentifier: "nejreeLoginController") as? nejreeLoginController{
                
                APP_DEL.isLoginFromCartList = true
                vc.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    @IBAction func btnCheckoutAction(_ sender: UIButton) {
        
        
        for pro in self.products {
            
            //if pro.item_error != "" {
            if ((pro.item_error?.first?.type ?? "") != "") {
                
                self.view.makeToast(APP_LBL().some_of_your_products_are_out_of_stock, duration: 1.0, position: .center)
                return
            }
        }
        
        if (UserDefaults.standard.bool(forKey: "isLogin")) {
            
            
            let vc = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "nejreeAddressBookController") as! nejreeAddressBookController
            vc.isFromCheckout = true
            vc.isStoreCreditApplied = self.isStoreCreditApplied
            vc.appliedStoreCreditString = self.appliedStoreCreditString
            vc.store_creditwithoutcurrency = self.store_creditwithoutcurrency
            vc.cartTotal = self.total
            vc.cartProducts = self.products
            vc.isCountryCodEnabled = (viewCartJson["data"]["is_cod_enable_for_country"].stringValue == "1")
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
            
        } else {
            
            if let vc = UIStoryboard(name: "cedMageLogin", bundle: nil).instantiateViewController(withIdentifier: "nejreeLoginController") as? nejreeLoginController{
                
                APP_DEL.isLoginFromCartList = true
                vc.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    @objc func btnWishProductAction(_ sender: UIButton) {
        
        if self.products[sender.tag].is_wishlist == "0" {
            self.addToWishList(index: sender.tag)
        } else {
            self.removeFromWishList(index: sender.tag)
        }
    }
    
    
    @objc func btnEditGiftCardAction(_ sender: UIButton) {
        
        selectedEditGiftIndex = sender.tag
        
        let filt = APP_DEL.arrGftCardData.first(where: { (item) -> Bool in ((item.child_product_id ?? "") == (products[sender.tag].child_product_id ?? ""))})
        if filt != nil {

            let comp = products[sender.tag].sub_total?.components(separatedBy: " ")
            let giftPrice = String(format: "%.2f", Double(comp?[1] ?? "0.0")!)
            
            let vc = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "NejreeGiftcardConfirmVC") as! NejreeGiftcardConfirmVC
            vc.delegateSelectGiftcardOption = self
            vc.name = filt?.name ?? ""
            vc.mobile = filt?.phone ?? ""
            vc.message = filt?.description ?? ""
            vc.price = giftPrice
//            vc.imageURL = self.arrGiftcard[self.carousel.currentItemIndex].product_image ?? ""
            vc.modalPresentationStyle = .overFullScreen
            vc.modalTransitionStyle = .crossDissolve
            self.present(vc, animated: true, completion: nil)
            
        }
        
        
        
    }
    
    func selectGiftcardOption(confirm: Bool, name: String, phone: String, message: String) {
        
        self.view.endEditing(true)
        
        if confirm == false {
            return;
        }
        
     
        
        guard let storeId = UserDefaults.standard.value(forKey: "storeId") as? String else {return}
        var postData = [String:String]()
        postData["store_id"] = storeId

 
        postData["product_id"] = products[selectedEditGiftIndex].product_id
        postData["child_product_id"] = products[selectedEditGiftIndex].child_product_id
        postData["qty"] = "1"
        postData["po_number"] = phone
        postData["item_id"] = products[selectedEditGiftIndex].item_id
        if let cart_id = self.defaults.object(forKey: "cartId") as? String {
            postData["cart_id"] = cart_id;
        }
        
        
        print("\(products[selectedEditGiftIndex].options_selected)")
        
      
        
        
        

        
        let filt = APP_DEL.arrGftCardData.first(where: { (item) -> Bool in ((item.child_product_id ?? "") == (products[selectedEditGiftIndex].child_product_id ?? ""))})
        if filt != nil {
            
            let nameAttributeID = filt?.name_option_attribute_id ?? "79"
            let phoneAttributeID = filt?.phone_option_attribute_id ?? "80"
            let descAttributeID = filt?.description_option_attribute_id ?? "81"
            
            postData["Custom"] = "{\"options\":{\"\(nameAttributeID)\":\"\(name)\",\"\(phoneAttributeID)\":\"\(phone)\",\"\(descAttributeID)\":\"\(message.encodeEmoji)\"}}"
            
            var super_attribute = "{";
            super_attribute += "\""+("\(products[selectedEditGiftIndex].options_selected?.first?.option_id ?? 0)")+"\":";
            super_attribute += "\""+(products[selectedEditGiftIndex].options_selected?.first?.option_value ?? "")+"\",";

            if (super_attribute.last! == ",") {

                super_attribute = super_attribute.substring(to: super_attribute.index(before: super_attribute.endIndex));
            }
            super_attribute += "}";

            postData["super_attribute"] = super_attribute
        }
    
        if (UserDefaults.standard.object(forKey: "userInfoDict") != nil) {

            let userInfoDict = UserDefaults.standard.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
            postData["customer_id"] = userInfoDict["customerId"]!;
            postData["email"] = userInfoDict["email"]!;
        }

        postData["message"] = message.encodeEmoji
        postData["name"] = name
        postData["price"] = "\(products[selectedEditGiftIndex].subtotal_price_without_currency ?? 0.0)"

        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);

        API().callAPI(endPoint: "mobiconnect/checkout/editgiftcard", method: .POST, param: postData) { (json_res, err) in

            DispatchQueue.main.async {

                cedMageLoaders.removeLoadingIndicator(me: self);

                if err == nil {

                    let value=UserDefaults.standard.value(forKey: "AppleLanguages") as! [String]

                    if json_res[0]["success"].stringValue.uppercased() == "TRUE" {

                        APP_DEL.getGiftCardData { (res) in
                            print("getGiftCardData:- ", res)
                            self.getCartList()
                        }

                    }
                    else {

                        cedMageHttpException.showAlertView(me: self, msg: APP_LBL().gift_quantity_not_available, title: APP_LBL().error)
                    }
                }
            }
        }
    }
    
    
    @objc func btnDeleteProductAction(_ sender: UIButton) {
        
        removeProductIndex = sender.tag
        
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RemoveProductPopupVC") as! RemoveProductPopupVC
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        vc.delagateRemoveCartProduct = self
        self.present(vc, animated: true, completion: nil)
        
//        let showTitle = APP_LBL().confirmation
//        let showMsg = APP_LBL().operation_cant_be_undone
//
//        let confirmationAlert = UIAlertController(title: showTitle, message: showMsg, preferredStyle: UIAlertController.Style.alert);
//
//        confirmationAlert.addAction(UIAlertAction(title: APP_LBL().done_c, style: .default, handler: { (action: UIAlertAction!) in
//
//            self.removeProduct(index: sender.tag)
//        }));
//
//        confirmationAlert.addAction(UIAlertAction(title: APP_LBL().cancel, style: .default, handler: { (action: UIAlertAction!) in
//        }));
//
//        present(confirmationAlert, animated: true, completion: nil)
        
        
        
    }
    
    func removeCartProduct(res: Bool) {
        
        if res {
            
            self.removeProduct(index: removeProductIndex)
        }
    }
    
    func updateStoreCredit(isForUpdate: Bool, value: Bool) {
        
        if isForUpdate {
            
            if value == true {
                
                self.isStoreCreditApplied = true
                UserDefaults.standard.set(true, forKey: "isStoreCredit")
                
            } else {
                
                self.isStoreCreditApplied = false
                UserDefaults.standard.set(false, forKey: "isStoreCredit")
            }
        }
        
        if isStoreCreditApplied {
            
            self.lblStoreCreditValue.text = self.appliedStoreCreditString
            
            btnStoreCredit.setBackgroundImage(UIImage(named: "StoreSwitchOn"), for: .normal)
            
        } else {
            
            self.appliedStoreCreditString = "\(APP_DEL.selectedCountry.getCurrencySymbol().uppercased()) 0"
            self.lblStoreCreditValue.text = self.appliedStoreCreditString
            
            btnStoreCredit.setBackgroundImage(UIImage(named: "StoreSwitchOff"), for: .normal)
        }
                        
        if self.store_creditwithoutcurrency == "0"
        {
            self.lblStoreCreditApply.text = String(format: "%@ (\(APP_DEL.selectedCountry.getCurrencySymbol().uppercased()) 0)", APP_LBL().use_store_credit)
        }
        else
        {
            self.lblStoreCreditApply.text = String(format: "%@ (%@)", APP_LBL().use_store_credit,viewCartJson["data"]["store_credit"].stringValue)
        }
        
    }
    
    //MARK:- APPLY PROMO CODE
    func applyPromoCode() {
        
        if (self.txtPromoCode.text == "") {
            
            self.view.makeToast(APP_LBL().please_enter_coupon, duration: 2.0, position: .center, title: nil, image: nil, style: nil, completion: nil);
            return;
        }
        
        var postData = [String:String]()

        if UserDefaults.standard.bool(forKey: "isLogin") {
            
            if let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] {
                
                postData["hashkey"] = userInfoDict["hashKey"]!
                postData["customer_id"] = userInfoDict["customerId"]!;
            }
        }
        
      

        postData["store_id"] = UserDefaults.standard.value(forKey: "storeId") as? String
        
        if let cart_id = self.defaults.object(forKey: "cartId") as? String {
            postData["cart_id"] = cart_id;
        }
        
        postData["coupon_code"] = self.txtPromoCode.text!;
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader_withlockedbackground(me: self);
        
        API().callAPI(endPoint: "mobiconnect/checkout/coupon", method: .POST, param: postData) { (json_res, err) in
            
            DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                if err == nil {
                    
                    let json = json_res[0]
                    
                    let value=UserDefaults.standard.value(forKey: "AppleLanguages") as! [String]
                    
                    if(json["cart_id"]["success"].stringValue == "true"){
                        
                        self.view.makeToast("\(APP_LBL().you_can_use_the_discount_coupon_code) \(self.txtPromoCode.text!).", duration: 1.0, position: .center)
                    }
                    else{
                        
                        if value[0]=="ar" {
                            
                            cedMageHttpException.showAlertView(me: self, msg: json["cart_id"]["message"].stringValue, title: APP_LBL().error)
                            
                        } else {
                            
                            cedMageHttpException.showAlertView(me: self, msg: "\(APP_LBL().the_discount_coupan_code) \(self.txtPromoCode.text!) \(APP_LBL().is_invalid)", title: APP_LBL().error)
                        }
                      
                    }
                    
                    self.getCartList()
                }
            }
        }
    }
    
    //MARK:- REMOVE PROMO CODE
    func removePromoCode() {
        
        if (self.txtPromoCode.text == "") {
            
            self.view.makeToast(APP_LBL().please_enter_coupon, duration: 2.0, position: .center, title: nil, image: nil, style: nil, completion: nil);
            return;
        }
        
        var postData = [String:String]()

      
        if UserDefaults.standard.bool(forKey: "isLogin") {
            
            if let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] {
                
                postData["hashkey"] = userInfoDict["hashKey"]!
                postData["customer_id"] = userInfoDict["customerId"]!;
            }
        }
        
        
        
        postData["store_id"] = UserDefaults.standard.value(forKey: "storeId") as? String
        
        if let cart_id = self.defaults.object(forKey: "cartId") as? String {
            postData["cart_id"] = cart_id;
        }
        
        postData["remove"] = "1";
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader_withlockedbackground(me: self);
        
        API().callAPI(endPoint: "mobiconnect/checkout/coupon", method: .POST, param: postData) { (json_res, err) in
            
            DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                if err == nil {
                    
                    let json = json_res[0]
                    
                    let value=UserDefaults.standard.value(forKey: "AppleLanguages") as! [String]
                    
                    if (json["cart_id"]["success"].stringValue == "true") {
                        
                        if value[0]=="ar" {
                            
                            self.view.makeToast(json["cart_id"]["message"].stringValue, duration: 1.0, position: .center)
                            
                        } else {
                            
                            self.view.makeToast(APP_LBL().you_canceled_the_discount_coupon_code, duration: 1.0, position: .center)
                        }
                    }
                    
                    self.getCartList()
                }
            }
        }
    }
    
    //MARK:- SET STORE CREDIT
    func setStoreCredit() {
       
        
        if isGiftCardThereInCart {
            
            self.view.makeToast(APP_LBL().you_can_not_apply_store_credit_for_giftcard, duration: 2, position: .center, title: nil, image: nil, style: nil, completion: {
                Void in
                
            })
            
            return;
        }
        
        
        let currentStoreCredit = viewCartJson["data"]["store_creditwithoutcurrency"].stringValue
        if (currentStoreCredit == "0") {
         
            self.view.isUserInteractionEnabled = false;
            self.view.makeToast(APP_LBL().store_credit_not_available, duration: 2, position: .center, title: nil, image: nil, style: nil, completion: {
                Void in
                
                self.view.isUserInteractionEnabled = true;
            })
            
            return;
        }
        
        var postData = [String:String]()

        if(self.defaults.object(forKey: "userInfoDict") != nil){
            
            let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
            postData["hashkey"] = userInfoDict["hashKey"]!
            postData["customer_id"] = userInfoDict["customerId"]!;
            postData["store_id"] = UserDefaults.standard.value(forKey: "storeId") as? String
            
        } else {
            
            self.getCartList()
            return;
        }
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader_withlockedbackground(me: self);
        
        API().callAPI(endPoint: "mobiconnect/setStoreCredit", method: .POST, param: postData) { (json_res, err) in
            
            DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                if err == nil {
                    
                    let json = json_res[0]
                    
                    self.appliedStoreCreditString = json["applied_amout"].stringValue
                    self.updateStoreCredit(isForUpdate: true, value: true)
                    
                    self.getCartList()
                }
            }
        }
    }
    
    //MARK:- REMOVE STORE CREDIT
    func removeStoreCredit() {
        
        var postData = [String:String]()

        if (self.defaults.object(forKey: "userInfoDict") != nil) {
            
            let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
            postData["hashkey"] = userInfoDict["hashKey"]!
            postData["customer_id"] = userInfoDict["customerId"]!;
            postData["store_id"] = UserDefaults.standard.value(forKey: "storeId") as? String
            
        } else {
            
            self.getCartList()
            return;
        }
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader_withlockedbackground(me: self);
        
        API().callAPI(endPoint: "mobiconnect/removeStoreCredit", method: .POST, param: postData) { (json_res, err) in
            
            DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                if err == nil {
                    
                    let json = json_res[0]
                    
                    
                    if json["success"].stringValue == "true" {
                        self.updateStoreCredit(isForUpdate: true, value: false)
                    }
                    
                    self.getCartList()
                }
            }
        }
    }
    
    //MARK:- CART LIST
    func getCartList() {
        
        var postData = [String:String]()

        if let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] {

            postData["hashkey"] = userInfoDict["hashKey"]!
            postData["customer_id"] = userInfoDict["customerId"]!;
        } else {
            postData["customer_id"] = "0";
        }

        if let store_id = self.defaults.object(forKey: "storeId") as? String {
            postData["store_id"] = store_id;
        }

        if let cart_id = self.defaults.object(forKey: "cartId") as? String {
            postData["cart_id"] = cart_id;
        }
        postData["country_id"] = APP_DEL.selectedCountry.country_code ?? "SA"
//
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader_withlockedbackground(me: self);
        
     

//        let headers = [
//          "content-type": "application/json",
//          "authorization": "Bearer pgpdmn8i5t1kb2g8em3tc48bxwr1ojtq",
//          "cache-control": "no-cache",
//          "postman-token": "10c729fa-17db-5d3a-61f0-fa3c0acc442a"
//        ]
//        let parameters = ["parameters": [
//            "store_id": "1",
//            "hashkey": "47lwHdMOnOCArGRXyC6QDr3q5PPUU8m8",
//            "customer_id": "484612",
//            "cart_id": "6691269",
//            "country_id": "KW"
//          ]] as [String : Any]
//
//        let postData = try! JSONSerialization.data(withJSONObject: parameters, options: [])
//
//        let request = NSMutableURLRequest(url: NSURL(string: "https://stagenew.nejree.com/rest/V1/mobiconnect/checkout/viewcart")! as URL,
//                                                cachePolicy: .useProtocolCachePolicy,
//                                            timeoutInterval: 10.0)
//        request.httpMethod = "POST"
//        request.allHTTPHeaderFields = headers
//        request.httpBody = postData as Data
//
//        let session = URLSession.shared
//        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
//          if (error != nil) {
//            print(error)
//          } else {
//            let httpResponse = response as? HTTPURLResponse
//            print(httpResponse)
//
//            let json_res = JSON(data)
//
//            self.viewCartJson = json_res[0]
//            if let success =  json_res[0][0]["success"].stringValue as? String {
//            if success == "invalid_country"{
//                // redirect to splash screen
//                UserDefaults.standard.removeObject(forKey: Constants().USERDEFAULT_KEY_SELECTED_COUNTRY);
//
//                let test = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SplashVC") as! SplashVC
//                APP_DEL.window?.rootViewController = UINavigationController (rootViewController: test)
//                return
//            }
//            }
//        self.updateCartList()
//
//          }
//        })
//
//        dataTask.resume()
        
        
        
        
        API().callAPI(endPoint: "mobiconnect/checkout/viewcart/", method: .POST, param: postData) { (json_res, err) in

            DispatchQueue.main.async {

                cedMageLoaders.removeLoadingIndicator(me: self);

                if err == nil {

                    self.viewCartJson = json_res[0]
                    if let success =  json_res[0][0]["success"].stringValue as? String {
                        if success == "invalid_country"{
                            // redirect to splash screen
                            UserDefaults.standard.removeObject(forKey: Constants().USERDEFAULT_KEY_SELECTED_COUNTRY);

                            let test = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SplashVC") as! SplashVC
                            APP_DEL.window?.rootViewController = UINavigationController (rootViewController: test)
                            return
                        }
                    }
                    self.updateCartList()
                }
            }
        }
    }
    
    //MARK:- CART LIST
    func getEditCart(isUpdateQty:Bool? = false, selectedSize: String, selectedQty : String, selectedSizeValue: String = "") {
        
        // Note A: Check is same size product is selected then remove old one and change size. Not application on qty change cases
        if (products.count > 0 && !(isUpdateQty ?? false)){
            for indexProduct in 0..<products.count {
                let recProduct =  products[indexProduct]
                let filt = APP_DEL.arrGftCardData.first(where: { (item) -> Bool in ((item.child_product_id ?? "") == (recProduct.child_product_id ?? ""))})
                if filt == nil {
                    if (recProduct.product_id == selectedCartItem?.product_id){
                        if (recProduct.options_selected?.first?.value == selectedSizeValue){
                            // remove product here and on suceess callback of remove api we will call again Edit cart
                            self.removeProduct(index: indexProduct, isFromEditCart: true, selectedSize: selectedSize,selectedSizeValue: selectedSizeValue)
                            return
                        }
                    }
                }
            }
        }
        
        var postData = [String:String]()
        
        if let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] {
            
            postData["hashkey"] = userInfoDict["hashKey"]!
            postData["customer_id"] = userInfoDict["customerId"]!;
        } else {
            postData["customer_id"] = "0";
        }
        if let store_id = self.defaults.object(forKey: "storeId") as? String {
            postData["store_id"] = store_id;
        }
        
        if let cart_id = self.defaults.object(forKey: "cartId") as? String {
            postData["cart_id"] = cart_id;
        }
        
        
        if let item_id = self.selectedCartItem?.item_id as? String {
            postData["item_id"] = item_id;
        }
        
        if var qty = Int(selectedQty) {
//            if (Int(selectedCartItem?.options_selected?.first?.max_qty ?? "0") ?? 0 < qty){
//                qty = Int(selectedCartItem?.options_selected?.first?.max_qty ?? "0") ?? 0
//            }
            var maxQty = 0
            if let optionsSizeArray = selectedCartItem?.options_size {
                for rec in optionsSizeArray {
                    if ((rec.value ?? "") == selectedSizeValue){
                        maxQty = Int(rec.max_qty ?? "1") ?? 1
                    }
                }
            }
            qty = min(maxQty, qty) // if size change then we need to set minimum available qty
            qty = min(Int(max_qty_all_products) ?? 1, qty) // if size change then we need to set minimum available qty

            postData["qty"] = "\(qty)";
        }
        
//        var super_attribute = "{";
//        super_attribute += "\""+("\(self.selectedCartItem?.options_selected?.first?.option_id ?? 0)")+"\":";
//        super_attribute += "\""+(self.selectedCartItem?.options_selected?.first?.option_value ?? "")+"\",";
//
//        if (super_attribute.last! == ",") {
//
//            super_attribute = super_attribute.substring(to: super_attribute.index(before: super_attribute.endIndex));
//        }
//        super_attribute += "}";
        postData["super_attribute"] = selectedSize;
        
//        "item_id" : "4678078",
//               "qty" : "2",
//               "customer_id" : "47743",
//               "super_attribute" : "{\"169\":\"50\"}",
//               "store_id" : "1",
//               "hashkey" : "lIYzHM25oQFdq2svJumtoaWCRXiDaC1T",
//               "cart_id" : "5885821"
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader_withlockedbackground(me: self);
//        https://dev05.nejree.com/rest/V1/mobiconnect/checkout/editcart
        API().callAPI(endPoint: "mobiconnect/checkout/editcart", method: .POST, param: postData) { (json_res, err) in
            
            DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                if err == nil {
                    if (json_res[0]["success"].stringValue.lowercased() == "true".lowercased()) {
                        self.removeStoreCredit()
                    } else {
                        // show alert
                        self.selectedCartItem = nil
                        let strAlertMsg = APP_LBL().you_can_not_add_more_than_5_quantity
                       // strAlertMsg = strAlertMsg.replacingOccurrences(of: "X*", with: self.selectedCartItem?.options_selected?.first?.value ?? "")
                        cedMageHttpException.showAlertView(me: self, msg: strAlertMsg, title: APP_LBL().error)
                    }
                } else {
                    self.selectedCartItem = nil
                    // internet or server issue
                }
            }
        }
    }
    
    func updateCartList() {
        
        self.products.removeAll()
                
        if (viewCartJson["success"].stringValue.lowercased() == "false".lowercased()) {
            self.defaults.removeObject(forKey: "cartId")
            self.getRecommendedProducts()
        }
        
        self.btnApplePay.isHidden = !(viewCartJson["data"]["apple_pay_enable"].stringValue == "1")
            
        do {
            
            let catData = try viewCartJson["data"]["products"].rawData()
            self.products = try! JSONDecoder().decode([CartProduct].self, from: catData)
            if let cart_id = try viewCartJson["data"]["cart_id"].stringValue as? String {
                self.defaults.setValue(cart_id, forKey: "cartId")
            }
//            //Note A: Check for max qty for selected size and set max qty for option selected
//            for i in 0..<self.products.count {
//                let recProducts = self.products[i]
//                if let optionSizeArray = recProducts.options_size {
//                    for rec in optionSizeArray {
//                        if ((rec.value ?? "") == (recProducts.options_selected?.first?.value ?? "")){
//                            self.products[i].options_selected?[0].max_qty = rec.max_qty
//                        }
//                    }
//                }
//            }
            
        } catch {
            
        }
        
        
        self.total.removeAll()
        
        self.total["amounttopay"] = viewCartJson["data"]["total"][0]["amounttopay"].stringValue
        self.total["shipping_amount"] = viewCartJson["data"]["total"][0]["shipping_amount"].stringValue
        self.total["discount_amount"] = viewCartJson["data"]["total"][0]["discount_amount"].stringValue
        self.total["tax_amount"] = viewCartJson["data"]["total"][0]["tax_amount"].stringValue
        self.total["grandtotal"] = viewCartJson["data"]["grandtotal"].stringValue
        self.total["grandtotal_without_currency"] = viewCartJson["data"]["grandtotal_without_currency"].stringValue
        self.total["coupon"] = viewCartJson["data"]["coupon"].stringValue
        self.total["is_discount"] = viewCartJson["data"]["is_discount"].stringValue
        
        self.total["shipping_charge_new"] = viewCartJson["data"]["shipping_charge_new"].stringValue
        
         self.total["free_shipping_remaining"] = viewCartJson["data"]["free_shipping_remaining"].stringValue
                         
        self.store_creditwithoutcurrency = viewCartJson["data"]["store_creditwithoutcurrency"].stringValue
        
        
        
        
        //APP_DEL.auto_apply_storecredit
        
        self.checkNoData()
        
        self.updateDetail()
        
        let detail : [AnalyticKey:String] = [
            .Price : self.total["grandtotal"] as? String ?? "",
            .PriceWithoutCurrency : self.total["grandtotal_without_currency"] ?? ""
        ]
        
        API().setEvent(eventName: .OpenCart, eventDetail: detail) { (json, err) in }
    }
    func checkNoData() {
           
       if self.products.count == 0 {
           self.scrolVIEW.isHidden = true
           self.viewPayment.isHidden = true
                       
            APP_DEL.getCartCount()
           
        self.viewEmptyCart.isHidden = false
//           self.renderNoDataImage(imageName:"NewShopingEmpty");
        self.tblEmptyCart.reloadData()
        self.imgBackBG.isHidden = true
        self.btnBack.isHidden = true
           self.tabBarController?.tabBar.isHidden = false
           
       } else {
        self.imgBackBG.isHidden = false
        self.btnBack.isHidden = false
           self.viewEmptyCart.isHidden = true
           self.scrolVIEW.isHidden = false
           self.viewPayment.isHidden = false
           
           if isPerformTabHide {
               self.tabBarController?.tabBar.isHidden = true
           }
           
       }
   }
       
       func updateDetail() {
           
           self.tblView.reloadData()
           
           self.isGiftCardExist()
           
           
           if (self.total["coupon"] != "") && (self.total["is_discount"] == "true") {
               
               self.btnApplyPromoCode.setTitle(APP_LBL().remove.uppercased(), for: UIControl.State.normal);
               self.txtPromoCode.text = self.total["coupon"];
               
           } else {
               
               self.btnApplyPromoCode.setTitle(APP_LBL().apply.uppercased(), for: UIControl.State.normal);
               self.txtPromoCode.text = ""
           }
           
           self.updateStoreCredit(isForUpdate: false, value: false)
           
           if (total["discount_amount"] == "SAR 0") {
               self.lblDiscountValue.text = "0"
           } else {
               self.lblDiscountValue.text = self.total["discount_amount"]
           }
           
           
           if (total["tax_amount"] == "SAR 0") {
               self.lblTaxesValue.text = "0"
           } else {
               self.lblTaxesValue.text = self.total["tax_amount"]
           }

           
        self.lblRequiredFreeShipping.font  = UIFont(name: "Cairo-Regular", size: 16)!
           
       
        progressView.layer.cornerRadius = 5.0
        progressView.clipsToBounds = true
        if self.total["free_shipping_remaining"] == "0"{
            
            self.viewFreeShipping.superview?.isHidden = false
            
            self.lblRequiredFreeShipping.text = APP_LBL().your_order_qualify_for_free_shipping
            progressView.progress = 1.0
            progressView.setProgress(progressView.progress, animated: true)
            
            progressView.trackTintColor = UIColor.black
            progressView.progressTintColor = UIColor.init(hexString: "#FCB015")
        }
        else if self.total["free_shipping_remaining"] == "-1"
        {
            self.viewFreeShipping.superview?.isHidden = true
            self.lblRequiredFreeShipping.text = ""
            progressView.progress = 0.0
            progressView.setProgress(progressView.progress, animated: true)
        }
        else
        {
            
            progressView.trackTintColor = UIColor.black
            progressView.progressTintColor = UIColor.red
            
            self.viewFreeShipping.superview?.isHidden = false
            let stringFreeShipping = APP_LBL().free_shipping_remaining as String
            let strAddMoreAmount = APP_DEL.selectedCountry.getCurrencySymbol() + " " + (self.total["free_shipping_remaining"] ?? "") as String
            self.lblRequiredFreeShipping.text = stringFreeShipping.replacingOccurrences(of: "*", with: strAddMoreAmount)
            
        
            let FreeShippingAmount = Double(self.total["free_shipping_remaining"] ?? "0.0") ?? 0.0
            let minAmoutShipping = Double(APP_DEL.extrafee_minimum_order_amount) ?? 0.0
            let FinalProgress =  (FreeShippingAmount * 100.0) / minAmoutShipping
            
            progressView.progress = Float((100.0 - FinalProgress)/100.0)
            progressView.setProgress(progressView.progress, animated: true)
            
            let range = (self.lblRequiredFreeShipping.text! as NSString).range(of: strAddMoreAmount)
            let mutableAttributedString = NSMutableAttributedString.init(string: self.lblRequiredFreeShipping.text ?? "")
            mutableAttributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.init(hexString: "#FCB015") ?? UIColor.black, range: range)
            self.lblRequiredFreeShipping.attributedText = mutableAttributedString
           
        }
        
        
            if self.total["shipping_charge_new"] != "0" {
                
                
                self.lblShippingValue.text = (APP_DEL.selectedCountry.getCurrencySymbol().uppercased() + " " + (self.total["shipping_charge_new"] ?? "0"))
            }
            else
            {
                 self.lblShippingValue.text = APP_LBL().free
            }
        
        
           self.lblSubTotalValue.text = total["amounttopay"]
           self.lblTotalValue.text = self.total["grandtotal"]
           
           self.btnCheckout.setTitle("\(APP_LBL().checkout.uppercased()) (\(self.total["grandtotal"] ?? ""))", for: .normal)
           //btnApplePay.setTitle(" Pay \(self.total["grandtotal"] ?? "")", for: .normal)
           
           
        if isGiftCardThereInCart == false && APP_DEL.auto_apply_storecredit == "1" && self.store_creditwithoutcurrency != "0" && self.appliedStoreCreditString == "\(APP_DEL.selectedCountry.getCurrencySymbol().uppercased()) 0" && self.products.count != 0 && self.isStoreCreditApplyManually == false
        {
               
        if (self.store_creditwithoutcurrency != "") && (total["grandtotal"] != "\(APP_DEL.selectedCountry.getCurrencySymbol().uppercased()) 0")
                    {
                        self.setStoreCredit()
                    }

        }
           
       }
       
       func isGiftCardExist() {
           
           var res = false
           
           for pro in products {
                           
               let filt = APP_DEL.arrGftCardData.first(where: { (item) -> Bool in ((item.child_product_id ?? "") == (pro.child_product_id ?? ""))})
               
               if filt != nil {
                   
                   res = true
                   
                   break;
               }
           }
           
           isGiftCardThereInCart = res
       }
    
    //MARK:- REMOVE PRODUCT
    var removeProductIndex = -1
    func removeProduct(index: Int, isFromEditCart:Bool? = false,selectedSize: String = "",selectedSizeValue: String = "") {
        
        var postData = [String:String]()
        
        if UserDefaults.standard.bool(forKey: "isLogin") {
            
            let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
            postData["hashkey"] = userInfoDict["hashKey"]!
            postData["customer_id"] = userInfoDict["customerId"]!;
        }
        
      
        postData["store_id"] = UserDefaults.standard.value(forKey: "storeId") as? String
        
        guard (try? products[index].product_id!) != nil else {
           
            return;
        }
        
        
        postData["product_id"]=products[index].product_id!;
        postData["item_id"]=products[index].item_id!
        
        if let cart_id = self.defaults.object(forKey: "cartId") as? String {
            postData["cart_id"] = cart_id;
        }
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader_withlockedbackground(me: self);
        
        API().callAPI(endPoint: "mobiconnect/checkout/delete/", method: .POST, param: postData) { (json_res, err) in
            
            DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                if err == nil {
                    
                    let json = json_res[0]
                    
                    if (json["success"].stringValue == "removed_successfully") {
                        
                        let filt = APP_DEL.arrGftCardData.first(where: { (item) -> Bool in ((item.child_product_id ?? "") == (self.products[index].child_product_id ?? ""))})
                        
                        if filt != nil {
                            
                            let tempIndex = APP_DEL.arrGftCardData.firstIndex { (cat) -> Bool in
                                ((self.products[index].child_product_id ?? "") == (cat.child_product_id ?? ""))
                            }
                            
                            if tempIndex != nil {
                                APP_DEL.arrGftCardData.remove(at: tempIndex!)
                            }
                        }
                    }
                    
                    if (isFromEditCart ?? false){
                        self.products.remove(at: index)
                        self.getEditCart(selectedSize: selectedSize, selectedQty: "\(self.selectedCartItem?.quantity ?? 0)",selectedSizeValue: selectedSizeValue)
                    } else {
                        self.removeStoreCredit()
                    }
                }
            }
        }
    }
    
    //MARK:- ADD TO WISHLIST
    func addToWishList(index: Int) {
        
        
        if self.defaults.bool(forKey: "isLogin") == false {
            
            if let vc = UIStoryboard(name: "cedMageLogin", bundle: nil).instantiateViewController(withIdentifier: "nejreeLoginController") as? nejreeLoginController{
                APP_DEL.isLoginFromCartList = true
                vc.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
            return
        }
        
        
        var postData = [String:String]()
        
        let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
        postData["hashkey"] = userInfoDict["hashKey"]!
        postData["customer_id"] = userInfoDict["customerId"]!;
        postData["product_id"] = self.products[index].product_id!
        
        if let storeID =  UserDefaults.standard.value(forKey: "storeId") as? String {
            postData["store_id"] = storeID
        }
        
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader_withlockedbackground(me: self);
        
        API().callAPI(endPoint: "wishlist/addwishlist", method: .POST, param: postData) { (json_res, err) in
            
            DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                if err == nil {
                    
                    let json = json_res[0]["data"][0]
                    
                    let status = json["status"].stringValue;
                    let msg = json["message"].stringValue;
                    
                    var toastMessg = ""
                    cedMageLoaders.removeLoadingIndicator(me: self);
                    
                    if status == "true" {
                        
                        toastMessg = "\(self.products[index].product_name ?? "") " + APP_LBL().is_successfully_added_to_the_wishlist
                        
                        
                        Analytics.logEvent(AnalyticsEventAddToWishlist, parameters: [
                            AnalyticsParameterCurrency : APP_DEL.selectedCountry.getCurrencySymbol().uppercased(),
                            AnalyticsParameterItemCategory : "Shoes",
                            AnalyticsParameterItemID : self.products[index].product_id as Any,
                            AnalyticsParameterQuantity : "1",
                            AnalyticsParameterItemName : self.products[index].product_name ?? "",
                            AnalyticsParameterPrice : self.products[index].sub_total ?? "",
                            AnalyticsParameterValue : "",
                        ])
                        
                        var detail : [AnalyticKey:String] = [
                            .Currency : APP_DEL.selectedCountry.getCurrencySymbol().uppercased(),
                            .ItemCategory : "Shoes",
                            .ItemID : self.products[index].product_id ?? "",
                            .Quantity : "1",
                            .ItemName : self.products[index].product_name ?? "",
                            .Price : self.products[index].sub_total ?? "",
                            .PriceWithoutCurrency : "\(self.products[index].subtotal_price_without_currency ?? 0.0)"
                        ]
                        
                        API().setEvent(eventName: .AddToWishlist, eventDetail: detail) { (json, err) in }
                        
//                        if self.products.count > index {
//
//                            self.products[index].is_wishlist = (((self.products[index].is_wishlist ?? "") == "0") ? "1" : "0")
//                            self.tblView.reloadData()
//                        }
                        
                        self.getCartList()
                        
                        APP_DEL.isWishListShouldReload = true
                        
                    } else {
                        toastMessg = msg
                    }
                    
                    self.view.isUserInteractionEnabled = false;
                    self.view.makeToast(toastMessg, duration: 2, position: .center, title: nil, image: nil, style: nil, completion: {
                        Void in
                        
                        self.view.isUserInteractionEnabled = true;
                    })
                }
                
                APP_DEL.getCartCount()
            }
        }
    }
    
    //MARK:- REMOVE FROM WISHLIST
    func removeFromWishList(index: Int) {
        
        var postData = [String:String]()
        
        let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
        postData["hashkey"] = userInfoDict["hashKey"]!
        postData["customer_id"] = userInfoDict["customerId"]!;
        postData["product_id"] = self.products[index].product_id!
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader_withlockedbackground(me: self);
        
        API().callAPI(endPoint: "wishlist/removewishlist", method: .POST, param: postData) { (json_res, err) in
            
            DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                if err == nil {
                    
                    if self.products.count > index {
                        
                        self.products[index].is_wishlist = (((self.products[index].is_wishlist ?? "") == "0") ? "1" : "0")
                        
                         APP_DEL.isWishListShouldReload = true
                        
                        self.tblView.reloadData()
                    }
                }
                
                APP_DEL.getCartCount()
            }
        }
    }
    
    
    //MARK:- SAVE SHIPPING PAYMENT FOR CLEAR COD
    func saveShippingPayamentForClearCOD() {
        
        var postData = [String:String]()
        
        postData["Role"] = "USER";
        
        if let cartID =  UserDefaults.standard.value(forKey: "cartId") as? String {
            postData["cart_id"] = cartID
        }
        
        if let storeID =  UserDefaults.standard.value(forKey: "storeId") as? String {
            postData["store_id"] = storeID
        }
        
        if let userInfoDict = UserDefaults.standard.object(forKey: "userInfoDict") as? [String: String] {
            
            postData["hashkey"] = userInfoDict["hashKey"]
            postData["customer_id"] = userInfoDict["customerId"]
            postData["email"] = userInfoDict["email"]
        }
        
        postData["payment_method"] = "checkoutcom_apple_pay";
        
        postData["shipping_method"] = "freeshipping_freeshipping";
        
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader_withlockedbackground(me: self);
        
        API().callAPI(endPoint: "mobiconnect/checkout/saveshippingpayament", method: .POST, param: postData) { (json, err) in
            
            DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                self.viewCartFlow()
            }
        }
    }
    
    var popupSize = FFPopup()
    @objc func btnSizeAction(_ sender: UIButton) {
        if (allow_size_selection_incart ?? "0" != "1"){
            return
        }
        if let sizeArray = products[sender.tag].options_size {
            if (sizeArray.count > 0){
                selectedCartItem = products[sender.tag]
                let vi = Bundle.main.loadNibNamed("ProductSizePopup", owner: self, options: nil)?.first as! ProductSizePopup
                vi.parentVC = self
                vi.arrSize = sizeArray
                vi.setContent()
                vi.btnDone.addTarget(self, action: #selector(self.btnSizeDoneAction(_:)), for: .touchUpInside)
                vi.frame = CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: SCREEN_HEIGHT)
                
                self.popupSize = FFPopup(contetnView: vi, showType: .slideInFromBottom, dismissType: .slideOutToBottom, maskType: .dimmed, dismissOnBackgroundTouch: true, dismissOnContentTouch: false)
                self.popupSize.showInDuration = 0.3
                
                let layout = FFPopupLayout(horizontal: .left, vertical: .bottom)
                self.popupSize.show(layout: layout)
            }
        }
    }
    
    @objc func btnSizeDoneAction(_ sender: UIButton) {
        
        print("Size Done")
        popupSize.dismiss(animated: true)
    }
    
    var popupQty = FFPopup()
    @objc func btnQtyAction(_ sender: UIButton) {
        if (allow_size_selection_incart ?? "0" != "1"){
            return
        }
        if let optionsSizeArray = products[sender.tag].options_size {
            for rec in optionsSizeArray {
                if ((rec.value ?? "") == (products[sender.tag].options_selected?.first?.value ?? "")){
                    if (Int(rec.max_qty ?? "0") ?? 0 > 0){
                        selectedCartItem = products[sender.tag]
                        selectedCartItem?.options_selected?[0].max_qty = rec.max_qty ?? "0"
                        let vi = Bundle.main.loadNibNamed("ProductQtyPopup", owner: self, options: nil)?.first as! ProductQtyPopup
                        vi.parentVC = self
                        vi.maxQty = Int(rec.max_qty ?? "0") ?? 0
                        vi.setContent()
                        vi.btnDone.addTarget(self, action: #selector(self.btnQtyDoneAction(_:)), for: .touchUpInside)
                        vi.frame = CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: SCREEN_HEIGHT)
                        
                        self.popupQty = FFPopup(contetnView: vi, showType: .slideInFromBottom, dismissType: .slideOutToBottom, maskType: .dimmed, dismissOnBackgroundTouch: true, dismissOnContentTouch: false)
                        self.popupQty.showInDuration = 0.3
                        
                        let layout = FFPopupLayout(horizontal: .left, vertical: .bottom)
                        self.popupQty.show(layout: layout)
                    }
                }
            }
        }
        
    }
    
    @objc func btnQtyDoneAction(_ sender: UIButton) {
        
        print("Qty Done")
        popupQty.dismiss(animated: true)
    }
}


extension nejreeCartController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (tableView == tblEmptyCart){
            return 1
        }
        return products.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (tableView == tblEmptyCart){
            let cell = tableView.dequeueReusableCell(withIdentifier: "DetailProductCell", for: indexPath) as! DetailProductCell
//            cell.accessibilityIdentifier = identiSimilar
            cell.delegateDidSelectDetailProduct = self
            cell.selectionStyle = .none
            cell.isDisplayViewAll = false//(arrSimilar.count > limitSimilar)
            cell.btnViewAll.isHidden = !(arrRecommendedProducts.count > limitRecommend)
            cell.setProductData(product: arrRecommendedProducts)
            
            if APP_DEL.selectedLanguage == Arabic {
                cell.lblTitle.textAlignment = .right
            } else {
                cell.lblTitle.textAlignment = .left
            }
            
            
            cell.lblTitle.text = APP_LBL().recommended_products.uppercased()
            cell.lblTitle.textColor = UIColor.darkGray
            
           
            
            cell.alpha = ((arrRecommendedProducts.count == 0) ? 0 : 1)
            cell.isHidden = (arrRecommendedProducts.count == 0)

            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "NejreeAppleCheckoutCell", for: indexPath) as! NejreeAppleCheckoutCell
        cell.selectionStyle = .none
        
        cell.btnProductRedirctionImg.tag = indexPath.row
        cell.btnProductRedirctionImg.addTarget(self, action: #selector(ActionProductDetailRedirection(_:)), for: .touchUpInside)
        
        cell.btnProductRedirctionTitle.tag = indexPath.row
        cell.btnProductRedirctionTitle.addTarget(self, action: #selector(ActionProductDetailRedirection(_:)), for: .touchUpInside)
        
        cell.btnSize.tag = indexPath.row
        cell.btnSize.addTarget(self, action: #selector(btnSizeAction(_:)), for: .touchUpInside)
        
        cell.btnQty.tag = indexPath.row
        cell.btnQty.addTarget(self, action: #selector(btnQtyAction(_:)), for: .touchUpInside)
        
        cell.btnWish.tag = indexPath.row
        cell.btnWish.addTarget(self, action: #selector(btnWishProductAction(_:)), for: .touchUpInside)
        
        cell.btnDelete.tag = indexPath.row
        cell.btnDelete.addTarget(self, action: #selector(btnDeleteProductAction(_:)), for: .touchUpInside)
        
        
        cell.btnEditGiftCard.tag = indexPath.row
        cell.btnEditGiftCard.addTarget(self, action: #selector(btnEditGiftCardAction(_:)), for: .touchUpInside)
        
        let value = UserDefaults.standard.value(forKey: "AppleLanguages") as! [String]
        if value[0] == "ar" {
            cell.semanticContentAttribute = .forceRightToLeft
            cell.quantityLabel.textAlignment = .right
            cell.productName.textAlignment = .right
            cell.sizeValue.textAlignment = .right
        } else {
            cell.semanticContentAttribute = .forceLeftToRight
            cell.quantityLabel.textAlignment = .left
            cell.productName.textAlignment = .left
            cell.sizeValue.textAlignment = .left
        }
        
        let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: products[indexPath.row].total_regular_price!)
        attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 1, range: NSMakeRange(0, attributeString.length))
        
        cell.priceRegular.isHidden = true
        if products[indexPath.row].total_special_price != "no_special" {
            cell.priceLabel.text = products[indexPath.row].total_special_price;
            cell.priceRegular.attributedText = attributeString
            cell.priceRegular.isHidden = false
            cell.priceRegular.alpha = 1
            
        } else {
            cell.priceLabel.text = products[indexPath.row].total_regular_price
            cell.priceRegular.isHidden = true
            cell.priceRegular.alpha = 0
        }
        
        
        cell.btnSize.isUserInteractionEnabled = false
        cell.btnQty.isUserInteractionEnabled = false
        cell.imgQtyArrow.isHidden = true
        cell.imgSizeArrow.isHidden = true
        var productMaxQty = 0
        if (allow_size_selection_incart == "1"){
            if (products[indexPath.row].options_size?.count ?? 0 > 1){
                cell.imgSizeArrow.isHidden = false
                cell.btnSize.isUserInteractionEnabled = true
            } else  if (products[indexPath.row].options_size?.count ?? 0 == 1){
                if ((products[indexPath.row].options_size?.first?.value ?? "av") != (products[indexPath.row].options_selected?.first?.value ?? "asd")){
                    cell.imgSizeArrow.isHidden = false
                    cell.btnSize.isUserInteractionEnabled = true
                }
            }
//            else if ((Int(products[indexPath.row].options_selected?.first?.max_qty ?? "0") ?? 0) > 1) {
//                cell.imgQtyArrow.isHidden = false
//                cell.btnQty.isUserInteractionEnabled = true
//            }
                if let optionSizeArray = products[indexPath.row].options_size {
                    for rec in optionSizeArray {
                        if ((rec.value ?? "") == (products[indexPath.row].options_selected?.first?.value ?? "")){
                            productMaxQty = Int(rec.max_qty ?? "0") ?? 0
                            if ((Int(rec.max_qty ?? "0") ?? 0) > 1){
                                cell.imgQtyArrow.isHidden = false
                                cell.btnQty.isUserInteractionEnabled = true
                            } else if ((Int(rec.max_qty ?? "0") ?? 0) == 1){ // if max qty is one and selected is more than max qty
                                if ((Int(rec.max_qty ?? "0") ?? 0) < (products[indexPath.row].quantity ?? 0)){
                                    cell.imgQtyArrow.isHidden = false
                                    cell.btnQty.isUserInteractionEnabled = true
                                }
                            }
                        }
                    }
                }
        }
        
        cell.productName.text = products[indexPath.row].product_name ?? ""
        cell.quantityLabel.text = APP_LBL().qty.uppercased() + " " + "\(products[indexPath.row].quantity ?? 0)"
        cell.productImage.sd_setImage(with: URL(string: products[indexPath.row].product_image!), placeholderImage:nil)
        cell.btnEditGiftCard.isHidden = true
        
        
        let filt = APP_DEL.arrGftCardData.first(where: { (item) -> Bool in ((item.child_product_id ?? "") == (products[indexPath.row].child_product_id ?? ""))})
        if filt != nil {
            //Note A:Come here is product is gift
            var one = ""
            
            if value[0]=="ar" {
                
                one = (filt?.description ?? "")
                one = " " + (filt?.phone ?? "") + " | " + one
                one = (filt?.name ?? "") + " :" + APP_LBL().to + one
                
            } else {
                
                one = APP_LBL().to + ": " + (filt?.name ?? "")
                one = one + " | " + (filt?.phone ?? "") + " "
                one = one + (filt?.description ?? "")
            }
            
            cell.lblGiftDescr.text = one
            cell.lblGiftDescr.superview?.isHidden = false
            cell.btnSize.superview?.isHidden = true
            cell.sizeNotLabel.superview?.isHidden = true
            
            cell.btnEditGiftCard.isHidden = false
            
            if value[0] == "ar" {
                cell.productImage.sd_setImage(with: URL(string: products[indexPath.row].product_image!), placeholderImage:UIImage(named: "gift_card_ar"))
            } else {
                cell.productImage.sd_setImage(with: URL(string: products[indexPath.row].product_image!), placeholderImage:UIImage(named: "gift_card_eng"))
            }
            
            let comp = products[indexPath.row].sub_total?.components(separatedBy: " ")
            cell.lblGiftPrice.text = String(format: "%.2f", Double(comp?[1] ?? "0.0")!)
            
            cell.lblGiftPrice.isHidden = false
            cell.lblGiftPriceSAR.isHidden = false
            cell.imgQtyArrow.isHidden = true
            cell.btnQty.isUserInteractionEnabled = false
            cell.btnSize.isUserInteractionEnabled = false
            cell.lblGiftPriceSAR.text = APP_DEL.selectedCountry.getCurrencySymbol().uppercased()
            
        } else {
            
            cell.sizeValue.text = APP_LBL().size.uppercased() + ": " + (products[indexPath.row].options_selected?.first?.value ?? "")
            cell.lblGiftPrice.isHidden = true
            cell.lblGiftPriceSAR.isHidden = true
            cell.lblGiftPriceSAR.text = ""
            
            cell.lblGiftDescr.text = ""
            cell.lblGiftDescr.superview?.isHidden = true
            cell.btnSize.superview?.isHidden = false
            cell.btnQty.superview?.isHidden = false
            cell.btnQty.isUserInteractionEnabled = true
            cell.btnSize.isUserInteractionEnabled = true
            cell.sizeNotLabel.superview?.isHidden = true
            
            if ((products[indexPath.row].item_error?.first?.type ?? "") != "") {
                
                cell.btnSize.superview?.isHidden = false
                
                if (productMaxQty <= 0){
                    cell.btnQty.superview?.isHidden = true
                    cell.sizeNotLabel.superview?.isHidden = false
                    cell.sizeNotLabel?.text = APP_LBL().size_not_available
                } else {
                    cell.btnQty.isUserInteractionEnabled = true
                }
                
                if (products[indexPath.row].options_size?.count ?? 0 > 0){
    //                cell.sizeNotLabel?.text = APP_LBL().size_not_available
                    cell.qtyNotAvailable.backgroundColor = .black
                } else {
                    cell.sizeNotLabel.superview?.isHidden = false
                    cell.sizeNotLabel?.text = APP_LBL().item_out_of_stock
                }
    //            cell.lblOutOfStock?.superview?.isHidden = true
                
            } else {
                cell.sizeNotLabel.superview?.isHidden = true
    //            cell.lblOutOfStock?.superview?.isHidden = true
            }
            
            
        }
        
        if defaults.bool(forKey: "isLogin") == true {
            
            let filt = APP_DEL.arrGftCardData.first(where: { (item) -> Bool in ((item.child_product_id ?? "") == (products[indexPath.row].child_product_id ?? ""))})
            
            if filt != nil {
                cell.btnWish.isHidden = true
            } else {
                cell.btnWish.isHidden = false
            }
            
        } else {
            cell.btnWish.isHidden = false
        }
        
        if products[indexPath.row].is_wishlist == "0" {
            cell.btnWish.setImage(UIImage(named:"icon_wishlist_unselected"), for: .normal)
        } else {
            cell.btnWish.setImage(UIImage(named:"icon_wishlist_selected"), for: .normal)
        }
        
        cell.qtyNotAvailable.backgroundColor = .clear
   
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
         return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        return true
    }
    
}

extension nejreeCartController {
    
    func renderNoDataImage(imageName:String) {
        
        let noDataImageView = ShoppingEmpty();
        noDataImageView.frame = self.view.frame
        noDataImageView.buyNowButton.addTarget(self, action: #selector(continueShoppingTapped(_:)), for: UIControl.Event.touchUpInside)
        noDataImageView.btnAddFromWish.addTarget(self, action: #selector(ActionAddFromWishList(_:)), for: UIControl.Event.touchUpInside)
        
        
        noDataImageView.buyNowButton.setTitle(APP_LBL().start_shopping.uppercased(), for: .normal)
        noDataImageView.btnAddFromWish.setTitle(APP_LBL().add_from_wishlist.uppercased(), for: .normal)
        noDataImageView.youDontLabel.text = APP_LBL().you_box_look_empty_what_are_you_waiting_for
        
        noDataImageView.buyNowButton.setBorder()
        noDataImageView.buyNowButton.layer.cornerRadius = 5.0
        
        noDataImageView.btnAddFromWish.setBorder()
        noDataImageView.btnAddFromWish.layer.cornerRadius = 5.0
        
        noDataImageView.buyNowButton.titleLabel?.font = UIFont(name: "Cairo-Regular", size: 16)!
        noDataImageView.btnAddFromWish.titleLabel?.font = UIFont(name: "Cairo-Regular", size: 16)!
        
        noDataImageView.translatesAutoresizingMaskIntoConstraints = false;
        noDataImageView.contentMode = UIView.ContentMode.scaleAspectFit;
        self.view.addSubview(noDataImageView);
        self.view.addConstraint(NSLayoutConstraint(item: noDataImageView, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 0));
        self.view.addConstraint(NSLayoutConstraint(item: noDataImageView, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 0));
        self.view.addConstraint(NSLayoutConstraint(item: noDataImageView, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1, constant: 0));
        self.view.addConstraint(NSLayoutConstraint(item: noDataImageView, attribute: NSLayoutConstraint.Attribute.bottom, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: 0));
        
        for view in self.view.subviews{
            if view is UIButton{
                view.isHidden = true;
            }
        }
        
    }
    
    @objc func ActionAddFromWishList(_ sender: UIButton) {
        
        self.tabBarController?.selectedIndex = 3;
    }
    
    @objc func ActionProductDetailRedirection(_ sender: UIButton) {
        let filt = APP_DEL.arrGftCardData.first(where: { (item) -> Bool in ((item.child_product_id ?? "") == (products[sender.tag].child_product_id ?? ""))})
        
        if filt != nil {
            return;
        }
        
//        //if products[indexPath.row].item_error != "" {
//        if ((products[indexPath.row].item_error?.first?.type ?? "") != "") {
//            self.view.makeToast(APP_LBL().product_is_out_of_stock, duration: 1.0, position: .center)
//            return
//        }
        
        let vc = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "productViewController") as! productViewController
        APP_DEL.productIDglobal = products[sender.tag].product_id ?? ""
        vc.product_id = products[sender.tag].product_id ?? ""
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @objc func continueShoppingTapped(_ sender: UIButton) {
        
        if (APP_DEL.isComesFromOrder) {
            
            APP_DEL.changeLanguage()
            APP_DEL.isComesFromOrder = false
            
        } else {
            
            self.tabBarController?.selectedIndex = 2;
        }
    }
    
}


// Empty Cart
extension nejreeCartController: DidSelectDetailProduct{
    //MARK: PRODUCT DETAIL
    func getRecommendedProducts() {
        
        var postData = [String:String]()

        if let storeID =  UserDefaults.standard.value(forKey: "storeId") as? String {
            postData["store_id"] = storeID
        }
        

        if let genderID =  UserDefaults.standard.object(forKey: KEY_GENDER) as? String {
            postData["gender_id"] = genderID
        }
        postData["country_id"] =  APP_DEL.selectedCountry.country_code ?? "SA"
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);
        
        API().callAPI(endPoint: "mobiconnect/checkout/cartrecommended", method: .POST, param: postData) { (json_res, err) in
            
        //    DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                if err == nil {
                    
                    let json = json_res[0]
                    
                    do {
                        self.limitRecommend = json["data"]["recommended_products"]["limit"].intValue
                        self.tagRelated = json["data"]["recommended_products"]["filter"].stringValue
                        let relatedData = try json["data"]["recommended_products"]["products"].rawData()
                        self.arrRecommendedProducts = try JSONDecoder().decode([Product_data].self, from: relatedData)
                    } catch {
                        
                    }
                    
                    self.tblEmptyCart.delegate = self
                    self.tblEmptyCart.dataSource = self
                    self.tblEmptyCart.reloadData()
                    self.view.layoutIfNeeded()
                    //self.cnstrntHeightTblEmptyCart.constant = self.tblEmptyCart.contentSize.height
                }
          //  }
        }
    }
    
    func didSelectDetailProduct(type: String, index: Int) {
        if self.arrRecommendedProducts.count > index {
            let vc = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "productViewController") as! productViewController
            vc.product_id = self.arrRecommendedProducts[index].product_id ?? ""
            APP_DEL.productIDglobal = self.arrRecommendedProducts[index].product_id ?? ""
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            if let viewController =  UIStoryboard(name: "categorylayouts", bundle: nil).instantiateViewController(withIdentifier: "cedMageDefaultCollection") as?  cedMageDefaultCollection {

                UserDefaults.standard.set(tagRelated, forKey: "filtersToSend");

                viewController.strTitleLayer = APP_LBL().recommended_products.uppercased()
                //viewController.searchString = layer.layer_data?[index.row].tags ?? ""
                viewController.hidesBottomBarWhenPushed = true
                viewController.isFilterDisplay = false
                viewController.selectedCategory = "" //APP_DEL.isFromHomeBannerID
                self.navigationController?.pushViewController(viewController, animated: true)
            }
        }
    }
}

