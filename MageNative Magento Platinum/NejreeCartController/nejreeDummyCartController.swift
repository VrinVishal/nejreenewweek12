//
//  nejreeDummyCartController.swift
//  MageNative Magento Platinum
//
//  Created by vishal.n on 07/09/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class nejreeDummyCartController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.hidesBottomBarWhenPushed = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        
        
        let cart = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "nejreeCartController") as! nejreeCartController
        self.navigationController?.pushViewController(cart, animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
