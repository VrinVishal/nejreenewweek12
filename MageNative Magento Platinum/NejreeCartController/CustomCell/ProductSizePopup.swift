//
//  ProductSizePopup.swift
//  MageNative Magento Platinum
//
//  Created by vishal.n on 30/01/21.
//  Copyright © 2021 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit



class ProductSizePopup: UIView {
    
    @IBOutlet weak var lblSelectSize: UILabel!
    @IBOutlet weak var btnClose: UIButton!
    
    @IBOutlet weak var collView: UICollectionView!
    @IBOutlet weak var collViewHT: NSLayoutConstraint!
    
    @IBOutlet weak var btnDone: UIButton!
    
    var selctedIndex = -1
    var prevSelectedIndex = -1
    var arrSize: [Options_selected] = []
    
    var parentVC: UIViewController?

    func setContent() {
        
        self.lblSelectSize.text = APP_LBL().select_size.uppercased()
        self.btnDone.setTitle(APP_LBL().done.uppercased(), for: .normal)
        self.btnDone.backgroundColor = .lightGray
        btnDone.isUserInteractionEnabled = false
        
        // check which size is already selected and set that index selected
        var sizeArrayTemp : [Options_selected]?
        var sizeIdAlreadySelected = ""
        if let vc = self.parentVC as? nejreeCartController {
            sizeArrayTemp = vc.selectedCartItem?.options_size
            sizeIdAlreadySelected = vc.selectedCartItem?.options_selected?.first?.value ?? "0"
        } else if let vc = self.parentVC as? NejreeAppleCheckoutVC {
            sizeArrayTemp = vc.selectedCartItem?.options_size
            sizeIdAlreadySelected = vc.selectedCartItem?.options_selected?.first?.value ?? "0"
        }
        
        for i in 0..<(sizeArrayTemp?.count ?? 0) {
            let sizeRecord = sizeArrayTemp![i]
            if ((sizeRecord.value ?? "0") == sizeIdAlreadySelected){
                selctedIndex = i
                prevSelectedIndex = i
            }
        }
        
        collView.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
        self.collView.register(UINib(nibName: "CartProductSizeCell", bundle: nil), forCellWithReuseIdentifier: "CartProductSizeCell")
        self.collView.delegate = self
        self.collView.dataSource = self
        
        self.collView.reloadData()
        
       // self.collView.scrollToItem(at: IndexPath(row: selctedIndex, section: 0), at: .centeredVertically, animated: true)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?)
    {
        if let coll = object as? UICollectionView {
            
            if coll == collView {
                
                collView.layer.removeAllAnimations()
                collViewHT.constant = collView.contentSize.height
            }
        }
    }
    
    @IBAction func btnBGAction(_ sender: UIButton) {
        
        if let vc = self.parentVC as? nejreeCartController {
            vc.popupSize.dismiss(animated: true)
        } else if let vc = self.parentVC as? NejreeAppleCheckoutVC {
            vc.popupSize.dismiss(animated: true)
        }
    }
    
    @IBAction func btnDoneAction(_ sender: UIButton) {
        
        var super_attribute = "{";
        super_attribute += "\""+("\(self.arrSize[selctedIndex].option_id ?? 0)")+"\":";
        super_attribute += "\""+(self.arrSize[selctedIndex].option_value ?? "")+"\",";
        
        if (super_attribute.last! == ",") {
            
            super_attribute = super_attribute.substring(to: super_attribute.index(before: super_attribute.endIndex));
        }
        super_attribute += "}";
        if var vc = self.parentVC as? nejreeCartController {
            vc.popupQty.dismiss(animated: true)
            vc.getEditCart(isUpdateQty: false, selectedSize: super_attribute, selectedQty: "\(vc.selectedCartItem?.quantity ?? 1)", selectedSizeValue : self.arrSize[selctedIndex].value ?? "")
        } else if var vc = self.parentVC as? NejreeAppleCheckoutVC {
            vc.popupQty.dismiss(animated: true)
            vc.getEditCart(isUpdateQty: false, selectedSize: super_attribute, selectedQty: "\(vc.selectedCartItem?.quantity ?? 1)", selectedSizeValue : self.arrSize[selctedIndex].value ?? "")
        }
        
    }
}

extension ProductSizePopup: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.arrSize.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CartProductSizeCell", for: indexPath) as! CartProductSizeCell
        
        let si = self.arrSize[indexPath.row]
        
        cell.lblSize.text = "\(si.value ?? "")"
        if (Int(si.max_qty ?? "0") ?? 0  > 0){
            cell.lblStrike.isHidden = true
        } else {
            cell.lblStrike.isHidden = false
        }
        
        if selctedIndex == indexPath.row {
            cell.lblSize.superview?.backgroundColor = UIColor.black
            cell.lblSize.textColor = UIColor(hex: "#FC9A00")
        } else {
            cell.lblSize.superview?.backgroundColor = UIColor.white
            cell.lblSize.textColor = ((Int(si.max_qty ?? "0") ?? 0  > 0) ? UIColor.black : UIColor.black.withAlphaComponent(0.52))
        }
        
        cell.layer.borderColor = UIColor(hex: "#DDDDDD")?.cgColor
        cell.layer.borderWidth = 0.5
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let si = self.arrSize[indexPath.row]
        
        if (Int(si.max_qty ?? "0") ?? 0  > 0) && (selctedIndex != indexPath.row) {
            if (prevSelectedIndex != indexPath.row) {
                btnDone.isUserInteractionEnabled = true
                self.btnDone.backgroundColor = .black
            } else {
                btnDone.isUserInteractionEnabled = false
                self.btnDone.backgroundColor = .lightGray
            }
            selctedIndex = indexPath.row
            
        } else {
            btnDone.isUserInteractionEnabled = false
            self.btnDone.backgroundColor = .lightGray
            selctedIndex = -1
        }
        
        self.collView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: ((SCREEN_WIDTH - (40.0 + 2.0)) / 4.0), height: 45.0)
    }
        
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0.5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        
        return 0.5
    }
}
