//
//  CartProductQtyCell.swift
//  MageNative Magento Platinum
//
//  Created by vishal.n on 30/01/21.
//  Copyright © 2021 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class CartProductQtyCell: UICollectionViewCell {

    @IBOutlet weak var lblQty: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        lblQty.layer.cornerRadius = 2
        lblQty.clipsToBounds = true
        lblQty.layer.borderColor = UIColor.black.cgColor
        lblQty.layer.borderWidth = 0.5
    }

}
