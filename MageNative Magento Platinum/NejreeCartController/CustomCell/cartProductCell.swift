//
//  cartProductCell.swift
//  MageNative Magento Platinum
//
//  Created by cedcoss on 20/10/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class cartProductCell: UITableViewCell {

 
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var lblGiftPrice: UILabel!
    @IBOutlet weak var lblGiftPriceSAR: UILabel!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var quantityLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var outOfStockLabel: UILabel!
    
    @IBOutlet weak var btnAddtoWishList: UIButton!
    @IBOutlet weak var viewSlider: UIView!
    
    
    @IBOutlet weak var cartLeadingConstant: NSLayoutConstraint!
    @IBOutlet weak var sizeValue: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

   
}
