//
//  CartProductSizeCell.swift
//  MageNative Magento Platinum
//
//  Created by vishal.n on 30/01/21.
//  Copyright © 2021 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class CartProductSizeCell: UICollectionViewCell {

    @IBOutlet weak var lblSize: UILabel!
    @IBOutlet weak var lblStrike: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
