//
//  ProductQtyPopup.swift
//  MageNative Magento Platinum
//
//  Created by vishal.n on 30/01/21.
//  Copyright © 2021 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class ProductQtyPopup: UIView {
    
    @IBOutlet weak var lblSelectQuantity: UILabel!
    @IBOutlet weak var btnClose: UIButton!
    
    @IBOutlet weak var collView: UICollectionView!
    @IBOutlet weak var collViewHT: NSLayoutConstraint!
    
    @IBOutlet weak var btnDone: UIButton!
    
    var selctedIndex = -1
    var prevSelectedIndex = -1
    var arrQty = [String]()
    var maxQty = 0
    var parentVC: UIViewController?
    
    func setContent() {
        if (maxQty <= 0){
            self.removeFromSuperview()
            return
        }
        if (maxQty > (Int(max_qty_all_products) ?? 0)){
            maxQty = (Int(max_qty_all_products) ?? 0)
        }
        for i in 1...maxQty {
            arrQty.append("\(i)")
        }
        
        // check which qty is already selected and set that index selected
        var sizeArrayTemp : [Options_selected]?
        var sizeIdAlreadySelected = 0
        if let vc = self.parentVC as? nejreeCartController {
            sizeArrayTemp = vc.selectedCartItem?.options_size
            sizeIdAlreadySelected = vc.selectedCartItem?.quantity ?? 0
        } else if let vc = self.parentVC as? NejreeAppleCheckoutVC {
            sizeArrayTemp = vc.selectedCartItem?.options_size
            sizeIdAlreadySelected = vc.selectedCartItem?.quantity ?? 0
        }
        
        for i in 0..<arrQty.count {
            let sizeRecord = arrQty[i]
            if (sizeRecord == "\(sizeIdAlreadySelected)"){
                selctedIndex = i
                prevSelectedIndex = i
            }
        }
        
        self.lblSelectQuantity.text = APP_LBL().select_quantity.uppercased()
        self.btnDone.setTitle(APP_LBL().done.uppercased(), for: .normal)
        self.btnDone.backgroundColor = .lightGray
        btnDone.isUserInteractionEnabled = false

        //collView.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
        self.collView.register(UINib(nibName: "CartProductQtyCell", bundle: nil), forCellWithReuseIdentifier: "CartProductQtyCell")
        self.collView.delegate = self
        self.collView.dataSource = self
        
        self.collView.reloadData()
        self.collView.layoutIfNeeded()
        self.collView.scrollToItem(at: IndexPath(row: selctedIndex, section: 0), at: .left, animated: true)

    }
    
//    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?)
//    {
//        if let coll = object as? UICollectionView {
//
//            if coll == collView {
//
//                collView.layer.removeAllAnimations()
//                collViewHT.constant = collView.contentSize.height
//            }
//        }
//    }
    
    @IBAction func btnBGAction(_ sender: UIButton) {
        
        if let vc = self.parentVC as? nejreeCartController {
            vc.popupQty.dismiss(animated: true)
        } else if let vc = self.parentVC as? NejreeAppleCheckoutVC {
            vc.popupQty.dismiss(animated: true)
        } else if let vc = self.parentVC as? NejreeReturnOrderVC {
            vc.popupQty.dismiss(animated: true)
        }
    }
    
    @IBAction func btnDoneAction(_ sender: UIButton) {
        
        if var vc = self.parentVC as? nejreeCartController {
            var super_attribute = "{";
            super_attribute += "\""+("\(vc.selectedCartItem?.options_selected?.first?.option_id ?? 0)")+"\":";
            super_attribute += "\""+(vc.selectedCartItem?.options_selected?.first?.option_value ?? "")+"\",";
            
            if (super_attribute.last! == ",") {
                
                super_attribute = super_attribute.substring(to: super_attribute.index(before: super_attribute.endIndex));
            }
            super_attribute += "}";
//            vc.selectedCartItem?.quantity = Int(self.arrQty[selctedIndex]) ?? 0
            vc.popupQty.dismiss(animated: true)
            vc.getEditCart(isUpdateQty: true, selectedSize: super_attribute, selectedQty: self.arrQty[selctedIndex], selectedSizeValue : vc.selectedCartItem?.options_selected?.first?.value ?? "")
        } else if var vc = self.parentVC as? NejreeAppleCheckoutVC {
//            vc.selectedCartItem?.quantity = Int(self.arrQty[selctedIndex]) ?? 0
            var super_attribute = "{";
            super_attribute += "\""+("\(vc.selectedCartItem?.options_selected?.first?.option_id ?? 0)")+"\":";
            super_attribute += "\""+(vc.selectedCartItem?.options_selected?.first?.option_value ?? "")+"\",";
            
            if (super_attribute.last! == ",") {
                
                super_attribute = super_attribute.substring(to: super_attribute.index(before: super_attribute.endIndex));
            }
            super_attribute += "}";
            
            vc.popupQty.dismiss(animated: true)
            vc.getEditCart(isUpdateQty: true, selectedSize: super_attribute, selectedQty: self.arrQty[selctedIndex], selectedSizeValue : vc.selectedCartItem?.options_selected?.first?.value ?? "")
        } else if let vc = self.parentVC as? NejreeReturnOrderVC {
            
            vc.popupQty.dismiss(animated: true)
            vc.getEditCart(isUpdateQty: true, selectedSize: "", selectedQty: self.arrQty[selctedIndex], selectedSizeValue : "")
        }
        
    }

}

extension ProductQtyPopup: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.arrQty.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CartProductQtyCell", for: indexPath) as! CartProductQtyCell
        
        let si = self.arrQty[indexPath.row]
        
        cell.lblQty.text = si
        
        if selctedIndex == indexPath.row {
            cell.lblQty.backgroundColor = UIColor.black
            cell.lblQty.textColor = UIColor(hex: "#FC9A00")
        } else {
            cell.lblQty.backgroundColor = UIColor.white
            cell.lblQty.textColor = UIColor.black
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        if (selctedIndex != indexPath.row) {
            if (prevSelectedIndex != indexPath.row) {
                btnDone.isUserInteractionEnabled = true
                self.btnDone.backgroundColor = .black
            } else {
                btnDone.isUserInteractionEnabled = false
                self.btnDone.backgroundColor = .lightGray
            }
            selctedIndex = indexPath.row
        } else {
            btnDone.isUserInteractionEnabled = false
            self.btnDone.backgroundColor = .lightGray
            selctedIndex = -1
        }
        
        self.collView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: 40, height: 40)
    }
        
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        
        return 0
    }
}
