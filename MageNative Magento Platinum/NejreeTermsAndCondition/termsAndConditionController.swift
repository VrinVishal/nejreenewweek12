//
//  termsAndConditionController.swift
//  MageNative Magento Platinum
//
//  Created by MacMini on 29/08/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit
import WebKit

class termsAndConditionController: MagenativeUIViewController {

    @IBOutlet weak var termsTabel: UITableView!
    var data = [TnCData]()
    var data2 = [TnCData]()
    var data3 = [TnCData]()

  
    var str = String()
    
    var myWebView = WKWebView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        termsTabel.delegate = self
        termsTabel.dataSource = self
        termsTabel.contentInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 200, right: 0);
        myWebView.navigationDelegate = self
    
        // Do any additional setup after loading the view.
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    

}
extension termsAndConditionController: UITableViewDelegate,UITableViewDataSource,WKNavigationDelegate{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "t_cCell", for: indexPath) as! t_cCell
        myWebView.frame = CGRect(x: 0, y: 0, width: cell.frame.width, height: tableView.frame.size.height)
        
        cell.addSubview(myWebView)
        print(str)
        myWebView.loadHTMLString(str, baseURL: nil)
//        cell.topLabel.text = data[indexPath.row].title
//        cell.contentLabel.text = data[indexPath.row].content
//        cell.topLabel.numberOfLines = 0
//        cell.contentLabel.numberOfLines = 0
        cell.topLabel.isHidden = true
        cell.contentLabel.isHidden = true
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.frame.size.height
    }
    
    
   
    
    
    
}
