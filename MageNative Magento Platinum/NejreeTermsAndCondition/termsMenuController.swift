//
//  termsMenuController.swift
//  MageNative Magento Platinum
//
//  Created by MacMini on 29/08/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit
import WebKit

struct TnCData {
    var title: String?
    var content: String?
    
}

class termsMenuController: MagenativeUIViewController,UIGestureRecognizerDelegate {

    @IBOutlet weak var termsnConditionLabel: UILabel!
    @IBOutlet weak var imgTermsnCondition: UIImageView!
    @IBOutlet var viewBg : UIView!
    @IBOutlet weak var webview: WKWebView!
    
    @IBOutlet weak var btnBack: UIButton!

    var pageMenu: CAPSPageMenu?
    var controllerArray = [UIViewController]()
    var isFromGiftCard = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.isHidden = false
        termsnConditionLabel.isHidden = true
        if APP_DEL.selectedLanguage == Arabic{
            
            self.btnBack.setImage(UIImage(named: "BackArrowNewArabic"), for: .normal)
            
        } else {
          
            self.btnBack.setImage(UIImage(named: "BackArrowNew"), for: .normal)
        }
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)

        self.navigationController?.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: self, action: #selector(self.btnBackTapped(_:)))
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        self.getCMS()
        
        if navigationController?.responds(to: #selector(getter: UINavigationController.interactivePopGestureRecognizer)) ?? false {
                   
                   self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
                   self.navigationController?.interactivePopGestureRecognizer?.delegate = self
               }
        
    }
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
           
           return true
       }
    @objc func btnBackTapped(_ sender: UIBarButtonItem) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    func getCMS() {
        
        var postData = [String:String]()
        postData["language_id"] = UserDefaults.standard.value(forKey: "storeId") as? String
        
        if APP_DEL.selectedLanguage == Arabic{
            if isFromGiftCard {
                postData["cms_id"] = "terms-and-condition-for-giftcard-arabic"
            } else {
                postData["cms_id"] = "terms-and-condition-app-arabic"
            }
        } else {
            if isFromGiftCard {
                postData["cms_id"] = "terms-and-condition-for-giftcard-english"
            } else {
                postData["cms_id"] = "terms-and-condition-app-english"
            }
        }
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);
        
        API().callAPI(endPoint: "mobiconnect/cms/getpage/", method: .POST, param: postData) { (json, err) in
            
           // DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                if err == nil {
                    
                    let content = json[0]["data"]["result"][0]["content"].stringValue
                    let image = json[0]["data"]["result"][0]["image"].stringValue
                    
                    self.webview.loadHTMLString(content, baseURL: nil)
                    self.imgTermsnCondition.sd_setImage(with: URL(string: image), placeholderImage: nil)
                }
          //  }
        }
    }
    
    

}


//MARK:- CAPSPageMenuDelegate
extension termsMenuController : CAPSPageMenuDelegate {
    func willMoveToPage(_ controller: UIViewController, index: Int) {
        print(index)
    }
    func didMoveToPage(_ controller: UIViewController, index: Int) {
        print(index)
    }
}
