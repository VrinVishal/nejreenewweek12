//
//  nejreeAddressBookController.swift
//  MageNative Magento Platinum
//
//  Created by MacMini on 20/08/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit
import FirebaseAnalytics


struct addressData {
    var street: String?
    var city: String?
    var region: String?
    var country: String?
    var lastname: String?
    var firstname: String?
    var phone: String?
    var pincode: String?
    var address_id: String?
    var region_id: String?
    var neightbour_name: String?
    var selected: Bool
    var country_id: String?
    var neightbour_id: String?
    var city_id: String?
    var phone_code : String?
    var is_default : String?

}

struct AnalyticCartProduct: Codable {
    
    var productId: String?
    var productName: String?
    var productPrice: String?
    var productQuantity: String?
}

protocol addressSelectionDelegate {
    func addressIsSelected(dict : [String:String])
    func cancelAddressSelection()
}

protocol SelectAppleCheckoutAddress {
    func selectAppleCheckoutAddress(dict: [String:Any])
}

class nejreeAddressBookController: MagenativeUIViewController, UIGestureRecognizerDelegate,RemoveCartProduct {

   
    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var backButton: UIButton!

    @IBOutlet weak var addressBookTable: UITableView!
    @IBOutlet weak var cnstrntTblHeight: NSLayoutConstraint!

//    @IBOutlet weak var subHeadingLabel: UILabel!
    
    var FirstName = ""
    var LastName = ""
    
    var isFromCheckout = false
    var isForSelectionOfAddressAndBack = false
    var delegateSelection : addressSelectionDelegate?

    var isFromAccount = false

     var isStoreCreditApplied = false
    var appliedStoreCreditString = ""
    var store_creditwithoutcurrency = "0"
    
    var delegateSelectAppleCheckoutAddress: SelectAppleCheckoutAddress?
    var isFromSelectAppleCheckoutAddress = false
    
    var adresses = [addressData]()
    var selectedAddressId = String()
    var selectedAddress = [String:String]()
    // variables to store data from cart page
    var cartTotal = [String:String]()
    var cartProducts = [CartProduct]()
    var mainvc: UIViewController?
    var checkoutAs = "USER";
    var email_id = "test@test.com";
     var cart_id = "0";
    
    var orderStatusData = [String:String]()
    var removeProductIndex = -1
    var isCountryCodEnabled = true

    var selectedIndex = 888 {
        didSet{
            addressBookTable.reloadSections([0], with: .none)//reloadRows(at: [IndexPath(row: selectedIndex, section: 0)], with: .none)
        }
    }
    var isAddressSelected = true
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addressBookTable.isHidden = true
            if(defaults.object(forKey: "cartId") != nil){
                 cart_id = (defaults.object(forKey: "cartId") as? String)!;
             }else{
                 cart_id = "0"
             }
         let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
       email_id =  userInfoDict["email"]!
        
        self.getAddress()
        
        self.addressBookTable.separatorStyle = .none
        self.addressBookTable.delegate = self
        self.addressBookTable.dataSource = self
        
        topLabel.textAlignment = .left
        
//        topLabel.text = APP_LBL().address.uppercased()
        if isFromCheckout {
            topLabel.text = APP_LBL().choose_your_address.uppercased()
        } else {
            topLabel.text = APP_LBL().my_addresses.uppercased()
        }
        
        backButton.addTarget(self, action: #selector(dismissButtonTapped(_:)), for: .touchUpInside)

        
       
        if APP_DEL.selectedLanguage == Arabic {
          
            
            backButton.setImage(UIImage(named: "icon_back_white_Arabic"), for: .normal)
            
        } else {
            
            
            backButton.setImage(UIImage(named: "icon_back_white_round"), for: .normal)
            
        }

        NotificationCenter.default.addObserver(self, selector: #selector(newAddressAdded(_:)), name: NSNotification.Name(rawValue: "newAddressAddedId"), object: nil);

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        if navigationController?.responds(to: #selector(getter: UINavigationController.interactivePopGestureRecognizer)) ?? false {
                   
                   self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
                   self.navigationController?.interactivePopGestureRecognizer?.delegate = self
               }
    }
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        
        return true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    func getAddress() {
        
        var postData = [String:String]()
        
        if let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] {
            
            postData["hashkey"] = userInfoDict["hashKey"]!
            postData["customer_id"] = userInfoDict["customerId"]!;
        }
        
        if let store_id = self.defaults.object(forKey: "storeId") as? String {
            postData["store_id"] = store_id;
        }
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);
//        https://dev05.nejree.com/rest/V1/mobiconnect/customer/address
        API().callAPI(endPoint: "mobiconnect/customer/address/", method: .POST, param: postData) { (json_res, err) in

           // DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                if err == nil {
                    
                    let json = json_res[0]
                    
                    self.cnstrntTblHeight.constant = 10000
                    
                    var isDefaultAddressFound = false
                    for address in json["data"]["address"].arrayValue {
                        
                        self.adresses.append(addressData.init(street: address["street"].stringValue,
                                                              city: address["city"].stringValue,
                                                              region: address["region"].stringValue,
                                                              country: address["country"].stringValue,
                                                              lastname: address["lastname"].stringValue,
                                                              firstname: address["firstname"].stringValue,
                                                              phone: address["phone"].stringValue,
                                                              pincode: address["pincode"].stringValue,
                                                              address_id: address["address_id"].stringValue,
                                                              region_id: address["region_id"].stringValue,
                                                              neightbour_name: address["neighbour_name"].stringValue,
                                                              selected: false,
                                                              country_id : address["country_id"].stringValue,
                                                              neightbour_id: address["neighbour_id"].stringValue,
                                                              city_id: address["city_id"].stringValue,
                                                              phone_code : address["phone_code"].stringValue,
                                                              is_default: address["is_default"].stringValue))
                        
                        if address["is_default"].stringValue == "1"{
                            
                            let  addressRecord = addressData(street: address["address"].stringValue,
                                                               city: address["city"].stringValue,
                                                               region: address["region"].stringValue,
                                                               country: address["country"].stringValue,
                                                               lastname: address["lastname"].stringValue,
                                                               firstname: address["firstname"].stringValue,
                                                               phone: address["phone"].stringValue,
                                                               pincode: address["pincode"].stringValue,
                                                               address_id: address["address_id"].stringValue,
                                                               region_id: address["region_id"].stringValue,
                                                               neightbour_name: address["neighbour_name"].stringValue,
                                                               selected: false)
                            isDefaultAddressFound = true
                            
                            var tempAdd = [String:Any]()
                            
                            tempAdd["name"] = addressRecord.firstname
                            tempAdd["country"] = addressRecord.country
                            tempAdd["city"] = addressRecord.city
                            tempAdd["phone"] = addressRecord.phone
                            tempAdd["street"] = addressRecord.street
                            tempAdd["neighbour"] = addressRecord.neightbour_name
                            tempAdd["entity_id"] = addressRecord.address_id
                            tempAdd["firstname"] = addressRecord.firstname ?? ""
                            tempAdd["lastname"] = addressRecord.lastname ?? ""
      
                            
                            tempAddressObjApplePay = tempAdd
                        }
                        
                        
                    }
                    
                    if isDefaultAddressFound == false{
                        var tempAdd = [String:Any]()
                        if self.adresses.count > 0 {

                        tempAdd["name"] = self.adresses.first?.firstname
                        tempAdd["country"] = self.adresses.first?.country
                        tempAdd["city"] = self.adresses.first?.city
                        tempAdd["phone"] = self.adresses.first?.phone
                        tempAdd["street"] = self.adresses.first?.street
                        tempAdd["neighbour"] = self.adresses.first?.neightbour_name
                        tempAdd["entity_id"] = self.adresses.first?.address_id
                        tempAdd["firstname"] = self.adresses.first?.firstname ?? ""
                        tempAdd["lastname"] = self.adresses.first?.lastname ?? ""
  
                        
                        tempAddressObjApplePay = tempAdd
                        } else {
                            tempAddressObjApplePay = nil
                        }
                        
                    }
                    
                    
                    self.addressBookTable.reloadData()
                    self.addressBookTable.isHidden = false
                    
                    self.cnstrntTblHeight.constant = (194.0 * CGFloat(self.adresses.count)) + 70.0 + 20.0
                    
                    let c_id=UserDefaults.standard.value(forKey: "userInfoDict") as! [String:String]
                    
                    self.getRequiredFields()
                    
                }
        //    }
        }
    }
    
    func getRequiredFields() {
        
        let c_id = UserDefaults.standard.value(forKey: "userInfoDict") as! [String:String]
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);
        
        API().callAPI(endPoint: "mobiconnect/customer/getRequiredFields/\(c_id["customerId"]!)", method: .GET, param: [:]) { (json_res, err) in
            
          //  DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                if err == nil {
                    
                    let json = json_res[0]
                    
                    self.FirstName = json["firstname"].stringValue
                    self.LastName = json["lastname"].stringValue
                }
           // }
        }
    }
    
    //MARK:- SAVE BILLING SHIPPING
    func saveBillingShipping(address_id: String) {
        
        var postData = [String:String]()
        
        if let cartID =  UserDefaults.standard.value(forKey: "cartId") as? String {
            postData["cart_id"] = cartID
        }
        
        if let userInfoDict = UserDefaults.standard.object(forKey: "userInfoDict") as? [String: String] {
            postData["hashkey"] = userInfoDict["hashKey"]
            postData["customer_id"] = userInfoDict["customerId"]
        }
        
        postData["address_id"] = address_id
        
        if let store_id = self.defaults.object(forKey: "storeId") as? String {
            postData["store_id"] = store_id;
        }
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);
        
        API().callAPI(endPoint: "mobiconnect/checkout/savebillingshipping", method: .POST, param: postData) { (json_res, err) in
            
          //  DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                if err == nil {

                    let json = json_res[0]
                    
                    if json["success"].stringValue == "true" {
                        
                        
                        if self.cartTotal["grandtotal_without_currency"] == "0" {
                            
                            self.saveShippingPayament()
                        }
                        else
                        {
                            self.isAddressSelected = true
//                          if let vc = self.storyboard?.instantiateViewController(withIdentifier: "cedCheckout1") as? cedCheckout1
//                          {
//                              vc.addressData = selectedAddress;
//                              vc.mainvc = mainvc;
//                              vc.productsData = cartProducts;
//                              vc.totalOrderData = cartTotal;
//                           print(cartTotal)
//                            vc.isStoreCreditApplied = isStoreCreditApplied
//                            vc.appliedStoreCreditString = appliedStoreCreditString
//                              vc.checkoutAs = self.checkoutAs;
//                              self.navigationController?.pushViewController(vc, animated: true)
//                          }
//                          else {print("not from checkout")}
                            
                            let vc = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "NejreeCheckoutVC") as! NejreeCheckoutVC
                            vc.hidesBottomBarWhenPushed = true
                            
                            vc.isStoreCreditApplied = self.isStoreCreditApplied
                            vc.appliedStoreCreditString = self.appliedStoreCreditString
                            vc.store_creditwithoutcurrency = self.store_creditwithoutcurrency
                            vc.arrProduct = self.cartProducts
                            vc.isCountryCodEnabled = self.isCountryCodEnabled
                            vc.total = PaymentTotal(amounttopay: self.cartTotal["amounttopay"],
                                                    shipping_amount: self.cartTotal["shipping_amount"],
                                                    discount_amount: self.cartTotal["discount_amount"],
                                                    tax_amount: self.cartTotal["tax_amount"],
                                                    grandtotal: self.cartTotal["grandtotal"],
                                                    grandtotal_without_currency: self.cartTotal["grandtotal_without_currency"],
                                                    coupon: self.cartTotal["coupon"],
                                                    is_discount: self.cartTotal["is_discount"],
                                                   shipping_charge_new: self.cartTotal["shipping_charge_new"],
                                                   free_shipping_remaining: self.cartTotal["free_shipping_remaining"])
                                                    
                            
                            self.navigationController?.pushViewController(vc, animated: true)
                            
                        }
                    }
                    else
                    {
                        self.isAddressSelected = true
                        
                        APP_DEL.showAlertWithOkButton(parentVC: self, strTitle: APP_LBL().error, strMsg: APP_LBL().country_not_available_for_shipping)
                    }
                }
          //  }
        }
    }
    
    //MARK:- SAVE SHIPPING PAYMENT
    func saveShippingPayament() {
        
        var postData = [String:String]()
        
        postData["payment_method"] = "free";
        postData["shipping_method"] = "freeshipping_freeshipping"
        
        postData["Role"] = checkoutAs;
        postData["email"] = email_id;
        
        if let cartID =  UserDefaults.standard.value(forKey: "cartId") as? String {
            postData["cart_id"] = cartID
        }
        
        if let storeID =  UserDefaults.standard.value(forKey: "storeId") as? String {
            postData["store_id"] = storeID
        }
        
        if let userInfoDict = UserDefaults.standard.object(forKey: "userInfoDict") as? [String: String] {
            
            postData["hashkey"] = userInfoDict["hashKey"]
            postData["customer_id"] = userInfoDict["customerId"]
            postData["email"] = userInfoDict["email"]
        }
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);
        
        API().callAPI(endPoint: "mobiconnect/checkout/saveshippingpayament", method: .POST, param: postData) { (json, err) in
            
        //    DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                if err == nil {
                    
                    var tempParameterItemID = ""
                    
                    for prod in self.cartProducts {
                        if (prod.item_id ?? "") != "" {
                            tempParameterItemID = tempParameterItemID + ((tempParameterItemID == "") ? "\(prod.item_id ?? "")" : ",\(prod.item_id ?? "")")
                        }
                    }
                    
                    
                    print(tempParameterItemID)
                    
                    let detail : [AnalyticKey:String] = [
                        .ContentType : "free",
                        .ItemID : tempParameterItemID
                    ]
                    API().setEvent(eventName: .SelectContent, eventDetail: detail) { (json, err) in }
                    
                    Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
                        AnalyticsParameterContentType : "free",
                        AnalyticsParameterItemID : tempParameterItemID,
                    ])
                    
                    
                    
//                                if let vc = self.storyboard?.instantiateViewController(withIdentifier: "cedCheckout2") as? cedCheckout2{
//                                    vc.mainvc = self.mainvc
//
//
//                                    vc.isOrderReview = true;
//                                    vc.selectedPaymentMethod = "free"
//                                    vc.paymentMethodName = "free";
//                                    vc.productsData = self.cartProducts;
//                                    vc.addressData = self.selectedAddress;
//                                    vc.orderEmail = self.email_id;
//                                    vc.total = self.cartTotal
//                                    vc.cart_id = self.cart_id;
//                                    vc.isStoreCredit = self.isStoreCreditApplied//isStoreCredit
//                                    vc.appliedStoreCredit = self.appliedStoreCreditString//appliedStoreCredit
//                                    vc.codFee = jsonResponse["cod_fee"].stringValue
//                                    self.navigationController?.pushViewController(vc, animated: true);
//                                }
                    
                    self.saveOrder()
                }
        //    }
        }
    }
    
    @objc func dismissButtonTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func btnEditAddressAction(_ sender: UIButton) {
        
        let addressRecord = adresses[sender.tag]
        
        let storyboard = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil);
        let viewController = storyboard.instantiateViewController(withIdentifier: "nejreeAddAddressController") as! nejreeAddAddressController;
        viewController.isFromEditAddress = true
        viewController.FirstName = addressRecord.firstname ?? ""
        viewController.LastName = addressRecord.lastname ?? ""
        viewController.Country = addressRecord.country ?? ""
        viewController.CountryCode = addressRecord.country_id ?? ""
        viewController.City = addressRecord.city ?? ""
        viewController.Neighbourhood = addressRecord.neightbour_name ?? ""
        viewController.Postcode = addressRecord.pincode ?? ""
        viewController.Address_ID = addressRecord.address_id ?? ""
        viewController.is_default = addressRecord.is_default ?? ""
        viewController.phone_number = addressRecord.phone ?? ""
        self.navigationController?.pushViewController(viewController, animated: true);
    }
    
    //MARK:- SAVE ORDER
    func saveOrder() {
        
        let detail : [AnalyticKey:String] = [
            .Currency : APP_DEL.selectedCountry.getCurrencySymbol().uppercased(),
            .Coupon : self.cartTotal["coupon"] ?? "",
            .ItemID : getCoded().product_id,
            .Quantity : getCoded().quantity,
            .Price : getCoded().subTotal,
            .ItemName : getCoded().productName,
            .Value : cartTotal["grandtotal"] ?? "",
            .PriceWithoutCurrency : self.cartTotal["grandtotal_without_currency"] ?? ""
        ]
        API().setEvent(eventName: .BeginCheckout, eventDetail: detail) { (json, err) in }
        
        Analytics.logEvent(AnalyticsEventBeginCheckout, parameters: [
            AnalyticsParameterCurrency : APP_DEL.selectedCountry.getCurrencySymbol().uppercased(),
            AnalyticsParameterCoupon : self.cartTotal["coupon"] ?? "",
            AnalyticsParameterItemID : getCoded().product_id,
            AnalyticsParameterQuantity : getCoded().quantity,
            AnalyticsParameterPrice : getCoded().subTotal,
            AnalyticsParameterItemName : getCoded().productName,
            AnalyticsParameterValue : cartTotal["grandtotal"]!
        ])
        
        
        var postData = [String:String]()
        
        if let cartID =  UserDefaults.standard.value(forKey: "cartId") as? String {
            postData["cart_id"] = cartID
        } else {
            postData["cart_id"] = "0"
        }
        
        if let userInfoDict = UserDefaults.standard.object(forKey: "userInfoDict") as? [String: String] {
            
            postData["hashkey"] = userInfoDict["hashKey"]
            postData["customer_id"] = userInfoDict["customerId"]
            postData["email"] = userInfoDict["email"]
            
            //postData["shipping_fee"] = "0"
        }
        
        if let store_id = self.defaults.object(forKey: "storeId") as? String {
            postData["store_id"] = store_id;
        }
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);
        
        API().callAPI(endPoint: "mobiconnect/checkout/saveorder", method: .POST, param: postData) { (json_res, err) in
            
       //     DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                let json_0 = json_res[0]
                
                if err == nil {
                    
                    if (json_0["success"].stringValue == "true") {
                        
                        self.orderStatusData["orderId"] = json_0["order_id"].stringValue
                        self.orderStatusData["orderStatus"] = json_0["success"].stringValue
                        self.cartTotal["grandtotal"] = json_0["grandtotal"].stringValue
                        self.orderStatusData["order_date"] = json_0["order_date"].stringValue
                        
                        var arrTempBlocks : [ItemBlock] = []
                        for bl in self.cartProducts {
                            arrTempBlocks.append(ItemBlock(productId: bl.product_id, productName: bl.product_name, productPrice: bl.sub_total, productQuantity: "\(bl.quantity ?? 0)"))
                        }
                        let finalRes = self.getItemBlock(itemBlocks: arrTempBlocks)
                        
                        let strCoupon : String = self.cartTotal["coupon"] ?? ""
                        
                        let detail : [AnalyticKey:String] = [
                            .Value : self.cartTotal["grandtotal"] ?? "",
                            .Currency : APP_DEL.selectedCountry.getCurrencySymbol().uppercased(),
                            .Coupon : strCoupon,
                            .Tax : "",
                            .ItemID : finalRes.jsonString,
                            .TransactionID : self.orderStatusData["orderId"] ?? "",
                            .PriceWithoutCurrency : self.cartTotal["grandtotal_without_currency"] ?? ""
                        ]
                        API().setEvent(eventName: .Purchase, eventDetail: detail) { (json, err) in }
                        
                        Analytics.logEvent(AnalyticsEventPurchase, parameters: [
                            AnalyticsParameterValue : self.cartTotal,
                            AnalyticsParameterCurrency : APP_DEL.selectedCountry.getCurrencySymbol().uppercased(),
                            AnalyticsParameterCoupon : strCoupon,
                            AnalyticsParameterTax : "",
                            AnalyticsParameterItemID : finalRes.jsonString,
                            AnalyticsParameterTransactionID : self.orderStatusData["orderId"] as Any,

                        ])
                        
                        self.defaults.removeObject(forKey: "guestEmail");
                        self.defaults.removeObject(forKey: "cartId");
                        self.defaults.removeObject(forKey: "appliedCoupon");
                        self.defaults.setValue("0", forKey: "items_count")
                        
                        self.sendGiftCardSMS(order_id: self.orderStatusData["orderId"] ?? "")
                        
                    } else {
                        
                        self.view.makeToast(json_0["message"].stringValue, duration: 2, position: .center, title: nil, image: nil, style: nil, completion: nil)
                    }
                    
                    
                } else {
                                        
                    var arrTempBlocks : [ItemBlock] = []
                    for bl in self.cartProducts {
                        arrTempBlocks.append(ItemBlock(productId: bl.product_id, productName: bl.product_name, productPrice: bl.sub_total, productQuantity: "\(bl.quantity ?? 0)"))
                    }
                    
                    let finalRes = self.getItemBlock(itemBlocks: arrTempBlocks)

                    let Parameters = ["COUPON" : self.cartTotal["coupon"] ?? "",
                                      "CURRENCY" : APP_DEL.selectedCountry.getCurrencySymbol().uppercased() as Any,
                                      "ITEMS" : finalRes.jsonString,
                                      "VALUE" : ""
                    ]
                    
                    let detail : [AnalyticKey:String] = [
                        .Coupon : self.cartTotal["coupon"] ?? "",
                        .Currency : APP_DEL.selectedCountry.getCurrencySymbol().uppercased(),
                        .Items : finalRes.jsonString,
                        .Value : ""
                    ]
                    API().setEvent(eventName: .FailedCheckout, eventDetail: detail) { (json, err) in }
                    
                    Analytics.logEvent("failed_checkout", parameters: Parameters)
                }
           // }
        }
    }

    func sendGiftCardSMS(order_id: String) {

        guard let storeId = UserDefaults.standard.value(forKey: "storeId") as? String else {return}
        
        var postData = [String:String]()
        postData["store_id"] = storeId
        postData["order_id"] = order_id
        
        if let addressDynamic =  UserDefaults.standard.value(forKey: "selectedAddress") as? [String:Any] {
            
            postData["po_number"] = addressDynamic["phone"] as? String
        }
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);
        
        API().callAPI(endPoint: "mobiconnect/checkout/sendSmsGiftcard", method: .POST, param: postData) { (json_res, err) in
            
         //   DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                if err == nil {
                    
                    
                    var arrOrderItem = [orderItems]()
                                                                   
                arrOrderItem.removeAll()
                    
                    for i in 0..<self.cartProducts.count{
                                               
                    
                    let dictdata = self.cartProducts[i]
                                               
                    arrOrderItem.append(orderItems(product_type: dictdata.product_type,
                                                   product_name: dictdata.product_name,
                                                   product_price: dictdata.sub_total,
                                                   product_id: dictdata.product_id,
                                                   rowsubtotal: dictdata.sub_total,
                                                   product_qty: "\(dictdata.quantity ?? 0)",
                                                   product_image: dictdata.product_image,
                                                   selected: false,
                                                   itemId: dictdata.item_id,
                                                   optionSize: (dictdata.options_selected?.first?.value ?? "")))
                                               
                    }
                    
                    APP_DEL.isAccountPageRefreshAfterOrder = true
                    
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil);
                    let viewController = storyboard.instantiateViewController(withIdentifier: "cedMagePaymentSuccess") as! cedMagePaymentSuccess;
                    viewController.order_id = self.orderStatusData["orderId"]!
                    viewController.orderProduct = arrOrderItem
                    viewController.order_date = self.orderStatusData["order_date"]!
                    self.navigationController?.viewControllers = [viewController];
                }
          //  }
        }
    }

    func getCoded() -> (json: [[String:String]], jsonString: String, product_id : String, productName : String, subTotal : String, quantity : String) {
            
        var arrCoded : [AnalyticCartProduct] = []
        
        for d in self.cartProducts {
            arrCoded.append(AnalyticCartProduct(productId: d.product_id, productName: d.product_name, productPrice: d.sub_total?.components(separatedBy: " ").first, productQuantity: "\(d.quantity ?? 0)"))
        }
        
        var product_id : [String] = []
        var productName : [String] = []
        var subTotal : [String] = []
        var quantity : [String] = []
        
        for d in self.cartProducts {
            
            product_id.append(d.product_id ?? "")
            productName.append(d.product_name ?? "")
            subTotal.append(d.sub_total?.components(separatedBy: " ").first ?? "")
            quantity.append("\(d.quantity ?? 0)")
        }
        
        do {
            let data = try JSONEncoder().encode(arrCoded)
            let json : [[String : String]] = try JSONSerialization.jsonObject(with: data, options: []) as! [[String : String]]
            return (json: json,
                    jsonString: String(data: data, encoding: .utf8)!,
                    product_id : product_id.joined(separator: ","),
                    productName : productName.joined(separator: ","),
                    subTotal : subTotal.joined(separator: ","),
                    quantity : quantity.joined(separator: ","))
        } catch {
            print("ERROR...")
            return (json: [],
                    jsonString: "",
                    product_id : "",
                    productName : "",
                    subTotal : "",
                    quantity : "")
        }
    }
    
    func getItemBlock(itemBlocks: [ItemBlock]) -> (json: [[String:String]], jsonString: String) {
        
        do {
            let data = try JSONEncoder().encode(itemBlocks)
            let json : [[String : String]] = try JSONSerialization.jsonObject(with: data, options: []) as! [[String : String]]
            return (json: json, jsonString: String(data: data, encoding: .utf8)!)
        } catch {
            print("ERROR.")
            return (json: [], jsonString: "")
        }
    }
}

extension nejreeAddressBookController: UITableViewDelegate,UITableViewDataSource {
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return adresses.count + 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 7.0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let viewSeperator = UIView(frame: CGRect(x: 0.0, y: 0.0, width: SCREEN_WIDTH, height: 7.0))
        viewSeperator.backgroundColor = .clear
        return viewSeperator
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if ((adresses.count >= (indexPath.section + 1)) && (adresses.count > 0)){
            let cell = tableView.dequeueReusableCell(withIdentifier: "addressBookCell", for: indexPath) as! addressBookCell
            cell.insideView.cardView()
//            cell.layer.cornerRadius = 20

            cell.insideView.backgroundColor = .white
//            cell.insideView.layer.cornerRadius = 5.0
            
            cell.btnEdit.tag = indexPath.section
            cell.btnEdit.addTarget(self, action: #selector(self.btnEditAddressAction(_:)), for: UIControl.Event.touchUpInside)
            cell.btnDelete.tag = indexPath.section
            cell.btnDelete.addTarget(self, action: #selector(self.removeAddressTapped(_:)), for: UIControl.Event.touchUpInside)
            cell.btnDelete.isHidden = !isFromAccount
          //  cell.btnEdit.isHidden = !isFromAccount
            
           
            if APP_DEL.selectedLanguage == Arabic {
                
               // cell.countryName.text = "المملكة العربية السعودية"
                cell.countryName.textAlignment = .right
                
            } else {
                
                //cell.countryName.text = "Saudi Arabia"
                cell.countryName.textAlignment = .left
            }
            cell.countryName.text = adresses[indexPath.section].country
            cell.lblDefault.font = UIFont(fontName: "Cairo-Bold", fontSize: 15)
            cell.lblDefault.text = APP_LBL().default_address
            if ((adresses[indexPath.section].is_default ?? "") == "1"){
                cell.lblDefault.isHidden = false
            } else {
                cell.lblDefault.isHidden = true
            }
            
            let addressRecord = adresses[indexPath.section]
            
            cell.cityName.text = addressRecord.city
            cell.hoodName.text = addressRecord.neightbour_name
            if (addressRecord.phone?.count ?? 0 > 0){
                cell.mobileNumber.text =  "+" + (addressRecord.phone_code ?? "966") + "-" + (addressRecord.phone ?? "")
            } else {
                cell.mobileNumber.text = ""
            }
            
//            if value[0] == "ar" {
//                cell.userName.text = adresses[indexPath.row].lastname! + " " + adresses[indexPath.row].firstname!
//            } else {
                cell.userName.text = addressRecord.firstname! + " " + addressRecord.lastname!
            //}
            
            //cell.userName.text = adresses[indexPath.row].firstname! + " " + adresses[indexPath.row].lastname!
            cell.verifyImage.isHidden = true
            if selectedIndex != 888 {
                if indexPath.section == selectedIndex
                {
                    cell.verifyImage.isHidden = false
                    
                }
                else {
                    cell.verifyImage.isHidden = true
                    
                }
            }
            
           // if isFromCheckout { cell.removeAddressButton.isHidden = true }
            
//            cell.removeAddressButton.addTarget(self, action: #selector(removeAddressTapped(_:)), for: .touchUpInside)
//            cell.removeAddressButton.tag = Int(adresses[indexPath.row].address_id!)!
            cell.selectionStyle = .none
            return cell
    } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "addAddressCell", for: indexPath) as! addAddressCell
            cell.addAddressButton.addTarget(self, action: #selector(addNewAddress(_:)), for: .touchUpInside)
//            cell.addAddressButton.setTitle(APP_LBL().add_new_address.uppercased(), for: .normal)
//            cell.addAddressButton.roundCorners()
//            cell.addAddressButton.backgroundColor = .white
            cell.viewBg.cardView()
            cell.lblAddAddress.text = APP_LBL().add_new_address.uppercased()
            
        if APP_DEL.selectedLanguage == Arabic {
                cell.lblAddAddress.textAlignment = .right
            } else {
                cell.lblAddAddress.textAlignment = .left
            }
//            cell.addAddressButton.layer.borderColor = UIColor.init(hexString: "#FBAD18")?.cgColor
//            cell.addAddressButton.layer.borderWidth  = 1.0
            cell.selectionStyle = .none
            return cell
        }
        
        UITableViewCell()
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        var postData = [String:String]()
        
        
        
        if self.isFromSelectAppleCheckoutAddress {
            
            let addressRecord = adresses[indexPath.section]
            
            var tempAdd = [String:Any]()
            tempAdd["country_id"] = addressRecord.country ?? ""
            tempAdd["city"] = addressRecord.city ?? ""
            tempAdd["country"] = addressRecord.country ?? ""
            tempAdd["neighbour_name"] = addressRecord.neightbour_name ?? ""
            tempAdd["neighbour"] = addressRecord.neightbour_name ?? ""
            tempAdd["telephone"] = addressRecord.phone ?? ""
            tempAdd["firstname"] = addressRecord.firstname ?? ""
            tempAdd["lastname"] = addressRecord.lastname ?? ""
            tempAdd["entity_id"] = addressRecord.address_id ?? ""
            tempAdd["street"] = addressRecord.street ?? ""
            tempAdd["phone"] = addressRecord.phone

            self.delegateSelectAppleCheckoutAddress?.selectAppleCheckoutAddress(dict: tempAdd)
            self.navigationController?.popViewController(animated: true)
            
        } else if isAddressSelected {
            
            isAddressSelected = false
            
            if isFromCheckout {
                let addressRecord = adresses[indexPath.section]

                selectedAddress["name"] = addressRecord.firstname
                selectedAddress["country"] = addressRecord.country
                selectedAddress["city"] = addressRecord.city
                selectedAddress["phone"] = addressRecord.phone
                selectedAddress["street"] = addressRecord.street
                selectedAddress["neighbour"] = addressRecord.neightbour_name
                selectedAddress["entity_id"] = addressRecord.address_id
                defaults.setValue(selectedAddress, forKey: "selectedAddress")
                defaults.synchronize()
                self.selectedAddressId = addressRecord.address_id!
                
                selectedIndex = indexPath.section
                tableView.reloadData()
                self.saveBillingShipping(address_id: addressRecord.address_id!)
                
            } else if isForSelectionOfAddressAndBack {
                if let delegate = delegateSelection {
                    let addressRecord = adresses[indexPath.section]

                    selectedAddress["name"] = addressRecord.firstname
                    selectedAddress["country"] = addressRecord.country
                    selectedAddress["city"] = addressRecord.city
                    selectedAddress["phone"] = addressRecord.phone
                    selectedAddress["street"] = addressRecord.street
                    selectedAddress["neighbour"] = addressRecord.neightbour_name
                   selectedAddress["entity_id"] = addressRecord.address_id

                    delegate.addressIsSelected(dict: selectedAddress)
                    self.navigationController?.popViewController {
                        
                    }
                }
            }
            
        } else {
            return
        }
        

        
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if ((adresses.count >= (indexPath.section + 1)) && (adresses.count > 0)){
            return 180
        } else {
            return 70
        }
    }
    
//    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
//
//        if ((adresses.count >= (indexPath.section + 1)) && (adresses.count > 0)){
//            return false
//        } else {
//            return false
//        }
//    }
    
//    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
//
//
//        let swipeActionConfig = UISwipeActionsConfiguration(actions: [])
//        swipeActionConfig.performsFirstActionWithFullSwipe = false
//        return swipeActionConfig
//
//       
//    }
//    
//    
//    
//    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
//
//        print("Trailling")
//        
//        
//
//
//       let commentCell = tableView.cellForRow(at: indexPath)
//
//       let height = commentCell?.frame.size.height ?? 0.0
//
// 
//            
//
//        let deleteAction = UIContextualAction(style: .normal, title: nil) { (action, sourceView, completionHandler) in
//            print("index path of delete: \(indexPath)")
//            
////        let showTitle = APP_LBL().confirmation
////        let showMsg = APP_LBL().operation_cant_be_undone
////
////        let confirmationAlert = UIAlertController(title: showTitle, message: showMsg, preferredStyle: UIAlertController.Style.alert);
////
////        confirmationAlert.addAction(UIAlertAction(title: APP_LBL().done_c, style: .default, handler: { (action: UIAlertAction!) in
////
////
////
////        }));
////
////        confirmationAlert.addAction(UIAlertAction(title: APP_LBL().cancel, style: .default, handler: { (action: UIAlertAction!) in
////           }));
////
////           self.present(confirmationAlert, animated: true, completion: nil)
//          
//            
//            let btnSender = UIButton()
//           // btnSender.tag = indexPath.row
//             btnSender.tag = indexPath.section
//            
//            self.removeAddressTapped(btnSender)
//            
//            
//            completionHandler(true)
//        }
//
//                          
//          let commentCell1 = tableView.cellForRow(at: indexPath)
//
//          let height1 = commentCell1?.frame.size.height ?? 0.0
//
////          let backgroundImage1 = deleteImage(forHeight: height1)
////
////          if let backgroundImage1 = backgroundImage1 {
////              deleteAction.backgroundColor = UIColor(patternImage: backgroundImage1)
////          }
//        deleteAction.image = UIImage(named: "MyCartNewTrash")!
//        deleteAction.backgroundColor = UIColor(red: 250/255, green: 174/255, blue: 23/255, alpha: 1)
//
//           let swipeActionConfig = UISwipeActionsConfiguration(actions: [deleteAction])
//           swipeActionConfig.performsFirstActionWithFullSwipe = false
//
//           return swipeActionConfig
//        
//    }
    
    func deleteImage(forHeight height: CGFloat) -> UIImage? {
        var xPos = 0.0
       
        if APP_DEL.selectedLanguage == Arabic {
            xPos = 25
        } else {
            xPos = 25
        }
        
        let frame = CGRect(x: 0, y: 10, width: 300, height: height-10)

        UIGraphicsBeginImageContextWithOptions(CGSize(width:300, height: height-10), false, UIScreen.main.scale)

        let context = UIGraphicsGetCurrentContext()
        let nejreeColor = UIColor.init(hexString: "#FBAD18")
        context?.setFillColor(nejreeColor!.cgColor)
        context?.fill(frame)

        let image = UIImage(named: "MyCartNewTrash")

        image?.draw(in: CGRect(x: xPos , y: (Double(frame.size.height) / 2.0) - 12.5, width: 25, height: 25))

        let newImage = UIGraphicsGetImageFromCurrentImageContext()

        UIGraphicsEndImageContext()

        return newImage
    }
    
    @objc func removeAddressTapped(_ sender: UIButton) {
        
        removeProductIndex = sender.tag
        
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RemoveProductPopupVC") as! RemoveProductPopupVC
        vc.isFromDeleteAddress = true
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        vc.delagateRemoveCartProduct = self
        self.present(vc, animated: true, completion: nil)
        
        
//        let upview = UIView()
//        self.view.addSubview(upview)
//        upview.tag = 142
//        upview.backgroundColor = UIColor.black.withAlphaComponent(0.5)
//        upview.translatesAutoresizingMaskIntoConstraints = false
//        upview.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 0).isActive = true
//        upview.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0).isActive = true
//        upview.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0).isActive = true
//        upview.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0).isActive = true
//
//        let popup = removeAddress()
//        self.view.addSubview(popup)
//        popup.tag = 141
//        popup.backgroundColor = UIColor.init(hexString: "#F2F2F2")
//        popup.topImage.tintColor = UIColor.init(hexString: "#AF1F45")
//        popup.translatesAutoresizingMaskIntoConstraints = false
//        popup.widthAnchor.constraint(equalToConstant: self.view.frame.width - 50).isActive = true
//        popup.heightAnchor.constraint(equalToConstant: 200).isActive = true
//        popup.centerYAnchor.constraint(equalTo: self.view.centerYAnchor, constant: 0).isActive = true
//        popup.centerXAnchor.constraint(equalTo: self.view.centerXAnchor, constant: 0).isActive = true
//        popup.deleteButton.tag = sender.tag
//        popup.deleteButton.addTarget(self, action: #selector(deleteAddressButton(_:)), for: .touchUpInside)
//        popup.cancelButton.addTarget(self, action: #selector(cancelTapped(_:)), for: .touchUpInside)
//        popup.deleteButton.roundCorners()
//        popup.cancelButton.roundCorners()
//        popup.layer.cornerRadius = 5.0
//
//        popup.deleteButton.setTitle(APP_LBL().delete.uppercased(), for: .normal)
//        popup.cancelButton.setTitle(APP_LBL().cancel.uppercased(), for: .normal)
//        popup.noticeLabel.text = APP_LBL().notice.uppercased()
//        popup.statementLabel.text = APP_LBL().are_you_sure_you_want_to_delete_this_address
        
        
    }
    
    func removeCartProduct(res: Bool) {
           
           if res {
               
               //self.removeProduct(index: removeProductIndex)
            
            let btnDeleteAddress = UIButton()
            
            self.deleteAddressButton(btnDeleteAddress)
            
           }
       }
    
    
    
    
    @objc func cancelTapped(_ sender: UIButton) {
        self.view.viewWithTag(141)?.removeFromSuperview()
        self.view.viewWithTag(142)?.removeFromSuperview()
        
    }
    
    @objc func deleteAddressButton(_ sender: UIButton) {
        
//        self.view.viewWithTag(141)?.removeFromSuperview()
//        self.view.viewWithTag(142)?.removeFromSuperview()
        
        var postData = [String:String]()
        
        if let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] {
            
            postData["hashkey"] = userInfoDict["hashKey"]!
            postData["customer_id"] = userInfoDict["customerId"]!;
        }

        postData["address_id"] = adresses[removeProductIndex].address_id

        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);
        
        API().callAPI(endPoint: "mobiconnect/customer/deleteaddress/", method: .POST, param: postData) { (json_res, err) in
            
           // DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                if err == nil {
                    
                    if (json_res[0]["data"]["customer"][0]["status"].stringValue == "success") {
                        
                        self.resetVar()
                        self.getAddress()
                    }
                }
           // }
        }
    }
    
    @objc func addNewAddress(_ sender:UIButton){
        let storyboard = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil);
        let viewController = storyboard.instantiateViewController(withIdentifier: "nejreeAddAddressController") as! nejreeAddAddressController;
        viewController.FirstName = FirstName
        viewController.LastName = LastName
        viewController.CountryCode = APP_DEL.selectedCountry.country_code ?? "SA"
        self.navigationController?.pushViewController(viewController, animated: true);
    }
    
    func resetVar() {
        self.adresses = [addressData]()
    }
    
    @objc func newAddressAdded(_ notification: NSNotification){
        self.resetVar();
        self.getAddress()
    }
    
    
    
}
