//
//  addressBookCell.swift
//  MageNative Magento Platinum
//
//  Created by MacMini on 20/08/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class addressBookCell: UITableViewCell {

    
    @IBOutlet weak var insideView: UIView!
    @IBOutlet weak var countryName: UILabel!
    @IBOutlet weak var cityName: UILabel!
    @IBOutlet weak var hoodName: UILabel!
    @IBOutlet weak var mobileNumber: UILabel!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var lblDefault: UILabel!

    @IBOutlet weak var removeAddressButton: UIButton!
    @IBOutlet weak var verifyImage: UIImageView!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var btnDelete: UIButton!

    
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    
}
