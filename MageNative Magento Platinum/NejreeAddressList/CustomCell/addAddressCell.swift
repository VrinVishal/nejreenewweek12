//
//  addAddressCell.swift
//  MageNative Magento Platinum
//
//  Created by MacMini on 20/08/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class addAddressCell: UITableViewCell {

   
    @IBOutlet weak var lblAddAddress: UILabel!

    @IBOutlet weak var addAddressButton: UIButton!
    @IBOutlet weak var viewBg: UIView!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
