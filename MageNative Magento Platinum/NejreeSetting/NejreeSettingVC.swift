//
//  NejreeSettingVC.swift
//  MageNative Magento Platinum
//
//  Created by vishal.n on 12/02/21.
//  Copyright © 2021 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class NejreeSettingVC: UIViewController, UIGestureRecognizerDelegate {
    
    @IBOutlet weak var btnTitle: UILabel!
    @IBOutlet weak var imgBackBG: UIImageView!
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var tblViewHT: NSLayoutConstraint!
    
    var Stores = [[String:String]]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.tabBarController?.tabBar.isHidden = true
        setUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        tblView.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        if navigationController?.responds(to: #selector(getter: UINavigationController.interactivePopGestureRecognizer)) ?? false {
            
            self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
            self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        tblView.removeObserver(self, forKeyPath: "contentSize", context: nil)
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        if let tbl = object as? UITableView {
            
            if tbl == tblView {
                
                tblView.layer.removeAllAnimations()
                tblViewHT.constant = tblView.contentSize.height
            }
        }
        
        UIView.animate(withDuration: 0.1) {
            
            self.updateViewConstraints()
            self.loadViewIfNeeded()
        }
    }
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        
        return true
    }
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func showGender() {
        
        let actionsheet = UIAlertController(title: nil, message: nil, preferredStyle: IS_IPAD ? .alert : .actionSheet)
        
        for gen in arrGender {
            
            var genderTitle = ""
            let value=UserDefaults.standard.value(forKey: "AppleLanguages") as! [String]
            if value[0] == "ar" {
                genderTitle = (gen.title_ar ?? "").uppercased()
            } else {
                genderTitle = (gen.title ?? "").uppercased()
            }
            actionsheet.addAction(UIAlertAction(title: genderTitle, style: UIAlertAction.Style.default,handler: {
                action -> Void in
                
                UserDefaults.standard.setValue(gen.id, forKey: KEY_GENDER)
                self.tblView.reloadData()
                
            }))
        }
        actionsheet.addAction(UIAlertAction(title: APP_LBL().cancel, style: UIAlertAction.Style.cancel, handler: {
            action -> Void in
        }))
        self.present(actionsheet, animated: true, completion: nil)
    }
    
    func getStoreList() {
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);
        
        API().callAPI(endPoint: "mobiconnectstore/getlist", method: .GET, param: [:]) { (json_res, err) in
            
            //  DispatchQueue.main.async {
            
            cedMageLoaders.removeLoadingIndicator(me: self);
            
            if err == nil {
                
                let json = json_res[0]
                
                self.Stores.removeAll()
                
                for store in json["store_data"].arrayValue{
                    
                    let group_id = store["group_id"].stringValue
                    
                    let code = store["code"].stringValue
                    let store_id = store["store_id"].stringValue
                    let name = store["name"].stringValue
                    let is_active = store["is_active"].stringValue
                    let storeobject = ["group_id":group_id,"code":code,"store_id":store_id,"name":name,"is_active":is_active]
                    self.Stores.append(storeobject)
                }
                
                self.showStores()
            }
            //   }
        }
    }
    
    func showStores() {
        let actionsheet = UIAlertController(title: APP_LBL().select_store, message: nil, preferredStyle: IS_IPAD ? .alert : .actionSheet)
        
        for buttons in self.Stores {
            
            actionsheet.addAction(UIAlertAction(title: buttons["name"], style: UIAlertAction.Style.default,handler: {
                action -> Void in
                self.selectStore(store: buttons["store_id"])
            }))
        }
        actionsheet.addAction(UIAlertAction(title: APP_LBL().cancel, style: UIAlertAction.Style.cancel, handler: {
            action -> Void in
        }))
        if(UIDevice().model.lowercased() == "iPad".lowercased()){
            // actionsheet.popoverPresentationController?.sourceView = self.mainTable.cellForRow(at: IndexPath(row: 5, section: 1))
            
        }
        self.present(actionsheet, animated: true, completion: nil)
    }
    
    func selectStore(store:String?){
        //let params = ["store_id":store! as String]
        UserDefaults.standard.setValue(store, forKey: "storeId")
        self.setStore(url: "mobiconnectstore/setstore/"+store!)
    }
    
    func setStore(url: String) {
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);
        
        API().callAPI(endPoint: url, method: .GET, param: [:]) { (json_res, err) in
            
            //    DispatchQueue.main.async {
            
            cedMageLoaders.removeLoadingIndicator(me: self);
            
            if err == nil {
                
                let lang = json_res[0]["locale_code"].stringValue
                
                let language=lang.components(separatedBy: "_")
                if language[0]=="ar"
                {
                    UserDefaults.standard.set(["ar","en","fr"], forKey: "AppleLanguages")
                    UIView.appearance().semanticContentAttribute = .forceRightToLeft
                }
                else if language[0]=="en"
                {
                    UserDefaults.standard.set(["en","ar","fr"], forKey: "AppleLanguages")
                    UIView.appearance().semanticContentAttribute = .forceLeftToRight
                }
                
                APP_DEL.changeLanguage()
            }
            // }
        }
    }
}

extension NejreeSettingVC {
    
    func setUI() {
        
        self.btnTitle.text = APP_LBL().settings.uppercased()
        
        if APP_DEL.selectedLanguage == Arabic{
            imgBackBG.image = UIImage(named: "icon_back_white_Arabic")
        } else {
            imgBackBG.image = UIImage(named: "icon_back_white_round")
        }
        
        tblView.register(UINib(nibName: "NejreeSettingCell", bundle: nil), forCellReuseIdentifier: "NejreeSettingCell")
        tblView.rowHeight = UITableView.automaticDimension
        tblView.estimatedRowHeight = UITableView.automaticDimension
        tblView.dataSource = self
        tblView.delegate = self
        tblView.reloadData()
        
        tblView.round(redius: 6)
        tblView.layer.borderColor = UIColor.lightGray.cgColor
        tblView.layer.borderWidth = 0.5
    }
}

extension NejreeSettingVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "NejreeSettingCell") as! NejreeSettingCell
        cell.selectionStyle = .none
        
        if indexPath.row == 0 {
            
            cell.lblTitle.text = APP_LBL().country
            cell.lblValue.text = APP_DEL.selectedCountry.getCountryName().uppercased()
            
        } else if indexPath.row == 1 {
            
            cell.lblTitle.text = APP_LBL().gender
            
            var genderTitle = ""
            if let genderID =  UserDefaults.standard.object(forKey: KEY_GENDER) as? String {
                let temp = arrGender.first { (gen) -> Bool in
                    return (gen.id == genderID)
                }
                if let final = temp {
                    
                    if APP_DEL.selectedLanguage == Arabic{
                        genderTitle = (final.title_ar ?? "").uppercased()
                    } else {
                        genderTitle = (final.title ?? "").uppercased()
                    }
                }
            }

            cell.lblValue.text = genderTitle
            
        } else if indexPath.row == 2 {
            
            cell.lblTitle.text = APP_LBL().language
            
            if APP_DEL.selectedLanguage == Arabic{
                cell.lblValue.text = APP_LBL().english_locale.uppercased()
            }
            else {
                cell.lblValue.text = APP_LBL().arabic_locale.uppercased()
            }
        }

        
        cell.lblDevider.isHidden = (indexPath.row == (tableView.numberOfRows(inSection: 0) - 1))
        
        
        if APP_DEL.selectedLanguage == Arabic {
            cell.lblTitle.textAlignment = .right
            cell.lblValue.textAlignment = .left
            cell.imgArrow.image = UIImage(named: "IQButtonBarArrowLeft")
        } else {
            cell.lblTitle.textAlignment = .left
            cell.lblValue.textAlignment = .right
            cell.imgArrow.image = UIImage(named: "IQButtonBarArrowRight")
        }
        
        return cell;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let test = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: Constants().SCREEN_NAME_NejreeCountryLangSelectVC) as! NejreeCountryLangSelectVC
        test.isFromSplash = false
        test.hidesBottomBarWhenPushed = true
        self.navigationController?.pushToViewController(test, animated: true, completion: {
            
        })
//        if indexPath.row == 0 {
//            
//            let test = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: Constants().SCREEN_NAME_NejreeCountryLangSelectVC) as! NejreeCountryLangSelectVC
//            test.isFromSplash = false
//            self.navigationController?.pushToViewController(test, animated: true, completion: {
//                
//            })
//            
//        } else if indexPath.row == 1 {
//            let test = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: Constants().SCREEN_NAME_NejreeCountryLangSelectVC) as! NejreeCountryLangSelectVC
//            test.isFromSplash = false
//            self.navigationController?.pushToViewController(test, animated: true, completion: {
//                
//            })
////            return
////            self.showGender()
//            
//        } else if indexPath.row == 2 {
//            let test = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: Constants().SCREEN_NAME_NejreeCountryLangSelectVC) as! NejreeCountryLangSelectVC
//            test.isFromSplash = false
//            self.navigationController?.pushToViewController(test, animated: true, completion: {
//                
//            })
////            return
////            self.getStoreList()
//        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
}
