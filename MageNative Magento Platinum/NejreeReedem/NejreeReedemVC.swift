//
//  NejreeReedemVC.swift
//  MageNative Magento Platinum
//
//  Created by vishal.n on 10/07/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class NejreeReedemVC: UIViewController, SelectVoucherOption, UIGestureRecognizerDelegate {

    

    @IBOutlet weak var imgHeader: UIImageView!
    
    @IBOutlet weak var lblTitleYourStoreCredit: UILabel!
    @IBOutlet weak var lblYourStoreCredit: UILabel!
    
    @IBOutlet weak var lblRedeem: UILabel!
    
    @IBOutlet weak var lblTitleReedemYourVoucher: UILabel!
    @IBOutlet weak var viewDot_1_1: UIView!
    @IBOutlet weak var viewDot_1_2: UIView!
    
    @IBOutlet weak var viewName: UIView!
    @IBOutlet weak var txtVoucher: UITextField!
    
    @IBOutlet weak var btnApply: UIButton!
    @IBOutlet weak var btnBack: UIButton!
    
    var store_credit = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        APP_DEL.isRedeemFromPushNotification = false
        setUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        if navigationController?.responds(to: #selector(getter: UINavigationController.interactivePopGestureRecognizer)) ?? false {
                   
                   self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
                   self.navigationController?.interactivePopGestureRecognizer?.delegate = self
               }
    }
   
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
           
           return true
       }
    
    func setUI() {
        
        
        viewDot_1_1.round()
        viewDot_1_2.round()
    
        
        
        lblRedeem.text = APP_LBL().redeem.uppercased()
        lblRedeem.font = UIFont(name: "Cairo-Bold", size: 25)!
        lblRedeem.textAlignment = .left
        
        lblTitleYourStoreCredit.text = APP_LBL().store_credit.uppercased()
        lblTitleYourStoreCredit.font = UIFont(name: "Cairo-Regular", size: 16)!
        
        lblTitleReedemYourVoucher.text = APP_LBL().redeem_voucher_code.uppercased()
        lblTitleReedemYourVoucher.font = UIFont(name: "Cairo-Regular", size: 17)!
        
        lblYourStoreCredit.text = self.store_credit
        lblYourStoreCredit.font = UIFont(name: "Cairo-Regular", size: 16)!
        
        txtVoucher.attributedPlaceholder = NSAttributedString(string: APP_LBL().your_redeem_voucher.uppercased(),
        attributes: [NSAttributedString.Key.foregroundColor: UIColor.black])
        txtVoucher.font = UIFont(name: "Cairo-Regular", size: 14)!
        txtVoucher.delegate = self
        txtVoucher.keyboardType = .default
                
        let value=UserDefaults.standard.value(forKey: "AppleLanguages") as! [String]
        if value[0] == "ar" {
            txtVoucher.textAlignment = .right
        } else {
            txtVoucher.textAlignment = .left
        }
        
        viewName.setBorderBlack()
        viewName.setCornerRadius()
        
        btnApply.setTitle(APP_LBL().apply.uppercased(), for: .normal)
        btnApply.titleLabel?.font = UIFont(name: "Cairo-Regular", size: 17)!
        
        
       
        if value[0] == "ar" {
          
            
            btnBack.setImage(UIImage(named: "icon_back_white_Arabic"), for: .normal)
            
        } else {
            
            
            btnBack.setImage(UIImage(named: "icon_back_white_round"), for: .normal)
            
        }
         
        if APP_DEL.autoRedeemCode != ""{
            txtVoucher.text = APP_DEL.autoRedeemCode;
            APP_DEL.autoRedeemCode = ""
        }
        
    }
    
    @IBAction func btnBackAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnApplyAction(_ sender: Any) {
        
        self.view.endEditing(true)
        
        if txtVoucher.text == "" {
            self.view.makeToast(APP_LBL().please_enter_redeem_voucher_code, duration: 1.0, position: .center)
            return
        }

        var postData = [String:String]()
        postData["redeemecode"] = self.txtVoucher.text ?? ""
        
        if (UserDefaults.standard.object(forKey: "userInfoDict") != nil) {
            
            let userInfoDict = UserDefaults.standard.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
            postData["customer_id"] = userInfoDict["customerId"]!;
        }

        cedMageLoaders.addDefaultLoader(me: self);
        cedMageLoaders.removeLoadingIndicator(me: self);
        
        API().callAPI(endPoint: "mobiconnect/checkout/redeem", method: .POST, param: postData) { (json_res, err) in
            
            DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                if err == nil {
                    
                    if json_res[0]["code"] == "1" {

                        let vc = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "NejreeVoucherPopupVC") as! NejreeVoucherPopupVC
                        vc.delegateSelectVoucherOption = self
                        vc.status = true
                        vc.modalPresentationStyle = .overFullScreen
                        vc.modalTransitionStyle = .crossDissolve
                        self.present(vc, animated: true, completion: nil)

                        NotificationCenter.default.post(name: NSNotification.Name("RefreshAccount"), object: nil)
                        
                        self.updateStoreCredit()
                        
                    } else {

                        let vc = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "NejreeVoucherPopupVC") as! NejreeVoucherPopupVC
                        vc.delegateSelectVoucherOption = self
                        vc.status = false
                        vc.modalPresentationStyle = .overFullScreen
                        vc.modalTransitionStyle = .crossDissolve
                        self.present(vc, animated: true, completion: nil)
                        
                    }
                }
            }
        }
    }
    
    func updateStoreCredit() {
        
        var params = Dictionary<String,String>()
        
        if (UserDefaults.standard.bool(forKey: "isLogin")) {
            
            let userInfoDict = UserDefaults.standard.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
            params["customer_id"] = userInfoDict["customerId"]!;
            params["currency_code"] = "SAR";
        }
        
        if let storeID =  UserDefaults.standard.value(forKey: "storeId") as? String {
            params["store_id"] = storeID
        }
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);
        
        API().callAPI(endPoint: "mobiconnect/storecredit", method: .POST, param: params) { (json_res, err) in
            
            DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                if err == nil {
                    
                    let json = json_res[0]
                    
                    if (json["data"]["status"].stringValue == "true") {
                        
                        storeCreditAmount = json["data"]["store_credit"].stringValue;
                        self.lblYourStoreCredit.text = storeCreditAmount
                    }
                }
            }
        }
    }
    
    func selectVoucherOption(status: Bool, startShopping: Bool, addAnotherCode: Bool) {
        
        if startShopping {
           
            self.navigationController?.popToRootViewController(animated: true)
            self.tabBarController?.selectedIndex = 2
            
            self.tabBarController?.tabBar.isHidden = false
        } else if addAnotherCode {
            
            self.txtVoucher.text = ""
        }
    }
}


extension NejreeReedemVC: UITextFieldDelegate {
    
}
