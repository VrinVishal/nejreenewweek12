//
//  NejreeVoucherPopupVC.swift
//  MageNative Magento Platinum
//
//  Created by vishal.n on 10/07/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

protocol SelectVoucherOption {
    func selectVoucherOption(status: Bool, startShopping: Bool, addAnotherCode: Bool)
}

class NejreeVoucherPopupVC: UIViewController {

    @IBOutlet weak var imgStatus: UIImageView!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var btnStartShopping: UIButton!
    @IBOutlet weak var btnAddAnotherCode: UIButton!
    
    var status: Bool = false
    var delegateSelectVoucherOption: SelectVoucherOption?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setUI()
    }
    
    func setUI() {
        
        imgStatus.superview?.round(redius: 5.0)
        
        if self.status {
           
            imgStatus.image = UIImage(named: "status_true")
            lblDescription.text = APP_LBL().thanks_your_coucher_code
            
        } else {
            
            imgStatus.image = UIImage(named: "status_false")
            lblDescription.text = APP_LBL().sorry_the_voucher_code_is_incorrect
            
        }
        
        lblDescription.font = UIFont(name: "Cairo-Regular", size: 15)!
        
        btnStartShopping.setTitle(APP_LBL().start_shopping.uppercased(), for: .normal)
        btnStartShopping.titleLabel?.font = UIFont(name: "Cairo-Regular", size: 16)!
        
        btnAddAnotherCode.setTitle(APP_LBL().add_another_code.uppercased(), for: .normal)
        btnAddAnotherCode.titleLabel?.font = UIFont(name: "Cairo-Regular", size: 16)!
    }

    @IBAction func btnStartShoppingAction(_ sender: Any) {
        
        self.delegateSelectVoucherOption?.selectVoucherOption(status: self.status, startShopping: true, addAnotherCode: false)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnAddAnotherCodeAction(_ sender: Any) {
        
        self.delegateSelectVoucherOption?.selectVoucherOption(status: self.status, startShopping: false, addAnotherCode: true)
        self.dismiss(animated: true, completion: nil)
    }

}
