//
//  AddToCartPopupVC.swift
//  MageNative Magento Platinum
//
//  Created by vishal.n on 28/09/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

protocol AddToCartPopupWish {
    
    func addToCartPopupWish(res: Bool, dataToPost: [String:String])
}

class AddToCartPopupVC: UIViewController {
    
    
    @IBOutlet weak var viewPopup: UIView!
    @IBOutlet weak var offerLabel: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblRegularPrice: UILabel!
    @IBOutlet weak var imageCollection: UICollectionView!
    @IBOutlet weak var pageControlView: UIPageControl!
    @IBOutlet weak var lblProductName : UILabel!
    @IBOutlet weak var variationCollection: UICollectionView!
    @IBOutlet weak var lblSelectASize : UILabel!
    @IBOutlet weak var constrantCollectionHeight: NSLayoutConstraint!
    @IBOutlet weak var addToCartButton: UIButton!
    
    var offerAmount = ""
    var imagedata = [String]()
    var productData = productViewData()
    var attributeCode = String()
    var variationData = [String]()
    var variationKeys = [String]()
     var variationRemainingQty = [String]()
    
    
    var selectedVariation = String()
    var isSelected = false
    var deleagateAddToCartPopupWish: AddToCartPopupWish?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        variationCollection.delegate = self
        variationCollection.dataSource = self
        
        
        imageCollection.delegate = self
        imageCollection.dataSource = self
        imageCollection.isPagingEnabled = true
        pageControlView.numberOfPages = imagedata.count
        
        if #available(iOS 9, *) {
            
        }
        
//        pageControlView.customPageControl(dotFillColor: UIColor.init(hexString: "#fcb215")!, dotBorderColor: UIColor.init(hexString: "#fcb215")!, dotBorderWidth: 1)
        
        viewPopup.round(redius: 10)
        
        
        variationCollection.delegate = self
        variationCollection.dataSource = self
        
        lblSelectASize.text = "Select Size"//APP_LBL().select_a_size.uppercased()
        
        variationCollection.reloadData()
        self.view.layoutIfNeeded()
        constrantCollectionHeight.constant = variationCollection.contentSize.height
        self.view.layoutIfNeeded()
        variationCollection.reloadData()
        
        
        let value=UserDefaults.standard.value(forKey: "AppleLanguages") as! [String]
        if value[0]=="ar" {
            addToCartButton.titleLabel!.font  = UIFont(name: "Cairo-Regular", size: 14)!
        }
        
        addToCartButton.setTitleColor(.white, for: .normal)
        addToCartButton.setTitle(APP_LBL().add_to_my_box.uppercased(), for: .normal)
        addToCartButton.layer.cornerRadius = 0.0
        addToCartButton.clipsToBounds = true
        
        lblProductName.text = productData.product_name?.uppercased()
        if value[0] == "ar" {
            lblProductName.textAlignment = .right
        } else {
            lblProductName.textAlignment = .left
        }
                
        updateCartBtn()
        
    }
    
    
    func updateCartBtn() {
        
        if isSelected
        {
            addToCartButton.backgroundColor = UIColor.black;
        }
        else {
            addToCartButton.backgroundColor = UIColor.lightGray
        }
        
        
        offerLabel.round(redius: 5)
        
        if (APP_DEL.isDiscountTagEnable == "0"){
           offerLabel.isHidden = true
        }
        else
        {

            let offerString = offerAmount
            
            if offerString != "" && offerString != "0" {
                
//                let offer =
//                """
//                \(offerString) %
//                \(APP_LBL().off.uppercased())
//                """
                
                let offer = " \(offerString)% \(APP_LBL().off.uppercased()) "
                
                
                offerLabel.text = offer
                offerLabel.isHidden = false
            } else {
                offerLabel.isHidden = true
            }
        }
        
       // offerLabel.round(redius: offerLabel.frame.size.width/2)
        
        let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string:productData.regular_price!  + " " + productData.currency_symbol!)
        attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 1, range: NSMakeRange(0, attributeString.length))
        
       
        lblRegularPrice.isHidden = true
        if productData.special_price == "no_special" || productData.special_price == "" || productData.special_price == productData.regular_price {
            
            lblPrice.text = productData.regular_price!  + " " + productData.currency_symbol!
            lblRegularPrice.isHidden = true
            
        } else {
            
            
            lblPrice.text = productData.special_price!  + " " + productData.currency_symbol!
            lblRegularPrice.isHidden = false
            lblRegularPrice.attributedText = attributeString
        }
        
        
        
    }
    
    
    func getSelectedAttribute(from: Int, isSelected: Bool) {
        self.selectedVariation = variationKeys[from]
        
        let selectedData = variationData[from]

        self.isSelected = isSelected

        
        self.updateCartBtn()
    }
    
    @IBAction func btnCloseTapped(_ sender: UIButton) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func addToCartTapped(_ sender: UIButton) {
        
        if productData.stock == "OUT OF STOCK" {
            
            self.view.makeToast(APP_LBL().this_product_is_out_of_stock, duration: 1.0, position: .center)
            return
        }
        
        if isSelected == false {
            
            let msg = APP_LBL().please_select_product_size
            self.view.makeToast(msg, duration: 2, position: .center, title: nil, image: nil, style: nil, completion: {
                Void in
                
            })
            
            return;
        }
        
        var super_attribute = "{";
        super_attribute += "\""+attributeCode+"\":";
        super_attribute += "\""+selectedVariation+"\",";
        
        if (super_attribute.last! == ",") {
            
            super_attribute = super_attribute.substring(to: super_attribute.index(before: super_attribute.endIndex));
        }
        super_attribute += "}";
        
        self.deleagateAddToCartPopupWish?.addToCartPopupWish(res: true, dataToPost: ["type":self.productData.type!, "product_id":self.productData.product_id!, "super_attribute":super_attribute, "qty":"1","store_id":(UserDefaults.standard.value(forKey: "storeId") as! String?)!])
        
        self.dismiss(animated: true, completion: nil)
    }
}


extension AddToCartPopupVC: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == self.imageCollection {
            return imagedata.count
        } else {
            return variationData.count
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == self.imageCollection {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imageCollectionCell", for: indexPath) as! imageCollectionCell
            
            cell.productImage.sd_setImage(with: URL(string: imagedata[indexPath.row]), placeholderImage:nil)
            cell.productImage.contentMode = .scaleAspectFit
            
            return cell
            
        } else {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "variationCollectionCell", for: indexPath) as! variationCollectionCell
            cell.insideView.backgroundColor = .white
            cell.sizeLabel.text = variationData[indexPath.item]
            cell.sizeLabel.textColor = UIColor.black
            
            cell.lblRemainingQty.backgroundColor = UIColor.init(hexString: "#e84338")
            
            
            if variationRemainingQty[indexPath.item] != ""{
                cell.lblRemainingQty.isHidden = false
                
                cell.lblRemainingQty.text = variationRemainingQty[indexPath.item] + " " + APP_LBL().qty_left
                cell.lblRemainingQty.backgroundColor = UIColor.init(hexString: "#e84338")
                
                if cell.isSelected {
                 
                    cell.lblRemainingQty.isHidden = true
                }
                else
                 {
                    cell.lblRemainingQty.isHidden = false
                }
                
            }
            else
            {
                
                cell.lblRemainingQty.backgroundColor = .clear
                cell.lblRemainingQty.text = ""
                cell.lblRemainingQty.isHidden = true
            }
            
            
            return cell
            
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == self.imageCollection {
            return CGSize(width: SCREEN_WIDTH - 120, height: 200.0)
        } else {
            return CGSize(width: ((self.variationCollection.frame.size.width/4.0)), height: 45)
        }
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        
        if collectionView == self.imageCollection {
            
            //        let vc = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "zoomInController") as! zoomInController
            //        vc.images = self.imagedata
            //        parent.present(vc, animated: true)
            
        } else {
            
            self.getSelectedAttribute(from: indexPath.item, isSelected: true)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        
        if collectionView == self.imageCollection {
            
        } else {
            
            if let cell = collectionView.cellForItem(at: indexPath) as? variationCollectionCell {
                
                if cell.isSelected {

                    collectionView.deselectItem(at: indexPath, animated: true)
                    self.getSelectedAttribute(from: indexPath.item, isSelected: false)
                    return false
                }
            }
            
            return true
        }
        
        return true
    }
    
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        if collectionView == self.imageCollection {
            self.pageControlView.currentPage = indexPath.section
        } else {
            
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {

        if self.imageCollection == scrollView {
            let scrollPos = scrollView.contentOffset.x / self.imageCollection.frame.width
            self.pageControlView.currentPage = Int(scrollPos)
        } else {
            
        }
    }
}
