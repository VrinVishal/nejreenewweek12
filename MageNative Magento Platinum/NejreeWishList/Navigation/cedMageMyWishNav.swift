/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */

import UIKit

class cedMageMyWishNav: homedefaultNavigation {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        
        let defaults = UserDefaults.standard
        

        self.popToRootViewController(animated: false)
        if defaults.bool(forKey: "isLogin") == false {
            
            
            
            //  self.navigationController?.navigationBar.hidden = true
            let story = UIStoryboard(name: "cedMageLogin", bundle: nil)
            let vc = story.instantiateViewController(withIdentifier: "nejreeLoginController") as! nejreeLoginController
            let m = [vc]
            APP_DEL.isLoginFromWishList = true
            self.setViewControllers(m, animated: false)
            
            
        }
        else
        {
             //APP_DEL.isWishListShouldReload = true
            
            if APP_DEL.isWishListShouldReload == true{
                
                let newStory = UIStoryboard(name: "Main", bundle: nil)
                let vc = newStory.instantiateViewController(withIdentifier: "NejreeWishListVC") as! NejreeWishListVC
                let m = [vc]
                self.setViewControllers(m, animated: false)
    
            }
        }
//        else  if defaults.bool(forKey: "isLogin") == true {
//            let newStory = UIStoryboard(name: "Main", bundle: nil)
//            let vc = newStory.instantiateViewController(withIdentifier: "NejreeWishListVC") as! NejreeWishListVC
//            let m = [vc]
//            self.setViewControllers(m, animated: false)
//        }
        
    
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
