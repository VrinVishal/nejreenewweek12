//
//  NejreeWishListCell.swift
//  MageNative Magento Platinum
//
//  Created by Jayesh Dabhi on 18/03/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class NejreeWishListCell: UITableViewCell {

    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productName: UILabel!
    //@IBOutlet weak var quantityLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var outOfStockLabel: UILabel!
    
    @IBOutlet weak var imgWish: UIImageView!
    @IBOutlet weak var btnWish: UIButton!
    
    @IBOutlet weak var imgDelete: UIImageView!
    @IBOutlet weak var btnDelete: UIButton!
    
    @IBOutlet weak var btnAddToCart: UIButton!
    
    @IBOutlet weak var sizeValue: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
