//
//  NejreeWishListVC.swift
//  MageNative Magento Platinum
//
//  Created by Jayesh Dabhi on 18/03/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit
import FirebaseAnalytics
import Firebase
import FirebaseMessaging
import FirebaseDynamicLinks
import Foundation
import FirebaseAuth
import FirebaseCore

struct WishProductModel : Codable {
    
    let wishlist_item_id : String?
    let wishlist_id : String?
    let product_id : String?
    let store_id : String?
    let added_at : String?
    let description : String?
    let qty : Int?
    let product : WishProduct?

    enum CodingKeys: String, CodingKey {

        case wishlist_item_id = "wishlist_item_id"
        case wishlist_id = "wishlist_id"
        case product_id = "product_id"
        case store_id = "store_id"
        case added_at = "added_at"
        case description = "description"
        case qty = "qty"
        case product = "product"
    }
}
struct WishProduct : Codable {
    
    let product_id : String?
    let product_name : String?
    let type : String?
    let description : String?
    let stock_status : String?
    let product_url : String?
    let product_image : String?
    let special_price : String?
    let regular_price : String?
    let brands_name : Bool?
    let product_size : OptionsSelected?

    enum CodingKeys: String, CodingKey {

        case product_id = "product_id"
        case product_name = "product_name"
        case type = "type"
        case description = "description"
        case stock_status = "stock_status"
        case product_url = "product_url"
        case product_image = "product_image"
        case special_price = "special_price"
        case regular_price = "regular_price"
        case brands_name = "brands_name"
        case product_size = "options_selected"
    }
}


struct OptionsSelected : Codable {

    let optionId : Int?
    let optional_value : String?
    let label : String?
    let value : String?

    enum CodingKeys: String, CodingKey {
        case optionId = "option_id"
        case optional_value = "option_value"
        case label = "label"
        case value = "value"
    }
}





class NejreeWishListVC: cedMageViewController, AddToCartPopupWish {

    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var lblEmptyList: UILabel!
    @IBOutlet weak var lblEmptyList_1: UILabel!
    @IBOutlet weak var btnStartShopping: UIButton!
    
    
    @IBOutlet weak var viewButtons: UIView!
     //var WishAddRemoveIndex = 0
    
    var cellID = "NejreeWishListCell"
    
//    var objWishProductModel : WishProductModel?
    var arrWish : [WishProductModel] = []
    
    var productData = productViewData()
    var productImageArray = [String]()
    var variationData = [String]()
    var variationKeys = [String]()
    var variationRemainingQty = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
    

        self.tblView.register(UINib(nibName: cellID, bundle: nil), forCellReuseIdentifier: cellID)

        
      
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    override func viewWillAppear(_ animated: Bool) {
        
         super.viewWillAppear(true)
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.tabBarController?.tabBar.isHidden = false
        
        self.setUI()
        self.tblView.delegate = self
        self.tblView.dataSource = self
        
        cedMageLoaders.addDefaultLoader(me: self);
        btnStartShopping.isHidden = true
        self.viewButtons.isHidden = true

        APICALL()
    }
    
    
    override func viewDidDisappear(_ animated: Bool) {
      
    }
    
    
    func APICALL() {
        
        let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
        
        getWishList(dataToPost: ["customer_id" : userInfoDict["customerId"]! , "store_id" : (UserDefaults.standard.value(forKey: "storeId") as! String?)!])
    }
    
    
    func getWishList(dataToPost:[String:String]) {
        
        var postData = [String:String]()
        if defaults.bool(forKey: "isLogin") {
            
            let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
            
            postData["hashkey"] = userInfoDict["hashKey"]
            postData["customer_id"] = userInfoDict["customerId"]
            postData["country_id"] = APP_DEL.selectedCountry.country_code ?? "SA"
            for (key,val) in dataToPost {
                postData[key] = val;
            }
        }
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);
        
        API().callAPI(endPoint: "wishlist/getwishlist", method: .POST, param: postData) { (json_res, err) in
            
            DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                if err == nil {

                    var status = String()
                    
                    let json = json_res[0]["data"][0]
                    
                    status = json["success"].stringValue
                    
                    if status == "true" {
                        
                        self.arrWish.removeAll()
                        
                        for prod in json["wishlist_array"].arrayValue {
                            
                            let temp = WishProductModel(wishlist_item_id: prod["wishlist_item_id"].string, wishlist_id: prod["wishlist_id"].string, product_id: prod["product_id"].string, store_id: prod["wishlist_item_id"].string, added_at: prod["added_at"].string, description: prod["description"].string, qty: prod["qty"].int, product: WishProduct(product_id: prod["product"]["product_id"].string, product_name: prod["product"]["product_name"].string, type: prod["product"]["type"].string, description: prod["product"]["description"].string, stock_status: prod["product"]["stock_status"].string, product_url: prod["product"]["product_url"].string, product_image: prod["product"]["product_image"].string, special_price: prod["product"]["special_price"].string, regular_price: prod["product"]["regular_price"].string, brands_name: prod["product"]["brands_name"].bool, product_size: OptionsSelected(optionId: prod["product"]["options_selected"][0]["option_id"].int, optional_value: prod["product"]["options_selected"][0]["option_value"].string, label: prod["product"]["options_selected"][0]["label"].string, value: prod["product"]["options_selected"][0]["value"].string)))
                            
                            
                            self.arrWish.append(temp)
                        }
                        self.tblView.reloadData()
                        
                        APP_DEL.isWishListShouldReload = false
                        
                    } else {
                        
                        self.view.isUserInteractionEnabled = true;
                        
                        self.arrWish.removeAll()
                        self.tblView.reloadData()
                    }
                                        
                    self.checkEmptyWishList()
                }
                
                APP_DEL.getCartCount()
            }
        }
    }
    
    
    func setUI() {
        
        self.headingLabel.text = APP_LBL().my_wishlist.uppercased();//APP_LBL().my_wishlist_c
        headingLabel.font  = UIFont(name: "Cairo-Bold", size: 21)!
        self.lblEmptyList.text = APP_LBL().there_is_no_sneakers_added_in_your_wishlist.uppercased()
        self.lblEmptyList_1.text = APP_LBL().what_are_you_waiting_for
        
         self.btnStartShopping.addTarget(self, action: #selector(btnStartShoppingTapped(_:)), for: UIControl.Event.touchUpInside)
        
        
        // self.btnStartShopping.addTarget(self, action: #selector(btnStartShoppingTapped(_:)), for: .touchUpInside)
        
        //self.btnStartShopping.addTarget(self, action: #selector(btnStartShoppingTapped(_:)), for: UIControl.Event.touchUpInside)
        self.btnStartShopping.backgroundColor = UIColor.black
        self.btnStartShopping.setBorder()
       // self.btnStartShopping.layer.cornerRadius = 5.0
        self.btnStartShopping.setTitleColor(.white, for: .normal)
        self.btnStartShopping.setTitle(APP_LBL().start_shopping, for: .normal)
        
        self.lblEmptyList.superview?.isHidden = true

    }
    
    
    
    @objc func btnStartShoppingTapped(_ sender: UIButton) {
        
        if (self.arrWish.count == 0) {
            print("START SHOPPING")
            
            self.tabBarController?.selectedIndex = 2;

        } else {
            print("ADD ALL TO MY BOX")
        }
    }
    
    func checkEmptyWishList() {
        
        if (self.arrWish.count == 0) {
            self.tblView.isHidden = true
            self.view.bringSubviewToFront(self.btnStartShopping)
            self.lblEmptyList.superview?.isHidden = false
            self.btnStartShopping.isHidden = false
            self.viewButtons.isHidden = false
            self.btnStartShopping.setTitle(APP_LBL().start_shopping, for: .normal)
        } else {
            self.viewButtons.isHidden = true

            self.tblView.isHidden = false
            self.lblEmptyList.superview?.isHidden = true
//            self.btnStartShopping.setTitle("ADD ALL TO MY BOX".localized, for: .normal)
            self.btnStartShopping.isHidden = true
        }
    }
    
    @objc func deleteButtonTapped(_ sender: UIButton) {
        let showTitle = APP_LBL().confirmation
        let showMsg = APP_LBL().are_you_sure_you_want_to_remove_this_product_from_wish_list
        
        let confirmationAlert = UIAlertController(title: showTitle, message: showMsg, preferredStyle: UIAlertController.Style.alert);
        
        confirmationAlert.addAction(UIAlertAction(title: APP_LBL().done_c, style: .default, handler: { (action: UIAlertAction!) in
            self.performDeleteAction(sender: sender)
        }));
        
        confirmationAlert.addAction(UIAlertAction(title: APP_LBL().cancel, style: .default, handler: { (action: UIAlertAction!) in
        }));
        
        present(confirmationAlert, animated: true, completion: nil)
    }
    
    
    func RemoveFromWishList(Index : Int, dataToPost:[String:String], isFromAddToCart: Bool = false) {
        
        var postData = [String:String]()
        if defaults.bool(forKey: "isLogin") {
            
            let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
            
            postData["hashkey"] = userInfoDict["hashKey"]
            postData["customer_id"] = userInfoDict["customerId"]
            
            for (key,val) in dataToPost {
                postData[key] = val;
            }
        }
        
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);
        
        API().callAPI(endPoint: "wishlist/removewishlist", method: .POST, param: postData) { (json_res, err) in
            
            DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                if err == nil {
                    
                    
                    self.arrWish.remove(at: Index)
                    self.tblView.reloadData()
                    self.checkEmptyWishList()
                    if isFromAddToCart {
                         
                        //self.tabBarController?.selectedIndex = 1
                    }
                }
                
                APP_DEL.getCartCount()
            }
        }
    }
    
    func performDeleteAction(sender: UIButton) {
        
        let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
        
        RemoveFromWishList(Index: sender.tag, dataToPost: ["customer_id" : userInfoDict["customerId"]! , "product_id" : arrWish[sender.tag].product_id ?? ""])
    }
    
    @objc func btnAddToCartAction(sender: UIButton) {
    
        removeIndexForAddToCart = sender.tag
        self.getProductDetail(product_id: arrWish[sender.tag].product_id ?? "")
    }
    
    var removeIndexForAddToCart = 0
    func addToCartPopupWish(res: Bool, dataToPost: [String:String]) {
        
        var postData = [String:String]()
        
        for (key,val) in dataToPost {
            
            postData[key] = val;
        }
        
        
        if defaults.bool(forKey: "isLogin") {
            
            if let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] {
                
                postData["hashkey"] = userInfoDict["hashKey"]
                postData["customer_id"] = userInfoDict["customerId"]
            }
        }
        
        if (defaults.object(forKey: "cartId") != nil) {
            
            let cart_id = defaults.object(forKey: "cartId") as! String;
            postData["cart_id"] = cart_id;
        }
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);
        
        API().callAPI(endPoint: "mobiconnect/checkout/add/", method: .POST, param: postData) { (json_res, err) in
            
            DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                if err == nil {
                    
                    let msg = json_res[0]["cart_id"]["message"].stringValue;
                    
                    if msg != "" {
                        
                        if (json_res[0]["cart_id"]["success"].stringValue == "true") {
                            
                            let name = self.productData.product_name?.components(separatedBy: "-")
                            
                            let msgEng = "\(name![0]) \(APP_LBL().has_been_added_to_the_cart)"
                            
                            self.view.makeToast(msgEng, duration: 1.0, position: .center)
                            
                            //self.strTotalQuantity = json_res[0]["cart_id"]["items_count"].stringValue
                            var productPrice = ""
                            if self.productData.special_price == "no_special" || self.productData.special_price == "" || self.productData.special_price == self.productData.regular_price {
                                productPrice = self.productData.regular_price!
                            } else {
                                productPrice = self.productData.special_price!
                            }
                            
                            Analytics.logEvent(AnalyticsEventAddToCart, parameters: [
                                AnalyticsParameterCurrency : APP_DEL.selectedCountry.getCurrencySymbol().uppercased(),
                                AnalyticsParameterItemCategory : "Shoes",
                                AnalyticsParameterItemID : dataToPost["product_id"],
                                AnalyticsParameterQuantity : "1",
                                AnalyticsParameterItemName : self.productData.product_name ?? "",
                                AnalyticsParameterPrice : self.productData.special_price ?? "",
                            ])
                            
                            let detail : [AnalyticKey:String] = [
                                .Currency : APP_DEL.selectedCountry.getCurrencySymbol().uppercased(),
                                .ItemCategory : "Shoes",
                                .ItemID : dataToPost["product_id"] ?? "",
                                .Quantity : "1",
                                .ItemName : self.productData.product_name ?? "",
                                .Price : self.productData.special_price ?? "",
                                .PriceWithoutCurrency : productPrice
                            ]
                            API().setEvent(eventName: .AddToCart, eventDetail: detail) { (json, err) in }
                            
                            let userInfoDict = self.defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
                            
                            self.RemoveFromWishList(Index: self.removeIndexForAddToCart, dataToPost: ["customer_id" : userInfoDict["customerId"]! , "product_id" : self.arrWish[self.removeIndexForAddToCart].product_id ?? ""], isFromAddToCart: true)
                            
                        } else {
                            
                            if msg.contains("limit"){
                                APP_DEL.showAlertWithOkButton(parentVC: self, strTitle: APP_LBL().error, strMsg: APP_LBL().you_can_not_add_more_than_5_quantity)
                            }
                            else{
                                let name = self.productData.product_name?.components(separatedBy: "-")
                                self.view.makeToast("\(APP_LBL().quantity_of) \(name![0]) \(APP_LBL().not_available)", duration: 1.0, position: .center)
                            }
                            
                        }
                    }
                    
                    if (json_res[0]["cart_id"]["success"].stringValue == "true") {
                        
                        self.defaults.set(json_res[0]["cart_id"]["cart_id"].stringValue, forKey: "cartId");
                        self.defaults.set(json_res[0]["cart_id"]["items_count"].stringValue, forKey: "items_count");
                        
                    } else {
                        
                        let msg = json_res[0]["message"].stringValue;
                        
                        if msg != "" {
                            self.view.makeToast(msg, duration: 1.0, position: .center)
                        }
                    }
                }
            }
        }
    }
    //MARK: PRODUCT DETAIL
    func getProductDetail(product_id: String) {
        
        var postData = [String:String]()

        if let storeID =  UserDefaults.standard.value(forKey: "storeId") as? String {
            postData["store_id"] = storeID
        }
        
        if let userInfoDict = UserDefaults.standard.object(forKey: "userInfoDict") as? [String: String] {
            
            postData["hashkey"] = userInfoDict["hashKey"]
            postData["customer_id"] = userInfoDict["customerId"]
        }
        
        postData["prodID"] = product_id
        
        postData["country_id"] = APP_DEL.selectedCountry.country_code ?? "SA"
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);
        
        API().callAPI(endPoint: "mobiconnect/catalog/productview/", method: .POST, param: postData) { (json_res, err) in
            
            DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                if err == nil {
                    
                    let json = json_res[0]
                    
                    self.productImageArray.removeAll()
                    var i = 1;
                    for image in json["data"]["product-image"].arrayValue {
                        
                        self.productImageArray.append(image["image\(i)"][0].stringValue);
                        i = i + 1;
                    }
                    
                    self.variationData.removeAll()
                    self.variationKeys.removeAll()
                    for data in json["data"]["config-data"]["attributes"][0]["options"].arrayValue {
                        
                        self.variationData.append(data["label"].stringValue)
                        self.variationKeys.append(data["id"].stringValue)
                        self.variationRemainingQty.append(data["remain_qty"].stringValue)
                    }
                    
                    
                                        
                    self.productData = productViewData.init(Inwishlist: json["data"]["Inwishlist"][0].stringValue,
                                                            status: json["data"]["status"].stringValue,
                                                            product_review: json["data"]["product_review"].stringValue,
                                                            short_description: json["data"]["short-description"][0].stringValue,
                                                            main_prod_img: json["data"]["main-prod-img"].stringValue,
                                                            currency_symbol: json["data"]["currency_symbol"].stringValue,
                                                            stock: json["data"]["stock"][0].stringValue,
                                                            type: json["data"]["type"][0].stringValue,
                                                            product_id: json["data"]["product-id"][0].stringValue,
                                                            product_name: json["data"]["product-name"][0].stringValue,
                                                            product_url: json["data"]["product-url"][0].stringValue,
                                                            review_count: json["data"]["review_count"][0].intValue,
                                                            has_review: json["data"]["has_review"].stringValue,
                                                            regular_price: json["data"]["price"][0]["regular_price"][0].stringValue,
                                                            special_price: json["data"]["price"][0]["special_price"][0].stringValue,
                                                            available: json["data"]["available"].stringValue,
                                                            review: json["data"]["review"][0].stringValue,
                                                            description: json["data"]["description"][0].stringValue)

                    var productPrice = ""
                    if self.productData.special_price == "no_special" || self.productData.special_price == "" || self.productData.special_price == self.productData.regular_price {
                        productPrice = self.productData.regular_price!
                    } else {
                        productPrice = self.productData.special_price!
                    }
                    
                    let CartData = ["ITEM_CATEGORY" : "",
                                    "ITEM_ID" : json["data"]["product-id"][0].stringValue,
                                    "ITEM_LOCATION_ID" : "",
                                    "ITEM_NAME" : json["data"]["product-name"][0].stringValue,
                                    "PRICE" : json["data"]["price"][0]["regular_price"][0].stringValue,
                                    "QUANTITY" : "1",
                                    "VALUE" : json["data"]["price"][0]["regular_price"][0].stringValue
                    ]
                    Analytics.logEvent("add_to_cart", parameters: CartData)
                    
                    let detail : [AnalyticKey:String] = [
                        .ItemCategory : "",
                        .ItemID : json["data"]["product-id"][0].stringValue,
                        .ItemLocationID : "",
                        .ItemName : json["data"]["product-name"][0].stringValue,
                        .Price : json["data"]["price"][0]["regular_price"][0].stringValue,
                        .Quantity : "1",
                        .Value : json["data"]["price"][0]["regular_price"][0].stringValue,
                        .PriceWithoutCurrency : productPrice
                    ]
                    API().setEvent(eventName: .AddToCart, eventDetail: detail) { (json, err) in }
                    
                    Analytics.logEvent(AnalyticsEventViewItem, parameters: [
                        AnalyticsParameterCurrency : APP_DEL.selectedCountry.getCurrencySymbol().uppercased(),
                        AnalyticsParameterItemID : product_id,
                        AnalyticsParameterValue : productPrice
                    ])
                    
                    let detail_1 : [AnalyticKey:String] = [
                        .Currency : APP_DEL.selectedCountry.getCurrencySymbol().uppercased(),
                        .ItemID : product_id,
                        .Value : productPrice,
                        .PriceWithoutCurrency : productPrice
                    ]
                    API().setEvent(eventName: .ViewItem, eventDetail: detail_1) { (json, err) in }
                    
                    if self.productData.stock == "OUT OF STOCK" {
                        
                        self.view.makeToast(APP_LBL().this_product_is_out_of_stock, duration: 1.0, position: .center)
                        return
                        
                    } else {
                        
                        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AddToCartPopupVC") as! AddToCartPopupVC
                        vc.modalPresentationStyle = .overFullScreen
                        vc.modalTransitionStyle = .crossDissolve
                        vc.deleagateAddToCartPopupWish = self
                        vc.offerAmount = json["data"]["offer"].stringValue
                        vc.attributeCode = json["data"]["config-data"]["attributes"][0]["id"].stringValue
                        vc.imagedata = self.productImageArray
                        vc.productData = self.productData
                        vc.variationRemainingQty = self.variationRemainingQty
                        vc.variationData = self.variationData
                        vc.variationKeys = self.variationKeys
                        
                        self.present(vc, animated: true, completion: nil)
                    }
                }
            }
        }
    }
}

extension NejreeWishListVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrWish.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tblView.dequeueReusableCell(withIdentifier: self.cellID) as! NejreeWishListCell
        cell.selectionStyle = .none
        
        
        cell.outerView.backgroundColor = .white
        
        let product = self.arrWish[indexPath.row]
        
//        cell.deleteButton.tag = Int(product.product_id ?? "0")!
//        cell.deleteButton.tag = indexPath.row
//        cell.deleteButton.addTarget(self, action: #selector(deleteButtonTapped(_:)), for: .touchUpInside)
        
        cell.productName.text = product.product?.product_name ?? ""
        
        let value=UserDefaults.standard.value(forKey: "AppleLanguages") as! [String]
                       
        if value[0] == "ar" {
            
            cell.productName.textAlignment = .right
        }
        else
        {
            cell.productName.textAlignment = .left
        }
        
        
        cell.productImage.sd_setImage(with: URL(string: product.product?.product_image ?? ""), placeholderImage:nil)
        cell.outerView.cardView()
        
       
        if (product.product?.stock_status == "true") {
            cell.outOfStockLabel.isHidden = false
        }else{
            cell.outOfStockLabel.isHidden = true
        }
        
        
        
        cell.priceLabel.text = (((product.product?.special_price ?? "") == "no_special") ? (product.product?.regular_price ?? "") : (product.product?.special_price ?? ""))
        
//        //cell.quantityLabel.text = "QUANTITY: ".localized + product.quantity!

        cell.outOfStockLabel.text = APP_LBL().product_is_out_of_stock
        
        
        cell.btnDelete.tag = indexPath.row
        cell.btnDelete.addTarget(self, action: #selector(deleteButtonTapped(_:)), for: .touchUpInside)
        
        cell.btnAddToCart.tag = indexPath.row
        cell.btnAddToCart.addTarget(self, action: #selector(self.btnAddToCartAction(sender:)), for: .touchUpInside)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let product = self.arrWish[indexPath.row]

        if product.product?.stock_status == "true"{
            self.view.makeToast(APP_LBL().product_is_out_of_stock, duration: 1.0, position: .center)
            return
        }else{
            let vc = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "productViewController") as! productViewController
            vc.product_id = product.product_id ?? ""
            APP_DEL.productIDglobal = product.product_id ?? ""
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        return true
    }
    
//    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
//
//
//        let swipeActionConfig = UISwipeActionsConfiguration(actions: [])
//        swipeActionConfig.performsFirstActionWithFullSwipe = false
//        return swipeActionConfig
//
//
//    }
    
    
    
//    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
//
//        print("Trailling")
//
//
//        let CartAction = UIContextualAction(style: .destructive, title: nil) { (action, sourceView, completionHandler) in
//            print("index path of delete: \(indexPath)")
//
//            self.WishAddRemoveIndex = indexPath.row
//
//            completionHandler(true)
//        }
//
//               let commentCell = tableView.cellForRow(at: indexPath)
//
//               let height = commentCell?.frame.size.height ?? 0.0
//
//        let backgroundImage = CartImg(forHeight: height, forIndex: indexPath.row)
//
//               if let backgroundImage = backgroundImage {
//                   CartAction.backgroundColor = UIColor(patternImage: backgroundImage)
//               }
//
//
//        let deleteAction = UIContextualAction(style: .destructive, title: nil) { (action, sourceView, completionHandler) in
//            print("index path of delete: \(indexPath)")
//
//                let btnDeleteTag = UIButton()
//                   btnDeleteTag.tag = indexPath.row
//
//                  // self.performDeleteAction(sender: btnDeleteTag)
//
//            self.deleteButtonTapped(btnDeleteTag)
//
//
//            completionHandler(true)
//        }
//
//
//                      let commentCell1 = tableView.cellForRow(at: indexPath)
//
//                      let height1 = commentCell1?.frame.size.height ?? 0.0
//
//                        deleteAction.image = UIImage(named: "MyCartNewTrash")!
//                        deleteAction.backgroundColor = UIColor.black
//
////                      let backgroundImage1 = deleteImage(forHeight: height1)
////
////                      if let backgroundImage1 = backgroundImage1 {
////                          deleteAction.backgroundColor = UIColor(patternImage: backgroundImage1)
////                      }
//
//                if defaults.bool(forKey: "isLogin") == true{
//
//
//                     //let swipeActionConfig = UISwipeActionsConfiguration(actions: [deleteAction,CartAction])
//                    let swipeActionConfig = UISwipeActionsConfiguration(actions: [deleteAction])
//                     swipeActionConfig.performsFirstActionWithFullSwipe = false
//
//                     return swipeActionConfig
//
//
//                }
//               else
//               {
//                   let swipeActionConfig = UISwipeActionsConfiguration(actions: [deleteAction])
//                   swipeActionConfig.performsFirstActionWithFullSwipe = false
//
//                   return swipeActionConfig
//               }
//
//
//
//
//
//
//
//           }
    

    func CartImg(forHeight height: CGFloat, forIndex : Int) -> UIImage? {
        var xPos = 0.0
        let value=UserDefaults.standard.value(forKey: "AppleLanguages") as! [String]
        if value[0] == "ar" {
            xPos = 275
        } else {
            xPos = 25
        }

           let frame = CGRect(x: 0, y: 0, width: 300, height: height)

           UIGraphicsBeginImageContextWithOptions(CGSize(width: 300, height: height), false, UIScreen.main.scale)

           let context = UIGraphicsGetCurrentContext()

           context?.setFillColor(UIColor(red: 250/255, green: 174/255, blue: 23/255, alpha: 1).cgColor)
           context?.fill(frame)

        var image = UIImage()
        
       
        image = UIImage(named: "MyCartNewSlide")!
        
        image.draw(in: CGRect(x:  xPos, y: (Double(frame.size.height) / 2.0) - 14, width: 25, height: 25))

           let newImage = UIGraphicsGetImageFromCurrentImageContext()

           UIGraphicsEndImageContext()

           return newImage
       }
    
    func deleteImage(forHeight height: CGFloat) -> UIImage? {
        var xPos = 0.0
        let value=UserDefaults.standard.value(forKey: "AppleLanguages") as! [String]
        if value[0] == "ar" {
            xPos = 275
        } else {
            xPos = 25
        }
        
        let frame = CGRect(x: 0, y: 0, width: 300, height: height)

        UIGraphicsBeginImageContextWithOptions(CGSize(width:300, height: height), false, UIScreen.main.scale)

        let context = UIGraphicsGetCurrentContext()

        context?.setFillColor(UIColor.black.cgColor)
        context?.fill(frame)

        let image = UIImage(named: "MyCartNewTrash")

        image?.draw(in: CGRect(x: xPos , y: (Double(frame.size.height) / 2.0) - 12.5, width: 25, height: 25))

        let newImage = UIGraphicsGetImageFromCurrentImageContext()

        UIGraphicsEndImageContext()

        return newImage
    }
    
}
