//
//  HomeBrandVC.swift
//  MageNative Magento Platinum
//
//  Created by Vrinsoft on 04/08/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit
import IQKeyboardManager
import FirebaseAnalytics
//import FirebaseRemoteConfig

struct CategoryProduct : Codable {
    
    let product_id : String?
    let product_name : String?
    let type : String?
    let description : String?
    //let inwishlist : String?
   // let wishlist_item_id : Int?
    let source_id : String?
    let stock_status : String?
    let product_url : String?
    let product_image : String?
    let brands_name : String?
    let offer : String?
    let regular_price : String?
    let special_price : String?
    let tag : String?

    enum CodingKeys: String, CodingKey {

        case product_id = "product_id"
        case product_name = "product_name"
        case type = "type"
        case description = "description"
       // case inwishlist = "Inwishlist"
        //case wishlist_item_id = "wishlist_item_id"
        case source_id = "source_id"
        case stock_status = "stock_status"
        case product_url = "product-url"
        case product_image = "product_image"
        case brands_name = "brands_name"
        case offer = "offer"
        case regular_price = "regular_price"
        case special_price = "special_price"
         case tag = "tag"
    }
}

class HomeBrandVC: UIViewController, FilterApply , UIScrollViewDelegate, DidSelectSortType {

    @IBOutlet weak var imgBack: UIImageView!
    
//    @IBOutlet weak var scrllView: UIScrollView!
//    @IBOutlet weak var mainView: UIView!
    
    @IBOutlet weak var viewTopCat: UIView!
    @IBOutlet weak var collSubCategory: UICollectionView!
    
    @IBOutlet weak var viewBanner: UIView!
    @IBOutlet weak var imgBanner: UIImageView!
    
    @IBOutlet weak var viewCatType: UIView!
    @IBOutlet weak var collSubCatType: UICollectionView!
    
    @IBOutlet weak var collectionProduct: UICollectionView!
   // @IBOutlet weak var constraintCollectionProductHeight: NSLayoutConstraint!
    @IBOutlet weak var lblNoData: UILabel!
    
    @IBOutlet weak var viewSearch: UIView!
    @IBOutlet weak var viewTxtSearch: UIView!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var viewNotification: UIView!
    @IBOutlet weak var lblNotificCount: UILabel!
    @IBOutlet weak var btnClear: UIButton!

    @IBOutlet weak var viewFilter: UIView!
    @IBOutlet weak var btnFilter: UIButton!
    
    @IBOutlet weak var viewSearchList: UIView!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var htTbl: NSLayoutConstraint!
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var constraintBannerHeight: NSLayoutConstraint!
    @IBOutlet weak var stackVw: UIStackView!
    
    @IBOutlet weak var viewSortFilter: UIView!
    @IBOutlet weak var lblSort: UILabel!
    @IBOutlet weak var lblFilter: UILabel!
    
    var isSearchOpen : Bool = false
    var isFilterApplied : Bool = false
    
    var strTitle = ""

    var categoryId = ""
    var categoryImage = ""
    var isChild = ""
    
    var searchText = ""
    var suggentionsArray = [[String:String]]()
    
    var filterString = ""
    var filterDT = FilterData()
    
    var currentPage = 1
    var loading = true;
    var noProductCheck = false
    
    var isCheckLBLNoData = false

    var products = [CategoryProduct]()
    var BannerHeight = CGFloat()


    var level_1 : [Filter] = []
    var level_2 : [Filter] = []
    var level_1_key = ""
    var level_2_key = ""
    
    var nejreeColor = UIColor.init(hexString: "#FBAD18")
    
    var filterLevel1 : [String:String] = [:]
    var filterLevel1Index = -1
    var filterLevel2 : [String:String] = [:]
    var filterLevel2Index = -1

    var arrFilter : [Filter] = []
     var activityIndi = UIActivityIndicatorView()
     @IBOutlet weak var lblAppliedFilter: UILabel!
    @IBOutlet weak var lblAppliedSortTop: UILabel!
    @IBOutlet weak var lblAppliedFilterTop: UILabel!
    @IBOutlet weak var searchLabel: UILabel!

    var arrSort: [SortType] = [SortType]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
                
       // scrllView.delegate = self
        setData()
        let TopGesture = UISwipeGestureRecognizer(target: self, action: #selector(holeSwiped))
        TopGesture.direction = .up
        self.stackVw.addGestureRecognizer(TopGesture)
        
        lblAppliedFilter.layer.cornerRadius = lblAppliedFilter.frame.size.height/2
        lblAppliedFilter.clipsToBounds = true
        self.lblAppliedFilter.isHidden = true
        
        lblAppliedFilterTop.layer.cornerRadius = lblAppliedFilterTop.frame.size.height/2
        lblAppliedFilterTop.clipsToBounds = true
        self.lblAppliedFilterTop.isHidden = true
        
        lblAppliedSortTop.layer.cornerRadius = lblAppliedSortTop.frame.size.height/2
        lblAppliedSortTop.clipsToBounds = true
        self.lblAppliedSortTop.isHidden = true
        
        
        
        let BottomGesture = UISwipeGestureRecognizer(target: self, action: #selector(holeSwiped))
        BottomGesture.direction = .down
        self.stackVw.addGestureRecognizer(BottomGesture)
        setUI()
        
        activityIndi.frame = CGRect(x: 0.0, y: 0.0, width: 40.0, height: 40.0)
        activityIndi.center = self.view.center
        activityIndi.hidesWhenStopped = true
        activityIndi.style =
            UIActivityIndicatorView.Style.gray
        //self.view.addSubview(activityIndi)
        // activityIndi.startAnimating()
        activityIndi.isHidden = true
        
        self.isFilterApplied = false
        self.getCategoryProduct()
        self.getFilter()
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardNotification(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardNotification(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        setupHideKeyboardOnTap()
        
        
        self.categoryImage = ""
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
           super.viewWillAppear(animated)
           
           self.navigationController?.setNavigationBarHidden(true, animated: true)
           
           IQKeyboardManager.shared().isEnabled = false
           IQKeyboardManager.shared().isEnableAutoToolbar = false
           
           self.tblView.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
       }
    
    var isKeyboardShowing = false
    @objc func handleKeyboardNotification(notification: NSNotification) {
        
        self.isKeyboardShowing = (notification.name == UIResponder.keyboardWillShowNotification)
        
        if self.isKeyboardShowing {
            
        } else {
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.closeSuggetionView()
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
         APP_DEL.strFilterWithDeeplink = ""
        
        IQKeyboardManager.shared().isEnabled = true
        IQKeyboardManager.shared().isEnableAutoToolbar = true
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?)
    {
        self.tblView.layer.removeAllAnimations()

        self.htTbl.constant = self.tblView.contentSize.height
        UIView.animate(withDuration: 0.5) {
            self.updateViewConstraints()
            self.loadViewIfNeeded()
        }
    }
    
    
   @objc func holeSwiped(gesture: UISwipeGestureRecognizer){
       if let swipeGesture = gesture as? UISwipeGestureRecognizer{
           switch swipeGesture.direction {
           case .down :
               print("Down swipe")
               UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.transitionCrossDissolve, animations: {
                   
                   self.constraintBannerHeight.constant = self.BannerHeight
                 
                   self.closeSuggetionView()
                
                   self.view.layoutIfNeeded()
                   
               })
               
           case .up :
               
               print("Up swipe")
               UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.transitionCrossDissolve, animations: {
                          
                   self.constraintBannerHeight.constant = 0

                    self.closeSuggetionView()
                
                   self.view.layoutIfNeeded()
                          
               })
           default:
               print("other swipe")
           }
       }

   }

    func closeSuggetionView() {
         self.isSearchOpen = false
        self.txtSearch.text = ""
        self.displayClear(res: false)
        self.searchText(text: "")
        self.view.endEditing(true)
        self.btnClearAction(self.btnClear)
    }
    
    func setUI() {
        searchLabel.setFont(fontFamily: "Cairo-Bold", fontSize: 22)
        
        if (strTitle == ""){
            searchLabel.text = APP_LBL().shop_in_shop_title.uppercased()
        } else {
            searchLabel.text = (strTitle ?? APP_LBL().shop_in_shop_title).uppercased()
        }

        if APP_DEL.selectedLanguage == Arabic{
            self.txtSearch.textAlignment = .right
        } else {
            self.txtSearch.textAlignment = .left
        }
        
        if APP_DEL.selectedLanguage == Arabic{
            imgBack.image = UIImage(named: "icon_back_white_Arabic")
        } else {
            imgBack.image = UIImage(named: "icon_back_white_round")
        }
        
        viewNotification.round(redius: 16)
        lblNotificCount.round(redius: 5)
        viewSearch.round(redius: 16)
        viewTxtSearch.round(redius: 16)
        viewTxtSearch.layer.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        viewTxtSearch.layer.borderWidth = 0.5
        viewTxtSearch.layer.shadowColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        viewTxtSearch.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        viewTxtSearch.layer.shadowRadius = 4
        viewTxtSearch.layer.shadowOpacity = 0.5
        viewTxtSearch.isHidden = true
        viewSearchList.round(redius: 16)
        viewFilter.round(redius: 16)
        
        //        viewSortFilter.round(redius: 6)
        viewSortFilter.properLightShadow()
//        viewSortFilter.layer.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
//        viewSortFilter.layer.borderWidth = 0.5
        
        lblSort.text = APP_LBL().sort.uppercased()
        lblSort.font = UIFont(name: "Cairo-Regular", size: 15)!
        lblFilter.text = APP_LBL().filter.uppercased()
        lblFilter.font = UIFont(name: "Cairo-Regular", size: 15)!
        
        self.tblView.rowHeight = UITableView.automaticDimension
        self.tblView.estimatedRowHeight = UITableView.automaticDimension
        self.tblView.register(UINib(nibName: "SearchProductCell", bundle: nil), forCellReuseIdentifier: "SearchProductCell")
        self.tblView.dataSource = self
        self.tblView.delegate = self
        self.tblView.reloadData()
        self.tblView.allowsSelection = true
        
        tblView.keyboardDismissMode = .onDrag
        self.txtSearch.delegate = self
        self.txtSearch.placeholder = APP_LBL().search
        self.txtSearch.autocorrectionType = .no
        self.displayClear(res: false)
        
        self.htTbl.constant = 4.0
        self.viewSearchList.isHidden = true
        self.tblView.isHidden = true
                
    }
    
    @IBAction func btnSortAction(_ sender: UIButton) {
           
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NejreeSortVC") as! NejreeSortVC
        vc.arrSort = self.arrSort
        vc.preSort = self.sortType
        vc.delegateDidSelectSortType = self
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func btnFilterAction(_ sender: Any) {
        
        self.viewFilter.isHidden = true
        self.lblAppliedFilter.isHidden = true
        self.lblAppliedFilterTop.isHidden = true
        
        
        
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NejreeCategoryFilterVC") as! NejreeCategoryFilterVC
        vc.hidesBottomBarWhenPushed = true
        vc.categoryId = self.categoryId
        vc.filterDT = self.filterDT
        vc.isCategoryFilter = false
        vc.arrFilter = self.arrFilter
        vc.modalPresentationStyle = .overFullScreen
        vc.delegateFilterApply = self
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func btnSearchBottomAction(_ sender: Any) {
        
        if let viewController =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "cedMageSearchPage") as?  cedMageSearchPage {
            
            viewController.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(viewController, animated: true)
            
        }
    }
    
    func filterApply(filter: String, filterDT: FilterData, isForShowFilterView: Bool) {
        
        //self.scrllView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        
        //self.viewFilter.isHidden = false
        self.lblAppliedFilter.isHidden = false
        self.lblAppliedFilterTop.isHidden = false
        
        self.isFilterApplied = true
        if self.filterString == ""{
             self.lblAppliedFilter.isHidden = true
            self.lblAppliedFilterTop.isHidden = true
        }
        else
         {
             self.lblAppliedFilter.isHidden = false
            self.lblAppliedFilterTop.isHidden = false
        }
        
        if isForShowFilterView {
            return;
        }
        
        self.filterDT = filterDT
        self.currentPage = 1
        self.filterString = filter
        self.noProductCheck = false
        
        
        if self.filterString == ""{
            
            self.isFilterApplied = false
           
        }
        else
        {
            self.isFilterApplied = true
        }
        
        self.preselectedOption()
    }
        
    func preselectedOption() {
        
        if self.filterDT.selected_level_1_code != "" {
            
            if self.level_1.count > 0 {
                
                let filt_cat = self.level_1[0].filter_data?.first(where: { (item_category) -> Bool in (item_category.filter_code == self.filterDT.selected_level_1_code)})
                if filt_cat != nil {
                    
                    let index_row = self.level_1[0].filter_data?.firstIndex { (item_row) -> Bool in
                        return (filt_cat!.filter_code == item_row.filter_code)
                    }
                    
                    if let tempIndexRow = index_row {
                        
                        self.filterLevel1Index = tempIndexRow
                        filterLevel1 = [(level_1[0].filter_data?[tempIndexRow].filter_code ?? ""):(level_1[0].filter_data?[tempIndexRow].filter_value ?? "")]
                    }
                }
            }
            
        } else {
            
            self.filterLevel1Index = -1
            filterLevel1 = [:]
        }
        
        
        if self.filterDT.selected_level_2_code != "" {
            
            if self.level_2.count > 0 {
                
                let filt_type = self.level_2[0].filter_data?.first(where: { (item_type) -> Bool in (item_type.filter_code == self.filterDT.selected_level_2_code)})
                if filt_type != nil {
                    
                    let index_row = self.level_2[0].filter_data?.firstIndex { (item_row) -> Bool in
                        return (filt_type!.filter_code == item_row.filter_code)
                    }
                    
                    if let tempIndexRow = index_row {
                        
                        self.filterLevel2Index = tempIndexRow
                        filterLevel2 = [(level_2[0].filter_data?[tempIndexRow].filter_code ?? ""):(level_2[0].filter_data?[tempIndexRow].filter_value ?? "")]
                    }
                }
            }
            
        } else {
            
            self.filterLevel2Index = -1
            filterLevel2 = [:]
        }

        self.getCategoryProduct()
    }
    
    var sortType: SortType? = nil
    func didSelectSortType(res: Bool, type: SortType) {
        self.currentPage = 1
        self.noProductCheck = false

        if res {
            
            sortType = type
            self.getCategoryProduct()
        }
        
        if ((sortType?.code ?? "") == "default") || ((sortType?.code ?? "") == "") {
            self.lblAppliedSortTop.isHidden = true
        } else {
            self.lblAppliedSortTop.isHidden = false
        }
    }
    
    @IBAction func actionBtnBack( _ sender : UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {

       if scrollView == self.collectionProduct {
            if self.isSearchOpen{
                
                self.txtSearch.text = ""
                self.searchText(text: "")
                self.view.endEditing(true)
                self.btnClearAction(self.btnClear)
                
            }
        }
        
        
//        if scrollView == self.collectionProduct || scrollView == self.collSubCategory || scrollView == self.collSubCatType || scrollView == self.collectionProduct {
//
//            self.txtSearch.text = ""
//            self.displayClear(res: false)
//            self.searchText(text: "")
//            self.view.endEditing(true)
//            self.btnClearAction(self.btnClear)
//        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        
        let offset = scrollView.contentOffset.y
        if offset > 1 {
            UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.transitionCrossDissolve, animations: {
                
                self.constraintBannerHeight.constant = 0

                
                self.view.layoutIfNeeded()
                
            })
           
            
        }else{
            UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.transitionCrossDissolve, animations: {
                           
                    self.constraintBannerHeight.constant = self.BannerHeight

                           
                    self.view.layoutIfNeeded()
                           
                })
            
        }
        
        
        self.collectionProduct.performBatchUpdates({
            collectionProduct.layoutIfNeeded()
        }, completion: nil)
        
        
        
        if scrollView == self.collectionProduct{
            if  scrollView.contentOffset.y + scrollView.frame.size.height >= (scrollView.contentSize.height - 500) {
            
            if noProductCheck == false && self.loading == false {
                self.loading = true
                self.checkPagination()
                }
            }
        }
        

        
    }
    
    


}







extension HomeBrandVC {
    
    func setData(){
        
        let nib1 = UINib(nibName: "SubCategoryCell", bundle: nil)
        let nib2 = UINib(nibName: "SubCategoryTypeCell", bundle: nil)
        let nib3 = UINib(nibName: "CatProductCell", bundle: nil)

        collSubCategory.register(nib1, forCellWithReuseIdentifier: "SubCategoryCell")
        collSubCatType.register(nib2, forCellWithReuseIdentifier: "SubCategoryTypeCell")
        collectionProduct.register(nib3, forCellWithReuseIdentifier: "CatProductCell")
        
    
       // scrllView.delegate = self
        
        collSubCatType.delegate = self
        collSubCatType.dataSource = self
        
        collSubCategory.delegate = self
        collSubCategory.dataSource = self
        
        collectionProduct.delegate = self
        collectionProduct.dataSource = self
        
         BannerHeight = constraintBannerHeight.constant
        
        RELOAD(isFromProduct: false)
        
       
    }
      
    func RELOAD(isFromProduct: Bool) {
           
           
        
        
           self.lblNoData.text = APP_LBL().no_product_found.uppercased()
           
           collectionProduct.reloadData()
           
           view.layoutIfNeeded()
           if self.products.count == 0 {
     
               self.lblNoData.isHidden = isFromProduct ? false : true
            self.viewSortFilter.isHidden = true

           } else {
            self.viewSortFilter.isHidden = false

               self.lblNoData.isHidden = true
           }
           view.layoutIfNeeded()
           
           collectionProduct.reloadData()
           
           
           
           
           if self.level_1.count == 0 {
               self.viewTopCat.isHidden = true
               //self.viewBanner.isHidden = true
           } else {
               self.viewTopCat.isHidden = false
               //self.viewBanner.isHidden = false
           }
           self.viewBanner.isHidden = false

           if self.level_2.count == 0 {
               self.viewCatType.isHidden = true
           } else {
               self.viewCatType.isHidden = false
           }

           self.collSubCategory.reloadData()
           self.collSubCatType.reloadData()
           
           self.updateBannerImage()
           
           self.view.layoutIfNeeded()
       }
    
//    func RELOAD() {
//
//        self.lblNoData.text = APP_LBL().no_data_found.uppercased()
//
//        collectionProduct.reloadData()
//
//        view.layoutIfNeeded()
//        if self.products.count == 0 {
//
//
//            if (self.level_1.count == 0) && (self.level_2.count == 0) {
//                constraintCollectionProductHeight.constant = (SCREEN_HEIGHT - 220.0)
//            } else {
//                constraintCollectionProductHeight.constant = 300.0
//            }
//
//            self.lblNoData.isHidden = isCheckLBLNoData ? false : true
//
//        } else {
//
//            constraintCollectionProductHeight.constant = collectionProduct.contentSize.height
//
//            self.lblNoData.isHidden = true
//        }
//        view.layoutIfNeeded()
//        collectionProduct.reloadData()
//
//        if self.level_1.count == 0 {
//            self.viewTopCat.isHidden = true
//            //self.viewBanner.isHidden = true
//        } else {
//            self.viewTopCat.isHidden = false
//            //self.viewBanner.isHidden = false
//        }
//        self.viewBanner.isHidden = false
//
//        if self.level_2.count == 0 {
//            self.viewCatType.isHidden = true
//        } else {
//            self.viewCatType.isHidden = false
//        }
//
//        self.collSubCategory.reloadData()
//        self.collSubCatType.reloadData()
//
//        self.updateBannerImage()
//
//        self.view.layoutIfNeeded()
//    }
    
    func updateBannerImage() {

        if (level_1.count > 0) && ((level_1[0].filter_data?.count ?? 0) > (self.filterLevel1Index)) && (self.filterLevel1Index > -1) {
            
            self.imgBanner!.sd_setImage(with: URL(string: level_1[0].filter_data?[self.filterLevel1Index].filter_img ?? ""), placeholderImage: nil)
        } else {
            
            self.imgBanner.sd_setImage(with: URL(string: self.categoryImage), placeholderImage: UIImage(named: "bannerplaceholder"))
        }
    }
    
    func updateFilterSTR() -> String {

        var tempDict : [String : [String : String]] = [:]
        
        if filterString != "" {
            
            let strData = filterString.data(using: String.Encoding.utf8)
            tempDict = try! JSONSerialization.jsonObject(with: strData!, options: []) as! [String : [String : String]]
        }
        
        
        if filterLevel1 != [:] {
            tempDict[self.level_1_key] = self.filterLevel1
        } else {
            
            if let index = tempDict.index(forKey: self.level_1_key) {
                tempDict.remove(at: index)
            }
        }
        
        if filterLevel2 != [:] {
            tempDict[self.level_2_key] = self.filterLevel2
        } else {
            
            if let index = tempDict.index(forKey: self.level_2_key) {
                tempDict.remove(at: index)
            }
        }
                        
        if tempDict == [:] {
            return ""
        }
        
        let data = try! JSONSerialization.data(withJSONObject: tempDict, options: [])
        return String(data: data, encoding: .utf8)!
    }
    
    func getCategoryProduct() {
        
        guard let storeId = UserDefaults.standard.value(forKey: "storeId") as? String else {return}
        
        var postData = [String:String]()
        postData["page"] = "\(self.currentPage)"
        postData["id"] = categoryId
        postData["theme"] = "1"
        postData["store_id"] = storeId
        
       
    
        if APP_DEL.strFilterWithDeeplink != ""{
            
            postData["multi_filter"] = APP_DEL.strFilterWithDeeplink
           
        }
        else
        {
            postData["multi_filter"] = self.updateFilterSTR()//filterStringc
        }
        
        
        if self.filterString == ""{
            
            self.lblAppliedFilter.isHidden = true
            self.lblAppliedFilterTop.isHidden = true
        }
        else
        {
            self.lblAppliedFilter.isHidden = false
            self.lblAppliedFilterTop.isHidden = false
        }
        
        if let sType = sortType {
            postData["sort_type"] = sType.id ?? ""
        } else {
            postData["sort_type"] = "1"
        }
        
        
        print(postData["multi_filter"] ?? "")
            
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader_withlockedbackground(me: self);
        
        activityIndi.isHidden = false
        activityIndi.startAnimating()
        
        var baseURL = ""
        if self.currentPage == 1 {
            
             postData["required_cat_image"] = categoryId
            postData["country_id"] = APP_DEL.selectedCountry.country_code ?? "SA"
            baseURL = "mobiconnect/catalog/productwithattribute"
        }
        else
        {
            postData["country_id"] = APP_DEL.selectedCountry.country_code ?? "SA"

            baseURL = "mobiconnect/catalog/productwithoutattribute/"
        }
        self.loading = true
        
        API().callAPI(endPoint: baseURL, method: .POST, param: postData) { (json, err) in
            
          //  DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                self.activityIndi.isHidden = true
                self.activityIndi.stopAnimating()
                
                let json_0 = json[0]
                
                if err == nil {
                    
                   if (self.currentPage == 1) {
                     if json_0[0].stringValue == "NO_PRODUCTS" {
                         
//                         self.filterLevel2Index = -1
//                         self.filterLevel2 = [:]
//                         self.level_2 = []
//                         self.filterDT.selected_level_2_code = ""
                         self.noProductCheck = true;
                     }
                    }
                    else if json_0.stringValue == "NO_PRODUCTS"
                    {
                          self.noProductCheck = true;
                     
                    }
                    
                    if (self.currentPage == 1) {
                        
//                        if self.filterDT.selected_level_1_code == ""{
//
//                            _ = self.arrFilter.filter { (fil) -> Bool in
//
//
//                                if fil.type == .Shop_2 {
//                                    if self.filterDT.selected_level_2_code == ""{
//                                        self.level_2 = [fil]
//                                        return true
//
//                                    }
//
//
//                                }
//
//                                return false
//                            }
//                        }
                        
                       
                        
                        
                        if self.noProductCheck == false {
                            
                            
                            
                            if !self.isFilterApplied{
                                
                                do {
                                    
                                     self.categoryImage = json[1][0]["required_cat_image"].stringValue
                                    
                                    self.arrFilter = try JSONDecoder().decode([Filter].self, from: json[1][0]["filter"].rawData())
                                    
                                    //                                self.level_1 = try JSONDecoder().decode([Category].self, from: json_0["data"]["category_attribute_level1"].rawData())
                                    //                                self.level_2 = try JSONDecoder().decode([Category].self, from: json_0["data"]["category_attribute_level2"].rawData())
                                    
                                    self.arrSort = try JSONDecoder().decode([SortType].self, from: json[1][0]["sort_type"].rawData())
                                    
                                } catch { }
                                
                                
                                
                                _ = self.arrFilter.filter { (fil) -> Bool in
                                    
                                    if fil.type == .Shop_1 {
                                        
                                        if self.filterDT.selected_level_1_code == ""{
                                            
                                            self.level_1 = [fil]
                                            return true
                                        }
                                    }
                                    else if fil.type == .Shop_2 {
                                        if self.filterDT.selected_level_2_code == ""{
                                            self.level_2 = [fil]
                                            return true
                                            
                                        }
                                    }
                                    
                                    return false
                                }
                                
                                
                                
                                
                                if (self.level_1.count > 0) && ((self.level_1[0].att_code ?? "") != "") {
                                    self.level_1_key = self.level_1[0].att_code ?? ""
                                }
                                
                                if (self.level_2.count > 0) && ((self.level_2[0].att_code ?? "") != "") {
                                    self.level_2_key = self.level_2[0].att_code ?? ""
                                }
                                
                                
                                
                                
                            }
                            
                            
                          
                        }
                        
                        self.products = []
                    }
                            
                    
                    
                    do {
                        
                        
                        
                        
                        let catData = try json_0["data"]["products"].rawData()
                        let temp = try JSONDecoder().decode([CategoryProduct].self, from: catData)
                        self.products.append(contentsOf: temp as [CategoryProduct])
                        
                    } catch {
                        
                    }
                    
//                    if(json_0["data"]["products"].arrayValue.count == 0) {
//
//                        self.loading = false;
//                    }
//
//
//                    if (self.currentPage != 1) {
//
//                        self.loading = true
//                    }
                                                            
                } else {
                    
                    self.products = []
                }
                
                self.isCheckLBLNoData = true
                
               if self.currentPage == 1{
                   
                if (self.products.count > 0) && (self.collectionProduct.numberOfItems(inSection: 0) > 0) {
                    
                    self.collectionProduct?.scrollToItem(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
                }
                
                    self.RELOAD(isFromProduct: true)
               }
               else
               {
                   
                   self.collectionProduct.reloadData()
               }
          //  }
            
            self.loading = false;
        }
    }
    
    func getFilter() {
        
//        var postData = [String:String]()
//
//        guard let storeId = UserDefaults.standard.value(forKey: "storeId") as? String else {
//            return;
//        }
//        postData["store_id"] = storeId
//        postData["id"] = categoryId
//        postData["multi_filter"] = self.updateFilterSTR()
//
//        API().callAPI(endPoint: "mobiconnect/catalog/attributes", method: .POST, param: postData) { (json, err) in
//
//            DispatchQueue.main.async {
//
//               // cedMageLoaders.removeLoadingIndicator(me: self);
//
//                if err == nil {
//
//                    if json[0]["status"].stringValue == "true" {
//
//                        self.arrFilter = try! JSONDecoder().decode([Filter].self, from: json[0]["filter"].rawData())
//
//                        _ = self.arrFilter.filter { (fil) -> Bool in
//
//                            if fil.type == .Shop_1 {
//
//                                if self.filterDT.selected_level_1_code == ""{
//
//                                    self.level_1 = [fil]
//                                    return true
//                                }
//                            } else if fil.type == .Shop_2 {
//
//                                 if self.filterDT.selected_level_2_code == ""{
//                                    self.level_2 = [fil]
//                                    return true
//                                }
//
//                            }
//
//                            return false
//                        }
//
//
//                        if (self.level_1.count > 0) && ((self.level_1[0].att_code ?? "") != "") {
//                            self.level_1_key = self.level_1[0].att_code ?? ""
//                        }
//
//                        if (self.level_2.count > 0) && ((self.level_2[0].att_code ?? "") != "") {
//                            self.level_2_key = self.level_2[0].att_code ?? ""
//                        }
//
//                        self.RELOAD(isFromProduct: false)
//
//                    }
//                }
//            }
//        }
    }
    
    
    func checkPagination() {
        
        currentPage += 1
        self.getCategoryProduct()
        
    }
}



extension HomeBrandVC : UICollectionViewDataSource , UICollectionViewDelegate ,UICollectionViewDelegateFlowLayout{
   
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collectionProduct {
            return products.count
        } else if collectionView == collSubCategory {
            
            if level_1.count > 0 {
                
                return (level_1[0].filter_data?.count ?? 0)
            }
            
        } else if collectionView == collSubCatType {
            
            if level_2.count > 0 {
                
                return (level_2[0].filter_data?.count ?? 0)
            }
            
        }
        
        return 0
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == collectionProduct {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CatProductCell", for: indexPath) as! CatProductCell
            
            if (APP_DEL.isDiscountTagEnable == "0"){
                cell.offerLabel.isHidden = true
            }
            else
            {
                let offerString = products[indexPath.item].offer ?? ""
                
                if offerString != "" {
                    
                    let offer = " \(offerString)% \(APP_LBL().off.uppercased()) "
                    
                    cell.offerLabel.text = offer
                    cell.offerLabel.isHidden = false
                } else {
                    cell.offerLabel.isHidden = true
                }
            }
            
            cell.lblProductName.text = products[indexPath.item].product_name
            
            let tagString = products[indexPath.item].tag ?? ""
            if tagString != "" {
                
                
                cell.lblTags.text = " " + tagString + " "
                cell.lblTags.isHidden = false
            } else {
                cell.lblTags.isHidden = true
            }
            
            
            let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: products[indexPath.item].regular_price!)
            attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 1, range: NSMakeRange(0, attributeString.length))
            
            cell.lblBrandName.text = products[indexPath.item].brands_name
            
            cell.lblRegularPrice.isHidden = true
          if products[indexPath.item].special_price != "no_special" {
              
              cell.lblPrice.text = products[indexPath.item].special_price;
              
              cell.lblRegularPrice.attributedText = attributeString
              
              cell.lblRegularPrice.isHidden = false
              cell.lblRegularPrice.alpha = 1
              
          } else {
              
              cell.lblPrice.text = products[indexPath.item].regular_price
              
              cell.lblRegularPrice.isHidden = true
              cell.lblRegularPrice.alpha = 0
          }
            
//            if products[indexPath.item].special_price != "no_special" {
//
//                cell.lblPrice.text = products[indexPath.item].special_price;
//
//            } else {
//
//                cell.lblPrice.text = products[indexPath.item].regular_price
//            }
            
             cell.imgProduct.image = nil
            
//            if let downloadURL = SDImageCache.shared.imageFromCache(forKey: products[indexPath.item].product_image!){
//                cell.imgProduct!.image = downloadURL
//            } else {
                cell.imgProduct!.sd_setImage(with: URL(string: products[indexPath.item].product_image!), placeholderImage: nil)
            //}

            
            return cell
            
        }else if collectionView == collSubCategory {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SubCategoryCell", for: indexPath) as! SubCategoryCell
                  
            cell.lblTitle.text = (level_1[0].filter_data?[indexPath.item].filter_value ?? "").uppercased()
            
            if self.filterLevel1Index == (indexPath.item) {
                cell.lblTitle.textColor = nejreeColor//UIColor.black
                cell.viewBg.backgroundColor = UIColor.black//nejreeColor
            } else {
                cell.lblTitle.textColor = UIColor.white
                cell.viewBg.backgroundColor = UIColor.black
            }
            

            return cell
            
        } else if collectionView == collSubCatType {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SubCategoryTypeCell", for: indexPath) as! SubCategoryTypeCell
            
            cell.lblTitle.text = (level_2[0].filter_data?[indexPath.item].filter_value ?? "").uppercased()
            cell.imgType.backgroundColor = UIColor.white
            //cell.imgType!.sd_setImage(with: URL(string: level_2[0].filter_data?[indexPath.item].filter_img ?? ""), placeholderImage: nil)
            cell.imgType.image = nil
//            if let downloadURL = SDImageCache.shared.imageFromCache(forKey: level_2[0].filter_data?[indexPath.item].filter_img ?? ""){
//                cell.imgType!.image = downloadURL
//            } else {
                cell.imgType!.sd_setImage(with: URL(string: level_2[0].filter_data?[indexPath.item].filter_img ?? ""), placeholderImage: nil)
            //}
            
            if self.filterLevel2Index == (indexPath.item) {
                cell.lblTitle.textColor = nejreeColor
            } else {
                cell.lblTitle.textColor = UIColor.white
            }
            
            return cell
        }
            
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == collSubCategory {
            
            
            self.filterLevel1Index = indexPath.item
            
            if self.filterDT.selected_level_1_code == (level_1[0].filter_data?[indexPath.item].filter_code ?? ""){
                
                self.filterLevel1Index = -1
                self.filterDT.selected_level_1_code = ""
                filterLevel1 = [:]
                self.filterString = ""
            }
            else
            {
                filterLevel1 = [(level_1[0].filter_data?[indexPath.item].filter_code ?? ""):(level_1[0].filter_data?[indexPath.item].filter_value ?? "")]
                self.filterDT.selected_level_1_code = (level_1[0].filter_data?[indexPath.item].filter_code ?? "")
            }
            
          
            
            self.filterLevel2Index = -1
            filterLevel2 = [:]
            self.filterDT.selected_level_2_code = ""
            
          //  self.updateBannerImage()
            
             self.getFilter()
            
            self.filterApply(filter: self.filterString, filterDT: self.filterDT, isForShowFilterView: false)
            
        } else if collectionView == collSubCatType {
            
            self.filterLevel2Index = indexPath.item
            
            if self.filterDT.selected_level_2_code == (level_2[0].filter_data?[indexPath.item].filter_code ?? ""){
                self.filterLevel2Index = -1
                   self.filterDT.selected_level_2_code = ""
                   filterLevel2 = [:]
            }
            else
            {
                filterLevel2 = [(level_2[0].filter_data?[indexPath.item].filter_code ?? ""):(level_2[0].filter_data?[indexPath.item].filter_value ?? "")]
                self.filterDT.selected_level_2_code = (level_2[0].filter_data?[indexPath.item].filter_code ?? "")
            }
            
          self.getFilter()
                        
            self.filterApply(filter: self.filterString, filterDT: self.filterDT, isForShowFilterView: false)
            
        } else if collectionView == collectionProduct {
            
            let vc = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "productViewController") as! productViewController
            vc.product_id = products[indexPath.row].product_id ?? ""
             APP_DEL.productIDglobal = products[indexPath.row].product_id ?? ""
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == collSubCategory {
            var width = CGFloat()
            let height : CGFloat = 30
            let fixSpaceToAdd : CGFloat = 20.0//35.0
            
            var text = ""
            
            if level_1.count > 0 {
                text = (level_1[0].filter_data?[indexPath.item].filter_value ?? "").uppercased()
            }
            
            if text != "" {
                width =  self.textWidth(font:  UIFont(name: "Cairo-Regular", size: 15)!, text: text) + fixSpaceToAdd
            } else {
                width = 100
            }
                return CGSize(width: width, height: height)
        }else if collectionView == collSubCatType {
            
            return CGSize(width: collSubCatType.frame.size.height , height: collSubCatType.frame.size.height)
            
        }else{
           // return CGSize(width: (collectionProduct.frame.size.width/2)-10, height: (collectionProduct.frame.size.width/2)+108)
            return CGSize(width: (collectionProduct.frame.size.width/2)-3, height: (collectionProduct.frame.size.width/2)+70)
        }
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView == collectionProduct {
            return 0
        }else if collectionView == collSubCatType {
            return 10
        }else {
            return 0
        }

    }

    
   func textWidth(font: UIFont, text: String) -> CGFloat {
            let myText = text as NSString
            
            let rect = CGSize(width: CGFloat.greatestFiniteMagnitude, height: CGFloat.greatestFiniteMagnitude)
            let labelSize = myText.boundingRect(with: rect, options: .usesLineFragmentOrigin, attributes: [kCTFontAttributeName as NSAttributedString.Key: font], context: nil)
            
            return ceil(labelSize.width)
        }

    
    
    
    
    
}

//MARK:- SEARCH CODE
extension HomeBrandVC {
    
    @IBAction func btnSearchAction(_ sender: Any) {
        
        if self.viewTxtSearch.isHidden == true {
            
            self.viewTxtSearch.isHidden = true
            //self.viewSearch.isHidden = false
            self.txtSearch.becomeFirstResponder()
            
            UIView.animate(withDuration: 0.3, delay: 0.0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.7, options: [.curveLinear], animations: { () -> Void in

                self.isSearchOpen = true
                self.viewTxtSearch.isHidden = false
                //self.viewSearch.isHidden = true
                self.view.layoutIfNeeded()
                
            }, completion: nil)
            
        } else {
            
            self.view.endEditing(true)
            
            if txtSearch.text == ""{
                           
               self.closeSuggetionView()
               return
           }
            
            let detail : [AnalyticKey:String] = [
                .SearchTerm : searchText]
            API().setEvent(eventName: .Search, eventDetail: detail) { (json, err) in }
            
            Analytics.logEvent(AnalyticsEventSearch, parameters: [AnalyticsParameterSearchTerm : searchText])

            if let viewController =  UIStoryboard(name: "categorylayouts", bundle: nil).instantiateViewController(withIdentifier: "cedMageDefaultCollection") as?  cedMageDefaultCollection {
                viewController.strTitleLayer = searchText
                viewController.searchString = searchText
                viewController.hidesBottomBarWhenPushed = true
                viewController.selectedCategory = categoryId
                
                if (self.txtSearch.text != "") {
                    
                    self.navigationController?.pushViewController(viewController, animated: true)
                }
            }
        }
            
        
    }
    
    @IBAction func btnClearAction(_ sender: Any) {
        
        if self.txtSearch.text == "" {
                
            self.view.endEditing(true)
            
            self.viewTxtSearch.isHidden = false
            //self.viewSearch.isHidden = true
            self.isSearchOpen = false
            
            
            UIView.animate(withDuration: 0.3, delay: 0.0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.7, options: [.curveLinear], animations: { () -> Void in

                
                
                self.viewTxtSearch.isHidden = true
                //self.viewSearch.isHidden = false
                self.view.layoutIfNeeded()

            }, completion: nil)
            
        } else {
            
            self.txtSearch.text = ""
            self.displayClear(res: false)
            self.searchText(text: "")
        }
    }
    
    func displayClear(res: Bool) {
        
        //self.btnClear.isHidden = !res
    }
    
    func searchText(text: String) {

        searchText = text
        
        if text == "" {
                        
            self.htTbl.constant = 4.0
            self.viewSearchList.isHidden = true
            self.tblView.isHidden = true
            
        } else {
            
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(getHint), object: nil)
            self.perform(#selector(getHint), with: nil, afterDelay: 1)
        }

        //checkNoData()
    }
    
    
    @objc func getHint() {
        
        if(searchText.count >= 3) {
            
            searchText = searchText.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            getAutocomplete()
        }
    }
    
    func getAutocomplete() {
        
        guard let storeId = UserDefaults.standard.value(forKey: "storeId") as? String else {return}
        
        var postData = [String:String]()
        postData["page"] = "1"
        postData["store_id"] = storeId
        postData["q"] = searchText
        
        postData["autosearch"] = "yes"
            
        let defaults = UserDefaults.standard
        if defaults.bool(forKey: "isLogin") {
            
            if let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] {
                
                postData["hashkey"] = userInfoDict["hashKey"]
                postData["customer_id"] = userInfoDict["customerId"]
            }
        }
        postData["country_id"] = APP_DEL.selectedCountry.country_code ?? "SA"
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader_withlockedbackground(me: self);
        
        activityIndi.isHidden = false
        activityIndi.startAnimating()
       
        
        API().callAPI(endPoint: "mobiconnect/catalog/productsearch", method: .POST, param: postData) { (json, err) in
            
         //   DispatchQueue.main.async {
                
                self.activityIndi.isHidden = true
                self.activityIndi.stopAnimating()
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                let json_0 = json[0]
                
                if err == nil {

                   if (json[0].stringValue != "NO_PRODUCTS")
                    {
                        self.suggentionsArray.removeAll()
                        for (_,value) in json_0["data"]["suggestion"]
                        {
                            
                            var data = [String:String]()
                            data["product_id"] = value["product_id"].stringValue
                            data["product_name"] = value["product_name"].stringValue
                            data["product_image"] = value["product_image"].stringValue
                            self.suggentionsArray.append(data)
                        }
                        
                        self.tblView.reloadData()
                        self.viewSearchList.isHidden = false
                        self.tblView.isHidden = false
                    }
                    else
                    {
                        self.htTbl.constant = 4.0
                        self.viewSearchList.isHidden = true
                        self.tblView.isHidden = true
                    }

                }
           // }
        }
    }

}

extension HomeBrandVC: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if let str = textField.text, let swtRange = Range(range, in: str) {
            
            let fullString = str.replacingCharacters(in: swtRange, with: string)
            self.displayClear(res: (fullString.count > 0))
            searchText(text: fullString)
        }
        
        return true;
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        self.displayClear(res: ((textField.text ?? "").count > 0))
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {

       // filter(text: textField.text ?? "")
        self.view.endEditing(true)
        
        let detail : [AnalyticKey:String] = [
            .SearchTerm : searchText]
        API().setEvent(eventName: .Search, eventDetail: detail) { (json, err) in }
        Analytics.logEvent(AnalyticsEventSearch, parameters: [AnalyticsParameterSearchTerm : searchText])

        if let viewController =  UIStoryboard(name: "categorylayouts", bundle: nil).instantiateViewController(withIdentifier: "cedMageDefaultCollection") as?  cedMageDefaultCollection {
            viewController.strTitleLayer = searchText
            viewController.searchString = searchText
            viewController.hidesBottomBarWhenPushed = true
            viewController.selectedCategory = categoryId
            
            if (self.txtSearch.text != "") {
                
                self.navigationController?.pushViewController(viewController, animated: true)
            }
        }
        
        return true
    }
}

extension HomeBrandVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return suggentionsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchProductCell") as! SearchProductCell
        cell.selectionStyle = .none
        
        cell.imgView.sd_setImage(with: URL(string: suggentionsArray[indexPath.row]["product_image"] ?? ""), placeholderImage: nil)
            
        cell.lblTitle.text = suggentionsArray[indexPath.row]["product_name"]
        
        cell.lblDevider.isHidden = (indexPath.row == (suggentionsArray.count - 1))
        
        if APP_DEL.selectedLanguage == Arabic{
            cell.lblTitle.textAlignment = .right
            cell.imgArrow.image = UIImage(named: "arrows_ar")
        } else {
            cell.lblTitle.textAlignment = .left
            cell.imgArrow.image = UIImage(named: "arrows")
        }
        
        return cell;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
                
        self.btnClearAction(self.btnClear)
        
        self.closeSuggetionView()
        
        let vc = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "productViewController") as! productViewController
        APP_DEL.productIDglobal = suggentionsArray[indexPath.row]["product_id"]!
        vc.product_id = suggentionsArray[indexPath.row]["product_id"]!
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    
}
