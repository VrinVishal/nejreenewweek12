//
//  NotifyPopupVC.swift
//  MageNative Magento Platinum
//
//  Created by vishal.n on 18/02/21.
//  Copyright © 2021 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

protocol SubmitNotify {
    func submitNotify(res:Bool)
}

class NotifyPopupVC: UIViewController {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    
    @IBOutlet weak var viewEmail: UIView!
    @IBOutlet weak var txtEmailField: UITextField!
    
    @IBOutlet weak var viewMobile: UIView!
    @IBOutlet weak var txtMobileField: UITextField!
    @IBOutlet weak var lblPhoneCode: UILabel!
    
    @IBOutlet weak var btnSubmit: UIButton!
    
    let nejreeColor = UIColor.init(hexString: "#FBAD18")
    
    var delagateSubmitNotify: SubmitNotify?
    
    var email = ""
    var mobile = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.lblPhoneCode.text = "+" + (APP_DEL.selectedCountry.phone_code ?? "966")
        lblTitle.font = UIFont(name: "Cairo-Regular", size: 18)!
        lblDesc.font = UIFont(name: "Cairo-Regular", size: 16)!
        lblPhoneCode.font = UIFont(name: "Cairo-Bold", size: 16)!
        setUI()
    }
    
    @IBAction func btnSubmitAction(_ sender: UIButton) {
        email = txtEmailField.text ?? ""
        let validEmail = EmailVerifier.isValidEmail(testStr: email)
        
        if email == ""{
            self.view.makeToast(APP_LBL().please_enter_email, duration: 1.0, position: .center)
            return;
        }
        else if !validEmail {
            self.view.makeToast(APP_LBL().please_enter_valid_email, duration: 1.0, position: .center)
            
            return;
        }
        
        if ((txtMobileField.text ?? "").count) != (Int(APP_DEL.selectedCountry.phone_number_limit ?? "") ?? 9) {
            
            self.view.makeToast(APP_LBL().mobile_number_should_be_of_xyz_digits_only, duration: 1.0, position: .center)
            return;
        }
        
        self.delagateSubmitNotify?.submitNotify(res: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnCloseAction(_ sender: UIButton) {
        
        self.dismiss(animated: true, completion: nil)
    }

    func setUI() {
        
        lblTitle.superview?.round(redius: 6.0)
        
        lblTitle.text = APP_LBL().notify_me.uppercased()
        lblDesc.text = APP_LBL().get_notified_as_soon_as
                
        if APP_DEL.selectedLanguage == Arabic {
            lblTitle.textAlignment = .right
            lblDesc.textAlignment = .right
        } else {
            lblTitle.textAlignment = .left
            lblDesc.textAlignment = .left
        }
        
        //self.imgGiftCard.sd_setImage(with: URL(string: imageURL), placeholderImage: nil)
        
        txtEmailField.attributedPlaceholder = NSAttributedString(string: APP_LBL().email_star.uppercased(),
        attributes: [NSAttributedString.Key.foregroundColor: UIColor.black])
        
        txtMobileField.attributedPlaceholder = NSAttributedString(string: APP_LBL().phone_number_star.uppercased(),
               attributes: [NSAttributedString.Key.foregroundColor: UIColor.black])
        
        txtEmailField.font = UIFont(name: "Cairo-Regular", size: 14)!
        txtMobileField.font = UIFont(name: "Cairo-Regular", size: 14)!

        viewMobile.semanticContentAttribute = .forceLeftToRight
        viewEmail.setBorderBlack()
        viewMobile.setBorderBlack()
        
        viewEmail.setCornerRadius()
        viewMobile.setCornerRadius()
        
        txtEmailField.delegate = self
        txtMobileField.delegate = self
        
        
        if APP_DEL.selectedLanguage == Arabic {
            txtEmailField.textAlignment = .right
            txtMobileField.textAlignment = .right
        } else {
            txtEmailField.textAlignment = .left
            txtMobileField.textAlignment = .left
        }
        
        btnSubmit.setTitle(APP_LBL().submit.uppercased(), for: .normal)
        btnSubmit.titleLabel?.font = UIFont(name: "Cairo-Regular", size: 17)!
        
        txtMobileField.keyboardType = .asciiCapableNumberPad
        
        
        self.txtEmailField.text = self.email
        self.txtMobileField.text = self.mobile
    }

}

extension NotifyPopupVC: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
                
        if (textField) == txtMobileField {
            
            guard !string.isEmpty else {

                return true
            }
            
            if (CharacterSet(charactersIn: "0123456789").isSuperset(of: CharacterSet(charactersIn: string))) {

                if let text = textField.text, let range = Range(range, in: text) {

                    let proposedText = text.replacingCharacters(in: range, with: string)

                    if (APP_DEL.selectedCountry.country_code?.uppercased() == "SA"){
                        if proposedText.first == "5" && proposedText.count <= (Int(APP_DEL.selectedCountry.phone_number_limit ?? "") ?? 9) {
                            return true
                        }
                    } else {
                        if proposedText.count <= (Int(APP_DEL.selectedCountry.phone_number_limit ?? "") ?? 9) {
                            return true
                        }
                    }
                }
            }

            return false
            
        }
        
        return true
    }

}
