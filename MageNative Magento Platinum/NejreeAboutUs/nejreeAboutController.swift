//
//  nejreeAboutController.swift
//  MageNative Magento Platinum
//
//  Created by MacMini on 29/08/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit
import WebKit

class nejreeAboutController: MagenativeUIViewController,UIGestureRecognizerDelegate {

    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var imgAbout: UIImageView!
    @IBOutlet weak var webview: WKWebView!
    //var str = ""
     
    override func viewDidLoad() {
        super.viewDidLoad()
        if APP_DEL.selectedLanguage == Arabic{
            
            self.btnBack.setImage(UIImage(named: "BackArrowNewArabic"), for: .normal)
            
        } else {
          
            self.btnBack.setImage(UIImage(named: "BackArrowNew"), for: .normal)
        }
 
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
       @IBAction func btnBackAction(_ sender: UIButton) {
           
           self.navigationController?.popViewController(animated: true)
       }
       
       override func viewWillDisappear(_ animated: Bool) {
           self.navigationController?.setNavigationBarHidden(true, animated: true)
       }
       
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        self.navigationController?.setNavigationBarHidden(true, animated: true)

        self.getCMS()
        
        
       if navigationController?.responds(to: #selector(getter: UINavigationController.interactivePopGestureRecognizer)) ?? false {
           
           self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
           self.navigationController?.interactivePopGestureRecognizer?.delegate = self
       }

    }
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
           
           return true
       }
       
    
    func getCMS() {
        
        var postData = [String:String]()
        postData["language_id"] = UserDefaults.standard.value(forKey: "storeId") as? String
        
        if APP_DEL.selectedLanguage == Arabic{
            postData["cms_id"] = "about-us-app-arabic"
        } else {
            postData["cms_id"] = "about-us-app-english"
        }
        
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);
        
        API().callAPI(endPoint: "mobiconnect/cms/getpage/", method: .POST, param: postData) { (json, err) in
            
            //DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                if err == nil {
                    
                    let content = json[0]["data"]["result"][0]["content"].stringValue
                    let image = json[0]["data"]["result"][0]["image"].stringValue
                    
                    let fontName =  "Cairo-Regular"
                                      let fontSize = 85
                                      let fontSetting = "<span style=\"font-family: \(fontName);font-size: \(fontSize)\"</span>"
                                      
                                       self.webview.loadHTMLString( fontSetting + content, baseURL: nil)
                    
                  //  self.webview.loadHTMLString(content, baseURL: nil)
                    self.imgAbout.sd_setImage(with: URL(string: image), placeholderImage: nil)
                }
          //  }
        }
    }
    
}
