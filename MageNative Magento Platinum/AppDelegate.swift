/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */

import UIKit
import CoreData
import UserNotifications
import Firebase
import IQKeyboardManager
import FirebaseMessaging
import FirebaseDynamicLinks
import Foundation
import FirebaseAuth
import FirebaseCore
import NVActivityIndicatorView
import SKPhotoBrowser
import StoreKit
import Adjust

protocol UpdateHome {
    func updateHome(res: Bool)
}
protocol UpdateCategory {
    func updateCategory(res: Bool)
}
protocol UpdateSelfCategory {
    func updateSelfCategory(res: Bool)
}




var APP_DEL = UIApplication.shared.delegate as! AppDelegate
let IS_IPAD = UIDevice.current.userInterfaceIdiom == .pad ? true : false

//@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    var externalWindow: UIWindow?
    
    var notificationStatus : Bool = false
    
    var isComesFromOrder : Bool = false
    var isComesFromDeeplink : Bool = false
    var isLoginFromWishList : Bool = false
    var isLoginFromCartList : Bool = false
    var isWishListShouldReload : Bool = false
    var isAccountPageRefreshAfterOrder : Bool = false
    var isRedeemFromPushNotification : Bool = false
    var isProfileFromPushNotification : Bool = false
    var isOrderFromPushNotification : Bool = false
    var isOrderDetailFromPushNotification : Bool = false
    var isBackFromCancelOrder : Bool = false
    var isLogoutFromAddAddress : Bool = false
    
    var isCategoryFromHomeLayer : Bool = false
    var isTabBarCreated : Bool = false

    var isAppLaunch : Bool = false
    var isDiscountTagEnable = ""
    var strDeepLinkData = ""
    var strDeepLinkDataTemp = ""
    var productIDglobal = ""
    var strFirebaseToken = ""
    var strSignUpfromGiftcard = ""
    var strCodCharge = "0"
    var key_isCOD = "isCOD"
    var key_isCard = "key_isCard"
    
    var extrafee_status = ""
    var auto_apply_storecredit = ""
    var extrafee_minimum_order_amount = ""
    var category_record_seconds = "300"
    var autoRedeemCode = ""
    var SMSCharLimit = 140
    var isApplePayEnable = ""
    
    var selectedLanguage : String = ""
    
    static var newIndex = 0
    var arrGftCardData: [GiftItem] = []
    var home_featured_category : [String] = []
    var arrLBL: [LBL] = []
    
    var arrNotification: [AppNotif] = []
    
    var delegateUpdateHome: UpdateHome?
    var delegateUpdateCategory: UpdateCategory?
    var delegateUpdateSelfCategory: UpdateSelfCategory?
    
    var isFromHomeBannerID : String = ""
    var arrCategoriesCat: [CategoryLayer] = []
    var selectedCountry : countryData = countryData.init()
    
    let notificationDelegate = SampleNotificationDelegate()
    var strFilterWithDeeplink = ""
    var ActualDeeplink = ""
    var strHomeBannerImage = ""
    var activityIndicator : NVActivityIndicatorView!
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        let lg = UserDefaults.standard.value(forKey: "AppleLanguages") as! [String]
        
        if lg[0] == "ar" || lg[0] == "ur" {
            
            APP_DEL.selectedLanguage =  "ar"
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
            Bundle.setLanguage("ar")
        } else {
            
            APP_DEL.selectedLanguage =  "en"
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
            Bundle.setLanguage(lg[0])
        }
        
        print("Hello")
        
        APP_DEL = UIApplication.shared.delegate as! AppDelegate
        
        //UIApplication.shared.statusBarView?.backgroundColor = UIColor.systemYellow
        
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        FirebaseOptions.defaultOptions()?.deepLinkURLScheme = "bundleIDDL"
        
        solveMemoryIssue()
        UIApplication.shared.applicationIconBadgeNumber = 0

        
        self.countLaunchForReview()
        
        self.setupPushNotification(application)
        
        UITextField().keyboardAppearance =   UIKeyboardAppearance.light
        
        
        let adjustConfig = ADJConfig(appToken: Constants().ADJUST_APP_TOKEN, environment: Constants().ADJUST_ENVIRONMENT, allowSuppressLogLevel: true)
        adjustConfig?.logLevel = ADJLogLevelVerbose
        adjustConfig?.delegate = self
        Adjust.setEnabled(true)
        Adjust.appDidLaunch(adjustConfig)
        
        return true
    }
    
    
    func countLaunchForReview(){
        
        let defaults = UserDefaults.standard
        
        var actionCount = defaults.integer(forKey: "appOpenCount")
        actionCount += 1
        defaults.set(actionCount, forKey: "appOpenCount")
        
    }
    
    func solveMemoryIssue() {
        SDImageCache.shared.config.maxDiskAge = 3600 * 24 * 7
        SDImageCache.shared.config.maxDiskSize = 1024 * 1024 * 4 * 20
        SDImageCache.shared.config.shouldCacheImagesInMemory = false
        SDImageCache.shared.config.shouldUseWeakMemoryCache = false
        SDImageCache.shared.config.diskCacheReadingOptions = .mappedIfSafe
        SDWebImageManager.shared.optionsProcessor = SDWebImageOptionsProcessor() { url, options, context in
        var mutableOptions = options
        mutableOptions.insert(.avoidDecodeImage)
        return SDWebImageOptionsResult(options: mutableOptions, context: context)
        }
    }
    
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
         let sourceApplication: String? = options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String
        
        return true
    }

    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data)
    {
        var tokenString: String = ""
        for i in 0..<deviceToken.count {
            tokenString += String(format: "%02.2hhx", deviceToken[i] as CVarArg)
        }
    }
    
    func application(_ application: UIApplication, didRegister notificationSettings: UIUserNotificationSettings) {
        
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        let aps = notification.request.content.userInfo["aps"] as? NSDictionary
        print(aps as Any)
        
        completionHandler([.alert,.sound,.badge])
        
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        print(userInfo)
        
        if let val = userInfo["deeplink"] {
           
            //APP_DEL.strDeeplinkWithPushNotification = val as! String
            //NotificationCenter.default.post(name: NSNotification.Name(rawValue: "checkDeeplinkFromFirebase"), object: nil)
            let stringDeep = val as! String
            var array : [String] = []
            
             strDeepLinkDataTemp = stringDeep
            
            if stringDeep.contains("www"){
            
                array = stringDeep.components(separatedBy: "https://www.nejree.com/r/")
                
            }
            else
            {
                array = stringDeep.components(separatedBy: "https://nejree.com/r/")
            }
            
            if array.count > 1 {
                
                let strDeeplink = array[1]
               
                if APP_DEL.isAppLaunch{
                    self.DeeplinkForward(strDeeplink)
                }
                else
                {
                   
                    APP_DEL.strDeepLinkData = strDeeplink
                }
                
                
                
            }
            else
            {
                
                let tempString = array[0]
                if tempString.contains("image"){
                    
                    if let topVC : UIViewController = UIApplication.topViewController() {
                        self.DeeplinkForward(array[0])
                    }
                    else
                    {
                        self.strDeepLinkData = tempString
                    }
                    
                }
                else
                {
                    if let topVC : UIViewController = UIApplication.topViewController() {
                        self.strDeepLinkData = ""
                        APP_DEL.isFromHomeBannerID = ""
                        //APP_DEL.isFromHomeBannerTopID = ""
                        if topVC.tabBarController?.selectedIndex == 2 {
                            
                            let storyboard = UIStoryboard(name: "Main", bundle: nil);
                            let viewController = storyboard.instantiateViewController(withIdentifier: "NejreeBannerVC") as! NejreeBannerVC;
                            topVC.navigationController?.setViewControllers([viewController], animated: false)
                            
                        }
                        else{
                            topVC.tabBarController?.selectedIndex = 2
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "checkDeeplinkFromFirebase"), object: nil)
                        }
                    }
                    else
                    {
                        self.strDeepLinkData = array[0]
                    }
                }
                
                
                
            }
            
            
            
        }

    }

    
    
    func configureNotification() {
        if #available(iOS 10.0, *) {
            let center = UNUserNotificationCenter.current()
            center.requestAuthorization(options:[.badge, .alert, .sound]){ (granted, error) in }
            center.delegate = notificationDelegate
            let openAction = UNNotificationAction(identifier: "OpenNotification", title: NSLocalizedString("Abrir", comment: ""), options: UNNotificationActionOptions.foreground)
            let deafultCategory = UNNotificationCategory(identifier: "CustomSamplePush", actions: [openAction], intentIdentifiers: [], options: [])
            center.setNotificationCategories(Set([deafultCategory]))
        } else {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil))
        }
        UIApplication.shared.registerForRemoteNotifications()
    }
    
    func RegisterFirebaseTopic() {
        
   //     if UserDefaults.standard.bool(forKey: "isLogin") {

//            Messaging.messaging().subscribe(toTopic: "General") { error in
//                    print("Subscribed to \(strTopicName)) topic")
//            }
//        
//        Messaging.messaging().subscribe(toTopic: "GeneralNew") { error in
//                print("Subscribed to \(strTopicName)) topic")
//        }
//        
//        Messaging.messaging().subscribe(toTopic: "Test") { error in
//                print("Subscribed to \(strTopicName)) topic")
//        }
        
//
//
//            Messaging.messaging().subscribe(toTopic: "General-AR") { error in
//                    print("Subscribed to \(strTopicName)) topic")
//            }
//
//
//            Messaging.messaging().subscribe(toTopic: "General-EN") { error in
//                    print("Subscribed to \(strTopicName)) topic")
//            }
//
//            let appVersion = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as? String
//            let userInfoDict = UserDefaults.standard.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
//
//            var postData = [String:String]()
//            postData["customer_id"] = userInfoDict["customerId"]!;
//            postData["email"] = userInfoDict["email"]!;
//            postData["firebase_token"] = strFirebaseToken
//            postData["version"] = appVersion
//            postData["platform"] = "iOS"
//            postData["topic_id"] = strTopicID
//
//            API().callAPI(endPoint: "mobiconnect/customer/notification", method: .POST, param: postData) { (json, err) in }
//
//        }
        
        let userInfoDict = UserDefaults.standard.object(forKey: "userInfoDict") as? [String: String]
     
        var postData = [String:String]()
        
        postData["customerId"] = userInfoDict?["customerId"] ?? "";
        postData["customerName"] = UserDefaults.standard.object(forKey: "name") as? String ?? ""
        postData["customerEmail"] = UserDefaults.standard.object(forKey: "EmailId") as? String ?? ""
        postData["deviceOs"] = "ios"
        postData["language"] = APP_DEL.selectedLanguage
        postData["token"] = strFirebaseToken
        postData["mobileNumber"] = UserDefaults.standard.object(forKey: "mobile_number") as? String ?? ""
        postData["version"] = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? ""
        
        API().setToken(param: postData) { (json, err) in
            
            if err == nil {
                
                self.notificationStatus = json["status"].boolValue
                
                APP_DEL.arrNotification = []
                
                if json["messages"].arrayValue.count > 0 {
                    
                    for v in json["messages"].arrayValue{
                        
                        APP_DEL.arrNotification.append(AppNotif(title: v["title"].stringValue, body: v["body"].stringValue, image: v["image"].stringValue))
                    }
                }
                
                print(APP_DEL.arrNotification.count)
                
            } else {
                
            }
        }
    }
  
    
    func changeLanguage() {
        
        let lg = UserDefaults.standard.value(forKey: "AppleLanguages") as! [String]
        
        if lg[0] == "ar" || lg[0] == "ur" {
            
            APP_DEL.selectedLanguage =  "ar"
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
            Bundle.setLanguage("ar")
        } else {
            
            APP_DEL.selectedLanguage =  "en"
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
            Bundle.setLanguage(lg[0])
        }
        
        print(APP_DEL.selectedLanguage)
        
        lastCartIndex = 2
        
        self.getAddress()
        
        let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "mainTabbar") as? cedMageTabbar
        APP_DEL.window = UIWindow(frame: UIScreen.main.bounds)
        APP_DEL.window?.rootViewController = viewController
        APP_DEL.window?.makeKeyAndVisible()
        
        
        
        APP_DEL.activityIndicator = NVActivityIndicatorView(frame: CGRect(x: (UIScreen.main.bounds.width/2) - 25.0, y: (UIScreen.main.bounds.height/2) - 25.0, width: 50.0, height: 50.0), type: .ballPulse, color: UIColor.init(hexString: "#fcb215"), padding: 0)
        
        APP_DEL.window?.addSubview(APP_DEL.activityIndicator)
        APP_DEL.window?.bringSubviewToFront(APP_DEL.activityIndicator)
        APP_DEL.activityIndicator.isHidden = true
        
        
    }
    
    func getCartCount() {
        
        var postData = [String:String]()
        
        let defaults = UserDefaults.standard
        
        if (defaults.object(forKey: "cartId") != nil) {
            postData["cart_id"] = defaults.object(forKey: "cartId") as? String ?? ""
        } else {
            postData["cart_id"] = "0"
        }
        
        if (defaults.bool(forKey: "isLogin")) {

            if let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] {
                
                postData["hashkey"] = userInfoDict["hashKey"]
                postData["customer_id"] = userInfoDict["customerId"]
            }
        }
        
        API().callAPI(endPoint: "mobiconnect/checkout/getcartcount/", method: .POST, param: postData) { (json, err) in
            
            if err == nil {
                
                if (json[0]["success"].stringValue == "true" || json[0]["success"].stringValue == "false") {
                    
                    if (json[0]["items_count"].stringValue == "") {
                        
                        defaults.set("0", forKey: "items_count")
                        
                    } else {
                        
                        defaults.set(json[0]["items_count"].stringValue, forKey: "items_count")
                    }
                    
                    defaults.set(json[0]["wishlist_item_count"].stringValue, forKey: "wishlist_item_count")
                    
                    
                    defaults.setValue(json[0]["gender"].stringValue, forKey: "gender")

                    if (defaults.value(forKey: "storeId") == nil) {
                        
                        defaults.setValue(json[0]["default_store"].stringValue, forKey: "storeId")
                    }
                    
                }
            }
            
            NotificationCenter.default.post(name: NSNotification.Name("NejreeBadge"), object: nil)
        }
    }
    
    func getUnreadCount(completion: @escaping (Bool) -> ()) {
        
//        var postData = [String:String]()
//
//        if let userInfoDict = UserDefaults.standard.object(forKey: "userInfoDict") as? [String: String] {
//
//            postData["customer_id"] = userInfoDict["customerId"]
//        }
//        else
//        {
//             UserDefaults.standard.set(0, forKey: "NejreeUnreadCount")
//
//             completion(true)
//        }
//
//        API().callAPI(endPoint: "mobiconnect/unreadcount", method: .POST, param: postData) { (json, err) in
//
//            var unreadCount = 0
//
//            if (json.arrayValue.count > 0) {
//
//                unreadCount = json[0]["unread_count"].intValue
//            }
//
//            UserDefaults.standard.set(unreadCount, forKey: "NejreeUnreadCount")
//
//            completion(true)
//        }
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        //self.saveContext()
    }

}
extension URLSession {
    
    class  func getImage(url:URL,completionHaner:@escaping (Data?,Error?)->()){
        
        let task = URLSession.shared.dataTask(with: url as URL, completionHandler: {
            
            data,response,error in
            DispatchQueue.main.async {
              completionHaner(data,error)
                return
            }
            
        })
        task.resume()
    }
}

extension AppDelegate: MessagingDelegate {
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        print("The fcm token is " + fcmToken! ?? "")
        
        strFirebaseToken = fcmToken!
        
        //self.RegisterFirebaseTopic()

        
//        Messaging.messaging().subscribe(toTopic: "Test") { error in
//                print("Subscribed to \(strTopicName)) topic")
//        }
        
        
//        var postData = [String:String]()
//        postData["Token"] = fcmToken
//        postData["type"] = "1"
        
       // API().callAPI(endPoint: "mobinotifications/setdevice", method: .POST, param: postData) { (json, err) in }
        
    }
    
//    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
//        print(remoteMessage)
//    }
//
//
    
}





extension AppDelegate: UNUserNotificationCenterDelegate {
    
    func setupPushNotification(_ application: UIApplication) {
        
        if #available(iOS 10.0, *) {
            
            let center = UNUserNotificationCenter.current()
            center.delegate = self
            center.requestAuthorization(options: [.badge, .alert , .sound]) { (granted, error) in
                
                if granted {
                    DispatchQueue.main.async {
                        UIApplication.shared.registerForRemoteNotifications()
                        UIApplication.shared.applicationIconBadgeNumber = 0
                    }
                    
                } else  {
                    print(error as Any)
                }
            }
            
        } else {
            
            let type: UIUserNotificationType = [UIUserNotificationType.badge, UIUserNotificationType.alert, UIUserNotificationType.sound]
            let setting = UIUserNotificationSettings(types: type, categories: nil)
            UIApplication.shared.registerUserNotificationSettings(setting)
            UIApplication.shared.registerForRemoteNotifications()
            UIApplication.shared.applicationIconBadgeNumber = 0
        }
    }
}

extension UIApplication {
    
    class func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController?
    {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    }
}
 //MARK:- Firebase DynamicLinks
extension AppDelegate {
   
      func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
        URLSession.shared.dataTask(with: userActivity.webpageURL!) { (data, response, error) in

            if let actualURL = response?.url {
                print(actualURL)
                
                self.ActualDeeplink = actualURL.absoluteString
                
                self.strDeepLinkDataTemp = self.ActualDeeplink
                
                var actualDeepLinkTemp = actualURL.absoluteString
                actualDeepLinkTemp = actualDeepLinkTemp.replacingOccurrences(of: "%20", with: "") // added by ashish
                actualDeepLinkTemp = actualDeepLinkTemp.replacingOccurrences(of: "https://www.nejree.com/r/", with: "")
                actualDeepLinkTemp = actualDeepLinkTemp.replacingOccurrences(of: "https://dev05.nejree.com/r/", with: "")
                actualDeepLinkTemp = actualDeepLinkTemp.replacingOccurrences(of: "https://stagenew.nejree.com/r/", with: "")
                
                self.DeeplinkForward(actualDeepLinkTemp)
                
            }
        }.resume()
           return true
       }
    
    
    func getQueryStringParameter(url: String, param: String) -> String? {
      guard let url = URLComponents(string: url) else { return nil }
      return url.queryItems?.first(where: { $0.name == param })?.value
    }
    
    func DeeplinkForward(_ strDeeplink: String)
    {
        if !APP_DEL.isTabBarCreated{
            self.strDeepLinkData = strDeeplink
            return
        }
        
         print(strDeeplink)
  
        let filterArray = strDeeplink.components(separatedBy: "/filter?=")
        var strFilter = ""
        if filterArray.count > 1{
            strFilter = filterArray[1]
        }
        
        if strDeeplink.contains("9bdv.adj.st") {
            // Adjust code for deep link
//            strDeeplink = strDeeplink.
        }
        
        if strDeeplink.contains("https://nejree.com/") || strDeeplink.contains("https://www.nejree.com/r/")
        {
            if let topVC : UIViewController = UIApplication.topViewController() {
                self.strDeepLinkData = ""
                APP_DEL.isFromHomeBannerID = ""
                //APP_DEL.isFromHomeBannerTopID = ""
                DispatchQueue.main.async { // Main thread crash

                    if topVC.tabBarController?.selectedIndex == 2 {
                        
                        let storyboard = UIStoryboard(name: "Main", bundle: nil);
                        let viewController = storyboard.instantiateViewController(withIdentifier: "NejreeBannerVC") as! NejreeBannerVC;
                        topVC.navigationController?.setViewControllers([viewController], animated: false)
                        
                    }
                    else{
                        topVC.tabBarController?.selectedIndex = 2
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "checkDeeplinkFromFirebase"), object: nil)
                    }
                }
            }
            else
            {
                self.strDeepLinkData = strDeeplink
            }
        }
        
//      if strDeeplink.contains("catalog/category")
//      {
//            // For CategoryVc screen Cetegory
//            DispatchQueue.main.async {
//
//                var categoryID = ""
//                var FilterStringNew = ""
//                if strFilter != ""{
//
//
//                    let FilterString = strDeeplink.components(separatedBy: "filter?=")
//                    print(FilterString)
//
//                    categoryID = FilterString.first ?? ""
//                    categoryID = categoryID.replacingOccurrences(of: "catalog/category/", with: "")
//                    categoryID = categoryID.replacingOccurrences(of: "/", with: "")
//
//                   FilterStringNew = FilterString.last ?? ""
//
//                    FilterStringNew = FilterStringNew.replacingOccurrences(of: "%7B", with: "{")
//                    FilterStringNew = FilterStringNew.replacingOccurrences(of: "%22", with: "\"")
//                    FilterStringNew = FilterStringNew.replacingOccurrences(of: "%7D", with: "}")
//
//
//                    print(FilterStringNew)
//                }
//                else
//                {
//                    let comp_1 = strDeeplink.components(separatedBy: "catalog/category/")
//                    print(comp_1.last ?? "")
//                    categoryID = comp_1.last ?? ""
//                }
//
//
//
//              //  let urlString = strDeeplink.filter({ $0.name == "url" }).first?.value?.removingPercentEncoding
//
//                if let topVC : UIViewController = UIApplication.topViewController() {
//                    self.strDeepLinkData = ""
//                    if categoryID != ""{
//                        APP_DEL.isFromHomeBannerID = categoryID
//                    }
//
//                    if strFilter != ""{
//                        APP_DEL.strFilterWithDeeplink = FilterStringNew
//                    }
//
//                    if topVC.tabBarController?.selectedIndex == 0 {
//
//                        let storyboard = UIStoryboard(name: "Main", bundle: nil);
//                        let viewController = storyboard.instantiateViewController(withIdentifier: "NejreeCategoryVC") as! NejreeCategoryVC;
//                        topVC.navigationController?.setViewControllers([viewController], animated: false)
//
//                    }
//                    else{
//                        topVC.tabBarController?.selectedIndex = 0
//                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "checkDeeplinkFromFirebaseCategory"), object: nil)
//                    }
//                }
//                else
//                {
//                    self.strDeepLinkData = strDeeplink
//                }
//
//            }
//
//        }
//
//         else if strDeeplink.contains("catalog/section") && !strDeeplink.contains("category"){
//
//        DispatchQueue.main.async {
//
//            let categoryArray = strDeeplink.components(separatedBy: "/section")
//            var categoryID = ""
//
//            if categoryArray.count > 0{
//                categoryID = categoryArray[1]
//                categoryID = categoryID.replacingOccurrences(of: "/", with: "")
//            }
//
//            if let topVC : UIViewController = UIApplication.topViewController() {
//                self.strDeepLinkData = ""
//                APP_DEL.isFromHomeBannerID = ""
//                //APP_DEL.isFromHomeBannerTopID = categoryID
//                if topVC.tabBarController?.selectedIndex == 0 {
//
//                    let storyboard = UIStoryboard(name: "Main", bundle: nil);
//                    let viewController = storyboard.instantiateViewController(withIdentifier: "NejreeCategoryVC") as! NejreeCategoryVC;
//                    topVC.navigationController?.setViewControllers([viewController], animated: false)
//
//                }
//                else{
//                    topVC.tabBarController?.selectedIndex = 0
//                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "checkDeeplinkFromFirebaseCategory"), object: nil)
//                }
//            }
//            else
//            {
//                self.strDeepLinkData = strDeeplink
//            }
//
//
//        }
//
//      }
       
        else if strDeeplink.contains("rate"){
                 
                 DispatchQueue.main.async {
                     
                    
                    if let _ : UIViewController = UIApplication.topViewController() {
                         self.strDeepLinkData = ""
                         
                           SKStoreReviewController.requestReview()
                     }
                     else
                     {
                         self.strDeepLinkData = strDeeplink
                     }
                     
                     
                 }
                 
               }
        
        
        
//        else if strDeeplink.contains("catalog/section/") && strDeeplink.contains("category"){
//
//          DispatchQueue.main.async {
//
//              let categoryArray = strDeeplink.components(separatedBy: "/")
//              var categoryID = ""
//              var SubCategoryID = ""
//              if categoryArray.count > 3{
//                  categoryID = categoryArray[2]
//                 SubCategoryID = categoryArray[4]
//
//              }
//
//              if let topVC : UIViewController = UIApplication.topViewController() {
//                  self.strDeepLinkData = ""
//                  APP_DEL.isFromHomeBannerID = SubCategoryID
//                  //APP_DEL.isFromHomeBannerTopID = categoryID
//                  if topVC.tabBarController?.selectedIndex == 0 {
//
//                      let storyboard = UIStoryboard(name: "Main", bundle: nil);
//                      let viewController = storyboard.instantiateViewController(withIdentifier: "NejreeCategoryVC") as! NejreeCategoryVC;
//                      topVC.navigationController?.setViewControllers([viewController], animated: false)
//
//                  }
//                  else{
//                      topVC.tabBarController?.selectedIndex = 0
//                      NotificationCenter.default.post(name: NSNotification.Name(rawValue: "checkDeeplinkFromFirebaseCategory"), object: nil)
//                  }
//              }
//              else
//              {
//                  self.strDeepLinkData = strDeeplink
//              }
//
//
//          }
//
//        }
        
        
        else if strDeeplink.contains("section") && strDeeplink.contains("category"){
          
          DispatchQueue.main.async {
              
              let categoryArray = strDeeplink.components(separatedBy: "/")
              var categoryID = ""
              var SubCategoryID = ""
              if categoryArray.count > 3{
                  categoryID = categoryArray[1]
                 SubCategoryID = categoryArray[3]
             
              }
              
              if let topVC : UIViewController = UIApplication.topViewController() {
                  self.strDeepLinkData = ""
                  APP_DEL.isFromHomeBannerID = ""//SubCategoryID
                  //APP_DEL.isFromHomeBannerTopID = categoryID
                  if topVC.tabBarController?.selectedIndex == 2 {
                      
                      let storyboard = UIStoryboard(name: "Main", bundle: nil);
                      let viewController = storyboard.instantiateViewController(withIdentifier: "NejreeBannerVC") as! NejreeBannerVC;
                      topVC.navigationController?.setViewControllers([viewController], animated: false)
                      
                  }
                  else{
                      topVC.tabBarController?.selectedIndex = 2
                      NotificationCenter.default.post(name: NSNotification.Name(rawValue: "checkDeeplinkFromFirebase"), object: nil)
                  }
              }
              else
              {
                  self.strDeepLinkData = strDeeplink
              }
              
              
          }
          
        }
        
        else if strDeeplink.contains("section"){
               
               DispatchQueue.main.async {
                   
                   let categoryArray = strDeeplink.components(separatedBy: "section/")
                   var categoryID = ""
                  
                   if categoryArray.count > 1{
                       categoryID = categoryArray[1]
                       categoryID = categoryID.replacingOccurrences(of: "/", with: "")
                   }
                   
                   
                  
                   if let topVC : UIViewController = UIApplication.topViewController() {
                       self.strDeepLinkData = ""
                       APP_DEL.isFromHomeBannerID = ""
                       //APP_DEL.isFromHomeBannerTopID = categoryID
                       if topVC.tabBarController?.selectedIndex == 2 {
                           
                           let storyboard = UIStoryboard(name: "Main", bundle: nil);
                           let viewController = storyboard.instantiateViewController(withIdentifier: "NejreeBannerVC") as! NejreeBannerVC;
                           topVC.navigationController?.setViewControllers([viewController], animated: false)
                           
                       }
                       else{
                           topVC.tabBarController?.selectedIndex = 2
                           NotificationCenter.default.post(name: NSNotification.Name(rawValue: "checkDeeplinkFromFirebase"), object: nil)
                       }
                   }
                   else
                   {
                       self.strDeepLinkData = strDeeplink
                   }
                   
                   
               }
               
             }
        
        
        
      else if strDeeplink.contains("category"){
        // For Home screen Cetegory
        
            DispatchQueue.main.async {
                
                let categoryArray = strDeeplink.components(separatedBy: "category/")
                var categoryID = ""
                if categoryArray.count > 1{
                    categoryID = categoryArray[1]
                    categoryID = categoryID.replacingOccurrences(of: "/", with: "")
                }
                else
                {
                    categoryID = ""
                }
                
                if let topVC : UIViewController = UIApplication.topViewController() {
                    self.strDeepLinkData = ""
                    //APP_DEL.isFromHomeBannerTopID = categoryID
                    if topVC.tabBarController?.selectedIndex == 2 {
                        
                        let storyboard = UIStoryboard(name: "Main", bundle: nil);
                        let viewController = storyboard.instantiateViewController(withIdentifier: "NejreeBannerVC") as! NejreeBannerVC;
                        topVC.navigationController?.setViewControllers([viewController], animated: false)
                        
                    }
                    else{
                        topVC.tabBarController?.selectedIndex = 2
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "checkDeeplinkFromFirebase"), object: nil)
                    }
                }
                    
                    
                else
                {
                    self.strDeepLinkData = strDeeplink
                }
                
            }
        
        }
            
      else if strDeeplink.contains("catalog/product"){
            
            DispatchQueue.main.async {
                let productArray = strDeeplink.components(separatedBy: "catalog/product/")
                var productID = ""
                if productArray.count > 1{
                    productID = productArray[1]
                    
                    productID = productID.replacingOccurrences(of: "view/id/", with: "")
                }
                else
                {
                    productID = ""
                }
                
                if let topVC : UIViewController = UIApplication.topViewController() {
                    self.strDeepLinkData = ""
                    
                    if productID != ""{
                        let vc = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "productViewController") as! productViewController
                        vc.product_id = productID
                        APP_DEL.productIDglobal = productID
                        vc.hidesBottomBarWhenPushed = true
                        topVC.navigationController?.pushViewController(vc, animated: true)
                    }
                }
                else
                {
                    self.strDeepLinkData = strDeeplink
                }
            }
        
        
            
        }
 
      else if strDeeplink.contains("shop"){
            
            DispatchQueue.main.async {
                
                        var categoryID = ""
                        var FilterStringNew = ""
                        if strFilter != ""{
                            
                             let FilterString = strDeeplink.components(separatedBy: "filter?=")
                             print(FilterString)
                             
                             categoryID = FilterString.first ?? ""
                             categoryID = categoryID.replacingOccurrences(of: "shop/", with: "")
                             categoryID = categoryID.replacingOccurrences(of: "/", with: "")
                             
                            FilterStringNew = FilterString.last ?? ""
                             
                             FilterStringNew = FilterStringNew.replacingOccurrences(of: "%7B", with: "{")
                             FilterStringNew = FilterStringNew.replacingOccurrences(of: "%22", with: "\"")
                             FilterStringNew = FilterStringNew.replacingOccurrences(of: "%7D", with: "}")
                             
                             
                             print(FilterStringNew)
                        }
                        else
                        {
                            let comp_1 = strDeeplink.components(separatedBy: "shop/")
                            print(comp_1.last ?? "")
                            categoryID = comp_1.last ?? ""
                        }
                    
                        if let topVC : UIViewController = UIApplication.topViewController() {
                            self.strDeepLinkData = ""
                            if strFilter != ""{
                                APP_DEL.strFilterWithDeeplink = FilterStringNew
                            }
                
                            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HomeBrandVC") as! HomeBrandVC
                            vc.categoryImage = APP_DEL.strHomeBannerImage//categoryImage
                            vc.categoryId = categoryID
                            vc.hidesBottomBarWhenPushed = true
                            topVC.navigationController?.pushViewController(vc, animated: true)
                            
                            
                        }
                        else
                        {
                            self.strDeepLinkData = strDeeplink
                        }
            }
        }
            
      else if strDeeplink.contains("tag"){
        
         DispatchQueue.main.async {
            
            
            let FilterString = strDeeplink.components(separatedBy: "filter?=")
            print(FilterString)
            
            var FilterStringNew = FilterString.last ?? ""
            
            FilterStringNew = FilterStringNew.replacingOccurrences(of: "%7B", with: "{")
            FilterStringNew = FilterStringNew.replacingOccurrences(of: "%22", with: "\"")
            FilterStringNew = FilterStringNew.replacingOccurrences(of: "%7D", with: "}")
            
            if let topVC : UIViewController = UIApplication.topViewController() {
                self.strDeepLinkData = ""
                if let viewController =  UIStoryboard(name: "categorylayouts", bundle: nil).instantiateViewController(withIdentifier: "cedMageDefaultCollection") as?  cedMageDefaultCollection {
                    
                    UserDefaults.standard.set(FilterStringNew, forKey: "filtersToSend");

                    viewController.strTitleLayer = APP_LBL().deeplink_tag_label
                    //viewController.searchString = layer.layer_data?[index.row].tags ?? ""
                    viewController.hidesBottomBarWhenPushed = true
                    viewController.isFilterDisplay = false
                    viewController.selectedCategory = ""
                    topVC.navigationController?.pushViewController(viewController, animated: true)
                }
            
            }
            else
            {
                self.strDeepLinkData = strDeeplink
            }
            
        }
        
        
      }
        
      else if strDeeplink.contains("image"){
            
         DispatchQueue.main.async {
            
            let imageArray = strDeeplink.components(separatedBy: "image/")
                var strImagePath = ""
//                if imageArray.count > 1{
//                    strImagePath = imageArray[1]
//                    strImagePath = strImagePath.replacingOccurrences(of: "\\", with: "")
//                }
//                else
//                {
//                    strImagePath = ""
//                }
//
//
//                strImagePath =  self.ActualDeeplink.replacingOccurrences(of: "/r/image/", with: "/")
//                //let strImagePath =
            
                strImagePath = self.strDeepLinkDataTemp
                strImagePath =  strImagePath.replacingOccurrences(of: "/r/image/", with: "/")
            
            
                SKPhotoBrowserOptions.displayAction = false
                var arrImages = [SKPhoto]()
                
                let photo = SKPhoto.photoWithImageURL(strImagePath)
                photo.shouldCachePhotoURLImage = true
                arrImages.append(photo)
                
                let browser = SKPhotoBrowser(photos: arrImages)
                browser.modalPresentationStyle = .fullScreen
                browser.modalTransitionStyle = .crossDissolve
                self.window?.rootViewController?.present(browser, animated: true, completion: {})
        }
        
        }
        
        
      else if strDeeplink.contains("catalog/section"){
        
         DispatchQueue.main.async {
            
        }
        
      }
     
        
        
      else if strDeeplink.contains("product/list"){
        
         DispatchQueue.main.async {
            
            print("Product with List")
                   var categoryID = ""
                    var FilterStringNew = ""
                   
                    if strFilter != ""{
                       
                       let FilterString = strDeeplink.components(separatedBy: "filter?=")
                        print(FilterString)
                        
                        categoryID = FilterString.first ?? ""
                        categoryID = categoryID.replacingOccurrences(of: "product/list", with: "")
                        categoryID = categoryID.replacingOccurrences(of: "/", with: "")
                        
                       FilterStringNew = FilterString.last ?? ""
                        
                        FilterStringNew = FilterStringNew.replacingOccurrences(of: "%7B", with: "{")
                        FilterStringNew = FilterStringNew.replacingOccurrences(of: "%22", with: "\"")
                        FilterStringNew = FilterStringNew.replacingOccurrences(of: "%7D", with: "}")
                       
                       
                   }
                   else
                    {
                       let comp_1 = strDeeplink.components(separatedBy: "product/list/")
                       print(comp_1.last ?? "")
                       categoryID = comp_1.last ?? ""
                   }
                   
                       if let topVC : UIViewController = UIApplication.topViewController() {
                           self.strDeepLinkData = ""
                           
                           if strFilter != ""{
                               APP_DEL.strFilterWithDeeplink = FilterStringNew
                           }
                           
                           if let viewController =  UIStoryboard(name: "categorylayouts", bundle: nil).instantiateViewController(withIdentifier: "cedMageDefaultCollection") as?  cedMageDefaultCollection {
                             
                               viewController.strTitleLayer = APP_LBL().deeplink_Category_label
                               //viewController.searchString = layer.layer_data?[index.row].tags ?? ""
                               viewController.hidesBottomBarWhenPushed = true
                               viewController.isFilterDisplay = false
                               viewController.selectedCategory = categoryID
                               topVC.navigationController?.pushViewController(viewController, animated: true)
                           }
                           
                       }
                       else
                       {
                           self.strDeepLinkData = strDeeplink
                       }
            
            
            
        }
      }
        
      else if strDeeplink.contains("product"){
           
        DispatchQueue.main.async {
            
            var categoryID = ""
            var FilterStringNew = ""
            if strFilter != ""{
                
             
                let FilterString = strDeeplink.components(separatedBy: "filter?=")
                print(FilterString)
                
                categoryID = FilterString.first ?? ""
                categoryID = categoryID.replacingOccurrences(of: "product/", with: "")
                categoryID = categoryID.replacingOccurrences(of: "/", with: "")
                
               FilterStringNew = FilterString.last ?? ""
                
                FilterStringNew = FilterStringNew.replacingOccurrences(of: "%7B", with: "{")
                FilterStringNew = FilterStringNew.replacingOccurrences(of: "%22", with: "\"")
                FilterStringNew = FilterStringNew.replacingOccurrences(of: "%7D", with: "}")
                
                
                print(FilterStringNew)
            }
            else
            {
                let comp_1 = strDeeplink.components(separatedBy: "catalog/category/")
                print(comp_1.last ?? "")
                
                categoryID = comp_1.last ?? ""
                categoryID = categoryID.replacingOccurrences(of: "product/", with: "")
            }
            
         
            
          //  let urlString = strDeeplink.filter({ $0.name == "url" }).first?.value?.removingPercentEncoding
            
            if let topVC : UIViewController = UIApplication.topViewController() {
                self.strDeepLinkData = ""
               
                if strFilter != ""{
                    APP_DEL.strFilterWithDeeplink = FilterStringNew
                }
                
                if let viewController =  UIStoryboard(name: "categorylayouts", bundle: nil).instantiateViewController(withIdentifier: "cedMageDefaultCollection") as?  cedMageDefaultCollection {
                    
                  
                    
                    viewController.strTitleLayer = APP_LBL().deeplink_tag_label
                    viewController.hidesBottomBarWhenPushed = true
                    viewController.isFilterDisplay = false
                    viewController.isComesFromProductLayer = true
                    viewController.selectedCategory = categoryID
                    topVC.navigationController?.pushViewController(viewController, animated: true)
                }
                
            }
            else
            {
                self.strDeepLinkData = strDeeplink
            }
            
        }
        
        
      }
        
      else if strDeeplink.contains("search"){
      
          DispatchQueue.main.async {
        var searchString = ""
            var FilterStringNew = ""
             if strFilter != ""{
                
                let FilterString = strDeeplink.components(separatedBy: "filter?=")
                 print(FilterString)
                 
                 searchString = FilterString.first ?? ""
                 searchString = searchString.replacingOccurrences(of: "search/", with: "")
                 searchString = searchString.replacingOccurrences(of: "/", with: "")
                searchString = searchString.replacingOccurrences(of: "$_$", with: " ")

                FilterStringNew = FilterString.last ?? ""
                 FilterStringNew = FilterStringNew.replacingOccurrences(of: "%7B", with: "{")
                 FilterStringNew = FilterStringNew.replacingOccurrences(of: "%22", with: "\"")
                 FilterStringNew = FilterStringNew.replacingOccurrences(of: "%7D", with: "}")


                 print(FilterStringNew)
                
            }
            else
             {
                let productArray = strDeeplink.components(separatedBy: "search/")
                
                if productArray.count > 1{
                    searchString = productArray[1].replacingOccurrences(of: "$_$", with: " ")
                    searchString = searchString.replacingOccurrences(of: "%20", with: " ")
                }
                else
                {
                    searchString = ""
                }
            }
        
            if let topVC : UIViewController = UIApplication.topViewController() {
                
                let detail : [AnalyticKey:String] = [
                    .SearchTerm : searchString]
                API().setEvent(eventName: .Search, eventDetail: detail) { (json, err) in }
                
                Analytics.logEvent(AnalyticsEventSearch, parameters: [AnalyticsParameterSearchTerm : searchString])
                
                self.strDeepLinkData = ""
                APP_DEL.strFilterWithDeeplink = FilterStringNew
                if let viewController =  UIStoryboard(name: "categorylayouts", bundle: nil).instantiateViewController(withIdentifier: "cedMageDefaultCollection") as?  cedMageDefaultCollection {
                    viewController.strTitleLayer = APP_LBL().recommended_products.uppercased()
                    viewController.hidesBottomBarWhenPushed = true
                    viewController.searchString = searchString
                    viewController.selectedCategory = ""
                    topVC.navigationController?.pushViewController(viewController, animated: true)
                    
                }
                
            }
            else
            {
                self.strDeepLinkData = strDeeplink
            }
        
        }
            
        }
            
      else if strDeeplink.contains("cart"){
    
    DispatchQueue.main.async {
    
        if let topVC : UIViewController = UIApplication.topViewController() {
            self.strDeepLinkData = ""
            
            if topVC.tabBarController?.selectedIndex == 1 {
                
                let newStory = UIStoryboard(name: "Main", bundle: nil)
                let vc = newStory.instantiateViewController(withIdentifier: "nejreeCartController") as! nejreeCartController
                topVC.navigationController?.setViewControllers([vc], animated: false)
                
            }
            else{
                topVC.tabBarController?.selectedIndex = 1
            }
        }
        else
        {
            self.strDeepLinkData = strDeeplink
        }
        
        }
    }
            
      else if strDeeplink.contains("account"){
        DispatchQueue.main.async {
        
    
        let accountType = strDeeplink.components(separatedBy: "account/")
        if accountType.count > 1{
            let accountString = accountType[1]
            
            if UserDefaults.standard.bool(forKey: "isLogin") {
                if accountString.contains("giftcard/redeem"){
                    
                    self.autoRedeemCode = ""
                    let RedeemCodeArray = accountString.components(separatedBy: "/")
                    if RedeemCodeArray.count == 3{
                        self.autoRedeemCode = RedeemCodeArray[2]
                    }
                    
                    
                    self.isRedeemFromPushNotification = true
                    self.strDeepLinkData = ""
                    if let topVC : UIViewController = UIApplication.topViewController() {
                        if topVC.tabBarController?.selectedIndex == 4 {
                            
                            let newStory = UIStoryboard(name: "cedMageAccounts", bundle: nil)
                            let vc = newStory.instantiateViewController(withIdentifier: "nAccountController") as! nAccountController
                            topVC.navigationController?.setViewControllers([vc], animated: false)
                            
                        }
                        else{
                             NotificationCenter.default.post(name: NSNotification.Name("RefreshAccount"), object: nil)
                            topVC.tabBarController?.selectedIndex = 4
                        }
                    }
                    else
                    {
                        self.strDeepLinkData = strDeeplink
                    }
                    
                    
                }
                else if accountString.contains("security"){
                    self.isProfileFromPushNotification = true
                    self.strDeepLinkData = ""
                    if let topVC : UIViewController = UIApplication.topViewController() {
                        if topVC.tabBarController?.selectedIndex == 4 {
                            
                            let viewcontroll = UIStoryboard(name: "cedMageAccounts", bundle: nil).instantiateViewController(withIdentifier: "profileView") as! cedMageProfileView
                            topVC.navigationController?.setViewControllers([viewcontroll], animated: false)
                            
                        }
                        else{
                             NotificationCenter.default.post(name: NSNotification.Name("RefreshAccount"), object: nil)
                            topVC.tabBarController?.selectedIndex = 4
                        }
                    }
                    else
                    {
                        self.strDeepLinkData = strDeeplink
                    }
                    
                    
                    
                }
                else if accountString.contains("wishlist"){
                    
                    if let topVC : UIViewController = UIApplication.topViewController() {
                        self.strDeepLinkData = ""
                        
                        if topVC.tabBarController?.selectedIndex == 3 {
                            
                                let newStory = UIStoryboard(name: "Main", bundle: nil)
                            let vc = newStory.instantiateViewController(withIdentifier: "NejreeWishListVC") as! NejreeWishListVC
                            topVC.navigationController?.setViewControllers([vc], animated: false)
                            
                        }
                        else{
                            topVC.tabBarController?.selectedIndex = 3
                        }
                    }
                    else
                    {
                        self.strDeepLinkData = strDeeplink
                    }
                    
                    
                }
                else if accountString.contains("order") || accountString.contains("track"){
                    
                 
                    let orderArray = strDeeplink.components(separatedBy: "order/")
                    var orderID = ""
                    if orderArray.count > 1{
                        orderID = orderArray[1]
                    }
                    else
                    {
                        orderID = ""
                    }
                    self.strDeepLinkData = ""
                    if let topVC : UIViewController = UIApplication.topViewController() {
                        if topVC.tabBarController?.selectedIndex == 4 {
                            
                            if orderID != ""{
                                
                                orderID = orderID.replacingOccurrences(of: "/track", with: "")
                                self.isOrderDetailFromPushNotification = true
                                    let orderId = orderID
                                    let viewcontroll = UIStoryboard(name: "cedMageAccounts", bundle: nil).instantiateViewController(withIdentifier: "nejreeSingleOrderController") as! nejreeSingleOrderController
                                    viewcontroll.orderId = orderId
                                topVC.navigationController?.pushViewController(viewcontroll, animated: true)
                            }
                            else
                            {
                                    self.isOrderFromPushNotification = true
                                let viewcontroll = UIStoryboard(name: "cedMageAccounts", bundle: nil).instantiateViewController(withIdentifier: "cedMageOrders") as! cedMageMyOrders
                                topVC.navigationController?.setViewControllers([viewcontroll], animated: false)
                            }
                            
                            
                            
                        }
                        else{
                            
                                if orderID != ""{
                                orderID = orderID.replacingOccurrences(of: "/track", with: "")
                                self.isOrderDetailFromPushNotification = true
                                    let orderId = orderID
                                    let viewcontroll = UIStoryboard(name: "cedMageAccounts", bundle: nil).instantiateViewController(withIdentifier: "nejreeSingleOrderController") as! nejreeSingleOrderController
                                    viewcontroll.orderId = orderId
                                topVC.navigationController?.pushViewController(viewcontroll, animated: true)
                            }
                            else
                                {
                                    
                                     self.isOrderFromPushNotification = true
                                     NotificationCenter.default.post(name: NSNotification.Name("RefreshAccount"), object: nil)
                                    topVC.tabBarController?.selectedIndex = 4
                            }
                            
                            
                        }
                    }
                    
                    
                }
                
            }
            else
            {
                
                self.strDeepLinkData = strDeeplink
                if let topVC : UIViewController = UIApplication.topViewController() {
                    if let vc = UIStoryboard(name: "cedMageLogin", bundle: nil).instantiateViewController(withIdentifier: "nejreeLoginController") as? nejreeLoginController{
                        APP_DEL.isLoginFromCartList = false
                        vc.hidesBottomBarWhenPushed = true
                        topVC.navigationController?.pushViewController(vc, animated: true)
                    }
                }
            }
        }
        else{
            
            
                if UserDefaults.standard.bool(forKey: "isLogin") {
                    self.strDeepLinkData = ""
                if let topVC : UIViewController = UIApplication.topViewController() {
                    if topVC.tabBarController?.selectedIndex == 4 {
                        
                        let newStory = UIStoryboard(name: "cedMageAccounts", bundle: nil)
                        let vc = newStory.instantiateViewController(withIdentifier: "nAccountController") as! nAccountController
                        topVC.navigationController?.setViewControllers([vc], animated: false)
                        
                    }
                    else{
                        topVC.tabBarController?.selectedIndex = 4
                    }
                }
            }
            else
                {
                self.strDeepLinkData = strDeeplink
                if let topVC : UIViewController = UIApplication.topViewController() {
                    if let vc = UIStoryboard(name: "cedMageLogin", bundle: nil).instantiateViewController(withIdentifier: "nejreeLoginController") as? nejreeLoginController{
                        APP_DEL.isLoginFromCartList = false
                        vc.hidesBottomBarWhenPushed = true
                        topVC.navigationController?.pushViewController(vc, animated: true)
                    }
                }
            }
            
            
        }
        
        
    }
    
    }
        
      else if strDeeplink.contains("giftcard"){
        
           DispatchQueue.main.async {
        
            let giftArray = strDeeplink.components(separatedBy: "giftcard/")
            var giftcardID = ""
            var categoryId = "" // ashish code
            if giftArray.count > 1{
                giftcardID = giftArray[1]
                categoryId = "0"
            }
            else
            {
                giftcardID = ""
                categoryId = "0"
            }
           
            if let topVC : UIViewController = UIApplication.topViewController() {
                 self.strDeepLinkData = ""
                
                let vc = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "NejreeGiftcardVC") as! NejreeGiftcardVC
                vc.category_id = categoryId
                vc.giftCardID = giftcardID
                vc.hidesBottomBarWhenPushed = true
                topVC.navigationController?.pushViewController(vc, animated: true)
                
            }
            else
            {
                self.strDeepLinkData = strDeeplink
            }
        }
        
      }
        
      else
      {
        
        DispatchQueue.main.async {
              
                  if let topVC : UIViewController = UIApplication.topViewController() {
                      self.strDeepLinkData = ""
                      
                      if topVC.tabBarController?.selectedIndex == 2 {
                          
                          let storyboard = UIStoryboard(name: "Main", bundle: nil);
                          let viewController = storyboard.instantiateViewController(withIdentifier: "NejreeBannerVC") as! NejreeBannerVC;
                           topVC.navigationController?.setViewControllers([viewController], animated: false)
                          
                      }
                      else{
                          topVC.tabBarController?.selectedIndex = 2
                      }
                  }
                  else
                  {
                      self.strDeepLinkData = strDeeplink
                  }
                  
                }
        }
        
    }
    
    
    
    func getGiftCardData(completion: @escaping (_ res: Bool) -> ()) {

        
        if (UserDefaults.standard.bool(forKey: "isLogin")) == false {
            completion(false)
            return;
        }
        
        var postData = [String:String]()
        
        if UserDefaults.standard.bool(forKey: "isLogin") {
            let userInfoDict = UserDefaults.standard.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();

            postData["customer_id"] = userInfoDict["customerId"]!;
        }
        
        if(UserDefaults.standard.object(forKey: "cartId") != nil){
        
            postData["cart_id"] = UserDefaults.standard.object(forKey: "cartId") as? String ?? ""
        }
        
        API().callAPI(endPoint: "mobiconnect/checkout/getgiftdata", method: .POST, param: postData) { (json, err) in
            
            if err == nil {
                
                do {
                    
                    self.arrGftCardData.removeAll()
                    let catData = try json[0]["result"].rawData()
                    self.arrGftCardData = try JSONDecoder().decode([GiftItem].self, from: catData)
                    
                    completion(true)
                    
                } catch let error {
                    
                    print(error.localizedDescription)
                    completion(false)
                }
            }
        }
    }
}



extension UIViewController
{
    func isModal() -> Bool
    {
        return self.presentingViewController?.presentedViewController == self || (self.navigationController != nil && self.navigationController?.presentingViewController?.presentedViewController == self.navigationController) || self.tabBarController?.presentingViewController is UITabBarController
    }
    
}


extension UILabel{
    func flipLabel(align: NSTextAlignment){
        self.textAlignment = align
        self.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
    }
}




class Application: UIApplication,UIApplicationDelegate {

    override open var userInterfaceLayoutDirection: UIUserInterfaceLayoutDirection {

        get {

            let lg = UserDefaults.standard.value(forKey: "AppleLanguages") as! [String]

            if lg[0] == "ar" || lg[0] == "ur" {

                return .rightToLeft

            } else {

                return .leftToRight

            }

        }
    }

}

struct EnAr {
    let en : String
    let ar : String
}

extension String {
    
    func isEN() -> Bool {
        
        return (CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789").isSuperset(of: CharacterSet(charactersIn: self)))
    }
    
    func getArToEnDigit() -> String {
        
        let arDigit: [EnAr] = [
            EnAr(en: "0", ar: "٠"),
            EnAr(en: "1", ar: "١"),
            EnAr(en: "2", ar: "٢"),
            EnAr(en: "3", ar: "٣"),
            EnAr(en: "4", ar: "٤"),
            EnAr(en: "5", ar: "٥"),
            EnAr(en: "6", ar: "٦"),
            EnAr(en: "7", ar: "٧"),
            EnAr(en: "8", ar: "٨"),
            EnAr(en: "9", ar: "٩")
        ]

        var res = self
        for digit in arDigit {
            res = res.replacingOccurrences(of: digit.ar, with: digit.en)
        }
        
        return res;
    }
}

extension UIViewController{
    
    //Call this once to dismiss open keyboards by tapping anywhere in the view controller
    func setupHideKeyboardOnTap() {

    self.view.addGestureRecognizer(self.endEditingRecognizer())
    self.navigationController?.navigationBar.addGestureRecognizer(self.endEditingRecognizer())
    }

    private func endEditingRecognizer() -> UIGestureRecognizer {

    let tap = UITapGestureRecognizer(target: self, action: #selector(self.callEndEditing))
    tap.cancelsTouchesInView = false
    return tap
    }

    @objc private func callEndEditing() {
        self.view.endEditing(true)
    }
    
}

extension UIApplication {

    var statusBarView: UIView? {
        return value(forKey: "statusBar") as? UIView
    }

}

extension UIView{
    public func cardView(){
        self.layer.cornerRadius = 2
        self.layer.masksToBounds = false
        self.layer.shadowOffset = CGSize(width: 3, height: 3)
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.2
    }
    
    public func productCardView(){
          self.layer.cornerRadius = 2
          self.layer.masksToBounds = false
        self.layer.shadowOffset = CGSize(width: 1.5, height: 1.5)
          self.layer.shadowColor = UIColor.black.cgColor
          self.layer.shadowOpacity = 0.1
    
      }
    
    public func imageShadow() {
        self.layer.masksToBounds = false
        self.layer.shadowOffset = CGSize(width: 3, height: 3)
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.3
    }
    
    public func properLightShadow() {
//        self.layer.masksToBounds = false
//        self.layer.shadowOffset = CGSize(width: 2, height: 2)
//        self.layer.shadowColor = UIColor.lightGray.cgColor
//        self.layer.shadowOpacity = 0.3
        
        self.layer.masksToBounds = false
        self.layer.cornerRadius = 6.0
        self.layer.borderWidth = 0.5
        self.layer.borderColor = UIColor.lightGray.cgColor
         self.layer.shadowColor = UIColor.lightGray.cgColor
//         self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: self.layer.cornerRadius).cgPath
        self.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        self.layer.shadowOpacity = 0.3
        self.layer.shadowRadius = 1.0
    }
    
}

// MARK :- REDIRECTION HANDLE
extension AppDelegate {
    
    func needToShowOtpScreenOrNot() -> Bool {
            if (APP_DEL.selectedCountry.country_code != "SA"){
                if (allow_otp_non_saudi == "1"){
                    return true
                } else {
                    return false
                }
            }
            
            return true
        }
    
    func getCountryFromUserDefault() -> Bool {
        
        if  (APP_UDS.value(forKey: Constants().USERDEFAULT_KEY_SELECTED_COUNTRY) != nil), let dict  = APP_UDS.value(forKey:Constants().USERDEFAULT_KEY_SELECTED_COUNTRY)
        {
            let dictCountry : [String : Any] = dict as! [String : Any]
            if let jsonData = try? JSONSerialization.data(withJSONObject: dictCountry, options: [.prettyPrinted])
            {
                do
                {
                    let countryRec = try JSONDecoder().decode(countryData.self, from: jsonData)
                    APP_DEL.selectedCountry = countryRec
                    print("SelectedCountry :-", countryRec)
                    codTotalToDiableOption = Int(APP_DEL.selectedCountry.CODDisableAfterTotal ?? 1200)
                    APP_DEL.extrafee_minimum_order_amount = "\(Int(APP_DEL.selectedCountry.extrafee_minimum_order_amount ?? 0) )"
                    return true
                } catch {
                    return false
                }
            }
        }
        return false
    }
    
    func setTabBarAfterAPIsCalling(){
        

        let window = UIApplication.shared.keyWindow!
        let test = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: Constants().SCREEN_NAME_DummySplashVC) as! DummySplashVC
        dummySplash = test.view!
        dummySplash.frame = window.bounds
        window.addSubview(dummySplash);


        APP_DEL.changeLanguage()


        APP_DEL.getCartCount()

        if UserDefaults.standard.bool(forKey: "isLogin") {

            self.getAddress()
        }
        
        
    }
    
    func getAddress() {
            
            var postData = [String:String]()
            
            if let storeID =  UserDefaults.standard.value(forKey: "storeId") as? String {
                postData["store_id"] = storeID
            }
            
            if let userInfoDict = UserDefaults.standard.object(forKey: "userInfoDict") as? [String: String] {
                
                postData["hashkey"] = userInfoDict["hashKey"]
                postData["customer_id"] = userInfoDict["customerId"]
            }

            
            API().callAPI(endPoint: "mobiconnect/customer/address/", method: .POST, param: postData) { (json_res, err) in
                
                if err == nil {
                    
                    let json = json_res[0]
                    
                    if (json["data"]["address"].arrayValue.count > 0) {
                        var isDefaultAddressFound = false
                        
                        for address in json["data"]["address"].arrayValue {
                            
                            if address["is_default"].stringValue == "1"{
                             let  addressRecord = addressData(street: address["address"].stringValue,
                                                                city: address["city"].stringValue,
                                                                region: address["region"].stringValue,
                                                                country: address["country"].stringValue,
                                                                lastname: address["lastname"].stringValue,
                                                                firstname: address["firstname"].stringValue,
                                                                phone: address["phone"].stringValue,
                                                                pincode: address["pincode"].stringValue,
                                                                address_id: address["address_id"].stringValue,
                                                                region_id: address["region_id"].stringValue,
                                                                neightbour_name: address["neighbour_name"].stringValue,
                                                                selected: false)
                                isDefaultAddressFound = true
                                
                                var tempAdd = [String:Any]()
                                
                                tempAdd["name"] = addressRecord.firstname
                                tempAdd["country"] = addressRecord.country
                                tempAdd["city"] = addressRecord.city
                                tempAdd["phone"] = addressRecord.phone
                                tempAdd["street"] = addressRecord.street
                                tempAdd["neighbour"] = addressRecord.neightbour_name
                                tempAdd["entity_id"] = addressRecord.address_id
                                tempAdd["firstname"] = addressRecord.firstname ?? ""
                                tempAdd["lastname"] = addressRecord.lastname ?? ""
          
                                
                                tempAddressObjApplePay = tempAdd
                                
                                break;
                            }
                            
                        }
                        
                        
                        if (isDefaultAddressFound == false){
                            
                            if json["data"]["address"].arrayValue.count > 0 {

                                let address = json["data"]["address"].arrayValue[0]
                                    let   addressRecord = addressData(street: address["address"].stringValue,
                                                                    city: address["city"].stringValue,
                                                                    region: address["region"].stringValue,
                                                                    country: address["country"].stringValue,
                                                                    lastname: address["lastname"].stringValue,
                                                                    firstname: address["firstname"].stringValue,
                                                                    phone: address["phone"].stringValue,
                                                                    pincode: address["pincode"].stringValue,
                                                                    address_id: address["address_id"].stringValue,
                                                                    region_id: address["region_id"].stringValue,
                                                                    neightbour_name: address["neighbour_name"].stringValue,
                                                                    selected: false)
                                    
                                    var tempAdd = [String:Any]()
                                    
                                    tempAdd["name"] = addressRecord.firstname
                                    tempAdd["country"] = addressRecord.country
                                    tempAdd["city"] = addressRecord.city
                                    tempAdd["phone"] = addressRecord.phone
                                    tempAdd["street"] = addressRecord.street
                                    tempAdd["neighbour"] = addressRecord.neightbour_name
                                    tempAdd["entity_id"] = addressRecord.address_id
                                    tempAdd["firstname"] = addressRecord.firstname ?? ""
                                    tempAdd["lastname"] = addressRecord.lastname ?? ""
              
                                 
      
                            
                            tempAddressObjApplePay = tempAdd
                            } else {
                                tempAddressObjApplePay = nil
                            }
                            
                            
                            
                            
                       
                            
                            
                        }
                        
                    
                    }
                }
            }
        }
    
    func showAlertWithOkButton(parentVC: UIViewController, strTitle : String, strMsg : String){
            let actionsheet = UIAlertController(title: strTitle, message: strMsg, preferredStyle: IS_IPAD ? .alert : .alert)
                    
            actionsheet.addAction(UIAlertAction(title: APP_LBL().ok.uppercased(), style: UIAlertAction.Style.default,handler: {
                action -> Void in
                
               
            }))

            
            parentVC.present(actionsheet, animated: true, completion: nil)
        }
}

extension AppDelegate: AdjustDelegate {
    
    func adjustEventTrackingSucceeded(_ eventSuccessResponseData: ADJEventSuccess?) {
        
        print("EventTrackingSucceeded:-")
        print(eventSuccessResponseData?.message ?? "")
    }
    
    func adjustEventTrackingFailed(_ eventFailureResponseData: ADJEventFailure?) {
        
        print("EventTrackingFailed:-")
        print(eventFailureResponseData?.message ?? "")
    }
    
    func adjustSessionTrackingSucceeded(_ sessionSuccessResponseData: ADJSessionSuccess?) {
        
        print("SessionTrackingSucceeded:-")
        print(sessionSuccessResponseData?.message ?? "")
    }
    
    func adjustSessionTrackingFailed(_ sessionFailureResponseData: ADJSessionFailure?) {
        
        print("SessionTrackingFailed:-")
        print(sessionFailureResponseData?.message ?? "")
    }
    
    func adjustDeeplinkResponse(_ deeplink: URL?) -> Bool {
        print("adjustDeeplinkResponse:-\(String(describing: deeplink))")
        return true
    }
}
