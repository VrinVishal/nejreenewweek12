//
//  cedMageProfileVerifyOtp.swift
//  MageNative Magento Platinum
//
//  Created by cedcoss on 22/08/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit
import FirebaseAuth
class cedMageProfileVerifyOtp: cedMageViewController {
    
    var postData =  [String:String]()
    
    @IBOutlet weak var txtStep_1: UITextField!
    @IBOutlet weak var txtStep_2: UITextField!
    
    @IBOutlet weak var otpStack: UIStackView!
    
    @IBOutlet weak var otp_1: UITextField!
    @IBOutlet weak var otp_2: UITextField!
    @IBOutlet weak var otp_3: UITextField!
    @IBOutlet weak var otp_4: UITextField!
    @IBOutlet weak var otp_5: UITextField!
    @IBOutlet weak var otp_6: UITextField!
    
    @IBOutlet weak var continueButton: UIButton!
    
    @IBOutlet weak var resendButton: UIButton!
    
     @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var lblResend: UILabel!
    @IBOutlet weak var lblNotReceived: UILabel!
    @IBOutlet weak var lblDigitCode:  UILabel!
    
    
    
    
    var isFromLogin = false
    
    var enteredOTP = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        continueButton.addTarget(self, action: #selector(self.continueTapped(_:)), for: .touchUpInside)
         resendButton.addTarget(self, action: #selector(self.resendButtonTapped(_:)), for: .touchUpInside)
        
        if APP_DEL.selectedLanguage == Arabic{
           
           btnBack.setImage(UIImage(named: "BackArrowNewArabic"), for: .normal)
           
       } else {
         
           btnBack.setImage(UIImage(named: "BackArrowNew"), for: .normal)
       }
        
        
        otpStack.semanticContentAttribute = .forceLeftToRight
        
        otp_1.delegate = self
        otp_2.delegate = self
        otp_3.delegate = self
        otp_4.delegate = self
        otp_5.delegate = self
        otp_6.delegate = self
        
        otp_1.keyboardType = .asciiCapableNumberPad
        otp_2.keyboardType = .asciiCapableNumberPad
        otp_3.keyboardType = .asciiCapableNumberPad
        otp_4.keyboardType = .asciiCapableNumberPad
        otp_5.keyboardType = .asciiCapableNumberPad
        otp_6.keyboardType = .asciiCapableNumberPad
        
        self.txtStep_1.setBorder()
        self.txtStep_1.setCornerRadius()
        txtStep_1.layer.borderColor = nejreeColor?.cgColor
        
        self.txtStep_2.setBorder()
        self.txtStep_2.setCornerRadius()
        
        //resendButton.titleLabel?.text = APP_LBL().resend
        
        //resendButton.setTitle(APP_LBL().resend, for: .normal)
        headingLabel.text = APP_LBL().verify_phone_number.uppercased()
        lblDigitCode.text = APP_LBL().a_six_digit_code_was_sent_to_your_phone_number
        //otpTextField.placeholder = APP_LBL().six_digit_code
        continueButton.setTitle(APP_LBL().continue_c.uppercased(), for: .normal)
        
        lblResend.text = APP_LBL().resend.uppercased()
        lblNotReceived.text = APP_LBL().didnt_receive_otp + " - "
        
        otp_1.setCornerRadius()
        otp_2.setCornerRadius()
        otp_3.setCornerRadius()
        otp_4.setCornerRadius()
        otp_5.setCornerRadius()
        otp_6.setCornerRadius()
        
        otp_1.setBorder()
        otp_1.layer.borderColor = UIColor.white.cgColor
        otp_2.setBorder()
        otp_2.layer.borderColor = UIColor.white.cgColor
        otp_3.setBorder()
        otp_3.layer.borderColor = UIColor.white.cgColor
        otp_4.setBorder()
        otp_4.layer.borderColor = UIColor.white.cgColor
        otp_5.setBorder()
        otp_5.layer.borderColor = UIColor.white.cgColor
        otp_6.setBorder()
        otp_6.layer.borderColor = UIColor.white.cgColor

        
        continueButton.setCornerRadius()
        continueButton.setBorder()
        continueButton.layer.borderColor = UIColor.white.cgColor
        continueButton.setTitleColor(UIColor.white, for: .normal)
        continueButton.backgroundColor = UIColor.black
        
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func btnPopAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }

    func updateOtpUI() {
        
        var isValid = true
        
        let arrTxt = [otp_1, otp_2, otp_3, otp_4, otp_5, otp_6]
        
        for txt in arrTxt {
            
            if ((txt?.text ?? "") == "") || ((txt?.text ?? "") == " ") {
                txt?.layer.borderColor = nejreeColor?.cgColor
                txt?.textColor = UIColor.white
                
                isValid = false
                
            } else {
                                
                txt?.layer.borderColor = UIColor.white.cgColor
                txt?.textColor = nejreeColor
            }
        }
        
        if isValid {
            
            continueButton.layer.borderColor = nejreeColor?.cgColor
            continueButton.setTitleColor(UIColor.white, for: .normal)
            continueButton.backgroundColor = nejreeColor
            
        } else {
            
            continueButton.layer.borderColor = UIColor.white.cgColor
            continueButton.setTitleColor(UIColor.white, for: .normal)
            continueButton.backgroundColor = UIColor.black
        }
        
    }
    
    @objc func resendButtonTapped(_ sender: UIButton) {
        
        var lang_value = ""
        
        if APP_DEL.selectedLanguage == Arabic{
            lang_value = "2"
        } else {
            lang_value = "1"
        }
        
        var params = ["telephone": postData["mobile_numbers"] ?? "", "store_id" : lang_value]
        
        if let userInfoDict = UserDefaults.standard.object(forKey: "userInfoDict") as? [String: String] {
            
            params["hashkey"] = userInfoDict["hashKey"]
            params["customer_id"] = userInfoDict["customerId"]
        }
        params["country_id"] =  APP_DEL.selectedCountry.country_code ?? "SA"
      
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);
        
        API().callAPI(endPoint: "mobiconnect/customer/otpdata", method: .POST, param: params) { (json_res, err) in
            
            DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                if err == nil {
                    
                    self.view.makeToast(APP_LBL().otp_sent_success, duration: 1.0, position: .center)
                }
            }
        }
        
//        PhoneAuthProvider.provider().verifyPhoneNumber(postData["mobile_numbers"] ?? "", uiDelegate: nil) { (secretKey, error) in
//
//            if error == nil {
//
//                guard let verificationKey = secretKey else {return}
//                self.defaults.set(verificationKey, forKey: "mobileVerificationId")
//                self.defaults.synchronize()
//
//                self.view.makeToast("OTP sent Successfully", duration: 1.0, position: .center)
//            }else {
//                print("error is \(error?.localizedDescription)")
//                self.view.makeToast((error?.localizedDescription)!, duration: 2.0, position: .center)
//            }
//
//
//        }
    }
    
    
    @objc func continueTapped(_ sender: UIButton) {
        
        var navigationArray = self.navigationController?.viewControllers //To get all UIViewController stack as Array
        
        if(navigationArray!.count > 2){
            navigationArray!.remove(at: (navigationArray?.count)! - 2) // To remove previous UIViewController
            self.navigationController?.viewControllers = navigationArray!
        }
        
        var otp = otp_1.text!
        otp = otp + otp_2.text!
        otp = otp + otp_3.text!
        otp = otp + otp_4.text!
        otp = otp + otp_5.text!
        otp = otp + otp_6.text!
        
        if otp == "" {
            
            self.view.makeToast(APP_LBL().please_enter_six_digit_otp, duration: 1.0, position: .center); return
        }
        
        var param = ["telephone": postData["mobile_numbers"] ?? "", "numberotp" : otp]
        
        if let userInfoDict = UserDefaults.standard.object(forKey: "userInfoDict") as? [String: String] {
            
            param["hashkey"] = userInfoDict["hashKey"]
            param["customer_id"] = userInfoDict["customerId"]
        }
        
        if let storeID =  UserDefaults.standard.value(forKey: "storeId") as? String {
            param["store_id"] = storeID
        }
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self)
        
        API().callAPI(endPoint: "mobiconnect/customer/verifyotp", method: .POST, param: param) { (json_res, err) in
            
            DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                if err == nil {
                    
                    if json_res[0]["code"] == 0 {
                        
                        self.view.makeToast(APP_LBL().please_enter_six_digit_otp, duration: 1.0, position: .center)
                        return
                    }
                    
                    self.view.makeToast(APP_LBL().otp_verification_successful, duration: 2.0, position: .center, title: nil, image: nil, style: nil, completion: { (comp) in
                        
                        self.saveMobile(mobile: self.postData["m_number"]!)

                    })
                }
            }
        }
    }
    
    //MARK:- SAVE MOBILE
    func saveMobile(mobile: String) {
        
        var param = [String:String]()

        if (self.defaults.object(forKey: "userInfoDict") != nil) {
            
            let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
            param["hashkey"] = userInfoDict["hashKey"]!
            param["customer_id"] = userInfoDict["customerId"]!;
        }
        
        if let storeId = defaults.value(forKey: "storeId") as? String {
            param["store_id"] = storeId
        }
        
        param["mobile_numbers"] = mobile
        
        param["phonecode"] = "+" + (APP_DEL.selectedCountry.phone_code ?? "966")
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);
        
        API().callAPI(endPoint: "mobiconnect/saveMobile", method: .POST, param: param) { (json_res, err) in
            
            DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                if err == nil {
                    
                    if json_res[0]["data"]["customer"][0]["status"].stringValue == "success" {
                        
                        self.defaults.setValue(self.postData["m_number"], forKey: "mobile_number")
                        
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "numberAdded"), object: nil);

                        if self.isFromLogin {
                            
                            self.defaults.set(true, forKey: "isLogin")
                            
                            NotificationCenter.default.post(name: NSNotification.Name("RefreshAccount"), object: nil)
                            
                            self.navigationController?.popToRootViewController(animated: true)
                            
                        } else {
                            
                            self.navigationController?.popViewController(animated: true)
                        }
                    }
                }
            }
        }
    }

}

extension cedMageProfileVerifyOtp: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == otp_1 {
            otp_2.becomeFirstResponder()
        } else if textField == otp_2 {
            otp_3.becomeFirstResponder()
        } else if textField == otp_3 {
            otp_4.becomeFirstResponder()
        } else if textField == otp_4 {
            otp_5.becomeFirstResponder()
        } else if textField == otp_5 {
            otp_6.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let validString : String = "0123456789٠١٢٣٤٥٦٧٨٩"
        
        if string.isEmpty == false && validString.contains(string) == false{
            return false;
        }
        
        if textField.text == "" {
            textField.text = ""
        }
        
        if otp_1.text != "" && otp_2.text != "" && otp_3.text != "" && otp_4.text != "" && otp_5.text != "" && otp_6.text != ""  {
            
             txtStep_2.layer.borderColor = nejreeColor?.cgColor
            
        }
//        else
//        {
//            txtStep_1.layer.borderColor = UIColor.white.cgColor
//        }
        
        
        if textField == otp_1 {
            
            if !(string == "") {
                
                textField.text = string
                otp_2.becomeFirstResponder()
                return false
            }
            
        } else if textField == otp_2 {
            
            if (string == "") {
                
                textField.text = " "
                otp_1.becomeFirstResponder()
                return false
                
            } else {
                
                textField.text = string
                otp_3.becomeFirstResponder()
                return false
            }
            
        } else if textField == otp_3 {
            
            if (string == "") {
                
                textField.text = " "
                otp_2.becomeFirstResponder()
                return false
                
            } else {
                textField.text = string
                otp_4.becomeFirstResponder()
                return false
            }
            
        } else if textField == otp_4 {
            
            if (string == "") {
                
                textField.text = " "
                otp_3.becomeFirstResponder()
                return false
                
            } else {
                
                textField.text = string
                otp_5.becomeFirstResponder()
                return false
            }
            
        } else if textField == otp_5 {
            
            if (string == "") {
                
                textField.text = " "
                otp_4.becomeFirstResponder()
                return false
                
            } else {
                
                textField.text = string
                otp_6.becomeFirstResponder()
                
                return false
            }
            
        } else if textField == otp_6 {
            
            if (string == "") {
                
                textField.text = " "
                otp_5.becomeFirstResponder()
                return false
                
            } else {
                
                textField.text = string
                txtStep_2.layer.borderColor = nejreeColor?.cgColor
                textField.resignFirstResponder()
                return false
            }
            
        }
        
        
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        self.updateOtpUI()
    }
    
    /*
    func textField(_ textField: UITextField, shouldChangeCharactersIn range:NSRange, replacementString string: String) -> Bool {

        if (textField == otp_1) || (textField == otp_2) || (textField == otp_3) || (textField == otp_4) || (textField == otp_5) || (textField == otp_6) {
            
            let tag : Int = textField.tag
            let text = textField.text ?? ""

            if (string == "") {

                if (text.count > 0) && (tag > 1) {
                    
                    let txt_current = otpStack.viewWithTag(tag) as! UITextField
                    txt_current.text = ""
                    
                    let txt_back = otpStack.viewWithTag(tag - 1) as! UITextField
                    txt_back.becomeFirstResponder()
                    
                } else if (tag > 1) {
                    
                    textField.text = ""
                    
                    let temptf = otpStack.viewWithTag(tag - 1) as! UITextField
                    temptf.becomeFirstResponder()
                }
                
            } else {

                if (tag < 6) {
                    
                    textField.text = string
                    
                    let temptf = otpStack.viewWithTag(tag + 1) as! UITextField
                    temptf.becomeFirstResponder()
                    
                } else if (tag == 6) {
                    
                    textField.resignFirstResponder()
                    
                    //call api here
                    
                    return false
                }
            }

            return true
        }

        return true

    }


    func textFieldDidEndEditing(_ textField: UITextField) {
        

//        guard let str = textField.text else {return}
//
//            if str.count > 6 || str.count < 6 {
//                otpImage.image = UIImage(named: "wrongname")
//                otpImage.isHidden = false
//                otpView.layer.borderColor = UIColor.init(hexString: "#AF1F45")?.cgColor
//            } else {
//                otpImage.image = UIImage(named: "verified")
//                otpImage.isHidden = false
//                otpView.layer.borderColor = nejreeColor?.cgColor
//            }
        }

    */
    
}
