/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */


import UIKit

class cedMageProfileView: cedMageViewController, UIGestureRecognizerDelegate {
    
    @IBOutlet weak var headingLabel: UILabel!
    
    @IBOutlet weak var lblpersonalInfo: UILabel!
    
    @IBOutlet weak var name: SkyFloatingLabelTextField!
    @IBOutlet weak var emailField: SkyFloatingLabelTextField!
    @IBOutlet weak var mobileNumber: UILabel!
    @IBOutlet weak var oldPassword: SkyFloatingLabelTextField!
    @IBOutlet weak var newPassword: SkyFloatingLabelTextField!
    @IBOutlet weak var confirmPassword: SkyFloatingLabelTextField!
    @IBOutlet weak var lblChnagePhoneNumber: UILabel!
    @IBOutlet weak var lblCountryCode: UILabel!
    
    @IBOutlet weak var btnback: UIButton!
    @IBOutlet weak var upDateData: UIButton!
    @IBOutlet weak var nameImage: UIImageView!
    @IBOutlet weak var emailImage: UIImageView!
    @IBOutlet weak var lNameSideView: UIView!
    @IBOutlet weak var lNameImage: UIImageView!
    
    @IBOutlet weak var nameView: UIView!
    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var phoneView: UIView!
    @IBOutlet weak var lNameView: UIView!
    
    @IBOutlet weak var oldPasswordView: UIView!
    @IBOutlet weak var newPasswordView: UIView!
    @IBOutlet weak var confirmPasswordView: UIView!
    
    @IBOutlet weak var oldPasswordImage: UIImageView!
    @IBOutlet weak var newPasswordImage: UIImageView!
    @IBOutlet weak var confirmPasswordImage: UIImageView!
    
    @IBOutlet weak var lName: SkyFloatingLabelTextField!
    @IBOutlet weak var lNameValidImage: UIImageView!
    
    @IBOutlet weak var changePhoneNumber: UIButton!
    
 
    
    
    //@IBOutlet weak var myProfileLabel: UILabel!
    //@IBOutlet weak var rewuiredFieldlabel: UILabel!
    @IBOutlet weak var passwordAndSecurityLabel: UILabel!
    @IBOutlet weak var weAdvicelabel: UILabel!
    
    @IBOutlet weak var nameSideView: UIView!
    @IBOutlet weak var emailSideView: UIView!
    
    @IBOutlet weak var namelockImage: UIImageView!
    @IBOutlet weak var emaillockImage: UIImageView!
    
    var jsonData = [String:String]()
    
    let CharSet = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ")
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        lblCountryCode.text = "+" + (APP_DEL.selectedCountry.phone_code ?? "966")
            APP_DEL.isProfileFromPushNotification = false
        lblCountryCode.text = ""
        
        headingLabel.text = APP_LBL().profile.uppercased()
        headingLabel.textAlignment = .left
         //self.navigationController?.navigationBar.isHidden = false
        
        if APP_DEL.selectedLanguage == Arabic{
            emailField.isLTRLanguage = false
            name.isLTRLanguage = false;
            lName.isLTRLanguage = false
            oldPassword.isLTRLanguage = false
            newPassword.isLTRLanguage = false;
            confirmPassword.isLTRLanguage = false
            
            btnback.setImage(UIImage(named: "BackArrowNewArabic"), for: .normal)
            
        } else {
            emailField.isLTRLanguage = true
            name.isLTRLanguage = true;
            lName.isLTRLanguage = true
            oldPassword.isLTRLanguage = true
            newPassword.isLTRLanguage = true;
            confirmPassword.isLTRLanguage = true
            
            btnback.setImage(UIImage(named: "BackArrowNew"), for: .normal)
            
        }
        
        
        self.phoneView.semanticContentAttribute = .forceLeftToRight
        self.mobileNumber.textAlignment = .left
        
        let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
        //getCornerRound(sender: changePassword)
        emailField.text = userInfoDict["email"]
        upDateData.addTarget(self, action: #selector(self.updatePass(sender:)), for: .touchUpInside)

        
        //viewHeight.constant = 700
        name.delegate = self
        lName.delegate = self
        emailField.delegate = self
        oldPassword.delegate = self
        newPassword.delegate = self
        confirmPassword.delegate = self
        
//        myProfileLabel.text = APP_LBL().my_profile
//        rewuiredFieldlabel.text = APP_LBL().required_fields.uppercased()
        passwordAndSecurityLabel.text = APP_LBL().password_and_security.uppercased()
        weAdvicelabel.text = APP_LBL().we_advice_you_to_change_your_password_every_month_for_more_security
        weAdvicelabel.textColor = UIColor.white
        
        name.placeholder = " "+APP_LBL().firstname_star
        lName.placeholder = APP_LBL().last_name_star
        emailField.placeholder = APP_LBL().email_star.uppercased()
        
        oldPassword.placeholder = APP_LBL().old_password
        newPassword.placeholder = APP_LBL().new_password
        confirmPassword.placeholder = APP_LBL().confirm_password
        
        changePhoneNumber.setTitle("", for: .normal)
        self.lblChnagePhoneNumber.text = APP_LBL().change_phone_number.uppercased()
        //changePassword.setTitle(APP_LBL().change_password.uppercased(), for: .normal)
        
        
        mobileNumber.text = UserDefaults.standard.value(forKey: "mobile_number") as? String
        
        //self.navigationItem.title = APP_LBL().profile_and_security
        
        nameView.setCornerRadius()//roundCornersWithShadow()
        lNameView.setCornerRadius()//roundCornersWithShadow()
        emailView.setCornerRadius()//roundCornersWithShadow()
        phoneView.setCornerRadius()//roundCornersWithShadow()
        oldPasswordView.setCornerRadius()
        newPasswordView.setCornerRadius()
        confirmPasswordView.setCornerRadius()
        
        nameView.setBorder()
        nameView.layer.borderColor = nejreeColor?.cgColor
        lNameView.setBorder()
        lNameView.layer.borderColor = nejreeColor?.cgColor
        emailView.setBorder()
        emailView.layer.borderColor = nejreeColor?.cgColor
        phoneView.setBorder()
        phoneView.layer.borderColor = UIColor.gray.cgColor
        
        oldPasswordView.setBorder()
        newPasswordView.setBorder()
        confirmPasswordView.setBorder()
        
        oldPasswordView.layer.borderColor = UIColor.white.cgColor
        newPasswordView.layer.borderColor = UIColor.white.cgColor
        confirmPasswordView.layer.borderColor = UIColor.white.cgColor
        
        //changePhoneNumber.setCornerRadius()//roundCorners()
        //changePassword.setCornerRadius()//roundCorners()
        
        lblpersonalInfo.text = APP_LBL().personal_info.uppercased()
        
        nameSideView.backgroundColor = .black
        emailSideView.backgroundColor = .black
        lNameSideView.backgroundColor = .black
        namelockImage.roundImageAndBorderColor()
        emaillockImage.roundImageAndBorderColor()
        lNameImage.roundImageAndBorderColor()
        //changePhoneNumber.setBorder()
        //changePassword.setBorder()
        upDateData.backgroundColor = .black
        upDateData.setBorder()
        upDateData.setCornerRadius()
        upDateData.setTitle(APP_LBL().update_my_profile.uppercased(), for: .normal)
        upDateData.layer.borderColor = UIColor.white.cgColor
        upDateData.setTitleColor(UIColor.white, for: .normal)
        upDateData.isHidden = false
        
        
        //changePassword.addTarget(self, action: #selector(changePasswordTapped(_:)), for: .touchUpInside)
        changePhoneNumber.addTarget(self, action: #selector(changePhoneNumber(_:)), for: .touchUpInside)
        
        emailField.isEnabled = false
        emailImage.isHidden = false
        nameImage.isHidden = false
        lNameValidImage.isHidden = false
        self.emailImage.image = UIImage(named: "iconCorrectCheckMark")
        self.nameImage.image = UIImage(named: "iconCorrectCheckMark")
        self.lNameValidImage.image = UIImage(named: "iconCorrectCheckMark")
        
        oldPasswordImage.image = UIImage(named: "")
        newPasswordImage.image = UIImage(named: "")
        confirmPasswordImage.image = UIImage(named: "")
        
        
//        if #available(iOS 11.0, *) {
//            emaillockImage.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMaxYCorner]
//            namelockImage.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMaxYCorner]
//            lNameImage.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMaxYCorner]
//        } else {
//            // Fallback on earlier versions
//        }
        
           NotificationCenter.default.addObserver(self, selector: #selector(numberAdded(_:)), name: NSNotification.Name(rawValue: "numberAdded"), object: nil);
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.getRequiredFields()

        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        if navigationController?.responds(to: #selector(getter: UINavigationController.interactivePopGestureRecognizer)) ?? false {
                     
                     self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
                     self.navigationController?.interactivePopGestureRecognizer?.delegate = self
                 }
        
         
    }
    
func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
    
    return true
}
    func getCornerRound(sender:UIButton){
        sender.layer.borderWidth = 1
        sender.layer.borderColor = UIColor.gray.cgColor
    }
    
    @objc func changePasswordTapped(_ sender: UIButton) {
        let vc = UIStoryboard(name: "cedMageAccounts", bundle: nil).instantiateViewController(withIdentifier: "cedMageProfileChangePassword") as! cedMageProfileChangePassword
        vc.jsonData = jsonData
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func changePhoneNumber(_ sender: UIButton) {
        let vc = UIStoryboard(name: "cedMageAccounts", bundle: nil).instantiateViewController(withIdentifier: "cedMageProfileChangeNumber") as! cedMageProfileChangeNumber
        vc.jsonData = jsonData
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func numberAdded(_ sender: NSNotification) {
        self.mobileNumber.text = UserDefaults.standard.value(forKey: "mobile_number") as! String
        self.upDateData.isHidden = false
    }
    
    func isValidDetail() -> Bool {
        
        var isValid = true
        
        if name.text == "" {
            
            isValid = false
            return isValid
        }
        
        if lName.text == "" {
            
            isValid = false
            return isValid
        }
        
        if !(isValidEmail(email: emailField.text!)) {
            
            isValid = false
            return isValid
        }
        
        if isValid && (oldPassword.text == "") && (newPassword.text == "") && (confirmPassword.text == "") {
            
            isValid = true
            return isValid
            
        } else {
            
            if (oldPassword.text ?? "").count < 6 {
                
                isValid = false
                return isValid
            }
            
            if (newPassword.text ?? "").count < 6 {
                
                isValid = false
                return isValid
            }
            
            if (confirmPassword.text != newPassword.text) || ((confirmPassword.text ?? "").count < 6) {
                
                isValid = false
                return isValid
            }
            
        }
                
        return isValid
    }
    

    @IBAction func btnBackAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func updatePass(sender:UIButton){
         let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
        
       
        
       // guard let nameArray = name.text//name.text?.components(separatedBy: " ") else {return}
        
        let firstname = name.text//nameArray.first else {return}
        let lastname = lName.text//self.name.text?.replacingOccurrences(of: firstname, with: "").trimmingCharacters(in: .whitespacesAndNewlines)
        let mobile = mobileNumber.text!
        
        
        if firstname == ""{
            
            cedMageHttpException.showAlertView(me: self, msg: APP_LBL().please_enter_first_name, title: APP_LBL().error)
            return;
        }
        else if lastname == ""{
            cedMageHttpException.showAlertView(me: self, msg: APP_LBL().please_enter_last_name, title: APP_LBL().error)
            return;
        }
        
        else if mobile == ""{
            cedMageHttpException.showAlertView(me: self, msg: APP_LBL().please_enter_phone_no, title: APP_LBL().error)
            return;
        }
        else if (mobile.count) != (Int(APP_DEL.selectedCountry.phone_number_limit ?? "") ?? 9) {
            
            cedMageHttpException.showAlertView(me: self, msg: APP_LBL().mobile_number_should_be_of_xyz_digits_only, title: APP_LBL().error)
            return;
        }
        
     
        
        if (self.oldPassword.text != "") || (self.newPassword.text != "") || (self.confirmPassword.text != "") {
            
            if (oldPassword.text ?? "") == "" {
                
                cedMageHttpException.showAlertView(me: self, msg: APP_LBL().please_enter_old_password, title: APP_LBL().error)
                return;
            }
            
            if (newPassword.text ?? "") == "" {
                
                cedMageHttpException.showAlertView(me: self, msg: APP_LBL().please_enter_new_password, title: APP_LBL().error)
                return;
            }
            
            if (newPassword.text ?? "").count < 6 {
                
                cedMageHttpException.showAlertView(me: self, msg: APP_LBL().please_enter_valid_password, title: APP_LBL().error)
                return;
            }
            
            if (confirmPassword.text ?? "") == "" {
                
                cedMageHttpException.showAlertView(me: self, msg: APP_LBL().please_enter_cnfrm_password, title: APP_LBL().error)
                return;
            }
            
            if (confirmPassword.text ?? "").count < 6 {
                
                cedMageHttpException.showAlertView(me: self, msg: APP_LBL().please_enter_valid_password, title: APP_LBL().error)
                return;
            }
            
            if (confirmPassword.text != newPassword.text) {
                
                cedMageHttpException.showAlertView(me: self, msg: APP_LBL().password_not_match, title: APP_LBL().error)
                return;
            }
            
           let postString = ["new_password":newPassword.text!,
                              "email":emailField.text!,
                              "confirm_password":confirmPassword.text!,
                              "old_password":oldPassword.text!,
                              "change_password":"1",
                              "firstname":firstname! as String,
                              "lastname":lastname,
                              "mobile_numbers":mobile]
            
             self.update(param: postString as! [String : String])
            
        }
        else
        {
             let postString = ["email":emailField.text!,
                                 "firstname":firstname,
                                 "lastname":lastname,
                                 "mobile_numbers":mobile]
            
            self.update(param: postString as! [String : String])
        }
 
       
        
    }
    
    func getRequiredFields() {
        
        let c_id = UserDefaults.standard.value(forKey: "userInfoDict") as! [String:String]
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);
        
        API().callAPI(endPoint: "mobiconnect/customer/getRequiredFields/\(c_id["customerId"]!)", method: .GET, param: [:]) { (json_res, err) in
            
            DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                if err == nil {
                    
                    self.jsonData["firstname"] = json_res[0]["firstname"].stringValue
                    self.jsonData["lastname"] = json_res[0]["lastname"].stringValue
                    self.name.text = (self.jsonData["firstname"] ?? "")
                    self.lName.text = (self.jsonData["lastname"] ?? "")
                    UserDefaults.standard.set(self.jsonData["firstname"]! + " " + self.jsonData["lastname"]!, forKey: "name")
                    
                    UserDefaults.standard.synchronize()
                    
                    let tempArray = json_res[0]["data"].arrayValue
                    for rec in tempArray {
                        if let recName = rec["name"].stringValue as? String{
                            if recName == "phone_code" {
                                self.lblCountryCode.text = rec["value"].stringValue ?? ""
                            }
                        }
                    }
                    
                    
                    if (self.lblCountryCode.text == ""){
                        self.lblCountryCode.text = "+" + (APP_DEL.selectedCountry.phone_code ?? "966")
                    }
                    
//                    if let countryCode = json_res[0]["data"]["6"]["value"].stringValue as? String {
//                        self.lblCountryCode.text = countryCode
//                    } else {
//                        self.lblCountryCode.text = "+" + (APP_DEL.selectedCountry.phone_code ?? "966")
//                    }
                }
            }
        }
    }
    
    func update(param: [String:String]) {
        
        var newPram = param
        
        if let userInfoDict = UserDefaults.standard.object(forKey: "userInfoDict") as? [String: String] {
            
            newPram["hashkey"] = userInfoDict["hashKey"]
            newPram["customer_id"] = userInfoDict["customerId"]
        }
        
        if let storeID =  UserDefaults.standard.value(forKey: "storeId") as? String {
            newPram["store_id"] = storeID
        }
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);
        
        API().callAPI(endPoint: "mobiconnect/customer/update", method: .POST, param: newPram) { (json_res, err) in
            
            DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                if err == nil {
                    
                    let json = json_res[0]
                    
                    if (json["data"]["customer"][0]["status"].stringValue == "error") {
                        
                        cedMageHttpException.showAlertView(me: self, msg: json["data"]["customer"][0]["message"].stringValue, title: APP_LBL().error)
                    }
                    
                    if (json["data"]["customer"][0]["status"].stringValue == "success") {
                        
                        
                        self.view.makeToast(APP_LBL().profile_updated_successfully, duration: 1.0, position: .center)
                        
                        UserDefaults.standard.set(json["data"]["customer"][0]["firstname"].stringValue + " " + json["data"]["customer"][0]["lastname"].stringValue, forKey: "name")
                        
                        UserDefaults.standard.synchronize()
                        
                        
                        if (self.oldPassword.text != "") || (self.newPassword.text != "") || (self.confirmPassword.text != "") {
                            
                            
                            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                                
                                if (self.defaults.bool(forKey: "isLogin") == true) {
                                    
                                    self.logout()
                                }
                            }
                        }
                    }
                    
                    if (json["data"]["customer"][0]["status"].stringValue == "false") {
                        
                        cedMageHttpException.showAlertView(me: self, msg: json["data"]["customer"][0]["message"].stringValue, title: APP_LBL().error)
                    }
                }
            }
        }
    }
    
    func logout() {
        
        var newPram = [String: String]()
        
        if let userInfoDict = UserDefaults.standard.object(forKey: "userInfoDict") as? [String: String] {
            
            newPram["hashkey"] = userInfoDict["hashKey"]
            newPram["customer_id"] = userInfoDict["customerId"]
        }
        
        if let storeID =  UserDefaults.standard.value(forKey: "storeId") as? String {
            newPram["store_id"] = storeID
        }
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);
        
        API().callAPI(endPoint: "mobiconnect/customer/logout", method: .POST, param: newPram) { (json_res, err) in
            
            DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                if err == nil {
                    
                    self.defaults.set(false, forKey: "isLogin");
                    self.defaults.removeObject(forKey: "userInfoDict");
                    self.defaults.removeObject(forKey: "selectedAddressId");
                    self.defaults.setValue("Magenative App For Magento 2", forKey: "name")
                    UserDefaults.standard.set(nil,forKey: "LIAccessToken")
                    UserDefaults.standard.synchronize();
                    self.defaults.synchronize()
                    self.defaults.removeObject(forKey: "selectedAddressDescription");
                    
                    self.defaults.removeObject(forKey: "cartId");
                    self.defaults.removeObject(forKey: "cart_summary");
                    self.defaults.removeObject(forKey: "guestEmail");
                    self.defaults.set("0", forKey: "items_count");
                    self.defaults.removeObject(forKey: "name")
                    self.setCartCount(view: self, items: "0")
                    self.navigationController?.navigationBar.isHidden = false
                    
                    APP_DEL.changeLanguage()
                }
            }
        }
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination is cedMageProfileChangePassword {
            if let viewControl = segue.destination as?  cedMageProfileChangePassword {
                viewControl.jsonData = jsonData
            }
        }else {
            if let viewControl = segue.destination as?  cedMageProfileChangeNumber {
                viewControl.jsonData = jsonData
            }
        }
    }
}


extension cedMageProfileView: UITextFieldDelegate {
    
    
   func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if (textField == mobileNumber )  {
            
            guard !string.isEmpty else {

                return true
            }
            
            if (CharacterSet(charactersIn: "0123456789").isSuperset(of: CharacterSet(charactersIn: string))) {

                if let text = textField.text, let range = Range(range, in: text) {

                    let proposedText = text.replacingCharacters(in: range, with: string)

                    if (APP_DEL.selectedCountry.country_code?.uppercased() == "SA"){
                        if proposedText.first == "5" && proposedText.count <= (Int(APP_DEL.selectedCountry.phone_number_limit ?? "") ?? 9) {
                            return true
                        }
                    } else {
                        if proposedText.count <= (Int(APP_DEL.selectedCountry.phone_number_limit ?? "") ?? 9) {
                            return true
                        }
                    }
                }
            }

            return false
        }
        else if (textField == name || textField == lName){
            
            if let x = string.rangeOfCharacter(from: NSCharacterSet.decimalDigits) {
               return false
            } else {
            return true
            }
            
//           if  (CharacterSet(charactersIn: "0123456789").isSuperset(of: CharacterSet(charactersIn: string))) {
//
//               return false
//           }
//            else
//           {
//             return true
//            }

        }
        
        
        return true
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField == name {
            
            if textField.text == "" {
                
                textField.textColor = nejreeColor
                nameView.layer.borderColor = UIColor.white.cgColor
                nameImage.image = UIImage(named: "IconIncorrectCheck")
                
            } else {
                
                textField.textColor = UIColor.white
                nameView.layer.borderColor = nejreeColor?.cgColor
                nameImage.image = UIImage(named: "iconCorrectCheckMark")
            }
            
        } else if textField == lName {
            
            if textField.text == "" {
                
                textField.textColor = nejreeColor
                lNameView.layer.borderColor = UIColor.white.cgColor
                lNameValidImage.image = UIImage(named: "IconIncorrectCheck")
                
            } else {
                
                textField.textColor = UIColor.white
                lNameView.layer.borderColor = nejreeColor?.cgColor
                lNameValidImage.image = UIImage(named: "iconCorrectCheckMark")
            }
            
        } else if textField == emailField {
            
            emailImage.isHidden = false
            if isValidEmail(email: textField.text!) {
                self.emailImage.image = UIImage(named: "iconCorrectCheckMark")
            } else {
                self.emailImage.image = UIImage(named: "IconIncorrectCheck")
            }
        }
        
        
        
        else if textField == oldPassword {
            
            if (textField.text ?? "").count < 6 {
                
                textField.textColor = nejreeColor
                oldPasswordView.layer.borderColor = UIColor.white.cgColor
                oldPasswordImage.image = UIImage(named: "IconIncorrectCheck")
                
            } else {
                
                textField.textColor = UIColor.white
                oldPasswordView.layer.borderColor = nejreeColor?.cgColor
                oldPasswordImage.image = UIImage(named: "iconCorrectCheckMark")
            }
        }
        else if textField == newPassword {
            
            if (textField.text ?? "").count < 6 {
                
                textField.textColor = nejreeColor
                newPasswordView.layer.borderColor = UIColor.white.cgColor
                newPasswordImage.image = UIImage(named: "IconIncorrectCheck")
                
            } else {
                
                textField.textColor = UIColor.white
                newPasswordView.layer.borderColor = nejreeColor?.cgColor
                newPasswordImage.image = UIImage(named: "iconCorrectCheckMark")
            }
        }
        else if textField == confirmPassword {
            
            if (textField.text != newPassword.text) || ((textField.text ?? "").count < 6) {
                
                textField.textColor = nejreeColor
                confirmPasswordView.layer.borderColor = UIColor.white.cgColor
                confirmPasswordImage.image = UIImage(named: "IconIncorrectCheck")
                
            } else {
                
                textField.textColor = UIColor.white
                confirmPasswordView.layer.borderColor = nejreeColor?.cgColor
                confirmPasswordImage.image = UIImage(named: "iconCorrectCheckMark")
            }
        }

        if isValidDetail() {
            
            upDateData.layer.borderColor = nejreeColor?.cgColor
            upDateData.backgroundColor = nejreeColor
            upDateData.setTitleColor(UIColor.white, for: .normal)
            
        } else {
            
            upDateData.layer.borderColor = UIColor.white.cgColor
            upDateData.backgroundColor = UIColor.black
            upDateData.setTitleColor(UIColor.white, for: .normal)
        }
        
//        switch textField {
//        case name:
//            let str = name.text!
//
//            if str == "" {
//                nameImage.isHidden = false
//                self.nameImage.image = UIImage(named: "wrongname")
//                self.nameView.layer.borderColor = UIColor.init(hexString: "#AF1F45")?.cgColor
//                self.namelockImage.tintColor = UIColor.init(hexString: "#AF1F45")
//                return
//            }
//
//            let decimalCharacters = CharacterSet.decimalDigits
//            let decimalRange = str.rangeOfCharacter(from: decimalCharacters)
//            nameImage.isHidden = false
//            if decimalRange != nil {
//                self.nameImage.image = UIImage(named: "wrongname")
//                 self.nameView.layer.borderColor = UIColor.init(hexString: "#AF1F45")?.cgColor
//                self.namelockImage.tintColor = UIColor.init(hexString: "#AF1F45")
//            }else {
//                self.nameImage.image = UIImage(named: "verified")
//                self.nameView.layer.borderColor = nejreeColor?.cgColor
//                self.namelockImage.tintColor = nejreeColor
//            }
//
//            self.upDateData.isHidden = false
//
//        case lName:
//            let str = lName.text!
//
//            if str == "" {
//                lNameValidImage.isHidden = false
//                self.lNameValidImage.image = UIImage(named: "wrongname")
//                self.lNameView.layer.borderColor = UIColor.init(hexString: "#AF1F45")?.cgColor
//                self.lNameImage.tintColor = UIColor.init(hexString: "#AF1F45")
//                return
//            }
//
//            let decimalCharacters = CharacterSet.decimalDigits
//            let decimalRange = str.rangeOfCharacter(from: decimalCharacters)
//            self.lNameValidImage.isHidden = false
//            if decimalRange != nil {
//                self.lNameValidImage.image = UIImage(named: "wrongname")
//                 self.lNameView.layer.borderColor = UIColor.init(hexString: "#AF1F45")?.cgColor
//                self.lNameImage.tintColor = UIColor.init(hexString: "#AF1F45")
//            }else {
//                self.lNameValidImage.image = UIImage(named: "verified")
//                self.lNameView.layer.borderColor = nejreeColor?.cgColor
//                self.lNameImage.tintColor = nejreeColor
//            }
//
//
//            self.upDateData.isHidden = false
//
//        case emailField:
//            let str = emailField.text!
//            emailImage.isHidden = false
//            if isValidEmail(email: str) {
//                self.emailImage.image = UIImage(named: "verified")
//            } else {
//                self.emailImage.image = UIImage(named: "wrongname")
//            }
//
//        default:
//            print("adghsfajsdgfasdfgdas")
//        }
    }
    
    
    
}
