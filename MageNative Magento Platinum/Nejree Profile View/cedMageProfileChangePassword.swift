//
//  cedMageProfileChangePassword.swift
//  MageNative Magento Platinum
//
//  Created by cedcoss on 21/08/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class cedMageProfileChangePassword: cedMageViewController {
    
    
    @IBOutlet weak var headingLabel: UILabel!
    
    
    @IBOutlet weak var newPassword: SkyFloatingLabelTextField!
    
    @IBOutlet weak var oldPassword: SkyFloatingLabelTextField!
    var jsonData = [String:String]()

    @IBOutlet weak var changePasswordBtn: UIButton!
    
    @IBOutlet weak var changePasswordLabel: UILabel!
    @IBOutlet weak var requiredFieldLabel: UILabel!
    @IBOutlet weak var oldPasswordView: UIView!
    @IBOutlet weak var newPasswordView: UIView!
    @IBOutlet weak var changeBtnView: UIView!
    @IBOutlet weak var oldPasswordImage: UIImageView!
    @IBOutlet weak var newPasswordImage: UIImageView!
    
    
    var restedURL = ""
    var FirstName = ""
      var LastName = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        headingLabel.text = APP_LBL().change_password.uppercased()
        
        self.navigationItem.title = APP_LBL().profile_and_security

        changePasswordLabel.text = APP_LBL().change_password.uppercased()
        requiredFieldLabel.text = APP_LBL().required_fields.uppercased()
        
        //newPassword.textAlignment = .center
        //oldPassword.textAlignment = .center
        
        newPassword.placeholder = APP_LBL().new_password.uppercased()+"*"
        oldPassword.placeholder = APP_LBL().old_password.uppercased()
        
        changePasswordBtn.setTitle(APP_LBL().change_my_password.uppercased(), for: .normal)
        
        newPasswordView.roundCorners()
        oldPasswordView.roundCorners()
        newPasswordView.setBorder()
        oldPasswordView.setBorder()
        
        changeBtnView.roundCorners()
        changeBtnView.layer.borderWidth = 1.0
        changeBtnView.layer.borderColor = UIColor.darkGray.cgColor
        changePasswordBtn.setTitleColor(.darkGray, for: .normal)
        
        newPassword.delegate = self
        oldPassword.delegate = self
        
        self.getRequiredFields()
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    @IBAction func changePassword(_ sender: Any) {
        let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
        guard let oldPassword = oldPassword.text else {return}
        guard let newPassword = newPassword.text else {return}
      
        self.view.endEditing(true)
        
              
         
        if oldPassword == ""{
             cedMageHttpException.showAlertView(me: self, msg: APP_LBL().please_enter_password, title: APP_LBL().error)
             return;
         }
         
         else if newPassword == ""{
             cedMageHttpException.showAlertView(me: self, msg: APP_LBL().please_enter_new_password, title: APP_LBL().error)
             return;
         }
        else if (newPassword.count) < 6 {
            
            cedMageHttpException.showAlertView(me: self, msg: APP_LBL().password_must_be_minimun_six_characters_long, title: APP_LBL().error)
            return;
        }

        let mobile = defaults.value(forKey: "mobile_number") as? String
        
        let postString =  ["new_password":newPassword as String,"email":userInfoDict["email"],"confirm_password":newPassword,"old_password":oldPassword,"change_password":"1" ,"firstname":FirstName as String,"lastname":LastName,"mobile_numbers":mobile!]
        self.update(param: postString as! [String : String])
    }
    
    func getRequiredFields() {
        
        let c_id = UserDefaults.standard.value(forKey: "userInfoDict") as! [String:String]
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);
        
        API().callAPI(endPoint: "mobiconnect/customer/getRequiredFields/\(c_id["customerId"]!)", method: .GET, param: [:]) { (json_res, err) in
            
            DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                if err == nil {
                    
                    let json = json_res[0]

                    self.FirstName = json["firstname"].stringValue
                    self.LastName = json["lastname"].stringValue
                    
                    
                    
                }
            }
        }
    }
    
    func update(param: [String:String]) {
        
        var newPram = param
        
        if let userInfoDict = UserDefaults.standard.object(forKey: "userInfoDict") as? [String: String] {
            
            newPram["hashkey"] = userInfoDict["hashKey"]
            newPram["customer_id"] = userInfoDict["customerId"]
        }
        
        if let storeID =  UserDefaults.standard.value(forKey: "storeId") as? String {
            newPram["store_id"] = storeID
        }
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);
        
        API().callAPI(endPoint: "mobiconnect/customer/update", method: .POST, param: newPram) { (json_res, err) in
            
            DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                if err == nil {
                    
                    let json = json_res[0]
                    
                    if (json["data"]["customer"][0]["status"].stringValue == "error") {
                        
                        if APP_DEL.selectedLanguage == Arabic{
                             cedMageHttpException.showAlertView(me: self, msg: "كلمة مرور قديمة غير صالحة.", title: APP_LBL().error)
                        } else {
                            cedMageHttpException.showAlertView(me: self, msg: json["data"]["customer"][0]["message"].stringValue, title: APP_LBL().error)
                        }
                    }
                    
                    if (json["data"]["customer"][0]["status"].stringValue == "success") {
                        
                        self.view.makeToast(json["data"]["customer"][0]["message"].stringValue, duration: 1.0, position: .center)
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            //self.navigationController?.popViewController(animated: true)
                            //NotificationCenter.default.post(name: NSNotification.Name(rawValue: "numberAdded"), object: nil);
                            if(self.defaults.bool(forKey: "isLogin") == true)
                        {
                            self.logout()
                        }
                    }
                        
                    }
                    if(json["data"]["customer"][0]["status"].stringValue == "false"){
                        cedMageHttpException.showAlertView(me: self, msg: json["data"]["customer"][0]["message"].stringValue, title: APP_LBL().error)
                    }
                }
            }
        }
    }
    
    func logout() {
        
        var newPram = [String: String]()
        
        if let userInfoDict = UserDefaults.standard.object(forKey: "userInfoDict") as? [String: String] {
            
            newPram["hashkey"] = userInfoDict["hashKey"]
            newPram["customer_id"] = userInfoDict["customerId"]
        }
        
        if let storeID =  UserDefaults.standard.value(forKey: "storeId") as? String {
            newPram["store_id"] = storeID
        }
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);
        
        API().callAPI(endPoint: "mobiconnect/customer/logout", method: .POST, param: newPram) { (json_res, err) in
            
            DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                if err == nil {
                    
                    self.defaults.set(false, forKey: "isLogin");
                    self.defaults.removeObject(forKey: "userInfoDict");
                    self.defaults.removeObject(forKey: "selectedAddressId");
                    self.defaults.setValue("Magenative App For Magento 2", forKey: "name")
                    UserDefaults.standard.set(nil,forKey: "LIAccessToken")
                    UserDefaults.standard.synchronize();
                    self.defaults.synchronize()
                    self.defaults.removeObject(forKey: "selectedAddressDescription");
                    
                    self.defaults.removeObject(forKey: "cartId");
                    self.defaults.removeObject(forKey: "cart_summary");
                    self.defaults.removeObject(forKey: "guestEmail");
                    self.defaults.set("0", forKey: "items_count");
                    self.defaults.removeObject(forKey: "name")
                    self.setCartCount(view: self, items: "0")
                    self.navigationController?.navigationBar.isHidden = false
                    
                    APP_DEL.changeLanguage()
                }
            }
        }
    }
}

extension cedMageProfileChangePassword: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        switch textField {
        case oldPassword:
            if oldPassword.text == "" {
                oldPasswordImage.image = UIImage(named: "wrongname")
                oldPasswordImage.isHidden = false
                oldPasswordView.layer.borderColor = UIColor.init(hexString: "#AF1F45")?.cgColor
            } else {
                oldPasswordImage.isHidden = true
                oldPasswordView.layer.borderColor = nejreeColor?.cgColor
            }
        default:
            if newPassword.text == "" {
                newPasswordImage.image = UIImage(named: "wrongname")
                newPasswordImage.isHidden = false
                newPasswordView.layer.borderColor = UIColor.init(hexString: "#AF1F45")?.cgColor
            } else {
                newPasswordImage.isHidden = true
                newPasswordView.layer.borderColor = nejreeColor?.cgColor
            }
        }
        
        
        if self.oldPassword.text != "" && self.newPassword.text != "" {
            self.changeBtnView.setBorder()
            self.changePasswordBtn.setTitleColor(.white, for: .normal)
        } else {
            
            self.changeBtnView.layer.borderColor = UIColor.darkGray.cgColor
            self.changePasswordBtn.setTitleColor(.darkGray, for: .normal)
        }
        
    }
    
    
}
