//
//  cedMageProfileChangeNumber.swift
//  MageNative Magento Platinum
//
//  Created by cedcoss on 21/08/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit
import FirebaseAuth
class cedMageProfileChangeNumber: cedMageViewController {

    
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var lblCountryCode: UILabel!

    @IBOutlet weak var newPhoneNumber: SkyFloatingLabelTextField!
    @IBOutlet weak var txtStep_1: UITextField!
    @IBOutlet weak var txtStep_2: UITextField!

    var jsonData = [String:String]()
    @IBOutlet weak var numberImage: UIImageView!
    @IBOutlet weak var btnBack: UIButton!
   
//    @IBOutlet weak var changePhoneNumberLabel: UILabel!
//    @IBOutlet weak var requiredFiledLabel: UILabel!
    @IBOutlet weak var changeNumberBtn: UIButton!
    @IBOutlet weak var phoneNumberView: UIView!
    @IBOutlet weak var changeBtnView: UIView!
    
    var isFromLogin = false
    var countryCode = "+\(APP_DEL.selectedCountry.phone_code ?? "966")"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        countryCode = "+" + (APP_DEL.selectedCountry.phone_code ?? "966")
        lblCountryCode.text = "+" + (APP_DEL.selectedCountry.phone_code ?? "966")
        if APP_DEL.selectedLanguage == Arabic{
            btnBack.setImage(UIImage(named: "BackArrowNewArabic"), for: .normal)
        } else {
            btnBack.setImage(UIImage(named: "BackArrowNew"), for: .normal)
        }
        
        
       
        headingLabel.text = APP_LBL().change_phone_number.uppercased()
        headingLabel.textAlignment = .left
        
        self.txtStep_1.setBorder()
        self.txtStep_1.setCornerRadius()
        self.txtStep_2.setBorder()
        self.txtStep_2.setCornerRadius()
        
        
        newPhoneNumber.placeholder = APP_LBL().phone_number_star.uppercased()
        newPhoneNumber.delegate = self
        newPhoneNumber.textAlignment = .left
        newPhoneNumber.keyboardType = .asciiCapableNumberPad
        
        phoneNumberView.setCornerRadius()
        phoneNumberView.setBorder()
        phoneNumberView.layer.borderColor = UIColor.white.cgColor
        phoneNumberView.semanticContentAttribute = .forceLeftToRight
        
        
        changeNumberBtn.setTitle(APP_LBL().continue_c.uppercased(), for: .normal)
        changeNumberBtn.setTitleColor(UIColor.white, for: .normal)
        
        changeBtnView.setBorder()
        changeBtnView.setCornerRadius()//roundCorners()
        changeBtnView.layer.borderColor = UIColor.white.cgColor
        
        
        
        numberImage.image = UIImage(named: "")
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func btnBackAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func changeNumber(_ sender: Any) {
           
            let mobileNumber = newPhoneNumber.text
            if mobileNumber == "" {
                
                self.view.makeToast(APP_LBL().please_enter_phone_no, duration: 2.0, position: .center);
                return
            }
            
            if mobileNumber!.count != (Int(APP_DEL.selectedCountry.phone_number_limit ?? "") ?? 9) {
                
                self.view.makeToast(APP_LBL().mobile_number_should_be_of_xyz_digits_only, duration: 2.0, position: .center);
                return
            }
            
        let number = "+" + (APP_DEL.selectedCountry.phone_code ?? "966") + (mobileNumber!).getArToEnDigit()
            
            if (!APP_DEL.needToShowOtpScreenOrNot()){
                self.saveMobile(mobile: number)
                return
            }

            var lang_value = ""
           
            if APP_DEL.selectedLanguage == Arabic{
                lang_value = "2"
            } else {
                lang_value = "1"
            }
            
            var postData = ["telephone": number, "store_id" : lang_value]

            if let userInfoDict = UserDefaults.standard.object(forKey: "userInfoDict") as? [String: String] {
                
                postData["hashkey"] = userInfoDict["hashKey"]
                postData["customer_id"] = userInfoDict["customerId"]
            }
            postData["country_id"] =  APP_DEL.selectedCountry.country_code ?? "SA"
          
            cedMageLoaders.removeLoadingIndicator(me: self);
            cedMageLoaders.addDefaultLoader(me: self);
            
            
            API().callAPI(endPoint: "mobiconnect/customer/otpdata", method: .POST, param: postData) { (json_res, err) in
                
                DispatchQueue.main.async {
                    
                    cedMageLoaders.removeLoadingIndicator(me: self);
                    
                    if err == nil {
                        
                        if json_res[0]["code"] == 0 {
                            
                            self.view.makeToast(APP_LBL().please_enter_valid_phone_no, duration: 1.0, position: .center)
                            
                        } else {
                            
                            self.jsonData["mobile_numbers"] = number
                            self.jsonData["m_number"] = mobileNumber
                            
                            if let viewControl = UIStoryboard(name: "cedMageAccounts", bundle: nil).instantiateViewController(withIdentifier: "cedMageProfileVerifyOtp") as? cedMageProfileVerifyOtp {
                                
                                viewControl.postData = self.jsonData
                                viewControl.isFromLogin = self.isFromLogin
                                cedMageLoaders.removeLoadingIndicator(me: self)
                                self.navigationController?.pushViewController(viewControl, animated: true)
                            }
                        }
                    }
                }
            }
        }
        
        //MARK:- SAVE MOBILE
        func saveMobile(mobile: String) {
            
            var param = [String:String]()

            if (self.defaults.object(forKey: "userInfoDict") != nil) {
                
                let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
                param["hashkey"] = userInfoDict["hashKey"]!
                param["customer_id"] = userInfoDict["customerId"]!;
            }
            
            if let storeId = defaults.value(forKey: "storeId") as? String {
                param["store_id"] = storeId
            }
            
            param["mobile_numbers"] = mobile
            param["phonecode"] = "+" + (APP_DEL.selectedCountry.phone_code ?? "966")
            
            
            cedMageLoaders.removeLoadingIndicator(me: self);
            cedMageLoaders.addDefaultLoader(me: self);
            
            API().callAPI(endPoint: "mobiconnect/saveMobile", method: .POST, param: param) { (json_res, err) in
                
                DispatchQueue.main.async {
                    
                    cedMageLoaders.removeLoadingIndicator(me: self);
                    
                    if err == nil {
                        
                        if json_res[0]["data"]["customer"][0]["status"].stringValue == "success" {
                            
                            self.defaults.setValue(self.newPhoneNumber.text, forKey: "mobile_number")
                            
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "numberAdded"), object: nil);

                            if self.isFromLogin {
                                
                                self.defaults.set(true, forKey: "isLogin")
                                
                                NotificationCenter.default.post(name: NSNotification.Name("RefreshAccount"), object: nil)
                                
                                self.navigationController?.popToRootViewController(animated: true)
                                
                            } else {
                                
                                self.navigationController?.popViewController(animated: true)
                            }
                        }
                    }
                }
            }
        }
    
    
    
}

extension cedMageProfileChangeNumber: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if (textField == newPhoneNumber) {
            
            guard !string.isEmpty else {

                return true
            }
            
            if (CharacterSet(charactersIn: "0123456789٠١٢٣٤٥٦٧٨٩").isSuperset(of: CharacterSet(charactersIn: string))) {

                if let text = textField.text, let range = Range(range, in: text) {

                    let proposedText = text.replacingCharacters(in: range, with: string)


                    if (APP_DEL.selectedCountry.country_code?.uppercased() == "SA"){
                        if (proposedText.first == "5" || proposedText.first == "٥") && proposedText.count <= (Int(APP_DEL.selectedCountry.phone_number_limit ?? "") ?? 9) {
                            return true
                        }
                    } else {
                        if proposedText.count <= (Int(APP_DEL.selectedCountry.phone_number_limit ?? "") ?? 9) {
                            return true
                        }
                    }
                }
            }

            return false
        }
        
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == newPhoneNumber {
            textField.textColor = UIColor.white
        }
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField == newPhoneNumber {
            
            if (textField.text ?? "").count != (Int(APP_DEL.selectedCountry.phone_number_limit ?? "") ?? 9) {
                
                textField.textColor = nejreeColor
                phoneNumberView.layer.borderColor = UIColor.white.cgColor
                numberImage.image = UIImage(named: "IconIncorrectCheck")
                
                changeNumberBtn.setTitleColor(UIColor.white, for: .normal)
                changeBtnView.backgroundColor = UIColor.black
                changeBtnView.layer.borderColor = UIColor.white.cgColor
                
            } else {
                
                textField.textColor = UIColor.white
                phoneNumberView.layer.borderColor = nejreeColor?.cgColor
                numberImage.image = UIImage(named: "iconCorrectCheckMark")
                
                changeNumberBtn.setTitleColor(UIColor.white, for: .normal)
                changeBtnView.backgroundColor = nejreeColor
                changeBtnView.layer.borderColor = nejreeColor?.cgColor
                
                 txtStep_1.layer.borderColor = nejreeColor?.cgColor
                
            }
        }
        
        
//        guard let str = textField.text else {return}
        
//        if str.count > 9 || str.count < 9 {
//            numberImage.image = UIImage(named: "wrongname")
//            numberImage.isHidden = false
//            changeBtnView.layer.borderColor = UIColor.darkGray.cgColor
//            phoneNumberView.layer.borderColor = UIColor.init(hexString: "#AF1F45")?.cgColor
//            changeNumberBtn.setTitleColor(.darkGray, for: .normal)
//        } else {
//            numberImage.image = UIImage(named: "verified")
//            numberImage.isHidden = false
//            changeBtnView.setBorder()
//            phoneNumberView.layer.borderColor = nejreeColor?.cgColor
//            changeNumberBtn.setTitleColor(.white, for: .normal)
//        }
    }
    
    
    
}
