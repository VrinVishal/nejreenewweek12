//
//  nejreePrivacyController.swift
//  MageNative Magento Platinum
//
//  Created by MacMini on 29/08/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit
import WebKit

class nejreePrivacyController: MagenativeUIViewController {

    @IBOutlet weak var privacyTable: UITableView!
    var data = [TnCData]()
    var str = String()
    var myWebView = WKWebView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        privacyTable.delegate = self
        privacyTable.dataSource = self
//        privacyTable.contentInset = UIEdgeInsetsMake(0, 0, 200, 0);
        myWebView.navigationDelegate = self
        // Do any additional setup after loading the view.
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
}

extension nejreePrivacyController: UITableViewDelegate,UITableViewDataSource,WKNavigationDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "privacyCell", for: indexPath) as! privacyCell
        
        myWebView.frame = CGRect(x: 0, y: 0, width: cell.frame.width, height: privacyTable.frame.size.height)
        
        cell.addSubview(myWebView)
        print(str)
        myWebView.loadHTMLString(str, baseURL: nil)
        
//        cell.topLabel.text = data[indexPath.row].title
//        cell.contentLabel.text = data[indexPath.row].content
        
        cell.topLabel.numberOfLines = 0
        cell.contentLabel.numberOfLines = 0
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return privacyTable.frame.size.height
    }
    
    
    
}
