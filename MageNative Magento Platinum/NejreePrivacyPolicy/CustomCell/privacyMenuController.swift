//
//  privacyMenuController.swift
//  MageNative Magento Platinum
//
//  Created by MacMini on 29/08/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit
import WebKit
class privacyMenuController: MagenativeUIViewController,UIGestureRecognizerDelegate {

    @IBOutlet weak var btnBack: UIButton!
    
//    var pageMenu: CAPSPageMenu?
//    var controllerArray = [UIViewController]()
//    var data = [TnCData]()
//    var data2 = [TnCData]()
//    var data3 = [TnCData]()
    
//    var str1 = String()
//    var str2 = String()
//    var str3 = String()
    
    @IBOutlet weak var privacyPolicyLabel: UILabel!
    
    @IBOutlet weak var imgPrivacyPolicy: UIImageView!
    @IBOutlet var viewBg : UIView!
    @IBOutlet weak var webview: WKWebView!

    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.navigationBar.isHidden = false
        privacyPolicyLabel.isHidden = true
        // Do any additional setup after loading the view.
        
        if APP_DEL.selectedLanguage == Arabic {
            
            self.btnBack.setImage(UIImage(named: "BackArrowNewArabic"), for: .normal)
            
        } else {
          
            self.btnBack.setImage(UIImage(named: "BackArrowNew"), for: .normal)
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)

        self.getCMS()
        
        if navigationController?.responds(to: #selector(getter: UINavigationController.interactivePopGestureRecognizer)) ?? false {
            
            self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
            self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        }
    }
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
           
           return true
       }
    
    override func viewWillDisappear(_ animated: Bool) {
              self.navigationController?.setNavigationBarHidden(true, animated: true)
          }
    
    func getCMS() {
        
        var postData = [String:String]()
        postData["language_id"] = UserDefaults.standard.value(forKey: "storeId") as? String
        
        if APP_DEL.selectedLanguage == Arabic{
            postData["cms_id"] = "privacy-policy-app-arabic"
        } else {
            postData["cms_id"] = "privacy-policy-app-english"
        }
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);
        
        API().callAPI(endPoint: "mobiconnect/cms/getpage/", method: .POST, param: postData) { (json, err) in
            
            //DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                if err == nil {
                    
                    let content = json[0]["data"]["result"][0]["content"].stringValue
                    let image = json[0]["data"]["result"][0]["image"].stringValue
                    
                    let fontName =  "Cairo-Regular"
                   let fontSize = 45
                   let fontSetting = "<span style=\"font-family: \(fontName);font-size: \(fontSize)\"</span>"
                   
                    self.webview.loadHTMLString( fontSetting + content, baseURL: nil)
                    
                    
                   // self.webview.loadHTMLString(content, baseURL: nil)
                    self.imgPrivacyPolicy.sd_setImage(with: URL(string: image), placeholderImage: nil)
                }
          //  }
        }
    }

}


//MARK:- CAPSPageMenuDelegate
extension privacyMenuController : CAPSPageMenuDelegate {
    func willMoveToPage(_ controller: UIViewController, index: Int) {
        print(index)
    }
    func didMoveToPage(_ controller: UIViewController, index: Int) {
        print(index)
    }
}
