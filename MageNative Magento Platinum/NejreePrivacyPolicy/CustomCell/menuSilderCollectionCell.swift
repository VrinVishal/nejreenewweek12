//
//  menuSilderCollectionCell.swift
//  MageNative Magento Platinum
//
//  Created by MacMini on 29/08/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class menuSilderCollectionCell: UICollectionViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var downView: UIView!
    
    
    override var isSelected: Bool {
        didSet{
            downView.backgroundColor = isSelected ? #colorLiteral(red: 0.9882352941, green: 0.6980392157, blue: 0.08235294118, alpha: 1) : .white
        }
    }
    
    
    
}
