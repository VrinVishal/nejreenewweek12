//
//  main.swift
//  MageNative Magento Platinum
//
//  Created by vishal.n on 30/07/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import Foundation
import UIKit

CommandLine.unsafeArgv.withMemoryRebound(to: UnsafeMutablePointer<Int8>.self, capacity: Int(CommandLine.argc)) {argv in
    
    _ = UIApplicationMain(CommandLine.argc, argv, NSStringFromClass(Application.self), NSStringFromClass(AppDelegate.self))
}
