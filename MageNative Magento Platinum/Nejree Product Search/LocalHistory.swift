//
//  LocalHistory.swift
//  MageNative Magento Platinum
//
//  Created by vishal.n on 20/11/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import Foundation


class LocalHistory: NSObject {
    
    private let KEY_HISTORY = "LocalHistory"
    
    func setHistory(text: String) -> [String] {
        
        if let temp = UserDefaults.standard.value(forKey: KEY_HISTORY) as? [String] {
            
            var arrHistory = temp
            
            if arrHistory.count >= 6 {
                arrHistory.removeLast()
            }
            
            if arrHistory.contains(text) {
                
                if let index = arrHistory.index(of: text) {
                    arrHistory.remove(at: index)
                }
            }
            
            arrHistory.insert(text, at: 0)
            
            UserDefaults.standard.set(arrHistory, forKey: KEY_HISTORY)
            
            return arrHistory
            
        } else {
            
            UserDefaults.standard.set([text], forKey: KEY_HISTORY)
            
            return [text]
        }
    }
    
    func getHistory() -> [String] {
        
        return UserDefaults.standard.stringArray(forKey: KEY_HISTORY) ?? []
    }
    
    func remove(text: String) {
        
        if let temp = UserDefaults.standard.value(forKey: KEY_HISTORY) as? [String] {
            
            var arrHistory = temp
            
            if arrHistory.contains(text) {
                
                if let index = arrHistory.index(of: text) {
                    arrHistory.remove(at: index)
                }
            }
            
            UserDefaults.standard.set(arrHistory, forKey: KEY_HISTORY)
        }
    }
    
    func removeAll() {
        
        if let _ = UserDefaults.standard.value(forKey: KEY_HISTORY) as? [String] {
            
            UserDefaults.standard.set([String](), forKey: KEY_HISTORY)
        }
    }
}
