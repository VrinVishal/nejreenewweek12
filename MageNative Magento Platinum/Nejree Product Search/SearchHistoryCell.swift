//
//  SearchHistoryCell.swift
//  MageNative Magento Platinum
//
//  Created by vishal.n on 20/11/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class SearchHistoryCell: UITableViewCell {

    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDevider: UILabel!
    @IBOutlet weak var imgArrow: UIImageView!
    @IBOutlet weak var imgSearch: UIImageView!
    @IBOutlet weak var btnRemove: UIButton!
    @IBOutlet weak var btnArrow: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
       lblTitle.font =  UIFont(name: "Cairo-Regular", size: 16)!
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
