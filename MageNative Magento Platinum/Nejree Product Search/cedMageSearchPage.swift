//
//  cedMageSearchPage.swift
//  MageNative Magento Platinum
//
//  Created by vishal.n on 01/07/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit
import IQKeyboardManager
import SwiftyJSON

struct RecommendedSearchTags : Codable {
    var title: String?
}

class cedMageSearchPage: UIViewController,UIGestureRecognizerDelegate {
    
    @IBOutlet weak var viewBG: UIView!
    
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var viewClear: UIView!
    
    @IBOutlet weak var viewSearch: UIView!
    @IBOutlet weak var tblSearch: UITableView!
    @IBOutlet weak var htSearchTbl: NSLayoutConstraint!
    
    @IBOutlet weak var viewHistory: UIView!
    @IBOutlet weak var lblHistory: UILabel!
    @IBOutlet weak var lblClearAllHistory: UILabel!
    @IBOutlet weak var tblHistory: UITableView!
    @IBOutlet weak var htHistoryTbl: NSLayoutConstraint!
    
    @IBOutlet weak var viewTrending: UIView!
    @IBOutlet weak var lblTrending: UILabel!
    @IBOutlet weak var tblTrending: UITableView!
    @IBOutlet weak var htTrendingTbl: NSLayoutConstraint!
    
    @IBOutlet weak var viewNoData: UIView!
    @IBOutlet weak var lblNoData: UILabel!
    
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var btnExplore: UIButton!
    
    @IBOutlet weak var bottomHT: NSLayoutConstraint!
    
    var arrSearch = [[String:String]]()
    var arrHistory = [String]()
    var arrRecommendedSearchTags = [RecommendedSearchTags]()

    
    var isFirstTime = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUI()
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardNotification(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardNotification(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    var isKeyboardShowing = false
    @objc func handleKeyboardNotification(notification: NSNotification) {
        
        if let endFrame = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            
            self.isKeyboardShowing = (notification.name == UIResponder.keyboardWillShowNotification)
            
            UIView.animate(withDuration: 0.0, delay: 0, options: .curveLinear, animations: {
                
                var keyboardHeight = UIScreen.main.bounds.height - endFrame.origin.y
                if #available(iOS 11, *) {
                    if keyboardHeight > 0 {
                        keyboardHeight = keyboardHeight - self.view.safeAreaInsets.bottom
                    }
                    self.bottomHT.constant = (keyboardHeight + 0)//8)
                }
                self.view.layoutIfNeeded()
                
            }, completion: { (completed) in })
        }
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    override func viewWillAppear(_ animated: Bool) {
                
        arrHistory = LocalHistory().getHistory()
        arrSearch = [[String:String]]()
        self.checkNoData()
        
        self.getSearchRecommended()
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        IQKeyboardManager.shared().isEnabled = false
        IQKeyboardManager.shared().isEnableAutoToolbar = false
        
        self.tblSearch.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
        self.tblHistory.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
        self.tblTrending.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
        
        self.txtSearch.becomeFirstResponder()
       
        if navigationController?.responds(to: #selector(getter: UINavigationController.interactivePopGestureRecognizer)) ?? false {
            
            self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
            self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        }
        
        self.txtSearch.text = ""
        
    }
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        
        return true
    }
   
    override func viewWillDisappear(_ animated: Bool) {
        
        IQKeyboardManager.shared().isEnabled = true
        IQKeyboardManager.shared().isEnableAutoToolbar = true
    }
    
    func dismissVC() {
        
        let transition:CATransition = CATransition()
        transition.duration = 0.0
        transition.timingFunction = CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType.fade
        transition.subtype = CATransitionSubtype.fromTop
        self.navigationController?.view.layer.add(transition, forKey: kCATransition)
        self.navigationController?.popViewController(animated: false)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?)
    {
        if let tbl = object as? UITableView {
            
            if tbl == self.tblHistory {
             
                self.tblHistory.layer.removeAllAnimations()
                self.htHistoryTbl.constant = self.tblHistory.contentSize.height
                
            } else if tbl == self.tblTrending {
                
                self.tblTrending.layer.removeAllAnimations()
                self.htTrendingTbl.constant = self.tblTrending.contentSize.height
                
            } else if tbl == self.tblSearch {
                
                self.tblSearch.layer.removeAllAnimations()
                self.htSearchTbl.constant = self.tblSearch.contentSize.height
            }
        }
        
        UIView.animate(withDuration: 0.5) {
            self.updateViewConstraints()
            self.loadViewIfNeeded()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnExploreAction(_ sender: UIButton){
        
        self.tabBarController?.selectedIndex = 0;
    }
    
    @IBAction func btnSearchAction(_ sender: Any) {
        
        self.view.endEditing(true)
        
        if txtSearch.text == ""{
            
            return
        }
        
        if let viewController =  UIStoryboard(name: "categorylayouts", bundle: nil).instantiateViewController(withIdentifier: "cedMageDefaultCollection") as?  cedMageDefaultCollection {
            
            viewController.hidesBottomBarWhenPushed = true
            viewController.searchString = filterText
            viewController.strTitleLayer = filterText
            viewController.selectedCategory = APP_DEL.isFromHomeBannerID
            
            
            
            if (self.txtSearch.text != "") {
                
                let detail : [AnalyticKey:String] = [
                    .SearchTerm : filterText
                ]
                API().setEvent(eventName: .Search, eventDetail: detail) { (json, err) in }

                arrHistory = LocalHistory().setHistory(text: txtSearch.text ?? "")
                
                self.navigationController?.pushViewController(viewController, animated: true)
            }
        }
        
        
        
    }
    
    @IBAction func btnClearAction(_ sender: UIButton) {
        
        if (self.txtSearch.text == "") {
         
            dismissVC()
            
        } else {
            
            self.txtSearch.text = ""
            self.filter(text: "")
        }
    }
    
    func filter(text: String) {
        
        filterText = text
        
        if text == "" {
            
            viewClear.isHidden = false//true
            
            arrSearch.removeAll()
            
            self.txtSearch.text = ""
            checkNoData()
            
        } else {
            
            viewClear.isHidden = false
            
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(getHint), object: nil)
            self.perform(#selector(getHint), with: nil, afterDelay: 1)
        }
        
    }
    
    var filterText = ""
    @objc func getHint() {
        
        if(filterText.count >= 3) {
            
            filterText = filterText.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            getAutocomplete()
        }
    }
    
    func checkNoData() {
        
        if ((self.arrSearch.count == 0) && (self.txtSearch.text == "")) {
            
            self.viewSearch.isHidden = true
            
            if (arrRecommendedSearchTags.count > 0 && arrHistory.count > 0) {
                
                self.viewHistory.isHidden = false
                self.viewTrending.isHidden = false
                self.viewNoData.isHidden = true
                
            } else if (arrRecommendedSearchTags.count == 0 && arrHistory.count == 0) {
                
                self.viewHistory.isHidden = true
                self.viewTrending.isHidden = true
                self.viewNoData.isHidden = isFirstTime ? true : false
                
            } else {
                
                self.viewHistory.isHidden = !(arrHistory.count > 0)
                self.viewTrending.isHidden = !(arrRecommendedSearchTags.count > 0)
                self.viewNoData.isHidden = true
            }
            
            
            
            self.btnSearch.superview?.isHidden = true
            self.btnExplore.superview?.isHidden = false
            
        } else {
            
            self.viewHistory.isHidden = true
            self.viewTrending.isHidden = true
            
            if (arrSearch.count == 0) {
                
                self.viewSearch.isHidden = true
                self.viewNoData.isHidden = isFirstTime ? true : false
                
            } else {
                self.viewSearch.isHidden = false
                self.viewNoData.isHidden = true
            }
            
            self.btnSearch.superview?.isHidden = false
            self.btnExplore.superview?.isHidden = true
        }
        
        self.view.layoutIfNeeded()
        
        self.tblSearch.reloadData()
        self.tblHistory.reloadData()
        self.tblTrending.reloadData()
    }
    
    @objc func btnRemoveHistoryAction(_ sender: UIButton) {
        
        if arrHistory.count > sender.tag {
            
            LocalHistory().remove(text: arrHistory[sender.tag])
            arrHistory.remove(at: sender.tag)
            
            self.checkNoData()
        }
        
    }
    
    @objc func btnArrowProductAction(_ sender: UIButton) {
        
        if arrSearch.count > sender.tag {
            let text = arrSearch[sender.tag]["product_name"] ?? ""
            self.txtSearch.text = text
            filter(text: text)
        }
    }
    
    @objc func btnArrowHistoryAction(_ sender: UIButton) {
        
        if arrHistory.count > sender.tag {
            let text = arrHistory[sender.tag]
            self.txtSearch.text = text
            filter(text: text)
        }
    }
    
    @objc func btnArrowTrendingAction(_ sender: UIButton) {
        
        if arrRecommendedSearchTags.count > sender.tag {
            let text = arrRecommendedSearchTags[sender.tag].title ?? ""
            self.txtSearch.text = text
            filter(text: text)
        }
    }
    
    @IBAction func btnRemoveAllHistoryAction(_ sender: UIButton) {
        
        let actionsheet = UIAlertController(title: nil, message: APP_LBL().are_you_sure_to_remove_all_suggestion, preferredStyle: IS_IPAD ? .alert : .alert)
        
        actionsheet.addAction(UIAlertAction(title: APP_LBL().yes.uppercased(), style: UIAlertAction.Style.default,handler: {
            action -> Void in
            
            LocalHistory().removeAll()
            self.arrHistory = []
            
            self.checkNoData()
        }))
        
        actionsheet.addAction(UIAlertAction(title: APP_LBL().cancel, style: UIAlertAction.Style.cancel, handler: {
            action -> Void in
        }))
        self.present(actionsheet, animated: true, completion: nil)
        
    }
    
    func getSearchRecommended(){
        let storeID = UserDefaults.standard.value(forKey: "storeId") as! String?
        var postData = [String:String]()
        postData["store_id"] = storeID
        postData["country_id"] = APP_DEL.selectedCountry.country_code ?? "SA"
        if let genderID =  UserDefaults.standard.object(forKey: KEY_GENDER) as? String {
            postData["gender_id"] = genderID
        }

        
        API().callAPI(endPoint: "mobiconnect/checkout/searchterm", method: .POST, param: postData) { (json_res, err) in

            cedMageLoaders.removeLoadingIndicator(me: self);


            let catData = try! json_res[0]["keywords"].rawData()
            self.arrRecommendedSearchTags = try! JSONDecoder().decode([RecommendedSearchTags].self, from: catData)
            self.checkNoData()

        }
    }
    
    func getAutocomplete() {
        
        guard let storeId = UserDefaults.standard.value(forKey: "storeId") as? String else {return}
        
        var postData = [String:String]()
        postData["page"] = "1"
        postData["store_id"] = storeId
        postData["q"] = filterText
        postData["autosearch"] = "yes"
        
        let defaults = UserDefaults.standard
        if defaults.bool(forKey: "isLogin") {
            
            if let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] {
                
                postData["hashkey"] = userInfoDict["hashKey"]
                postData["customer_id"] = userInfoDict["customerId"]
            }
        }
        postData["country_id"] = APP_DEL.selectedCountry.country_code ?? "SA"
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader_withlockedbackground(me: self);
        
        API().callAPI(endPoint: "mobiconnect/catalog/productsearch", method: .POST, param: postData) { (json, err) in
            
            DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                self.arrSearch.removeAll()
                
                if err == nil {
                    
                    if (json[0].stringValue != "NO_PRODUCTS") {
                        
                        for (_,value) in json[0]["data"]["suggestion"] {
                            var data = [String:String]()
                            data["product_id"] = value["product_id"].stringValue
                            data["product_name"] = value["product_name"].stringValue
                            data["product_image"] = value["product_image"].stringValue
                            self.arrSearch.append(data)
                        }
                    }
                }
                
                self.checkNoData()
            }
        }
    }
    
    
    
    func setUI() {
        
        viewBG.round(redius: 8.0)
        viewBG.layer.borderWidth = 0.3
        viewBG.layer.borderColor = UIColor.lightGray.cgColor
        
        viewClear.isHidden = false
        
        let value=UserDefaults.standard.value(forKey: "AppleLanguages") as! [String]
        if value[0] == "ar" {
            self.txtSearch.textAlignment = .right
        } else {
            self.txtSearch.textAlignment = .left
        }
        
        self.tblSearch.rowHeight = UITableView.automaticDimension
        self.tblSearch.estimatedRowHeight = UITableView.automaticDimension
        self.tblSearch.register(UINib(nibName: "SearchProductCell", bundle: nil), forCellReuseIdentifier: "SearchProductCell")
        self.tblSearch.dataSource = self
        self.tblSearch.delegate = self
        self.tblSearch.reloadData()
        self.tblSearch.round(redius: 7.0)
        self.tblSearch.layer.borderWidth = 0.3
        self.tblSearch.layer.borderColor = UIColor.lightGray.cgColor
            
        lblClearAllHistory.text = APP_LBL().clear_all.uppercased()
        lblClearAllHistory.font =  UIFont(name: "Cairo-Regular", size: 14)!
        
        
        self.tblHistory.rowHeight = UITableView.automaticDimension
        self.tblHistory.estimatedRowHeight = UITableView.automaticDimension
        self.tblHistory.register(UINib(nibName: "SearchHistoryCell", bundle: nil), forCellReuseIdentifier: "SearchHistoryCell")
        self.tblHistory.dataSource = self
        self.tblHistory.delegate = self
        self.tblHistory.reloadData()
        self.tblHistory.round(redius: 7.0)
        self.tblHistory.layer.borderWidth = 0.3
        self.tblHistory.layer.borderColor = UIColor.lightGray.cgColor
        
        self.tblTrending.rowHeight = UITableView.automaticDimension
        self.tblTrending.estimatedRowHeight = UITableView.automaticDimension
        self.tblTrending.register(UINib(nibName: "SearchHistoryCell", bundle: nil), forCellReuseIdentifier: "SearchHistoryCell")
        self.tblTrending.dataSource = self
        self.tblTrending.delegate = self
        self.tblTrending.reloadData()
        self.tblTrending.round(redius: 7.0)
        self.tblTrending.layer.borderWidth = 0.3
        self.tblTrending.layer.borderColor = UIColor.lightGray.cgColor
        
        if (APP_DEL.selectedLanguage == Arabic) {
            lblHistory.textAlignment = .right
            lblTrending.textAlignment = .right
        } else {
            lblHistory.textAlignment = .left
            lblTrending.textAlignment = .left
        }

        lblHistory.font =  UIFont(name: "Cairo-Regular", size: 15)!
        lblTrending.font =  UIFont(name: "Cairo-Regular", size: 15)!
        
        lblHistory.text = APP_LBL().search_history
        lblTrending.text = APP_LBL().trending_now
        
        
        self.btnExplore.superview?.isHidden = true
        self.btnSearch.superview?.isHidden = true
        
        self.lblNoData.text = APP_LBL().no_product_found.uppercased()
        
        self.txtSearch.delegate = self
        self.txtSearch.placeholder = APP_LBL().search_for_product_brands_more
        self.txtSearch.autocorrectionType = .no
        self.txtSearch.shouldResignOnTouchOutsideMode = .enabled
        
        btnSearch.round(redius: 0)
        btnSearch.setTitle(APP_LBL().search.uppercased(), for: .normal)
        
        btnExplore.round(redius: 0)
        btnExplore.setTitle(APP_LBL().explore_our_products.uppercased(), for: .normal)
        
        checkNoData()
    }
}


extension cedMageSearchPage: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        var fullString = ""
        if let str = textField.text, let swtRange = Range(range, in: str) {
            fullString = str.replacingCharacters(in: swtRange, with: string)
        }
        
        filter(text: fullString)
        
        return true;
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        self.view.endEditing(true)
        
        self.btnSearchAction(btnSearch)
        
        return true
    }
}

extension cedMageSearchPage: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == self.tblSearch {
            return arrSearch.count
        } else if tableView == self.tblHistory {
            return arrHistory.count
        } else if tableView == self.tblTrending {
            return arrRecommendedSearchTags.count
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 0.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == self.tblSearch {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "SearchProductCell") as! SearchProductCell
            cell.selectionStyle = .none
            
            cell.btnArrow.tag = indexPath.row
            cell.btnArrow.addTarget(self, action: #selector(self.btnArrowProductAction(_:)), for: .touchUpInside)
            
            cell.imgView.sd_setImage(with: URL(string: arrSearch[indexPath.row]["product_image"] ?? ""), placeholderImage: nil)
            
            cell.lblTitle.text = arrSearch[indexPath.row]["product_name"]
            
            cell.lblDevider.isHidden = (indexPath.row == (arrSearch.count - 1))
            
            let value=UserDefaults.standard.value(forKey: "AppleLanguages") as! [String]
            if value[0] == "ar" {
                cell.lblTitle.textAlignment = .right
                cell.imgArrow.image = UIImage(named: "arrows_ar")
            } else {
                cell.lblTitle.textAlignment = .left
                cell.imgArrow.image = UIImage(named: "arrows")
            }
            
            return cell;
            
        } else if tableView == self.tblHistory {

            let cell = tableView.dequeueReusableCell(withIdentifier: "SearchHistoryCell") as! SearchHistoryCell
            cell.selectionStyle = .none
            
            cell.btnRemove.tag = indexPath.row
            cell.btnRemove.addTarget(self, action: #selector(self.btnRemoveHistoryAction(_:)), for: .touchUpInside)
            
            cell.btnArrow.tag = indexPath.row
            cell.btnArrow.addTarget(self, action: #selector(self.btnArrowHistoryAction(_:)), for: .touchUpInside)
            
            cell.lblTitle.text = arrHistory[indexPath.row]
            
            cell.lblDevider.isHidden = (indexPath.row == (arrHistory.count - 1))
            
            let value=UserDefaults.standard.value(forKey: "AppleLanguages") as! [String]
            if value[0] == "ar" {
                cell.lblTitle.textAlignment = .right
                cell.imgArrow.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
            } else {
                cell.lblTitle.textAlignment = .left
            }
            
           
            
            cell.btnRemove.superview?.isHidden = false
            cell.imgSearch.superview?.isHidden = false
            cell.imgArrow.superview?.isHidden = false
            
            return cell;
            
        } else if tableView == self.tblTrending {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "SearchHistoryCell") as! SearchHistoryCell
            cell.selectionStyle = .none
            
            cell.btnArrow.tag = indexPath.row
            cell.btnArrow.addTarget(self, action: #selector(self.btnArrowTrendingAction(_:)), for: .touchUpInside)
            
            cell.lblTitle.text = self.arrRecommendedSearchTags[indexPath.row].title ?? ""
            
            cell.lblDevider.isHidden = (indexPath.row == (arrRecommendedSearchTags.count - 1))
            
            let value=UserDefaults.standard.value(forKey: "AppleLanguages") as! [String]
            if value[0] == "ar" {
                cell.lblTitle.textAlignment = .right
                cell.imgArrow.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
            } else {
                cell.lblTitle.textAlignment = .left
            }
            
            cell.btnRemove.superview?.isHidden = true
            cell.imgSearch.superview?.isHidden = true
            cell.imgArrow.superview?.isHidden = false
            
            return cell;
        }
        
        return UITableViewCell();
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
//        if tableView == self.tblHistory {
//            return true
//        }
        
        return false
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
//        if tableView == self.tblHistory {
//
//            if editingStyle == .delete {
//
//                LocalHistory().remove(text: arrHistory[indexPath.row])
//                arrHistory.remove(at: indexPath.row)
//
//                self.checkNoData()
//            }
//
//        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == self.tblSearch {
            
            let vc = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "productViewController") as! productViewController
            APP_DEL.productIDglobal = arrSearch[indexPath.row]["product_id"]!
            vc.product_id = arrSearch[indexPath.row]["product_id"]!
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
            
            arrHistory = LocalHistory().setHistory(text: txtSearch.text ?? "")
            
        } else if tableView == self.tblHistory {
            
            if let viewController =  UIStoryboard(name: "categorylayouts", bundle: nil).instantiateViewController(withIdentifier: "cedMageDefaultCollection") as?  cedMageDefaultCollection {
                
                viewController.hidesBottomBarWhenPushed = true
                viewController.searchString = arrHistory[indexPath.row]
                viewController.strTitleLayer = arrHistory[indexPath.row]
                viewController.selectedCategory = APP_DEL.isFromHomeBannerID
                
                let detail : [AnalyticKey:String] = [
                    .SearchTerm : arrHistory[indexPath.row]
                ]
                API().setEvent(eventName: .Search, eventDetail: detail) { (json, err) in }
                self.navigationController?.pushViewController(viewController, animated: true)
                
            }
            
        } else if tableView == self.tblTrending {
            
            if let viewController =  UIStoryboard(name: "categorylayouts", bundle: nil).instantiateViewController(withIdentifier: "cedMageDefaultCollection") as?  cedMageDefaultCollection {
                
                viewController.hidesBottomBarWhenPushed = true
                viewController.searchString = arrRecommendedSearchTags[indexPath.row].title ?? ""
                viewController.strTitleLayer = arrRecommendedSearchTags[indexPath.row].title ?? ""
                viewController.selectedCategory = APP_DEL.isFromHomeBannerID
                
                let detail : [AnalyticKey:String] = [
                    .SearchTerm : arrRecommendedSearchTags[indexPath.row].title ?? ""
                ]
                API().setEvent(eventName: .Search, eventDetail: detail) { (json, err) in }
                self.navigationController?.pushViewController(viewController, animated: true)
                
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    
}
