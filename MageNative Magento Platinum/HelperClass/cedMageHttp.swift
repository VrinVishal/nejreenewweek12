//
//  cedMageHttp.swift
//  MageNative Magento Platinum
//
//  Created by CEDCOSS Technologies Private Limited on 12/08/16.
//  Copyright © 2016 CEDCOSS Technologies Private Limited. All rights reserved.
//

import Foundation
import UIKit

class CustomView: UIView {
    var alignmentRectInsetsOverride: UIEdgeInsets?
    override var alignmentRectInsets: UIEdgeInsets {
        return alignmentRectInsetsOverride ?? super.alignmentRectInsets
    }
}

class cedMageViewController: UIViewController {
    var externalWindow : UIWindow?
    let defaults = UserDefaults.standard
    let color = cedMage.getInfoPlist(fileName: "cedMage", indexString: "themeColor")
    var themecolor = UIColor()
    var nejreeColor = UIColor.init(hexString: "#FBAD18")
    
    override func viewDidLoad() {
        setupNav()
        themecolor = cedMage.UIColorFromRGB(colorCode: color as! String)
    }
    
    override func viewWillAppear(_ animated: Bool) {

        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        self.navigationItem.backBarButtonItem?.tintColor = .white
        
        let barButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.search, target: self, action: #selector(cedMageViewController.gotoSearch(sender:)))
        barButton.tintColor = .white

        let buttonWidth = 55;
        let buttonHeight = 25;
        let topSpace = 10;
        
        let button: UIButton = UIButton();
        button.frame = CGRect(x: 0, y: 0, width: CGFloat(buttonWidth), height: CGFloat(buttonHeight + topSpace))
//        button.imageInsets = UIEdgeInsets(top: 0, left: -13.0, bottom: 0, right: 13.0) self.navigationItem.rightBarButtonItem = rightBtn
        button.translatesAutoresizingMaskIntoConstraints = false
        
        let label = UILabel();
        label.tag = 123;
        label.font = label.font.withSize(10)
        label.textAlignment = .center;
        label.textColor = .white//fontColorTool()
        label.backgroundColor = UIColor.init(hexString: "#FBAD18")
        APP_DEL.getCartCount()
        let lblHT = 14
        label.frame.origin.x = CGFloat((buttonWidth / 2) - (lblHT / 2))
        label.frame.origin.y = 6
        label.frame.size = CGSize(width: lblHT, height: lblHT)
        label.layer.cornerRadius = CGFloat((lblHT / 2))
        label.layer.masksToBounds = true
        
        let imageView = UIImageView(image: UIImage(named: "cart2"))
        let reduceImgHt = 5
        imageView.frame = CGRect(x: 0, y: topSpace + reduceImgHt, width: buttonWidth, height: buttonHeight - reduceImgHt)
        imageView.contentMode = .scaleAspectFit
      
        button.addSubview(imageView)
        button.layer.cornerRadius = 5
        button.backgroundColor = .clear
        button.addTarget(self, action:#selector(cedMageViewController.gotoCart(sender:)) , for: UIControl.Event.touchUpInside)
        button.addSubview(label);
        
       
        
        let cartButton = UIBarButtonItem(customView: button)
        
        if let themeColor = cedMage.getInfoPlist(fileName: "cedMage", indexString: "fontColor") as? String{
             cartButton.tintColor =  cedMage.UIColorFromRGB(colorCode:themeColor)
        }
        
        if !(self is nejreeCartController || self is nejreeAddressBookController){
            
            if APP_DEL.selectedLanguage == Arabic{
                button.contentMode = .left
            } else {
                button.contentMode = .right
            }
        }
        
        let vW : CGFloat = 82
        let vH : CGFloat = 44
        
             let titleV = UIView(frame: CGRect(x: 0, y: 0, width: vW, height: vH))
             titleV.backgroundColor = .black
            
            let imgW : CGFloat = 63
               let imgH : CGFloat = 34
               let titleImage = UIImageView(frame: CGRect(x: ((vW - imgW)/2.0), y: ((vH - imgH)/2.0), width: imgW, height: imgH))
               titleImage.image = UIImage(named: "header-Title")
               titleImage.contentMode = .scaleAspectFit
               titleImage.backgroundColor = .clear
               let tappableButton = UIButton(frame: titleImage.frame)
               tappableButton.addTarget(self, action: #selector(self.logoNejreeTapped), for: .touchUpInside)
               tappableButton.setTitle("", for: .normal)
               titleV.addSubview(titleImage)
               titleV.addSubview(tappableButton)
//               self.navigationItem.titleView = titleV //Note A: remove logo from navigation bar

       // homedefaultNavigation().addToggleButton(me: self)

    }
    
    @objc func logoNejreeTapped() {
        
        print("logoNejreeTapped Called")
        if let topVC : UIViewController = UIApplication.topViewController() {
            topVC.tabBarController?.selectedIndex = 2
        }
        
        
        if let topVC : UIViewController = UIApplication.topViewController() {
            topVC.navigationController?.popToRootViewController(animated: true)
        }
    }
    
    func intializeExternalScreen(external:UIScreen){
        self.externalWindow = UIWindow(frame: external.bounds)
        self.externalWindow?.screen = external
        self.externalWindow?.isHidden = false
        let view  = UIView(frame: (self.externalWindow?.frame)!)
        let imageView = UIImageView(image: UIImage(named: "splash"))
        imageView.frame = view.frame
        imageView.contentMode = .scaleAspectFit
        view.addSubview(imageView)
        view.backgroundColor = UIColor.white
        self.externalWindow?.addSubview(view)
        self.externalWindow?.isHidden = false
    }
    
    func getRequest(url:String,store:Bool)
    {
        cedMageLoaders.addDefaultLoader(me: self)
        let storeId = defaults.value(forKey: "storeId")
        let resendUrl = url
        var postString = String()
        let httpUrl = cedMage.getInfoPlist(fileName:"cedMage",indexString: "cedBaseUrl") as! String
        if store {
            if(storeId != nil){
                postString += (storeId! as! String)
            }
        }
        
        let reqUrl = httpUrl+url+postString
        var makeRequest = URLRequest(url: URL(string: reqUrl)!)
        //let requestHeader = cedMage.getInfoPlist(fileName:"cedMage",indexString: "requestheader") as! String
        
        
        makeRequest.httpMethod = "GET"
        makeRequest.setValue(API().getAuthorization(), forHTTPHeaderField: "Authorization")
        
        print(reqUrl)
        print(postString)
        //makeRequest.httpBody = postString.data(using: String.Encoding.utf8)
        //makeRequest.setValue(requestHeader, forHTTPHeaderField: "Mobiconnectheader")
        let task = URLSession.shared.dataTask(with: makeRequest, completionHandler: {data,response,error in
            
            
            // check for http errors
            if let httpStatus = response as? HTTPURLResponse , httpStatus.statusCode != 200
            {
                
                DispatchQueue.main.async
                    {
                        cedMageLoaders.removeLoadingIndicator(me: self)
                      
                        self.renderNoDataImage(view: self, imageName: "no_module")
                        print("statusCode should be 200, but is \(httpStatus.statusCode)")
                        
                        print("response = \(response)")
                }
                return;
            }
            
            // code to fetch values from response :: start
            
            guard error == nil && data != nil else
            {
                DispatchQueue.main.async
                    {
                        cedMageLoaders.removeLoadingIndicator(me: self)
                        cedMageHttpException.showAlertView(me: self, msg: error?.localizedDescription, title: APP_LBL().error )
                        self.getRequest(url: resendUrl, store: store)
                        print("error=\(error)")
                }
                return ;
            }
            
            DispatchQueue.main.async
                {
                    cedMageLoaders.removeLoadingIndicator(me: self)
                    
                    self.recieveResponse(data: data, requestUrl: url, response: response)
                  
            }
        })
        
        task.resume()
    }
    func send_req_pagination(url:String,store:Bool)
    {
        let resendUrl = url
        cedMageLoaders.addDefaultLoader(me: self)
        let storeId = defaults.value(forKey: "storeId")
        var postString = String()
        let httpUrl = cedMage.getInfoPlist(fileName:"cedMage",indexString: "cedBaseUrl") as! String
        if store {
            if(storeId != nil){
                postString += (storeId! as! String)
            }
        }
        
        let reqUrl = httpUrl+url+postString
        var makeRequest = URLRequest(url: URL(string: reqUrl)!)
        //let requestHeader = cedMage.getInfoPlist(fileName:"cedMage",indexString: "requestheader") as! String
        
        makeRequest.httpMethod = "GET"
        makeRequest.setValue(API().getAuthorization(), forHTTPHeaderField: "Authorization")
        print(reqUrl)
        print(postString)
        //makeRequest.httpBody = postString.data(using: String.Encoding.utf8)
        //makeRequest.setValue(requestHeader, forHTTPHeaderField: "Mobiconnectheader")
        let task = URLSession.shared.dataTask(with: makeRequest, completionHandler: {data,response,error in
            
            
            // check for http errors
            if let httpStatus = response as? HTTPURLResponse , httpStatus.statusCode != 200
            {
                
                DispatchQueue.main.async
                    {
                        
                        cedMageLoaders.removeLoadingIndicator(me: self)
                  
                        self.renderNoDataImage(view: self, imageName: "no_module")
                        print("statusCode should be 200, but is \(httpStatus.statusCode)")
                        
                        print("response = \(response)")
                }
                return;
            }
            
            // code to fetch values from response :: start
            
            
            guard error == nil && data != nil else
            {
                DispatchQueue.main.async
                    {
                      
                      cedMageLoaders.removeLoadingIndicator(me: self)
                        cedMageHttpException.showAlertView(me: self, msg: error?.localizedDescription, title: APP_LBL().error )
                        self.send_req_pagination(url: resendUrl, store: store)
                        print("error=\(error)")
                }
                return ;
            }
            
            DispatchQueue.main.async
                {
                   
                   cedMageLoaders.removeLoadingIndicator(me: self)
                    
                    self.recieveResponse(data: data, requestUrl: url, response: response)
                    let screen = UIScreen.screens
                    if(screen.count > 1){
                        let mainScreen = UIScreen.main
                        for scrn in screen {
                            if(scrn != mainScreen){
                                //scrn = mainScreen
                                self.intializeExternalScreen(external: scrn)
                            }else{
                                //  self.window?.makeKeyAndVisible()
                            }
                        }
                    }
            }
        })
        
        task.resume()
    }
    
    
    //MArk :MageNative Wheel code controller basis
    func sendWheelRequest(url:String,params:String){
        
        cedMageLoaders.addDefaultLoader(me: self)
        let baseUrl = cedMage.getInfoPlist(fileName:"cedMage",indexString: "cedAppBaseUrl") as! String
        let reqUrl = baseUrl+url
        
        var postString=params
        
        var makeRequest = URLRequest(url: URL(string: reqUrl)!)
        
        let requestHeader = cedMage.getInfoPlist(fileName:"cedMage",indexString: "requestheader") as! String
        let storeId = defaults.value(forKey: "storeId") as? String
        
        if(params != ""){
            makeRequest.httpMethod = "POST"
            if storeId != nil {
                postString += "&store_id="+storeId!
            }
            
        }
        
        print(reqUrl)
        print(postString)
        makeRequest.httpBody = postString.data(using: String.Encoding.utf8)
        makeRequest.setValue(requestHeader, forHTTPHeaderField: "mobiconnectheader")
        let task = URLSession.shared.dataTask(with: makeRequest, completionHandler: {data,response,error in
            
            
            // check for http errors
            if let httpStatus = response as? HTTPURLResponse , httpStatus.statusCode != 200
            {
                
                DispatchQueue.main.async
                    {
                        cedMageLoaders.removeLoadingIndicator(me: self)
                     
                        self.renderNoDataImage(view: self, imageName: "no_module")
                        print("poststring=\(postString)")
                        print("statusCode should be 200, but is \(httpStatus.statusCode)")
                        
                        print("response = \(response)")
                }
                return;
            }
            
            // code to fetch values from response :: start
            
            
            guard error == nil && data != nil else
            {
                DispatchQueue.main.async
                    {
                        cedMageLoaders.removeLoadingIndicator(me: self)
                        cedMageHttpException.showAlertView(me: self, msg: error?.localizedDescription, title: APP_LBL().error)
                        
                        print("error=\(error)")
                }
                return ;
            }
            
            DispatchQueue.main.async
                {
                    cedMageLoaders.removeLoadingIndicator(me: self)
                    
                    self.recieveResponse(data: data, requestUrl: url, response: response)
                    
            }
        })
        
        task.resume()
    }
    
    //Mark:
    func sendRequest(url:String,params:Dictionary<String,String>?,store:Bool = true,isLoaderDisplay:Bool = true)
    {
        
        let resendUrl = url
        cedMageLoaders.removeLoadingIndicator(me: self)
        
        if isLoaderDisplay {
            cedMageLoaders.addDefaultLoader(me: self)
        }        
        
        let httpUrl = cedMage.getInfoPlist(fileName:"cedMage",indexString: "cedBaseUrl") as! String
        let reqUrl = httpUrl+url
        
        var postString=Dictionary<String,Dictionary<String,String>>()
        var postString1=""
        print(reqUrl)
        var makeRequest = URLRequest(url: URL(string: reqUrl)!)
        makeRequest.setValue(API().getAuthorization(), forHTTPHeaderField: "Authorization")
        _ = cedMage.getInfoPlist(fileName:"cedMage",indexString: "requestheader") as! String
        let storeId = defaults.value(forKey: "storeId") as? String
        
        if(params != nil){
            makeRequest.httpMethod = "POST"
            postString=["parameters":[:]]
            for (key,value) in params!
            {
                _ = postString["parameters"]?.updateValue(value, forKey:key)
            }
            if store {
                if storeId != nil {
                    _ = postString["parameters"]?.updateValue(storeId!, forKey:"store_id")
                }
            }
            postString1=postString.convtToJson() as String
            makeRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
            makeRequest.setValue(API().getAuthorization(), forHTTPHeaderField: "Authorization")
          
        }
        
        print(reqUrl)
        print(postString)
        makeRequest.httpBody = postString1.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let task = URLSession.shared.dataTask(with: makeRequest, completionHandler: {data,response,error in
            
            // check for http errors
            if let httpStatus = response as? HTTPURLResponse , httpStatus.statusCode != 200
            {
                
                DispatchQueue.main.async
                    {
                        cedMageLoaders.removeLoadingIndicator(me: self)
                        
                       
                        
                        print("poststring=\(postString1)")
                        guard let data = data else {return}
                        print(NSString(data: data, encoding: String.Encoding.utf8.rawValue) ?? "")
                        print("statusCode should be 200, but is \(httpStatus.statusCode)")
                        
                        print("response = \(response)")
                        self.recieveResponse(data: data, requestUrl: url, response: response)
                }
                return;
            }
            
            // code to fetch values from response :: start
            
            
            guard error == nil && data != nil else
            {
                DispatchQueue.main.async
                    {
                        
                        cedMageLoaders.removeLoadingIndicator(me: self)
                        cedMageHttpException.showAlertView(me: self, msg: error?.localizedDescription, title: APP_LBL().error )
                        self.sendRequest(url: resendUrl, params: params)
                        print("error=\(error)")
                        self.recieveResponse(data: data, requestUrl: url, response: response)
                }
                return ;
            }
            
            DispatchQueue.main.async
                {
                    cedMageLoaders.removeLoadingIndicator(me: self)
                    
                    self.recieveResponse(data: data, requestUrl: url, response: response)
                    
            }
        })
        
        task.resume()
        }
    
    func failedWithError(data:Data?,requestUrl:String?,response:URLResponse?){
        
    }
    
        
    //Mark:
    func sendCheckoutRequest(url:String,params:Dictionary<String,String>?){
        
        cedMageLoaders.addDefaultLoader(me: self)
        let httpUrl = "http://demo.cedcommerce.com/magento2/mage-native/"
        let reqUrl = httpUrl+url
        
        var postString=Dictionary<String,Dictionary<String,String>>()
        var postString1:String=""
        print(reqUrl)
        var makeRequest = URLRequest(url: URL(string: reqUrl)!)
        
        _ = cedMage.getInfoPlist(fileName:"cedMage",indexString: "requestheader") as! String
        let storeId = defaults.value(forKey: "storeId")
        
        if(params != nil){
            makeRequest.httpMethod = "POST"
            postString=["parameters":[:]]
            for (key,value) in params!
            {
                _ = postString["parameters"]?.updateValue(value, forKey:key)
            }
            if storeId != nil {
                _ = postString["parameters"]?.updateValue(storeId as! String, forKey:"store_id")
            }
            postString1=postString.convtToJson() as String
        }
        
        print(reqUrl)
        print(postString)
        //makeRequest.httpBody = postString1.data(using: String.Encoding.utf8.rawValue)
        //makeRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let task = URLSession.shared.dataTask(with: makeRequest, completionHandler: {data,response,error in
            // check for http errors
            if let httpStatus = response as? HTTPURLResponse , httpStatus.statusCode != 200
            {
                
                DispatchQueue.main.async
                    {
                        cedMageLoaders.removeLoadingIndicator(me: self)
                      
                        self.renderNoDataImage(view: self, imageName: "no_module")
                        
                        print("poststring=\(postString1)")
                        print("statusCode should be 200, but is \(httpStatus.statusCode)")
                        
                        print("response = \(response)")
                }
                return;
            }
            
            // code to fetch values from response :: start
            
            
            guard error == nil && data != nil else
            {
                DispatchQueue.main.async
                    {
                        cedMageLoaders.removeLoadingIndicator(me: self)
                        cedMageHttpException.showAlertView(me: self, msg: error?.localizedDescription, title: APP_LBL().error )
                        print("error=\(error)")
                }
                return ;
            }
            
            DispatchQueue.main.async
                {
                    cedMageLoaders.removeLoadingIndicator(me: self)
                    
                    self.recieveResponse(data: data, requestUrl: url, response: response)
                    
            }
        })
        
        task.resume()
    }
    
    
    
    func recieveResponse(data:Data?,requestUrl:String?,response:URLResponse?){
        
    }
    
    
    //SetUPNavigationBarColor
    
    func setupNav(){
        if let themeColor = cedMage.getInfoPlist(fileName: "cedMage", indexString: "themeColor") as? String{
      //  self.navigationController?.navigationBar.barTintColor = cedMage.UIColorFromRGB(colorCode: themeColor)
             self.navigationController?.navigationBar.barTintColor = UIColor.black
            self.navigationController?.navigationBar.tintColor = .white//cedMage.UIColorFromRGB(colorCode:themeColor)
            
        }
        
    }
    
    //Mark:Goto Cart Page
    
    @objc func gotoCart(sender:UIButton){
//        if let cartPage = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "cartViewNav") as? UINavigationController{//
//
//           let cart = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "nejreeCartController") as! nejreeCartController
//            cartPage.setViewControllers([cart], animated: true)
//        if !(self.navigationController?.viewControllers.last is nejreeCartController) {
//            cart.mainVc = self;
//            cartPage.modalPresentationStyle = .fullScreen
//                  self.present(cartPage, animated: true, completion: nil)
//            }
//
//        }
//        if !(self.navigationController?.viewControllers.last is CartViewController) {
//            self.navigationController?.pushViewController(cartPage, animated: true)
//        }
    }
    
    func updateCartCount(){
        print("updateCartCount");
        let cartCountSection = self.navigationItem.rightBarButtonItem;
        let cartCountLabel = cartCountSection?.customView?.viewWithTag(123) as? UILabel;
        print("cartCountLabel???");
        
        
        cartCountLabel?.text = "$";
        print(cartCountLabel?.text ?? "cartCountLabel");
    }
    
    //Mark:goto Search page
    
    @objc func gotoSearch(sender:UIBarButtonItem){
        let searchPage = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "searchView") as! cedMageSearchPage
        if !(self.navigationController?.viewControllers.last is cedMageSearchPage) {
            self.navigationController?.pushViewController(searchPage, animated: true)
        }
       // self.navigationController?.pushViewController(searchPage, animated: true)
    }
    
    
    
    func setCartCount(view:UIViewController,items:String){
        let vv = view.navigationItem.rightBarButtonItem;
        let label = vv?.customView?.viewWithTag(123) as? UILabel;
//        label?.backgroundColor = UIColor.init(hexString: "#FBAD18")
        if(items != ""){
            label?.text = items;
        }
        
    }
    func renderNoDataImage(view:UIViewController,imageName:String){
        let noDataImageView = nodataImageView();
        noDataImageView.translatesAutoresizingMaskIntoConstraints = false;
        if imageName == "noProduct"{
            let value=UserDefaults.standard.value(forKey: "AppleLanguages") as! [String]
            noDataImageView.topLabel.text = (value[0] == "ar") ? "عذرا ، لا يوجد تطابق لبحثك" : "Sorry, there is no match for your search"
            //noDataImageView.topLabel.text = "Sorry,There is no match for your search."
            noDataImageView.continueShopping.setTitle(APP_LBL().search.uppercased(), for: .normal)
            noDataImageView.continueShopping.addTarget(self, action: #selector(cedMageViewController.continueShopping), for: .touchUpInside)
        }else if imageName == "noWishlist" {
            noDataImageView.topLabel.text = APP_LBL().you_have_no_items_in_your_wishlist.uppercased()
            noDataImageView.continueShopping.setTitle(APP_LBL().continue_shopping, for: .normal)
            noDataImageView.continueShopping.addTarget(self, action: #selector(cedMageViewController.continueShopping), for: .touchUpInside)
        }else if imageName == "no_order" {
            noDataImageView.topLabel.text = APP_LBL().you_have_no_orders.uppercased()
            noDataImageView.continueShopping.isHidden = true
            noDataImageView.continueShopping.setTitle(APP_LBL().cancel, for: .normal)
            noDataImageView.continueShopping.addTarget(self, action: #selector(cedMageViewController.cancelClicked(_:)), for: .touchUpInside)
        }else if imageName == "noAddress" {
            noDataImageView.topLabel.text = APP_LBL().looks_like_you_dont_have_any_saved_addresses.uppercased()
            noDataImageView.continueShopping.setTitle(APP_LBL().continue_shopping, for: .normal)
            noDataImageView.continueShopping.addTarget(self, action: #selector(cedMageViewController.continueShopping), for: .touchUpInside)
        }else if imageName == "nodownloads" {
            noDataImageView.topLabel.text = APP_LBL().you_have_not_purchased_any_downloadable_products_yet.uppercased()
            noDataImageView.continueShopping.setTitle(APP_LBL().continue_shopping, for: .normal)
            noDataImageView.continueShopping.addTarget(self, action: #selector(cedMageViewController.continueShopping), for: .touchUpInside)
        }else if imageName == "noDeals"{
            noDataImageView.topLabel.text = APP_LBL().no_exciting_offers_to_show.uppercased()
            noDataImageView.continueShopping.isHidden = true
            noDataImageView.continueShopping.setTitle(APP_LBL().continue_shopping, for: .normal)
            noDataImageView.continueShopping.addTarget(self, action: #selector(cedMageViewController.continueShopping), for: .touchUpInside)
        }else if imageName == "noReviews"{
            noDataImageView.topLabel.text = APP_LBL().no_reviews_yet_be_the_first_one_to_review.uppercased()
            noDataImageView.continueShopping.isHidden = true
            noDataImageView.continueShopping.setTitle(APP_LBL().continue_shopping, for: .normal)
            noDataImageView.continueShopping.addTarget(self, action: #selector(cedMageViewController.continueShopping), for: .touchUpInside)
        }else if imageName == "no_module" {
            noDataImageView.topLabel.text = APP_LBL().cant_connect_please_check_your_network_connection.uppercased()
            noDataImageView.continueShopping.isHidden = true
            noDataImageView.continueShopping.setTitle(APP_LBL().continue_shopping, for: .normal)
            noDataImageView.continueShopping.addTarget(self, action: #selector(cedMageViewController.continueShopping), for: .touchUpInside)
        }
        
        
        noDataImageView.fontColorTool()
        
        noDataImageView.continueShopping.backgroundColor = .black//UIColor.init(hexString: "#FBAD18")
        noDataImageView.continueShopping.roundCorners()
        noDataImageView.continueShopping.setBorder()
//        noDataImageView.continueShopping.addTarget(self, action: #selector(cedMageViewController.continueShopping), for: .touchUpInside)
        noDataImageView.contentMode = UIView.ContentMode.scaleAspectFit;
        view.view.addSubview(noDataImageView);
        view.view.addConstraint(NSLayoutConstraint(item: noDataImageView, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view.view, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 0));
        view.view.addConstraint(NSLayoutConstraint(item: noDataImageView, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view.view, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 0));
        view.view.addConstraint(NSLayoutConstraint(item: noDataImageView, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view.view, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1, constant: 0));
        view.view.addConstraint(NSLayoutConstraint(item: noDataImageView, attribute: NSLayoutConstraint.Attribute.bottom, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view.view, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: 0));
        
    }
    
    @objc func continueShopping(_ sender:UIButton){
        if let viewController = UIStoryboard(name: "homeLayouts", bundle: nil).instantiateViewController(withIdentifier: "nejreeHomeController") as? nejreeHomeController {
            self.navigationController?.setViewControllers([viewController], animated: true)
        }
    }
    
    
    @objc func cancelClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //Mark: POP ViewController
    
    
}

