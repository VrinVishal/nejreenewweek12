/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */
import UIKit

class homedefaultNavigation: UINavigationController,UINavigationControllerDelegate {
    
    var currentViewController: UIViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.navigationBar.layer.shadowColor = UIColor.black.cgColor
        self.navigationBar.layer.shadowOffset = CGSize(width: 0.0, height: 3.0)
        self.navigationBar.layer.shadowRadius = 4.0
        self.navigationBar.layer.shadowOpacity = 0.3
        self.navigationBar.layer.masksToBounds = false
        self.navigationBar.setBackgroundImage(UIImage(), for: UIBarPosition.any, barMetrics: UIBarMetrics.default)
        self.navigationBar.shadowImage = UIImage()
//self.navigationController?.navigationBar.titleTextAttributes = [ NSAttributedStringKey.font: UIFont(name: "CaviarDreams", size: 20)!]
        
        let barsButton = UIBarButtonItem(image: UIImage(named:"hamp"), style: UIBarButtonItem.Style.plain, target: self, action: nil)
        barsButton.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = barsButton
        if let data = UserDefaults.standard.value(forKey: "NotificationData") as? [String:Any]
        {
            UserDefaults.standard.removeObject(forKey: "NotificationData")
            print(data)
            if let datalink_type = data["link_type"] as? String{
                if let link_id = data["link_id"] as? String{
                    if(datalink_type == "2"){
                        let viewcontoller = UIStoryboard(name: "categorylayouts", bundle: nil).instantiateViewController(withIdentifier: "cedMageDefaultCollection") as? cedMageDefaultCollection
                            viewcontoller?.selectedCategory = link_id
                        self.pushViewController(viewcontoller!, animated: true)
                    }else if (datalink_type == "1"){
                        
                        
//                        let productview = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "rootPageViewController") as! cedMageProductViewRoot
//                        productview.pageData = [["product_id":link_id]]
//                        let instance = cedMage.singletonInstance
//                        instance.storeParameterInteger(parameter: 0)
//                        self.pushViewController(productview
//                            , animated: true)
                        
                    }else if (datalink_type == "3"){
                        
                        
//                        let view = UIStoryboard(name: "cedMageAccounts", bundle: nil)
//                        let viewControl = view.instantiateViewController(withIdentifier: "cmsWebView") as! cedMageCmsWebView
//                        let url = link_id
//                        viewControl.pageUrl = url
//                        self.pushViewController(viewControl, animated: true)
                    }
                }
                
            }
            
        }
    }
    
    
    func addToggleButton(me:UIViewController){
        let toglebut = UIButton()
        toglebut.frame.size = CGSize(width:25, height:25)
        toglebut.setBackgroundImage(UIImage(named: "hamp"), for: UIControl.State.normal)
        toglebut.tintColor = UIColor.white
        
       // toglebut.addTarget(self, action: #selector(cedMageHomeNavigation.toggleDrawer), for: UIControl.Event.touchUpInside)
        let togglebutton = UIBarButtonItem(customView: toglebut)
        let backButton = UIButton(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        backButton.addTarget(self, action: #selector(cedMageHomeNavigation.backfunc(sender:)), for: .touchUpInside)
        let img1=UIImage(named:"IQButtonBarArrowLeft")
        let defaults=UserDefaults.standard
        let value=defaults.value(forKey: "AppleLanguages") as! [String]
        if value[0]=="ar"
        {
            //cell.rightView.image=UIImage(cgImage: (img1?.cgImage)!, scale: (img1?.scale)!, orientation: UIImageOrientation.downMirrored)
            
            backButton.setImage(UIImage(cgImage: (img1?.cgImage)!, scale: (img1?.scale)!, orientation: UIImage.Orientation.upMirrored), for: UIControl.State.normal)
            backButton.backgroundColor=UIColor.black
        }
        else
        {
            backButton.setImage(UIImage(named:"IQButtonBarArrowLeft"), for: .normal)
            backButton.backgroundColor=UIColor.clear
        }
        currentViewController = me
        backButton.tintColor = .white
        let backBarButton = UIBarButtonItem(customView: backButton)
        if let _ = (me as? cedMageSearchPage) {
            //me.navigationItem.leftBarButtonItem = togglebutton
            
        }
        else {
            me.navigationItem.leftBarButtonItems = [backBarButton]
        }
        
        
    }
    
    
    @objc func backfunc(sender:UIButton){
        let mainview = self//self.sideDrawerViewController?.mainViewController  as? homedefaultNavigation
        let result =  mainview.popViewController(animated: true)
        print(result as Any)
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
}
extension UIImage{
    class func imageFromColor(color: UIColor, frame: CGRect) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(frame.size, false, 0)
        color.setFill()
        UIRectFill(frame)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
}

