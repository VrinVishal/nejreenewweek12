/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */


import UIKit

class CustomDesigning: NSObject {
    
    class func makeButtonCircled(_ button:UIButton){
        button.layer.cornerRadius = 0.5 * button.frame.size.width;
     }

}




extension UIView
{
    
    func makeViewCircled(size:CGFloat){
        self.layer.cornerRadius = 0.5 * size;
    }
    
    
    func makeCornerRounded(cornerRadius:CGFloat){
        self.layer.cornerRadius = translateAccordingToDevice(cornerRadius) ;
    }
    
    func makeCard(_ view:UIView,cornerRadius:CGFloat,color:UIColor,shadowOpacity:Float){
        let cornerRadius: CGFloat = cornerRadius
        let shadowColor: UIColor? = color
        let shadowOpacity: Float = shadowOpacity
        view.layer.masksToBounds = false
        view.layer.shadowOffset = CGSize.zero
        view.layer.shadowColor = shadowColor?.cgColor
        view.layer.shadowOpacity = shadowOpacity
        view.layer.shadowRadius = cornerRadius
        view.layer.cornerRadius = cornerRadius
        
    }
    
    
    /*func makeCardUsingThemeColor(view:UIView,cornerRadius:CGFloat,color:UIColor,shadowOpacity:Float){
        
        let colorString = Ced_CommonVendor.getInfoPlist("themecolor") as? String;
        let theme_color = Ced_CommonVendor.UIColorFromRGB(colorString!);
        
        let cornerRadius: CGFloat = cornerRadius
        let shadowColor: UIColor? = theme_color
        let shadowOpacity: Float = shadowOpacity
        view.layer.masksToBounds = false
        view.layer.shadowOffset = CGSizeZero
        view.layer.shadowColor = shadowColor?.CGColor
        view.layer.shadowOpacity = shadowOpacity
        view.layer.shadowRadius = cornerRadius
        view.layer.cornerRadius = cornerRadius
        view.layer.borderColor = shadowColor?.CGColor
        view.layer.borderWidth = CGFloat(2);
        
    }*/
}
