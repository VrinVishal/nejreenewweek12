//
//  Constants.swift
//  MageNative Magento Platinum
//
//  Created by Jayesh Dabhi on 10/02/21.
//  Copyright © 2021 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit
import Adjust




let APP_UDS = UserDefaults.standard


class Constants {

    // MARK :- USER DEFAULTS
    lazy var USERDEFAULT_KEY_SELECTED_COUNTRY : String = "USERDEFAULT_KEY_SELECTED_COUNTRY"

    // MARK :- ADJUST CONFIG
   // let ADJUST_ENVIRONMENT = ADJEnvironmentSandbox;
    let ADJUST_ENVIRONMENT = ADJEnvironmentProduction;
    let ADJUST_APP_TOKEN = "580j0ywj0gzk" // ios token
    
    // MARK :- CELL NAME
    lazy var CELL_NAME_CUSTOMSINGLECOUNTRYCELL : String = "CustomSingleCountryCell"
    
    
    // MARK :- SCREEN NAME
    lazy var SCREEN_NAME_DummySplashVC : String = "DummySplashVC"
    lazy var SCREEN_NAME_NejreeCountryLangSelectVC : String = "NejreeCountryLangSelectVC"

    
    
    lazy var SCREEN_NAME_GenderVC : String = "GenderVC"
    
    
}
