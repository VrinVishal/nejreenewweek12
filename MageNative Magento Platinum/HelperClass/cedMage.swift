/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */
import UIKit

var isCheckForUpdate = false
var isForcefullyUpdate = false

var strTopicID = ""
var strTopicName = ""
var strCodCharge = ""


class cedMage: NSObject {
    static let singletonInstance = cedMage()
    var selectedWidgetIndexe = NSInteger()
    
    /*
     Get data from plist file cedMage Common File
     */
    class func getInfoPlist(fileName:String?,indexString:NSString) ->AnyObject?{
        
        
        let path = Bundle.main.path(forResource: fileName, ofType: "plist")
        let storedvalues = NSDictionary(contentsOfFile: path!)
        let response: AnyObject? = storedvalues?.object(forKey: indexString) as AnyObject?
        return response
    }
    
    /*
     store the slider Index siglet
     */
    func storeParameterInteger(parameter:NSInteger){
        selectedWidgetIndexe = parameter
    }
    
    /*
     Return sigleton instance
     */
    func getInfo() -> NSInteger{
        return selectedWidgetIndexe
    }
    
    /**
     UIcolor From color Code
     */
    class func UIColorFromRGB( colorCode: String, alpha: Float = 1.0) -> UIColor {
        var colorCode = colorCode
        colorCode = colorCode.components(separatedBy: "#").last!
        let scanner = Scanner(string:colorCode)
        var color:UInt32 = 0;
        scanner.scanHexInt32(&color)
        let mask = 0x000000FF
        let r = CGFloat(Float(Int(color >> 16) & mask)/255.0)
        let g = CGFloat(Float(Int(color >> 8) & mask)/255.0)
        let b = CGFloat(Float(Int(color) & mask)/255.0)
        return UIColor(red: r, green: g, blue: b, alpha: CGFloat(alpha))
    }
    
    class func delay(delay:Double, closure: @escaping ()->()) {
        DispatchQueue.main.asyncAfter(deadline: .now() + delay) {
            closure()
        }
        
    }
    //Mark: Get random Color
    class func getRandomColor() -> UIColor{
        let randomRed:CGFloat = CGFloat(drand48())
        let randomGreen:CGFloat = CGFloat(drand48())
        let randomBlue:CGFloat = CGFloat(drand48())
        return UIColor(red: randomRed, green: randomGreen, blue: randomBlue, alpha: 1.0)
        
    }
    
    
    func createFolder(name:String)->String?{
        let paths = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
        let documentsDirectory: String = paths[0]
        let dataPath = documentsDirectory.appending(name)
        print("**\(dataPath)")
        
        do {
            try FileManager.default.createDirectory(atPath: dataPath , withIntermediateDirectories: false, attributes: nil)
        } catch let error as NSError {
            print(error.localizedDescription);
        }
        
        let writePath = dataPath.appending("/test.png")
        do { try UIImage(named: "login")!.pngData()?.write(to: URL(fileURLWithPath:writePath), options: NSData.WritingOptions.completeFileProtectionUnlessOpen)
        }
        catch{
            
        }
        return dataPath
        
    }
    func reSizeImageAccording2View(image: UIImage, targetSize: CGSize) -> UIImage? {
        let size = image.size
        
        let widthRatio  = targetSize.width  / image.size.width
        let heightRatio = targetSize.height / image.size.height
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width:size.width * heightRatio, height:size.height * heightRatio)
        } else {
            newSize = CGSize(width:size.width * widthRatio,  height:size.height * widthRatio)
        }
        
        let rect = CGRect(x:0, y:0, width:newSize.width, height:newSize.height)
        
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
    
    func addGradient(view:UIView){
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = view.frame
        gradient.colors = [UIColor.green.cgColor, UIColor.black.cgColor,UIColor.blue.cgColor]
        gradient.locations = [0.0, 0.25, 0.75, 1.0]
        view.layer.addSublayer(gradient)
    }

}


extension UILabel {
    func setFont(fontFamily:String,fontSize:CGFloat){
        self.font = UIFont(name: fontFamily, size: fontSize)
        
    }
}
extension Dictionary{
    func convtToJson() -> NSString {
        do {
            let json = try JSONSerialization.data(withJSONObject: self, options: JSONSerialization.WritingOptions.prettyPrinted)
            return NSString(data: json, encoding: String.Encoding.utf8.rawValue)!
        }catch {
            return ""
        }
    }
}
extension UIView{
    func setThemeColor(){
        let color = cedMage.getInfoPlist(fileName:"cedMage",indexString: "themeColor") as! String
        if color == "#FFFFFF" {
            if self is UIButton {
                
                self.backgroundColor = .black
                return
            }
            if self is UILabel {
                if let label = self as? UILabel {
                    //                        label.textColor = .black
                    label.fontColorTool()
                }
            }
        }
        //        if color == "#000000" {
        //            if self is UIButton {
        //
        //                self.backgroundColor = .black
        //                if let buttom = self as? UIButton{
        //                    buttom.setTitleColor(UIColor(hexString: "#d99110"), for: .normal)
        //                }
        //                return
        //            }
        //            if self is UILabel {
        //                if let label = self as? UILabel {
        //                    label.textColor = UIColor(hexString: "#d99110")
        //                }
        //            }
        //        }
        self.backgroundColor = cedMage.UIColorFromRGB(colorCode: color)
        
        
        
    }
}
//MARK: Overload Print function
func print(items:Any...){
    
    
}

func print(_ items: Any..., separator: String = " ", terminator: String = "\n") {
    let debug = cedMage.getInfoPlist(fileName:"cedMage",indexString: "APP_DEBUG") as? String
    if(debug == "true"){
        Swift.print(items[0], separator:separator, terminator: terminator)
    }
    let layoutdebug = cedMage.getInfoPlist(fileName:"cedMage",indexString: "LAYOUT_DEBUG") as? String
    if(layoutdebug == "false"){
        UserDefaults.standard.setValue(false, forKey: "_UIConstraintBasedLayoutLogUnsatisfiable")
    }
}


@IBDesignable class GradientView: UIView {
    @IBInspectable var topColor: UIColor = UIColor.white
    @IBInspectable var bottomColor: UIColor = UIColor.white
    
    override class var layerClass: AnyClass {
        return CAGradientLayer.self
    }
    
    override func layoutSubviews() {
        (layer as! CAGradientLayer).startPoint = CGPoint(x: 0.0, y: 0.5)
        (layer as! CAGradientLayer).endPoint = CGPoint(x: 1.0, y: 0.5)
        (layer as! CAGradientLayer).colors = [topColor.cgColor, bottomColor.cgColor]
    }
}


@IBDesignable class GradientViewLabel: UILabel {
    @IBInspectable var topColor: UIColor = UIColor.white
    @IBInspectable var bottomColor: UIColor = UIColor.white
    
    override class var layerClass: AnyClass {
        return CAGradientLayer.self
    }
    
    override func layoutSubviews() {
        (layer as! CAGradientLayer).startPoint = CGPoint(x: 0.0, y: 0.5)
        (layer as! CAGradientLayer).endPoint = CGPoint(x: 1.0, y: 0.5)
        (layer as! CAGradientLayer).colors = [topColor.cgColor, bottomColor.cgColor]
    }
}


