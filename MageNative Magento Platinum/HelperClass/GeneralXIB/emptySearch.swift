//
//  emptySearch.swift
//  MageNative Magento Platinum
//
//  Created by cedcoss on 15/11/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class emptySearch: UIView {

    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var emptyImage: UIImageView!
    @IBOutlet weak var sorryLabel: UILabel!
    @IBOutlet weak var exploreButton: UIButton!
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    private func commonInit() {
        
        Bundle.main.loadNibNamed("emptySearch", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        
    }
    
}
