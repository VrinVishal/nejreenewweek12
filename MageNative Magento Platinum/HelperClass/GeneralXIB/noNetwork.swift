//
//  noNetwork.swift
//  MageNative Magento Platinum
//
//  Created by cedcoss on 30/10/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class noNetwork: UIView {

    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var networkImage: UIImageView!
    @IBOutlet weak var networkLabel: UILabel!
    @IBOutlet weak var retryButton: UIButton!
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("noNetwork", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }
    

}
