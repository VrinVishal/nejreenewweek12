//
//  ShoppingEmpty.swift
//  MageNative Magento Platinum
//
//  Created by Manohar Singh Rawat on 25/08/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class ShoppingEmpty: UIView {

    // Our custom view from the XIB file
    var view: UIView!
    
    @IBOutlet weak var buyView: UIView!
    @IBOutlet weak var buyNowButton: UIButton!
    
    @IBOutlet weak var btnAddFromWish: UIButton!
    
    
    @IBOutlet weak var youDontLabel: UILabel!
    @IBOutlet weak var subtotalLabel: UILabel!
    @IBOutlet weak var checkoutButton: UIButton!
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var subtotalValueLabel: UILabel!
    
    override init(frame: CGRect)
    {
        // 1. setup any properties here
        
        // 2. call super.init(frame:)
        super.init(frame: frame)
        
        // 3. Setup view from .xib file
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        // 1. setup any properties here
        
        // 2. call super.init(coder:)
        super.init(coder: aDecoder)
        
        // 3. Setup view from .xib file
        xibSetup()
    }
    
    func xibSetup()
    {
        view = loadViewFromNib()
        
        // use bounds not frame or it'll be offset
        view.frame = bounds
        
        // Make the view stretch with containing view
        view.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        
        //extra setup
        // self.makeCard(self, cornerRadius: 2, color: UIColor.black, shadowOpacity: 0.4);
        //extra setup
      //  buyView.cardView()
        
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(view);
    }
    
    func loadViewFromNib() -> UIView
    {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "ShoppingEmpty", bundle: bundle)
        
        // Assumes UIView is top level and only object in CustomView.xib file
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
}
