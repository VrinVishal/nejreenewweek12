//
//  removeAddress.swift
//  MageNative Magento Platinum
//
//  Created by cedcoss on 19/10/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class removeAddress: UIView {

    @IBOutlet var contentView: UIView!
    @IBOutlet weak var topImage: UIImageView!
    @IBOutlet weak var noticeLabel: UILabel!
    @IBOutlet weak var statementLabel: UILabel!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("removeAddress", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight,.flexibleWidth]
        
    }
    
    
    
    
    
    
    
}
