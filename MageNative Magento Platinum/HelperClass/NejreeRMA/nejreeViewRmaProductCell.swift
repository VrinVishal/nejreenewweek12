//
//  nejreeViewRmaProductCell.swift
//  MageNative Magento Platinum
//
//  Created by cedcoss on 18/10/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class nejreeViewRmaProductCell: UITableViewCell {

   
    @IBOutlet weak var itemView: UIView!
    @IBOutlet weak var itemImage: UIImageView!
    @IBOutlet weak var itemName: UILabel!
    @IBOutlet weak var quantityLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var reasonLabel: UILabel!
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

   

}
