//
//  nejreeRmaListingController.swift
//  MageNative Magento Platinum
//
//  Created by cedcoss on 16/10/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

struct rmaList {
    var created_at: String?
    var order_id: String?
    var status: String?
    var id: String?
    var return_id: String?
    var updated_at: String?
}



class nejreeRmaListingController: MagenativeUIViewController {

    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var RmaTable: UITableView!
    var rmas = [rmaList]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        headingLabel.text = APP_LBL().my_returns.uppercased()
        RmaTable.isHidden = true
        sendRequestForRmaListing()
        // Do any additional setup after loading the view.
    }
    

    func setupRmaListingController() {
        RmaTable.delegate = self
        RmaTable.dataSource = self
        RmaTable.separatorStyle = .none
        RmaTable.reloadData()
        RmaTable.isHidden = false
    }
    
    func sendRequestForRmaListing () {
        let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
        let customerId = userInfoDict["customerId"]!;
        self.sendRequest(url: "mobiconnect/awRmaRequest/getlist", params: ["customer_id":customerId,"page":"1"], store: true)
    }
    
    override func recieveResponse(data: Data?, requestUrl: String?, response: URLResponse?) {
        guard let data = data else {return}
        do {
            var json = try JSON(data: data)
            json = json[0]
            print(json)
            
            if json["data"]["success"].stringValue == "false" {
                renderNoDataImage(view: self, imageName: "no_order")
                return
            }
            
            for rma in json["data"]["rma_list"].arrayValue {
                let temp = rmaList(created_at: rma["created_at"].stringValue,
                                   order_id: rma["order_id"].stringValue,
                                   status: rma["status"].stringValue,
                                   id: rma["id"].stringValue,
                                   return_id: rma["return_id"].stringValue,
                                   updated_at: rma["updated_at"].stringValue)
                
                self.rmas.append(temp)
            }
            setupRmaListingController()
            
        } catch let error {
            print(error.localizedDescription)
        }
    }
    
    
    
}


extension nejreeRmaListingController: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rmas.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "rmaListCell", for: indexPath) as! rmaListCell
        cell.orderNumber.text = APP_LBL().return_hash_semicolon + " " + rmas[indexPath.row].return_id!
        cell.dateLabel.text = rmas[indexPath.row].created_at
        cell.totalLabel.text = rmas[indexPath.row].id
        cell.statusLabel.text = rmas[indexPath.row].status
        
        cell.insideView.backgroundColor = .white
        cell.insideView.cardView()
        
        cell.selectionStyle = .none
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = UIStoryboard(name: "cedMageAccounts", bundle: nil).instantiateViewController(withIdentifier: "nejreeRmaViewController") as! nejreeRmaViewController
        vc.id = rmas[indexPath.row].id!
        vc.orderId = rmas[indexPath.row].order_id!
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
}
