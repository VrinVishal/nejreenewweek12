//
//  nejreeRmaViewController.swift
//  MageNative Magento Platinum
//
//  Created by cedcoss on 18/10/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

struct viewRmaProduct {
    var product_id: String?
    var price: String?
    var reason: String?
    var product_image: String?
    var product_name: String?
    var item_qty: String?
}


class nejreeRmaViewController: MagenativeUIViewController {

    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var viewRmaTable: UITableView!
    @IBOutlet weak var returnIdLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var returnedItemsHeading: UILabel!
    
    var products = [viewRmaProduct]()
    var date = String()
    var id = String()
    var orderId = String()
    var reason = [[String:String]]()
    var rmaStatus = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        headingLabel.text = APP_LBL().my_returns.uppercased()
        viewRmaTable.isHidden = true
        sendRequestForRmaView()
        
        returnIdLabel.text = APP_LBL().return_.uppercased()+": " + orderId
        returnedItemsHeading.text = APP_LBL().returned_items.uppercased()
        dateLabel.text = date
        
        
        
        
        // Do any additional setup after loading the view.
    }
    func setupRmaViewController() {
        viewRmaTable.delegate = self
        viewRmaTable.dataSource = self
        viewRmaTable.separatorStyle = .none
        viewRmaTable.reloadData()
        viewRmaTable.isHidden = false
    }
    func sendRequestForRmaView() {
        let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
        _ = userInfoDict["hashKey"]!;
        let customerId = userInfoDict["customerId"]!;
        self.sendRequest(url: "mobiconnect/awRmaRequest/view", params: ["id":id, "customer_id":customerId])
    }
    override func recieveResponse(data: Data?, requestUrl: String?, response: URLResponse?) {
        guard let data = data else {return}
        do {
            var json = try JSON(data: data)
            json = json[0]
            print(json)
            
            if json["data"]["success"].stringValue == "true" {
                for p in json["data"]["rma_view"]["order_item"].arrayValue {
                    let temp = viewRmaProduct(product_id: p["product_id"].stringValue,
                                              price:  p["price"].stringValue,
                                              reason:  p["reason"].stringValue,
                                              product_image:  p["product_image"].stringValue,
                                              product_name:  p["product_name"].stringValue,
                                              item_qty:  p["item_qty"].stringValue)
                    self.products.append(temp)
                }
                
                for res in json["data"]["resaon_arr"].arrayValue {
                    let temp = ["key":res["key"].stringValue, "value": res["value"].stringValue]
                    self.reason.append(temp)
                }
                
                self.rmaStatus = json["data"]["rma_view"]["status"].stringValue
                setupRmaViewController()
            }
            
            
        } catch let error {
            print(error.localizedDescription)
        }
    }
    
    func getReasonString(id: String) -> String {
        var selectedReason = String()
        for res in self.reason {
            if res["key"] == id {
                selectedReason = res["value"]!
            }
        }
        return selectedReason
    }
    
}




extension nejreeRmaViewController: UITableViewDelegate,UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return products.count
        default:
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "nejreeViewRmaProductCell", for: indexPath) as! nejreeViewRmaProductCell
            cell.itemView.roundCorners()
            cell.itemView.setBorder()
            cell.itemName.text = products[indexPath.row].product_name
//            cell.itemImage.sd_setImage(with: URL(string: products[indexPath.row].product_image!), placeholderImage: UIImage(named: "placeholder"))
            
            cell.itemImage.sd_setImage(with: URL(string: products[indexPath.row].product_image!), placeholderImage: nil)
            
            cell.priceLabel.text = products[indexPath.row].price
            cell.reasonLabel.text = getReasonString(id: products[indexPath.row].reason!)
            cell.quantityLabel.text = APP_LBL().quantity_semicolon + " " + products[indexPath.row].item_qty!
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "nejreeViewRmaStatusCell", for: indexPath) as! nejreeViewRmaStatusCell
            cell.orderStatusLabel.text = rmaStatus
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return 120
        default:
            return 40
        }
    }
    
}
