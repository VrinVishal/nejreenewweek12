//
//  nejreeViewRmaStatusCell.swift
//  MageNative Magento Platinum
//
//  Created by cedcoss on 18/10/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class nejreeViewRmaStatusCell: UITableViewCell {

 
    @IBOutlet weak var statusHeading: UILabel!
    @IBOutlet weak var orderStatusLabel: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        statusHeading.text = APP_LBL().status.uppercased()
    }

    

}
