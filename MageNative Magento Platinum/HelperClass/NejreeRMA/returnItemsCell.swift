//
//  returnItemsCell.swift
//  MageNative Magento Platinum
//
//  Created by cedcoss on 17/10/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class returnItemsCell: UITableViewCell {

   
    @IBOutlet weak var itemView: UIView!
    @IBOutlet weak var dropdownView: UIView!
    @IBOutlet weak var itemSideview: UIView!
    @IBOutlet weak var itemImage: UIImageView!
    @IBOutlet weak var itemName: UILabel!
    @IBOutlet weak var itemQuantity: UILabel!
    @IBOutlet weak var itemPrice: UILabel!
    @IBOutlet weak var dropdownButton: UIButton!
    @IBOutlet weak var downImage: UIImageView!
    
    
    
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

   

}
