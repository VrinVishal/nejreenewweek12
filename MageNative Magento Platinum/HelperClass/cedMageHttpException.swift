//
//  cedMageHttpException.swift
//  MageNative Magento Platinum
//
//  Created by CEDCOSS Technologies Private Limited on 02/09/16.
//  Copyright © 2016 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class cedMageHttpException: NSObject {
    
    static func showAlertView(me:UIViewController,msg:String?,title:String){
        let AlertBckView = UIAlertController(title: title, message: msg!, preferredStyle: UIAlertController.Style.alert)
        //AlertBckView.view.setThemeColor()
        let OkAction = UIAlertAction(title: APP_LBL().ok.uppercased(), style: UIAlertAction.Style.destructive, handler: nil)
        
        AlertBckView.addAction(OkAction)
        me.present(AlertBckView, animated: true, completion: nil)
    }
    
    static func showAlertViewOnWindow(msg:String?,title:String){
        let AlertBckView = UIAlertController(title: title, message: msg!, preferredStyle: UIAlertController.Style.alert)
        //AlertBckView.view.setThemeColor()
        let OkAction = UIAlertAction(title: APP_LBL().ok.uppercased(), style: UIAlertAction.Style.destructive, handler: nil)
        
        AlertBckView.addAction(OkAction)
        UIApplication.shared.windows.first?.rootViewController!.present(AlertBckView, animated: true, completion: nil)
    }
    
    static func showHttpErrorImage(me:UIViewController,img:String){
        let bounds = UIScreen.main.bounds
        let imagView = UIImageView(frame: bounds)
        imagView.image = UIImage(named: img)
        imagView.contentMode = .scaleAspectFit
        me.view.addSubview(imagView)
    }
    
}
