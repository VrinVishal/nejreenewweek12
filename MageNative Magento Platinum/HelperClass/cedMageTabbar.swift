/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */


import UIKit
import NVActivityIndicatorView

var lastCartIndex = 2

class cedMageTabbar: UITabBarController ,UITabBarControllerDelegate{
    
    @IBOutlet weak var tabbar: UITabBar!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        let colorString = cedMage.getInfoPlist(fileName:"cedMage",indexString: "themeColor") as! String
        //self.tabBar.barTintColor = UIColor.blueColor()
        self.tabBar.barTintColor = UIColor.black;
        //self.tabBar.tintColor = UIColor.black;
        //self.tabBar.backgroundImage =   UIImage.imageFromColor(color: cedMage.UIColorFromRGB(colorCode: colorString), frame: CGRect(x: 0, y: 0, width: 64, height: 360))
        self.tabBar.shadowImage = UIImage()
        self.tabBar.isTranslucent = false
        self.tabBar.layer.shadowOffset = CGSize(width: 0, height: 0);
        self.tabBar.layer.shadowRadius = 2;
        self.tabBar.layer.shadowColor = UIColor.lightGray.cgColor;
        self.tabBar.layer.shadowOpacity = 0.5;
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateBadge(not:)), name: NSNotification.Name("NejreeBadge"), object: nil)
        
       
    }
    
    @objc func updateBadge(not: Notification) {
        
        self.updateCartBadge()
        self.updateWishBadge()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        for index in (0..<self.tabBar.items!.count) {
            
            let item = (self.tabBar.items![index])
            
            if index == 0 {
                item.title = APP_LBL().category.uppercased()
            }
            
            if index == 1 {
                item.title = APP_LBL().my_box.uppercased()
            }
            
            if index == 2 {
                item.title = APP_LBL().home.uppercased()
            }
            
            
            if index == 3 {
                item.title = APP_LBL().my_wishlist_c.uppercased()
            }
            
            if index == 4 {
                item.title = APP_LBL().account.uppercased()
            }
            
            
            
            
            //item.titlePositionAdjustment = UIOffset(horizontal: 0, vertical: -5)
            
            let unselectedItem = [NSAttributedString.Key.foregroundColor: UIColor.gray, NSAttributedString.Key.font : UIFont(name: "Cairo-Regular", size: 10)!]
            let selectedItem = [NSAttributedString.Key.foregroundColor: UIColor(red: 254/255, green: 154/255, blue: 0/255, alpha: 1.0), NSAttributedString.Key.font : UIFont(name: "Cairo-Regular", size: 10)!]
            item.setTitleTextAttributes(unselectedItem, for: .normal)
            item.setTitleTextAttributes(selectedItem, for: .selected)
            
            item.selectedImage = item.selectedImage?.withRenderingMode(.alwaysOriginal)
            item.image = item.image?.withRenderingMode(.alwaysOriginal)

        }
        
        if(APP_DEL.isLoginFromWishList){
             
             APP_DEL.isLoginFromWishList = false
             
             self.selectedIndex = 3
         }
         else if (APP_DEL.isLoginFromCartList){
             
             APP_DEL.isLoginFromCartList = false
             self.selectedIndex = 1
         }
        else if APP_DEL.isLogoutFromAddAddress{
            
             self.selectedIndex = 4
        }
         else
         {
            //self.selectedIndex = 2;
            self.selectedIndex = lastCartIndex;
         }
        
        APP_DEL.isTabBarCreated = true
        
        NotificationCenter.default.post(name: NSNotification.Name("NejreeBadge"), object: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func updateCartBadge() {
        
        if let tabItems = tabBar.items, tabItems.count > 1 {
            
            let defaults = UserDefaults.standard
            if let cartval = defaults.value(forKey: "items_count") as? String {
                
                if cartval == "0" || cartval == "" {
                    
                    let tabItem = tabItems[1]
                    tabItem.badgeValue = nil
                    return
                    
                } else {
                    
                    let tabItem = tabItems[1]
                    tabItem.badgeValue = cartval
                }
                
            } else {
                
                let tabItem = tabItems[1]
                tabItem.badgeValue = nil
            }
        }
    }
    
    func updateWishBadge() {
        
        if let tabItems = tabBar.items, tabItems.count > 3 {
            
            let defaults = UserDefaults.standard
            if let wishval = defaults.value(forKey: "wishlist_item_count") as? String {
                
                if wishval == "0" || wishval == "" {
                    
                    let tabItem = tabItems[3]
                    tabItem.badgeValue = nil
                    return
                    
                } else {
                    
                    let tabItem = tabItems[3]
                    tabItem.badgeValue = wishval
                }
                
            } else {
                
                let tabItem = tabItems[3]
                tabItem.badgeValue = nil
            }
        }
    }
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        if let viewController = viewController as? UINavigationController {
            viewController.popToRootViewController(animated: false)
        }
        
    }
   
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        
        let index = tabBarController.viewControllers!.index(of: viewController)
        
        if index != 1 {
                
            lastCartIndex = index ?? 2
        }
        
        
        if index == 2 {

            if self.selectedIndex != 2 {

                return true
            }

            if let navigationController = viewController as? UINavigationController {

                if let nejreeBannerVC = navigationController.viewControllers.last as? NejreeBannerVC
                {
                    nejreeBannerVC.collView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
                }
            }
            
        }
        
        return true
    }

}
