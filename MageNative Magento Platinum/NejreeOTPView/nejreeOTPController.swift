//
//  nejreeOTPController.swift
//  MageNative Magento Platinum
//
//  Created by MacMini on 19/08/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseAuth
import FirebaseAnalytics



class nejreeOTPController: MagenativeUIViewController,UITextFieldDelegate {

    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var otpView: UIView!
    @IBOutlet weak var otpTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var resendButton: UIButton!
    @IBOutlet weak var validOtpImage: UIImageView!
    @IBOutlet weak var sideView: UIView!
    var mainvc: UIViewController?
    var isOldUser = false
    
    var isAPICalledOTP = false
    
    
    var postString = [String:String]()
    var userCred = [String:String]()
    var name = ""
    var isCheckout = false
    var total = [String:String]()
    var mobileNumber = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        if(isOldUser)
//        {
//            self.backButton.isHidden = true;
//        }
        self.navigationController?.navigationBar.isHidden = true
        setupOtpView()
        resendButton.isHidden = false
        resendButton.addTarget(self, action: #selector(resendButtonTapped(_:)), for: .touchUpInside)
        // Do any additional setup after loading the view.
        
        
        if APP_DEL.selectedLanguage == Arabic {
            
            backButton.setImage(UIImage(named: "BackArrowNewArabic"), for: .normal)
            
        } else {
          
            backButton.setImage(UIImage(named: "BackArrowNew"), for: .normal)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }

    
    
    func setupOtpView() {
        
        topLabel.text = APP_LBL().verify_phone_number
        contentLabel.text = APP_LBL().a_six_digit_code_was_sent_to_your_phone_number
        contentLabel.numberOfLines = 0
        otpView.roundCorners()//layer.cornerRadius = 5
        continueButton.layer.borderColor = UIColor.lightGray.cgColor
        continueButton.layer.borderWidth = 1.0
        continueButton.setTitle(APP_LBL().continue_c.uppercased(), for: .normal)
        continueButton.setTitleColor(.lightGray, for: .normal)
        continueButton.roundCorners()//layer.cornerRadius = 5
        
        resendButton.setTitle(APP_LBL().re_send.uppercased(), for: .normal)
        resendButton.backgroundColor = UIColor.clear
        resendButton.setTitleColor(UIColor.init(hexString: "#FBAD18"), for: .normal)

        otpTextField.textAlignment = .center
        otpTextField.placeholder = APP_LBL().six_digit_code.uppercased()
        otpTextField.keyboardType = .asciiCapableNumberPad
        otpTextField.delegate = self
        
        
        
        if isOldUser {
            //backButton.setImage(UIImage(named: ""), for: .normal)
        } else {
            
             cedMageLoaders.addDefaultLoader(me: self)
            
             self.sendRequest(url: "mobiconnect/customer/register", params: self.postString as? Dictionary<String, String>)
            
            backButton.setImage(UIImage(named: "crossMark"), for: .normal)
        }
        
        
        backButton.tintColor = .white
        backButton.addTarget(self, action: #selector(backButtonTapped(_:)), for: .touchUpInside)
        
        continueButton.addTarget(self, action: #selector(continueTapped(_:)), for: .touchUpInside)
        
        sideView.isHidden = true
        validOtpImage.image=UIImage(named: "")
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        guard let str = textField.text else {return}
        
        if str.count > 6 || str.count < 6 {
            validOtpImage.image = UIImage(named: "wrongname")
            otpView.layer.borderWidth = 1
            otpView.layer.borderColor = UIColor.init(hexString: "#741A31")?.cgColor
            otpTextField.titleColor = UIColor.clear
            sideView.backgroundColor = .black
            continueButton.layer.borderColor = UIColor.lightGray.cgColor
        } else {
            otpView.layer.borderColor = nejreeColor?.cgColor
            validOtpImage.image = UIImage(named: "verified")
            resendButton.isHidden = false
            continueButton.alpha = 1
            continueButton.setTitleColor(.white, for: .normal)
            continueButton.layer.borderColor = UIColor.init(hexString: "#FBAD18")?.cgColor
            otpTextField.titleColor = UIColor.clear
        }
    }
    
    
    
    
    
        
        @objc func resendButtonTapped(_ sender: UIButton) {
        
        
        let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
                                           let hashKey = userInfoDict["hashKey"] ?? "";
                                              let customerId = userInfoDict["customerId"] ?? "";
            let mobileNo = "+\(APP_DEL.selectedCountry.phone_code ?? "966")" + mobileNumber


        var lang_value = ""
             
                     if APP_DEL.selectedLanguage == Arabic {
                          lang_value = "2"
                      } else {
                          lang_value = "1"
                      }
               
        

            let params = ["telephone": mobileNo, "hashkey": hashKey,"customer_id" : customerId, "store_id" : lang_value, "country_id" : (APP_DEL.selectedCountry.country_code ?? "SA")]


                            let url = "mobiconnect/customer/otpdata"

                            var httpUrl = cedMage.getInfoPlist(fileName:"cedMage",indexString: "cedBaseUrl") as! String
                            let reqUrl = httpUrl+url
        
        
                            var postString=Dictionary<String,Dictionary<String,String>>()
                            var postString1=""
                            print(reqUrl)
                            var makeRequest = URLRequest(url: URL(string: reqUrl)!)

                            _ = cedMage.getInfoPlist(fileName:"cedMage",indexString: "requestheader") as! String
                            let storeId = defaults.value(forKey: "storeId") as? String

                            if(params != nil){
                                makeRequest.httpMethod = "POST"

                                    postString=["parameters":[:]]
                                    for (key,value) in params
                                    {
                                        _ = postString["parameters"]?.updateValue(value ?? "", forKey:key)
                                    }

                                    if storeId != nil {
                                        _ = postString["parameters"]?.updateValue(storeId!, forKey:"store_id")
                                    }

                                    postString1=postString.convtToJson() as String
                                    makeRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
                                
                                makeRequest.setValue(API().getAuthorization(), forHTTPHeaderField: "Authorization")

                            }
            

                            print(reqUrl)
                            print(postString)
                            makeRequest.httpBody = postString1.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))

                            let task = URLSession.shared.dataTask(with: makeRequest, completionHandler: {data,response,error in
                                //cedMageLoaders.removeLoadingIndicator(me: self)
                          //      print(NSString(data: data!, encoding: String.Encoding.utf8.rawValue)!)
                                // check for http errors
                                if let httpStatus = response as? HTTPURLResponse , httpStatus.statusCode != 200
                                {

                                    DispatchQueue.main.async
                                        {
                                            cedMageLoaders.removeLoadingIndicator(me: self)



                                            print("poststring=\(postString1)")
                                            print(NSString(data: data!, encoding: String.Encoding.utf8.rawValue) ?? "")
                                            print("statusCode should be 200, but is \(httpStatus.statusCode)")

                                            print("response = \(response)")

                                    }
                                    return;
                                }

                                // code to fetch values from response :: start


                                guard error == nil && data != nil else
                                {
                                    DispatchQueue.main.async
                                        {

                                            cedMageLoaders.removeLoadingIndicator(me: self)
                                            cedMageHttpException.showAlertView(me: self, msg: error?.localizedDescription, title: APP_LBL().error )
                                            print("error=\(error)")

                                    }
                                    return ;
                                }

                                DispatchQueue.main.async
                                    {
                                        cedMageLoaders.removeLoadingIndicator(me: self)

                                        print("response = \(response)")
                                       var json = try!JSON(data:data!)
                                       print(json)

                                      self.view.makeToast(APP_LBL().otp_sent_success, duration: 1.0, position: .center)
                                        


                                       /// self.recieveResponse(data: data, requestUrl: url, response: response)

                                }
                            })

                            task.resume()
        
        
        
//        PhoneAuthProvider.provider().verifyPhoneNumber("+966" + mobileNumber, uiDelegate: nil) { (secretKey, error) in
//
//            if error == nil {
//                print("secretKey \(secretKey)")
//                guard let verificationKey = secretKey else {return}
//                self.defaults.set(verificationKey, forKey: "mobileVerificationId")
//                self.defaults.synchronize()
//                self.view.makeToast("OTP sent Successfully".localized, duration: 1.0, position: .center)
//            }else {
//                print("error is \(error?.localizedDescription)")
//                self.view.makeToast((error?.localizedDescription)!, duration: 2.0, position: .center)
//            }
//
//        }
    }
    
    
    @objc func backButtonTapped(_ sender: UIButton) {
        
//        LoginManager().logOut()
//        GIDSignIn.sharedInstance().signOut()
//
//        self.defaults.set(false, forKey: "isLogin");
//        self.defaults.removeObject(forKey: "userInfoDict");
//        self.defaults.removeObject(forKey: "selectedAddressId");
//        defaults.setValue("Magenative App For Magento 2", forKey: "name")
//        UserDefaults.standard.set(nil,forKey: "LIAccessToken")
//        UserDefaults.standard.synchronize();
//        defaults.synchronize()
//        self.defaults.removeObject(forKey: "selectedAddressDescription");
//
//        self.defaults.removeObject(forKey: "cartId");
//        self.defaults.removeObject(forKey: "cart_summary");
//        self.defaults.removeObject(forKey: "guestEmail");
//        self.defaults.set("0", forKey: "items_count");
//        self.defaults.removeObject(forKey: "name")
//        //self.defaults.removeObject(forKey: "mobile_number")
//        self.setCartCount(view: self, items: "0")
        
        self.navigationController?.popViewController(animated: true)
//        self.dismiss(animated: true) {
//
//        }
    }
    
    
    @objc func continueTapped(_ sender: UIButton) {
        
        //self.view.user
       
        
//        if isAPICalledOTP {
//            return;
//        }
        
        print("Clicked")
        
        
        guard let otp = otpTextField.text else {return}
        
        if otp == "" {
            self.view.makeToast(APP_LBL().please_enter_six_digit_otp, duration: 1.0, position: .center); return
        }
        
         cedMageLoaders.addDefaultLoader(me: self)
        isAPICalledOTP = true
        
        let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
                                    let hashKey = userInfoDict["hashKey"]!;
                                       let customerId = userInfoDict["customerId"]!;

               let params = ["telephone": "+966" + mobileNumber, "hashkey": hashKey,"customer_id" : customerId, "numberotp" : otp]


                  
        
                    let url = "mobiconnect/customer/verifyotp"

                    var httpUrl = cedMage.getInfoPlist(fileName:"cedMage",indexString: "cedBaseUrl") as! String
                    let reqUrl = httpUrl+url
        

                     var postString=Dictionary<String,Dictionary<String,String>>()
                     var postString1=""
                     print(reqUrl)
                     var makeRequest = URLRequest(url: URL(string: reqUrl)!)

                     _ = cedMage.getInfoPlist(fileName:"cedMage",indexString: "requestheader") as! String
                     let storeId = defaults.value(forKey: "storeId") as? String

                     if(params != nil){
                         makeRequest.httpMethod = "POST"

                             postString=["parameters":[:]]
                             for (key,value) in params
                             {
                                 _ = postString["parameters"]?.updateValue(value ?? "", forKey:key)
                             }

                             if storeId != nil {
                                 _ = postString["parameters"]?.updateValue(storeId!, forKey:"store_id")
                             }

                             postString1=postString.convtToJson() as String
                             makeRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
                         makeRequest.setValue(API().getAuthorization(), forHTTPHeaderField: "Authorization")

                     }

                     print(reqUrl)
                     print(postString)
                     makeRequest.httpBody = postString1.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))

                     let task = URLSession.shared.dataTask(with: makeRequest, completionHandler: {data,response,error in
                         //cedMageLoaders.removeLoadingIndicator(me: self)
                   //      print(NSString(data: data!, encoding: String.Encoding.utf8.rawValue)!)
                         // check for http errors
                         if let httpStatus = response as? HTTPURLResponse , httpStatus.statusCode != 200
                         {

                             DispatchQueue.main.async
                                 {
                                     cedMageLoaders.removeLoadingIndicator(me: self)



                                     print("poststring=\(postString1)")
                                     print(NSString(data: data!, encoding: String.Encoding.utf8.rawValue) ?? "")
                                     print("statusCode should be 200, but is \(httpStatus.statusCode)")

                                     print("response = \(response)")

                             }
                             return;
                         }

                         // code to fetch values from response :: start


                         guard error == nil && data != nil else
                         {
                             DispatchQueue.main.async
                                 {

                                     cedMageLoaders.removeLoadingIndicator(me: self)
                                     cedMageHttpException.showAlertView(me: self, msg: error?.localizedDescription, title: APP_LBL().error )
                                     print("error=\(error)")

                             }
                             return ;
                         }

                         DispatchQueue.main.async
                             {
                                 cedMageLoaders.removeLoadingIndicator(me: self)

                                 print("response = \(response)")
                                guard let data = data else {return}
                                //            var json = try! JSON(data: data)
                                guard var json = try? JSON(data: data) else {
                                    return;
                                }
                                print(json)


//                                let json = try? JSONSerialization.jsonObject(with: data!, options: [])
//
//                                    print(json)


                               if json == nil{


                               }
                               else{

                                if json[0]["code"] == 0 {
                                   cedMageLoaders.removeLoadingIndicator(me: self)
                                  self.view.makeToast(APP_LBL().please_enter_correct_otp, duration: 1.0, position: .center)
                                    return
                                }
                                
                                    if self.isOldUser {

                                        
                                        
                                         self.view.makeToast(APP_LBL().otp_verification_successful, duration: 2.0, position: .center)

                                        let userInfoDict = self.defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
                                        let custId = userInfoDict["customerId"]
                                        let storeId = UserDefaults.standard.value(forKey: "storeId") as! String?
                                        cedMageLoaders.removeLoadingIndicator(me: self)
                                    
                                        self.sendRequest(url: "mobiconnect/saveMobile", params:["customer_id":custId!,"store_id":storeId!,"mobile_numbers":self.mobileNumber, "phonecode" : "+" + (APP_DEL.selectedCountry.phone_code ?? "966")])
                                    }
                                    else
                                    {
                                        let userInfoDict = self.defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
                                        
                                        let detail : [AnalyticKey:String] = [
                                            .Email : userInfoDict["email"] ?? "",
                                            .PhoneNumber : userInfoDict["mobile_number"] ?? ""
                                        ]
                                        API().setEvent(eventName: .SignUp, eventDetail: detail) { (json, err) in }
                                        
                                        let DataLogin = ["METHOD" : "email",
                                                                  "EMAIL" : userInfoDict["email"] as Any,
                                                                  "PHONE_NUMBER" : userInfoDict["mobile_number"] as Any] as [String : Any]
                                        Analytics.logEvent(AnalyticsEventSignUp, parameters: DataLogin)
                                        
                                        
                                        self.view.makeToast(APP_LBL().login_successful, duration: 1.5, position: .center, title: nil, image: nil, style: nil) { (true) in

                                                     let view = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "mainTabbar") as! cedMageTabbar
                                                     view.modalPresentationStyle = .fullScreen
                                                     self.present(view, animated: true, completion: nil)
                                    }
                                }

                                }

                         }
                     })

                     task.resume()

        cedMageLoaders.removeLoadingIndicator(me: self)
        
    }
    
    override func recieveResponse(data: Data?, requestUrl: String?, response: URLResponse?) {
        guard let data = data else {return}
//            var json = try! JSON(data: data)
        guard var json = try? JSON(data: data) else {
            return;
        }
        
        print(json)
        
        if requestUrl == "mobiconnect/saveMobile" {
            
            self.defaults.setValue(self.mobileNumber, forKey: "mobile_number")
            self.defaults.set(true, forKey: "isLogin")
            
            (UIApplication.shared.delegate as! AppDelegate).changeLanguage()
            
//            let view = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "mainTabbar") as! cedMageTabbar
//            view.modalPresentationStyle = .fullScreen
//            self.present(view, animated: true, completion: nil)
        }
        
        if requestUrl != "mobiconnect/customer/login" {
            let status = json[0]["data"]["customer"][0]["status"].stringValue;
            if(status == "success")
            {
                let isConfirm = json[0]["data"]["customer"][0]["isConfirmationRequired"].stringValue
                print(isConfirm)
                if(isConfirm == "NO"){
                   
                    let email=userCred["email"]
                    let password=userCred["password"]
                    name = json[0]["data"]["customer"][0]["name"].stringValue
                    self.loginFunction(email: email!, password: password!)
                }else{
                    cedMageHttpException.showAlertView(me: self, msg: APP_LBL().registration_successful, title: APP_LBL().success)
                    cedMage.delay(delay: 2, closure: {
                        _ = self.navigationController?.popToRootViewController(animated: true)
                    })
                }
                return;
            }else{
                
                let upview = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height))
                upview.backgroundColor = UIColor.black.withAlphaComponent(0.5)
                upview.tag = 151
                self.view.addSubview(upview)
                let popup = nejreeExistingUser()
                popup.tag = 152
                self.view.addSubview(popup)
                popup.translatesAutoresizingMaskIntoConstraints = false
                popup.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 20).isActive = true
                popup.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -20).isActive = true
                popup.centerYAnchor.constraint(equalTo: self.view.centerYAnchor, constant: 0).isActive = true
                popup.centerXAnchor.constraint(equalTo: self.view.centerXAnchor, constant: 0).isActive = true
                popup.heightAnchor.constraint(equalToConstant: 200).isActive = true
                popup.signInButton.roundCorners()
                popup.cancelButton.roundCorners()
                popup.cancelButton.addTarget(self, action: #selector(popupCancelTapped(_:)), for: .touchUpInside)
                popup.signInButton.addTarget(self, action: #selector(popupsignInTapped(_:)), for: .touchUpInside)

                return
            }
        } else {
            
            if json["data"]["customer"][0]["status"].stringValue == "error" {
                cedMageLoaders.removeLoadingIndicator(me: self)
                self.view.makeToast(json["data"]["customer"][0]["message"].stringValue, duration: 1.0, position: .center)
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    self.dismiss(animated: true, completion: nil)
                    return
                }
            }
            
            defaults.setValue(name, forKey: "name")
            defaults.setValue(json[0]["data"]["customer"][0]["mobile_number"].stringValue, forKey: "mobile_number")
            defaults.set(true,forKey: "isLogin")
           
            let customer_id = json[0]["data"]["customer"][0]["customer_id"].stringValue;
            let hashKey = json[0]["data"]["customer"][0]["hash"].stringValue;
            let cart_summary = json[0]["data"]["customer"][0]["cart_summary"].intValue;
            //saving value in NSUserDefault to use later on :: start
            let email=userCred["email"]
            let dict = ["email": email!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines), "customerId": customer_id, "hashKey": hashKey];
            self.defaults.set(true, forKey: "isLogin")
            self.defaults.set(dict, forKey: "userInfoDict");
            self.defaults.set(cart_summary, forKey: "cart_summary");
            self.defaults.setValue( json[0]["data"]["customer"][0]["gender"].stringValue, forKey: "gender")
            self.defaults.synchronize()
            
        
                   
//        let view = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "mainTabbar") as! cedMageTabbar
//        view.modalPresentationStyle = .fullScreen
//        self.present(view, animated: true, completion: nil)

                 var lang_value = ""
                              
                                    if APP_DEL.selectedLanguage == Arabic {
                                          lang_value = "2"
                                      } else {
                                          lang_value = "1"
                                      }
                                       
           
            
            let params = ["telephone": json[0]["data"]["customer"][0]["mobile_number"].stringValue, "hashkey": hashKey,"customer_id" : customer_id, "store_id" : lang_value, "country_id" : (APP_DEL.selectedCountry.country_code ?? "SA")]

                        

                        let url = "mobiconnect/customer/otpdata"

                        var httpUrl = cedMage.getInfoPlist(fileName:"cedMage",indexString: "cedBaseUrl") as! String
                        let reqUrl = httpUrl+url
            
            
                         var postString=Dictionary<String,Dictionary<String,String>>()
                         var postString1=""
                         print(reqUrl)
                         var makeRequest = URLRequest(url: URL(string: reqUrl)!)

                         _ = cedMage.getInfoPlist(fileName:"cedMage",indexString: "requestheader") as! String
                         let storeId = defaults.value(forKey: "storeId") as? String

                         if(params != nil){
                             makeRequest.httpMethod = "POST"

                                 postString=["parameters":[:]]
                            _ = postString["parameters"]?.updateValue((APP_DEL.selectedCountry.country_code ?? "SA"), forKey:"country_id")
                                 for (key,value) in params
                                 {
                                     _ = postString["parameters"]?.updateValue(value ?? "", forKey:key)
                                 }

                                 if storeId != nil {
                                     _ = postString["parameters"]?.updateValue(storeId!, forKey:"store_id")
                                 }

                                 postString1=postString.convtToJson() as String
                                 makeRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
                             makeRequest.setValue(API().getAuthorization(), forHTTPHeaderField: "Authorization")

                         }

                         print(reqUrl)
                         print(postString)
                         makeRequest.httpBody = postString1.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))

                         let task = URLSession.shared.dataTask(with: makeRequest, completionHandler: {data,response,error in
                             //cedMageLoaders.removeLoadingIndicator(me: self)
                       //      print(NSString(data: data!, encoding: String.Encoding.utf8.rawValue)!)
                             // check for http errors
                             if let httpStatus = response as? HTTPURLResponse , httpStatus.statusCode != 200
                             {

                                 DispatchQueue.main.async
                                     {
                                         cedMageLoaders.removeLoadingIndicator(me: self)



                                         print("poststring=\(postString1)")
                                         print(NSString(data: data!, encoding: String.Encoding.utf8.rawValue) ?? "")
                                         print("statusCode should be 200, but is \(httpStatus.statusCode)")

                                         print("response = \(response)")

                                 }
                                 return;
                             }

                             // code to fetch values from response :: start


                             guard error == nil && data != nil else
                             {
                                 DispatchQueue.main.async
                                     {

                                         cedMageLoaders.removeLoadingIndicator(me: self)
                                         cedMageHttpException.showAlertView(me: self, msg: error?.localizedDescription, title: APP_LBL().error )
                                         print("error=\(error)")

                                 }
                                 return ;
                             }

                             DispatchQueue.main.async
                                 {
                                     cedMageLoaders.removeLoadingIndicator(me: self)

                                     print("response = \(response)")
//                                    var json = try!JSON(data:data!)
//                                    print(json)


                                    let json = try? JSONSerialization.jsonObject(with: data!, options: [])
                                    print(json)


                                    if json == nil{

                                        self.view.makeToast(APP_LBL().login_successful, duration: 1.5, position: .center, title: nil, image: nil, style: nil) { (true) in

                                            (UIApplication.shared.delegate as! AppDelegate).changeLanguage()
                                            
//                                                         let view = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "mainTabbar") as! cedMageTabbar
//                                                         view.modalPresentationStyle = .fullScreen
//                                                         self.present(view, animated: true, completion: nil)
                                        }
                                    }
                                    else
                                    {

                                    }


//                                   print("\(json[0]["code"])")
//
//
//                                   if json[0]["code"] == 0 {
//                                        cedMageLoaders.removeLoadingIndicator(me: self)
//                                       self.view.makeToast("Please enter valid phone number".localized, duration: 1.0, position: .center)
//                                   }
//                                   else
//                                   {
//
//                                   }




                                    /// self.recieveResponse(data: data, requestUrl: url, response: response)

                             }
                         })

                         task.resume()
            
            
            
            
         
            
        }
        
    }
    
    
    
    
    
    func loginFunction(email:String,password:String){
        var email = email;
        email = email.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        
        var password = password;
        password = password.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        
        var postString = ["email":email,"password":password];
        if(defaults.object(forKey: "cartId") != nil)
        {
            let cart_id = defaults.object(forKey: "cartId") as? String;
            //postString += "&cart_id="+cart_id!;
            postString.updateValue(cart_id!, forKey: "cart_id")
        }
        self.sendRequest(url: "mobiconnect/customer/login", params: postString)
    }
    
    @objc func popupCancelTapped(_ sender: UIButton) {
        self.view.viewWithTag(151)?.removeFromSuperview()
        self.view.viewWithTag(152)?.removeFromSuperview()
    }
    @objc func popupsignInTapped(_ sender: UIButton) {
        self.view.viewWithTag(151)?.removeFromSuperview()
        self.view.viewWithTag(152)?.removeFromSuperview()
        let vc = UIStoryboard(name: "cedMageLogin", bundle: nil).instantiateViewController(withIdentifier: "nejreeLoginController") as! nejreeLoginController
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    
    

}
