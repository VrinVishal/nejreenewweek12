//
//  NejreeSignupVC.swift
//  MageNative Magento Platinum
//
//  Created by Vrinsoft on 22/07/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseAnalytics



class NejreeSignupVC: MagenativeUIViewController,UIGestureRecognizerDelegate {
    
    @IBOutlet weak var otpStack: UIStackView!

    @IBOutlet weak var scrllView: UIScrollView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var imgBgBanner: UIImageView!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var lblSignUpTitle: UILabel!
    
    @IBOutlet weak var viewOne: UIView!
    @IBOutlet weak var lblOne: UILabel!
    
    @IBOutlet weak var lblLine: UILabel!

    
    
    @IBOutlet weak var viewTwo: UIView!
    @IBOutlet weak var lblTwo: UILabel!
    
    @IBOutlet weak var viewField: UIView!
    @IBOutlet weak var viewOtp: UIView!
    
    @IBOutlet weak var imgSelectTerms: UIImageView!
    @IBOutlet weak var lblAccept: UILabel!
    @IBOutlet weak var lblTermsAndConditions: UILabel!

    @IBOutlet weak var btnSignUp: UIButton!
    @IBOutlet weak var btnTermsAndCond: UIButton!
    
    @IBOutlet weak var lblSixDigitCodeWasSent: UILabel!
    
    @IBOutlet weak var txtOne: UITextField!
    @IBOutlet weak var txtTwo: UITextField!
    @IBOutlet weak var txtThree: UITextField!
    @IBOutlet weak var txtFour: UITextField!
    @IBOutlet weak var txtFive: UITextField!
    @IBOutlet weak var txtSix: UITextField!
    
    @IBOutlet weak var lblDidntReceive: UILabel!
    @IBOutlet weak var lblResend: UILabel!
    @IBOutlet weak var btnResend: UIButton!
    
    @IBOutlet weak var btnContinue: UIButton!
    
    @IBOutlet weak var lblAlreadyHaveAccount: UILabel!
    @IBOutlet weak var btnSignin: UIButton!
    @IBOutlet weak var lblSigin: UILabel!
    
    @IBOutlet weak var viewFirstName: UIView!
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var FirstNameVerifyImage: UIImageView!

    @IBOutlet weak var viewLastName: UIView!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var LastNameVerifyImage: UIImageView!
    
    @IBOutlet weak var viewEmail: UIView!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var emailVerifyImage: UIImageView!
    
    @IBOutlet weak var viewPhoneNum: UIView!
    @IBOutlet weak var txtPhoneNum: UITextField!
    @IBOutlet weak var PhoneNumVerifyImage: UIImageView!

    @IBOutlet weak var viewPassword: UIView!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var PasswordVerifyImage: UIImageView!
    
    @IBOutlet weak var viewConfirmPassword: UIView!
    @IBOutlet weak var txtConfirmPassword: UITextField!
    @IBOutlet weak var ConfirmPasswordVerifyImage: UIImageView!
    
    @IBOutlet weak var lblCountryCode: UILabel!
    
    @IBOutlet weak var BtnAcceptTerms: UIButton!
    
    var isTermsSelected = Bool()
    var subscribed = 0
    var dataToPost = [String: String]()
    var userCred = [String:String]()
    var countryCode = APP_DEL.selectedCountry.phone_code
    var name = ""
    
    var mainvc: UIViewController?
    var isCheckout = false
    var isOldUser = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lblCountryCode.text = "+" + (APP_DEL.selectedCountry.phone_code ?? "966")
        setUI()
    }
    
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        
           if navigationController?.responds(to: #selector(getter: UINavigationController.interactivePopGestureRecognizer)) ?? false {
               
               self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
               self.navigationController?.interactivePopGestureRecognizer?.delegate = self
           }
           
       }
       
       func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
           
           return true
       }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func setUI() {
        
        viewOtp.isHidden = true
        
        isTermsSelected = false
        imgSelectTerms.image = UIImage(named: "icon_checkbox_white_un")
        otpStack.semanticContentAttribute = .forceLeftToRight
        
        if APP_DEL.selectedLanguage == Arabic {
            
            self.btnBack.setImage(UIImage(named: "BackArrowNewArabic"), for: .normal)
            
            txtFirstName.textAlignment = .right
            txtLastName.textAlignment = .right
            txtEmail.textAlignment = .right
//            txtPhoneNum.textAlignment = .right
            txtPassword.textAlignment = .right
            txtConfirmPassword.textAlignment = .right
            
        }else{
            self.btnBack.setImage(UIImage(named: "BackArrowNew"), for: .normal)
            
            txtFirstName.textAlignment = .left
            txtLastName.textAlignment = .left
            txtEmail.textAlignment = .left
//            txtPhoneNum.textAlignment = .left
            txtPassword.textAlignment = .left
            txtConfirmPassword.textAlignment = .left
        }
        
        lblSignUpTitle.text = APP_LBL().sign_up.uppercased()
        txtFirstName.placeholder = APP_LBL().first_name.uppercased()
        txtLastName.placeholder = APP_LBL().lastname.uppercased()
        txtEmail.placeholder = APP_LBL().email_star.uppercased()
        txtPhoneNum.placeholder = APP_LBL().phone_star.uppercased()
        txtPassword.placeholder = APP_LBL().password_star.uppercased()
        txtConfirmPassword.placeholder = APP_LBL().confirm_password_star.uppercased()
        
     
        txtFirstName.attributedPlaceholder = NSAttributedString(string: APP_LBL().first_name.uppercased(),
        attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        
        txtLastName.attributedPlaceholder = NSAttributedString(string: APP_LBL().lastname.uppercased(),
               attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        
        txtEmail.attributedPlaceholder = NSAttributedString(string: APP_LBL().email_star.uppercased(),
               attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        
        txtPhoneNum.attributedPlaceholder = NSAttributedString(string: APP_LBL().phone_star.uppercased(),
               attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        txtPhoneNum.keyboardType = .asciiCapableNumberPad
        
        txtPassword.attributedPlaceholder = NSAttributedString(string: APP_LBL().password_star.uppercased(),
               attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        
        txtConfirmPassword.attributedPlaceholder = NSAttributedString(string: APP_LBL().confirm_password_star.uppercased(),
               attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        
        lblAccept.text = APP_LBL().accept_terms_condition
        lblTermsAndConditions.text = APP_LBL().terms_amp_condition.uppercased()
        
        btnSignUp.setTitle(APP_LBL().sign_up, for: .normal)
        lblSigin.text = APP_LBL().sign_in.uppercased()
        lblAlreadyHaveAccount.text = APP_LBL().already_have_an_account
        
        btnContinue.setTitle(APP_LBL().continue_c.uppercased(), for: .normal)

        lblSixDigitCodeWasSent.text = APP_LBL().a_six_digit_code_was_sent_to_your_phone_number
        
        lblDidntReceive.text = String(format: "%@ -", APP_LBL().didnt_receive_otp)
        
        lblResend.text = APP_LBL().resend.uppercased()
        
        FirstNameVerifyImage.image = UIImage(named: "")
        LastNameVerifyImage.image = UIImage(named: "")
        emailVerifyImage.image = UIImage(named: "")
        PhoneNumVerifyImage.image = UIImage(named: "")
        PasswordVerifyImage.image = UIImage(named: "")
        ConfirmPasswordVerifyImage.image = UIImage(named: "")
                
        viewOne.setCornerRadius()
        viewTwo.setCornerRadius()
        
        viewFirstName.setCornerRadius()
        viewLastName.setCornerRadius()
        viewEmail.setCornerRadius()
        viewPhoneNum.setCornerRadius()
        viewPassword.setCornerRadius()
        viewConfirmPassword.setCornerRadius()

        txtOne.setCornerRadius()
        txtTwo.setCornerRadius()
        txtThree.setCornerRadius()
        txtFour.setCornerRadius()
        txtFive.setCornerRadius()
        txtSix.setCornerRadius()
        
        txtOne.keyboardType = .asciiCapableNumberPad
        txtTwo.keyboardType = .asciiCapableNumberPad
        txtThree.keyboardType = .asciiCapableNumberPad
        txtFour.keyboardType = .asciiCapableNumberPad
        txtFive.keyboardType = .asciiCapableNumberPad
        txtSix.keyboardType = .asciiCapableNumberPad
        
        viewOne.setBorder()
        viewTwo.setBorder()
        
        viewFirstName.setBorder()
        viewLastName.setBorder()
        viewEmail.setBorder()
        viewPhoneNum.setBorder()
        viewPassword.setBorder()
        viewConfirmPassword.setBorder()
        
        viewPhoneNum.semanticContentAttribute = .forceLeftToRight
        txtPhoneNum.textAlignment = .left
        
        txtOne.setBorder()
        txtTwo.setBorder()
        txtThree.setBorder()
        txtFour.setBorder()
        txtFive.setBorder()
        txtSix.setBorder()
        
        txtFirstName.delegate = self
        txtLastName.delegate = self
        txtEmail.delegate = self
        txtPhoneNum.delegate = self
        txtPassword.delegate = self
        txtPassword.isSecureTextEntry = true
        txtConfirmPassword.delegate = self
        txtConfirmPassword.isSecureTextEntry = true
        txtOne.delegate = self
        txtTwo.delegate = self
        txtThree.delegate = self
        txtFour.delegate = self
        txtFive.delegate = self
        txtSix.delegate = self
        
        btnContinue.setCornerRadius()
        btnSignUp.setCornerRadius()
        
        btnContinue.setBorder()
        btnSignUp.setBorder()
    }
    
    @IBAction func actionBtnResend(_ sender: UIButton) {
            
        let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
        let hashKey = userInfoDict["hashKey"] ?? "";
        let customerId = userInfoDict["customerId"] ?? "";
        
        let mobileNo = ("+\(APP_DEL.selectedCountry.phone_code ?? "966")" + txtPhoneNum.text!).getArToEnDigit()

        var lang_value = ""
        
        if APP_DEL.selectedLanguage == Arabic {
            lang_value = "2"
        } else {
            lang_value = "1"
        }
                               
        let params = ["telephone": mobileNo,
                      "hashkey": hashKey,
                      "customer_id" : customerId,
                      "store_id" : lang_value]
        
        self.sendOtpData(data: params, isForResend: true)
        
    }
    
    @IBAction func actionBtnAcceptTerms(_ sender: UIButton) {
        
        self.isTermsSelected = !isTermsSelected
        
        if isTermsSelected == true {
            imgSelectTerms.image = UIImage(named: "icon_checkbox_white_sel")
        }else{
            imgSelectTerms.image = UIImage(named: "icon_checkbox_white_un")
        }
    }
    
    @IBAction func actionBtnSignup(_ sender: UIButton) {
        
        let validEmail = EmailVerifier.isValidEmail(testStr: txtEmail.text!)
        
        if !txtFirstName.hasText {
            cedMageHttpException.showAlertView(me: self, msg: APP_LBL().please_enter_first_name, title: APP_LBL().error)

        }else if !txtLastName.hasText {
            cedMageHttpException.showAlertView(me: self, msg: APP_LBL().please_enter_last_name, title: APP_LBL().error)

        }else if !txtEmail.hasText {
            cedMageHttpException.showAlertView(me: self, msg: APP_LBL().please_enter_email, title: APP_LBL().error)

        }else if !validEmail {
        
           cedMageHttpException.showAlertView(me: self, msg: APP_LBL().please_enter_valid_email, title: APP_LBL().error)
            
        }else if !txtPhoneNum.hasText {
            cedMageHttpException.showAlertView(me: self, msg: APP_LBL().please_enter_phone_no, title: APP_LBL().error)

        }else if txtPhoneNum.text!.count < 6 {
            cedMageHttpException.showAlertView(me: self, msg: APP_LBL().please_enter_valid_phone_no, title: APP_LBL().error)

        }else if !txtPassword.hasText{
            cedMageHttpException.showAlertView(me: self, msg: APP_LBL().please_enter_password, title: APP_LBL().error)

        }else if txtPassword.text!.count < 6 {
            cedMageHttpException.showAlertView(me: self, msg: APP_LBL().password_must_contain_more_than_6_digit, title: APP_LBL().error)

        }else if !txtConfirmPassword.hasText{
            cedMageHttpException.showAlertView(me: self, msg: APP_LBL().please_enter_cnfrm_password, title: APP_LBL().error)

        }else if txtPassword.text != txtConfirmPassword.text{
            cedMageHttpException.showAlertView(me: self, msg: APP_LBL().password_and_confirm_password_doesnt_match, title: APP_LBL().error)

        }else if self.isTermsSelected == false{
            cedMageHttpException.showAlertView(me: self, msg: APP_LBL().please_select_terms_and_conditions, title: APP_LBL().error)

        } else {
            
            self.verifyEmail(email: txtEmail.text!)

        }
        
    }
    
    @objc func popupCancelTapped(_ sender: UIButton) {
        
        self.view.viewWithTag(151)?.removeFromSuperview()
        self.view.viewWithTag(152)?.removeFromSuperview()
    }
    
    @objc func popupsignInTapped(_ sender: UIButton) {
        
        self.view.viewWithTag(151)?.removeFromSuperview()
        self.view.viewWithTag(152)?.removeFromSuperview()
        let vc = UIStoryboard(name: "cedMageLogin", bundle: nil).instantiateViewController(withIdentifier: "nejreeLoginController") as! nejreeLoginController
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func actionBtnBack(_ sender: UIButton) {
        
        if viewOtp.isHidden == false {
            viewField.isHidden = false
            viewOtp.isHidden = true
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func actionBtnTermsAndConditions(_ sender: UIButton) {
        
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "termsMenuController") as! termsMenuController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func actionBtnCountinue(_ sender: UIButton) {

        if !txtOne.hasText || !txtTwo.hasText || !txtThree.hasText || !txtFour.hasText || !txtFive.hasText  || !txtSix.hasText {
        
            cedMageHttpException.showAlertView(me: self, msg: APP_LBL().please_enter_six_digit_otp, title: APP_LBL().error)
            return;
        }
        
        self.verifyOTP()
    }
    
    @IBAction func actionBtnSignin(_ sender: UIButton) {
        
//        let vc = UIStoryboard(name: "cedMageLogin", bundle: nil).instantiateViewController(withIdentifier: "nejreeLoginController") as! nejreeLoginController
//        vc.modalPresentationStyle = .fullScreen
//        self.present(vc, animated: true)
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    
    func didFailAutocompleteWithError(_ error: Error) {
        print("error in fetching nearby cities with \(error.localizedDescription)")
    }
    
    //MARK:- VERIFY EMAIL
    func verifyEmail(email: String) {
        
        var postData = [String:String]()

        if (self.defaults.object(forKey: "userInfoDict") != nil) {
            
            let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
            postData["hashkey"] = userInfoDict["hashKey"]!
            postData["customer_id"] = userInfoDict["customerId"]!;
        }
        postData["email"] = email;
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);
        
        API().callAPI(endPoint: "mobiconnect/customer/verifyEmail", method: .POST, param: postData) { (json_res, err) in
            
        //    DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                if err == nil {
                    
                    let json = json_res[0]
                    
                    if json["data"]["customer"][0]["status"].stringValue == "true" {
                        
                        // already exist
                        let upview = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height))
                        upview.backgroundColor = UIColor.black.withAlphaComponent(0.5)
                        upview.tag = 151
                        self.view.addSubview(upview)
                        let popup = nejreeExistingUser()
                        popup.tag = 152
                        self.view.addSubview(popup)
                        popup.translatesAutoresizingMaskIntoConstraints = false
                        popup.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 20).isActive = true
                        popup.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -20).isActive = true
                        popup.centerYAnchor.constraint(equalTo: self.view.centerYAnchor, constant: 0).isActive = true
                        popup.centerXAnchor.constraint(equalTo: self.view.centerXAnchor, constant: 0).isActive = true
                        popup.heightAnchor.constraint(equalToConstant: 200).isActive = true
                        popup.clipsToBounds = true
                        popup.layer.cornerRadius = 8.0
//                        popup.signInButton.roundCorners()
//                        popup.cancelButton.roundCorners()
                        popup.cancelButton.addTarget(self, action: #selector(self.popupCancelTapped(_:)), for: .touchUpInside)
                        popup.signInButton.addTarget(self, action: #selector(self.popupsignInTapped(_:)), for: .touchUpInside)
                        popup.statementLabel.text = APP_LBL().looks_like_an_account_with_the_entered_email_already_exists
                        popup.noticeLabel.text = APP_LBL().notice
                        popup.signInButton.setTitle(APP_LBL().sign_in_c, for: .normal)
                        popup.cancelButton.setTitle(APP_LBL().cancel, for: .normal)
                        
                        return
                        
                    } else {
                        
                        self.dataToPost = ["firstname":self.txtFirstName.text! ,"lastname":self.txtLastName.text!,"email":self.txtEmail.text! ,"password":self.txtPassword.text! ,"is_subscribed":"\(self.subscribed)","gender":"","dob": "" ,"taxvat":"","prefix":"","suffix":"","middlename":"","mobile_numbers":self.txtPhoneNum.text!.getArToEnDigit()];
                        
                        self.userCred["email"] = self.txtEmail.text!
                        self.userCred["password"] = self.txtPassword.text!
                        
                        if (APP_DEL.needToShowOtpScreenOrNot()){
                            self.sendOTPNew(data: self.dataToPost)
                        } else {
                            self.registerUser(data: self.dataToPost)
                        }
                    }
                }
          //  }
        }
    }
    
    //MARK:- SEND OTP NEW
    func sendOTPNew(data: [String:String]) {
        
        let mobileNumber = txtPhoneNum.text!.getArToEnDigit()
        
        if mobileNumber == "" {
            self.view.makeToast(APP_LBL().please_enter_mobile_number, duration: 2.0, position: .center);
            return
        }
        
        if mobileNumber.count != (Int(APP_DEL.selectedCountry.phone_number_limit ?? "") ?? 9) {
            
            self.view.makeToast(APP_LBL().mobile_number_should_be_of_xyz_digits_only, duration: 2.0, position: .center);
            return
        }
        
        viewOtp.isHidden = false
        viewField.isHidden = true
        
        if isOldUser {
            //backButton.setImage(UIImage(named: ""), for: .normal)
        } else {

            self.registerUser(data: data)
        }
    }
    
    //MARK:- REGISTER USER
    func registerUser(data: [String : String]) {
        
        var postData = data

        if (self.defaults.object(forKey: "userInfoDict") != nil) {
            
            let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
            postData["hashkey"] = userInfoDict["hashKey"]!
            postData["customer_id"] = userInfoDict["customerId"]!;
            postData["store_id"] = UserDefaults.standard.value(forKey: "storeId") as? String
        }
        postData["country_id"] =  APP_DEL.selectedCountry.country_code ?? "SA"

        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);
        
        API().callAPI(endPoint: "mobiconnect/customer/register", method: .POST, param: postData) { (json_res, err) in
            
         //   DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                if err == nil {
                    
                    let json = json_res[0]
                    
                    let status = json["data"]["customer"][0]["status"].stringValue;
                    
                    if (status == "success") {
                        let isConfirm = json["data"]["customer"][0]["isConfirmationRequired"].stringValue
                        print(isConfirm)
                        
//                        if (isConfirm == "NO") {
//
//                            let email=self.userCred["email"]
//                            let password=self.userCred["password"]
//                            self.name = json["data"]["customer"][0]["name"].stringValue
//
//                            self.login(em: email!, passwd: password!)
//
//                        } else {
                            
                            self.dataToPost = ["firstname":self.txtFirstName.text! ,"lastname":self.txtLastName.text!,"email":self.txtEmail.text! ,"password":self.txtPassword.text! ,"is_subscribed":"\(self.subscribed)","gender":"","dob": "" ,"taxvat":"","prefix":"","suffix":"","middlename":"","mobile_numbers":self.txtPhoneNum.text!.getArToEnDigit(),"customerId":json["data"]["customer"][0]["customer_id"].stringValue];
                            
                            let dict = ["email": self.txtEmail.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines), "customerId": json["data"]["customer"][0]["customer_id"].stringValue, "hashKey": json["data"]["customer"][0]["hash"].stringValue];
                            self.defaults.set(dict, forKey: "userInfoDict");
                            
                            let mobileNo = ("+\(APP_DEL.selectedCountry.phone_code ?? "966")" + self.txtPhoneNum.text!).getArToEnDigit()

                            var lang_value = ""
                           
                            if APP_DEL.selectedLanguage == Arabic {
                                lang_value = "2"
                            } else {
                                lang_value = "1"
                            }
                                                   
                            let params = ["telephone": mobileNo,
                                          "hashkey": "",
                                          "customer_id" : json["data"]["customer"][0]["customer_id"].stringValue,
                                          "store_id" : lang_value]
                            
                            self.isForRegisterOTP = true
                           // self.sendOtpData(data: params, isForResend: false)
                            
                        if (APP_DEL.needToShowOtpScreenOrNot()){
                            self.sendOtpData(data: params, isForResend: false)
                        } else {
                            cedMageHttpException.showAlertView(me: self, msg: APP_LBL().registration_successful, title: APP_LBL().success)
                            cedMage.delay(delay: 2, closure: {
                                let email=self.userCred["email"]
                                let password=self.userCred["password"]
                               
                                self.login(em: email!, passwd: password!)
                            })
                        }
                        
                        return;
                        
                    } else {
                        
                        let title = APP_LBL().error
                        let errorMsg = json["data"]["customer"][0]["message"].string;
                        cedMageHttpException.showAlertView(me: self, msg: errorMsg, title: title);
                        return
                    }
                }
         //   }
        }
    }
    
    //MARK:- LOGIN USER
    func login(em: String, passwd: String) {
        

        let email = em.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let password = passwd.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        
        var postData = ["email":email,"password":password];
        
        if (defaults.object(forKey: "cartId") != nil)
        {
            let cart_id = defaults.object(forKey: "cartId") as? String;
            postData["cart_id"] = cart_id
        }
        

        if (self.defaults.object(forKey: "userInfoDict") != nil) {

            postData["store_id"] = UserDefaults.standard.value(forKey: "storeId") as? String
        }
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);
        
        API().callAPI(endPoint: "mobiconnect/customer/login", method: .POST, param: postData) { (json, err) in
            
         //   DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                if err == nil {
                    
                    if json["data"]["customer"][0]["status"].stringValue == "error" {
                         
                         self.view.makeToast(json["data"]["customer"][0]["message"].stringValue, duration: 1.0, position: .center)
                         DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                             self.dismiss(animated: true, completion: nil)
                             return
                         }
                     }
                     
                     self.defaults.setValue(json[0]["data"]["customer"][0]["name"].stringValue, forKey: "name")
                     self.defaults.setValue(json[0]["data"]["customer"][0]["mobile_number"].stringValue, forKey: "mobile_number")
                     self.defaults.set(true,forKey: "isLogin")
                    
                     let customer_id = json[0]["data"]["customer"][0]["customer_id"].stringValue;
                     let hashKey = json[0]["data"]["customer"][0]["hash"].stringValue;
                     let cart_summary = json[0]["data"]["customer"][0]["cart_summary"].intValue;
                     //saving value in NSUserDefault to use later on :: start
                     let email=self.userCred["email"]
                     let dict = ["email": email!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines), "customerId": customer_id, "hashKey": hashKey];
                     self.defaults.set(true, forKey: "isLogin")
                     self.defaults.set(dict, forKey: "userInfoDict");
                     self.defaults.set(cart_summary, forKey: "cart_summary");
                     self.defaults.setValue( json[0]["data"]["customer"][0]["gender"].stringValue, forKey: "gender")
                     self.defaults.synchronize()
                     
                     APP_DEL.RegisterFirebaseTopic()
                    
                    self.view.makeToast(APP_LBL().login_successful, duration: 1.5, position: .center, title: nil, image: nil, style: nil) { (true) in
                        
                        self.isForRegisterOTP = false
                        APP_DEL.changeLanguage()
                    }

                }
         //   }
        }
    }
    
    //MARK:- SEND OTP DATA
    var isForRegisterOTP = false
    func sendOtpData(data: [String : String], isForResend: Bool = false) {
        var postData = data


        postData["country_id"] =  APP_DEL.selectedCountry.country_code ?? "SA"
      //  postData["phonecode"] = "+" + (APP_DEL.selectedCountry.phone_code ?? "966")
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);
        
        API().callAPI(endPoint: "mobiconnect/customer/otpdata", method: .POST, param: postData) { (json, err) in
            
            //DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                if err == nil {
                    
                    if isForResend {
                        
                        self.view.makeToast(APP_LBL().otp_sent_success, duration: 1.0, position: .center)
                        
                    } else if self.isForRegisterOTP {
                      
                        
                    } else {
                        
                        self.view.makeToast(APP_LBL().login_successful, duration: 1.5, position: .center, title: nil, image: nil, style: nil) { (true) in

                            APP_DEL.changeLanguage()
                        }
                    }
                }
            //}
        }
    }
    
    //MARK:- VERIFY OTP
    func verifyOTP() {
        
        let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
        let hashKey = userInfoDict["hashKey"] ?? "";
        let customerId = userInfoDict["customerId"] ?? "";
        
        let otp = String(format: "%@%@%@%@%@%@",txtOne.text!,txtTwo.text!,txtThree.text!,txtFour.text!,txtFive.text!,txtSix.text!)
        
        var params = ["telephone": ("+\(APP_DEL.selectedCountry.phone_code ?? "966")" + self.txtPhoneNum.text!).getArToEnDigit(), "hashkey": hashKey,"customer_id" : customerId, "numberotp" : otp]
        
        if let storeId = defaults.value(forKey: "storeId") as? String {
            params["store_id"] = storeId
        }

        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);
        
        API().callAPI(endPoint: "mobiconnect/customer/verifyotp", method: .POST, param: params) { (json, err) in
            
          //  DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                if err == nil {
                    
                    if json[0]["code"] == 0 {
                        
                        self.view.makeToast(APP_LBL().please_enter_correct_otp, duration: 1.0, position: .center)
                        return
                    }
                    
                    if self.isOldUser {
                        
                        self.view.makeToast(APP_LBL().otp_verification_successful, duration: 2.0, position: .center)
                        
                        self.saveMobile(mobile: self.txtPhoneNum.text!.getArToEnDigit())
                        
                    } else {
                                                
                        
                        if self.isForRegisterOTP {
                        
                            let userInfoDict = self.defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
                            
                            
                            let DataLogin = ["METHOD" : "email",
                                             "EMAIL" : userInfoDict["email"] as Any,
                                             "PHONE_NUMBER" : userInfoDict["mobile_number"] as Any] as [String : Any]
                            let detail : [AnalyticKey:String] = [
                                .Email : userInfoDict["email"] ?? "",
                                .PhoneNumber : userInfoDict["mobile_number"] ?? ""
                            ]
                            API().setEvent(eventName: .SignUp, eventDetail: detail) { (json, err) in }
                            Analytics.logEvent(AnalyticsEventSignUp, parameters: DataLogin)
                            
                            
                            let email=self.userCred["email"]
                            let password=self.userCred["password"]
                            self.name = json["data"]["customer"][0]["name"].stringValue
                            
                            self.login(em: email!, passwd: password!)
                            
                        } else {
                            
                            self.view.makeToast(APP_LBL().login_successful, duration: 1.5, position: .center, title: nil, image: nil, style: nil) { (true) in
                                
                                APP_DEL.changeLanguage()
                            }
                        }
                    }
                }
          //  }
        }
    }
    
    //MARK:- SAVE MOBILE
    func saveMobile(mobile: String) {
        
        var postData = [String:String]()

        if (self.defaults.object(forKey: "userInfoDict") != nil) {
            
            let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
            postData["hashkey"] = userInfoDict["hashKey"]!
            postData["customer_id"] = userInfoDict["customerId"]!;
        }
        
        if let storeId = defaults.value(forKey: "storeId") as? String {
            postData["store_id"] = storeId
        }
        
        postData["mobile_numbers"] = mobile
        postData["phonecode"] = "+" + (APP_DEL.selectedCountry.phone_code ?? "966")
        
        
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);
        
        API().callAPI(endPoint: "mobiconnect/saveMobile", method: .POST, param: postData) { (json_res, err) in
            
          //  DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                if err == nil {
                    
                    self.defaults.setValue(mobile, forKey: "mobile_number")
                    self.defaults.set(true, forKey: "isLogin")
                    
                    APP_DEL.changeLanguage()
                }
          //  }
        }
    }
}

extension NejreeSignupVC : UITextFieldDelegate {
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
           if textField == txtOne {
               txtTwo.becomeFirstResponder()
           } else if textField == txtTwo {
               txtThree.becomeFirstResponder()
           } else if textField == txtThree {
               txtFour.becomeFirstResponder()
           } else if textField == txtFour {
               txtFive.becomeFirstResponder()
           }else if textField == txtFive {
               txtSix.becomeFirstResponder()
           }else {
               textField.resignFirstResponder()
           }
           return true
       }
    
    func textFieldDidEndEditing(_ textField: UITextField) {

         let str = textField.text!
     
         switch textField {
             
         case txtFirstName:
             print(str)
             if str != "" {
                 let decimalCharacters = CharacterSet.decimalDigits
                 
                 let decimalRange = str.rangeOfCharacter(from: decimalCharacters)
                 
                 if decimalRange != nil {
                      txtFirstName.textColor = nejreeColor
                     viewFirstName.layer.borderColor = UIColor.init(hexString: "#FFFFFF")?.cgColor
                     self.FirstNameVerifyImage.image = UIImage(named: "IconIncorrectCheck")
                 } else {
                     txtFirstName.textColor = UIColor.white
                     viewFirstName.layer.borderColor = nejreeColor?.cgColor
                     self.FirstNameVerifyImage.image = UIImage(named: "iconCorrectCheckMark")
                 }
             }else{
                 txtFirstName.textColor = nejreeColor
                 viewFirstName.layer.borderColor = UIColor.init(hexString: "#FFFFFF")?.cgColor
                 self.FirstNameVerifyImage.image = UIImage(named: "IconIncorrectCheck")
             }
             
            case txtLastName:
            print(str)
            if str != "" {
                let decimalCharacters = CharacterSet.decimalDigits
                
                let decimalRange = str.rangeOfCharacter(from: decimalCharacters)
                
                if decimalRange != nil {
                     txtLastName.textColor = nejreeColor
                    viewLastName.layer.borderColor = UIColor.init(hexString: "#FFFFFF")?.cgColor
                    self.LastNameVerifyImage.image = UIImage(named: "IconIncorrectCheck")
                }else {
                    txtLastName.textColor = UIColor.white
                    viewLastName.layer.borderColor = nejreeColor?.cgColor
                    self.LastNameVerifyImage.image = UIImage(named: "iconCorrectCheckMark")
                }
            }
            else
            {
                txtLastName.textColor = nejreeColor
                viewLastName.layer.borderColor = UIColor.init(hexString: "#FFFFFF")?.cgColor
                self.LastNameVerifyImage.image = UIImage(named: "IconIncorrectCheck")
            }

            
        
            
            
         case txtEmail:
             if isValidEmail(email: txtEmail.text!) {
                 txtEmail.textColor = UIColor.white
                 viewEmail.layer.borderColor = nejreeColor?.cgColor
                 self.emailVerifyImage.image = UIImage(named: "iconCorrectCheckMark")
             } else {

                 txtEmail.textColor = nejreeColor
                 viewEmail.layer.borderColor = UIColor.init(hexString: "#FFFFFF")?.cgColor
                 self.emailVerifyImage.image = UIImage(named: "IconIncorrectCheck")
             }
            
         case txtPassword :
             if str.count >= 6 {
                txtPassword.textColor = UIColor.white
                viewPassword.layer.borderColor = nejreeColor?.cgColor
                self.PasswordVerifyImage.image = UIImage(named: "iconCorrectCheckMark")
            }else{
                txtPassword.textColor = nejreeColor
                viewPassword.layer.borderColor = UIColor.init(hexString: "#FFFFFF")?.cgColor
                self.PasswordVerifyImage.image = UIImage(named: "IconIncorrectCheck")
            }
            
            
            case txtConfirmPassword :
                if str.count >= 6 {
                    txtConfirmPassword.textColor = UIColor.white
                    viewConfirmPassword.layer.borderColor = nejreeColor?.cgColor
                    self.ConfirmPasswordVerifyImage.image = UIImage(named: "iconCorrectCheckMark")
                }else{
                    txtConfirmPassword.textColor = nejreeColor
                    viewConfirmPassword.layer.borderColor = UIColor.init(hexString: "#FFFFFF")?.cgColor
                    self.ConfirmPasswordVerifyImage.image = UIImage(named: "IconIncorrectCheck")
                }
            
         case txtPhoneNum :
            print(str)
            if str.count != (Int(APP_DEL.selectedCountry.phone_number_limit ?? "") ?? 9) {
                txtPhoneNum.textColor = UIColor.white
                viewPhoneNum.layer.borderColor = nejreeColor?.cgColor
                self.PhoneNumVerifyImage.image = UIImage(named: "iconCorrectCheckMark")
            } else {
                txtPhoneNum.textColor = nejreeColor
                viewPhoneNum.layer.borderColor = UIColor.init(hexString: "#FFFFFF")?.cgColor
                self.PhoneNumVerifyImage.image = UIImage(named: "IconIncorrectCheck")
            }
            
            
            
         default:
            print(str)
            if str.count == 1 {
                textField.textColor = UIColor.white
                textField.layer.borderColor = nejreeColor?.cgColor
            } else {
                textField.textColor = nejreeColor
                textField.layer.borderColor = UIColor.init(hexString: "#FFFFFF")?.cgColor
            }

         }

        if txtFirstName.text != "" && txtLastName.text != "" && txtEmail.text != "" && txtPhoneNum.text != "" && txtPassword.text != "" && txtConfirmPassword.text != "" {
            btnSignUp.titleLabel?.textColor = UIColor.black
            btnSignUp.backgroundColor = UIColor(red: 251/255, green: 173/255, blue: 24/255, alpha: 1.0)
            viewOne.layer.borderColor = UIColor(red: 251/255, green: 173/255, blue: 24/255, alpha: 1.0).cgColor
            lblOne.textColor = UIColor(red: 251/255, green: 173/255, blue: 24/255, alpha: 1.0)
            lblLine.backgroundColor = UIColor(red: 251/255, green: 173/255, blue: 24/255, alpha: 1.0)
         }else{
             btnSignUp.titleLabel?.textColor = UIColor.white
             btnSignUp.backgroundColor = UIColor.clear
             viewOne.layer.borderColor = UIColor.white.cgColor
             lblOne.textColor = UIColor.white
             lblLine.backgroundColor = UIColor.white
         }
        
        
        
        
        
        if txtOne.text == "" || txtTwo.text  == "" || txtThree.text  == "" || txtFour.text  == "" || txtFive.text  == "" || txtSix.text  == "" {
            btnContinue.titleLabel?.textColor = UIColor.white
            btnContinue.backgroundColor = UIColor.clear
        }else{
            if str == " " || str == "" {
                btnContinue.titleLabel?.textColor = UIColor.white
                btnContinue.backgroundColor = UIColor.clear
                lblTwo.textColor = UIColor.white
                viewTwo.layer.borderColor = UIColor.white.cgColor
            }else{
                btnContinue.titleLabel?.textColor = UIColor.black
                btnContinue.backgroundColor = UIColor(red: 251/255, green: 173/255, blue: 24/255, alpha: 1.0)
                lblTwo.textColor =  UIColor(red: 251/255, green: 173/255, blue: 24/255, alpha: 1.0)
                viewTwo.layer.borderColor = UIColor(red: 251/255, green: 173/255, blue: 24/255, alpha: 1.0).cgColor
            }
        }
    
        
     }

     
     func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
         
        let validString : String = "0123456789٠١٢٣٤٥٦٧٨٩"
        
        if  string.isEmpty == false && validString.contains(string) == false  && textField != txtLastName && textField != txtFirstName && textField != txtEmail && textField != txtPassword && textField != txtConfirmPassword && textField != txtPhoneNum {
            return false
        }
        
        if textField.text == ""{
            textField.text = ""
        }
        if textField == txtOne {
            if !(string == "") {
                textField.text = string
                txtTwo.becomeFirstResponder()
                //  txtSecond.text = " "
                return false
            }
        }else if textField == txtTwo {
            if (string == "") {
                textField.text = ""
                txtOne.becomeFirstResponder()
                return false
            } else {
                textField.text = string
                textField.resignFirstResponder()
                txtThree.becomeFirstResponder()
                //  txtThird.text = " "
                return false
            }
        }else if textField == txtThree {
            if (string == "") {
                
                textField.text = ""
                txtTwo.becomeFirstResponder()
                return false
            } else {
                textField.text = string
                txtFour.becomeFirstResponder()
                // txtFour.text = " "
                return false
            }
        } else if textField == txtFour {
            if (string == "") {
                
                textField.text = ""
                txtThree.becomeFirstResponder()
                return false
            } else {
                textField.text = string
                txtFive.becomeFirstResponder()
                // txtFour.text = " "
                return false
            }
        }else if textField == txtFive {
            if (string == "") {
                
                textField.text = ""
                txtFour.becomeFirstResponder()
                return false
            } else {
                textField.text = string
                txtSix.becomeFirstResponder()
                // txtFour.text = " "
                return false
            }
        }else if textField == txtSix {
            if (string == "") {
                textField.text = ""
                txtFive.becomeFirstResponder()
                return false
            } else {
                
                textField.text = string
                textField.resignFirstResponder()
                return false
            }
        }
        
        
         if (textField == txtFirstName){
             
             if txtFirstName.text!.count > 51 {
                 
                 return false
             }
         }else if (textField == txtLastName){
            
            if txtLastName.text!.count > 51 {
                return false
            }
            
         } else if (textField == txtPhoneNum){
             if (CharacterSet(charactersIn: "0123456789٠١٢٣٤٥٦٧٨٩").isSuperset(of: CharacterSet(charactersIn: string))) {

                 if let text = textField.text, let range = Range(range, in: text) {

                     let proposedText = text.replacingCharacters(in: range, with: string)

                    if (APP_DEL.selectedCountry.country_code?.uppercased() == "SA"){
                        if proposedText.first == "5" && proposedText.count <= (Int(APP_DEL.selectedCountry.phone_number_limit ?? "") ?? 9) {
                            return true
                        }
                    } else {
                        if proposedText.count <= (Int(APP_DEL.selectedCountry.phone_number_limit ?? "") ?? 9) {
                            return true
                        }
                    }
                 }
             }

             return false
         }
         
         return true;
     }
     
     
    
}
