//
//  verifyOTP.swift
//  MageNative Magento Platinum
//
//  Created by MacMini on 16/07/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class verifyOTP: UIView {

    
    @IBOutlet var myView: UIView!
    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var OTPtextField: SkyFloatingLabelTextField!
    @IBOutlet weak var verifyButton: UIButton!
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        
        Bundle.main.loadNibNamed("verifyOTP", owner: self, options: nil)
        addSubview(myView)
        myView.frame = self.bounds
        myView.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        OTPtextField.keyboardType = .asciiCapableNumberPad
    }
    
    
}
