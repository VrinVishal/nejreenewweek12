//
//  registerButtonCell.swift
//  MageNative Magento Platinum
//
//  Created by MacMini on 19/07/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class registerButtonCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        lblAccept.font = UIFont(name: "Cairo-Regular", size: 15)!
        lblAccept.text = APP_LBL().accept_terms_condition + " "
        
        lblTermsAndCondition.font = UIFont(name: "Cairo-Regular", size: 15)!
        lblTermsAndCondition.text = APP_LBL().terms_amp_condition.uppercased()
    }

    
    @IBOutlet weak var registerButton: UIButton!
    
    @IBOutlet weak var btnIAccepts: UIButton!
    @IBOutlet weak var btnTermsAndCondition: UIButton!
    
    @IBOutlet weak var imgTermsCondi: UIImageView!
    
    @IBOutlet weak var lblAccept: UILabel!
    @IBOutlet weak var lblTermsAndCondition: UILabel!
   
}
