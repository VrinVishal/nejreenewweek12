//
//  nejreeRegisterController.swift
//  MageNative Magento Platinum
//
//  Created by MacMini on 15/07/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseAnalytics



class nejreeRegisterController: MagenativeUIViewController,UIGestureRecognizerDelegate {
    
    let upView = UIView()
    let otpView = verifyOTP()
    var isVerified = false
    var userCred = [String:String]()
    var name = ""
    var isCheckout = false
    var total = [String:String]()
    var tapGesture = UITapGestureRecognizer()
    var subscribed = 0
    var dataToPost = [String: String]() // param to post on register api
    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var registerTable: UITableView!
    @IBOutlet weak var dismissButton: UIButton!
    //var total = [String:String]()
    var cartProducts = [[String:String]]()
    var email = String()
    var isFromCheckOut = Bool()
    var mainvc: UIViewController?
    var countryCode = APP_DEL.selectedCountry.phone_code
    
    var isTermsSelected = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        registerTable.delegate = self
        registerTable.dataSource = self
        registerTable.separatorStyle = .none
        registerTable.alwaysBounceVertical = false
        tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissPopup(_:)))
        tapGesture.delegate = self
        
        self.navigationController?.navigationBar.isHidden = true
        
      
       
        
        dismissButton.addTarget(self, action: #selector(dismissBttonTapped(_:)), for: .touchUpInside)
        // Do any additional setup after loading the view.
        
        if APP_DEL.selectedLanguage == Arabic{
            
            dismissButton.setImage(UIImage(named: "BackArrowNewArabic"), for: .normal)
            
        } else {
          
            dismissButton.setImage(UIImage(named: "BackArrowNew"), for: .normal)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
     
    }
    
    
    @objc func dismissBttonTapped(_ sneder: UIButton) {
        
        if isModal() {
            self.dismiss(animated: true, completion: nil)
        } else {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    
    override func recieveResponse(data: Data?, requestUrl: String?, response: URLResponse?) {
        guard let data = data else {return}
        let json = try!JSON(data: data)
        
        print(json)
        
        if requestUrl != "mobiconnect/customer/login" {
            let status = json[0]["data"]["customer"][0]["status"].stringValue;
            if(status == "success")
            {
                let isConfirm = json[0]["data"]["customer"][0]["isConfirmationRequired"].stringValue
                print(isConfirm)
                if(isConfirm == "NO"){
                    let email=userCred["email"]
                    let password=userCred["password"]
                    name = json[0]["data"]["customer"][0]["name"].stringValue
                    self.loginFunction(email: email!, password: password!)
                }else{
                    
                    cedMageHttpException.showAlertView(me: self, msg: APP_LBL().registration_successful, title: APP_LBL().success)
                    cedMage.delay(delay: 2, closure: {
                        _ = self.navigationController?.popToRootViewController(animated: true)
                    })
                }
                return;
            }else{
                let title = APP_LBL().error
                let errorMsg = json[0]["data"]["customer"][0]["message"].string;
                cedMageHttpException.showAlertView(me: self, msg: errorMsg, title: title);
                return
            }
        } else {
            defaults.setValue(name, forKey: "name")
            defaults.set(true,forKey: "isLogin")
            let customer_id = json[0]["data"]["customer"][0]["customer_id"].stringValue;
            let hashKey = json[0]["data"]["customer"][0]["hash"].stringValue;
            let cart_summary = json[0]["data"]["customer"][0]["cart_summary"].intValue;
            //saving value in NSUserDefault to use later on :: start
            let email=userCred["email"]
            
            UserDefaults.standard.set(email, forKey: "EmailId")
            self.defaults.set(email, forKey: "EmailId")
            
            let dict = ["email": email!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines), "customerId": customer_id, "hashKey": hashKey];
            self.defaults.set(true, forKey: "isLogin")
            self.defaults.set(dict, forKey: "userInfoDict");
            self.defaults.set(cart_summary, forKey: "cart_summary");
            self.defaults.setValue( json[0]["data"]["customer"][0]["gender"].stringValue, forKey: "gender")
            self.view.makeToast(APP_LBL().login_successful, duration: 1.5, position: .center, title: nil, image: nil, style: nil) { (true) in
               
                    
                
            }
            
        }
        
    }
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool{
        if ((touch.view?.isDescendant(of: upView))!) {
            return false
        }else{
            return true
        }
    }
    
    @objc func dismissPopup(_ sender: UITapGestureRecognizer) {
        self.upView.removeFromSuperview()
    }
    
    override func parseResponseReceivedFromAPI(_ jsonResponse: JSON) {
        print(jsonResponse)
        
        if jsonResponse[0]["data"]["customer"][0]["status"].stringValue == "true" {
            // already exist
            let upview = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height))
            upview.backgroundColor = UIColor.black.withAlphaComponent(0.5)
            upview.tag = 151
            self.view.addSubview(upview)
            let popup = nejreeExistingUser()
            popup.tag = 152
            self.view.addSubview(popup)
            popup.translatesAutoresizingMaskIntoConstraints = false
            popup.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 20).isActive = true
            popup.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -20).isActive = true
            popup.centerYAnchor.constraint(equalTo: self.view.centerYAnchor, constant: 0).isActive = true
            popup.centerXAnchor.constraint(equalTo: self.view.centerXAnchor, constant: 0).isActive = true
            popup.heightAnchor.constraint(equalToConstant: 200).isActive = true
            popup.clipsToBounds = true
            popup.layer.cornerRadius = 8.0
            popup.signInButton.roundCorners()
            popup.cancelButton.roundCorners()
            popup.cancelButton.addTarget(self, action: #selector(popupCancelTapped(_:)), for: .touchUpInside)
            popup.signInButton.addTarget(self, action: #selector(popupsignInTapped(_:)), for: .touchUpInside)
            popup.statementLabel.text = APP_LBL().looks_like_an_account_with_the_entered_email_already_exists
            popup.noticeLabel.text = APP_LBL().notice
            popup.signInButton.setTitle(APP_LBL().sign_in_c, for: .normal)
            popup.cancelButton.setTitle(APP_LBL().cancel, for: .normal)
            
            return
        } else {
            self.sendOTPNew(with: dataToPost)
        }
        
    }
    
    
    @objc func popupCancelTapped(_ sender: UIButton) {
        self.view.viewWithTag(151)?.removeFromSuperview()
        self.view.viewWithTag(152)?.removeFromSuperview()
    }
    @objc func popupsignInTapped(_ sender: UIButton) {
        self.view.viewWithTag(151)?.removeFromSuperview()
        self.view.viewWithTag(152)?.removeFromSuperview()
        let vc = UIStoryboard(name: "cedMageLogin", bundle: nil).instantiateViewController(withIdentifier: "nejreeLoginController") as! nejreeLoginController
        self.navigationController?.pushViewController(vc, animated: true)
        //vc.modalPresentationStyle = .fullScreen
        //self.present(vc, animated: true, completion: nil)
    }
    
    
    @objc func btnAcceptAction(_ sender: UIButton) {
        
        isTermsSelected = !isTermsSelected
        
        self.registerTable.reloadSections([2], with: .none)
    }
    
    @objc func btnTermsAndConditionAction(_ sender: UIButton) {
        
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "termsMenuController") as! termsMenuController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
}


extension nejreeRegisterController: UITableViewDataSource,UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch section {
        case 0:
            return 1
        case 1,2,3:
            return 1
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        case 0:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "registerImageCell", for: indexPath) as! registerImageCell
            cell.backgroundColor = .black
            cell.selectionStyle = .none
            return cell
            
            
            
          
        case 1:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "textFieldCell", for: indexPath) as! textFieldCell
                      
                      cell.validationimage.image = UIImage(named: "")
                      cell.emailValidImage.image = UIImage(named: "")
                      cell.passwordValidImage.image = UIImage(named: "")
                      cell.cPasswordValidImage.image = UIImage(named: "")
                      cell.mobileValidImage.image = UIImage(named: "")
                      cell.lNameValidImage.image = UIImage(named: "")
                      
                      cell.sideView.backgroundColor = .black//isHidden = true
                      cell.emailSideView.backgroundColor = .black//isHidden = true
                      cell.passwordSideView.backgroundColor = .black//isHidden = true
                      cell.cPasswordSideView.backgroundColor = .black//isHidden = true
                      
                    if APP_DEL.selectedLanguage == Arabic{
                
                          cell.rightMobileView.isHidden = true
                          cell.leftMobileView.isHidden = false
                          cell.leftMobileSideView.backgroundColor = .black
                          
                      } else {
                          //cell.rightMobileView.isHidden = true
                          cell.rightMobileView.isHidden = true
                          cell.leftMobileView.isHidden = false
                          cell.leftMobileSideView.backgroundColor = .black//isHidden = true
                      }
                      cell.lNameSideView.backgroundColor = .black
                                  
                      
                      cell.name.placeholder = APP_LBL().first_name
                      cell.lNameTectField.placeholder = APP_LBL().lastname
                      cell.email.placeholder = APP_LBL().email_star.uppercased()
                      cell.mobileNumber.placeholder = APP_LBL().phone_star
                      cell.password.placeholder = APP_LBL().password_star.uppercased()
                      cell.confirmPassword.placeholder = APP_LBL().confirm_password_star
                      cell.password.isSecureTextEntry = true
                      cell.confirmPassword.isSecureTextEntry = true
                      
                      cell.topLabel.text = APP_LBL().sign_up
                      
                        cell.mobileNumber.keyboardType = .asciiCapableNumberPad
            
                      //cell.countryCode.text = "+966".localized
                     // cell.countryCode.isEnabled = false
                      
                      cell.nameView.roundCorners()//layer.cornerRadius = 5
                      cell.lastNameView.roundCorners()
                      cell.emailView.roundCorners()//layer.cornerRadius = 5
                      cell.mobileNumberView.roundCorners()//layer.cornerRadius = 5
                     // cell.countryCodeView.roundCorners()//layer.cornerRadius = 5
                      cell.passwordView.roundCorners()//layer.cornerRadius = 5
                      cell.confirmPasswordView.roundCorners()//layer.cornerRadius = 5
                      
                      cell.nameView.setBorder()
                      cell.lastNameView.setBorder()
                      cell.emailView.setBorder()
                      cell.passwordView.setBorder()
                      cell.confirmPasswordView.setBorder()
                      cell.mobileNumberView.setBorder()
                     // cell.countryCodeView.setBorder()
                      
                      cell.nameImage.roundImageAndBorderColor()
                      cell.lNameImage.roundImageAndBorderColor()
                      cell.emailImage.roundImageAndBorderColor()
                      
                    if APP_DEL.selectedLanguage == Arabic{
                          //cell.rightPhoneImage.roundImageAndBorderColor()
                          cell.leftPhoneImage.roundImageAndBorderColor()
                      } else {
                          cell.leftPhoneImage.roundImageAndBorderColor()
                      }
                      
                      //cell.phoneImage.roundImageAndBorderColor()
                      cell.passwordImage.roundImageAndBorderColor()
                      cell.cPasswordImage.roundImageAndBorderColor()
                     
                      cell.name.delegate = self
                      cell.lNameTectField.delegate = self
                      cell.email.delegate = self
                      cell.mobileNumber.delegate = self
                      cell.password.delegate = self
                      cell.confirmPassword.delegate = self
                      
                      
                      ////cell.phoneImage.semanticContentAttribute = .forceLeftToRight
                      cell.mobileNumberView.semanticContentAttribute = .forceLeftToRight
                      cell.mobileNumberCenterView.semanticContentAttribute = .forceLeftToRight
                      
                      cell.lblCountryCode.semanticContentAttribute = .forceLeftToRight
                      
                      cell.rightPhoneImage.semanticContentAttribute = .forceLeftToRight
                      cell.leftPhoneImage.semanticContentAttribute = .forceLeftToRight
             
                    if APP_DEL.selectedLanguage == Arabic{
                          cell.name.isLTRLanguage = false
                          cell.lNameTectField.isLTRLanguage = false
                          cell.email.isLTRLanguage = false
                          
                          cell.mobileNumber.isLTRLanguage = true
                          
                          cell.password.isLTRLanguage = false
                          cell.confirmPassword.isLTRLanguage = false
                      } else {
                          
                          cell.name.isLTRLanguage = true
                          cell.lNameTectField.isLTRLanguage = true
                          cell.email.isLTRLanguage = true
                          
                          cell.mobileNumber.isLTRLanguage = true
                          
                          cell.password.isLTRLanguage = true
                          cell.confirmPassword.isLTRLanguage = true
                      }
                      cell.selectionStyle = .none
                      return cell
            
            
            
            
            
            
            
          
        case 2:
            
            
                      let cell = tableView.dequeueReusableCell(withIdentifier: "registerButtonCell", for: indexPath) as! registerButtonCell
                      cell.registerButton.addTarget(self, action: #selector(registerButton(_:)), for: .touchUpInside)
                      
                      cell.btnIAccepts.addTarget(self, action: #selector(self.btnAcceptAction(_:)), for: .touchUpInside)
                      cell.btnTermsAndCondition.addTarget(self, action: #selector(self.btnTermsAndConditionAction(_:)), for: .touchUpInside)
                      
                      if isTermsSelected {
                            cell.imgTermsCondi.image = UIImage(named: "icon_checkbox_white_sel")
                      } else {
                            cell.imgTermsCondi.image = UIImage(named: "icon_checkbox_white_un")
                      }
                      
                      cell.registerButton.setTitle(APP_LBL().sign_up, for: .normal)
                      cell.registerButton.backgroundColor = .black
                      cell.registerButton.layer.borderColor = UIColor.lightGray.cgColor
                      cell.registerButton.layer.borderWidth = 1.0
                      cell.registerButton.roundCorners()//layer.cornerRadius = 10.0
                      cell.selectionStyle = .none
                      return cell
            
            
           
        case 3:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "loginRedirectCell", for: indexPath) as! loginRedirectCell
                       cell.gotoLogin.setTitle(APP_LBL().sign_in_instead, for: .normal)
                       cell.gotoLogin.setTitleColor(UIColor.init(hexString: "#FBAD18"), for: .normal)
                       cell.gotoLogin.addTarget(self, action: #selector(gotoLoginTapped(_:)), for: .touchUpInside)
                       cell.gotoLogin.backgroundColor = UIColor.clear
                       cell.selectionStyle = .none
                       return cell
            
        default:
            return UITableViewCell()
        }
        
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
//        switch indexPath.section {
//        case 0:
//            return 450
//        case 1:
//            return 55
//        case 2:
//            return 50
//        case 3:
//            return 200
//        default:
//            return 0
//        }
        
        switch indexPath.section {
        case 0:
            return 182
        case 1:
            return 450
        case 2:
            return 55 + 35
        case 3:
            return 50
        default:
            return 0
        }
        
    }
    
    
    @objc func gotoLoginTapped(_ sender: UIButton) {
        
        let vc = UIStoryboard(name: "cedMageLogin", bundle: nil).instantiateViewController(withIdentifier: "nejreeLoginController") as! nejreeLoginController
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true) 
        //self.navigationController?.pushViewController(vc, animated: true)
        //self.dismiss(animated: true, completion: nil)
    }
    

    
    
    
    
    @objc func registerButton(_ sender: UIButton) {
                
        let ip = IndexPath(row: 0, section: 1)
        let cell = registerTable.cellForRow(at: ip) as! textFieldCell
        let fName = cell.name.text
        let lName = cell.lNameTectField.text
        let mobNumber = cell.mobileNumber.text
        let email = cell.email.text
        let pwd = cell.password.text
        let cPwd = cell.confirmPassword.text
        /* let ip2 = IndexPath(row: 0, section: 1)
         let cell2 = registerTable.cellForRow(at: ip2) as! mobileOTPVerificationCell
         let mobNumber = cell2.mobileTextField.text */
        //        print(mobNumber)
        let validEmail = EmailVerifier.isValidEmail(testStr: email!)
        
       
        
        
//        if fName == "" || lName == "" || email == "" || pwd == "" || cPwd == "" || mobNumber == ""
//        {
//            self.view.makeToast("All fields are required".localized, duration: 2.0, position: .center);
//            return
//
//        }
        
        self.view.endEditing(true)
        
        
        if fName == ""{
            
            cedMageHttpException.showAlertView(me: self, msg: APP_LBL().please_enter_first_name, title: APP_LBL().error)
            return;
        }
        else if lName == ""{
            cedMageHttpException.showAlertView(me: self, msg: APP_LBL().please_enter_last_name, title: APP_LBL().error)
            return;
        }
        else if email == ""{
            cedMageHttpException.showAlertView(me: self, msg: APP_LBL().please_enter_email, title: APP_LBL().error)
            return;
        }
        else if !validEmail {
             cedMageHttpException.showAlertView(me: self, msg: APP_LBL().please_enter_valid_email, title: APP_LBL().error)
            return;
        }
        else if mobNumber == ""{
            cedMageHttpException.showAlertView(me: self, msg: APP_LBL().please_enter_phone_no, title: APP_LBL().error)
            return;
        }
        else if (mobNumber?.count)! < 6 {
            
            cedMageHttpException.showAlertView(me: self, msg: APP_LBL().please_enter_valid_phone_no, title: APP_LBL().error)
            return;
        }
        else if pwd == ""{
            cedMageHttpException.showAlertView(me: self, msg: APP_LBL().please_enter_password, title: APP_LBL().error)
            return;
        }
        else if (pwd?.count)! < 6 {
            
            cedMageHttpException.showAlertView(me: self, msg: APP_LBL().password_must_contain_more_than_6_digit, title: APP_LBL().error)
            return;
        }
        else if cPwd == ""{
            cedMageHttpException.showAlertView(me: self, msg: APP_LBL().please_enter_cnfrm_password, title: APP_LBL().error)
            return;
        }
        else if pwd != cPwd {
            cedMageHttpException.showAlertView(me: self, msg: APP_LBL().password_and_confirm_password_doesnt_match, title: APP_LBL().error)
            return;
        }
        else if self.isTermsSelected == false {
            cedMageHttpException.showAlertView(me: self, msg: APP_LBL().please_select_terms_and_conditions, title: APP_LBL().error)
            return;
            }
            //        else if !isVerified {self.view.makeToast("Phone number not verified".localized, duration: 2.0, position: .center); return}
        else {
            //self.sendRequest(url: "mobiconnect/customer/verifyEmail", params: ["email":email!])
            self.makeRequestToAPI("mobiconnect/customer/verifyEmail", dataToPost: ["email":email!])
            
            
            
            

            //cedMageLoaders.addDefaultLoader(me: self)
            dataToPost = ["firstname":fName! ,"lastname":lName!,"email":email! ,"password":pwd! ,"is_subscribed":"\(subscribed)","gender":"","dob": "" ,"taxvat":"","prefix":"","suffix":"","middlename":"","mobile_numbers":mobNumber!];

            userCred["email"] = email
            userCred["password"] = pwd
//
//            sendOTPNew(with: postString as! [String : String])
//

        }
        
        
    }
    
    
    
    func sendOTPNew(with: [String:String]) {
        let ip = IndexPath(row: 0, section: 1)
        let cell = registerTable.cellForRow(at: ip) as! textFieldCell
        let mobileNumber = cell.mobileNumber.text!
        let countryCode = self.countryCode//cell.countryCode.text!
        let mobileWithCode = countryCode ?? "+966" + mobileNumber
        
        if mobileNumber == "" {self.view.makeToast(APP_LBL().please_enter_mobile_number, duration: 2.0, position: .center); return}
       // if mobileNumber.count > 10 {cedMageLoaders.removeLoadingIndicator(me: self);self.view.makeToast(APP_LBL().mobile_number_should_be_of_xyz_digits_only, duration: 2.0, position: .center); return}
        if (mobileNumber.count != (Int(APP_DEL.selectedCountry.phone_number_limit ?? "") ?? 9)) {cedMageLoaders.removeLoadingIndicator(me: self);self.view.makeToast(APP_LBL().mobile_number_should_be_of_xyz_digits_only, duration: 2.0, position: .center); return}
    
         cedMageLoaders.removeLoadingIndicator(me: self)
        let vc = UIStoryboard(name: "cedMageLogin", bundle: nil).instantiateViewController(withIdentifier: "nejreeOTPController") as! nejreeOTPController
        vc.postString = with
        vc.userCred = self.userCred
        vc.mainvc = self.mainvc;
        vc.isCheckout = self.isCheckout
        vc.total = self.total
        vc.mobileNumber = mobileNumber
        cedMageLoaders.removeLoadingIndicator(me: self)
        //vc.modalPresentationStyle = .fullScreen
        //self.present(vc, animated: true)
        self.navigationController?.pushViewController(vc, animated: true)
        
        
//        PhoneAuthProvider.provider().verifyPhoneNumber(mobileWithCode, uiDelegate: nil) { (secretKey, error) in
//
//            if error == nil {
//                print("secretKey \(secretKey)")
//                guard let verificationKey = secretKey else {return}
//                self.defaults.set(verificationKey, forKey: "mobileVerificationId")
//                self.defaults.synchronize()
//
//                let vc = UIStoryboard(name: "cedMageLogin", bundle: nil).instantiateViewController(withIdentifier: "nejreeOTPController") as! nejreeOTPController
//                vc.postString = with
//                vc.userCred = self.userCred
//                vc.mainvc = self.mainvc;
//                vc.isCheckout = self.isCheckout
//                vc.total = self.total
//                vc.mobileNumber = mobileNumber
//                cedMageLoaders.removeLoadingIndicator(me: self)
//                vc.modalPresentationStyle = .fullScreen
//                self.present(vc, animated: true)
//
//            }else {
//                cedMageLoaders.removeLoadingIndicator(me: self)
//                print("error is \(error?.localizedDescription)")
//                cedMageHttpException.showAlertView(me: self, msg: error?.localizedDescription ?? "", title: "Error")
//            }
//
//
//        }
    }
    
    
    @objc func textDidChanges(_ sender: UITextField) {
      //  fetcher?.sourceTextHasChanged(sender.text)
    }
    
    
   
    func didFailAutocompleteWithError(_ error: Error) {
        print("error in fetching nearby cities with \(error.localizedDescription)")
    }
    
    
 /*   @objc func sendOTPtapped(_ sender: UIButton) {
        
        let ip = IndexPath(row: 0, section: 1)
        let cell = registerTable.cellForRow(at: ip) as! mobileOTPVerificationCell
        let mobileNumber = cell.mobileTextField.text!
        let countryCode = "+966"//cell.countryCodeField.text!
        let mobileWithCode = "+966" + mobileNumber
        
        if mobileNumber == "" {self.view.makeToast("Please enter mobile number".localized, duration: 2.0, position: .center); return}
        if mobileNumber.count > 10 {self.view.makeToast("Mobile number should be of 9 digits only".localized, duration: 2.0, position: .center); return}
        if mobileNumber.count < 9 {self.view.makeToast("Mobile number should be of 9-10 digits only".localized, duration: 2.0, position: .center); return}
        //        else if mobileNumber.count > 13 {self.view.makeToast("Mobile number should be of 10 digits", duration: 2.0, position: .center); return}
        PhoneAuthProvider.provider().verifyPhoneNumber(mobileWithCode, uiDelegate: nil) { (secretKey, error) in
            
            if error == nil {
                print("secretKey \(secretKey)")
                guard let verificationKey = secretKey else {return}
                self.defaults.set(verificationKey, forKey: "mobileVerificationId")
                self.defaults.synchronize()
                
                self.upView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
                self.upView.backgroundColor = UIColor.black.withAlphaComponent(0.8)
                self.view.addSubview(self.upView)
                
                self.otpView.frame = CGRect(x: 0, y: 0, width: self.upView.frame.width - 50, height: 150)
                self.otpView.center = self.upView.center
                self.otpView.verifyButton.addTarget(self, action: #selector(self.verifyTapped(_:)), for: .touchUpInside)
                self.otpView.verifyButton.layer.cornerRadius = 8.0
                self.otpView.verifyButton.backgroundColor = UIColor.init(hexString: "#FBAD18")
                self.otpView.layer.cornerRadius = 10.0
                if #available(iOS 12.0, *) {
                    self.otpView.OTPtextField.textContentType = .oneTimeCode
                } else {
                    // Fallback on earlier versions
                }
                self.otpView.OTPtextField.placeholder = "Enter Your OTP".localized
                self.otpView.topLabel.text = "Verify OTP".localized
                self.otpView.verifyButton.setTitle("Verify".localized, for: .normal)
                self.upView.addSubview(self.otpView)
                
                self.upView.addGestureRecognizer(self.tapGesture)
                
                cell.sendOtpBtn.setTitle("Verified".localized, for: .normal)
                cell.sendOtpBtn.isEnabled = false
            }else {
                
                print("error is \(error?.localizedDescription)")
            }
            
            
        }
        
    } */
    
    
    @objc func verifyTapped(_ sender: UIButton) {
        
        let otp = otpView.OTPtextField.text!
        
        let veriId = defaults.value(forKey: "mobileVerificationId") as! String
        
        let cred = PhoneAuthProvider.provider().credential(withVerificationID: veriId , verificationCode: otp)
        
        Auth.auth().signIn(with: cred) { (success, error) in
            if error == nil {
                print("yuss successs \(success)")
                self.view.makeToast(APP_LBL().otp_verification_successful, duration: 2.0, position: .center)
                self.isVerified = true
                self.upView.removeFromSuperview()
            } else {
                print("error is \(error?.localizedDescription)")
                self.view.makeToast((error?.localizedDescription)!, duration: 3.0, position: .center)
                self.isVerified = false
            }
        }
        
    }
    
    func loginFunction(email:String,password:String){
        var email = email;
        email = email.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        
        var password = password;
        password = password.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        
        var postString = ["email":email,"password":password];
        if(defaults.object(forKey: "cartId") != nil)
        {
            let cart_id = defaults.object(forKey: "cartId") as? String;
            //postString += "&cart_id="+cart_id!;
            postString.updateValue(cart_id!, forKey: "cart_id")
        }
        self.sendRequest(url: "mobiconnect/customer/login", params: postString)
    }
    
}



extension nejreeRegisterController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let indexPath = IndexPath(row: 0, section: 1)
        let cell = registerTable.cellForRow(at: indexPath) as! textFieldCell
        
        if cell.mobileNumber == textField {
            
            guard !string.isEmpty else {

                return true
            }
            
            if (CharacterSet(charactersIn: "0123456789").isSuperset(of: CharacterSet(charactersIn: string))) {

                if let text = textField.text, let range = Range(range, in: text) {

                    let proposedText = text.replacingCharacters(in: range, with: string)

                    if (APP_DEL.selectedCountry.country_code?.uppercased() == "SA"){
                        if proposedText.first == "5" && proposedText.count <= (Int(APP_DEL.selectedCountry.phone_number_limit ?? "") ?? 9) {
                            return true
                        }
                    } else {
                        if proposedText.count <= (Int(APP_DEL.selectedCountry.phone_number_limit ?? "") ?? 9) {
                            return true
                        }
                    }
                }
            }

            return false
            
        } else if cell.name == textField || cell.lNameTectField == textField {
         
            if let x = string.rangeOfCharacter(from: NSCharacterSet.decimalDigits) {
               return false
            } else {
            return true
            }
        }
        
        
        return true;
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        let indexPath = IndexPath(row: 0, section: 1)
        let cell = registerTable.cellForRow(at: indexPath) as! textFieldCell
        let str = textField.text!
       
        switch textField {
            
        case cell.name:
            
            print(str)
            if str != "" {
                let decimalCharacters = CharacterSet.decimalDigits
                
                let decimalRange = str.rangeOfCharacter(from: decimalCharacters)
                
                if decimalRange != nil {
                    cell.validationimage.image = UIImage(named: "wrongname")
                    cell.nameView.layer.borderWidth = 1
                    cell.nameView.layer.borderColor = UIColor.init(hexString: "#741A31")!.cgColor
                    cell.name.titleColor = UIColor.init(hexString: "#741A31")!
                    cell.sideView.backgroundColor = UIColor.black
                    cell.nameImage.tintColor = UIColor.init(hexString: "#741A31")
                }else {
                    cell.validationimage.image = UIImage(named: "verified")
                    cell.name.titleColor = .white
                    cell.sideView.backgroundColor = UIColor.init(hexString: "#fcb215")
                    cell.sideView.isHidden = false
                    cell.nameImage.tintColor = UIColor.init(hexString: "#FBAD18")
                    cell.nameView.layer.borderColor = nejreeColor?.cgColor
                }
            } else {
                cell.validationimage.image = UIImage(named: "wrongname")
                cell.nameView.layer.borderWidth = 1
                cell.nameView.layer.borderColor = UIColor.init(hexString: "#741A31")!.cgColor
                cell.name.titleColor = UIColor.init(hexString: "#741A31")!
                cell.sideView.backgroundColor = UIColor.black
                cell.nameImage.tintColor = UIColor.init(hexString: "#741A31")
                return
            }
        case cell.lNameTectField:
            print(str)
            if str != "" {
                let decimalCharacters = CharacterSet.decimalDigits
                
                let decimalRange = str.rangeOfCharacter(from: decimalCharacters)
                
                if decimalRange != nil {
                    cell.lNameValidImage.image = UIImage(named: "wrongname")
                    cell.lastNameView.layer.borderWidth = 1
                    cell.lastNameView.layer.borderColor = UIColor.init(hexString: "#741A31")!.cgColor
                    cell.lNameTectField.titleColor = UIColor.init(hexString: "#741A31")!
                    cell.lNameSideView.backgroundColor = UIColor.black
                    cell.lNameImage.tintColor = UIColor.init(hexString: "#741A31")
                }else {
                    cell.lNameValidImage.image = UIImage(named: "verified")
                    cell.lNameTectField.titleColor = .white
                    cell.lNameSideView.backgroundColor = UIColor.init(hexString: "#fcb215")
                    cell.lNameSideView.isHidden = false
                    cell.lNameImage.tintColor = UIColor.init(hexString: "#FBAD18")
                    cell.lastNameView.layer.borderColor = nejreeColor?.cgColor
                }
            } else {
                cell.lNameValidImage.image = UIImage(named: "wrongname")
                cell.lastNameView.layer.borderWidth = 1
                cell.lastNameView.layer.borderColor = UIColor.init(hexString: "#741A31")!.cgColor
                cell.lNameTectField.titleColor = UIColor.init(hexString: "#741A31")!
                cell.lNameSideView.backgroundColor = UIColor.black
                cell.lNameImage.tintColor = UIColor.init(hexString: "#741A31")
                return
            }
            
        case cell.email:
            
            print(str)
            if isValidEmail(email: str) {
                print("hello")
                cell.emailSideView.backgroundColor = UIColor.init(hexString: "#FBAD18")
                cell.emailValidImage.image = UIImage(named: "verified")
                cell.emailSideView.isHidden = false
                cell.emailImage.tintColor = nejreeColor
                cell.emailView.setBorder()
                
            } else {
                cell.emailSideView.backgroundColor = UIColor.black
                cell.emailValidImage.image = UIImage(named: "wrongname")
                cell.emailView.layer.borderWidth = 0.5
                cell.emailView.layer.borderColor = UIColor.init(hexString: "#741A31")!.cgColor
                cell.emailImage.tintColor = UIColor.init(hexString: "#741A31")
            }
            
        case cell.mobileNumber:
            print(str)
            if str.count == 9 {
                
                if APP_DEL.selectedLanguage == Arabic{
//                    cell.rightPhoneImage.tintColor = nejreeColor
//                    cell.rightMobileView.isHidden = false
//                    cell.rightMobileSideView.backgroundColor = UIColor.init(hexString: "#FBAD18")
                    
                    cell.leftPhoneImage.tintColor = nejreeColor
                    cell.leftMobileView.isHidden = false
                    cell.leftMobileSideView.backgroundColor = UIColor.init(hexString: "#FBAD18")
                } else {
                    cell.rightMobileView.isHidden = true
                    cell.leftPhoneImage.tintColor = nejreeColor
                    cell.leftMobileView.isHidden = false
                    cell.leftMobileSideView.backgroundColor = UIColor.init(hexString: "#FBAD18")
                }
                //cell.mobileSideView.backgroundColor = UIColor.init(hexString: "#FBAD18")
                
                cell.mobileValidImage.image = UIImage(named: "verified")
                
                //cell.mobileSideView.isHidden = false
                
                cell.mobileNumber.titleColor = UIColor.white
                
                cell.mobileNumberView.layer.borderWidth = 1.0
                //cell.phoneImage.tintColor = nejreeColor
                
                //cell.countryCodeView.setBorder()
                cell.mobileNumberView.setBorder()
                
            } else {
                
                if APP_DEL.selectedLanguage == Arabic{
                    
                    cell.leftPhoneImage.tintColor = UIColor.init(hexString: "#741A31")
                    cell.leftMobileView.isHidden = false
                    cell.leftMobileSideView.backgroundColor = UIColor.black
//                    cell.rightPhoneImage.tintColor = UIColor.init(hexString: "#741A31")
//                    cell.rightMobileView.isHidden = false
//                    cell.rightMobileSideView.backgroundColor = UIColor.black
                } else {
                    cell.leftPhoneImage.tintColor = UIColor.init(hexString: "#741A31")
                    cell.leftMobileView.isHidden = false
                    cell.rightMobileView.isHidden = true
                    cell.leftMobileSideView.backgroundColor = UIColor.black
                }
                //cell.mobileSideView.backgroundColor = UIColor.black
                //cell.phoneImage.tintColor = UIColor.init(hexString: "#741A31")
                
                cell.mobileValidImage.image = UIImage(named: "wrongname")
                cell.mobileNumber.titleColor = UIColor.init(hexString: "#741A31")!
                cell.mobileNumberView.layer.borderWidth = 1.0
                cell.mobileNumberView.layer.borderColor = UIColor.init(hexString: "#741A31")!.cgColor
            }
            
        case cell.password:
            print(str)
            if str.count >= 6 {
                cell.passwordSideView.backgroundColor = UIColor.init(hexString: "#FBAD18")
                cell.passwordValidImage.image = UIImage(named: "verified")
                cell.passwordSideView.isHidden = false
                cell.password.titleColor = UIColor.white
                cell.passwordImage.tintColor = UIColor.init(hexString: "#FBAD18")
                cell.passwordView.setBorder()
            } else {
                cell.passwordSideView.backgroundColor = UIColor.black
                cell.passwordValidImage.image = UIImage(named: "wrongname")
                cell.passwordView.layer.borderWidth = 0.5
                cell.password.titleColor = UIColor.init(hexString: "#741A31")!
                cell.passwordView.layer.borderColor = UIColor.init(hexString: "#741A31")!.cgColor
                cell.passwordImage.tintColor = UIColor.init(hexString: "#741A31")
            }
            
        case cell.confirmPassword:
            print(str)
            
            if str != "" {
                let str2 = cell.password.text!
                if str == str2 {
                    cell.cPasswordSideView.backgroundColor = UIColor.init(hexString: "#FBAD18")
                    cell.cPasswordValidImage.image = UIImage(named: "verified")
                    cell.cPasswordSideView.backgroundColor = nejreeColor
                    cell.confirmPassword.titleColor = UIColor.clear
                    cell.cPasswordImage.tintColor = UIColor.init(hexString: "#FBAD18")
                    cell.confirmPasswordView.setBorder()
                } else {
                    cell.cPasswordValidImage.image = UIImage(named: "wrongname")
                    cell.confirmPasswordView.layer.borderWidth = 1.0
                    cell.confirmPassword.titleColor = UIColor.clear
                    cell.confirmPasswordView.layer.borderColor = UIColor.init(hexString: "#741A31")!.cgColor
                    cell.cPasswordSideView.backgroundColor = .black
                    cell.cPasswordImage.tintColor = UIColor.init(hexString: "#741A31")
                }
            } else {
                cell.cPasswordValidImage.image = UIImage(named: "wrongname")
                cell.confirmPasswordView.layer.borderWidth = 1.0
                cell.confirmPassword.titleColor = UIColor.clear
                cell.confirmPasswordView.layer.borderColor = UIColor.init(hexString: "#741A31")!.cgColor
                cell.cPasswordSideView.backgroundColor = .black
                cell.cPasswordImage.tintColor = UIColor.init(hexString: "#741A31")
            }
            
            
            
           
            
        default:
            print("hellodefault")
        }
        
        if let cl = registerTable.cellForRow(at: IndexPath(row: 0, section: 2)) as? registerButtonCell
        {
            if cell.name.text! != "" && cell.email.text! != "" && cell.mobileNumber.text! != "" && cell.password.text! != "" && cell.confirmPassword.text! != "" {
                
               // cl.registerButton.backgroundColor = UIColor.init(hexString: "#fcb215")
               // cl.registerButton.alpha = 1
                cl.registerButton.layer.borderColor = UIColor.init(hexString: "#FBAD18")?.cgColor
                cl.registerButton.layer.borderWidth = 1.0
                cl.registerButton.setTitleColor(.white, for: .normal)
                
            } else {
                cl.registerButton.layer.borderColor = UIColor.lightGray.cgColor
                cl.registerButton.layer.borderWidth = 1.0
                cl.registerButton.setTitleColor(.lightGray, for: .normal)
            }
        }
       
        
        
        
        
    }
    
    
   /* func textFieldDidBeginEditing(_ textField: UITextField) {
        let indexPath = IndexPath(row: 0, section: 0)
        let cell = registerTable.cellForRow(at: indexPath) as! textFieldCell
        
        cell.validationimage.image = UIImage(named: "")
    }*/
    
    
}


extension UIView {
    func roundCorners() {
        self.clipsToBounds = true
        self.layer.cornerRadius = 15.0
        if #available(iOS 11.0, *) {
            self.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMinYCorner]
        } else {
            // Fallback on earlier versions
        }
    }
    
    func roundOfferLabel() {
        self.clipsToBounds = true
        self.layer.cornerRadius = 5.0
        if #available(iOS 11.0, *) {
            self.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        } else {
            // Fallback on earlier versions
        }
    }
    
    
    
    func setBorder() {
        self.layer.borderColor = UIColor.init(hexString: "#FFFFFF")?.cgColor
        self.layer.borderWidth = 0.5
    }
    func setBorderBlack() {
        self.layer.borderColor = UIColor.black.cgColor
        self.layer.borderWidth = 0.5
    }
    
    func setCornerRadius() {
        self.layer.cornerRadius = 0.0
    }
    
    func roundImageAndBorderColor() {
        self.clipsToBounds = true
        self.layer.cornerRadius = 15.0
        self.layer.borderColor = UIColor.clear.cgColor
        self.layer.borderWidth = 1.0
        if #available(iOS 11.0, *) {
            
            if APP_DEL.selectedLanguage == Arabic{
                self.layer.maskedCorners = [.layerMinXMinYCorner]
            }else {
                self.layer.maskedCorners = [.layerMaxXMinYCorner]
            }
        } else {
            // Fallback on earlier versions
        }
    }
}

