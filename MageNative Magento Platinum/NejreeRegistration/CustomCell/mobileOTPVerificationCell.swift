//
//  mobileOTPVerificationCell.swift
//  MageNative Magento Platinum
//
//  Created by MacMini on 16/07/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit
import FirebaseAuth

class mobileOTPVerificationCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
//        insideView.cardView()
        
        // Initialization code
    }

    @IBOutlet weak var insideView: UIView!
    
    @IBOutlet weak var mobileTextField: SkyFloatingLabelTextField!
    
    @IBOutlet weak var sendOtpBtn: UIButton!
    
    @IBOutlet weak var countryCodeField: SkyFloatingLabelTextField!
    
    
}
