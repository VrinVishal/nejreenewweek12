//
//  textFieldCell.swift
//  MageNative Magento Platinum
//
//  Created by MacMini on 15/07/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class textFieldCell: UITableViewCell {
    
    
    
    @IBOutlet weak var name: SkyFloatingLabelTextField!
    @IBOutlet weak var lNameTectField: SkyFloatingLabelTextField!
    @IBOutlet weak var email: SkyFloatingLabelTextField!
    @IBOutlet weak var mobileNumber: SkyFloatingLabelTextField!
    @IBOutlet weak var countryCode: SkyFloatingLabelTextField!
    @IBOutlet weak var password: SkyFloatingLabelTextField!
    @IBOutlet weak var confirmPassword: SkyFloatingLabelTextField!
    @IBOutlet weak var topLabel: UILabel!
    
    @IBOutlet weak var lblCountryCode: UILabel!
    @IBOutlet weak var validationimage: UIImageView!
    @IBOutlet weak var nameView: UIView!
    @IBOutlet weak var lastNameView: UIView!
    
    //side View For textfields
    @IBOutlet weak var sideView: UIView!
    @IBOutlet weak var emailSideView: UIView!
    @IBOutlet weak var passwordSideView: UIView!
    @IBOutlet weak var cPasswordSideView: UIView!
    //@IBOutlet weak var mobileSideView: UIView!
    @IBOutlet weak var leftMobileView: UIView!
    @IBOutlet weak var rightMobileView: UIView!
    @IBOutlet weak var leftMobileSideView: UIView!
    @IBOutlet weak var rightMobileSideView: UIView!
    @IBOutlet weak var lNameSideView: UIView!
    
    // outer View For textfields
    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var passwordView: UIView!
    @IBOutlet weak var confirmPasswordView: UIView!
    @IBOutlet weak var mobileNumberView: UIView!
    @IBOutlet weak var mobileNumberCenterView: UIView!
    //@IBOutlet weak var countryCodeView: UIView!
    
    // imageView for Validation
    @IBOutlet weak var emailValidImage: UIImageView!
    @IBOutlet weak var mobileValidImage: UIImageView!
    @IBOutlet weak var cPasswordValidImage: UIImageView!
    @IBOutlet weak var passwordValidImage: UIImageView!
    @IBOutlet weak var lNameValidImage: UIImageView!
    
    // imageView for side images
    
    @IBOutlet weak var nameImage: UIImageView!
    @IBOutlet weak var emailImage: UIImageView!
    //@IBOutlet weak var phoneImage: UIImageView!
    @IBOutlet weak var leftPhoneImage: UIImageView!
    @IBOutlet weak var rightPhoneImage: UIImageView!
    @IBOutlet weak var passwordImage: UIImageView!
    @IBOutlet weak var cPasswordImage: UIImageView!
    @IBOutlet weak var lNameImage: UIImageView!
    
    // sideViews width constraints
    
    @IBOutlet weak var namesideViewHeight: NSLayoutConstraint!
    @IBOutlet weak var emailSideViewHeight: NSLayoutConstraint!
    @IBOutlet weak var leftMobileSideViewHeight: NSLayoutConstraint!
    @IBOutlet weak var rightMobileSideViewHeight: NSLayoutConstraint!
    @IBOutlet weak var passwordSideViewHeight: NSLayoutConstraint!
    @IBOutlet weak var cPasswordsideViewHeight: NSLayoutConstraint!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        mobileNumber.keyboardType = .asciiCapableNumberPad
    }
    
    
}

