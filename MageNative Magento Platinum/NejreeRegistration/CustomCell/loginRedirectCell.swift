//
//  loginRedirectCell.swift
//  MageNative Magento Platinum
//
//  Created by MacMini on 29/07/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class loginRedirectCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBOutlet weak var myStack: UIStackView!
    @IBOutlet weak var accountLabel: UILabel!
    @IBOutlet weak var gotoLogin: UIButton!
    
}
