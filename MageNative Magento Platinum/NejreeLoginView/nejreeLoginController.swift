//
//  nejreeLoginController.swift
//  MageNative Magento Platinum
//
//  Created by cedcoss on 12/10/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit
import FirebaseAnalytics


class nejreeLoginController: MagenativeUIViewController,UIGestureRecognizerDelegate {

   
    @IBOutlet weak var loginTable: UITableView!
    @IBOutlet weak var dismissButton: UIButton!
    
    var email = String() // used for social login
    var mainvc: UIViewController?
    var isFromCheckOut = Bool()
    var total = [String:String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupLoginController()
        APP_DEL.isLogoutFromAddAddress = false
        
         self.navigationController?.setNavigationBarHidden(true, animated: true)
        
//        if(APP_DEL.isLoginFromWishList){
//
//             self.navigationController?.navigationBar.isHidden = true
//        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
     
        if navigationController?.responds(to: #selector(getter: UINavigationController.interactivePopGestureRecognizer)) ?? false {
            
            self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
            self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        }
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        
        return true
    }
    
    func setupLoginController() {
        loginTable.backgroundColor = .black
        loginTable.delegate = self
        loginTable.dataSource = self
        loginTable.separatorStyle = .none
    }
    @objc func dismissButtonTapped(_ sender: UIButton) {
        
        if isFromWishList != "" {
            isFromWishList = ""
        }
        
        APP_DEL.isLoginFromWishList = false
        APP_DEL.isRedeemFromPushNotification = false
        APP_DEL.isProfileFromPushNotification = false
        APP_DEL.isOrderFromPushNotification = false
        
        APP_DEL.strDeepLinkData = ""
        
        self.navigationController?.popViewController(animated: true)
        //self.dismiss(animated: true, completion: nil)
    }
    override func recieveResponse(data: Data?, requestUrl: String?, response: URLResponse?) {
        guard let data = data else {return}
        do {
            var json = try JSON(data: data)
            json = json[0]
            print(json)
            
            if json["data"]["customer"][0]["status"].stringValue == "success" {
                if json["data"]["customer"][0]["isConfirmationRequired"].stringValue.lowercased() == "YES".lowercased() {
                   
                    cedMageHttpException.showAlertView(me: self, msg: json["data"]["customer"][0]["message"].stringValue, title: "Error")
                    return
                }
                let customer_id = json["data"]["customer"][0]["customer_id"].stringValue;
                let hashKey = json["data"]["customer"][0]["hash"].stringValue;
                let cart_summary = json["data"]["customer"][0]["cart_summary"].intValue;
                let name = json["data"]["customer"][0]["name"].stringValue;
                let mobile = json["data"]["customer"][0]["mobile_number"].stringValue
                
                let dict = ["email": email, "customerId": customer_id, "hashKey": hashKey];
                print(dict)
                
              
                
                self.defaults.set(name, forKey: "name")
                self.defaults.set(dict, forKey: "userInfoDict");
                self.defaults.set(mobile, forKey: "mobile_number");
                self.defaults.set(String(cart_summary), forKey: "items_count");
                self.setCartCount(view: self, items: String(cart_summary))
                
                UserDefaults.standard.set(email, forKey: "EmailId")
                self.defaults.set(email, forKey: "EmailId")
                
                // checking for old user
                if mobile == "" {
                    
                    let vc = UIStoryboard(name: "cedMageAccounts", bundle: nil).instantiateViewController(withIdentifier: "cedMageProfileChangeNumber") as! cedMageProfileChangeNumber
                    //vc.jsonData = self.jsonData
                    vc.isFromLogin = true
                    vc.hidesBottomBarWhenPushed = true
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                    return
                }
                
                let DataLogin = ["METHOD" : "email",
                                   "EMAIL" : email as Any,
                                   "PHONE_NUMBER" : mobile as Any] as [String : Any]
                Analytics.logEvent(AnalyticsEventLogin, parameters: DataLogin)
                
                let detail : [AnalyticKey:String] = [
                    .Email : email,
                    .PhoneNumber : mobile
                ]
                API().setEvent(eventName: .Login, eventDetail: detail) { (json, err) in }
                
                self.defaults.set(true, forKey: "isLogin")
                NotificationCenter.default.post(name: NSNotification.Name("loadDrawerAgain"), object: nil)
                self.view.makeToast(APP_LBL().login_successful, duration: 1.0, position: .center)
                APP_DEL.getCartCount()
                
                APP_DEL.getGiftCardData { (res) in
                    print("getGiftCardData:- ", res)
                }
                
                self.navigationController?.view.makeToast(APP_LBL().login_successful, duration: 2.0, position: .center, title: nil, image: nil, style: nil, completion: nil)
                cedMage.delay(delay: 1, closure: {
                    NotificationCenter.default.post(name: NSNotification.Name("loadDrawerAgain"), object: nil);
                    self.navigationController?.navigationBar.isHidden = false
                    APP_DEL.RegisterFirebaseTopic()
                    if !self.isFromCheckOut {
                        
                        (UIApplication.shared.delegate as! AppDelegate).changeLanguage()
                        
//                        let view = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "mainTabbar") as! cedMageTabbar
//                        view.modalPresentationStyle = .fullScreen
//                        self.present(view, animated: true, completion: nil)
                    }else{

                    }
                  
                })
            }else{
                cedMageHttpException.showAlertView(me: self, msg: APP_LBL().invalid_login_or_password, title: APP_LBL().error)
            }
        }catch let error {
            print(error.localizedDescription)
        }
    }
    
    
}

extension nejreeLoginController: UITableViewDataSource,UITableViewDelegate {
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        case 0:
            
            
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "nejreeLoginImageCell", for: indexPath) as! nejreeLoginImageCell
            cell.btnBack.addTarget(self, action: #selector(dismissButtonTapped(_:)), for: .touchUpInside)
            
            if(APP_DEL.isLoginFromWishList){
             
                cell.btnBack.isHidden = true
                
            }
            else{
                cell.btnBack.isHidden = false
            }
            
            let value=UserDefaults.standard.value(forKey: "AppleLanguages") as! [String]
            if value[0] == "ar" {
                
                cell.btnBack.setImage(UIImage(named: "BackArrowNewArabic"), for: .normal)
                
            } else {
              
                cell.btnBack.setImage(UIImage(named: "BackArrowNew"), for: .normal)
            }
            
            
            
            
            return cell
           
        case 1:
            
            
            
            
            
           let cell = tableView.dequeueReusableCell(withIdentifier: "nejreeSignInCell", for: indexPath) as! nejreeSignInCell
           
          // cell.emailSideView.backgroundColor = .black
          // cell.passwordSideView.backgroundColor = .black
           cell.emailVerifyImage.image = UIImage(named: "")
           cell.passwordVerifyImage.image = UIImage(named: "")
           
          // cell.emailView.roundCorners()
          // cell.passwordView.roundCorners()
           cell.emailView.setBorder()
           cell.passwordView.setBorder()
           
           
           
          // cell.signinButton.roundCorners()
           cell.signinButton.layer.borderColor = UIColor.lightGray.cgColor
           cell.signinButton.layer.borderWidth = 1.0
           cell.signinButton.setTitleColor(.lightGray, for: .normal)
           cell.signinButton.addTarget(self, action: #selector(signinTapped(_:)), for: .touchUpInside)
          // cell.emailImage.roundImageAndBorderColor()
          // cell.passwordImage.roundImageAndBorderColor()
           cell.forgotPasswordButton.addTarget(self, action: #selector(forgotpasswordTapped(_:)), for: .touchUpInside)
           cell.gotoRegisterButton.addTarget(self, action: #selector(gotoRegisterTapped(_:)), for: .touchUpInside)
           
           cell.passwordField.isSecureTextEntry = true
           cell.emailField.delegate = self
           cell.passwordField.delegate = self
           
           
           cell.emailView.setCornerRadius()
           cell.passwordView.setCornerRadius()
           
           cell.passwordField.clearsOnBeginEditing = false
           let value=UserDefaults.standard.value(forKey: "AppleLanguages") as! [String]
           if value[0] == "ar" {
               cell.emailField.textAlignment = .right
               cell.passwordField.textAlignment = .right
           } else {
               cell.emailField.textAlignment = .left
               cell.passwordField.textAlignment = .left
           }
           
           cell.selectionStyle = .none
           return cell

          
        default:
          
            
              let cell = tableView.dequeueReusableCell(withIdentifier: "nejreeSocialLoginCell", for: indexPath) as! nejreeSocialLoginCell
             // cell.facebookButton.addTarget(self, action: #selector(facebookTapped(_:)), for: .touchUpInside)
              cell.facebookView.backgroundColor = .black
              cell.facebookView.roundCorners()
              cell.facebookView.setBorder()
              cell.facebookImage.roundImageAndBorderColor()
              cell.facebookImage.layer.borderColor = nejreeColor?.cgColor
              
             // cell.googleButton.addTarget(self, action: #selector(googleTapped(_:)), for: .touchUpInside)
              cell.googleView.backgroundColor = .black
              cell.googleView.roundCorners()
              cell.googleView.setBorder()
              cell.googleImage.roundImageAndBorderColor()
              cell.googleImage.layer.borderColor = nejreeColor?.cgColor
              
              cell.selectionStyle = .none
              return cell
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return UITableView.automaticDimension
        case 1:
            return 392//150
        default:
            return 0
        }
    }
    
    @objc func forgotpasswordTapped(_ sender: UIButton) {
        let vc = UIStoryboard(name: "cedMageLogin", bundle: nil).instantiateViewController(withIdentifier: "nejreeForgetController") as! nejreeForgetController
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @objc func gotoRegisterTapped(_ sender: UIButton) {
        let vc = UIStoryboard(name: "cedMageLogin", bundle: nil).instantiateViewController(withIdentifier: "NejreeSignupVC") as! NejreeSignupVC
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
//        vc.modalPresentationStyle = .fullScreen
//        self.present(vc, animated: true)
    }
    
    
   
        
    }

    



extension nejreeLoginController: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        let cell = loginTable.cellForRow(at: IndexPath(row: 0, section: 1)) as! nejreeSignInCell
        
        let validEmail = EmailVerifier.isValidEmail(testStr: cell.emailField.text!)
        
        switch textField {
        case cell.emailField:
            if isValidEmail(email: cell.emailField.text!) {
                
                cell.emailField.textColor = UIColor.white
                
                cell.emailSideView.backgroundColor = nejreeColor
                cell.emailImage.tintColor = nejreeColor
                cell.emailView.layer.borderColor = nejreeColor?.cgColor
                cell.emailVerifyImage.image = UIImage(named: "iconCorrectCheckMark")
            } else {
                
                cell.emailField.textColor = nejreeColor
                
                cell.emailSideView.backgroundColor = .black
                cell.emailImage.tintColor = UIColor.init(hexString: "#FBAD18")
                cell.emailView.layer.borderColor = UIColor.init(hexString: "#FFFFFF")?.cgColor
                cell.emailVerifyImage.image = UIImage(named: "IconIncorrectCheck")
            }
        default:
            if cell.passwordField.text!.count >= 6 {
                
                 cell.passwordField.textColor = UIColor.white
                
                cell.passwordSideView.backgroundColor = nejreeColor
                cell.passwordImage.tintColor = nejreeColor
                cell.passwordView.layer.borderColor = nejreeColor?.cgColor
                cell.passwordVerifyImage.image = UIImage(named: "iconCorrectCheckMark")
            } else {
                
                 cell.passwordField.textColor = nejreeColor
                
                cell.passwordSideView.backgroundColor = .black
                cell.passwordImage.tintColor = UIColor.init(hexString: "#FBAD18")
                cell.passwordView.layer.borderColor = UIColor.init(hexString: "#FFFFFF")?.cgColor
                cell.passwordVerifyImage.image = UIImage(named: "IconIncorrectCheck")
            }
        }
        
        if cell.emailField.text! != "" && cell.passwordField.text! != "" && validEmail && cell.passwordField.text!.count >= 6 {
            //cell.signinButton.setBorder()
            cell.signinButton.setTitleColor(.white, for: .normal)
            cell.signinButton.backgroundColor = UIColor(red: 251/255, green: 173/255, blue: 24/255, alpha: 1.0)
            cell.signinButton.titleLabel?.textColor = UIColor.black
        }else {
            cell.signinButton.layer.borderColor = UIColor.lightGray.cgColor
            cell.signinButton.setTitleColor(.lightGray, for: .normal)
            cell.signinButton.backgroundColor = UIColor.clear
            cell.signinButton.titleLabel?.textColor = UIColor.white
        }
        
    }
    
    @objc func signinTapped(_ sender: UIButton) {
        let cell = loginTable.cellForRow(at: IndexPath(row: 0, section: 1)) as! nejreeSignInCell
        
        email = cell.emailField.text!
        email = email.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
        var password = cell.passwordField.text!
        password = password.trimmingCharacters(in:NSCharacterSet.whitespacesAndNewlines);
        
        
         self.view.endEditing(true)
        let validEmail = EmailVerifier.isValidEmail(testStr: email)
               
          
          if email == ""{
              cedMageHttpException.showAlertView(me: self, msg: APP_LBL().please_enter_email, title: APP_LBL().error)
                 return;
             }
             else if !validEmail {
              cedMageHttpException.showAlertView(me: self, msg: APP_LBL().please_enter_valid_email, title: APP_LBL().error)
                 return;
             }
             else if password == ""{
                 cedMageHttpException.showAlertView(me: self, msg: APP_LBL().please_enter_password, title: APP_LBL().error)
                 return;
             }
          
          
          if email == "" || password == "" {
              self.view.makeToast(APP_LBL().fields_should_not_be_left_empty, duration: 1.0, position: .center);
              return
              
          }
        var postString = ["email":email,"password":password]
        if(defaults.object(forKey: "cartId") != nil) {
            let cart_id = defaults.object(forKey: "cartId") as? String
            postString.updateValue(cart_id!, forKey: "cart_id")
        }
        self.sendRequest(url: "mobiconnect/customer/login/", params: postString)
    }
    
    
}
