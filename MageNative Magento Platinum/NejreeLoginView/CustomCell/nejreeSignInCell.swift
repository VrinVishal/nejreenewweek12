//
//  nejreeSignInCell.swift
//  MageNative Magento Platinum
//
//  Created by cedcoss on 12/10/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class nejreeSignInCell: UITableViewCell {

   
    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var passwordView: UIView!
    @IBOutlet weak var emailSideView: UIView!
    @IBOutlet weak var passwordSideView: UIView!
    @IBOutlet weak var emailImage: UIImageView!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var emailVerifyImage: UIImageView!
    @IBOutlet weak var passwordImage: UIImageView!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var passwordVerifyImage: UIImageView!
    @IBOutlet weak var forgotPasswordButton: UIButton!
    @IBOutlet weak var signinButton: UIButton!
    @IBOutlet weak var gotoRegisterButton: UIButton!
    
    @IBOutlet weak var lblForgotPassword: UILabel!
    @IBOutlet weak var lblNewToNejree: UILabel!
    @IBOutlet weak var lblSignUp: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        signinButton.setTitle(APP_LBL().sign_in.uppercased(), for: .normal)
       // emailField.placeholder = APP_LBL().email_star.uppercased()
      //  passwordField.placeholder = APP_LBL().password_star.uppercased()
       // forgotPasswordButton.setTitle(APP_LBL().forgot_password.uppercased(), for: .normal)
      //  gotoRegisterButton.setTitle(APP_LBL().new_to_nejree_sign_up.uppercased(), for: .normal)
        
        emailField.attributedPlaceholder = NSAttributedString(string: APP_LBL().email_star.uppercased(),
        attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        
        passwordField.attributedPlaceholder = NSAttributedString(string: APP_LBL().password_star.uppercased(),
               attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        
        emailField.font = UIFont(name: "Cairo-Regular", size: 14)!
        passwordField.font = UIFont(name: "Cairo-Regular", size: 14)!
        
        
         lblForgotPassword.font = UIFont(name: "Cairo-Regular", size: 15)!
        lblNewToNejree.font = UIFont(name: "Cairo-Regular", size: 17)!
        lblSignUp.font = UIFont(name: "Cairo-Regular", size: 20)!
        
        lblForgotPassword.text = APP_LBL().forgot_password.uppercased()
       // lblSignUp.text = APP_LBL().sign_up.uppercased()
        
        
//        let underlineAttribute = [NSUnderlineStyleAttributeName: NSUnderlineStyle.StyleSingle.rawValue]
//        let underlineAttributedString = NSAttributedString(string: "StringWithUnderLine", attributes: underlineAttribute)
//        lblSignUp.attributedText = underlineAttributedString
//
        
        
        lblSignUp.attributedText = NSAttributedString(string: String(format: "%@!", APP_LBL().sign_up.uppercased()), attributes:
        [.underlineStyle: NSUnderlineStyle.single.rawValue])
        
        lblNewToNejree.text = APP_LBL().new_to_nejree_sign_up.uppercased()
        
    }



}
