//
//  nejreeSocialLoginCell.swift
//  MageNative Magento Platinum
//
//  Created by cedcoss on 12/10/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class nejreeSocialLoginCell: UITableViewCell {

   
    @IBOutlet weak var facebookView: UIView!
    @IBOutlet weak var facebookButton: UIButton!
    @IBOutlet weak var facebookImage: UIImageView!
    
    @IBOutlet weak var googleView: UIView!
    @IBOutlet weak var googleButton: UIButton!
    @IBOutlet weak var googleImage: UIImageView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

  

}
