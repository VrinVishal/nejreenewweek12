//
//  nejreeLoginImageCell.swift
//  MageNative Magento Platinum
//
//  Created by cedcoss on 14/10/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class nejreeLoginImageCell: UITableViewCell {

    
     @IBOutlet weak var btnBack: UIButton!
     @IBOutlet weak var lblSignIn: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.lblSignIn.text = APP_LBL().sign_in.uppercased()
        self.lblSignIn.font = UIFont(name: "Cairo-Regular", size: 18)!
        
    }

    

}
