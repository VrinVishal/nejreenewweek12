//
//  nAccountController.swift
//  MageNative Magento Platinum
//
//  Created by cedcoss on 12/10/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit
import SafariServices
import FirebaseMessaging

var storeCreditAmount = String()

class nAccountController: MagenativeUIViewController , SFSafariViewControllerDelegate
{

  
    @IBOutlet weak var accountTable: UITableView!
    
    var Stores = [[String:String]]()
    
    
    @IBOutlet weak var viewThankYou : UIView!
    @IBOutlet weak var lblThankYou : UILabel!
    @IBOutlet weak var btnThankYou : UIButton!
    
     var ExpandConnectWithUs : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        accountTable.isHidden = true
        sendrequestForStoreCredit()
        
        self.navigationController?.navigationBar.isHidden = true
        
        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(self.refreshAccount(not:)), name: NSNotification.Name("RefreshAccount"), object: nil)
    }
    
    @objc func refreshAccount(not: Notification) {
        
        sendrequestForStoreCredit()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func setupAccountController() {
        
        let nib = UINib(nibName: "nAccountConnectWithUsCell", bundle: nil)
              accountTable.register(nib, forCellReuseIdentifier: "nAccountConnectWithUsCell")
        
        let nib1 = UINib(nibName: "nAccountStoreCreditCell", bundle: nil)
                     accountTable.register(nib1, forCellReuseIdentifier: "nAccountStoreCreditCell")
        
        accountTable.delegate = self
        accountTable.dataSource = self
        accountTable.separatorStyle = .none
        accountTable.backgroundColor = .black
        accountTable.isHidden = false
        accountTable.reloadData()
        
        lblThankYou.text = APP_LBL().thanks_for_contacting_us
        btnThankYou.titleLabel?.text = APP_LBL().thank_you.uppercased()
        btnThankYou.titleLabel?.font = UIFont(name: "Cairo-Regular", size: 17)!
        lblThankYou.font = UIFont(name: "Cairo-Regular", size: 15)!
        
        btnThankYou.setCornerRadius()
        btnThankYou.addTarget(self, action: #selector(ActionButtonThankyou(_:)), for: .touchUpInside)
        
        viewThankYou.setCornerRadius()
        self.viewThankYou.isHidden = true
        
        self.view.bringSubviewToFront(self.viewThankYou)
        
        
        if APP_DEL.isRedeemFromPushNotification{
            let vc = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "NejreeReedemVC") as! NejreeReedemVC
            vc.store_credit = storeCreditAmount
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        if APP_DEL.isProfileFromPushNotification{
            let viewcontroll = UIStoryboard(name: "cedMageAccounts", bundle: nil).instantiateViewController(withIdentifier: "profileView") as! cedMageProfileView
            self.navigationController?.pushViewController(viewcontroll, animated: true)
        }
        
        if APP_DEL.isOrderFromPushNotification{
            let viewcontroll = UIStoryboard(name: "cedMageAccounts", bundle: nil).instantiateViewController(withIdentifier: "cedMageOrders") as! cedMageMyOrders
            self.navigationController?.pushViewController(viewcontroll, animated: true)
        }
        
    }
    
    @objc func ActionButtonThankyou(_ sender: UIButton){
        
         ExpandConnectWithUs = false
        self.viewThankYou.isHidden = true
         accountTable.reloadData()
        
        }
    
    func sendrequestForStoreCredit() {
        if(defaults.bool(forKey: "isLogin"))
               {
                self.storeCredit()
        } else {
            setupAccountController()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.accountTable.reloadData()
        self.tabBarController?.tabBar.isHidden = false
        
        if APP_DEL.isLogoutFromAddAddress{
            
            if let vc = UIStoryboard(name: "cedMageLogin", bundle: nil).instantiateViewController(withIdentifier: "nejreeLoginController") as? nejreeLoginController{
                APP_DEL.isLoginFromWishList = false
                vc.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
        }
        
    }
    
    func storeCredit() {
        
        var newPram = [String: String]()
        
        if let userInfoDict = UserDefaults.standard.object(forKey: "userInfoDict") as? [String: String] {

            newPram["customer_id"] = userInfoDict["customerId"]
        }
        
        if let storeID =  UserDefaults.standard.value(forKey: "storeId") as? String {
            newPram["store_id"] = storeID
        }
        
        newPram["currency_code"] = "SAR";
        
        newPram["country_id"] = APP_DEL.selectedCountry.country_code ?? "SA"
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);
        
        API().callAPI(endPoint: "mobiconnect/storecredit", method: .POST, param: newPram) { (json_res, err) in
            
          //  DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                if err == nil {
                    
                    let json = json_res[0]
                    
                    if (json["data"]["status"].stringValue == "true") {
                        storeCreditAmount = json["data"]["store_credit"].stringValue;
                        self.setupAccountController()
                    }
                }
          //  }
        }
    }
    
    func logout() {
        
        var newPram = [String: String]()
        
        if let userInfoDict = UserDefaults.standard.object(forKey: "userInfoDict") as? [String: String] {
            
            newPram["hashkey"] = userInfoDict["hashKey"]
            newPram["customer_id"] = userInfoDict["customerId"]
        }
        
        if let storeID =  UserDefaults.standard.value(forKey: "storeId") as? String {
            newPram["store_id"] = storeID
        }
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);
        
        API().callAPI(endPoint: "mobiconnect/customer/logout", method: .POST, param: newPram) { (json_res, err) in
            
          //  DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                if err == nil {
                    
                    self.defaults.set(false, forKey: "isLogin");
                    self.defaults.removeObject(forKey: "userInfoDict");
                    self.defaults.removeObject(forKey: "selectedAddressId");
                    self.defaults.setValue("Magenative App For Magento 2", forKey: "name")
                    UserDefaults.standard.set(nil,forKey: "LIAccessToken")
                    UserDefaults.standard.synchronize();
                    self.defaults.synchronize()
                    self.defaults.removeObject(forKey: "selectedAddressDescription");
                    
                    tempAddressObjApplePay = nil
                    
                    self.defaults.removeObject(forKey: "cartId");
                    self.defaults.removeObject(forKey: "cart_summary");
                    self.defaults.removeObject(forKey: "guestEmail");
                    self.defaults.set("0", forKey: "items_count");
                    self.defaults.removeObject(forKey: "name")
                    //self.defaults.removeObject(forKey: "mobile_number")
                    self.setCartCount(view: self, items: "0")
                    self.navigationController?.navigationBar.isHidden = false
                    
                    UserDefaults.standard.set(0, forKey: "NejreeUnreadCount")
                    UserDefaults.standard.set("0", forKey: "wishlist_item_count")
                    
                    APP_DEL.changeLanguage()
                    
                    Messaging.messaging().unsubscribe(fromTopic: strTopicName) { (err) in
                        print("UnSubscribed to \(strTopicName)) topic")
                    }
                }
          //  }
        }
    }
    
    func getList() {
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);

        API().callAPI(endPoint: "mobiconnectstore/getlist", method: .GET, param: [:]) { (json_res, err) in
            
          //  DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                if err == nil {
                    
                    let json = json_res[0]
                    
                    self.Stores.removeAll()
                    
                    for store in json["store_data"].arrayValue{
                        
                        let group_id = store["group_id"].stringValue
                        
                        let code = store["code"].stringValue
                        let store_id = store["store_id"].stringValue
                        let name = store["name"].stringValue
                        let is_active = store["is_active"].stringValue
                        let storeobject = ["group_id":group_id,"code":code,"store_id":store_id,"name":name,"is_active":is_active]
                        self.Stores.append(storeobject)
                    }
                    
                    self.showStores()
                }
         //   }
        }
    }
        
    func contactUS(param: [String:String]) {
        
        var newPram = param
        
        if let storeID =  UserDefaults.standard.value(forKey: "storeId") as? String {
            newPram["store_id"] = storeID
        }
        newPram["country_id"] =  APP_DEL.selectedCountry.country_code ?? "SA"

        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);

        API().callAPI(endPoint: "contactus", method: .POST, param: newPram) { (json_res, err) in
            
         //   DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                if err == nil {
                    
                    self.viewThankYou.isHidden = false
                }
           // }
        }
    }
    
    func setStore(url: String) {

        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);

        API().callAPI(endPoint: url, method: .GET, param: [:]) { (json_res, err) in
            
        //    DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                if err == nil {
                    
                    let lang = json_res[0]["locale_code"].stringValue
                    
                    let language=lang.components(separatedBy: "_")
                    if language[0]=="ar"
                    {
                        UserDefaults.standard.set(["ar","en","fr"], forKey: "AppleLanguages")
                        UIView.appearance().semanticContentAttribute = .forceRightToLeft
                    }
                    else if language[0]=="en"
                    {
                        UserDefaults.standard.set(["en","ar","fr"], forKey: "AppleLanguages")
                        UIView.appearance().semanticContentAttribute = .forceLeftToRight
                    }
                    
                    APP_DEL.changeLanguage()
                }
           // }
        }
    }
    
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {

        controller.dismiss(animated: true, completion: nil)
    }
}

extension nAccountController: UITableViewDelegate,UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 8
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            if defaults.bool(forKey: "isLogin") == true {return 1} else {return 1}
        case 1:
           if defaults.bool(forKey: "isLogin") == true {return 0} else {return 1}
        case 2:
            if defaults.bool(forKey: "isLogin") == true {return 0} else {return 0}
        case 3:
           if defaults.bool(forKey: "isLogin") == true {return 6} else {return 0}
        case 4:
            return 0
        case 5:
            return 7
        case 6:
            return 2
        default:
            if defaults.bool(forKey: "isLogin") == true {return 1} else {return 0}
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "nAcountImageCell", for: indexPath) as! nAcountImageCell
            
            if APP_DEL.selectedLanguage == Arabic{
                cell.bannerImage.image = UIImage(named: "MyAccountNewBanner")
                cell.lblMyAccount.semanticContentAttribute = .forceLeftToRight
            }
            else{
                cell.bannerImage.image = UIImage(named: "MyAccountNewBanner")
            }
            
            cell.lblMyAccount.text = APP_LBL().my_account.uppercased()
            cell.lblMyAccount.font = UIFont(name: "Cairo-Regular", size: 18)!
           
            cell.selectionStyle = .none
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "naccountloginRegisterCell", for: indexPath) as! naccountloginRegisterCell
           // cell.signinButton.roundCorners()
            cell.signinButton.setBorder()
            cell.signinButton.addTarget(self, action: #selector(redirectToLogin(_:)), for: .touchUpInside)
            cell.createAccountButton.addTarget(self, action: #selector(redirectToSignup(_:)), for: .touchUpInside)
            
           // cell.lblCreateNewAccount.font = UIFont(name: "Cairo-Regular", size: 15)!
             cell.signinButton.setCornerRadius()
            
           
            
            cell.lblWelcomeToNejree.font = UIFont(name: "Cairo-Regular", size: 18)!
            cell.signinButton.titleLabel?.font = UIFont(name: "Cairo-Regular", size: 15)!
            
            cell.backgroundColor = .black
            cell.selectionStyle = .none
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "accountsigninHeadingCell", for: indexPath) as! accountsigninHeadingCell
            cell.backgroundColor = .black
            
            if(defaults.bool(forKey: "isLogin"))
            {
                cell.signInLabel.text = APP_LBL().my_account
            } else {
                cell.signInLabel.text = APP_LBL().sign_in_c
            }
            
          
             cell.signInLabel.textAlignment = .left
           
            cell.selectionStyle = .none
            return cell
        case 3:
            switch indexPath.row {
            case 0:
                let cell = tableView.dequeueReusableCell(withIdentifier: "nAccountStoreCreditCell", for: indexPath) as! nAccountStoreCreditCell
                cell.backgroundColor = .black
                
               
                let name = self.defaults.value(forKey: "name") as! String
                let nameArray = name.components(separatedBy: " ")
                if nameArray.count > 1 {
               
                cell.lblUserName.attributedText = NSAttributedString(string: String(format: " %@", nameArray[0]), attributes:
                                       [.underlineStyle: NSUnderlineStyle.single.rawValue])
                } else {
                   
                    cell.lblUserName.attributedText = NSAttributedString(string: String(format: " %@", self.defaults.value(forKey: "name") as! String), attributes:
                                                          [.underlineStyle: NSUnderlineStyle.single.rawValue])
                    
                }
                            
                
                
                cell.lblStoreCredit.text = APP_LBL().store_credit_semicolon.uppercased()
                cell.lblStoreCreditAmount.text = storeCreditAmount
                
                
//                cell.storeCreditLabel.isHidden = false
//                cell.storeCreditLabel.text = APP_LBL().store_credit_semicolon + " " + storeCreditAmount
//                cell.headingLabel.text = APP_LBL().account
                cell.selectionStyle = .none
                return cell
            case 1:
                let cell = tableView.dequeueReusableCell(withIdentifier: "nAccountContentCell", for: indexPath) as! nAccountContentCell
                cell.contentLabel.text = APP_LBL().orders
                cell.contentLabel.textColor = .white
                cell.backgroundColor = .black
                cell.selectionStyle = .none
                return cell
                
            case 2:
                let cell = tableView.dequeueReusableCell(withIdentifier: "nAccountContentCell", for: indexPath) as! nAccountContentCell
                cell.contentLabel.text = APP_LBL().redeem
                cell.contentLabel.textColor = .white
                cell.backgroundColor = .black
                cell.selectionStyle = .none
                return cell
                
//            case 3:
//                let cell = tableView.dequeueReusableCell(withIdentifier: "nAccountContentCell", for: indexPath) as! nAccountContentCell
//                cell.contentLabel.text = APP_LBL().my_wishlist
//                cell.contentLabel.textColor = .white
//                cell.backgroundColor = .black
//                cell.selectionStyle = .none
//                return cell
            case 3:
                let cell = tableView.dequeueReusableCell(withIdentifier: "nAccountContentCell", for: indexPath) as! nAccountContentCell
                cell.contentLabel.text = APP_LBL().exchange_request
                cell.contentLabel.textColor = .white
                cell.backgroundColor = .black
                cell.selectionStyle = .none
                return cell
            case 4:
                let cell = tableView.dequeueReusableCell(withIdentifier: "nAccountContentCell", for: indexPath) as! nAccountContentCell
                cell.contentLabel.text = APP_LBL().profile_and_security
                cell.contentLabel.textColor = .white
                cell.backgroundColor = .black
                cell.selectionStyle = .none
                return cell
            default:
                let cell = tableView.dequeueReusableCell(withIdentifier: "nAccountContentCell", for: indexPath) as! nAccountContentCell
                cell.contentLabel.text = APP_LBL().address
                cell.contentLabel.textColor = .white
                cell.backgroundColor = .black
                cell.selectionStyle = .none
                return cell
            }
        case 4:
//                let cell = tableView.dequeueReusableCell(withIdentifier: "nAccountContentCell", for: indexPath) as! nAccountContentCell
//                cell.contentLabel.text = APP_LBL().settings
//                cell.contentLabel.textColor = .white
//                cell.backgroundColor = .black
//                cell.selectionStyle = .none
//                return cell
            return UITableViewCell()
           
        case 5:
            switch indexPath.row {
            case 0:
                let cell = tableView.dequeueReusableCell(withIdentifier: "nAccountHeadingCell", for: indexPath) as! nAccountHeadingCell
                cell.backgroundColor = .black
                cell.headingLabel.font = UIFont(name: "Cairo-Regular", size: 18)!
                cell.headingLabel.textColor = UIColor(red: 117/255, green: 117/255, blue: 117/255, alpha: 1.0)
                cell.headingLabel.text = APP_LBL().support.uppercased()
                cell.selectionStyle = .none
                return cell
            case 1:
                let cell = tableView.dequeueReusableCell(withIdentifier: "nAccountContentCell", for: indexPath) as! nAccountContentCell
                cell.contentLabel.text = APP_LBL().about_nejree
                cell.contentLabel.textColor = .white
                cell.backgroundColor = .black
                cell.selectionStyle = .none
                return cell
            case 2:
                let cell = tableView.dequeueReusableCell(withIdentifier: "nAccountContentCell", for: indexPath) as! nAccountContentCell
                cell.contentLabel.text = APP_LBL().terms_amp_condition
                cell.contentLabel.textColor = .white
                cell.backgroundColor = .black
                cell.selectionStyle = .none
                return cell
            case 3:
                let cell = tableView.dequeueReusableCell(withIdentifier: "nAccountContentCell", for: indexPath) as! nAccountContentCell
                
                let value=UserDefaults.standard.value(forKey: "AppleLanguages") as! [String]
                cell.contentLabel.text = APP_LBL().privacy_exchange_policy
                
                //cell.contentLabel.text = "Privacy Policy nijree".localized
                
                cell.contentLabel.textColor = .white
                cell.backgroundColor = .black
                cell.selectionStyle = .none
                return cell
            case 4:
                let cell = tableView.dequeueReusableCell(withIdentifier: "nAccountContentCell", for: indexPath) as! nAccountContentCell
                cell.contentLabel.text = APP_LBL().settings
                cell.contentLabel.textColor = .white
                cell.backgroundColor = .black
                cell.selectionStyle = .none
                return cell
                
            case 5:
                let cell = tableView.dequeueReusableCell(withIdentifier: "nAccountContentCell", for: indexPath) as! nAccountContentCell
                cell.contentLabel.text = APP_LBL().faqs
                cell.contentLabel.textColor = .white
                cell.backgroundColor = .black
                cell.selectionStyle = .none
                return cell
                
            default:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "nAccountContentCell", for: indexPath) as! nAccountContentCell
                cell.contentLabel.text = APP_LBL().rate_this_app
                cell.contentLabel.textColor = .white
                cell.backgroundColor = .black
                cell.selectionStyle = .none
                return cell
            }
        case 6:
            switch indexPath.row {
           case 0:
             let cell = tableView.dequeueReusableCell(withIdentifier: "nAccountConnectWithUsCell", for: indexPath) as! nAccountConnectWithUsCell
             cell.backgroundColor = .black
            
             if ExpandConnectWithUs {
                 cell.viewConnectUsData.isHidden = false
                 cell.imgArrow.image = UIImage(named: "icon_arrow_up")
             }else{
                 cell.viewConnectUsData.isHidden = true
                 cell.imgArrow.image = UIImage(named: "icon_arrow_down")

             }
             
             
             cell.btnExpand.addTarget(self, action: #selector(ActionExpandConnectWithUs), for: .touchUpInside)
              cell.btnSubmit.addTarget(self, action: #selector(actionBtnSubmit), for: .touchUpInside)
             
             cell.viewEmail.setBorder()
             cell.viewName.setBorder()
             cell.viewPhoneNumber.setBorder()
             cell.viewTextView.setBorder()
             
             cell.btnSubmit.setBorder()
             
             cell.btnSubmit.layer.borderColor = UIColor.lightGray.cgColor
             cell.btnSubmit.layer.borderWidth = 0.5
             
             
             cell.txtName.text = ""
             cell.txtEmail.text = ""
             cell.txtPhoneNumber.text = ""
             cell.txtDes.text = APP_LBL().whats_in_your_mind
             
             cell.txtName.placeholder = APP_LBL().name
             cell.txtEmail.placeholder = APP_LBL().email_star
             cell.txtPhoneNumber.placeholder = APP_LBL().phone_number_star
            
            
            cell.txtName.attributedPlaceholder = NSAttributedString(string: APP_LBL().name,
            attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
            
            cell.txtEmail.attributedPlaceholder = NSAttributedString(string: APP_LBL().email_star,
            attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
            
            cell.txtPhoneNumber.attributedPlaceholder = NSAttributedString(string: APP_LBL().phone_number_star,
            attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
            
            
            
            
             cell.lblConnectWithUs.text = APP_LBL().connect_with_us
            cell.btnSubmit.setTitle(APP_LBL().submit.uppercased(), for: .normal)
            
            
             
             
             cell.txtName.font = UIFont(name: "Cairo-Regular", size: 15)!
             cell.txtEmail.font = UIFont(name: "Cairo-Regular", size: 15)!
             cell.txtPhoneNumber.font = UIFont(name: "Cairo-Regular", size: 15)!
             cell.txtDes.font = UIFont(name: "Cairo-Regular", size: 15)!
             
            // cell.lblConnectWithUs.font = UIFont(name: "Cairo-Regular", size: 18)!
             
              cell.viewName.setCornerRadius()
              cell.viewEmail.setCornerRadius()
              cell.viewPhoneNumber.setCornerRadius()
             cell.viewTextView.setCornerRadius()
             
             cell.btnSubmit.titleLabel?.textColor = UIColor.white
             cell.btnSubmit.backgroundColor = UIColor.clear
             
             cell.emailVerifyImage.image = UIImage(named: "")
             cell.nameVerifyImage.image = UIImage(named: "")
            cell.phoneVerifyImage.image = UIImage(named: "")
            cell.viewPhoneNumber.semanticContentAttribute = .forceLeftToRight
            cell.txtPhoneNumber.textAlignment = .left
             let value=UserDefaults.standard.value(forKey: "AppleLanguages") as! [String]
             if value[0] == "ar" {
                 cell.txtEmail.textAlignment = .right
                cell.txtName.textAlignment = .right
//                 cell.txtPhoneNumber.textAlignment = .right
                cell.txtDes.textAlignment = .right
                 cell.lblConnectWithUs.textAlignment = .right
             } else {
                cell.txtEmail.textAlignment = .left
                 cell.txtName.textAlignment = .left
//                  cell.txtPhoneNumber.textAlignment = .left
                 cell.txtDes.textAlignment = .left
                   cell.lblConnectWithUs.textAlignment = .left
             }
             
             
             
             
             cell.selectionStyle = .none
             return cell
            default:
                 let cell = tableView.dequeueReusableCell(withIdentifier: "nAccountContactCell", for: indexPath) as! nAccountContactCell
                 cell.backgroundColor = .black
                // cell.phoneButton.roundCorners()
                 cell.phoneButton.setBorder()
                 cell.phoneButton.setTitle(APP_LBL().call_us.uppercased(), for: .normal)
                 cell.phoneButton.setTitleColor(.white, for: .normal)
                 
                 let value=UserDefaults.standard.value(forKey: "AppleLanguages") as! [String]
                 if value[0]=="ar" {
                    cell.phoneButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 30, bottom: 0, right: 0)
                    cell.emailButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)
                 } else {
                    cell.phoneButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 30)
                    cell.emailButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 30)
                 }
                 
                 
                 cell.phoneButton.addTarget(self, action: #selector(callButtonClicked(_:)), for: .touchUpInside)
                 
               //  cell.emailButton.roundCorners()
                 cell.emailButton.setBorder()
                 cell.emailButton.setTitle(APP_LBL().email_us.uppercased(), for: .normal)
                 cell.emailButton.setTitleColor(.white, for: .normal);

                 
                  cell.emailButton.titleLabel?.font = UIFont(fontName: "Cairo-Regular", fontSize: 13)
                 cell.phoneButton.titleLabel?.font = UIFont(fontName: "Cairo-Regular", fontSize: 13)
                 
                 if value[0] == "ar" {
                    cell.emailButton.titleLabel?.font = UIFont(fontName: "Cairo-Regular", fontSize: 10)
                 }

                 
                 
                 
                 cell.emailButton.addTarget(self, action: #selector(mailButtonClicked(_:)), for: .touchUpInside)
                 cell.selectionStyle = .none
                 return cell
            }
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "nAccountLogoutCell", for: indexPath) as! nAccountLogoutCell
            cell.backgroundColor = .black
          //  cell.logoutButton.roundCorners()
            cell.logoutButton.setBorder()
             cell.logoutButton.titleLabel?.font = UIFont(fontName: "Cairo-Regular", fontSize: 17)
            cell.logoutButton.setTitle(APP_LBL().sign_out_c, for: .normal)
            cell.logoutButton.setTitleColor(.white, for: .normal)
            cell.logoutButton.addTarget(self, action: #selector(signOutTapped(_:)), for: .touchUpInside)
            cell.selectionStyle = .none
            return cell
        }
    }
    @IBAction func actionBtnSubmit(_ sender: UIButton) {
            
                let ip = IndexPath(row: 0, section: 6)
               let cell = accountTable.cellForRow(at: ip) as! nAccountConnectWithUsCell
        
            let validEmail = EmailVerifier.isValidEmail(testStr: cell.txtEmail.text!)
            let mobNumber = cell.txtPhoneNumber.text
        
             let description = cell.txtDes.text
            
            if cell.txtName.text == ""{

            cedMageHttpException.showAlertView(me: self, msg: APP_LBL().please_enter_name, title: APP_LBL().error)
                
            
               return;
           }

            else if cell.txtEmail.text == ""{
                
                cedMageHttpException.showAlertView(me: self, msg: APP_LBL().please_enter_email, title: APP_LBL().error)
                
               
               return;
           }
           else if !validEmail {
              
                 cedMageHttpException.showAlertView(me: self, msg: APP_LBL().please_enter_valid_email, title: APP_LBL().error)
                
               return;
           }
            else if cell.txtPhoneNumber.text == ""{
                
                cedMageHttpException.showAlertView(me: self, msg: APP_LBL().please_enter_phone_no, title: APP_LBL().error)
                
              
               return;
           }
            else if (mobNumber?.count)! < (Int(APP_DEL.selectedCountry.phone_number_limit ?? "9") ?? 9) {

                cedMageHttpException.showAlertView(me: self, msg: APP_LBL().please_enter_valid_phone_no, title: APP_LBL().error)
               return;
           }
                
            else if description == "" {

                cedMageHttpException.showAlertView(me: self, msg: APP_LBL().please_enter_whats_in_you_mind, title: APP_LBL().error)
                      return;
            }
                
            else if description == APP_LBL().whats_in_your_mind {

                cedMageHttpException.showAlertView(me: self, msg: APP_LBL().please_enter_whats_in_you_mind, title: APP_LBL().error)
                      return;
            }
                    
                
                
            else
            {
                
                var params = Dictionary<String,String>()
                params["firstname"] = cell.txtName.text;
                params["email"] = cell.txtEmail.text
                params["number"] = cell.txtPhoneNumber.text
                params["reason_text"] = cell.txtDes.text
                
                self.contactUS(param: params)
                

            }

        }
    
    @IBAction func ActionExpandConnectWithUs(_ sender : UIButton){
        if ExpandConnectWithUs {
            ExpandConnectWithUs = false
        }else{
            ExpandConnectWithUs = true
        }
        
        accountTable.reloadData()

       
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+0.1, execute: {
            let indexPath = IndexPath(row: 0, section: 6)
            self.accountTable.scrollToRow(at: indexPath, at: UITableView.ScrollPosition.bottom, animated: true)
        })
        
    }
    
    func safariViewControllerDidFinish(controller: SFSafariViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 3:
            switch indexPath.row {
            case 1:
                let viewcontroll = UIStoryboard(name: "cedMageAccounts", bundle: nil).instantiateViewController(withIdentifier: "cedMageOrders") as! cedMageMyOrders
                viewcontroll.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(viewcontroll, animated: true)
            case 2:
                let vc = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "NejreeReedemVC") as! NejreeReedemVC
         
                
                vc.store_credit = storeCreditAmount
               // vc.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(vc, animated: true)
//            case 3:
//
//            if defaults.bool(forKey: "isLogin") {
//
//
//                let wish = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "NejreeWishListVC") as! NejreeWishListVC
//                self.navigationController?.pushViewController(wish, animated: true)
//
//            }else{
//
//            if let vc = UIStoryboard(name: "cedMageLogin", bundle: nil).instantiateViewController(withIdentifier: "nejreeLoginController") as? nejreeLoginController {
//
//                vc.modalPresentationStyle = .fullScreen
//            self.present(vc, animated: true)
//            }
//        }

            
            
            
            case 3:
                
                let safariVC = SFSafariViewController(url: NSURL(string: "http://nejree.net/cs/rtn")! as URL)
                safariVC.modalPresentationStyle = .overFullScreen
                safariVC.modalTransitionStyle = .crossDissolve
                safariVC.delegate = self
                self.present(safariVC, animated: true, completion: nil)
                
                
//                let viewcontroll = UIStoryboard(name: "cedMageAccounts", bundle: nil).instantiateViewController(withIdentifier: "nejreeRmaListingController") as! nejreeRmaListingController
//                self.navigationController?.pushViewController(viewcontroll, animated: true)
            case 4:
                let viewcontroll = UIStoryboard(name: "cedMageAccounts", bundle: nil).instantiateViewController(withIdentifier: "profileView") as! cedMageProfileView
                viewcontroll.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(viewcontroll, animated: true)
            case 5:
                let viewcontroll = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "nejreeAddressBookController") as! nejreeAddressBookController
                viewcontroll.isFromAccount = true
                viewcontroll.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(viewcontroll, animated: true)
            default:
                print("nothing")
            }
        case 4:
//            switch indexPath.row {
//            case 0:
//                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NejreeSettingVC") as! NejreeSettingVC
//                self.navigationController?.pushViewController(vc, animated: true)
            
                
//            default:
//                print("nothing")
//            }
            break;
            
        case 5:
            switch indexPath.row {
            case 1:
                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "nejreeAboutController") as! nejreeAboutController
                 vc.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(vc, animated: true)
            case 2:
                 let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "termsMenuController") as! termsMenuController
                 vc.hidesBottomBarWhenPushed = true
                 self.navigationController?.pushViewController(vc, animated: true)
            case 3:
                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "privacyMenuController") as! privacyMenuController
                vc.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(vc, animated: true)
            case 4:
                
                let test = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: Constants().SCREEN_NAME_NejreeCountryLangSelectVC) as! NejreeCountryLangSelectVC
                test.isFromSplash = false
                test.hidesBottomBarWhenPushed = true
                self.navigationController?.pushToViewController(test, animated: true, completion: {
                    
                })
                
//                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NejreeSettingVC") as! NejreeSettingVC
//                vc.hidesBottomBarWhenPushed = true
//                self.navigationController?.pushViewController(vc, animated: true)
            case 5:
                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "FAQController") as! FAQController
                vc.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(vc, animated: true)
            case 6:
                print("redirect rate us")
                
                
                if let url = URL(string: "itms-apps://itunes.apple.com/app/id\(APP_ID)?action=write-review")
                {
                    if #available(iOS 10.0, *) {
                        
                        UIApplication.shared.open(url, options: [:]) { (opend) in
                            print("version Popup:- ",opend)
                        }
                        
                    } else {
                        
                        if UIApplication.shared.canOpenURL(url as URL) {
                            UIApplication.shared.openURL(url as URL)
                        } else {
                            print("Can not open")
                        }
                    }
                    
                } else {
                    
                    //Just check it on phone not simulator!
                    print("Can not open")
                }
            default:
                print("nothing")
            }
        default:
            print("nothing")
        }
    }
    
    
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return UITableView.automaticDimension
        case 1:
            return UITableView.automaticDimension
        case 2:
            return UITableView.automaticDimension
        case 3:
            switch indexPath.row {
            case 0:
                return UITableView.automaticDimension
            default:
                return UITableView.automaticDimension
            }
        case 4:
            switch indexPath.row {
            case 0:
                return UITableView.automaticDimension
            default:
                return UITableView.automaticDimension
            }
        case 5:
            switch indexPath.row {
            case 0:
                return UITableView.automaticDimension
            default:
                return UITableView.automaticDimension
            }
          case 6:
          switch indexPath.row {
          case 0:
              return UITableView.automaticDimension
          default:
              return 45
          }
        default:
            return UITableView.automaticDimension
        }
    }
    
}

// MARK:- Ectensiion for @objc functions
extension nAccountController {
    
    @objc func signOutTapped(_ sender: UIButton) {
        
        if(defaults.bool(forKey: "isLogin") == true)
        {
            let isCOD : Bool = (UserDefaults.standard.value(forKey: APP_DEL.key_isCOD) as? Bool) ?? false
            let isCardSelected : Bool = (UserDefaults.standard.value(forKey: APP_DEL.key_isCard) as? Bool) ?? false
            
            if isCOD == true || isCardSelected == true {
          

                UserDefaults.standard.setValue(false, forKey: APP_DEL.key_isCOD)
                UserDefaults.standard.setValue(false, forKey: APP_DEL.key_isCard)
                
                self.saveShippingPayament()
                
            } else {
                
                self.logout()
            }
        }
        
        
    }
    
    //MARK:- SAVE SHIPPING PAYMENT
        func saveShippingPayament() {

                var postData = [String:String]()

                postData["Role"] = "USER";

                if let cartID =  UserDefaults.standard.value(forKey: "cartId") as? String {
                    postData["cart_id"] = cartID
                }

                if let storeID =  UserDefaults.standard.value(forKey: "storeId") as? String {
                    postData["store_id"] = storeID
                }

                if let userInfoDict = UserDefaults.standard.object(forKey: "userInfoDict") as? [String: String] {

                    postData["hashkey"] = userInfoDict["hashKey"]
                    postData["customer_id"] = userInfoDict["customerId"]
                    postData["email"] = userInfoDict["email"]
                }

                postData["payment_method"] = "checkoutcom_apple_pay";

                postData["shipping_method"] = "freeshipping_freeshipping";


                cedMageLoaders.removeLoadingIndicator(me: self);
                cedMageLoaders.addDefaultLoader(me: self);

                API().callAPI(endPoint: "mobiconnect/checkout/saveshippingpayament", method: .POST, param: postData) { (json, err) in

                  //  DispatchQueue.main.async {

                        cedMageLoaders.removeLoadingIndicator(me: self);

                        self.logout()
                  //  }
                }
            }
    
    @objc func redirectToLogin(_ sender: UIButton){
            if let vc = UIStoryboard(name: "cedMageLogin", bundle: nil).instantiateViewController(withIdentifier: "nejreeLoginController") as? nejreeLoginController{
    //            self.navigationController?.pushViewController(vc, animated: true)
                //vc.modalPresentationStyle = .fullScreen
                APP_DEL.isLoginFromWishList = false
                vc.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    @objc func redirectToSignup(_ sender: UIButton){
            
        //NejreeSignupVC
        //nejreeRegisterController
            if let vc = UIStoryboard(name: "cedMageLogin", bundle: nil).instantiateViewController(withIdentifier: "NejreeSignupVC") as? NejreeSignupVC {
//                vc.modalPresentationStyle = .fullScreen
//                self.present(vc, animated: true, completion: nil);
                
                APP_DEL.isLoginFromWishList = false
                vc.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(vc, animated: true)
                
            }
        }
    @objc func callButtonClicked(_ sender: UIButton){
        guard let number = URL(string: "tel://" + "920009750") else { return }
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(number)
            } else {
                // Fallback on earlier versions
            }
        }
    @objc func mailButtonClicked(_ sender: UIButton){
            let email = "support@nejree.com"
            let url = URL(string: "mailto:\(email)")
            UIApplication.shared.openURL(url!)
        }
    func showStores(){
        let actionsheet = UIAlertController(title: APP_LBL().select_store, message: nil, preferredStyle: IS_IPAD ? .alert : .actionSheet)
        
        for buttons in self.Stores {
            
            actionsheet.addAction(UIAlertAction(title: buttons["name"], style: UIAlertAction.Style.default,handler: {
                action -> Void in
                self.selectStore(store: buttons["store_id"])
            }))
        }
        actionsheet.addAction(UIAlertAction(title: APP_LBL().cancel, style: UIAlertAction.Style.cancel, handler: {
            action -> Void in
        }))
        if(UIDevice().model.lowercased() == "iPad".lowercased()){
           // actionsheet.popoverPresentationController?.sourceView = self.mainTable.cellForRow(at: IndexPath(row: 5, section: 1))
            
        }
        self.present(actionsheet, animated: true, completion: nil)
    }
    func selectStore(store:String?){
           //let params = ["store_id":store! as String]
           defaults.setValue(store, forKey: "storeId")
            self.setStore(url: "mobiconnectstore/setstore/"+store!)
       }
    
    func showGender() {
        
        let actionsheet = UIAlertController(title: nil, message: nil, preferredStyle: IS_IPAD ? .alert : .actionSheet)
        
        for gen in arrGender {
            var genderTitle = ""
            let value=UserDefaults.standard.value(forKey: "AppleLanguages") as! [String]
            if value[0] == "ar" {
                genderTitle = (gen.title_ar ?? "").uppercased()
            } else {
                genderTitle = (gen.title ?? "").uppercased()
            }
            actionsheet.addAction(UIAlertAction(title: genderTitle, style: UIAlertAction.Style.default,handler: {
                action -> Void in
                UserDefaults.standard.setValue(gen.id, forKey: KEY_GENDER)
                self.accountTable.reloadData()
            }))
        }
        actionsheet.addAction(UIAlertAction(title: APP_LBL().cancel, style: UIAlertAction.Style.cancel, handler: {
            action -> Void in
        }))
        self.present(actionsheet, animated: true, completion: nil)
    }
}
