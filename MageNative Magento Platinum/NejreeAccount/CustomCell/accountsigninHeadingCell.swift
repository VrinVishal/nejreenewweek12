//
//  accountsigninHeadingCell.swift
//  MageNative Magento Platinum
//
//  Created by cedcoss on 12/10/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class accountsigninHeadingCell: UITableViewCell {

    
    @IBOutlet weak var signInLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        signInLabel.text = APP_LBL().sign_in.uppercased()
    }

    
   

}
