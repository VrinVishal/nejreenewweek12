//
//  nAccountConnectWithUsswift
//  MageNative Magento Platinum
//
//  Created by Vrinsoft on 02/07/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class nAccountConnectWithUsCell: UITableViewCell, UITextFieldDelegate, UITextViewDelegate {

    
    @IBOutlet weak var viewConnetWithUs: UIView!
    @IBOutlet weak var lblConnectWithUs: UILabel!
    @IBOutlet weak var imgArrow: UIImageView!
    
    @IBOutlet weak var btnExpand: UIButton!
    
    @IBOutlet weak var btnSubmit: UIButton!
    
    @IBOutlet weak var viewEmail: UIView!
    @IBOutlet weak var txtEmail: UITextField!
    
    @IBOutlet weak var viewName: UIView!
    @IBOutlet weak var txtName: UITextField!
    
    @IBOutlet weak var viewPhoneNumber: UIView!
    @IBOutlet weak var txtPhoneNumber: UITextField!
    
    @IBOutlet weak var lblCountryCode: UILabel!
    @IBOutlet weak var viewConnectUsData: UIView!
    
    @IBOutlet weak var emailVerifyImage: UIImageView!
    @IBOutlet weak var nameVerifyImage: UIImageView!
    @IBOutlet weak var phoneVerifyImage: UIImageView!
    
    var nejreeColor = UIColor.init(hexString: "#FBAD18")
    
    @IBOutlet weak var viewTextView: UIView!
    @IBOutlet weak var txtDes: UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        SetUIView()
        lblCountryCode.text = "+" + (APP_DEL.selectedCountry.phone_code ?? "966")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    
    func SetUIView(){
        
        
        txtPhoneNumber.keyboardType = .asciiCapableNumberPad
    }
    
    
    
    
    private func textViewDidBeginEditing(_ textView: UITextView) {
        
        if (txtDes.text == APP_LBL().whats_in_your_mind) {
                   txtDes.text = ""
                   txtDes.textColor = UIColor.white //optional
               }
       }
    

    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        
        if (txtDes.text == APP_LBL().whats_in_your_mind) {
                    txtDes.text = ""
                    txtDes.textColor = UIColor.white //optional
        }
        
        
        return true
    }
    
    
       
       func textViewDidEndEditing(_ textView: UITextView) {
         
               if (txtDes.text == "") {
                  txtDes.text = APP_LBL().whats_in_your_mind
                //txtDes.textColor = UIColor.lightGray
               }
               
               if txtDes.text.count <= 0 {
                   txtDes.text = APP_LBL().whats_in_your_mind
            
            //        txtDes.textColor = UIColor.lightGray //optional

               }
        if txtName.text != "" && txtEmail.text != "" && txtPhoneNumber.text != "" && txtDes.text != "" && txtDes.text != APP_LBL().whats_in_your_mind {
            btnSubmit.titleLabel?.textColor = UIColor.black
            btnSubmit.backgroundColor = UIColor(red: 251/255, green: 173/255, blue: 24/255, alpha: 1.0)
            
            
        }else {
            btnSubmit.titleLabel?.textColor = UIColor.white
            btnSubmit.backgroundColor = UIColor.clear
        }
        
        
       }
    
   func textFieldDidEndEditing(_ textField: UITextField) {

        let str = textField.text!
    
        switch textField {
            
        case txtName:
            print(str)
            if str != "" {
                let decimalCharacters = CharacterSet.decimalDigits
                
                let decimalRange = str.rangeOfCharacter(from: decimalCharacters)
                
                if decimalRange != nil {
                     txtName.textColor = nejreeColor
                    viewName.layer.borderColor = UIColor.init(hexString: "#FFFFFF")?.cgColor
                    self.nameVerifyImage.image = UIImage(named: "IconIncorrectCheck")
                }else {
                    txtName.textColor = UIColor.white
                    viewName.layer.borderColor = nejreeColor?.cgColor
                    self.nameVerifyImage.image = UIImage(named: "iconCorrectCheckMark")
                }
            }
            else
            {
                txtName.textColor = nejreeColor
                viewName.layer.borderColor = UIColor.init(hexString: "#FFFFFF")?.cgColor
                self.nameVerifyImage.image = UIImage(named: "IconIncorrectCheck")
            }
            
        case txtEmail:
            if isValidEmail(email: txtEmail.text!) {
                txtEmail.textColor = UIColor.white
                viewEmail.layer.borderColor = nejreeColor?.cgColor
                self.emailVerifyImage.image = UIImage(named: "iconCorrectCheckMark")
            } else {

                txtEmail.textColor = nejreeColor
                viewEmail.layer.borderColor = UIColor.init(hexString: "#FFFFFF")?.cgColor
                self.emailVerifyImage.image = UIImage(named: "IconIncorrectCheck")
            }
        default:
            print(str)
            if str.count == Int(APP_DEL.selectedCountry.phone_number_limit ?? "") {
                txtPhoneNumber.textColor = UIColor.white
                viewPhoneNumber.layer.borderColor = nejreeColor?.cgColor
                self.phoneVerifyImage.image = UIImage(named: "iconCorrectCheckMark")
                            
            } else {
                            
                txtPhoneNumber.textColor = nejreeColor
                viewPhoneNumber.layer.borderColor = UIColor.init(hexString: "#FFFFFF")?.cgColor
                self.phoneVerifyImage.image = UIImage(named: "IconIncorrectCheck")
            }
        }

    
    
    if txtName.text != "" && txtEmail.text != "" && txtPhoneNumber.text != "" && txtDes.text != "" && txtDes.text != APP_LBL().whats_in_your_mind {
            btnSubmit.titleLabel?.textColor = UIColor.black
            btnSubmit.backgroundColor = UIColor(red: 251/255, green: 173/255, blue: 24/255, alpha: 1.0)
            
            
        }else {
            btnSubmit.titleLabel?.textColor = UIColor.white
            btnSubmit.backgroundColor = UIColor.clear
        }
    
    

    }

    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if (textField == txtName){
            
            if let x = string.rangeOfCharacter(from: NSCharacterSet.decimalDigits) {
                        return false
             } else {
                
                if txtName.text!.count > 51 {
                    
                    return false
                }
                
                return true
             }
            
            
            
        }
        else if (textField == txtPhoneNumber){
            if (CharacterSet(charactersIn: "0123456789").isSuperset(of: CharacterSet(charactersIn: string))) {

                if let text = textField.text, let range = Range(range, in: text) {

                    let proposedText = text.replacingCharacters(in: range, with: string)

                    if (APP_DEL.selectedCountry.country_code?.uppercased() == "SA"){
                        if proposedText.first == "5" && proposedText.count <= (Int(APP_DEL.selectedCountry.phone_number_limit ?? "") ?? 9) {
                            return true
                        }
                    } else {
                        if proposedText.count <= (Int(APP_DEL.selectedCountry.phone_number_limit ?? "") ?? 9) {
                            return true
                        }
                    }
                }
            }

            return false
        }
        
        return true;
    }
    
    
    
}
