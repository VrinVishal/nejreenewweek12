//
//  naccountloginRegisterCell.swift
//  MageNative Magento Platinum
//
//  Created by cedcoss on 12/10/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class naccountloginRegisterCell: UITableViewCell {

    @IBOutlet weak var signinButton: UIButton!
    @IBOutlet weak var createAccountButton: UIButton!
    
    @IBOutlet weak var lblWelcomeToNejree: UILabel!
    
    @IBOutlet weak var lblCreateNewAccount: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        signinButton.setTitle(APP_LBL().sign_in_c, for: .normal)
       // createAccountButton.setTitle(APP_LBL().create_account.uppercased(), for: .normal)
        lblWelcomeToNejree.text = APP_LBL().welcomeToNejree.uppercased()
        
       // lblCreateNewAccount.text = APP_LBL().create_account.uppercased()
        
        lblCreateNewAccount.attributedText = NSAttributedString(string: String(format: "%@", APP_LBL().create_account.uppercased()), attributes:
                          [.underlineStyle: NSUnderlineStyle.single.rawValue])
    }

   

}
