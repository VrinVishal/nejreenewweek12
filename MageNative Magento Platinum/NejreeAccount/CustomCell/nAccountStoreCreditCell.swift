//
//  nAccountStoreCreditCell.swift
//  MageNative Magento Platinum
//
//  Created by vishaldhamecha on 08/07/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class nAccountStoreCreditCell: UITableViewCell {

    
    @IBOutlet weak var LblWelcomeToNejree: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    
    @IBOutlet weak var lblStoreCredit: UILabel!
    @IBOutlet weak var lblStoreCreditAmount: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        LblWelcomeToNejree.font = UIFont(name: "Cairo-Regular", size: 21)!
        lblUserName.font = UIFont(name: "Cairo-Regular", size: 21)!
        lblStoreCredit.font = UIFont(name: "Cairo-Regular", size: 20)!
        lblStoreCreditAmount.font = UIFont(name: "Cairo-Regular", size: 20)!
                
        LblWelcomeToNejree.text = String(format: "%@,", APP_LBL().welcomeToNejree.uppercased())
        
        lblStoreCredit.text = String(format: "%@", APP_LBL().store_credit.uppercased())
        
               
                
//        lblSignUp.attributedText = NSAttributedString(string: String(format: "%@!", APP_LBL().sign_up.uppercased()), attributes:
//                [.underlineStyle: NSUnderlineStyle.single.rawValue])
//
               
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
