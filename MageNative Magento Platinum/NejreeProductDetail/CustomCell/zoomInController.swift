//
//  zoomInController.swift
//  MageNative Magento Platinum
//
//  Created by MacMini on 31/08/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class zoomInController: MagenativeUIViewController, UIScrollViewDelegate{

    @IBOutlet weak var dismissButton: UIButton!
    @IBOutlet weak var myScroll: UIScrollView!
    @IBOutlet weak var myImage: UIImageView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var IsImagesLaunch : Bool = false
    
    var images = [String]()
    var isSelected = 0 {
        didSet {
            myImage.sd_setImage(with: URL(string: images[isSelected]), placeholderImage: UIImage(named: ""))
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        myScroll.delegate = self
        myScroll.minimumZoomScale = 1
        myScroll.maximumZoomScale = 4
        collectionView.delegate = self
        collectionView.dataSource = self
        
//        myImage.sd_setImage(with: URL(string: images[isSelected]), placeholderImage: UIImage(named: "placeholder"))
        myImage.sd_setImage(with: URL(string: images[isSelected]), placeholderImage: nil)
        
        dismissButton.addTarget(self, action: #selector(dismissTapped(_:)), for: .touchUpInside)
        
        if APP_DEL.selectedLanguage == Arabic {
            collectionView.contentInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 120)
        }else {
            collectionView.contentInset = UIEdgeInsets.init(top: 0, left: 120, bottom: 0, right: 0)
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
            self.IsImagesLaunch = true
        })
        
        
        // Do any additional setup after loading the view.
    }
    

    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return myImage
    }

    @objc func dismissTapped(_ sender: UIButton) {
        
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    
    
    
}

extension zoomInController: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "zoomInCollectionCell", for: indexPath) as! zoomInCollectionCell
//        cell.image.sd_setImage(with: URL(string: images[indexPath.item]), placeholderImage: UIImage(named: "placeholder"))
        
        cell.image.sd_setImage(with: URL(string: images[indexPath.item]), placeholderImage: nil)
        
        cell.image.cardView()
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width/4, height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.isSelected = indexPath.item
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView != myScroll {
            
            if self.IsImagesLaunch{
                let center = self.view.convert(self.collectionView.center, to: self.collectionView)
                if let index = collectionView!.indexPathForItem(at: center) {
                    
                    //self.isSelected = (index.item)
                    if self.isSelected != index.item {
                        self.isSelected = (index.item)
                    }
                }
            }
            
           
        }
    }
    
    
    
}
