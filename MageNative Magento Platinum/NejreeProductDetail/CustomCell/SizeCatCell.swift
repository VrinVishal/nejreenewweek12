//
//  SizeCatCell.swift
//  MageNative Magento Platinum
//
//  Created by Vrinsoft on 17/07/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class SizeCatCell: UITableViewCell {

    
    
    @IBOutlet weak var lblLblTitle: UILabel!
    
    @IBOutlet weak var imgArrow: UIImageView!
    
    @IBOutlet weak var tblView: UITableView!
    
    @IBOutlet weak var constaintTblHeight: NSLayoutConstraint!
    
    @IBOutlet weak var viewTblData: UIView!
    
    var arrSizeChartData = NSMutableArray()
    
    var arrMainData = NSMutableArray()
    
    var cat_id = String()
    
    override func awakeFromNib() {
        super.awakeFromNib()
     
        let nib = UINib(nibName: "SizeCell", bundle: nil)
        self.tblView.register(nib, forCellReuseIdentifier: "SizeCell")
            
        tblView.delegate = self
        tblView.dataSource = self
        
            
        tblView.reloadData()
        self.tblView.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)
        


    }
    
    
          override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?)
          {
              if(keyPath == "contentSize"){
                  if let newvalue = change?[.newKey]
                  {
//                      let newsize  = newvalue as! CGSize
//                      self.constaintTblHeight.constant = newsize.height
                    self.constaintTblHeight.constant = CGFloat(44.5*Double(arrSizeChartData.count))
                    self.contentView.layoutIfNeeded()
                  }
              }
          }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    
    }
    
    
    
}


extension SizeCatCell : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrSizeChartData.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SizeCell", for: indexPath) as! SizeCell
        
        if indexPath.row == 0 {
            
            
            if APP_DEL.selectedLanguage == Arabic {
               
                cell.lblUs.text = "‘CM’"
                cell.lblUk.text = "‘EU’ المقاسات الأوروبيه"
                cell.lblEu.text = "‘UK’ المقاسات البريطانية"
                cell.lblCm.text = "‘US’ المقاسات الامريكية"
                
                cell.lblCm.backgroundColor = UIColor.black
                cell.lblEu.backgroundColor = UIColor.black
                cell.lblUk.backgroundColor = UIColor.black
                cell.lblUs.backgroundColor = UIColor.black
                
                cell.lblUs.textColor = UIColor.init(hexString: "#FBAD18")
                cell.lblEu.textColor = UIColor.white
                cell.lblUk.textColor = UIColor.white
                cell.lblCm.textColor = UIColor.white
                
                cell.contentView.backgroundColor = UIColor.white
                
            } else {
                 
                cell.lblCm.text = "‘CM’"
                cell.lblEu.text = "‘EU’ المقاسات الأوروبيه"
                cell.lblUk.text = "‘UK’ المقاسات البريطانية"
                cell.lblUs.text = "‘US’ المقاسات الامريكية"
                
                cell.lblCm.backgroundColor = UIColor.black
                cell.lblEu.backgroundColor = UIColor.black
                cell.lblUk.backgroundColor = UIColor.black
                cell.lblUs.backgroundColor = UIColor.black
                
                cell.lblCm.textColor = UIColor.init(hexString: "#FBAD18")
                cell.lblEu.textColor = UIColor.white
                cell.lblUk.textColor = UIColor.white
                cell.lblUs.textColor = UIColor.white
                
                cell.contentView.backgroundColor = UIColor.white
            }
            
            
            
            
        }else{
            
            let dictdata = arrSizeChartData[indexPath.row-1] as! NSDictionary

            if APP_DEL.selectedLanguage == Arabic {
            
                if cat_id == "1" {
                    cell.lblUs.text = dictdata["cm"] as? String
                    cell.lblUk.text = dictdata["men_eu"] as? String
                    cell.lblEu.text = dictdata["men_uk"] as? String
                    cell.lblCm.text = dictdata["men_us"] as? String
                }else if cat_id == "2"{
                    cell.lblUs.text = dictdata["cm"] as? String
                    cell.lblUk.text = dictdata["women_eu"] as? String
                    cell.lblEu.text = dictdata["women_uk"] as? String
                    cell.lblCm.text = dictdata["women_us"] as? String
                }else if cat_id == "3" {
                    cell.lblUs.text = dictdata["cm"] as? String
                    cell.lblUk.text = dictdata["kid_eu"] as? String
                    cell.lblEu.text = dictdata["kid_uk"] as? String
                    cell.lblCm.text = dictdata["kid_us"] as? String
                }
                
            } else {
                 
                if cat_id == "1" {
                    cell.lblCm.text = dictdata["cm"] as? String
                    cell.lblEu.text = dictdata["men_eu"] as? String
                    cell.lblUk.text = dictdata["men_uk"] as? String
                    cell.lblUs.text = dictdata["men_us"] as? String
                }else if cat_id == "2"{
                    cell.lblCm.text = dictdata["cm"] as? String
                    cell.lblEu.text = dictdata["women_eu"] as? String
                    cell.lblUk.text = dictdata["women_uk"] as? String
                    cell.lblUs.text = dictdata["women_us"] as? String
                }else if cat_id == "3" {
                    cell.lblCm.text = dictdata["cm"] as? String
                    cell.lblEu.text = dictdata["kid_eu"] as? String
                    cell.lblUk.text = dictdata["kid_uk"] as? String
                    cell.lblUs.text = dictdata["kid_us"] as? String
                }
            }
            
            
            
            cell.contentView.backgroundColor = UIColor.black
            
            cell.lblCm.backgroundColor = UIColor.white
            cell.lblUk.backgroundColor = UIColor.white
            cell.lblUk.backgroundColor = UIColor.white
            cell.lblUs.backgroundColor = UIColor.white
            
            cell.lblCm.textColor = UIColor.black
            cell.lblUk.textColor = UIColor.black
            cell.lblUk.textColor = UIColor.black
            cell.lblUs.textColor = UIColor.black
        }
                            
        cell.selectionStyle = .none
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
    
    
}
