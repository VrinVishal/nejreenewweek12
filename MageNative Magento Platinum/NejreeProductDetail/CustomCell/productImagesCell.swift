//
//  productImagesCell.swift
//  MageNative Magento Platinum
//
//  Created by MacMini on 21/08/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit



protocol setPageDelegate {
    
    func getCurrentpage(with: Int)
    
}



class productImagesCell: UITableViewCell {

    
    @IBOutlet weak var imageCollection: UICollectionView!
    @IBOutlet weak var pageControlView: UIPageControl!

    @IBOutlet weak var btnBack: UIButton!
    
    
    var imagedata = [String]()
    var delegate: setPageDelegate!
    var parent = UIViewController()
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        imageCollection.delegate = self
        imageCollection.dataSource = self
        imageCollection.isPagingEnabled = true
        pageControlView.numberOfPages = imagedata.count
//        pageControlView.currentPage = 0
//        pageControlView.customPageControl(dotFillColor: UIColor.init(hexString: "#fcb215")!, dotBorderColor: UIColor.init(hexString: "#fcb215")!, dotBorderWidth: 1)
        // Initialization code
    }

   
    
}

extension productImagesCell: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imagedata.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imageCollectionCell", for: indexPath) as! imageCollectionCell
//        cell.productImage.sd_setImage(with: URL(string: imagedata[indexPath.row]), placeholderImage: UIImage(named: "placeholder"))
        
        cell.productImage.sd_setImage(with: URL(string: imagedata[indexPath.row]), placeholderImage:nil)
        
        cell.productImage.contentMode = .scaleAspectFit
        //cell.insideView.cardView()
//        print("indexPath is \(indexPath)")
       // delegate.getCurrentpage(with: indexPath.item)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        //return CGSize(width: parent.view.frame.width - 50 , height: collectionView.frame.height)
        return CGSize(width: SCREEN_WIDTH, height: SCREEN_HEIGHT * 0.67)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "zoomInController") as! zoomInController
        vc.images = self.imagedata
        parent.present(vc, animated: true)
    
    }
    
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        self.pageControlView.currentPage = indexPath.section
    }
        
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//    let center = CGPoint(x: scrollView.contentOffset.x + (scrollView.frame.width / 2), y: (scrollView.frame.height / 2))
//        if let ip = imageCollection.indexPathForItem(at: center) {
//        //self.pageControl.currentPage = ip.row
//            print(ip.row)
//            delegate.getCurrentpage(with: ip.row)
//    }
        let scrollPos = scrollView.contentOffset.x / self.frame.width
//        delegate.getCurrentpage(with: Int(scrollPos))//.currentPage = Int(scrollPos)
        self.pageControlView.currentPage = Int(scrollPos)

    }
    
    
    
    
    
}
