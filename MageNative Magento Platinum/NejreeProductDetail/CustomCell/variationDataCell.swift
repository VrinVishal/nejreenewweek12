//
//  variationDataCell.swift
//  MageNative Magento Platinum
//
//  Created by MacMini on 21/08/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit



protocol selectedAttributeDelegate {
    func getSelectedAttribute(from: Int, isSelected: Bool)
}

class variationDataCell: UITableViewCell {

    var variationData = [String]()
    var variationRemainingQty = [String]()
    
    
    
    var delegate: selectedAttributeDelegate!
    
    @IBOutlet weak var variationCollection: UICollectionView!
    @IBOutlet weak var lblSelectASize : UILabel!
    
 
    
    @IBOutlet weak var btnSizeGuide: UIButton!
    @IBOutlet weak var constrantCollectionHeight: NSLayoutConstraint!
    
    @IBOutlet weak var viewSizes: UIView!
    @IBOutlet weak var viewSimiRecom: UIView!
    @IBOutlet weak var btnRecommended: UIButton!
    @IBOutlet weak var btnSimilar: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        variationCollection.delegate = self
        variationCollection.dataSource = self
        
        variationCollection.reloadData()
        contentView.layoutIfNeeded()
        constrantCollectionHeight.constant = variationCollection.contentSize.height
        contentView.layoutIfNeeded()
        variationCollection.reloadData()

        btnSizeGuide.backgroundColor = .white
        btnSizeGuide.layer.borderWidth = 1.0
        btnSizeGuide.layer.borderColor = UIColor.black.cgColor
        btnSizeGuide.setTitleColor(UIColor.black, for: .normal)

        let title = String(format: "%@%@%@", "", APP_LBL().size_chart, "")
        
        btnSizeGuide.setTitle(title, for: .normal)
        
        btnSimilar.setTitle(APP_LBL().similar_products.uppercased(), for: .normal)
        btnRecommended.setTitle(APP_LBL().recommended_products.uppercased(), for: .normal)
        btnSimilar.round(redius: 5.0)
        btnRecommended.round(redius: 5.0)
        
        btnSimilar.titleLabel?.adjustsFontSizeToFitWidth = true
        btnRecommended.titleLabel?.adjustsFontSizeToFitWidth = true
    }
    
   
}

extension variationDataCell: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return variationData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "variationCollectionCell", for: indexPath) as! variationCollectionCell
        cell.insideView.backgroundColor = UIColor.init(hexString: "#FAFAFA")
//        cell.insideView.layer.cornerRadius = 6.0
        cell.insideView.layer.borderWidth = 0.5
        
        print(variationRemainingQty[indexPath.item])
      
        cell.lblRemainingQty.backgroundColor = UIColor.init(hexString: "#e84338")
        
        
        if variationRemainingQty[indexPath.item] != ""{
            cell.lblRemainingQty.isHidden = false
            
            cell.lblRemainingQty.text = variationRemainingQty[indexPath.item] + " " + APP_LBL().qty_left
             cell.lblRemainingQty.backgroundColor = UIColor.init(hexString: "#e84338")
            
            if cell.isSelected {
             
                cell.lblRemainingQty.isHidden = true
            }
            else
             {
                cell.lblRemainingQty.isHidden = false
            }
            
        }
        else
        {
            
            cell.lblRemainingQty.backgroundColor = .clear
            cell.lblRemainingQty.text = ""
            cell.lblRemainingQty.isHidden = true
        }

        cell.sizeLabel.text = variationData[indexPath.item]
        cell.sizeLabel.textColor = UIColor.black
                
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: ((self.variationCollection.frame.size.width/4.0)), height: 45)
    }
    
    func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        
        if let cell = collectionView.cellForItem(at: indexPath) as? variationCollectionCell {
            
            if cell.isSelected {

                collectionView.deselectItem(at: indexPath, animated: true)
                delegate.getSelectedAttribute(from: indexPath.item, isSelected: false)                
                return false
            }
        }
        
        return true
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        delegate.getSelectedAttribute(from: indexPath.item, isSelected: true)
    
    }
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    
    
    
    
    
}
