//
//  nameAndRatingCell.swift
//  MageNative Magento Platinum
//
//  Created by MacMini on 21/08/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class nameAndRatingCell: UITableViewCell {
    
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var productNameDes: UILabel!
    @IBOutlet weak var lblOutOfStock: UILabel!
    @IBOutlet weak var ratingView: FloatRatingView!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var discountPriceLabel: UILabel!

    @IBOutlet weak var btnWish: UIButton!
    @IBOutlet weak var btnShare: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        btnWish.setTitle(" "+APP_LBL().wishlist.uppercased()+" ", for: .normal)
        btnShare.setTitle(" "+APP_LBL().share.uppercased()+" ", for: .normal)
        
        

    }

    
}
