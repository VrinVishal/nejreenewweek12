//
//  SizeCell.swift
//  MageNative Magento Platinum
//
//  Created by Vrinsoft on 17/07/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class SizeCell: UITableViewCell {

    
    @IBOutlet weak var lblCm: UILabel!
    @IBOutlet weak var lblEu: UILabel!
    @IBOutlet weak var lblUk: UILabel!
    @IBOutlet weak var lblUs: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
