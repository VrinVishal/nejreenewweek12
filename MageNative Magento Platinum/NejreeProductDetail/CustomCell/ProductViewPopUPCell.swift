//
//  ProductViewPopUPCell.swift
//  MageNative Magento Platinum
//
//  Created by Vrinsoft on 08/07/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class ProductViewPopUPCell: UITableViewCell {

    
    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var lblQty: UILabel!
    @IBOutlet weak var lblSize: UILabel!
    
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var imgCross: UIImageView!
    
    @IBOutlet weak var lblAmount: UILabel!
    
    @IBOutlet weak var lblOutOfStock: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
