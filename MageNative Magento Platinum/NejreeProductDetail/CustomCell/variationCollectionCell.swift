//
//  variationCollectionCell.swift
//  MageNative Magento Platinum
//
//  Created by MacMini on 21/08/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class variationCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var insideView: UIView!
    @IBOutlet weak var sizeLabel: UILabel!
    @IBOutlet weak var lblRemainingQty : UILabel!
    
    override var isSelected: Bool{
        didSet{
            insideView.backgroundColor = isSelected ? .black: .white
            sizeLabel.textColor = isSelected ? UIColor.init(hexString: "#FBAD18") : .black
            lblRemainingQty.isHidden = isSelected ? false : false
            
        }
    }
    
    
}
