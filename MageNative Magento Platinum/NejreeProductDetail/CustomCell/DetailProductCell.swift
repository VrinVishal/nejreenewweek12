//
//  DetailProductCell.swift
//  MageNative Magento Platinum
//
//  Created by vishal.n on 21/12/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

protocol DidSelectDetailProduct {
    
    func didSelectDetailProduct(type: String, index: Int)
}

class DetailProductCell: UITableViewCell {
    
    @IBOutlet weak var collView: UICollectionView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnViewAll: UIButton!

    var delegateDidSelectDetailProduct: DidSelectDetailProduct?
    
    var arrProducts : [Product_data] = []
    var isDisplayViewAll = false
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        collView.delegate = self
        collView.dataSource = self
        
        collView.register(UINib(nibName: "LayerProductItemCell", bundle: nil), forCellWithReuseIdentifier: "LayerProductItemCell")
        
        collView.keyboardDismissMode = .onDrag
        
        btnViewAll.setTitle(APP_LBL().view_all.uppercased(), for: .normal)
        
        if APP_DEL.selectedLanguage == Arabic {
            lblTitle.textAlignment = .right
        } else {
            lblTitle.textAlignment = .left
        }
        
   
        
    }
    
    func setProductData(product: [Product_data]) {
        
        self.arrProducts = product
        self.collView.reloadData()
        
        if APP_DEL.selectedLanguage == Arabic {
            guard collView.numberOfItems(inSection: 0) > 0 else { return }
            let indexPath = IndexPath(item: 0, section: 0)
            collView.scrollToItem(at: indexPath, at: .left, animated: false)
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func btnViewAllAction(_ sender: Any) {
        
        self.delegateDidSelectDetailProduct?.didSelectDetailProduct(type: self.accessibilityIdentifier ?? "", index: self.arrProducts.count)
    }
}

extension DetailProductCell: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return (isDisplayViewAll ? (arrProducts.count + 1) : (arrProducts.count));
    }
    

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LayerProductItemCell", for: indexPath) as! LayerProductItemCell
        
     //   cell.insideView.productCardView()
        
        if ((isDisplayViewAll == true) && (indexPath.item == arrProducts.count)) {
            
            if APP_DEL.selectedLanguage == Arabic {
                cell.imgViewAll.image = UIImage(named: "icon_view_all_ar")
            } else {
                cell.imgViewAll.image = UIImage(named: "icon_view_all")
            }
                        
            cell.viewAll.isHidden = false
            cell.insideView.isHidden = true
            
        } else {
            
            cell.viewAll.isHidden = true
            cell.insideView.isHidden = false
            
        if (APP_DEL.isDiscountTagEnable == "0"){
            cell.offerLabel.isHidden = true
        }
        else
        {
            let offerString = arrProducts[indexPath.item].offer ?? ""
            
            if offerString != "" {
                
                let offer = " \(offerString)% \(APP_LBL().off.uppercased()) "
                
                cell.offerLabel.text = offer
                cell.offerLabel.isHidden = false
            } else {
                cell.offerLabel.isHidden = true
            }
        }
            
            let tagString = arrProducts[indexPath.item].tag ?? ""
            if tagString != "" {
                
              
                cell.lblTags.text = " " + tagString + " "
                cell.lblTags.isHidden = false
            } else {
                cell.lblTags.isHidden = true
            }
            
            
           // cell.offerLabel.round(redius: 18)
            
            let name = arrProducts[indexPath.item].product_name?.components(separatedBy: "-")
            cell.productName.text = arrProducts[indexPath.item].product_name
            if let names = name {
                
                var newName = names[0]
                if newName.first == " " {
                    newName.removeFirst()
                }
                
                if(names.count>1) {
                    cell.productColor.text = names[1]
                }
            }
            
            cell.productColor.text = ""
            
            let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: arrProducts[indexPath.item].regular_price!)
            attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 1, range: NSMakeRange(0, attributeString.length))
            
            cell.lblBrandName.text = arrProducts[indexPath.item].brands_name
            
            if arrProducts[indexPath.item].special_price != "no_special" {
                
                cell.productPrice.text = arrProducts[indexPath.item].special_price;
                
                cell.regularPrice.attributedText = attributeString
                
                cell.regularPrice.isHidden = false
                cell.regularPrice.alpha = 1
                
            } else {
                
                cell.productPrice.text = arrProducts[indexPath.item].regular_price
                
                cell.regularPrice.isHidden = true
                cell.regularPrice.alpha = 0
            }
            
            //        if let rating = Float(arrProducts[indexPath.item].review!) {
            //
            //            cell.ratingView.rating = rating
            //        }
            
             cell.productImage.image = nil
//            if let downloadURL = SDImageCache.shared.imageFromCache(forKey: arrProducts[indexPath.item].product_image!){
//                cell.productImage!.image = downloadURL
//            } else {
                cell.productImage!.sd_setImage(with: URL(string: arrProducts[indexPath.item].product_image!), placeholderImage: nil)
        //    }
            

        }

        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        self.delegateDidSelectDetailProduct?.didSelectDetailProduct(type: self.accessibilityIdentifier ?? "", index: indexPath.row)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: ((SCREEN_WIDTH / 2) - 10), height: 240.0);
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10.0
    }
}
