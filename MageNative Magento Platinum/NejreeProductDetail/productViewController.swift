//
//  productViewController.swift
//  MageNative Magento Platinum
//
//  Created by MacMini on 21/08/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit
import FirebaseAnalytics
import Firebase
import FirebaseMessaging
import FirebaseDynamicLinks
import Foundation
import FirebaseAuth
import FirebaseCore
import SKPhotoBrowser

var isFromWishList = ""

struct productViewData {
    
    var Inwishlist: String?
    var status: String?
    var product_review: String?
    var short_description: String?
    var main_prod_img: String?
    var currency_symbol: String?
    var stock: String?
    var type: String?
    var product_id: String?
    var product_name: String?
    var product_url: String?
    var review_count: Int?
    var has_review: String?
    var regular_price: String?
    var special_price: String?
    var available: String?
    var review: String?
    var description: String?
    
}

class productViewController: MagenativeUIViewController, UIGestureRecognizerDelegate, DidSelectDetailProduct, SubmitNotify {

    var product_id = ""
    var productData = productViewData()
    var productImageArray = [String]()
    var variationData = [String]()
    var variationKeys = [String]()
    var variationRemainingQty = [String]()
    
    
    var currentPage = 0
    var selectedVariation = String()
    //    var attributeOptionId = [String:String]()
    var attributeCode = String()
    var maxLines = Int()
    var isSelected = false
    var variationPrice = [[String:String]]()
    var isSelectWishlist : Bool = false
    var mainvc: UIViewController?
    var isFirstTime = true
    var isDummyFirstTime = false
    var products = [CartProduct]()
    var total = [String:String]()
    
    var selectedAddressId = String()
    var selectedAddress = [String:String]()
    var strShareURl = String()
    
    var orderId = ""
    var paymentMethod = ""
    var strProductType = ""
    
    var strProductQty = "1"
    var kidsSize = ""
    var apparelImage = ""
    
    var genderID = ""
    var isApplePayEnabled = String()
    var strPublicKey = ""
    var strSecretKey = ""
    var AmountFinal : Double!
    var strCurrentCheckoutMode = ""
    var productsData = [CartProduct]()
    
    var isOutOfStock = false
    
    @IBOutlet weak var productTable: UITableView!
    @IBOutlet weak var btnApplyStoreCredit: UIButton!
    @IBOutlet weak var viewPopUpBG: UIView!
    
    
    @IBOutlet weak var viewAddress: UIView!
    
    @IBOutlet weak var viewPopUpProduct: UIView!
    
    @IBOutlet weak var viewBottom: UIView!
    
    @IBOutlet weak var viewTxtCode: UIView!
    @IBOutlet weak var txtCode: UITextField!
    @IBOutlet weak var applyPromoButton: UIButton!
    @IBOutlet weak var btnUseStoreCredit: UIButton!
    @IBOutlet weak var tblProductList: UITableView!
    
    @IBOutlet weak var btnProccedToCheckout: UIButton!
    
    
    @IBOutlet weak var lblSubTotalHeading: UILabel!
    @IBOutlet weak var lblSubTotalValue: UILabel!
    
    
    @IBOutlet weak var lblDiscountHeading: UILabel!
    @IBOutlet weak var lblDiscountValue: UILabel!
    
    @IBOutlet weak var lblCountryName: UILabel!
    @IBOutlet weak var lblCityName: UILabel!
    @IBOutlet weak var lblHoodName: UILabel!
    @IBOutlet weak var lblMobileNumber: UILabel!
    @IBOutlet weak var lblName: UILabel!
    
    
    @IBOutlet weak var lblAddAddress: UILabel!
    @IBOutlet weak var viewAddressData: UIView!
    
    
    @IBOutlet weak var btnAddNewAddress: UIButton!
    @IBOutlet weak var btnEditAddress: UIButton!
    
    
    @IBOutlet weak var lblStoreCreditHeading: UILabel!
    @IBOutlet weak var lblStoreCreditValue: UILabel!
    @IBOutlet weak var lblUserStoreCredit: UILabel!
    @IBOutlet weak var imgStoreCredit: UIImageView!
    
    
    
    @IBOutlet weak var viewAddedCard: UIView!
    @IBOutlet weak var constraintTopViewAddedControl: NSLayoutConstraint!
    @IBOutlet weak var lblAddedCart: UILabel!
    
    @IBOutlet weak var viewProductImg: UIView!
    @IBOutlet weak var imgProduct: UIImageView!
    
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var lblQty: UILabel!
    @IBOutlet weak var lblSize: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var btnViewCart: UIButton!
    
    @IBOutlet weak var addToCartButton: UIButton!
    
    @IBOutlet weak var ApplePayButton: UIButton!
        
    @IBOutlet weak var btnNotifyMe: UIButton!

    
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var constraintViewBottom: NSLayoutConstraint!
    
    
    var brandName = String()
    
    var isStoreCreditApplied : Bool = false
    var appliedStoreCreditString : String = ""
    var storeId = String()
    
    var tempViewCartJson = JSON()
    
    var isFreeOrderUsingStoreCredit = false
    var availableStoreCreditInt : Int = 0
    
    let identiSimilar = "similar"
    var tagSimilar = ""
    var limitSimilar = 0
    var arrSimilar : [Product_data] = []
    
    let identiRelated = "related"
    var tagRelated = ""
    var limitRelated = 0
    var arrRelated : [Product_data] = []

    
    
    
    
    //MARK: View Controller Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.productTable.register(UINib(nibName: "DetailProductCell", bundle: nil), forCellReuseIdentifier: "DetailProductCell")
        //DetailProductCell
        
        APP_DEL.strDeepLinkData = ""
        self.AddedCartPopupData()
        
        self.setPopUpData()
        
        product_id = APP_DEL.productIDglobal
        
        guard let storeId = defaults.value(forKey: "storeId") as? String else {return}
        
        self.getProductDetail()
        
        self.paymentMethod = "checkoutcom_apple_pay"
        
        isFromWishList = ""
        
        productTable.separatorStyle = .none
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        addToCartButton.isHidden = true
        ApplePayButton.isHidden = true
        btnNotifyMe.superview?.superview?.isHidden = true
        btnNotifyMe.round(redius: 0.0)
        btnNotifyMe.setTitle(APP_LBL().notify_me.uppercased(), for: .normal)

        btnBack.addTarget(self, action: #selector(ActionBtnBack(_:)), for: .touchUpInside)
        
        if APP_DEL.selectedLanguage == Arabic {
            
            btnBack.setImage(UIImage(named: "icon_back_white_Arabic"), for: .normal)
            
        } else {
            
            btnBack.setImage(UIImage(named: "icon_back_white_round"), for: .normal)
        }
    }
    
    var lastContentOffset: CGFloat = 0
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.lastContentOffset = scrollView.contentOffset.y
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        UIView.transition(with: btnBack, duration: 0.4,
            options: .transitionCrossDissolve,
            animations: {
                
            self.btnBack.isHidden = false
        })
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if self.lastContentOffset < scrollView.contentOffset.y {
            // did move up
            
            UIView.transition(with: btnBack, duration: 0.4, options: .transitionCrossDissolve, animations: {
                                                                
                self.btnBack.isHidden = true
            })
            
            self.hideTabBar()
            
        } else if self.lastContentOffset > scrollView.contentOffset.y {
            // did move down
            
            UIView.transition(with: btnBack, duration: 0.4, options: .transitionCrossDissolve, animations: {
                                
                self.btnBack.isHidden = false
            })
            
            self.showTabBar()

        } else {
            // didn't move
            
        }
    }
    
    func submitNotify(res: Bool) {
        
        if res {
            
            sendNotify()
        }
    }
    
    func sendNotify() {
        
        var postData = [String:String]()
        
        if let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] {
            
            postData["hashkey"] = userInfoDict["hashKey"]!
            postData["customer_id"] = userInfoDict["customerId"]!;
        } else {
            postData["customer_id"] = "0";
        }
        postData["product_id"] = self.product_id;
        postData["child_product_id"] = "";
        
        if defaults.bool(forKey: "isLogin") {
            postData["email"] = (UserDefaults.standard.value(forKey: "EmailId") as? String  ?? "")
            postData["po_number"] = (UserDefaults.standard.value(forKey: "mobile_number") as? String  ?? "")
        }
        postData["message"] = "";
        
        
        if let store_id = self.defaults.object(forKey: "storeId") as? String {
            postData["store_id"] = store_id;
        }
        
//        if let cart_id = self.defaults.object(forKey: "cartId") as? String {
//            postData["cart_id"] = cart_id;
//        }
        postData["country_id"] = APP_DEL.selectedCountry.country_code ?? "SA"

        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader_withlockedbackground(me: self);
        
        API().callAPI(endPoint: "mobiconnect/checkout/notify", method: .POST, param: postData) { (json_res, err) in
            
            DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                if err == nil {
                    
                    if json_res[0]["success"].stringValue == "true" {
                        
                        APP_DEL.showAlertWithOkButton(parentVC: self, strTitle: APP_LBL().success, strMsg: APP_LBL().thank_you_for_connecting_with_us_we_will_contact_you_soon)
                        
                    } else {
                        
                        APP_DEL.showAlertWithOkButton(parentVC: self, strTitle: APP_LBL().success, strMsg: APP_LBL().notify_user_submited_form_already)
                    }
                }
            }
        }
    }
    
    @IBAction func btnNotifyMeAction(_ sender: UIButton) {
        
        let vc = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "NotifyPopupVC") as! NotifyPopupVC
        
        if defaults.bool(forKey: "isLogin") {
            vc.email = (UserDefaults.standard.value(forKey: "EmailId") as? String  ?? "")
            vc.mobile = (UserDefaults.standard.value(forKey: "mobile_number") as? String  ?? "")
        }
        
        vc.delagateSubmitNotify = self
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        self.present(vc, animated: true, completion: nil)
    }
    
    var isTabbarHidden = true
    
    func hideTabBar() {
        var frame = self.tabBarController?.tabBar.frame
        frame!.origin.y = self.view.frame.size.height + (frame?.size.height)!

        UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.transitionCrossDissolve, animations: {
            
            self.tabBarController?.tabBar.frame = frame!
            self.tabBarController?.tabBar.isHidden = true
            self.isTabbarHidden = true
            
            self.constraintViewBottom.constant = 0.0
            
            self.view.layoutIfNeeded()
            
        }) { (res) in }
    }

    func showTabBar() {
        var frame = self.tabBarController?.tabBar.frame
        frame!.origin.y = self.view.frame.size.height - (frame?.size.height)!

        UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.transitionCrossDissolve, animations: {
            
            self.tabBarController?.tabBar.frame = frame!
            self.tabBarController?.tabBar.isHidden = false
            self.isTabbarHidden = false
            
            self.constraintViewBottom.constant = 44.0
            
            self.view.layoutIfNeeded()
            
        }) { (res) in }
    }
    
    @IBAction func actionBtnViewCart(_ sender: UIButton) {
        
         self.tabBarController?.selectedIndex = 1
        self.navigationController?.popViewController(animated: true)
    }
    
    func AddedCartPopupData() {
        
        viewAddedCard.cardView()
        constraintTopViewAddedControl.constant = -300
        viewProductImg.cardView()
        
        lblAddedCart.text = APP_LBL().added_to_cart.uppercased()
        btnViewCart.setTitle(APP_LBL().view_cart.uppercased(), for: .normal)
    }
  
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        if navigationController?.responds(to: #selector(getter: UINavigationController.interactivePopGestureRecognizer)) ?? false {
            
            self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
            self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        }
        
        if self.isTabbarHidden {
            
            self.tabBarController?.tabBar.isHidden = true
            self.constraintViewBottom.constant = 0.0
        }
        
    }
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        
        return true
    }
    
    func getTagJsonString(tagString: String) -> String {
        
        var dict : [String:String] = [:]
        
        if tagString != "" {
            
            for temp in (tagString.components(separatedBy: ",")) {
                
                let temp_2 = temp.components(separatedBy: "/")
                
                if temp_2.count > 1 {
                    dict[temp_2[0]] = temp_2[1]
                }
            }
        }
        
        if dict == [:] {
            
            return ""
            
        } else {
            
            let data = try! JSONSerialization.data(withJSONObject: ["tag":dict], options: [])
            let jsonSTR = String(data: data, encoding: .utf8)!;
            
            return jsonSTR
        }
    }

    func didSelectDetailProduct(type: String, index: Int) {
        
        if type == self.identiSimilar {
            
            if arrSimilar.count > index {
                
                let vc = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "productViewController") as! productViewController
                vc.product_id = arrSimilar[index].product_id ?? ""
                APP_DEL.productIDglobal = arrSimilar[index].product_id ?? ""
                vc.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(vc, animated: true)
                
            } else {
                
                if let viewController =  UIStoryboard(name: "categorylayouts", bundle: nil).instantiateViewController(withIdentifier: "cedMageDefaultCollection") as?  cedMageDefaultCollection {
                    
                    UserDefaults.standard.set(tagSimilar, forKey: "filtersToSend");
                    viewController.strTitleLayer = APP_LBL().similar_products.uppercased()
                    //viewController.strTitleLayer = layer.layer_name ?? ""
                    //viewController.searchString = layer.layer_data?[index.row].tags ?? ""
                    viewController.hidesBottomBarWhenPushed = true
                    viewController.isFilterDisplay = false
                    viewController.selectedCategory = "" //APP_DEL.isFromHomeBannerID
                    self.navigationController?.pushViewController(viewController, animated: true)
                }
            }
            
        } else if type == self.identiRelated {
            
            if arrRelated.count > index {
                
                let vc = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "productViewController") as! productViewController
                vc.product_id = arrRelated[index].product_id ?? ""
                APP_DEL.productIDglobal = arrRelated[index].product_id ?? ""
                vc.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(vc, animated: true)
                
            } else {
                
                if let viewController =  UIStoryboard(name: "categorylayouts", bundle: nil).instantiateViewController(withIdentifier: "cedMageDefaultCollection") as?  cedMageDefaultCollection {
                    
                    UserDefaults.standard.set(tagRelated, forKey: "filtersToSend");
                    
                    viewController.strTitleLayer = APP_LBL().recommended_products.uppercased()
                    //viewController.searchString = layer.layer_data?[index.row].tags ?? ""
                    viewController.hidesBottomBarWhenPushed = true
                    viewController.isFilterDisplay = false
                    viewController.selectedCategory = "" //APP_DEL.isFromHomeBannerID
                    self.navigationController?.pushViewController(viewController, animated: true)
                }
            }
        }
    }
    
    func setPopUpData() {
        
        viewAddress.cardView()
        
        viewPopUpBG.isHidden = true
        
        
        btnApplyStoreCredit.isSelected = false
        lblUserStoreCredit.font  = UIFont(name: "Cairo-Regular", size: 14)!
        self.appliedStoreCreditString = "\(APP_DEL.selectedCountry.getCurrencySymbol().uppercased()) 0"
        lblSubTotalHeading.text = APP_LBL().subtotal.uppercased()
        lblDiscountHeading.text = APP_LBL().discount.uppercased()
        lblStoreCreditHeading.text = APP_LBL().store_credit.uppercased()
        
        lblSubTotalHeading.font  = UIFont(name: "Cairo-Regular", size: 13)!
        lblDiscountHeading.font  = UIFont(name: "Cairo-Regular", size: 13)!
        lblStoreCreditHeading.font  = UIFont(name: "Cairo-Regular", size: 13)!
        
        
        lblSubTotalValue.font  = UIFont(name: "Cairo-Regular", size: 13)!
        lblDiscountValue.font  = UIFont(name: "Cairo-Regular", size: 13)!
        lblStoreCreditValue.font  = UIFont(name: "Cairo-Regular", size: 13)!
        
        applyPromoButton.titleLabel?.font = UIFont(name: "Cairo-Regular", size: 14)!
        
        
        viewTxtCode.setBorder()
        viewTxtCode.setCornerRadius()
        
        btnProccedToCheckout.titleLabel?.font = UIFont(name: "Cairo-Regular", size: 16)!
        btnProccedToCheckout.titleLabel?.textColor = UIColor.white
        
        
        //btnProccedToCheckout.setCornerRadius()
        btnProccedToCheckout.setBorder()
        
        let nib = UINib(nibName: "ProductViewPopUPCell", bundle: nil)
        tblProductList.register(nib, forCellReuseIdentifier: "ProductViewPopUPCell")
        
        tblProductList.delegate = self
        tblProductList.dataSource = self
        tblProductList.reloadData()
        
        self.generateDynamicLink()
        
    }
    
    @objc func btnSimilarProdAction(_ sender: UIButton) {
        
        scrolBottom()
    }
    
    @objc func btnRecommendedProdAction(_ sender: UIButton) {
        
        scrolBottom()
    }
    
    func scrolBottom() {
        
        let scrollPoint = CGPoint(x: 0, y: self.productTable.contentSize.height - self.productTable.frame.size.height)
        self.productTable.setContentOffset(scrollPoint, animated: true)
    }
     
    //MARK: Action Button Add To Cart
    @IBAction func addToCartTapped(_ sender: UIButton) {
        
        if productData.stock == "OUT OF STOCK" {
            
            self.view.makeToast(APP_LBL().this_product_is_out_of_stock, duration: 1.0, position: .center)
            return
        }
        
        if isSelected == false {
            
            let msg = APP_LBL().please_select_product_size
            self.view.makeToast(msg, duration: 2, position: .center, title: nil, image: nil, style: nil, completion: {
                Void in
                
            })
            
            return;
        }
        
        var super_attribute = "{";
        super_attribute += "\""+attributeCode+"\":";
        super_attribute += "\""+selectedVariation+"\",";
        
        if (super_attribute.last! == ",") {
            
            super_attribute = super_attribute.substring(to: super_attribute.index(before: super_attribute.endIndex));
        }
        super_attribute += "}";
        addToCartButton.isUserInteractionEnabled = false
        self.addToCartONLY(dataToPost: ["type":self.productData.type!, "product_id":self.productData.product_id!, "super_attribute":super_attribute, "qty":"1","store_id":(UserDefaults.standard.value(forKey: "storeId") as! String?)!])
    }
    
    //MARK: PRODUCT DETAIL
    func getProductDetail() {
        
        var postData = [String:String]()

        if let storeID =  UserDefaults.standard.value(forKey: "storeId") as? String {
            postData["store_id"] = storeID
        }
        
        if let userInfoDict = UserDefaults.standard.object(forKey: "userInfoDict") as? [String: String] {
            
            postData["hashkey"] = userInfoDict["hashKey"]
            postData["customer_id"] = userInfoDict["customerId"]
        }
        
        if let genderID =  UserDefaults.standard.object(forKey: KEY_GENDER) as? String {
            postData["gender_id"] = genderID
        }
        
        postData["country_id"] = APP_DEL.selectedCountry.country_code ?? "SA"
        
        postData["prodID"] = product_id
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);
        
        //https://dev05.nejree.com/rest/V1/mobiconnect/catalog/productview
        
        
        API().callAPI(endPoint: "mobiconnect/catalog/productview/", method: .POST, param: postData) { (json_res, err) in
            
        //    DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                if err == nil {
                    
                    let json = json_res[0]
                    
                    var i = 1;
                    for image in json["data"]["product-image"].arrayValue {
                        
                        self.productImageArray.append(image["image\(i)"][0].stringValue);
                        i = i + 1;
                    }
                    
                    for data in json["data"]["config-data"]["attributes"][0]["options"].arrayValue {
                        
                        self.variationData.append(data["label"].stringValue)
                        self.variationKeys.append(data["id"].stringValue)
                        self.variationRemainingQty.append(data["remain_qty"].stringValue)
                    }
                    
                    for (key,value) in json["data"]["config-data"]["index"] {
                        
                        let k = key
                        let kk = k.components(separatedBy: "#")
                        let actualKey = kk[1]
                        var variationPriceData = [String:String]()
                        variationPriceData["key"] = actualKey
                        variationPriceData["finalPrice"] = value["price"]["finalPrice"][0].stringValue
                        variationPriceData["oldPrice"] = value["price"]["oldPrice"][0].stringValue
                        self.variationPrice.append(variationPriceData)
                    }
                    
                    self.attributeCode = json["data"]["config-data"]["attributes"][0]["id"].stringValue
                    self.brandName = json["data"]["brand_name"].stringValue
                    self.genderID = json["data"]["genderId"][0].stringValue
                    self.kidsSize = json["data"]["kidSizeImage"].stringValue
                    
                    self.apparelImage = json["data"]["size_chart_img"].stringValue
                    
                    do {
                        
                        self.limitSimilar = json["data"]["similar_products"]["limit"].intValue
                        self.tagSimilar = json["data"]["similar_products"]["filter"].stringValue
                        let similarData = try json["data"]["similar_products"]["products"].rawData()
                        self.arrSimilar = try JSONDecoder().decode([Product_data].self, from: similarData)
                        
                        self.limitRelated = json["data"]["recommended_products"]["limit"].intValue
                        self.tagRelated = json["data"]["recommended_products"]["filter"].stringValue
                        let relatedData = try json["data"]["recommended_products"]["products"].rawData()
                        self.arrRelated = try JSONDecoder().decode([Product_data].self, from: relatedData)
                        
                    } catch {
                        
                    }
                    
                    
                    
                    
                    let CartData = ["ITEM_CATEGORY" : "",
                                    "ITEM_ID" : json["data"]["product-id"][0].stringValue,
                                    "ITEM_LOCATION_ID" : "",
                                    "ITEM_NAME" : json["data"]["product-name"][0].stringValue,
                                    "PRICE" : json["data"]["price"][0]["regular_price"][0].stringValue,
                                    "QUANTITY" : "1",
                                    "VALUE" : json["data"]["price"][0]["regular_price"][0].stringValue
                    ]
                    Analytics.logEvent("add_to_cart", parameters: CartData)
                    
                   
                    
                    self.isApplePayEnabled = json["data"]["apple_pay_enable"].stringValue;
                    
                    self.strProductType = json["data"]["product_type"][0].stringValue
                    
                    self.productData = productViewData.init(Inwishlist: json["data"]["Inwishlist"][0].stringValue,
                                                            status: json["data"]["status"].stringValue,
                                                            product_review: json["data"]["product_review"].stringValue,
                                                            short_description: json["data"]["short-description"][0].stringValue,
                                                            main_prod_img: json["data"]["main-prod-img"].stringValue,
                                                            currency_symbol: json["data"]["currency_symbol"].stringValue,
                                                            stock: json["data"]["stock"][0].stringValue,
                                                            type: json["data"]["type"][0].stringValue,
                                                            product_id: json["data"]["product-id"][0].stringValue,
                                                            product_name: json["data"]["product-name"][0].stringValue,
                                                            product_url: json["data"]["product-url"][0].stringValue,
                                                            review_count: json["data"]["review_count"][0].intValue,
                                                            has_review: json["data"]["has_review"].stringValue,
                                                            regular_price: json["data"]["price"][0]["regular_price"][0].stringValue,
                                                            special_price: json["data"]["price"][0]["special_price"][0].stringValue,
                                                            available: json["data"]["available"].stringValue,
                                                            review: json["data"]["review"][0].stringValue,
                                                            description: json["data"]["description"][0].stringValue)
                    
                    
                    var productPrice = ""
                    if self.productData.special_price == "no_special" || self.productData.special_price == "" || self.productData.special_price == self.productData.regular_price {
                        
                        productPrice = self.productData.regular_price!
                        
                    } else {
                        
                        productPrice = self.productData.special_price!
                    }
                    
                    let detail : [AnalyticKey:String] = [
                        .ItemCategory : "",
                        .ItemID : json["data"]["product-id"][0].stringValue,
                        .ItemLocationID : "",
                        .ItemName : json["data"]["product-name"][0].stringValue,
                        .Price : json["data"]["price"][0]["regular_price"][0].stringValue,
                        .Quantity : "1",
                        .Value : json["data"]["price"][0]["regular_price"][0].stringValue,
                        .PriceWithoutCurrency : productPrice
                    ]
                    API().setEvent(eventName: .AddToCart, eventDetail: detail) { (json, err) in }
                    
//                    var productPrice = ""
//                    if self.productData.special_price == "no_special" || self.productData.special_price == "" || self.productData.special_price == self.productData.regular_price {
//                        
//                        productPrice = self.productData.regular_price!
//                        
//                    } else {
//                        
//                        productPrice = self.productData.special_price!
//                    }
                    
                    Analytics.logEvent(AnalyticsEventViewItem, parameters: [
                        AnalyticsParameterCurrency : APP_DEL.selectedCountry.getCurrencySymbol().uppercased(),
                        AnalyticsParameterItemID : self.product_id,
                        AnalyticsParameterValue : productPrice
                    ])
                    
                    let detail_1 : [AnalyticKey:String] = [
                        .Currency : APP_DEL.selectedCountry.getCurrencySymbol().uppercased(),
                        .ItemID : self.product_id,
                        .Value : productPrice,
                        .PriceWithoutCurrency : productPrice
                    ]
                    API().setEvent(eventName: .ViewItem, eventDetail: detail_1) { (json, err) in }
                    
                    if ((self.productData.Inwishlist ?? "") == "IN") {
                        self.isSelectWishlist = true
                    } else {
                        self.isSelectWishlist = false
                    }
                    
                    //self.isOutOfStock = (self.productData.stock == "OUT OF STOCK")
                    if self.variationData.count > 0{
                        self.isOutOfStock = false
                    }
                    else
                    {
                        self.isOutOfStock = true
                    }
                    
                    if self.isOutOfStock {
                       
                        self.btnNotifyMe.superview?.superview?.isHidden = false
                        self.ApplePayButton.superview?.superview?.isHidden = true
                    } else {
                        self.btnNotifyMe.superview?.superview?.isHidden = true
                        self.ApplePayButton.superview?.superview?.isHidden = false
                    }
                    
                    self.productTable.delegate = self
                    self.productTable.dataSource = self
                    self.productTable.reloadData()
                }
          //  }
        }
    }
    
    //MARK: ADD TO CART ONLY
    func addToCartONLY(dataToPost:[String:String]) {
        
        var postData = [String:String]()
        
        for (key,val) in dataToPost {
            
            postData[key] = val;
        }
        
        
        if defaults.bool(forKey: "isLogin") {
            
            if let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] {
                
                postData["hashkey"] = userInfoDict["hashKey"]
                postData["customer_id"] = userInfoDict["customerId"]
            }
        }
        
        if (defaults.object(forKey: "cartId") != nil) {
            
            let cart_id = defaults.object(forKey: "cartId") as! String;
            postData["cart_id"] = cart_id;
        }
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);
        
        API().callAPI(endPoint: "mobiconnect/checkout/add/", method: .POST, param: postData) { (json_res, err) in
            
         //   DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                if err == nil {
                    
                    let msg = json_res[0]["cart_id"]["message"].stringValue;
                    
                    if msg != "" {
                        
                        if (json_res[0]["cart_id"]["success"].stringValue == "true") {
                            
                            let name = self.productData.product_name?.components(separatedBy: "-")
                            
                            let msgEng = "\(name![0]) \(APP_LBL().has_been_added_to_the_cart)"
                            
                            self.view.makeToast(msgEng, duration: 1.0, position: .center)
                            
                            self.strProductQty = json_res[0]["cart_id"]["totalQty"].stringValue
//                            self.lblQty.text = json_res[0]["cart_id"]["totalQty"].stringValue
                            var productPrice = ""
                            if self.productData.special_price == "no_special" || self.productData.special_price == "" || self.productData.special_price == self.productData.regular_price {
                                
                                productPrice = self.productData.regular_price!
                                
                            } else {
                                
                                productPrice = self.productData.special_price!
                            }
                            
                            Analytics.logEvent(AnalyticsEventAddToCart, parameters: [
                                AnalyticsParameterCurrency : APP_DEL.selectedCountry.getCurrencySymbol().uppercased(),
                                AnalyticsParameterItemCategory : "Shoes",
                                AnalyticsParameterItemID : self.product_id,
                                AnalyticsParameterQuantity : "1",
                                AnalyticsParameterItemName : self.productData.product_name ?? "",
                                AnalyticsParameterPrice : self.productData.special_price ?? "",
                            ])
                            
                            let detail_1 : [AnalyticKey:String] = [
                                .Currency : APP_DEL.selectedCountry.getCurrencySymbol().uppercased(),
                                .ItemCategory : "Shoes",
                                .ItemID : self.product_id,
                                .Quantity : "1",
                                .ItemName : self.productData.product_name ?? "",
                                .Price : self.productData.special_price ?? "",
                                .PriceWithoutCurrency : productPrice
                            ]
                            API().setEvent(eventName: .AddToCart, eventDetail: detail_1) { (json, err) in }
                            
                            
                            self.isSelected = false
                            self.addToCartButton.backgroundColor = UIColor.lightGray
                            
                            self.productTable.reloadData()
                            
                            UIView.animate(withDuration: Double(0.5), animations: {
                                self.constraintTopViewAddedControl.constant = 0
                                self.view.layoutIfNeeded()
                                
                                
                                DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) { // Change `2.0`
                                    UIView.animate(withDuration: Double(0.5), animations: {
                                        self.constraintTopViewAddedControl.constant = -300
                                        
                                        self.view.layoutIfNeeded()
                                    })
                                }
                            })
                            
                        } else {
                            
                            if msg.contains("limit"){
                                APP_DEL.showAlertWithOkButton(parentVC: self, strTitle: APP_LBL().error, strMsg: APP_LBL().you_can_not_add_more_than_5_quantity)
                            }
                            else{
                                
                                let name = self.productData.product_name?.components(separatedBy: "-")
                                
                                APP_DEL.showAlertWithOkButton(parentVC: self, strTitle: APP_LBL().error, strMsg: "\(APP_LBL().quantity_of) \(name![0]) \(APP_LBL().not_available)")
                                
                              
                               // self.view.makeToast("\(APP_LBL().quantity_of) \(name![0]) \(APP_LBL().not_available)", duration: 1.0, position: .center)
                            }
                            
                           
                        }
                    }
                    
                    if (json_res[0]["cart_id"]["success"].stringValue == "true") {
                        
                        self.defaults.set(json_res[0]["cart_id"]["cart_id"].stringValue, forKey: "cartId");
                        self.defaults.set(json_res[0]["cart_id"]["items_count"].stringValue, forKey: "items_count");
                        
                        self.setcartCount()
                        
                    } else {
                        
                        let msg = json_res[0]["message"].stringValue;
                        
                        if msg != "" {
                            self.view.makeToast(msg, duration: 1.0, position: .center)
                        }
                    }
                }
            self.addToCartButton.isUserInteractionEnabled = true
           // }
        }
    }
    
    //MARK: APPLE PAY
    @IBAction func ProccedWithApplePay(_ sender: UIButton) {
        
        if defaults.bool(forKey: "isLogin") {
            
            if productData.stock == "OUT OF STOCK" {
                self.view.makeToast(APP_LBL().this_product_is_out_of_stock, duration: 1.0, position: .center)
                return
            }
            
            if isSelected == false {
                
                let msg = APP_LBL().please_select_product_size
                self.view.makeToast(msg, duration: 2, position: .center, title: nil, image: nil, style: nil, completion: {
                    Void in
                    
                })
                return;
            }
            
            var super_attribute = "{";
            super_attribute += "\""+attributeCode+"\":";
            super_attribute += "\""+selectedVariation+"\",";
            
            if (super_attribute.last! == ",") {
                
                super_attribute = super_attribute.substring(to: super_attribute.index(before: super_attribute.endIndex));
            }
            super_attribute += "}";
            
            
            var params = Dictionary<String,String>()
            
            params["type"] = self.productData.type!;
            params["product_id"] = self.productData.product_id!
            params["super_attribute"] = super_attribute
            params["qty"] = "1"
            params["store_id"] = (UserDefaults.standard.value(forKey: "storeId") as! String?)!
            
            cedMageLoaders.removeLoadingIndicator(me: self);
            cedMageLoaders.addDefaultLoader(me: self);
            
            API().callAPI(endPoint: "mobiconnect/checkout/add/", method: .POST, param: params) { (json_res, err) in
                
               // DispatchQueue.main.async {
                    
                    cedMageLoaders.removeLoadingIndicator(me: self);
                    
                    if err == nil {
                        
                        let json_0 = json_res[0]
                        
                        let msg = json_res[0]["cart_id"]["message"].stringValue;
                        
                        if (json_0["cart_id"]["success"].stringValue == "true") {
                            
                            self.defaults.set(json_0["cart_id"]["cart_id"].stringValue, forKey: "cartId");
                            self.defaults.set(json_0["cart_id"]["items_count"].stringValue, forKey: "items_count");
                            
                            self.setcartCount()
                            
                            self.getCartList(isFromApplePay: true)
                            
                        } else {
                            
                            if msg.contains("limit"){
                                APP_DEL.showAlertWithOkButton(parentVC: self, strTitle: APP_LBL().error, strMsg: APP_LBL().you_can_not_add_more_than_5_quantity)
                            }
                            else{
                                let name = self.productData.product_name?.components(separatedBy: "-")
                                self.view.makeToast("\(APP_LBL().quantity_of) \(name![0]) \(APP_LBL().not_available)", duration: 1.0, position: .center)
                            }
                        }
                    }
              //  }
            }
            
        } else {
            
            isFromWishList = self.product_id
            
            if let vc = UIStoryboard(name: "cedMageLogin", bundle: nil).instantiateViewController(withIdentifier: "nejreeLoginController") as? nejreeLoginController{
                
                APP_DEL.isLoginFromWishList = false
                vc.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }

    //MARK: VIEW CART
    func getCartList(isFromApplePay: Bool) {
        
        var params = Dictionary<String,String>()
        
        guard let storeId = defaults.value(forKey: "storeId") as? String else {
            return;
        }
        self.storeId = storeId
        
        var cartId = defaults.object(forKey: "cartId")
        
        params["store_id"] = storeId
        
        if (defaults.object(forKey: "cartId") != nil) {
            
            cartId = (defaults.object(forKey: "cartId") as? String)!;
            
            params["cart_id"] = cartId as? String;
            
            
            if (defaults.object(forKey: "userInfoDict") != nil) {
                
                userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
            }
            
            if defaults.bool(forKey: "isLogin") {
                
                if let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] {
                    
                    params["hashkey"] = userInfoDict["hashKey"]
                    params["customer_id"] = userInfoDict["customerId"]
                }
            }
            
        } else {
            
            params["customer_id"] = "0"
        }
        
        params["country_id"] = APP_DEL.selectedCountry.country_code ?? "SA"
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);
        
        API().callAPI(endPoint: "mobiconnect/checkout/viewcart", method: .POST, param: params) { (json_res, err) in
            
         //   DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                if err == nil {
                    
                    let json_0 = json_res[0]
                    
                    self.products.removeAll()
                    
                    self.tempViewCartJson = json_0
                    
                    if (json_0["success"].stringValue.lowercased() == "false".lowercased()) {
                        
                        self.viewPopUpBG.isHidden = true
                        self.defaults.removeObject(forKey: "cartId")
                        return
                    }
                    
                    
                    do {
                        
                        let catData = try json_0["data"]["products"].rawData()
                        self.products = try JSONDecoder().decode([CartProduct].self, from: catData)
                        
                    } catch {
                        
                    }
                    
                    
                    
                    self.productsData = self.products
                    
                    self.total.removeAll()
                    
                    self.total["amounttopay"] = json_0["data"]["total"][0]["amounttopay"].stringValue
                    self.total["shipping_amount"] = json_0["data"]["total"][0]["shipping_amount"].stringValue
                    self.total["discount_amount"] = json_0["data"]["total"][0]["discount_amount"].stringValue
                    self.total["tax_amount"] = json_0["data"]["total"][0]["tax_amount"].stringValue
                    self.total["grandtotal"] = json_0["data"]["grandtotal"].stringValue
                    self.total["grandtotal_without_currency"] = json_0["data"]["grandtotal_without_currency"].stringValue
                    self.total["coupon"] = json_0["data"]["coupon"].stringValue
                    self.total["is_discount"] = json_0["data"]["is_discount"].stringValue

                    self.btnProccedToCheckout.setTitle(" Pay" +  "\(self.total["grandtotal"]!)", for: .normal)
                    self.btnProccedToCheckout.titleLabel?.font = UIFont(name: "Cairo-Regular", size: 17)!
                    self.lblSubTotalValue.text = self.total["amounttopay"]
                    self.lblDiscountValue.text = self.total["discount_amount"]!
                    
                    if (json_0["data"]["store_credit"].stringValue == "") {
                        
                        self.lblUserStoreCredit.text = String(format: "%@ (\(APP_DEL.selectedCountry.getCurrencySymbol().uppercased()) 0)", APP_LBL().use_store_credit)
                        
                        self.btnApplyStoreCredit.isUserInteractionEnabled = false
                    }
                    else
                    {
                        self.btnApplyStoreCredit.isUserInteractionEnabled = true
                        
                        self.lblUserStoreCredit.text = String(format: "%@ (%@)", APP_LBL().use_store_credit, json_0["data"]["store_credit"].stringValue)
                    }
                    
                    
                    if self.isStoreCreditApplied {
                        
                        self.imgStoreCredit.image = UIImage(named: "StoreSwitchOn")
                        self.btnApplyStoreCredit.isSelected = true
                        
                    } else {
                        
                        self.imgStoreCredit.image = UIImage(named: "StoreSwitchOff")
                        self.btnApplyStoreCredit.isSelected = false
                    }
                    
                    self.lblStoreCreditValue.text = self.appliedStoreCreditString
                    
                    
                    self.tblProductList.delegate = self
                    self.tblProductList.dataSource = self
                    self.tblProductList.separatorStyle = .none
                    self.tblProductList.reloadData()
                    self.tblProductList.isHidden = false
                    self.tblProductList.alwaysBounceVertical = false
                    
                    if isFromApplePay {
                        
                        let temp = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "NejreeAppleCheckoutVC") as! NejreeAppleCheckoutVC
                        temp.viewCartJson = self.tempViewCartJson
                        temp.isStoreCreditApplied = self.isStoreCreditApplied
                        temp.appliedStoreCreditString = self.appliedStoreCreditString
                        temp.hidesBottomBarWhenPushed = true
                        self.navigationController?.pushViewController(temp, animated: true)
                    }
                }
          //  }
        }
    }
    
    // MARK: ADD OR REMOVE FROM WISHLIST
    @objc func addToWishListTapped(_ sender: UIButton) {
        
        if defaults.bool(forKey: "isLogin") {
            
            var super_attribute = "{";
            super_attribute += "\""+attributeCode+"\":";
            super_attribute += "\""+selectedVariation+"\",";
            
            if(super_attribute.last! == ","){
                super_attribute = super_attribute.substring(to: super_attribute.index(before: super_attribute.endIndex));
            }
            super_attribute += "}";

            let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
            
            if isSelectWishlist == true {

                self.removeFromWishList(dataToPost: ["customer_id" : userInfoDict["customerId"]! , "product_id" : productData.product_id ?? ""])
                
            } else {
                
                self.addToWishList(dataToPost:["customer_id" : userInfoDict["customerId"]!, "product_id":self.productData.product_id!])
            }
            
        } else {
            
            isFromWishList = self.product_id
            
            if let vc = UIStoryboard(name: "cedMageLogin", bundle: nil).instantiateViewController(withIdentifier: "nejreeLoginController") as? nejreeLoginController{
                
                APP_DEL.isLoginFromWishList = false
                vc.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }

    //MARK: REMOVE FROM WISHLIST
    func removeFromWishList(dataToPost:[String:String]) {
        
        var postData = [String:String]()
        
        if defaults.bool(forKey: "isLogin") {
            
            if let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] {
                
                postData["hashkey"] = userInfoDict["hashKey"]
                postData["customer_id"] = userInfoDict["customerId"]
            }
            
            for (key,val) in dataToPost {
                
                postData[key] = val;
            }
        }
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);
        
        API().callAPI(endPoint: "wishlist/removewishlist", method: .POST, param: postData) { (json_res, err) in
            
          //  DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                if err == nil {
                    
                    self.isSelectWishlist = false
                    self.productTable.reloadData()
                    
                    APP_DEL.isWishListShouldReload = true
                }
                
                APP_DEL.getCartCount()
          //  }
        }
    }
    
    //MARK: ADD TO WISHLIST
    func addToWishList(dataToPost:[String:String]) {
        
        var postData = [String:String]()
        
        if defaults.bool(forKey: "isLogin") {
            
            if let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] {
                
                postData["hashkey"] = userInfoDict["hashKey"]
                postData["customer_id"] = userInfoDict["customerId"]
            }
            
            for (key,val) in dataToPost {
                
                postData[key] = val;
            }
        }
        
        if let storeID =  UserDefaults.standard.value(forKey: "storeId") as? String {
            postData["store_id"] = storeID
        }
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);
        
        API().callAPI(endPoint: "wishlist/addwishlist", method: .POST, param: postData) { (json_res, err) in
            
           // DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                if err == nil {
                    
                    let json = json_res[0]["data"][0]
                    
                    let status = json["status"].stringValue;
                    let msg = json["message"].stringValue;
                    
                    var toastMessg = ""
                    
                    if status == "true" {
                        
                        toastMessg = "\(self.productData.product_name ?? "") " + APP_LBL().is_successfully_added_to_the_wishlist
                        self.isSelectWishlist = true
                        self.productTable.reloadData()
                        
                        var productPrice = ""
                        if self.productData.special_price == "no_special" || self.productData.special_price == "" || self.productData.special_price == self.productData.regular_price {
                            
                            productPrice = self.productData.regular_price!
                            
                        } else {
                            
                            productPrice = self.productData.special_price!
                        }
                        
                        Analytics.logEvent(AnalyticsEventAddToWishlist, parameters: [
                            AnalyticsParameterCurrency : APP_DEL.selectedCountry.getCurrencySymbol().uppercased(),
                            AnalyticsParameterItemCategory : "Shoes",
                            AnalyticsParameterItemID : self.product_id,
                            AnalyticsParameterQuantity : "1",
                            AnalyticsParameterItemName : self.productData.product_name ?? "",
                            AnalyticsParameterPrice : self.productData.special_price ?? "",
                            AnalyticsParameterValue : "",
                        ])
                        
                        let detail_1 : [AnalyticKey:String] = [
                            .Currency : APP_DEL.selectedCountry.getCurrencySymbol().uppercased(),
                            .ItemCategory : "Shoes",
                            .ItemID : self.product_id,
                            .Quantity : "1",
                            .ItemName : self.productData.product_name ?? "",
                            .Price : self.productData.special_price ?? "",
                            .Value : "",
                            .PriceWithoutCurrency : productPrice
                        ]
                        API().setEvent(eventName: .AddToWishlist, eventDetail: detail_1) { (json, err) in }
                        
                        APP_DEL.isWishListShouldReload = true
                        
                    } else {
                        toastMessg = msg
                    }
                    
                    self.view.isUserInteractionEnabled = false;
                    self.view.makeToast(toastMessg, duration: 2, position: .center, title: nil, image: nil, style: nil, completion: {
                        Void in
                        
                        self.view.isUserInteractionEnabled = true;
                    })
                }
                
                APP_DEL.getCartCount()
          //  }
        }
    }
    
    // MARK: REMOVE PRODUCT FROM CART
    func removeFromCart(sender: UIButton) {
        
        var postData = [String:String]()
        var cartId = "0"
        
        if (defaults.object(forKey: "cartId") != nil) {
            cartId = (defaults.object(forKey: "cartId") as? String)!;
        }
    
    
    
        if (defaults.object(forKey: "userInfoDict") != nil) {
            
            if let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] {
                
                postData["hashkey"] = userInfoDict["hashKey"]
                postData["customer_id"] = userInfoDict["customerId"]
            }
            
            if cartId != ""{
                postData["cart_id"] = cartId;
            }
            
            postData["product_id"]=products[sender.tag].product_id!;
            postData["item_id"]=products[sender.tag].item_id!
            
        } else {
            
            if cartId != "" {
                postData["cart_id"] = cartId;
            }

            postData["product_id"]=products[sender.tag].product_id!;
            postData["item_id"]=products[sender.tag].item_id!
        }
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);
        
        API().callAPI(endPoint: "mobiconnect/checkout/delete/", method: .POST, param: postData) { (json_res, err) in
            
           // DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                if err == nil {
                    
                    let json_0 = json_res[0]
                    
                    if (json_0["success"].stringValue == "removed_successfully") {
                        
                        self.getCartList(isFromApplePay: false)
                    }
                }
           // }
        }
    }
    
    @objc func actionShareUrl(_sender : UIButton) {
        self.shareApplication(viewC: self)
    }
        
    @IBAction func btnSizeGuideAction(_ sender : UIButton){
        
       if self.strProductType == "apparel"{
            
            SKPhotoBrowserOptions.displayAction = false
            var arrImages = [SKPhoto]()

            let photo = SKPhoto.photoWithImageURL(self.apparelImage)
            photo.shouldCachePhotoURLImage = true
            arrImages.append(photo)

            let browser = SKPhotoBrowser(photos: arrImages)
            browser.modalPresentationStyle = .fullScreen
            browser.modalTransitionStyle = .crossDissolve
            APP_DEL.window?.rootViewController?.present(browser, animated: true, completion: {})

            return;
        }
        
        
        let vc = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "SizeChartVC") as! SizeChartVC
        vc.brandName = self.brandName
        vc.kidsURL = self.kidsSize
        vc.genderID = genderID
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    func setcartCount() {
        
        let cartCount = self.defaults.value(forKey: "items_count") as! String;
        
        cedMageViewController().setCartCount(view:self,items: cartCount)
        
        DispatchQueue.main.async {
            cedMageLoaders.removeLoadingIndicator(me: self);
        }
        
        APP_DEL.getCartCount()
    }
    
    func generateDynamicLink(){
               
            
                   guard let link = URL(string: "https://www.nejree.com/catalog/product/view/id/\(product_id)")
                    else { return }
                   let dynamicLinksDomainURIPrefix = "https://links.nejree.com"
        let linkBuilder = DynamicLinkComponents(link: link, domainURIPrefix: dynamicLinksDomainURIPrefix)

        linkBuilder?.iOSParameters = DynamicLinkIOSParameters(bundleID: "com.nejree.ourapp")
        linkBuilder?.iOSParameters?.appStoreID = "1454057624"
        linkBuilder?.iOSParameters?.minimumAppVersion = "11.0"

        linkBuilder?.androidParameters = DynamicLinkAndroidParameters(packageName: "com.nejree.ourapp")
        linkBuilder?.androidParameters?.minimumVersion = 0

        guard let longDynamicLink = linkBuilder?.url else { return }
                   
       let opti = DynamicLinkComponentsOptions()
        opti.pathLength = .short
        
        DynamicLinkComponents.shortenURL(longDynamicLink, options: opti) { url, warnings, error in
                     guard let url = url, error == nil else {
                        return
                    }
            
            self.strShareURl = url.absoluteString
                   }
               
        
        }
    
    func shareApplication(viewC : UIViewController) {
        
        let textToShare = APP_LBL().product_share_text
        
        if let myWebsite = NSURL(string: strShareURl) {
            
            let objectsToShare = [textToShare, myWebsite] as [Any]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            
            activityVC.excludedActivityTypes = [UIActivity.ActivityType.airDrop, UIActivity.ActivityType.addToReadingList,.postToFacebook ,.postToTwitter,.message]
            
            activityVC.popoverPresentationController?.sourceView = viewC.view
            
            
            let cell = productTable.cellForRow(at: IndexPath(row: 0, section: 1)) as! nameAndRatingCell
            
            activityVC.popoverPresentationController?.sourceRect = cell.btnShare.frame
            
            viewC.present(activityVC, animated: true, completion: nil)
        }
    }
        
    @IBAction func ActionBtnBack(_ sender : UIButton){
        
        self.navigationController?.popViewController(animated: true)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    @objc func deleteButtonTapped(_ sender: UIButton) {
        
        let showTitle = APP_LBL().confirmation
        let showMsg = APP_LBL().operation_cant_be_undone
        
        let confirmationAlert = UIAlertController(title: showTitle, message: showMsg, preferredStyle: UIAlertController.Style.alert);
        
        confirmationAlert.addAction(UIAlertAction(title: APP_LBL().done_c, style: .default, handler: { (action: UIAlertAction!) in
            self.removeFromCart(sender: sender)
        }));
        
        confirmationAlert.addAction(UIAlertAction(title: APP_LBL().cancel, style: .default, handler: { (action: UIAlertAction!) in
        }));
        
        present(confirmationAlert, animated: true, completion: nil)
    }

    
    func isBrandExist() -> Bool {
        
        var res = false
        
         if self.strProductType == "apparel" && self.apparelImage == ""{
            
            return false;
            
        }
        
        
        if brandName == "Puma" || brandName == "بوما"{
            
           res = true
        }
        else if brandName == "Nike" || brandName == "نايكي"{
        
           res = true
        }

        else if brandName == "Adidas" || brandName == "أديداس" || brandName == "Adidas Originals" || brandName == "أديداس أوريجنالز" {
        
           res = true
        }
        else if brandName == "Fila" || brandName == "فيلا"{
        
           res = true
        }
        else if brandName == "Under Armour" || brandName == "اندر ارمور"{
        
            res = true
        }
        else if brandName == "Asics" || brandName == "أسيكس"{
        
           res = true
        }
        else if brandName == "Reebok" || brandName == "ريبوك"{
        
            res = true
        }
        else if brandName == "New Balance" || brandName == "نيو بالانس"{
        
           res = true
        }
        else if brandName == "Vans" || brandName == "فانز"{
        
          res = true
        }
        else if brandName == "Converse" || brandName == "كونفيرس"{
        
            res = true
        }
        
        return res;
    }
}

extension productViewController: UITableViewDelegate,UITableViewDataSource {
    
    
   func numberOfSections(in tableView: UITableView) -> Int {
       if tableView == tblProductList {
            return 1
        }else{
        return 5 + 2
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == tblProductList {
           return products.count
        }else{
            return 1
        }
        
    }
    
    

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
           
        
        if tableView == tblProductList {
            
           let cell = tableView.dequeueReusableCell(withIdentifier: "ProductViewPopUPCell", for: indexPath) as! ProductViewPopUPCell
           // cell.outerView.backgroundColor = .white
            
            cell.lblProductName.text = products[indexPath.row].product_name ?? ""
            cell.lblQty.text = APP_LBL().quantity_semicolon + " " + "\(products[indexPath.row].quantity ?? 0)"

            
            cell.imgProduct.sd_setImage(with: URL(string: products[indexPath.row].product_image!), placeholderImage:nil)
            
            
            cell.lblAmount.text = products[indexPath.row].sub_total ?? ""
           // cell.outerView.cardView()
            
            //if products[indexPath.row].item_error != "" {
            if ((products[indexPath.row].item_error?.first?.type ?? "") != "") {
                cell.lblOutOfStock.isHidden = false
                
            }
            else {
                cell.lblOutOfStock.isHidden = true
                
            }
            
            cell.lblOutOfStock.text = APP_LBL().product_is_out_of_stock;
            
            cell.lblSize.text = APP_LBL().size.uppercased() + ": " + (products[indexPath.row].options_selected?.first?.value ?? "")
            
           
            cell.btnClose.tag = indexPath.row
            cell.btnClose.addTarget(self, action: #selector(deleteButtonTapped(_:)), for: .touchUpInside)
            cell.selectionStyle = .none
            return cell
        }else{
        
        
           switch indexPath.section {
           case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "productImagesCell", for: indexPath) as! productImagesCell
            cell.imagedata = self.productImageArray
            
            if cell.imagedata.count > indexPath.row {
                imgProduct.sd_setImage(with: URL(string: cell.imagedata[indexPath.row]), placeholderImage:nil)
            }            

            cell.selectionStyle = .none
            cell.delegate = self
            cell.parent = self
            cell.selectionStyle = .none
            cell.pageControlView.numberOfPages = productImageArray.count
            cell.pageControlView.currentPage = currentPage
//            cell.pageControlView.customPageControl(dotFillColor: UIColor.init(hexString: "#fcb215")!, dotBorderColor: UIColor.init(hexString: "#fcb215")!, dotBorderWidth: 1)
            
            cell.pageControlView.tintColor  = UIColor.lightGray
            cell.pageControlView.currentPageIndicatorTintColor = UIColor.init(hexString: "#fcb215")!
                                    
            
            
            return cell
            
           case 1:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "nameAndRatingCell", for: indexPath) as! nameAndRatingCell
                
                if APP_DEL.selectedLanguage == Arabic {
                    cell.semanticContentAttribute = .forceRightToLeft
                    cell.productName.textAlignment = .right
                }
                
                cell.btnWish.addTarget(self, action: #selector(addToWishListTapped(_:)), for: .touchUpInside)
                
                if isSelectWishlist {
                    cell.btnWish.setImage(UIImage(named: "wish_selected-1"), for: .normal)
                }else{
                    cell.btnWish.setImage(UIImage(named: "wish_unselected"), for: .normal)
                }
                
                if APP_DEL.selectedLanguage == Arabic {
                    cell.btnWish.semanticContentAttribute = .forceRightToLeft
                }
                
                cell.btnShare.addTarget(self, action: #selector(actionShareUrl), for: .touchUpInside)
                
                //                        let name = productData.product_name?.components(separatedBy: "-")
                //cell.productName.text = name![0].uppercased()//productData.product_name
                cell.productName.text = self.brandName.uppercased()
                
                lblQty.text = APP_LBL().quantity_semicolon + " " + self.strProductQty
                
                lblProductName.text = productData.product_name?.uppercased()
                
                cell.productNameDes.text = productData.product_name?.uppercased()
                
                
                let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: productData.currency_symbol! + " " + productData.regular_price!)
                attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 1, range: NSMakeRange(0, attributeString.length))
                
                
                if productData.special_price == "no_special" || productData.special_price == "" || productData.special_price == productData.regular_price {
                    
                    cell.priceLabel.text = productData.currency_symbol! + " " + productData.regular_price!
                    
                    cell.discountPriceLabel.attributedText = nil
                    cell.discountPriceLabel.text = ""
                    cell.discountPriceLabel.superview?.isHidden = true
                } else {
                    cell.discountPriceLabel.superview?.isHidden = false
                    cell.discountPriceLabel.text = ""
                    cell.discountPriceLabel.attributedText = attributeString
                }
                
                
                
                //            cell.productColor.text = ""
            
                cell.lblOutOfStock.text = APP_LBL().out_of_stock.uppercased()
                cell.lblOutOfStock.textColor = .red
                cell.lblOutOfStock.superview?.isHidden = !self.isOutOfStock
                
                cell.priceLabel.text = productData.currency_symbol! + " " + productData.special_price!
                cell.priceLabel.textColor = nejreeColor
                
                lblAmount.text = productData.currency_symbol! + " " + productData.special_price!
                lblAmount.textColor = nejreeColor
                
                let count = Float(productData.review_count ?? 0)
                cell.ratingView.rating = count
                cell.ratingView.editable = false
                cell.ratingView.tintColor = UIColor.init(hexString: "#fcb215")
                cell.ratingView.backgroundColor = UIColor.clear
            
                if APP_DEL.selectedLanguage == Arabic {
                    
                    cell.productNameDes.textAlignment = .right
                    cell.lblOutOfStock.textAlignment = .left
                }
                else
                {
                    cell.productNameDes.textAlignment = .left
                    cell.lblOutOfStock.textAlignment = .right
                }
            
                cell.selectionStyle = .none
                return cell
            
              
           case 2:
                let cell = tableView.dequeueReusableCell(withIdentifier: "variationDataCell", for: indexPath) as! variationDataCell
                cell.variationData = self.variationData
                cell.variationRemainingQty = self.variationRemainingQty
                
                cell.btnSimilar.addTarget(self, action: #selector(btnSimilarProdAction(_:)), for: .touchUpInside)
                cell.btnRecommended.addTarget(self, action: #selector(btnRecommendedProdAction(_:)), for: .touchUpInside)
                
                cell.delegate = self
                cell.selectionStyle = .none
                
                if variationData.count > 0{
                    
                    cell.lblSelectASize.text = APP_LBL().select_a_size.uppercased()
                    cell.lblSelectASize.textColor = .black
                }
                else
                {
                    cell.lblSelectASize.text = APP_LBL().product_is_out_of_stock.uppercased()
                    cell.lblSelectASize.textColor = .red
                }
                       
                
                
               
                               
            if APP_DEL.selectedLanguage == Arabic {

                    cell.lblSelectASize.textAlignment = .right
                } else {
                    cell.lblSelectASize.textAlignment = .left
                }
                
                cell.btnSizeGuide.tag = indexPath.row
                cell.btnSizeGuide.addTarget(self, action: #selector(btnSizeGuideAction(_:)), for: .touchUpInside)
                
                if self.isBrandExist() {
                    cell.btnSizeGuide.isHidden = false
                    cell.btnSizeGuide.isUserInteractionEnabled = true
                    cell.btnSizeGuide.setTitle(String(format: "%@", APP_LBL().size_chart), for: .normal)
                } else {
                    cell.btnSizeGuide.isHidden = true
                    cell.btnSizeGuide.isUserInteractionEnabled = false
                    cell.btnSizeGuide.setTitle("", for: .normal)
                }
                
                cell.variationCollection.reloadData()
                cell.variationCollection.reloadData()
                cell.contentView.layoutIfNeeded()
                cell.constrantCollectionHeight.constant = cell.variationCollection.contentSize.height
                cell.contentView.layoutIfNeeded()
                cell.variationCollection.reloadData()
                                   
                
                if self.isOutOfStock {
                    
                    cell.viewSizes.isHidden = true
                    cell.viewSimiRecom.isHidden = true
                    
//                    if self.arrSimilar.count > 0 && self.tagSimilar != ""{
//                        cell.btnSimilar.isHidden = false
//                    }
//                    else{
//                        cell.btnSimilar.isHidden = true
//                    }
//
//                    if self.arrRelated.count > 0 && self.tagRelated != ""{
//                        cell.btnRecommended.isHidden = false
//                    }
//                    else
//                    {
//                        cell.btnRecommended.isHidden = true
//                    }
                    
                    
                } else {
                    
                    cell.viewSizes.isHidden = false
                    cell.viewSimiRecom.isHidden = true
                }
        
                return cell
           case 3:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "productPriceCell", for: indexPath) as! productPriceCell
                         
            
             let string = productData.description
             
             cell.descriptionLabel.numberOfLines = 0
             
            if self.isOutOfStock {
                cell.descriptionLabel.attributedText = nil
                cell.descriptionLabel.text = ""
            } else {
                cell.descriptionLabel.attributedText = htmlToAttributedString(string: string!)//string?.html2Attributed
            }
             
             self.maxLines = cell.descriptionLabel.calculateMaxLines()
                                                
             if APP_DEL.selectedLanguage == Arabic {
              
                   cell.descriptionLabel.textAlignment = .right
             }
            else
            {
                   cell.descriptionLabel.textAlignment = .left
            }
            
             cell.selectionStyle = .none
             return cell
              
           case 4:
               let cell = tableView.dequeueReusableCell(withIdentifier: "addToCartCell", for: indexPath) as! addToCartCell
                              
                              
            cell.addToCartButton.isHidden = true
            cell.ApplePayButton.isHidden = true
                              
          //  addToCartButton.roundCorners()
                        
                           if APP_DEL.selectedLanguage == Arabic {
                            addToCartButton.titleLabel!.font  = UIFont(name: "Cairo-Regular", size: 14)!
                           }
               
                              
                              addToCartButton.setTitleColor(.white, for: .normal)
                              if isSelected
                              {
                                    addToCartButton.backgroundColor = UIColor.black;
                              }
                              else {
                                    addToCartButton.backgroundColor = UIColor.lightGray
                              }
                             addToCartButton.setTitle(APP_LBL().add_to_my_box.uppercased(), for: .normal)
                            // addToCartButton.clipsToBounds = true
                              
                              ApplePayButton.backgroundColor = UIColor.black;
                              ApplePayButton.setTitleColor(.white, for: .normal)
                            //  ApplePayButton.clipsToBounds = true
                              ApplePayButton.setTitle(" Pay", for: .normal)
                              
            
                              if self.isApplePayEnabled == "1" {
                               
                               ApplePayButton.isHidden = false
                               addToCartButton.isHidden = false
                              }
                              else
                              {
                               ApplePayButton.isHidden = true
                               addToCartButton.isHidden = false
                              }
                              
                              cell.selectionStyle = .none
                              return cell
            
           case 5:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "DetailProductCell", for: indexPath) as! DetailProductCell
            cell.accessibilityIdentifier = identiSimilar
            cell.delegateDidSelectDetailProduct = self

            cell.isDisplayViewAll = false//(arrSimilar.count > limitSimilar)
            cell.btnViewAll.isHidden = !(arrSimilar.count > limitSimilar)
            cell.setProductData(product: arrSimilar)
            
            if APP_DEL.selectedLanguage == Arabic {
                cell.lblTitle.textAlignment = .right
            }
            else
            {
                cell.lblTitle.textAlignment = .left
            }
            
            
            cell.lblTitle.text = APP_LBL().similar_products.uppercased()
            //cell.collView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .init(), animated: false)
            cell.alpha = ((arrSimilar.count == 0) ? 0 : 1)
            cell.isHidden = (arrSimilar.count == 0)
            return cell
            
            case 6:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "DetailProductCell", for: indexPath) as! DetailProductCell
            cell.accessibilityIdentifier = identiRelated
            cell.delegateDidSelectDetailProduct = self

            cell.isDisplayViewAll = false//(arrRelated.count > limitRelated)
            cell.btnViewAll.isHidden = !(arrRelated.count > limitRelated)
            cell.setProductData(product: arrRelated)
            
            if APP_DEL.selectedLanguage == Arabic {
                cell.lblTitle.textAlignment = .right
            }
            else
            {
                cell.lblTitle.textAlignment = .left
            }
            
            cell.lblTitle.text = APP_LBL().recommended_products.uppercased()
            //cell.collView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .init(), animated: false)
            cell.alpha = ((arrRelated.count == 0) ? 0 : 1)
            cell.isHidden = (arrRelated.count == 0)
            return cell
            

           default:
               return UITableViewCell()
           }
           
        }
    
       }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if tableView == tblProductList {
            return UITableView.automaticDimension
        }else{
            
            switch indexPath.section {
            case 0:
                return SCREEN_HEIGHT * 0.54
            case 1:
                 return UITableView.automaticDimension
            case 2:
                return UITableView.automaticDimension
            case 3:
                 return UITableView.automaticDimension
            case 4:
                return 0
            case 5:
                return (((self.arrSimilar.count > 0) && (self.tagSimilar != "")) ? UITableView.automaticDimension : 0)
            case 6:
                return (((self.arrRelated.count > 0) && (self.tagRelated != "")) ? UITableView.automaticDimension : 0)
            default:
                return 0
            }
            
        }
        
        
        
        
    }
   
    
    
}

extension productViewController: selectedAttributeDelegate,setPageDelegate {
    func getSelectedAttribute(from: Int, isSelected: Bool) {
        self.selectedVariation = variationKeys[from]
        
        let selectedData = variationData[from]
        
        lblSize.text = APP_LBL().size.uppercased() + ": " + selectedData

        self.isSelected = isSelected
                
       /* let cell = productTable.cellForRow(at: IndexPath(row: 0, section: 2)) as! productPriceCell
        cell.priceLabel.text = variationPrice[from]["finalPrice"]
        cell.discountPriceLabel.text = variationPrice[from]["oldPrice"] */
        
        if isSelected
        {
         addToCartButton.backgroundColor = UIColor.black;
        }
        else {
         addToCartButton.backgroundColor = UIColor.lightGray
        }

        
        productTable.reloadSections([4], with: .none)
        
    }
    
    func getCurrentpage(with: Int) {
        self.currentPage = with
//        let ip = IndexPath(row: 0, section: 0)
//        guard let cell = productTable.cellForRow(at: ip) as? productImagesCell else {return}
//        self.productTable.reloadSections([0], with: .none)
    }
    
    
    
    func htmlToAttributedString(string : String) -> NSAttributedString{
        var attribStr = NSMutableAttributedString()
        
        do {//, allowLossyConversion: true
            attribStr = try NSMutableAttributedString(data: string.data(using: .unicode)!, options: [ .documentType: NSAttributedString.DocumentType.html], documentAttributes: nil)
            
            let textRangeForFont : NSRange = NSMakeRange(0, attribStr.length)
            attribStr.addAttributes([NSAttributedString.Key.font : UIFont(name: "Cairo", size:13)!], range: textRangeForFont)
            
        } catch {
            
        }
        
        return attribStr
    }
    
    
    
    
    
    
    
    
    
}


extension UIPageControl {
    
    func customPageControl(dotFillColor:UIColor, dotBorderColor:UIColor, dotBorderWidth:CGFloat) {
        for (pageIndex, dotView) in self.subviews.enumerated() {
            if self.currentPage == pageIndex {
                dotView.backgroundColor = dotFillColor
                dotView.layer.cornerRadius = dotView.frame.size.height / 2
            }else{
                dotView.backgroundColor = .clear
                dotView.layer.cornerRadius = dotView.frame.size.height / 2
                dotView.layer.borderColor = dotBorderColor.cgColor
                dotView.layer.borderWidth = dotBorderWidth
            }
        }
    }
  
    
}

extension UILabel {
   
    func calculateMaxLines() -> Int {
        let maxSize = CGSize(width: frame.size.width, height: CGFloat(Float.infinity))
        let charSize = font.lineHeight
        let text = (self.text ?? "") as NSString
        let textSize = text.boundingRect(with: maxSize, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        let linesRoundedUp = Int(ceil(textSize.height/charSize))
        return linesRoundedUp
    }
    
}

extension String{
    
    var htmlStripped : String{
        return self.replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil)
    }
    
}


extension String {
    var html2Attributed: NSAttributedString? {
        do {
            guard let data = data(using: String.Encoding.utf8) else {
                return nil
            }
            return try NSAttributedString(data: data,
                                          options: [.documentType: NSAttributedString.DocumentType.html,
                                                    .characterEncoding: String.Encoding.utf8.rawValue],
                                          documentAttributes: nil)
        } catch {
            
            return nil
        }
    }
}


