//
//  SizeGuideMenVC.swift
//  MageNative Magento Platinum
//
//  Created by pratima on 05/05/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit
let appcolor = UIColor(red: 251.0/255.0, green: 173.0/255.0, blue: 24.0/255.0, alpha: 1.0)

class SizeGuideMenVC: MagenativeUIViewController {
    
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var btnUs: UIButton!
    @IBOutlet weak var btnUk: UIButton!
    @IBOutlet weak var btnEu: UIButton!
    @IBOutlet weak var sizecollection: UICollectionView!
    @IBOutlet weak var btnBackToproduct: UIButton!
    var arrSizechart = NSMutableArray()
    
    @IBOutlet weak var constrraintHeight: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        UIView.appearance().semanticContentAttribute = .forceLeftToRight
        
        
        let nib = UINib(nibName: "SizechatCell", bundle: nil)
        sizecollection.register(nib, forCellWithReuseIdentifier: "SizechatCell")
        btnBackToproduct.backgroundColor = .black
//        btnBackToproduct.roundCorners()
        btnBackToproduct.layer.borderWidth = 1.0
        btnBackToproduct.layer.borderColor = UIColor.black.cgColor
        btnBackToproduct.setTitleColor(UIColor.white, for: .normal)
        self.USsizeChart()
        btnUs.backgroundColor = .black
        btnUs.layer.cornerRadius = 6.0
        btnUs.layer.borderWidth = 1.0
        btnUs.layer.borderColor = UIColor.black.cgColor
        btnUs.setTitleColor(UIColor.white, for: .normal)
        
        btnUk.backgroundColor = .white
        btnUk.layer.cornerRadius = 6.0
        btnUk.layer.borderWidth = 1.0
        btnUk.layer.borderColor = UIColor.lightGray.cgColor
        btnUk.setTitleColor(UIColor.black, for: .normal)
        
        btnEu.backgroundColor = .white
        btnEu.layer.cornerRadius = 6.0
        btnEu.layer.borderWidth = 1.0
        btnEu.layer.borderColor = UIColor.lightGray.cgColor
        btnEu.setTitleColor(UIColor.black, for: .normal)
        
        
        headingLabel.text = "دليل مقاسات الرجال"
        
        btnBackToproduct.roundCorners()
        
        btnBackToproduct.setTitle("العودة إلى المنتج", for: .normal)
    }
   
    override func viewDidDisappear(_ animated: Bool) {
        
        let value = UserDefaults.standard.value(forKey: "AppleLanguages") as! [String]
        if value[0] == "ar"{
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
            
        }
        else if value[0] == "en"{
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
            
        }
        
        
    }
    
    @IBAction func btnSizeReaload( _ sender:UIButton){
        if sender.tag == 1 {
            //US
            btnUs.backgroundColor = .black
            btnUs.layer.cornerRadius = 6.0
            btnUs.layer.borderWidth = 1.0
            btnUs.layer.borderColor = UIColor.black.cgColor
            btnUs.setTitleColor(UIColor.white, for: .normal)
            self.USsizeChart()
            btnUk.backgroundColor = .white
            btnUk.layer.cornerRadius = 6.0
            btnUk.layer.borderWidth = 1.0
            btnUk.layer.borderColor = UIColor.lightGray.cgColor
            btnUk.setTitleColor(UIColor.black, for: .normal)
            
            btnEu.backgroundColor = .white
            btnEu.layer.cornerRadius = 6.0
            btnEu.layer.borderWidth = 1.0
            btnEu.layer.borderColor = UIColor.lightGray.cgColor
            btnEu.setTitleColor(UIColor.black, for: .normal)
        } else if sender.tag == 2 {
            //UK
            btnUk.backgroundColor = .black
            btnUk.layer.cornerRadius = 6.0
            btnUk.layer.borderWidth = 1.0
            btnUk.layer.borderColor = UIColor.black.cgColor
            btnUk.setTitleColor(UIColor.white, for: .normal)
            self.UKsizeChart()
            btnUs.backgroundColor = .white
            btnUs.layer.cornerRadius = 6.0
            btnUs.layer.borderWidth = 1.0
            btnUs.layer.borderColor = UIColor.lightGray.cgColor
            btnUs.setTitleColor(UIColor.black, for: .normal)
            
            btnEu.backgroundColor = .white
            btnEu.layer.cornerRadius = 6.0
            btnEu.layer.borderWidth = 1.0
            btnEu.layer.borderColor = UIColor.lightGray.cgColor
            btnEu.setTitleColor(UIColor.black, for: .normal)
        }else if sender.tag == 3 {
            //EU
            btnEu.backgroundColor = .black
            btnEu.layer.cornerRadius = 6.0
            btnEu.layer.borderWidth = 1.0
            btnEu.layer.borderColor = UIColor.black.cgColor
            btnEu.setTitleColor(UIColor.white, for: .normal)
            self.EUsizeChart()
            
            btnUk.backgroundColor = .white
            btnUk.layer.cornerRadius = 6.0
            btnUk.layer.borderWidth = 1.0
            btnUk.layer.borderColor = UIColor.lightGray.cgColor
            btnUk.setTitleColor(UIColor.black, for: .normal)
            
            btnUs.backgroundColor = .white
            btnUs.layer.cornerRadius = 6.0
            btnUs.layer.borderWidth = 1.0
            btnUs.layer.borderColor = UIColor.lightGray.cgColor
            btnUs.setTitleColor(UIColor.black, for: .normal)
        }
    }

}


extension SizeGuideMenVC:UICollectionViewDelegate,UICollectionViewDataSource {
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrSizechart.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SizechatCell", for: indexPath) as! SizechatCell
        let dict = arrSizechart.object(at: indexPath.row) as? NSDictionary
        if(indexPath.row == 0){
           
            //cell.viewstack.addBorder(color: .black, backgroundColor: .black, thickness: 2)

            cell.lblCM.backgroundColor = UIColor.black
            cell.lblCM.textColor = appcolor
            cell.lbl241.backgroundColor = UIColor.black
            cell.lbl241.textColor = appcolor
            cell.lbl242.backgroundColor = UIColor.black
            cell.lbl242.textColor = appcolor
            cell.lbl251.backgroundColor = UIColor.black
            cell.lbl251.textColor = appcolor
            cell.lbl252.backgroundColor = UIColor.black
            cell.lbl252.textColor = appcolor
            cell.lbl261.backgroundColor = UIColor.black
            cell.lbl261.textColor = appcolor
            cell.lbl262.backgroundColor = UIColor.black
            cell.lbl262.textColor = appcolor
            cell.lbl271.backgroundColor = UIColor.black
            cell.lbl271.textColor = appcolor
            cell.lbl272.backgroundColor = UIColor.black
            cell.lbl272.textColor = appcolor
            cell.lbl281.backgroundColor = UIColor.black
            cell.lbl281.textColor = appcolor
            cell.lbl282.backgroundColor = UIColor.black
            cell.lbl282.textColor = appcolor
            cell.lbl291.backgroundColor = UIColor.black
            cell.lbl291.textColor = appcolor
            cell.lbl292.backgroundColor = UIColor.black
            cell.lbl292.textColor = appcolor
            cell.lbl301.backgroundColor = UIColor.black
            cell.lbl301.textColor = appcolor
            cell.lbl302.backgroundColor = UIColor.black
            cell.lbl302.textColor = appcolor
            cell.lbl311.backgroundColor = UIColor.black
            cell.lbl311.textColor = appcolor
            cell.lbl312.backgroundColor = UIColor.black
            cell.lbl312.textColor = appcolor
            cell.lbl321.backgroundColor = UIColor.black
            cell.lbl321.textColor = appcolor
            cell.lbl322.backgroundColor = UIColor.black
            cell.lbl322.textColor = appcolor
            cell.lbl331.backgroundColor = UIColor.black
            cell.lbl331.textColor = appcolor
            cell.lbl332.backgroundColor = UIColor.black
            cell.lbl332.textColor = appcolor
            cell.lbl341.backgroundColor = UIColor.black
            cell.lbl341.textColor = appcolor
        } else {
           // cell.viewstack.addBorder(color: .black, backgroundColor: .white, thickness: 2)
            
            cell.lblCM.backgroundColor = UIColor.black
            cell.lblCM.textColor = appcolor
            cell.lbl241.backgroundColor = UIColor.white
            cell.lbl241.textColor = UIColor.lightGray
            cell.lbl242.backgroundColor = UIColor.white
            cell.lbl242.textColor = UIColor.lightGray
            cell.lbl251.backgroundColor = UIColor.white
            cell.lbl251.textColor = UIColor.lightGray
            cell.lbl252.backgroundColor = UIColor.white
            cell.lbl252.textColor = UIColor.lightGray
            cell.lbl261.backgroundColor = UIColor.white
            cell.lbl261.textColor = UIColor.lightGray
            cell.lbl262.backgroundColor = UIColor.white
            cell.lbl262.textColor = UIColor.lightGray
            cell.lbl271.backgroundColor = UIColor.white
            cell.lbl271.textColor = UIColor.lightGray
            cell.lbl272.backgroundColor = UIColor.white
            cell.lbl272.textColor = UIColor.lightGray
            cell.lbl281.backgroundColor = UIColor.white
            cell.lbl281.textColor = UIColor.lightGray
            cell.lbl282.backgroundColor = UIColor.white
            cell.lbl282.textColor = UIColor.lightGray
            cell.lbl291.backgroundColor = UIColor.white
            cell.lbl291.textColor = UIColor.lightGray
            cell.lbl292.backgroundColor = UIColor.white
            cell.lbl292.textColor = UIColor.lightGray
            cell.lbl301.backgroundColor = UIColor.white
            cell.lbl301.textColor = UIColor.lightGray
            cell.lbl302.backgroundColor = UIColor.white
            cell.lbl302.textColor = UIColor.lightGray
            cell.lbl311.backgroundColor = UIColor.white
            cell.lbl311.textColor = UIColor.lightGray
            cell.lbl312.backgroundColor = UIColor.white
            cell.lbl312.textColor = UIColor.lightGray
            cell.lbl321.backgroundColor = UIColor.white
            cell.lbl321.textColor = UIColor.lightGray
            cell.lbl322.backgroundColor = UIColor.white
            cell.lbl322.textColor = UIColor.lightGray
            cell.lbl331.backgroundColor = UIColor.white
            cell.lbl331.textColor = UIColor.lightGray
            cell.lbl332.backgroundColor = UIColor.white
            cell.lbl332.textColor = UIColor.lightGray
            cell.lbl341.backgroundColor = UIColor.white
            cell.lbl341.textColor = UIColor.lightGray
        }
        
        
        cell.lblCM.text = dict?.value(forKey: "category") as? String
        cell.lbl241.text = dict?.value(forKey: "c_typ1") as? String
        cell.lbl242.text = dict?.value(forKey: "c_typ2") as? String
        cell.lbl251.text = dict?.value(forKey: "c_typ3") as? String
        cell.lbl252.text = dict?.value(forKey: "c_typ4") as? String
        cell.lbl261.text = dict?.value(forKey: "c_typ5") as? String
        cell.lbl262.text = dict?.value(forKey: "c_typ6") as? String
        cell.lbl271.text = dict?.value(forKey: "c_typ7") as? String
        cell.lbl272.text = dict?.value(forKey: "c_typ8") as? String
        cell.lbl281.text  = dict?.value(forKey: "c_typ9") as? String
        cell.lbl282.text  = dict?.value(forKey: "c_typ10") as? String
        cell.lbl291.text  = dict?.value(forKey: "c_typ11") as? String
        cell.lbl292.text  = dict?.value(forKey: "c_typ12") as? String
        cell.lbl301.text  = dict?.value(forKey: "c_typ13") as? String
        cell.lbl302.text  = dict?.value(forKey: "c_typ14") as? String
        cell.lbl311.text  = dict?.value(forKey: "c_typ15") as? String
        cell.lbl312.text  = dict?.value(forKey: "c_typ16") as? String
        cell.lbl321.text  = dict?.value(forKey: "c_typ17") as? String
        cell.lbl322.text  = dict?.value(forKey: "c_typ18") as? String
        cell.lbl331.text  = dict?.value(forKey: "c_typ19") as? String
        cell.lbl332.text  = dict?.value(forKey: "c_typ20") as? String
        cell.lbl341.text  = dict?.value(forKey: "c_typ21") as? String
        return cell
    }
    
    
    
}
extension SizeGuideMenVC {
    //MARK:- EUSIZE CHART
    func EUsizeChart(){
        arrSizechart = NSMutableArray()
        let dict1 = NSMutableDictionary()
        dict1.setValue("CM", forKey: "category")
        dict1.setValue("24", forKey: "c_typ1")
        dict1.setValue("24.5", forKey: "c_typ2")
        dict1.setValue("25", forKey: "c_typ3")
        dict1.setValue("25.5", forKey: "c_typ4")
        dict1.setValue("26", forKey: "c_typ5")
        dict1.setValue("26.5", forKey: "c_typ6")
        dict1.setValue("27", forKey: "c_typ7")
        dict1.setValue("27.5", forKey: "c_typ8")
        dict1.setValue("28", forKey: "c_typ9")
        dict1.setValue("28.5", forKey: "c_typ10")
        dict1.setValue("29", forKey: "c_typ11")
        dict1.setValue("29.5", forKey: "c_typ12")
        dict1.setValue("30", forKey: "c_typ13")
        dict1.setValue("30.5", forKey: "c_typ14")
        dict1.setValue("31", forKey: "c_typ15")
        dict1.setValue("31.5", forKey: "c_typ16")
        dict1.setValue("32", forKey: "c_typ17")
        dict1.setValue("32.5", forKey: "c_typ18")
        dict1.setValue("33", forKey: "c_typ19")
        dict1.setValue("33.5", forKey: "c_typ20")
        dict1.setValue("34", forKey: "c_typ21")
        
        
        let dict2 = NSMutableDictionary()
        dict2.setValue("NIKE", forKey: "category")
        dict2.setValue("38.5", forKey: "c_typ1")
        dict2.setValue("39", forKey: "c_typ2")
        dict2.setValue("40", forKey: "c_typ3")
        dict2.setValue("40.5", forKey: "c_typ4")
        dict2.setValue("41", forKey: "c_typ5")
        dict2.setValue("42", forKey: "c_typ6")
        dict2.setValue("42.5", forKey: "c_typ7")
        dict2.setValue("43", forKey: "c_typ8")
        dict2.setValue("44", forKey: "c_typ9")
        dict2.setValue("44.5", forKey: "c_typ10")
        dict2.setValue("45", forKey: "c_typ11")
        dict2.setValue("45.5", forKey: "c_typ12")
        dict2.setValue("46", forKey: "c_typ13")
        dict2.setValue("47", forKey: "c_typ14")
        dict2.setValue("47.5", forKey: "c_typ15")
        dict2.setValue("", forKey: "c_typ16")
        dict2.setValue("", forKey: "c_typ17")
        dict2.setValue("", forKey: "c_typ18")
        dict2.setValue("", forKey: "c_typ19")
        dict2.setValue("", forKey: "c_typ20")
        dict2.setValue("", forKey: "c_typ21")
        
        
        let dict3 = NSMutableDictionary()
        dict3.setValue("ADIDAS", forKey: "category")
        dict3.setValue("38.7", forKey: "c_typ1")
        dict3.setValue("39.3", forKey: "c_typ2")
        dict3.setValue("40", forKey: "c_typ3")
        dict3.setValue("40.7", forKey: "c_typ4")
        dict3.setValue("41.3", forKey: "c_typ5")
        dict3.setValue("42", forKey: "c_typ6")
        dict3.setValue("42.7", forKey: "c_typ7")
        dict3.setValue("43.3", forKey: "c_typ8")
        dict3.setValue("44", forKey: "c_typ9")
        dict3.setValue("44.7", forKey: "c_typ10")
        dict3.setValue("45.3", forKey: "c_typ11")
        dict3.setValue("46", forKey: "c_typ12")
        dict3.setValue("46.7", forKey: "c_typ13")
        dict3.setValue("47.3", forKey: "c_typ14")
        dict3.setValue("48", forKey: "c_typ15")
        dict3.setValue("48.7", forKey: "c_typ16")
        dict3.setValue("", forKey: "c_typ17")
        dict3.setValue("", forKey: "c_typ18")
        dict3.setValue("", forKey: "c_typ19")
        dict3.setValue("", forKey: "c_typ20")
        dict3.setValue("", forKey: "c_typ21")
        
        
        let dict4 = NSMutableDictionary()
        dict4.setValue("PUMA", forKey: "category")
        dict4.setValue("38", forKey: "c_typ1")
        dict4.setValue("38.5", forKey: "c_typ2")
        dict4.setValue("39", forKey: "c_typ3")
        dict4.setValue("40", forKey: "c_typ4")
        dict4.setValue("40.5", forKey: "c_typ5")
        dict4.setValue("41", forKey: "c_typ6")
        dict4.setValue("42", forKey: "c_typ7")
        dict4.setValue("42.5", forKey: "c_typ8")
        dict4.setValue("43", forKey: "c_typ9")
        dict4.setValue("44", forKey: "c_typ10")
        dict4.setValue("44.5", forKey: "c_typ11")
        dict4.setValue("45", forKey: "c_typ12")
        dict4.setValue("46", forKey: "c_typ13")
        dict4.setValue("46.5", forKey: "c_typ14")
        dict4.setValue("47", forKey: "c_typ15")
        dict4.setValue("48.5", forKey: "c_typ16")
        dict4.setValue("", forKey: "c_typ17")
        dict4.setValue("", forKey: "c_typ18")
        dict4.setValue("", forKey: "c_typ19")
        dict4.setValue("", forKey: "c_typ20")
        dict4.setValue("", forKey: "c_typ21")
        
        let dict5 = NSMutableDictionary()
        dict5.setValue("FILA", forKey: "category")
        dict5.setValue("39", forKey: "c_typ1")
        dict5.setValue("39.5", forKey: "c_typ2")
        dict5.setValue("40", forKey: "c_typ3")
        dict5.setValue("41", forKey: "c_typ4")
        dict5.setValue("41.5", forKey: "c_typ5")
        dict5.setValue("42", forKey: "c_typ6")
        dict5.setValue("43-43.5", forKey: "c_typ7")
        dict5.setValue("44", forKey: "c_typ8")
        dict5.setValue("44.5", forKey: "c_typ9")
        dict5.setValue("45", forKey: "c_typ10")
        dict5.setValue("45.5", forKey: "c_typ11")
        dict5.setValue("46", forKey: "c_typ12")
        dict5.setValue("", forKey: "c_typ13")
        dict5.setValue("", forKey: "c_typ14")
        dict5.setValue("", forKey: "c_typ15")
        dict5.setValue("", forKey: "c_typ16")
        dict5.setValue("", forKey: "c_typ17")
        dict5.setValue("", forKey: "c_typ18")
        dict5.setValue("", forKey: "c_typ19")
        dict5.setValue("", forKey: "c_typ20")
        dict5.setValue("", forKey: "c_typ21")
        
        let dict6 = NSMutableDictionary()
        dict6.setValue("UNDERARMUR", forKey: "category")
        dict6.setValue("", forKey: "c_typ1")
        dict6.setValue("", forKey: "c_typ2")
        dict6.setValue("40", forKey: "c_typ3")
        dict6.setValue("40.5", forKey: "c_typ4")
        dict6.setValue("41", forKey: "c_typ5")
        dict6.setValue("42", forKey: "c_typ6")
        dict6.setValue("42.5", forKey: "c_typ7")
        dict6.setValue("43", forKey: "c_typ8")
        dict6.setValue("44", forKey: "c_typ9")
        dict6.setValue("44.5", forKey: "c_typ10")
        dict6.setValue("45", forKey: "c_typ11")
        dict6.setValue("45.5", forKey: "c_typ12")
        dict6.setValue("46", forKey: "c_typ13")
        dict6.setValue("47", forKey: "c_typ14")
        dict6.setValue("47.5", forKey: "c_typ15")
        dict6.setValue("48", forKey: "c_typ16")
        dict6.setValue("48.5", forKey: "c_typ17")
        dict6.setValue("49", forKey: "c_typ18")
        dict6.setValue("49.5", forKey: "c_typ19")
        dict6.setValue("50", forKey: "c_typ20")
        dict6.setValue("50.5", forKey: "c_typ21")
        
        let dict7 = NSMutableDictionary()
        dict7.setValue("ASICS", forKey: "category")
        dict7.setValue("", forKey: "c_typ1")
        dict7.setValue("39", forKey: "c_typ2")
        dict7.setValue("39.5", forKey: "c_typ3")
        dict7.setValue("40", forKey: "c_typ4")
        dict7.setValue("40.5", forKey: "c_typ5")
        dict7.setValue("41", forKey: "c_typ6")
        dict7.setValue("42", forKey: "c_typ7")
        dict7.setValue("42.5", forKey: "c_typ8")
        dict7.setValue("43", forKey: "c_typ9")
        dict7.setValue("44", forKey: "c_typ10")
        dict7.setValue("44.5", forKey: "c_typ11")
        dict7.setValue("45", forKey: "c_typ12")
        dict7.setValue("45.5", forKey: "c_typ13")
        dict7.setValue("46", forKey: "c_typ14")
        dict7.setValue("47", forKey: "c_typ15")
        dict7.setValue("48", forKey: "c_typ16")
        dict7.setValue("48.5", forKey: "c_typ17")
        dict7.setValue("", forKey: "c_typ18")
        dict7.setValue("", forKey: "c_typ19")
        dict7.setValue("", forKey: "c_typ20")
        dict7.setValue("", forKey: "c_typ21")
        
        
        let dict8 = NSMutableDictionary()
        dict8.setValue("REEBOK", forKey: "category")
        dict8.setValue("", forKey: "c_typ1")
        dict8.setValue("38.5", forKey: "c_typ2")
        dict8.setValue("39", forKey: "c_typ3")
        dict8.setValue("40", forKey: "c_typ4")
        dict8.setValue("40.5", forKey: "c_typ5")
        dict8.setValue("41", forKey: "c_typ6")
        dict8.setValue("42", forKey: "c_typ7")
        dict8.setValue("42.5", forKey: "c_typ8")
        dict8.setValue("43", forKey: "c_typ9")
        dict8.setValue("44", forKey: "c_typ10")
        dict8.setValue("44.5", forKey: "c_typ11")
        dict8.setValue("45", forKey: "c_typ12")
        dict8.setValue("45.5", forKey: "c_typ13")
        dict8.setValue("46", forKey: "c_typ14")
        dict8.setValue("47", forKey: "c_typ15")
        dict8.setValue("48", forKey: "c_typ16")
        dict8.setValue("48.5", forKey: "c_typ17")
        dict8.setValue("49", forKey: "c_typ18")
        dict8.setValue("50", forKey: "c_typ19")
        dict8.setValue("", forKey: "c_typ20")
        dict8.setValue("", forKey: "c_typ21")
        
        let dict9 = NSMutableDictionary()
        dict9.setValue("New Balance", forKey: "category")
        dict9.setValue("38.5", forKey: "c_typ1")
        dict9.setValue("39.5", forKey: "c_typ2")
        dict9.setValue("40", forKey: "c_typ3")
        dict9.setValue("40.5", forKey: "c_typ4")
        dict9.setValue("41.5", forKey: "c_typ5")
        dict9.setValue("42", forKey: "c_typ6")
        dict9.setValue("42.5", forKey: "c_typ7")
        dict9.setValue("43", forKey: "c_typ8")
        dict9.setValue("44", forKey: "c_typ9")
        dict9.setValue("", forKey: "c_typ10")
        dict9.setValue("45", forKey: "c_typ11")
        dict9.setValue("45.5", forKey: "c_typ12")
        dict9.setValue("46.5", forKey: "c_typ13")
        dict9.setValue("47", forKey: "c_typ14")
        dict9.setValue("", forKey: "c_typ15")
        dict9.setValue("", forKey: "c_typ16")
        dict9.setValue("49", forKey: "c_typ17")
        dict9.setValue("", forKey: "c_typ18")
        dict9.setValue("50", forKey: "c_typ19")
        dict9.setValue("", forKey: "c_typ20")
        dict9.setValue("51", forKey: "c_typ21")
        
        let dict10 = NSMutableDictionary()
        dict10.setValue("VANS", forKey: "category")
        dict10.setValue("", forKey: "c_typ1")
        dict10.setValue("38.5", forKey: "c_typ2")
        dict10.setValue("39", forKey: "c_typ3")
        dict10.setValue("40", forKey: "c_typ4")
        dict10.setValue("40.5", forKey: "c_typ5")
        dict10.setValue("41", forKey: "c_typ6")
        dict10.setValue("42", forKey: "c_typ7")
        dict10.setValue("42.5", forKey: "c_typ8")
        dict10.setValue("43", forKey: "c_typ9")
        dict10.setValue("44", forKey: "c_typ10")
        dict10.setValue("44.5", forKey: "c_typ11")
        dict10.setValue("45", forKey: "c_typ12")
        dict10.setValue("46", forKey: "c_typ13")
        dict10.setValue("", forKey: "c_typ14")
        dict10.setValue("47", forKey: "c_typ15")
        dict10.setValue("", forKey: "c_typ16")
        dict10.setValue("48", forKey: "c_typ17")
        dict10.setValue("", forKey: "c_typ18")
        dict10.setValue("49", forKey: "c_typ19")
        dict10.setValue("", forKey: "c_typ20")
        dict10.setValue("50", forKey: "c_typ21")
        
        
        let dict11 = NSMutableDictionary()
        dict11.setValue("CONVERS", forKey: "category")
        dict11.setValue("", forKey: "c_typ1")
        dict11.setValue("39", forKey: "c_typ2")
        dict11.setValue("39.5", forKey: "c_typ3")
        dict11.setValue("40", forKey: "c_typ4")
        dict11.setValue("41", forKey: "c_typ5")
        dict11.setValue("41.5", forKey: "c_typ6")
        dict11.setValue("42", forKey: "c_typ7")
        dict11.setValue("42.5", forKey: "c_typ8")
        dict11.setValue("43", forKey: "c_typ9")
        dict11.setValue("44", forKey: "c_typ10")
        dict11.setValue("44.5", forKey: "c_typ11")
        dict11.setValue("45", forKey: "c_typ12")
        dict11.setValue("46", forKey: "c_typ13")
        dict11.setValue("46.5", forKey: "c_typ14")
        dict11.setValue("", forKey: "c_typ15")
        dict11.setValue("48", forKey: "c_typ16")
        dict11.setValue("", forKey: "c_typ17")
        dict11.setValue("", forKey: "c_typ18")
        dict11.setValue("", forKey: "c_typ19")
        dict11.setValue("", forKey: "c_typ20")
        dict11.setValue("", forKey: "c_typ21")
        
        arrSizechart.add(dict1)
        arrSizechart.add(dict2)
        arrSizechart.add(dict3)
        arrSizechart.add(dict4)
        arrSizechart.add(dict5)
        arrSizechart.add(dict6)
        arrSizechart.add(dict7)
        arrSizechart.add(dict8)
        arrSizechart.add(dict9)
        arrSizechart.add(dict10)
        arrSizechart.add(dict11)
        
        if (arrSizechart.count > 0) {
            
            sizecollection.delegate = self
            sizecollection.dataSource = self
            sizecollection.reloadData()
            self.view.layoutIfNeeded()
            self.constrraintHeight.constant =  CGFloat((arrSizechart.count * 50)+50)
            self.view.layoutIfNeeded()
            self.sizecollection.reloadData()

            
        }
    }
    
    //MARK:- UKSIZE CHART
      func UKsizeChart(){
          arrSizechart = NSMutableArray()
          let dict1 = NSMutableDictionary()
          dict1.setValue("CM", forKey: "category")
          dict1.setValue("24", forKey: "c_typ1")
          dict1.setValue("24.5", forKey: "c_typ2")
          dict1.setValue("25", forKey: "c_typ3")
          dict1.setValue("25.5", forKey: "c_typ4")
          dict1.setValue("26", forKey: "c_typ5")
          dict1.setValue("26.5", forKey: "c_typ6")
          dict1.setValue("27", forKey: "c_typ7")
          dict1.setValue("27.5", forKey: "c_typ8")
          dict1.setValue("28", forKey: "c_typ9")
          dict1.setValue("28.5", forKey: "c_typ10")
          dict1.setValue("29", forKey: "c_typ11")
          dict1.setValue("29.5", forKey: "c_typ12")
          dict1.setValue("30", forKey: "c_typ13")
          dict1.setValue("30.5", forKey: "c_typ14")
          dict1.setValue("31", forKey: "c_typ15")
          dict1.setValue("31.5", forKey: "c_typ16")
          dict1.setValue("32", forKey: "c_typ17")
          dict1.setValue("32.5", forKey: "c_typ18")
          dict1.setValue("33", forKey: "c_typ19")
          dict1.setValue("33.5", forKey: "c_typ20")
          dict1.setValue("34", forKey: "c_typ21")
          
          
          let dict2 = NSMutableDictionary()
          dict2.setValue("NIKE", forKey: "category")
          dict2.setValue("5", forKey: "c_typ1")
          dict2.setValue("5.5", forKey: "c_typ2")
          dict2.setValue("6", forKey: "c_typ3")
          dict2.setValue("6.5", forKey: "c_typ4")
          dict2.setValue("7", forKey: "c_typ5")
          dict2.setValue("7.5", forKey: "c_typ6")
          dict2.setValue("8", forKey: "c_typ7")
          dict2.setValue("8.5", forKey: "c_typ8")
          dict2.setValue("9", forKey: "c_typ9")
          dict2.setValue("9.5", forKey: "c_typ10")
          dict2.setValue("10", forKey: "c_typ11")
          dict2.setValue("10.5", forKey: "c_typ12")
          dict2.setValue("11", forKey: "c_typ13")
          dict2.setValue("11.5", forKey: "c_typ14")
          dict2.setValue("12", forKey: "c_typ15")
          dict2.setValue("", forKey: "c_typ16")
          dict2.setValue("", forKey: "c_typ17")
          dict2.setValue("", forKey: "c_typ18")
          dict2.setValue("", forKey: "c_typ19")
          dict2.setValue("", forKey: "c_typ20")
          dict2.setValue("", forKey: "c_typ21")
          
          
          let dict3 = NSMutableDictionary()
          dict3.setValue("ADIDAS", forKey: "category")
          dict3.setValue("5.5", forKey: "c_typ1")
          dict3.setValue("6", forKey: "c_typ2")
          dict3.setValue("6.5", forKey: "c_typ3")
          dict3.setValue("7", forKey: "c_typ4")
          dict3.setValue("7.5", forKey: "c_typ5")
          dict3.setValue("8", forKey: "c_typ6")
          dict3.setValue("8.5", forKey: "c_typ7")
          dict3.setValue("9", forKey: "c_typ8")
          dict3.setValue("9.5", forKey: "c_typ9")
          dict3.setValue("10", forKey: "c_typ10")
          dict3.setValue("10.5", forKey: "c_typ11")
          dict3.setValue("11", forKey: "c_typ12")
          dict3.setValue("11.5", forKey: "c_typ13")
          dict3.setValue("12", forKey: "c_typ14")
          dict3.setValue("12.5", forKey: "c_typ15")
          dict3.setValue("13", forKey: "c_typ16")
          dict3.setValue("", forKey: "c_typ17")
          dict3.setValue("", forKey: "c_typ18")
          dict3.setValue("", forKey: "c_typ19")
          dict3.setValue("", forKey: "c_typ20")
          dict3.setValue("", forKey: "c_typ21")
          
          
          let dict4 = NSMutableDictionary()
          dict4.setValue("PUMA", forKey: "category")
          dict4.setValue("5", forKey: "c_typ1")
          dict4.setValue("5.5", forKey: "c_typ2")
          dict4.setValue("6", forKey: "c_typ3")
          dict4.setValue("6.5", forKey: "c_typ4")
          dict4.setValue("7", forKey: "c_typ5")
          dict4.setValue("7.5", forKey: "c_typ6")
          dict4.setValue("8", forKey: "c_typ7")
          dict4.setValue("8.5", forKey: "c_typ8")
          dict4.setValue("9", forKey: "c_typ9")
          dict4.setValue("9.5", forKey: "c_typ10")
          dict4.setValue("10", forKey: "c_typ11")
          dict4.setValue("10.5", forKey: "c_typ12")
          dict4.setValue("11", forKey: "c_typ13")
          dict4.setValue("11.5", forKey: "c_typ14")
          dict4.setValue("12", forKey: "c_typ15")
          dict4.setValue("13", forKey: "c_typ16")
          dict4.setValue("", forKey: "c_typ17")
          dict4.setValue("", forKey: "c_typ18")
          dict4.setValue("", forKey: "c_typ19")
          dict4.setValue("", forKey: "c_typ20")
          dict4.setValue("", forKey: "c_typ21")
          
          let dict5 = NSMutableDictionary()
          dict5.setValue("FILA", forKey: "category")
          dict5.setValue("5.5", forKey: "c_typ1")
          dict5.setValue("6", forKey: "c_typ2")
          dict5.setValue("6.5", forKey: "c_typ3")
          dict5.setValue("7", forKey: "c_typ4")
          dict5.setValue("7.5", forKey: "c_typ5")
          dict5.setValue("8", forKey: "c_typ6")
          dict5.setValue("9", forKey: "c_typ7")
          dict5.setValue("9.5", forKey: "c_typ8")
          dict5.setValue("10", forKey: "c_typ9")
          dict5.setValue("10.5", forKey: "c_typ10")
          dict5.setValue("", forKey: "c_typ11")
          dict5.setValue("11", forKey: "c_typ12")
          dict5.setValue("", forKey: "c_typ13")
          dict5.setValue("", forKey: "c_typ14")
          dict5.setValue("", forKey: "c_typ15")
          dict5.setValue("", forKey: "c_typ16")
          dict5.setValue("", forKey: "c_typ17")
          dict5.setValue("", forKey: "c_typ18")
          dict5.setValue("", forKey: "c_typ19")
          dict5.setValue("", forKey: "c_typ20")
          dict5.setValue("", forKey: "c_typ21")
          
          let dict6 = NSMutableDictionary()
          dict6.setValue("UNDERARMUR", forKey: "category")
          dict6.setValue("", forKey: "c_typ1")
          dict6.setValue("", forKey: "c_typ2")
          dict6.setValue("6", forKey: "c_typ3")
          dict6.setValue("6.5", forKey: "c_typ4")
          dict6.setValue("7", forKey: "c_typ5")
          dict6.setValue("7.5", forKey: "c_typ6")
          dict6.setValue("8", forKey: "c_typ7")
          dict6.setValue("8.5", forKey: "c_typ8")
          dict6.setValue("9", forKey: "c_typ9")
          dict6.setValue("9.5", forKey: "c_typ10")
          dict6.setValue("10", forKey: "c_typ11")
          dict6.setValue("10.5", forKey: "c_typ12")
          dict6.setValue("11", forKey: "c_typ13")
          dict6.setValue("11.5", forKey: "c_typ14")
          dict6.setValue("12", forKey: "c_typ15")
          dict6.setValue("12.5", forKey: "c_typ16")
          dict6.setValue("13", forKey: "c_typ17")
          dict6.setValue("13.5", forKey: "c_typ18")
          dict6.setValue("14", forKey: "c_typ19")
          dict6.setValue("14.5", forKey: "c_typ20")
          dict6.setValue("15", forKey: "c_typ21")
          
          let dict7 = NSMutableDictionary()
          dict7.setValue("ASICS", forKey: "category")
          dict7.setValue("", forKey: "c_typ1")
          dict7.setValue("5", forKey: "c_typ2")
          dict7.setValue("5.5", forKey: "c_typ3")
          dict7.setValue("6", forKey: "c_typ4")
          dict7.setValue("6.6", forKey: "c_typ5")
          dict7.setValue("7", forKey: "c_typ6")
          dict7.setValue("7.5", forKey: "c_typ7")
          dict7.setValue("8", forKey: "c_typ8")
          dict7.setValue("8.5", forKey: "c_typ9")
          dict7.setValue("9", forKey: "c_typ10")
          dict7.setValue("9.5", forKey: "c_typ11")
          dict7.setValue("10", forKey: "c_typ12")
          dict7.setValue("", forKey: "c_typ13")
          dict7.setValue("10.5", forKey: "c_typ14")
          dict7.setValue("", forKey: "c_typ15")
          dict7.setValue("", forKey: "c_typ16")
          dict7.setValue("", forKey: "c_typ17")
          dict7.setValue("", forKey: "c_typ18")
          dict7.setValue("", forKey: "c_typ19")
          dict7.setValue("", forKey: "c_typ20")
          dict7.setValue("", forKey: "c_typ21")
          
          
          let dict8 = NSMutableDictionary()
          dict8.setValue("REEBOK", forKey: "category")
          dict8.setValue("", forKey: "c_typ1")
          dict8.setValue("5.5", forKey: "c_typ2")
          dict8.setValue("6", forKey: "c_typ3")
          dict8.setValue("6.5", forKey: "c_typ4")
          dict8.setValue("7", forKey: "c_typ5")
          dict8.setValue("7.5", forKey: "c_typ6")
          dict8.setValue("8", forKey: "c_typ7")
          dict8.setValue("8.5", forKey: "c_typ8")
          dict8.setValue("9", forKey: "c_typ9")
          dict8.setValue("9.5", forKey: "c_typ10")
          dict8.setValue("10", forKey: "c_typ11")
          dict8.setValue("10.5", forKey: "c_typ12")
          dict8.setValue("11", forKey: "c_typ13")
          dict8.setValue("11.5", forKey: "c_typ14")
          dict8.setValue("12", forKey: "c_typ15")
          dict8.setValue("12.5", forKey: "c_typ16")
          dict8.setValue("13", forKey: "c_typ17")
          dict8.setValue("13.5", forKey: "c_typ18")
          dict8.setValue("14", forKey: "c_typ19")
          dict8.setValue("", forKey: "c_typ20")
          dict8.setValue("", forKey: "c_typ21")
          
          let dict9 = NSMutableDictionary()
          dict9.setValue("New Balance", forKey: "category")
          dict9.setValue("5.5", forKey: "c_typ1")
          dict9.setValue("6", forKey: "c_typ2")
          dict9.setValue("6.5", forKey: "c_typ3")
          dict9.setValue("7", forKey: "c_typ4")
          dict9.setValue("7.5", forKey: "c_typ5")
          dict9.setValue("8", forKey: "c_typ6")
          dict9.setValue("8.5", forKey: "c_typ7")
          dict9.setValue("9", forKey: "c_typ8")
          dict9.setValue("9.5", forKey: "c_typ9")
          dict9.setValue("", forKey: "c_typ10")
          dict9.setValue("10.5", forKey: "c_typ11")
          dict9.setValue("11", forKey: "c_typ12")
          dict9.setValue("11.5", forKey: "c_typ13")
          dict9.setValue("12", forKey: "c_typ14")
          dict9.setValue("", forKey: "c_typ15")
          dict9.setValue("", forKey: "c_typ16")
          dict9.setValue("13.5", forKey: "c_typ17")
          dict9.setValue("", forKey: "c_typ18")
          dict9.setValue("14.5", forKey: "c_typ19")
          dict9.setValue("", forKey: "c_typ20")
          dict9.setValue("15.5", forKey: "c_typ21")
          
          let dict10 = NSMutableDictionary()
          dict10.setValue("VANS", forKey: "category")
          dict10.setValue("", forKey: "c_typ1")
          dict10.setValue("5.5", forKey: "c_typ2")
          dict10.setValue("6", forKey: "c_typ3")
          dict10.setValue("6.5", forKey: "c_typ4")
          dict10.setValue("7", forKey: "c_typ5")
          dict10.setValue("7.5", forKey: "c_typ6")
          dict10.setValue("8", forKey: "c_typ7")
          dict10.setValue("8.5", forKey: "c_typ8")
          dict10.setValue("9", forKey: "c_typ9")
          dict10.setValue("9.5", forKey: "c_typ10")
          dict10.setValue("10", forKey: "c_typ11")
          dict10.setValue("10.5", forKey: "c_typ12")
          dict10.setValue("11", forKey: "c_typ13")
          dict10.setValue("", forKey: "c_typ14")
          dict10.setValue("12", forKey: "c_typ15")
          dict10.setValue("", forKey: "c_typ16")
          dict10.setValue("13", forKey: "c_typ17")
          dict10.setValue("", forKey: "c_typ18")
          dict10.setValue("14", forKey: "c_typ19")
          dict10.setValue("", forKey: "c_typ20")
          dict10.setValue("15", forKey: "c_typ21")
          
          
            let dict11 = NSMutableDictionary()
                dict11.setValue("CONVERS", forKey: "category")
                dict11.setValue("", forKey: "c_typ1")
                dict11.setValue("6", forKey: "c_typ2")
                dict11.setValue("6.5", forKey: "c_typ3")
                dict11.setValue("7", forKey: "c_typ4")
                dict11.setValue("7.5", forKey: "c_typ5")
                dict11.setValue("8", forKey: "c_typ6")
                dict11.setValue("8.5", forKey: "c_typ7")
                dict11.setValue("9", forKey: "c_typ8")
                dict11.setValue("9.5", forKey: "c_typ9")
                dict11.setValue("10", forKey: "c_typ10")
                dict11.setValue("10.5", forKey: "c_typ11")
                dict11.setValue("11", forKey: "c_typ12")
                dict11.setValue("11.5", forKey: "c_typ13")
                dict11.setValue("12", forKey: "c_typ14")
                dict11.setValue("", forKey: "c_typ15")
                dict11.setValue("13", forKey: "c_typ16")
                dict11.setValue("", forKey: "c_typ17")
                dict11.setValue("", forKey: "c_typ18")
                dict11.setValue("", forKey: "c_typ19")
                dict11.setValue("", forKey: "c_typ20")
                dict11.setValue("", forKey: "c_typ21")
          
          arrSizechart.add(dict1)
          arrSizechart.add(dict2)
          arrSizechart.add(dict3)
          arrSizechart.add(dict4)
          arrSizechart.add(dict5)
          arrSizechart.add(dict6)
          arrSizechart.add(dict7)
          arrSizechart.add(dict8)
          arrSizechart.add(dict9)
          arrSizechart.add(dict10)
          arrSizechart.add(dict11)
        
        if (arrSizechart.count > 0) {
                   
                   sizecollection.delegate = self
                   sizecollection.dataSource = self
                   sizecollection.reloadData()
                    self.view.layoutIfNeeded()
                  self.constrraintHeight.constant =  CGFloat((arrSizechart.count * 50)+50)
                   self.view.layoutIfNeeded()
                   self.sizecollection.reloadData()

        
                   
               }
      }
    
    //MARK:- USSIZE CHART
    func USsizeChart(){
        arrSizechart = NSMutableArray()
        let dict1 = NSMutableDictionary()
        dict1.setValue("CM", forKey: "category")
        dict1.setValue("24", forKey: "c_typ1")
        dict1.setValue("24.5", forKey: "c_typ2")
        dict1.setValue("25", forKey: "c_typ3")
        dict1.setValue("25.5", forKey: "c_typ4")
        dict1.setValue("26", forKey: "c_typ5")
        dict1.setValue("26.5", forKey: "c_typ6")
        dict1.setValue("27", forKey: "c_typ7")
        dict1.setValue("27.5", forKey: "c_typ8")
        dict1.setValue("28", forKey: "c_typ9")
        dict1.setValue("28.5", forKey: "c_typ10")
        dict1.setValue("29", forKey: "c_typ11")
        dict1.setValue("29.5", forKey: "c_typ12")
        dict1.setValue("30", forKey: "c_typ13")
        dict1.setValue("30.5", forKey: "c_typ14")
        dict1.setValue("31", forKey: "c_typ15")
        dict1.setValue("31.5", forKey: "c_typ16")
        dict1.setValue("32", forKey: "c_typ17")
        dict1.setValue("32.5", forKey: "c_typ18")
        dict1.setValue("33", forKey: "c_typ19")
        dict1.setValue("33.5", forKey: "c_typ20")
        dict1.setValue("34", forKey: "c_typ21")
        
        
        let dict2 = NSMutableDictionary()
        dict2.setValue("NIKE", forKey: "category")
        dict2.setValue("6", forKey: "c_typ1")
        dict2.setValue("6.5", forKey: "c_typ2")
        dict2.setValue("7", forKey: "c_typ3")
        dict2.setValue("7.5", forKey: "c_typ4")
        dict2.setValue("8", forKey: "c_typ5")
        dict2.setValue("8.5", forKey: "c_typ6")
        dict2.setValue("9", forKey: "c_typ7")
        dict2.setValue("9.5", forKey: "c_typ8")
        dict2.setValue("10", forKey: "c_typ9")
        dict2.setValue("10.5", forKey: "c_typ10")
        dict2.setValue("11", forKey: "c_typ11")
        dict2.setValue("11.5", forKey: "c_typ12")
        dict2.setValue("12", forKey: "c_typ13")
        dict2.setValue("12.5", forKey: "c_typ14")
        dict2.setValue("13", forKey: "c_typ15")
        dict2.setValue("", forKey: "c_typ16")
        dict2.setValue("", forKey: "c_typ17")
        dict2.setValue("", forKey: "c_typ18")
        dict2.setValue("", forKey: "c_typ19")
        dict2.setValue("", forKey: "c_typ20")
        dict2.setValue("", forKey: "c_typ21")
        
        
        let dict3 = NSMutableDictionary()
        dict3.setValue("ADIDAS", forKey: "category")
        dict3.setValue("6", forKey: "c_typ1")
        dict3.setValue("6.5", forKey: "c_typ2")
        dict3.setValue("7", forKey: "c_typ3")
        dict3.setValue("7.5", forKey: "c_typ4")
        dict3.setValue("8", forKey: "c_typ5")
        dict3.setValue("8.5", forKey: "c_typ6")
        dict3.setValue("9", forKey: "c_typ7")
        dict3.setValue("9.5", forKey: "c_typ8")
        dict3.setValue("10", forKey: "c_typ9")
        dict3.setValue("10.5", forKey: "c_typ10")
        dict3.setValue("11", forKey: "c_typ11")
        dict3.setValue("11.5", forKey: "c_typ12")
        dict3.setValue("12", forKey: "c_typ13")
        dict3.setValue("12.5", forKey: "c_typ14")
        dict3.setValue("13", forKey: "c_typ15")
        dict3.setValue("13.5", forKey: "c_typ16")
        dict3.setValue("", forKey: "c_typ17")
        dict3.setValue("", forKey: "c_typ18")
        dict3.setValue("", forKey: "c_typ19")
        dict3.setValue("", forKey: "c_typ20")
        dict3.setValue("", forKey: "c_typ21")
        
        
        let dict4 = NSMutableDictionary()
        dict4.setValue("PUMA", forKey: "category")
        dict4.setValue("6", forKey: "c_typ1")
        dict4.setValue("6.5", forKey: "c_typ2")
        dict4.setValue("7", forKey: "c_typ3")
        dict4.setValue("7.5", forKey: "c_typ4")
        dict4.setValue("8", forKey: "c_typ5")
        dict4.setValue("8.5", forKey: "c_typ6")
        dict4.setValue("9", forKey: "c_typ7")
        dict4.setValue("9.5", forKey: "c_typ8")
        dict4.setValue("10", forKey: "c_typ9")
        dict4.setValue("10.5", forKey: "c_typ10")
        dict4.setValue("11", forKey: "c_typ11")
        dict4.setValue("11.5", forKey: "c_typ12")
        dict4.setValue("12", forKey: "c_typ13")
        dict4.setValue("12.5", forKey: "c_typ14")
        dict4.setValue("13", forKey: "c_typ15")
        dict4.setValue("14", forKey: "c_typ16")
        dict4.setValue("", forKey: "c_typ17")
        dict4.setValue("", forKey: "c_typ18")
        dict4.setValue("", forKey: "c_typ19")
        dict4.setValue("", forKey: "c_typ20")
        dict4.setValue("", forKey: "c_typ21")
        
        let dict5 = NSMutableDictionary()
        dict5.setValue("FILA", forKey: "category")
        dict5.setValue("6.5", forKey: "c_typ1")
        dict5.setValue("7", forKey: "c_typ2")
        dict5.setValue("7.5", forKey: "c_typ3")
        dict5.setValue("8", forKey: "c_typ4")
        dict5.setValue("8.5", forKey: "c_typ5")
        dict5.setValue("9", forKey: "c_typ6")
        dict5.setValue("10", forKey: "c_typ7")
        dict5.setValue("10.5", forKey: "c_typ8")
        dict5.setValue("11", forKey: "c_typ9")
        dict5.setValue("11.5", forKey: "c_typ10")
        dict5.setValue("", forKey: "c_typ11")
        dict5.setValue("12", forKey: "c_typ12")
        dict5.setValue("", forKey: "c_typ13")
        dict5.setValue("", forKey: "c_typ14")
        dict5.setValue("", forKey: "c_typ15")
        dict5.setValue("", forKey: "c_typ16")
        dict5.setValue("", forKey: "c_typ17")
        dict5.setValue("", forKey: "c_typ18")
        dict5.setValue("", forKey: "c_typ19")
        dict5.setValue("", forKey: "c_typ20")
        dict5.setValue("", forKey: "c_typ21")
        
        let dict6 = NSMutableDictionary()
        dict6.setValue("UNDERARMUR", forKey: "category")
        dict6.setValue("", forKey: "c_typ1")
        dict6.setValue("", forKey: "c_typ2")
        dict6.setValue("7", forKey: "c_typ3")
        dict6.setValue("7.5", forKey: "c_typ4")
        dict6.setValue("8", forKey: "c_typ5")
        dict6.setValue("8.5", forKey: "c_typ6")
        dict6.setValue("9", forKey: "c_typ7")
        dict6.setValue("9.5", forKey: "c_typ8")
        dict6.setValue("10", forKey: "c_typ9")
        dict6.setValue("10.5", forKey: "c_typ10")
        dict6.setValue("11", forKey: "c_typ11")
        dict6.setValue("11.5", forKey: "c_typ12")
        dict6.setValue("12", forKey: "c_typ13")
        dict6.setValue("12.5", forKey: "c_typ14")
        dict6.setValue("13", forKey: "c_typ15")
        dict6.setValue("13.5", forKey: "c_typ16")
        dict6.setValue("14", forKey: "c_typ17")
        dict6.setValue("14.5", forKey: "c_typ18")
        dict6.setValue("15", forKey: "c_typ19")
        dict6.setValue("15.5", forKey: "c_typ20")
        dict6.setValue("16", forKey: "c_typ21")
        
        let dict7 = NSMutableDictionary()
        dict7.setValue("ASICS", forKey: "category")
        dict7.setValue("", forKey: "c_typ1")
        dict7.setValue("6", forKey: "c_typ2")
        dict7.setValue("6.5", forKey: "c_typ3")
        dict7.setValue("7", forKey: "c_typ4")
        dict7.setValue("7.5", forKey: "c_typ5")
        dict7.setValue("8", forKey: "c_typ6")
        dict7.setValue("8.5", forKey: "c_typ7")
        dict7.setValue("9", forKey: "c_typ8")
        dict7.setValue("9.5", forKey: "c_typ9")
        dict7.setValue("10", forKey: "c_typ10")
        dict7.setValue("10.5", forKey: "c_typ11")
        dict7.setValue("11", forKey: "c_typ12")
        dict7.setValue("", forKey: "c_typ13")
        dict7.setValue("11.5", forKey: "c_typ14")
        dict7.setValue("", forKey: "c_typ15")
        dict7.setValue("", forKey: "c_typ16")
        dict7.setValue("", forKey: "c_typ17")
        dict7.setValue("", forKey: "c_typ18")
        dict7.setValue("", forKey: "c_typ19")
        dict7.setValue("", forKey: "c_typ20")
        dict7.setValue("", forKey: "c_typ21")
        
        
        let dict8 = NSMutableDictionary()
        dict8.setValue("REEBOK", forKey: "category")
        dict8.setValue("", forKey: "c_typ1")
        dict8.setValue("6.5", forKey: "c_typ2")
        dict8.setValue("7", forKey: "c_typ3")
        dict8.setValue("7.5", forKey: "c_typ4")
        dict8.setValue("8", forKey: "c_typ5")
        dict8.setValue("8.5", forKey: "c_typ6")
        dict8.setValue("9", forKey: "c_typ7")
        dict8.setValue("9.5", forKey: "c_typ8")
        dict8.setValue("10", forKey: "c_typ9")
        dict8.setValue("10.5", forKey: "c_typ10")
        dict8.setValue("11", forKey: "c_typ11")
        dict8.setValue("11.5", forKey: "c_typ12")
        dict8.setValue("12", forKey: "c_typ13")
        dict8.setValue("12.5", forKey: "c_typ14")
        dict8.setValue("13", forKey: "c_typ15")
        dict8.setValue("13.5", forKey: "c_typ16")
        dict8.setValue("14", forKey: "c_typ17")
        dict8.setValue("14.5", forKey: "c_typ18")
        dict8.setValue("15", forKey: "c_typ19")
        dict8.setValue("", forKey: "c_typ20")
        dict8.setValue("", forKey: "c_typ21")
        
        let dict9 = NSMutableDictionary()
        dict9.setValue("New Balance", forKey: "category")
        dict9.setValue("6", forKey: "c_typ1")
        dict9.setValue("6.5", forKey: "c_typ2")
        dict9.setValue("7", forKey: "c_typ3")
        dict9.setValue("7.5", forKey: "c_typ4")
        dict9.setValue("8", forKey: "c_typ5")
        dict9.setValue("8.5", forKey: "c_typ6")
        dict9.setValue("9", forKey: "c_typ7")
        dict9.setValue("9.5", forKey: "c_typ8")
        dict9.setValue("10", forKey: "c_typ9")
        dict9.setValue("", forKey: "c_typ10")
        dict9.setValue("11", forKey: "c_typ11")
        dict9.setValue("11.5", forKey: "c_typ12")
        dict9.setValue("12", forKey: "c_typ13")
        dict9.setValue("12.5", forKey: "c_typ14")
        dict9.setValue("", forKey: "c_typ15")
        dict9.setValue("", forKey: "c_typ16")
        dict9.setValue("14", forKey: "c_typ17")
        dict9.setValue("", forKey: "c_typ18")
        dict9.setValue("15", forKey: "c_typ19")
        dict9.setValue("", forKey: "c_typ20")
        dict9.setValue("16", forKey: "c_typ21")
        
        let dict10 = NSMutableDictionary()
        dict10.setValue("VANS", forKey: "category")
        dict10.setValue("", forKey: "c_typ1")
        dict10.setValue("6.5", forKey: "c_typ2")
        dict10.setValue("7", forKey: "c_typ3")
        dict10.setValue("7.5", forKey: "c_typ4")
        dict10.setValue("8", forKey: "c_typ5")
        dict10.setValue("8.5", forKey: "c_typ6")
        dict10.setValue("9", forKey: "c_typ7")
        dict10.setValue("9.5", forKey: "c_typ8")
        dict10.setValue("10", forKey: "c_typ9")
        dict10.setValue("10.5", forKey: "c_typ10")
        dict10.setValue("11", forKey: "c_typ11")
        dict10.setValue("11.5", forKey: "c_typ12")
        dict10.setValue("12", forKey: "c_typ13")
        dict10.setValue("", forKey: "c_typ14")
        dict10.setValue("13", forKey: "c_typ15")
        dict10.setValue("", forKey: "c_typ16")
        dict10.setValue("14", forKey: "c_typ17")
        dict10.setValue("", forKey: "c_typ18")
        dict10.setValue("15", forKey: "c_typ19")
        dict10.setValue("", forKey: "c_typ20")
        dict10.setValue("16", forKey: "c_typ21")
        
        
        let dict11 = NSMutableDictionary()
        dict11.setValue("CONVERS", forKey: "category")
        dict11.setValue("", forKey: "c_typ1")
        dict11.setValue("6", forKey: "c_typ2")
        dict11.setValue("6.5", forKey: "c_typ3")
        dict11.setValue("7", forKey: "c_typ4")
        dict11.setValue("7.5", forKey: "c_typ5")
        dict11.setValue("8", forKey: "c_typ6")
        dict11.setValue("8.5", forKey: "c_typ7")
        dict11.setValue("9", forKey: "c_typ8")
        dict11.setValue("9.5", forKey: "c_typ9")
        dict11.setValue("10", forKey: "c_typ10")
        dict11.setValue("10.5", forKey: "c_typ11")
        dict11.setValue("11", forKey: "c_typ12")
        dict11.setValue("11.5", forKey: "c_typ13")
        dict11.setValue("12", forKey: "c_typ14")
        dict11.setValue("", forKey: "c_typ15")
        dict11.setValue("13", forKey: "c_typ16")
        dict11.setValue("", forKey: "c_typ17")
        dict11.setValue("", forKey: "c_typ18")
        dict11.setValue("", forKey: "c_typ19")
        dict11.setValue("", forKey: "c_typ20")
        dict11.setValue("", forKey: "c_typ21")
        
        arrSizechart.add(dict1)
        arrSizechart.add(dict2)
        arrSizechart.add(dict3)
        arrSizechart.add(dict4)
        arrSizechart.add(dict5)
        arrSizechart.add(dict6)
        arrSizechart.add(dict7)
        arrSizechart.add(dict8)
        arrSizechart.add(dict9)
        arrSizechart.add(dict10)
        arrSizechart.add(dict11)
        
        if (arrSizechart.count > 0) {
            
            sizecollection.delegate = self
            sizecollection.dataSource = self
            sizecollection.reloadData()
            self.view.layoutIfNeeded()
          self.constrraintHeight.constant =  CGFloat((arrSizechart.count * 50)+50)
            self.view.layoutIfNeeded()
            self.sizecollection.reloadData()

        }
    }
    @IBAction func btnBackToproductAction(_ sender:UIButton){
        
        if let arrayViewCs = self.navigationController?.viewControllers {
                     for viewC in arrayViewCs {
                         if viewC.isKind(of: productViewController.classForCoder()) {
                             self.navigationController?.popToViewController(viewC, animated: true)
                         }
                     }
                 }
           
       }

   
}
extension UIStackView {
    func addBorder(color: UIColor, backgroundColor: UIColor, thickness: CGFloat) {
        let insetView = UIView(frame: bounds)
        insetView.backgroundColor = backgroundColor
        insetView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        insertSubview(insetView, at: 0)

        let borderBounds = CGRect(
            x: thickness,
            y: thickness,
            width: frame.size.width - thickness * 2,
            height: frame.size.height - thickness * 2)

        let borderView = UIView(frame: borderBounds)
        borderView.backgroundColor = color
        borderView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        insertSubview(borderView, at: 0)
    }
}

