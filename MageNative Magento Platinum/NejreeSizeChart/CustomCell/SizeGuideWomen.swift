//
//  SizeGuideWomen.swift
//  MageNative Magento Platinum
//
//  Created by pratima on 06/05/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class SizeGuideWomen: UICollectionViewCell {
    @IBOutlet weak var lblCM: UILabel!
    @IBOutlet weak var lbl241: UILabel!
       @IBOutlet weak var lbl242: UILabel!
       @IBOutlet weak var lbl251: UILabel!
       @IBOutlet weak var lbl252: UILabel!
       @IBOutlet weak var lbl261: UILabel!
       @IBOutlet weak var lbl262: UILabel!
       @IBOutlet weak var lbl271: UILabel!
       @IBOutlet weak var lbl272: UILabel!
       @IBOutlet weak var lbl281: UILabel!
       @IBOutlet weak var lbl282: UILabel!
       @IBOutlet weak var lbl291: UILabel!
       @IBOutlet weak var lbl292: UILabel!
       @IBOutlet weak var lbl301: UILabel!
       @IBOutlet weak var lbl302: UILabel!
       @IBOutlet weak var lbl311: UILabel!
       @IBOutlet weak var lbl312: UILabel!
      @IBOutlet weak var viewstack: UIStackView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
            lblCM.setCornerLabel()
              lbl241.setCornerLabel()
              lbl242.setCornerLabel()
              lbl251.setCornerLabel()
              lbl252.setCornerLabel()
              lbl261.setCornerLabel()
              lbl262.setCornerLabel()
              lbl271.setCornerLabel()
              lbl272.setCornerLabel()
              lbl281.setCornerLabel()
              lbl282.setCornerLabel()
              lbl291.setCornerLabel()
              lbl292.setCornerLabel()
              lbl301.setCornerLabel()
              lbl302.setCornerLabel()
              lbl311.setCornerLabel()
              lbl312.setCornerLabel()
    }

}
