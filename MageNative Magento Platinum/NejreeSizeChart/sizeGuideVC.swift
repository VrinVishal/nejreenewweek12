//
//  sizeGuideVC.swift
//  MageNative Magento Platinum
//
//  Created by pratima on 05/05/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit
import FirebaseAnalytics


class sizeGuideVC: MagenativeUIViewController {
    
    
    @IBOutlet weak var headingLabel: UILabel!
    
    @IBOutlet weak var lblTitleText: UILabel!
    
    @IBOutlet weak var lblView1: UILabel!
    @IBOutlet weak var lblView2: UILabel!
    @IBOutlet weak var lblView3: UILabel!
    @IBOutlet weak var lblView4: UILabel!
    @IBOutlet weak var lblView5: UILabel!
    
    @IBOutlet weak var View1: UIView!
    @IBOutlet weak var View2: UIView!
    @IBOutlet weak var View3: UIView!
    @IBOutlet weak var View4: UIView!
    @IBOutlet weak var View5: UIView!
    
    @IBOutlet weak var imageFirstView1: UIImageView!
    @IBOutlet weak var imageFirstView2: UIImageView!
    @IBOutlet weak var imageFirstView3: UIImageView!
    @IBOutlet weak var imageFirstView4: UIImageView!
    @IBOutlet weak var imageFirstView5: UIImageView!
    
    
    @IBOutlet weak var imageSecondView1: UIImageView!
    @IBOutlet weak var imageSecondView2: UIImageView!
    @IBOutlet weak var imageSecondView3: UIImageView!
    @IBOutlet weak var imageSecondView4: UIImageView!
    @IBOutlet weak var imageSecondView5: UIImageView!
    
    @IBOutlet weak var btn1: UIButton!
    @IBOutlet weak var btn2: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //UIView.appearance().semanticContentAttribute = .forceLeftToRight
        
         self.navigationController?.setNavigationBarHidden(false, animated: true)
        
        btn1.backgroundColor = .black
        btn1.roundCorners()
        btn1.layer.borderWidth = 1.0
        btn1.layer.borderColor = UIColor(red: 251.0/255.0, green: 173.0/255.0, blue: 24.0/255.0, alpha: 1.0).cgColor
       // btn1.setTitleColor(UIColor(red: 251.0/255.0, green: 173.0/255.0, blue: 24.0/255.0, alpha: 1.0), for: .normal)
        btn1.setTitleColor(UIColor.white, for: .normal)
        
        
        btn2.backgroundColor = .black
        btn2.roundCorners()
        btn2.layer.borderWidth = 1.0
        btn2.layer.borderColor = UIColor(red: 251.0/255.0, green: 173.0/255.0, blue: 24.0/255.0, alpha: 1.0).cgColor
        //btn2.setTitleColor(UIColor(red: 251.0/255.0, green: 173.0/255.0, blue: 24.0/255.0, alpha: 1.0), for: .normal)
        btn2.setTitleColor(UIColor.white, for: .normal)
        
        
      //  UIView.appearance().semanticContentAttribute = .forceLeftToRight
    }
    
    @IBAction func btn1Action(_ sender:UIButton){
        //MEN
        let vc = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "SizeGuideMenVC") as! SizeGuideMenVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btn2Action(_ sender:UIButton){
        //WOMEN
         let vc = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "SizeGuideWomenVC") as! SizeGuideWomenVC
        self.navigationController?.pushViewController(vc, animated: true)
    }

}
