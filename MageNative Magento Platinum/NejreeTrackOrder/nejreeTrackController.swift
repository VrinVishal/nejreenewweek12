//
//  nejreeTrackController.swift
//  MageNative Magento Platinum
//
//  Created by cedcoss on 15/10/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit
import SafariServices
class nejreeTrackController: cedMageViewController,SFSafariViewControllerDelegate {

    @IBOutlet weak var trackTable: UITableView!
    var status = String()
    var trackingUrl = String()
    var trackingId = String()
    var orderNumber = String()
    var shipmentNumber = String()
    var orderDate = String()
    var rmaStatus = String()
    var orderProduct = [orderItems]()
    
   
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        print("\(status)")
        if status == ""{
            status = "0";
        }
        
        self.navigationController?.navigationItem.title = ""
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        
        trackTable.delegate = self
        trackTable.dataSource = self
        //trackTable.separatorStyle = .none
        // Do any additional setup after loading the view.
        
       
        
        getGiftRedeem()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    override func viewWillAppear(_ animated: Bool) {
        
         super.viewWillAppear(animated)
       
       self.navigationController?.setNavigationBarHidden(true, animated: true)
    
    }
    func getGiftRedeem() {

        let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();

        var postData = [String:String]()
        postData["order_id"] = orderNumber
        postData["customer_id"] = userInfoDict["customerId"]!;

        API().callAPI(endPoint: "giftredeem", method: .POST, param: postData) { (json_res, err) in

          //  DispatchQueue.main.async {
                
                if err == nil {

                    arrGiftRedem.removeAll()

                    do {

                        let catData = try json_res[0]["result"].rawData()
                        arrGiftRedem = try JSONDecoder().decode([GiftRedem].self, from: catData)

                    } catch { }
                }
         //   }
        }
    }
    

}

extension nejreeTrackController: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if indexPath.row == 0 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "nejreeMyOrderImageCell", for: indexPath) as! nejreeLoginImageCell
            cell.btnBack.addTarget(self, action: #selector(dismissButtonTapped(_:)), for: .touchUpInside)

            if(APP_DEL.isLoginFromWishList){

                cell.btnBack.isHidden = true

            }
            else{
                cell.btnBack.isHidden = false
            }

            let value=UserDefaults.standard.value(forKey: "AppleLanguages") as! [String]
            if value[0] == "ar" {

                cell.btnBack.setImage(UIImage(named: "icon_back_white_Arabic"), for: .normal)

            } else {

                cell.btnBack.setImage(UIImage(named: "icon_back_white_round"), for: .normal)
            }



            cell.lblSignIn.text = APP_LBL().track_order.uppercased()

            return cell
            
        }else if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "orderItemsCell", for: indexPath) as! orderItemsCell
            //            cell.productImage.sd_setImage(with: URL(string: orderProducts[indexPath.row].product_image!), placeholderImage: UIImage(named: "placeholder"))
                        let value = UserDefaults.standard.value(forKey: "AppleLanguages") as! [String]
                        
                        if value[0] == "ar" {
                            cell.lblOrderItem.textAlignment = .right
                            cell.lblOrderDate.textAlignment = .right
                        } else {
                            cell.lblOrderItem.textAlignment = .left
                            cell.lblOrderDate.textAlignment = .left
                        }
                        cell.lblOrderItem.text = ""
                        cell.selectionStyle = .none
                        cell.productList = orderProduct
                        cell.rmaStatus = rmaStatus
                        cell.manageCarousel()
                        cell.parentVC = self
                        cell.lblOrderDate.text = ""
          
                                    
                        return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "trackingCell", for: indexPath) as! trackingCell
                    //cell.orderRecievedView.cardView()
            
            
            cell.trackingNumberHeading.isHidden = true
            cell.trackingNumber.isHidden = true
            cell.youCanTrackLabel.isHidden = true
            cell.trackUrlButton.isHidden = true
            
            cell.trackingNumberHeading.text = APP_LBL().tracking_number.uppercased() + "  " +  self.shipmentNumber
            cell.youCanTrackLabel.text = APP_LBL().you_can_track_your_order_from.uppercased()
            
                    if status == "0" {//"Pending" || status == "Canceled" ||  status == "Closed" {
                        
//                        cell.orderRecievedView.cardView()
                        cell.orderReceivedHeading.text = APP_LBL().order_received.uppercased()
                        cell.orderNumber.text = orderNumber//"ORDER #: " + orderNumber
                        cell.orderDate.text = self.orderDate
                        cell.orderRecievedDot.image = UIImage(named: "trakingOval")
                        cell.orderRecievedDot.tintColor = nejreeColor
                        cell.orderRecievedDot.clipsToBounds = true
                        cell.orderRecievedDot.layer.cornerRadius = 7.5
                        cell.orderRecievedDot.imageShadow()
                        
                        cell.orderLine.backgroundColor = nejreeColor
                        
                        cell.processingHeading.text = APP_LBL().processing.uppercased()
                        cell.processingHeading.textColor = .darkGray
                        cell.processingSubheading.text = APP_LBL().awaiting_to_process_your_order.uppercased()
                        cell.processingSubheading.textColor = .gray
                        cell.processingDot.image = UIImage(named: "trakingOval")
                        cell.processingDot.tintColor = .lightGray
                        
                        cell.processingDot.clipsToBounds = true
                        cell.processingDot.layer.cornerRadius = 7.5
                        
                        cell.shippingHeading.textColor = .lightGray
                        cell.shippingHeading.text = APP_LBL().shipping.uppercased()
                        cell.shippingSubheading.text = APP_LBL().awaiting_to_ship_your_sneakers.uppercased()
                        cell.shippingSubheading.textColor = .lightGray
                        //cell.shippingViewHeight.constant = 120
                        cell.shippingDot.image = UIImage(named: "trakingOval")
                        cell.shippingDot.tintColor = .lightGray
                        
                        
                        
                        
                        cell.deliveredHeading.text = APP_LBL().delivered.uppercased()
                        cell.deliveredHeading.textColor = .lightGray
                        cell.deliveredSubheading.text = APP_LBL().your_shipment_will_be_delivered_soon.uppercased()
                        cell.deliveredSubheading.textColor = .lightGray
                        cell.deliveredDot.image = UIImage(named: "trakingOval")
                        cell.deliveredDot.tintColor = .lightGray
                        
                    }
                    
                    if status == "1" {//"Processing" {
                        
                        
                        cell.orderReceivedHeading.text = APP_LBL().order_received.uppercased()
                        cell.orderNumber.text = orderNumber//"ORDER #: " + orderNumber
                        cell.orderDate.text = self.orderDate
                        cell.orderRecievedDot.image = UIImage(named: "trakingOval")
                        cell.orderRecievedDot.tintColor = nejreeColor
                        cell.orderRecievedDot.clipsToBounds = true
                        cell.orderRecievedDot.layer.cornerRadius = 7.5
                        //cell.orderRecievedDot.imageShadow()
                        cell.orderLine.backgroundColor = nejreeColor
                        
//                        cell.processingView.cardView()
                        cell.processingHeading.text = APP_LBL().processed.uppercased()
                        cell.processingHeading.textColor = nejreeColor
                        cell.processingSubheading.text = APP_LBL().your_order_has_been_processed.uppercased()
                        cell.processingSubheading.textColor = .gray
                        cell.processingDot.image = UIImage(named: "trakingOval")
//                        cell.processingSubheading.cardView()
                        cell.processingDot.tintColor = nejreeColor
                        cell.processingDot.clipsToBounds = true
                        cell.processingDot.layer.cornerRadius = 7.5
                        cell.processingDot.imageShadow()
                        cell.processingLine.backgroundColor = nejreeColor
                        
                        
                        cell.shippingHeading.textColor = .darkGray
                        cell.shippingHeading.text = APP_LBL().shipping.uppercased()
                        cell.shippingSubheading.text = APP_LBL().awaiting_to_ship_your_sneakers.uppercased()
                        cell.shippingSubheading.textColor = .lightGray
                        //cell.shippingViewHeight.constant = 120
                        
                        
                        cell.deliveredHeading.text = APP_LBL().delivered.uppercased()
                        cell.deliveredHeading.textColor = .lightGray
                        cell.deliveredSubheading.text = APP_LBL().your_shipment_will_be_delivered_soon.uppercased()
                        cell.deliveredSubheading.textColor = .lightGray
                    }
                    
                    if status == "2" {//"Shipped" {
                        
                        cell.orderReceivedHeading.text = APP_LBL().order_received.uppercased()
                        cell.orderNumber.text = orderNumber//"ORDER #: " + orderNumber
                        cell.orderDate.text = self.orderDate
                        cell.orderRecievedDot.image = UIImage(named: "trakingOval")
                        cell.orderRecievedDot.tintColor = nejreeColor
                        cell.orderRecievedDot.clipsToBounds = true
                        cell.orderRecievedDot.layer.cornerRadius = 7.5
                     //   cell.orderRecievedDot.imageShadow()
                        cell.orderLine.backgroundColor = nejreeColor
                        
                        
                        //cell.processingView.cardView()
                        cell.processingHeading.text = APP_LBL().processed.uppercased()
                        cell.processingHeading.textColor = nejreeColor
                        cell.processingSubheading.text = APP_LBL().your_order_has_been_processed.uppercased()
                        cell.processingSubheading.textColor = .gray
                        cell.processingDot.image = UIImage(named: "trakingOval")
                        cell.processingDot.tintColor = nejreeColor
                        cell.processingDot.clipsToBounds = true
                        cell.processingDot.layer.cornerRadius = 7.5
                    //    cell.processingDot.imageShadow()
                        cell.processingLine.backgroundColor = nejreeColor
                        
//                        cell.shippingView.cardView()
                        cell.shippingHeading.textColor = nejreeColor
                        cell.shippingHeading.text = APP_LBL().shipping.uppercased()
                        cell.shippingSubheading.text = APP_LBL().your_sneakers_have_been_shipped.uppercased()
                        cell.shippingSubheading.textColor = .lightGray
                        cell.shippingLine.backgroundColor = nejreeColor
                        cell.shippingDot.image = UIImage(named: "trakingOval")
                        cell.shippingDot.tintColor = nejreeColor
                        cell.shippingDot.clipsToBounds = true
                        cell.shippingDot.layer.cornerRadius = 7.5
                        cell.shippingDot.imageShadow()
                        cell.trackingNumberHeading.isHidden = false
                        cell.trackingNumber.isHidden = true
                        
                        cell.trackUrlButton.setTitle(APP_LBL().tracking_details.uppercased(), for: .normal)
                        cell.trackUrlButton.addTarget(self, action: #selector(trackUrlButtonTapped(_:)), for: .touchUpInside)
                       
                       
                        cell.deliveredHeading.text = APP_LBL().delivered.uppercased()
                        cell.deliveredHeading.textColor = .darkGray
                        cell.deliveredSubheading.text = APP_LBL().your_shipment_has_been_delivered.uppercased()
                        cell.deliveredSubheading.textColor = .lightGray
                        
                        
                        if self.trackingUrl != ""{
                            cell.youCanTrackLabel.isHidden = false
                            cell.trackUrlButton.isHidden = false
                            cell.trackingNumberHeading.isHidden = false
                        }
                        else
                        {
                            cell.youCanTrackLabel.isHidden = true
                            cell.trackUrlButton.isHidden = true
                            cell.trackingNumberHeading.isHidden = true
                        }
                        
                    }
                    if status == "3" {//"Shipped" {
                        
                        cell.orderReceivedHeading.text = APP_LBL().order_received.uppercased()
                        cell.orderNumber.text = orderNumber//"ORDER #: " + orderNumber
                        cell.orderDate.text = self.orderDate
                        cell.orderRecievedDot.image = UIImage(named: "trakingOval")
                        cell.orderRecievedDot.tintColor = nejreeColor
                        cell.orderLine.backgroundColor = nejreeColor
                        
                        
                        //cell.processingView.cardView()
                        cell.processingHeading.text = APP_LBL().processed.uppercased()
                        cell.processingHeading.textColor = nejreeColor
                        cell.processingSubheading.text = APP_LBL().your_order_has_been_processed.uppercased()
                        cell.processingSubheading.textColor = .gray
                        cell.processingDot.image = UIImage(named: "trakingOval")
                        cell.processingDot.tintColor = nejreeColor
                        cell.processingLine.backgroundColor = nejreeColor
                        
//                        cell.shippingView.cardView()
                        cell.shippingHeading.textColor = nejreeColor
                        cell.shippingHeading.text = APP_LBL().shipping.uppercased()
                        cell.shippingSubheading.text = APP_LBL().your_sneakers_has_been_shipped.uppercased()
                        cell.shippingSubheading.textColor = .lightGray
                        cell.shippingLine.backgroundColor = nejreeColor
                        cell.shippingDot.image = UIImage(named: "trakingOval")
                        cell.shippingDot.tintColor = nejreeColor
                        cell.trackingNumberHeading.isHidden = false
                       
                        cell.trackUrlButton.setTitle(APP_LBL().tracking_details.uppercased(), for: .normal)
                        cell.trackUrlButton.addTarget(self, action: #selector(trackUrlButtonTapped(_:)), for: .touchUpInside)
                        cell.deliveredHeading.text = APP_LBL().delivered.uppercased()

                        cell.deliveredHeading.textColor = nejreeColor
                        cell.deliveredSubheading.text = APP_LBL().your_shipment_has_been_delivered.uppercased()
                        cell.deliveredSubheading.textColor = .lightGray
                        
                        cell.deliveredDot.image = UIImage(named: "trakingOval")
                        cell.deliveredDot.tintColor = nejreeColor
                         cell.trackingNumber.isHidden = true
                        
                        
                        if self.trackingUrl != ""{
                            cell.youCanTrackLabel.isHidden = false
                            cell.trackUrlButton.isHidden = false
                            cell.trackingNumberHeading.isHidden = false
                        }
                        else
                        {
                            cell.youCanTrackLabel.isHidden = true
                            cell.trackUrlButton.isHidden = true
                            cell.trackingNumberHeading.isHidden = true
                        }
                        
                        
                       
                     
                        
                    }

                    cell.selectionStyle = .none
                    cell.parent = self
                    cell.trackingUrl = self.trackingUrl
            
            
                    let value=UserDefaults.standard.value(forKey: "AppleLanguages") as! [String]
            if value[0] == "ar" {
                cell.orderReceivedHeading.textAlignment = .right
                cell.orderNumber.textAlignment = .right
                cell.orderDate.textAlignment = .right
                
                cell.processingHeading.textAlignment = .right
                cell.processingSubheading.textAlignment = .right
                
                cell.shippingHeading.textAlignment = .right
                cell.shippingSubheading.textAlignment = .right
                cell.youCanTrackLabel.textAlignment = .right
                
                cell.deliveredHeading.textAlignment = .right
                cell.deliveredSubheading.textAlignment = .right
                
                cell.trackingNumberHeading.textAlignment = .right
                
            } else {
                
                cell.orderReceivedHeading.textAlignment = .left
                cell.orderNumber.textAlignment = .left
                cell.orderDate.textAlignment = .left
                
                cell.processingHeading.textAlignment = .left
                cell.processingSubheading.textAlignment = .left
                
                cell.shippingHeading.textAlignment = .left
                cell.shippingSubheading.textAlignment = .left
                cell.youCanTrackLabel.textAlignment = .left
                
                cell.deliveredHeading.textAlignment = .left
                cell.deliveredSubheading.textAlignment = .left
                
                cell.trackingNumberHeading.textAlignment = .left
            }
         //    cell.trackOrderButton.addTarget(self, action: #selector(trackDetailClick(_:)), for: .touchUpInside)
            cell.trackOrderButton.isHidden = true
          //  cell.trackOrderButton.setTitle(APP_LBL().tracking_details.uppercased(), for: .normal)
            
                    return cell
        }
        
        
    }
    
    
    @IBAction func trackUrlButtonTapped (_ sender : UIButton){
            let safariVC = SFSafariViewController(url: NSURL(string: self.trackingUrl)! as URL)
            safariVC.modalPresentationStyle = .overFullScreen
            safariVC.modalTransitionStyle = .crossDissolve
            safariVC.delegate = self
            self.present(safariVC, animated: true, completion: nil)
       }
    
    func safariViewControllerDidFinish(controller: SFSafariViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func dismissButtonTapped (_ sender : UIButton){
            self.navigationController?.popViewController(animated: true)
           self.dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
    
    
    
    
    
}
extension UIView {

    func dropShadow() {
        /*self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.5
        self.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.layer.shadowRadius = 1
        self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale*/
        let viewShadow = UIView(frame: self.frame)
        viewShadow.center = self.center
        viewShadow.backgroundColor = UIColor.yellow
        viewShadow.layer.shadowColor = UIColor.red.cgColor
        viewShadow.layer.shadowOpacity = 1
        viewShadow.layer.shadowOffset = CGSize.zero
        viewShadow.layer.shadowRadius = 5
        viewShadow.layer.masksToBounds = false;
        self.addSubview(viewShadow)
        

    }
}
