//
//  trackingCell.swift
//  MageNative Magento Platinum
//
//  Created by cedcoss on 15/10/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit
class trackingCell: UITableViewCell {

    @IBOutlet weak var cellview: UIView!
    // MARK: - Order Received
    @IBOutlet weak var orderReceivedHeading: UILabel!
    @IBOutlet weak var orderNumber: UILabel!
    @IBOutlet weak var orderDate: UILabel!
    @IBOutlet weak var orderRecievedDot: UIImageView!
    @IBOutlet weak var orderRecievedView: UIView!
    
    // MARK:- processing
    @IBOutlet weak var processingHeading: UILabel!
    @IBOutlet weak var processingSubheading: UILabel!
    @IBOutlet weak var processingDot: UIImageView!
    @IBOutlet weak var processingView: UIView!
    
    // MARK:- shipping
    @IBOutlet weak var shippingHeading: UILabel!
    @IBOutlet weak var shippingSubheading: UILabel!
    @IBOutlet weak var trackingNumber: UILabel!
    @IBOutlet weak var trackUrlButton: UIButton!
    @IBOutlet weak var shippingDot: UIImageView!
    @IBOutlet weak var shippingView: UIView!
    @IBOutlet weak var trackingNumberHeading: UILabel!
    @IBOutlet weak var youCanTrackLabel: UILabel!
    
    
    // MARK: - Delivered
    @IBOutlet weak var deliveredHeading: UILabel!
    @IBOutlet weak var deliveredSubheading: UILabel!
    @IBOutlet weak var deliveredDot: UIImageView!
    @IBOutlet weak var deliveredView: UIView!
    
  

    
    @IBOutlet weak var orderRecievedHeight: NSLayoutConstraint!
    @IBOutlet weak var proceesingViewHeight: NSLayoutConstraint!
    @IBOutlet weak var shippingViewHeight: NSLayoutConstraint!
    @IBOutlet weak var deliveryViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var orderLine: UIView!
    @IBOutlet weak var processingLine: UIView!
    @IBOutlet weak var shippingLine: UIView!
    
     @IBOutlet weak var trackOrderButton: UIButton!
    
    var trackingUrl = String()
    var parent: UIViewController!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
       
        
    }
   
//    @objc func trackUrlButtonTapped(_ sender: UIButton) {
//
//        let safariVC = SFSafariViewController(url: NSURL(string: trackingUrl)! as URL)
//        safariVC.modalPresentationStyle = .overFullScreen
//        safariVC.modalTransitionStyle = .crossDissolve
//        safariVC.delegate = self
//        self.present(safariVC, animated: true, completion: nil)
//
//
////        let view = UIStoryboard(name: "cedMageAccounts", bundle: nil)
////        let viewControl = view.instantiateViewController(withIdentifier: "cmsWebView") as! cedMageCmsWebView
////        let url = trackingUrl
////        viewControl.pageUrl = url
////        parent.navigationController?.pushViewController(viewControl, animated: true)
//    }
    
}
