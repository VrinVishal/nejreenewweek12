//
//  NejreeCategorySubVC.swift
//  MageNative Magento Platinum
//
//  Created by vishal.n on 03/02/21.
//  Copyright © 2021 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class NejreeCategorySubVC: MagenativeUIViewController, UIGestureRecognizerDelegate {
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var imgViewBackArrow: UIImageView!

    var category: CategoryLayer?
    var arrSubCategory = [CategoryLayer]()

    override func viewDidLoad() {
        super.viewDidLoad()

        self.setUI()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.navigationBar.isHidden = true
        self.navigationController?.isNavigationBarHidden = true
        
        if navigationController?.responds(to: #selector(getter: UINavigationController.interactivePopGestureRecognizer)) ?? false {
            
            self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
            self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        }
        
       
        if APP_DEL.selectedLanguage == Arabic{
            imgViewBackArrow.image = UIImage(named: "icon_back_white_Arabic")
        }
        else{
            imgViewBackArrow.image = UIImage(named: "icon_back_white_round")
        }
    }
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        
        return true
    }
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func setUI() {
        
        lblTitle.text = category?.main_category_name ?? ""
        tblView.register(UINib(nibName: "CategorySubCell", bundle: nil), forCellReuseIdentifier: "CategorySubCell")
        tblView.delegate = self
        tblView.dataSource = self
        tblView.reloadData()
    }

}

extension NejreeCategorySubVC : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrSubCategory.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategorySubCell", for: indexPath) as! CategorySubCell
        cell.selectionStyle = .none
        
        let subCat = arrSubCategory[indexPath.row]
        
        cell.lblTitle.text = subCat.main_category_name ?? ""
        
        cell.imgCategory.image = nil
        if let imgUrl = URL(string: (subCat.main_category_image ?? "")) {
            cell.imgCategory.sd_setImage(with: imgUrl, placeholderImage: nil)
        }
        
        if APP_DEL.selectedLanguage == Arabic{
            cell.lblTitle.textAlignment = .right
            cell.imgArrow.image = UIImage(named: "IQButtonBarArrowLeft")
        } else {
            cell.lblTitle.textAlignment = .left
            cell.imgArrow.image = UIImage(named: "IQButtonBarArrowRight")
        }
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let layer = self.arrSubCategory[indexPath.row]
        
        CategoryView().pushCategory(category: layer, vc: self) { (err) in }
    }
}
