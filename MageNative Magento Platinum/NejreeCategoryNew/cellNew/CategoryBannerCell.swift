//
//  CategoryBannerCell.swift
//  MageNative Magento Platinum
//
//  Created by vishal.n on 02/02/21.
//  Copyright © 2021 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class CategoryBannerCell: UICollectionViewCell {

    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var consWidth: NSLayoutConstraint!
    @IBOutlet weak var consHeight: NSLayoutConstraint!

    override func awakeFromNib() {
        super.awakeFromNib()

        lblTitle.font = UIFont(name: "Cairo-Bold", size: 22)!
        
        imgView.round(redius: 6.0)
    }
}
