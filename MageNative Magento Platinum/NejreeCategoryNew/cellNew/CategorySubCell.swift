//
//  CategorySubCell.swift
//  MageNative Magento Platinum
//
//  Created by vishal.n on 03/02/21.
//  Copyright © 2021 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class CategorySubCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgCategory: UIImageView!
    @IBOutlet weak var imgArrow: UIImageView!
    @IBOutlet weak var viewBg: UIView!
    private var shadowLayer: CAShapeLayer!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        lblTitle.font = UIFont(name: "Cairo-Bold", size: 20)!
        
        if (APP_DEL.selectedLanguage == Arabic){
            imgArrow.image = UIImage(named: "IQButtonBarArrowLeft")
        } else {
            imgArrow.image = UIImage(named: "IQButtonBarArrowRight")
        }

    }
    
    override func layoutSubviews() {
        imgCategory.round()

        viewBg.backgroundColor = .white

        viewBg.layer.cornerRadius = 2.0

        viewBg.layer.shadowColor = UIColor.gray.cgColor

        viewBg.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)

        viewBg.layer.shadowRadius = 3.0

        viewBg.layer.shadowOpacity = 0.3
//
        viewBg.layer.borderWidth = 0.2

        viewBg.layer.borderColor = UIColor.gray.cgColor
        self.clipsToBounds = false

        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
