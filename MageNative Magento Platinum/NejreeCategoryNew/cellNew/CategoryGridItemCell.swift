//
//  CategoryGridItemCell.swift
//  MageNative Magento Platinum
//
//  Created by vishal.n on 02/02/21.
//  Copyright © 2021 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class CategoryGridItemCell: UICollectionViewCell {
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var consWidth: NSLayoutConstraint!
    @IBOutlet weak var consHeight: NSLayoutConstraint!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        lblTitle.font = UIFont(name: "Cairo-Bold", size: 16)!
    }

}
