//
//  CategoryImageCell.swift
//  MageNative Magento Platinum
//
//  Created by vishal.n on 02/02/21.
//  Copyright © 2021 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class CategoryImageCell: UICollectionViewCell {

    
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var consWidth: NSLayoutConstraint!
    @IBOutlet weak var consHeight: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
