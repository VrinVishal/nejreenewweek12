//
//  CategoryGridCell.swift
//  MageNative Magento Platinum
//
//  Created by vishal.n on 02/02/21.
//  Copyright © 2021 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class CategoryGridCell: UICollectionViewCell {
    
    @IBOutlet weak var collView: UICollectionView!
    
    var arrLayer: [CategoryLayer] = []
    var itemSize : CGSize = CGSize.zero
    var itemSpace : CGFloat = 0.0
    
    var delegateDidSelectBanner: DidSelectBanner?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        collView.delegate = self
        collView.dataSource = self
        collView.register(UINib(nibName: "CategoryGridItemCell", bundle: nil), forCellWithReuseIdentifier: "CategoryGridItemCell")
        collView.reloadData()
        
    }
    
}


extension CategoryGridCell: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return arrLayer.count;
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = self.collView.dequeueReusableCell(withReuseIdentifier: "CategoryGridItemCell", for: indexPath) as! CategoryGridItemCell
        
        let data = arrLayer[indexPath.item]
        
        cell.consWidth.constant = itemSize.width
        cell.consHeight.constant = (itemSize.height - gridBottomTextHT)
        
        cell.lblTitle.text = " " + (data.main_category_name ?? "").uppercased() + " "
        
        cell.imgView.image = nil
        let imgStr = (data.main_category_image ?? "")
        if let imgUrl = URL(string: data.main_category_image ?? ""), imgStr != "" {
            cell.imgView.sd_setImage(with: imgUrl, placeholderImage: nil)
        } else {
            cell.imgView.backgroundColor = UIColor(hex: cat_bg_default_color_code)
        }
        
        cell.layer.borderColor = UIColor.lightGray.withAlphaComponent(0.6).cgColor
        cell.layer.borderWidth = 0.5
        //cell.round(redius: 6.0)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        self.delegateDidSelectBanner?.didSelectBanner(index: IndexPath(item: indexPath.item, section: self.tag))
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return itemSize
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return itemSpace
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}
