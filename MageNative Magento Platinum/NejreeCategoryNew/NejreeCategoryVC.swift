//
//  NejreeCategoryVC.swift
//  MageNative Magento Platinum
//
//  Created by vishal.n on 02/02/21.
//  Copyright © 2021 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

struct CategoryLayer : Codable {

    let main_category_id : String?
    let main_category_name : String?
    let main_category_image : String?
    let selected : Bool?
    let subcategories : [CategoryLayer]?
    let redirect_id : String?
    let sort_order : String?
    //let tags : String?

    var layer_option : String?
    var layer_ratio : String? = "1:0.4"
    var layer_height : String? = "150"
    var layer_width : String? = "150"
    var layer_item_space : String? = "10"
    var layer_top_space : String? = "10"
    var layer_bottom_space : String? = ""

    init(from decoder: Decoder) throws {

    let values = try decoder.container(keyedBy: CodingKeys.self)
    main_category_id = try values.decodeIfPresent(String.self, forKey: .main_category_id)
    main_category_name = try values.decodeIfPresent(String.self, forKey: .main_category_name)
    main_category_image = try values.decodeIfPresent(String.self, forKey: .main_category_image)
    redirect_id = try values.decodeIfPresent(String.self, forKey: .redirect_id)
    sort_order = try values.decodeIfPresent(String.self, forKey: .sort_order)
    layer_option = try values.decodeIfPresent(String.self, forKey: .layer_option)

    if let temp = try values.decodeIfPresent(String.self, forKey: .layer_ratio) { layer_ratio = temp } else { layer_ratio = "1:0.4" }
    if let temp = try values.decodeIfPresent(String.self, forKey: .layer_height) { layer_height = temp } else { layer_height = "150" }
    if let temp = try values.decodeIfPresent(String.self, forKey: .layer_width) { layer_width = temp } else { layer_width = "150" }
    if let temp = try values.decodeIfPresent(String.self, forKey: .layer_item_space) { layer_item_space = temp } else { layer_item_space = "10" }
    if let temp = try values.decodeIfPresent(String.self, forKey: .layer_top_space) { layer_top_space = temp } else { layer_top_space = "10" }
    if let temp = try values.decodeIfPresent(String.self, forKey: .layer_bottom_space) { layer_bottom_space = temp } else { layer_bottom_space = "0" }

    selected = try values.decodeIfPresent(Bool.self, forKey: .selected)
    subcategories = try values.decodeIfPresent([CategoryLayer].self, forKey: .subcategories)

    }
}

let gridBottomTextHT : CGFloat = 35

class NejreeCategoryVC: MagenativeUIViewController, UpdateCategory, UpdateSelfCategory, DidSelectBanner {
    
    @IBOutlet weak var collCategory: UICollectionView!
    
    @IBOutlet weak var collView: UICollectionView!
    @IBOutlet weak var lblNodata: UILabel!
    
    @IBOutlet weak var viewSearchBottom: UIView!
    @IBOutlet weak var lblSearch: UILabel!

    
    var arrBannerLayer: [CategoryLayer] = []
    var arrGridLayer: [CategoryLayer] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        APP_DEL.delegateUpdateSelfCategory = self
        APP_DEL.delegateUpdateCategory = self
        
        setUI()
    }
    
 
    func setUI() {
        lblSearch.font = UIFont(name: "Cairo-Regular", size: 16)!
        lblSearch.text = APP_LBL().search_for_product_brands_more
        lblSearch.textColor = UIColor.lightGray
            
        collCategory.register(UINib(nibName: "LayerHeaderWhiteCell", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "LayerHeaderWhiteCell")
        collCategory.register(UINib(nibName: "LayerHeaderWhiteCell", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: "LayerHeaderWhiteCell")
        collCategory.register(UINib(nibName: "CategoryMainCell", bundle: nil), forCellWithReuseIdentifier: "CategoryMainCell")
        collCategory.delegate = self
        collCategory.dataSource = self


        collView.register(UINib(nibName: "LayerHeaderWhiteCell", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "LayerHeaderWhiteCell")
        collView.register(UINib(nibName: "LayerHeaderWhiteCell", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: "LayerHeaderWhiteCell")
        collView.register(UINib(nibName: "CategoryBannerCell", bundle: nil), forCellWithReuseIdentifier: "CategoryBannerCell")
        collView.register(UINib(nibName: "CategoryGridCell", bundle: nil), forCellWithReuseIdentifier: "CategoryGridCell")
        collView.delegate = self
        collView.dataSource = self
        collView.keyboardDismissMode = .onDrag

        self.lblNodata.text = APP_LBL().no_data_found
        self.lblNodata.isHidden = true

        self.updateCategory(res: true)
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.navigationBar.isHidden = true
        self.navigationController?.isNavigationBarHidden = true
        
    }
    
    //MARK:- ACTION
    @IBAction func btnSearchNew(_ sender: UIButton) {
        let transition:CATransition = CATransition()
        transition.duration = 0.0
        transition.timingFunction = CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType.fade
        transition.subtype = CATransitionSubtype.fromTop
        self.navigationController?.view.layer.add(transition, forKey: kCATransition)
        if let viewController =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "cedMageSearchPage") as?  cedMageSearchPage {
            viewController.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(viewController, animated: false)
        }
    }
    
    @IBAction func btnSearchBottomAction(_ sender: Any) {

        if let viewController =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "cedMageSearchPage") as?  cedMageSearchPage {

            viewController.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(viewController, animated: true)

        }
    }
    
    func checkNoData() {
        if ((self.arrBannerLayer.count) + (self.arrGridLayer.count)) == 0 {
            self.collView.isHidden = true
            self.lblNodata.isHidden = false
        } else {
            self.collView.isHidden = false
            self.lblNodata.isHidden = true
            
            self.collView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        }
        
        self.collCategory.reloadData()
        self.collView.reloadData()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.changeTabBar(hidden: false, animated: true)
        }
        
    }

    func getAllCategory() {

        guard let storeId = UserDefaults.standard.value(forKey: "storeId") as? String else {
            return
        }

        var postData = [String:String]()
        postData["store_id"] = storeId

        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);

        API().callAPI(endPoint: "mobiconnectadvcart/category/getallcategorieslistnew", method: .POST, param: postData) { (json, err) in

            cedMageLoaders.removeLoadingIndicator(me: self);

            if (json[0]["status"].stringValue == "success")
            {
                let catData = try! json[0]["data"]["categories"].rawData()
                let tempCat = try! JSONDecoder().decode([CategoryLayer].self, from: catData)
                
                let tempCatSorted = tempCat.sorted { (one, two) -> Bool in
                    
                    if (one.sort_order == "" || one.sort_order == nil || two.sort_order == "" || two.sort_order == nil) {
                        return false;
                    } else {
                        return ((Int(one.sort_order ?? "0")!) < (Int(two.sort_order ?? "0")!));
                    }
                }
                
                APP_DEL.arrCategoriesCat = tempCatSorted
            }
            
            if APP_DEL.arrCategoriesCat.count > 0 {
                
                let tempSelected = APP_DEL.arrCategoriesCat.firstIndex { (layer) -> Bool in
                    return (layer.selected ?? false)
                }
                
                if let preSelected = tempSelected {
                    APP_DEL.isFromHomeBannerID = APP_DEL.arrCategoriesCat[preSelected].main_category_id ?? ""
                }
                
                self.getBannerLayers()
                
            } else {
                
                self.checkNoData()
            }
        }
    }

    func updateSelfCategory(res: Bool) {
        
        updateCategory(res: true)
    }
    
    func updateCategory(res: Bool) {
       
        if APP_DEL.arrCategoriesCat.count > 0 {

            if !APP_DEL.isCategoryFromHomeLayer{
                let tempSelected = APP_DEL.arrCategoriesCat.firstIndex { (layer) -> Bool in
                    return (layer.selected ?? false)
                }

                if let preSelected = tempSelected {
                    APP_DEL.isFromHomeBannerID = APP_DEL.arrCategoriesCat[preSelected].main_category_id ?? ""
                }
                APP_DEL.isCategoryFromHomeLayer = false
            }
            
            self.getBannerLayers()

        } else {
            self.getAllCategory()
        }
        
      
    }
    
    func getBannerLayers() {

        if APP_DEL.isFromHomeBannerID == "" {

            APP_DEL.isFromHomeBannerID = APP_DEL.arrCategoriesCat.first?.main_category_id ?? ""

        } else {

            let index_row = APP_DEL.arrCategoriesCat.firstIndex { (item_category) -> Bool in
                return (item_category.main_category_id == APP_DEL.isFromHomeBannerID)
            }

            if let tempIndexRow = index_row {

                APP_DEL.isFromHomeBannerID = APP_DEL.arrCategoriesCat[tempIndexRow].main_category_id ?? ""

            } else {

                APP_DEL.isFromHomeBannerID = APP_DEL.arrCategoriesCat.first?.main_category_id ?? ""
            }

        }

        self.arrBannerLayer = [CategoryLayer]()
        self.arrGridLayer = [CategoryLayer]()

        let catIndex = APP_DEL.arrCategoriesCat.firstIndex { (item_category) -> Bool in
            return (item_category.main_category_id == APP_DEL.isFromHomeBannerID)
        }

        if let catIndexFinal = catIndex {

            for item in (APP_DEL.arrCategoriesCat[catIndexFinal].subcategories ?? []) {

                if (item.subcategories?.count ?? 0) > 0 {
                    self.arrBannerLayer.append(item)
                } else {
                    self.arrGridLayer.append(item)
                }
            }

            self.checkNoData()
        }
    }

    func didSelectBanner(index: IndexPath) {
        
        if self.arrBannerLayer.count == index.section {
            
            //Grid Layer
            
            let layer = self.arrGridLayer[index.row]
            
            CategoryView().pushCategory(category: layer, vc: self) { (err) in }
            
        } else {
            
            //Banner Layer
            
            let layer = self.arrBannerLayer[index.section]
            
            CategoryView().pushCategory(category: layer, vc: self) { (err) in }
        }
    }
    
}

extension NejreeCategoryVC: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if scrollView == self.collView {
            
            if scrollView.panGestureRecognizer.translation(in: scrollView).y < 0 {
                
                //scrolling down
                changeTabBar(hidden: true, animated: true)
                
            } else {
                
                //scrolling up
                changeTabBar(hidden: false, animated: true)
            }
        }
    }

    func changeTabBar(hidden:Bool, animated: Bool) {
        
        
        return
        let tabBar = self.tabBarController?.tabBar
        let offset = (hidden ? UIScreen.main.bounds.size.height : UIScreen.main.bounds.size.height - (tabBar?.frame.size.height)! )
        if offset == tabBar?.frame.origin.y { return }
        print("changing origin y position")
        
        let duration:TimeInterval = (animated ? 0.5 : 0.0)
        UIView.animate(withDuration: duration, animations: {
            
            tabBar!.frame.origin.y = offset
            
        }, completion:nil)
    }
}

extension NejreeCategoryVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

    func numberOfSections(in collectionView: UICollectionView) -> Int {

        if collectionView == self.collView {

            if self.arrGridLayer.count > 0 {
                //Grid Layer
                return (self.arrBannerLayer.count + 1)
            } else {
                //Banner Layer
                return (self.arrBannerLayer.count)
            }
            
        } else if collectionView == self.collCategory {

            return 1;
        }

        return 0
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        if collectionView == self.collView {
            
            if self.arrBannerLayer.count == section {
                //Grid Layer
                return 1
            } else {
                //Banner Layer
                return 1
            }
            
        } else if collectionView == self.collCategory {
            
            return APP_DEL.arrCategoriesCat.count;
        }

        return 0;
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        if collectionView == self.collView {

            if self.arrBannerLayer.count == indexPath.section {
                
                //Grid Layer
                
                let layer = self.arrGridLayer.first
                
                let cell = self.collView.dequeueReusableCell(withReuseIdentifier: "CategoryGridCell", for: indexPath) as! CategoryGridCell
                cell.tag = indexPath.section

                cell.delegateDidSelectBanner = self

                cell.itemSize = CGSize(width: CGFloat(Float(layer?.layer_width ?? "0.0") ?? 0.0), height: CGFloat(Float(layer?.layer_height ?? "0.0") ?? 0.0) + gridBottomTextHT)
                cell.itemSpace = CGFloat(Float(layer?.layer_item_space ?? "0.0") ?? 0.0)

               // cell.collView.contentInset = UIEdgeInsets(top: 0, left: CGFloat(Float(layer?.layer_item_space ?? "0.0") ?? 0.0), bottom: 0, right: CGFloat(Float(layer?.layer_item_space ?? "0.0") ?? 0.0))

                cell.arrLayer = self.arrGridLayer

                cell.collView.reloadData()

                return cell
                
            } else {
                
                //Banner Layer
                
                let layer = self.arrBannerLayer[indexPath.section]
                
                let cell = self.collView.dequeueReusableCell(withReuseIdentifier: "CategoryBannerCell", for: indexPath) as! CategoryBannerCell
                cell.tag = indexPath.section

                let res = getGeometry(ratio: layer.layer_ratio ?? "")
                cell.consWidth.constant = (res.imageSize.width - (CGFloat(Float(layer.layer_item_space ?? "0.0") ?? 0.0) * 2.0))
                cell.consHeight.constant = res.imageSize.height
                cell.lblTitle.text = (layer.main_category_name ?? "").uppercased()
                
               
                if APP_DEL.selectedLanguage == Arabic{
                    cell.lblTitle.textAlignment = .right
                } else {
                    cell.lblTitle.textAlignment = .left
                }
                
                cell.imgView.image = nil
                
                let imgStr = (layer.main_category_image ?? "")
                if let imgUrl = URL(string: layer.main_category_image ?? ""), imgStr != "" {
                    cell.imgView.sd_setImage(with: imgUrl, placeholderImage: nil)
                    cell.lblTitle.textColor = .white
                } else {
                    cell.imgView.backgroundColor = UIColor(hex: cat_bg_default_color_code)
                    cell.lblTitle.textColor = .black
                }

                return cell
            }

        } else if collectionView == self.collCategory {

            let cell = self.collCategory.dequeueReusableCell(withReuseIdentifier: "CategoryMainCell", for: indexPath) as! CategoryMainCell

            let cat = APP_DEL.arrCategoriesCat[indexPath.row]

            
           
            
            cell.lblTitle.text = (cat.main_category_name ?? "").uppercased()

            if APP_DEL.isFromHomeBannerID == (cat.main_category_id ?? "") {

                cell.lblTitle.font = UIFont(name: "Cairo-Bold", size: 17)!
                cell.lblDevider.isHidden = false
                cell.lblTitle.textColor = UIColor(hex: "#FBAD18")!

            } else {

                cell.lblTitle.font = UIFont(name: "Cairo-Regular", size: 17)!
                cell.lblDevider.isHidden = true
                cell.lblTitle.textColor = UIColor.black
            }

           
            cell.lblTitle.superview?.backgroundColor = UIColor.white//UIColor.init(hexString: "#FBAD18")!
            cell.lblDevider.backgroundColor = UIColor(hex: "#FBAD18")!//UIColor.black

            return cell;
        }




        return UICollectionViewCell()
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == self.collCategory {

            let cat = APP_DEL.arrCategoriesCat[indexPath.row]
            
            if APP_DEL.isFromHomeBannerID != (cat.main_category_id ?? "") {
                
                APP_DEL.isFromHomeBannerID = (cat.main_category_id ?? "")
                
                if isDisplayHomeCategory {
                    APP_DEL.delegateUpdateHome?.updateHome(res: true)
                }
                
                self.collView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
                
                self.getBannerLayers()
            }
           //
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.changeTabBar(hidden: false, animated: true)
            }
            

        } else {
            
            if self.arrBannerLayer.count == indexPath.section {
                
                //Grid Layer
                
            } else {
                
                //Banner Layer
                self.didSelectBanner(index: indexPath)
                
            }
        }
    }

    func getSafeAreaHT() -> CGFloat {

        let verticalSafeAreaInset: CGFloat
        if #available(iOS 11.0, *) {
            verticalSafeAreaInset = self.view.safeAreaInsets.bottom + self.view.safeAreaInsets.top
        } else {
            verticalSafeAreaInset = 0.0
        }
        let safeAreaHeight = self.view.frame.height - verticalSafeAreaInset

        return (safeAreaHeight - 40.0)
    }

    func getGeometry(ratio: String) -> (itemSize: CGSize, imageSize: CGSize) {

        let temp = ratio.components(separatedBy: ":")

        if temp.count >= 2 {

            if (temp[0] != "*" && temp[1] != "*") {

                let WR : CGFloat = CGFloat(Double(temp[0])!)
                let HR : CGFloat = CGFloat(Double(temp[1])!)

                if WR >= 1.0 {

                    let wR : CGFloat = (1.0)
                    let hR : CGFloat = (HR / WR)

                    return (itemSize: CGSize(width: SCREEN_WIDTH * wR, height: SCREEN_WIDTH * hR),
                            imageSize: CGSize(width: SCREEN_WIDTH * wR, height: SCREEN_WIDTH * hR))

                } else {

                    let width : CGFloat = (SCREEN_WIDTH * WR)
                    let height : CGFloat = (SCREEN_WIDTH * HR)

                    return (itemSize: CGSize(width: SCREEN_WIDTH, height: height),
                            imageSize: CGSize(width: width, height: height))

                }

            } else if (temp[0] == "*" && temp[1] != "*") {

                let HR : CGFloat = CGFloat(Double(temp[1])!)

                let height : CGFloat = (SCREEN_WIDTH * HR)

                return (itemSize: CGSize(width: SCREEN_WIDTH, height: height),
                        imageSize: CGSize(width: SCREEN_WIDTH, height: height))

            } else if (temp[0] != "*" && temp[1] == "*") {

                let WR : CGFloat = CGFloat(Double(temp[0])!)

                let width : CGFloat = (SCREEN_WIDTH * WR)

                return (itemSize: CGSize(width: width, height: getSafeAreaHT()),
                        imageSize: CGSize(width: width, height: getSafeAreaHT()))

            } else if (temp[0] == "*" && temp[1] == "*") {

                return (itemSize: CGSize(width: SCREEN_WIDTH, height: getSafeAreaHT()),
                        imageSize: CGSize(width: SCREEN_WIDTH, height: getSafeAreaHT()))

            }
        }

        return (itemSize: CGSize.zero,
                imageSize: CGSize.zero)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        if collectionView == self.collView {
            
            if self.arrBannerLayer.count == indexPath.section {
                
                //Grid Layer
                
                let layer = self.arrGridLayer.first
                return CGSize(width: SCREEN_WIDTH, height: CGFloat(Float(layer?.layer_height ?? "0.0") ?? 0.0) + gridBottomTextHT)
                
            } else {
                
                //Banner Layer
                
                let layer = self.arrBannerLayer[indexPath.section]
                return getGeometry(ratio: layer.layer_ratio ?? "").itemSize
            }

        } else if collectionView == self.collCategory {

            return CGSize(width: (SCREEN_WIDTH / CGFloat(APP_DEL.arrCategoriesCat.count)), height: 40.0)
        }

        return CGSize.zero
    }

    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {

        if collectionView == self.collView {

            if self.arrBannerLayer.count == indexPath.section {
                
                //Grid Layer

                switch kind {

                case UICollectionView.elementKindSectionHeader:

                    guard let headerView = self.collView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "LayerHeaderWhiteCell", for: indexPath) as? LayerHeaderWhiteCell
                        else {
                            fatalError("Invalid view type")
                    }

                    return headerView

                case UICollectionView.elementKindSectionFooter:

                    guard let headerView = self.collView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "LayerHeaderWhiteCell", for: indexPath) as? LayerHeaderWhiteCell
                        else {
                            fatalError("Invalid view type")
                    }

                    return headerView

                default:

                    fatalError("Unexpected element kind")
                }
                
            } else {
                
                //Banner Layer
                
                switch kind {

                case UICollectionView.elementKindSectionHeader:

                    guard let headerView = self.collView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "LayerHeaderWhiteCell", for: indexPath) as? LayerHeaderWhiteCell
                        else {
                            fatalError("Invalid view type")
                    }

                    return headerView

                case UICollectionView.elementKindSectionFooter:

                    guard let headerView = self.collView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "LayerHeaderWhiteCell", for: indexPath) as? LayerHeaderWhiteCell
                        else {
                            fatalError("Invalid view type")
                    }

                    return headerView

                default:

                    fatalError("Unexpected element kind")
                }
            }

        } else if collectionView == self.collCategory {

            switch kind {

            case UICollectionView.elementKindSectionHeader:

                guard let headerView = self.collCategory.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "LayerHeaderWhiteCell", for: indexPath) as? LayerHeaderWhiteCell
                    else {
                        fatalError("Invalid view type")
                }

                headerView.backgroundColor = UIColor.black

                return headerView

            case UICollectionView.elementKindSectionFooter:

                guard let headerView = self.collCategory.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "LayerHeaderWhiteCell", for: indexPath) as? LayerHeaderWhiteCell
                    else {
                        fatalError("Invalid view type")
                }

                headerView.backgroundColor = UIColor.black

                return headerView


            default:

                fatalError("Unexpected element kind")
            }
        }

        return UICollectionReusableView()
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {

        if collectionView == self.collView {

            if self.arrBannerLayer.count == section {
                
                //Grid Layer
                
                let layer = self.arrGridLayer.first

                return CGSize(width: SCREEN_WIDTH, height: CGFloat(Float(layer?.layer_top_space ?? "0.0") ?? 0.0))
                
            } else {
                
                //Banner Layer
                
                let layer = self.arrBannerLayer[section]

                return CGSize(width: SCREEN_WIDTH, height: CGFloat(Float(layer.layer_top_space ?? "0.0") ?? 0.0))
            }
            
        } else {

            return CGSize(width: 0.0, height: 0.0)
        }

    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {

        if collectionView == self.collView {

            if self.arrBannerLayer.count == section {
                
                //Grid Layer
                
                let layer = self.arrGridLayer.first

                return CGSize(width: SCREEN_WIDTH, height: CGFloat(Float(layer?.layer_bottom_space ?? "0.0") ?? 0.0))
                
            } else {
                
                //Banner Layer
                
                let layer = self.arrBannerLayer[section]

                return CGSize(width: SCREEN_WIDTH, height: CGFloat(Float(layer.layer_bottom_space ?? "0.0") ?? 0.0))
            }
            
        } else {

            return CGSize(width: 0.0, height: 0.0)
        }
        
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {

        if collectionView == self.collView {

            return 0.0
            
        }  else {
            
            return 0.0
        }
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {

        if collectionView == self.collView {

            return 0

        } else {

            return 0
        }
    }
}


class CategoryView {
    
    func pushCategory(category: CategoryLayer, vc: UIViewController, completion: @escaping (String?) -> ()) {

        let option = LayerOption(rawValue: category.layer_option ?? "") ?? .None
        
        if (category.subcategories?.count ?? 0) > 0 {
            
            print("move to NejreeCategorySubVC")
            
            let view = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NejreeCategorySubVC") as! NejreeCategorySubVC
            view.category = category
            view.arrSubCategory = category.subcategories ?? []
            view.hidesBottomBarWhenPushed = true
            vc.navigationController?.pushViewController(view, animated: true)
            
        } else {
                
            if option == .productWithCategory {
                
                print("productWithCategory")
                
                if let view =  UIStoryboard(name: "categorylayouts", bundle: nil).instantiateViewController(withIdentifier: "cedMageDefaultCollection") as?  cedMageDefaultCollection {
                    
                    view.strTitleLayer = category.main_category_name ?? ""
                    view.hidesBottomBarWhenPushed = true
                    view.isFilterDisplay = false
                    view.isComesFromProductLayer = true
                    view.selectedCategory = category.redirect_id ?? ""
                    vc.navigationController?.pushViewController(view, animated: true)
                }
                
                completion(nil)
                
            } else if option == .GiftCard {
                
                let view = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "NejreeGiftcardVC") as! NejreeGiftcardVC
                view.category_id = category.redirect_id ?? ""
                view.hidesBottomBarWhenPushed = true
                vc.navigationController?.pushViewController(view, animated: true)
                
                completion(nil)
                
            } else if (option == .SpecificProduct) {
                
                print("SpecificProduct")
                
                let view = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "productViewController") as! productViewController
                view.product_id = category.redirect_id ?? ""
                APP_DEL.productIDglobal = category.redirect_id ?? ""
                view.hidesBottomBarWhenPushed = true
                vc.navigationController?.pushViewController(view, animated: true)
                
                completion(nil)
                
            } else if (option == .SpecificCategory) {
                
                print("SpecificCategory")
                
                APP_DEL.isFromHomeBannerID = category.redirect_id ?? ""
                APP_DEL.delegateUpdateSelfCategory?.updateSelfCategory(res: true)
                if isDisplayHomeCategory {
                    APP_DEL.delegateUpdateHome?.updateHome(res: true)
                }
                vc.navigationController?.popToRootViewController(animated: true)
                
                completion(nil)
                
            } else if (option == .ScrollableFullImage) {
                
                print("ScrollableFullImage")
                
                APP_DEL.isFromHomeBannerID = category.redirect_id ?? ""
                APP_DEL.delegateUpdateSelfCategory?.updateSelfCategory(res: true)
                if isDisplayHomeCategory {
                    APP_DEL.delegateUpdateHome?.updateHome(res: true)
                }
                vc.navigationController?.popToRootViewController(animated: true)
                
                completion(nil)
                
            } else if (option == .ShopInShop) {
                
                print("ShopInShop")
                
                let view = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HomeBrandVC") as! HomeBrandVC
                view.categoryImage = category.main_category_image ?? ""
                view.categoryId = category.redirect_id ?? ""
                view.hidesBottomBarWhenPushed = true
                view.strTitle = category.main_category_name ?? ""
                vc.navigationController?.pushViewController(view, animated: true)
                
                completion(nil)
                
            } else if (option == .Product) {
                
                print("Product")
                
                if let view =  UIStoryboard(name: "categorylayouts", bundle: nil).instantiateViewController(withIdentifier: "cedMageDefaultCollection") as?  cedMageDefaultCollection {
                    
                    let res = self.getTagJsonString(tagString: category.redirect_id ?? "")
                    UserDefaults.standard.set(res, forKey: "filtersToSend");
                    
                    view.strTitleLayer = category.main_category_name ?? ""
                    //viewController.searchString = layer.layer_data?[index.row].tags ?? ""
                    view.hidesBottomBarWhenPushed = true
                    view.isFilterDisplay = false
                    view.selectedCategory = APP_DEL.isFromHomeBannerID ?? ""
                    vc.navigationController?.pushViewController(view, animated: true)
                }
                
                completion(nil)
                
            } else if (option == .NoLayer || option == .None) {
                
                if let view =  UIStoryboard(name: "categorylayouts", bundle: nil).instantiateViewController(withIdentifier: "cedMageDefaultCollection") as?  cedMageDefaultCollection {
                    
                    let res = self.getTagJsonString(tagString: category.redirect_id ?? "")
                    UserDefaults.standard.set(res, forKey: "filtersToSend");
                    
                    view.strTitleLayer = category.main_category_name ?? ""
                    //viewController.searchString = layer.layer_data?[index.row].tags ?? ""
                    view.hidesBottomBarWhenPushed = true
                    view.isFilterDisplay = false
                    view.selectedCategory = category.main_category_id ?? ""
                    vc.navigationController?.pushViewController(view, animated: true)
                }
                
            }
        }
    }
    
    private func getTagJsonString(tagString: String) -> String {
        
        var dict : [String:String] = [:]
        
        if tagString != "" {
            
            for temp in (tagString.components(separatedBy: ",")) {
                
                let temp_2 = temp.components(separatedBy: "/")
                
                if temp_2.count > 1 {
                    dict[temp_2[0]] = temp_2[1]
                }
            }
        }
        
        if dict == [:] {
            
            return ""
            
        } else {
            
            let data = try! JSONSerialization.data(withJSONObject: ["tag":dict], options: [])
            let jsonSTR = String(data: data, encoding: .utf8)!;
            
            return jsonSTR
        }
    }
}
