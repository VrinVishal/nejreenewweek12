//
//  CategoryVC.swift
//  MageNative Magento Platinum
//
//  Created by Vrinsoft on 29/07/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit
import FirebaseAnalytics
import IQKeyboardManager

struct Category : Codable {
        
    let att_code : String?
    let att_label : String?
    let filter_data : [CategoryData]?
}

struct CategoryData : Codable {
        
    let filter_code : String?
    let filter_img : String?
    let filter_value : String?
}

struct CategoryProduct : Codable {
    
    let product_id : String?
    let product_name : String?
    let type : String?
    let description : String?
    //let inwishlist : String?
   // let wishlist_item_id : Int?
    let source_id : String?
    let stock_status : String?
    let product_url : String?
    let product_image : String?
    let brands_name : String?
    let offer : String?
    let regular_price : String?
    let special_price : String?
    let tag : String?

    enum CodingKeys: String, CodingKey {

        case product_id = "product_id"
        case product_name = "product_name"
        case type = "type"
        case description = "description"
       // case inwishlist = "Inwishlist"
        //case wishlist_item_id = "wishlist_item_id"
        case source_id = "source_id"
        case stock_status = "stock_status"
        case product_url = "product-url"
        case product_image = "product_image"
        case brands_name = "brands_name"
        case offer = "offer"
        case regular_price = "regular_price"
        case special_price = "special_price"
         case tag = "tag"
    }
}

class CategoryVC: MagenativeUIViewController, UIScrollViewDelegate, FilterApply, UpdateCategory {
   
    
    
    
    //MARK:- Search Outlet  --
    @IBOutlet weak var viewSearch: UIView!
    @IBOutlet weak var viewTxtSearch: UIView!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var btnSearch: UIButton!
    
    @IBOutlet weak var viewNotification: UIView!
    @IBOutlet weak var lblNotificCount: UILabel!
    
    @IBOutlet weak var btnClear: UIButton!
    
    @IBOutlet weak var viewFilter: UIView!
    @IBOutlet weak var btnFilter: UIButton!
    
    @IBOutlet weak var viewSearchList: UIView!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var htTbl: NSLayoutConstraint!
    
    var searchText = ""
    var suggentionsArray = [[String:String]]()
    var isFilterApplied : Bool = false
    var isCategoryLoadFromHome : Bool = false
   
    
    
    var arrFilter : [Filter] = []
    
    
   // @IBOutlet weak var scrllView: UIScrollView!
   // @IBOutlet weak var mainView: UIView!
    
    @IBOutlet weak var viewTopCat: UIView!
    @IBOutlet weak var collSubCategory: UICollectionView!
    
    @IBOutlet weak var viewBanner: UIView!
    @IBOutlet weak var imgBanner: UIImageView!
    @IBOutlet weak var constraintImageBannerHeight: NSLayoutConstraint!
    
    @IBOutlet weak var viewCatType: UIView!
    @IBOutlet weak var collSubCatType: UICollectionView!
    
    @IBOutlet weak var collectionProduct: UICollectionView!
   // @IBOutlet weak var constraintCollectionProductHeight: NSLayoutConstraint!
    @IBOutlet weak var lblNoData: UILabel!
    @IBOutlet weak var lblAppliedFilter: UILabel!
    
    @IBOutlet weak var collType: UICollectionView!
    @IBOutlet weak var constraintCollectionCategoryHeight: NSLayoutConstraint!
    

    
    

    
    @IBOutlet weak var stackVw: UIStackView!
    
    
    //var categoryId = ""
    var isChild = ""
    var categoryName = ""
    var categoryImage = ""
    var parentVC = categoryMenuBarController()
    
    var currentPage = 1
    var products = [CategoryProduct]()
    var filterString = ""
    var filterDT = FilterData()
    var loading = true;
    var noProductCheck = false
    var isColScroll = true
    var BannerHeight = CGFloat()
    
    var level_1 : [Filter] = []
    var level_2 : [Filter] = []
    
//    var level_1 : [Category] = []
//    var level_2 : [Category] = []
//
    
    
    var level_1_key = ""
    var level_2_key = ""
    
    var filterLevel1 : [String:String] = [:]
    var filterLevel1Index = -1
    var filterLevel2 : [String:String] = [:]
    var filterLevel2Index = -1

    var activityIndi = UIActivityIndicatorView()
    var isSearchOpen : Bool = false
    
    //var lastContentOffset: CGFloat = 0.0
    @IBOutlet weak var collTopCategory: UICollectionView!
    @IBOutlet weak var constraintCollTopCategory: NSLayoutConstraint!
    
    @IBOutlet weak var stackBottomHT: NSLayoutConstraint!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        activityIndi.frame = CGRect(x: 0.0, y: 0.0, width: 40.0, height: 40.0)
        activityIndi.center = self.view.center
        activityIndi.hidesWhenStopped = true
        activityIndi.style =
            UIActivityIndicatorView.Style.gray
       // self.view.addSubview(activityIndi)
        // activityIndi.startAnimating()
        activityIndi.isHidden = true
        
        lblAppliedFilter.layer.cornerRadius = lblAppliedFilter.frame.size.height/2
        lblAppliedFilter.clipsToBounds = true
        self.lblAppliedFilter.isHidden = true
        
        viewSearch.layer.cornerRadius = viewSearch.frame.size.height/2
        viewSearch.clipsToBounds = true
        
        getAllCategory()
        
        BannerHeight = constraintImageBannerHeight.constant
        
        self.setData()
        
        let TopGesture = UISwipeGestureRecognizer(target: self, action: #selector(holeSwiped))
        TopGesture.direction = .up
        self.stackVw.addGestureRecognizer(TopGesture)
        
        
        let BottomGesture = UISwipeGestureRecognizer(target: self, action: #selector(holeSwiped))
        BottomGesture.direction = .down
        self.stackVw.addGestureRecognizer(BottomGesture)
        
        self.setSearchUI()
        
        APP_DEL.delegateUpdateCategory = self
        
       
        NotificationCenter.default.addObserver(self, selector: #selector(checkDeeplinkFromFirebase), name: NSNotification.Name(rawValue: "checkDeeplinkFromFirebaseCategory"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardNotification(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardNotification(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)

        setupHideKeyboardOnTap()
        
    }
    
    func updateCategory(res: Bool) {
        
        self.filterLevel2Index = -1
        self.filterLevel2 = [:]
        self.level_2 = []
        self.filterDT.selected_level_2_code = ""
        
        self.filterLevel1Index = -1
        self.filterLevel1 = [:]
        self.level_1 = []
        self.filterDT.selected_level_1_code = ""
        
        self.noProductCheck = false
        
        
        
        if APP_DEL.arrCategoriesCat.count > 0 {
            //self.getAttributeByID()
            self.collTopCategory.reloadData()
            self.collType.reloadData()
            self.isCategoryLoadFromHome = true
            self.filterApply(filter: "", filterDT: FilterData(), isForShowFilterView: false)
        } else {
            self.getAllCategory()
        }
    }
    @objc func checkDeeplinkFromFirebase(){
           
       
        
        self.isFilterApplied = false
        
        self.filterLevel2Index = -1
        filterLevel2 = [:]
        self.filterDT.selected_level_2_code = ""
        
        self.filterLevel1Index = -1
        filterLevel1 = [:]
        self.filterDT.selected_level_1_code = ""
        
        self.filterApply(filter: "", filterDT: FilterData(), isForShowFilterView: false)
        self.collType.reloadData()
        
        self.level_1 = []
        self.level_2 = []
        self.products = []
        
        self.RELOAD(isFromProduct: false)
        
        APP_DEL.delegateUpdateHome?.updateHome(res: true)
           
       }
    
    
    var isKeyboardShowing = false
    @objc func handleKeyboardNotification(notification: NSNotification) {
        
        self.isKeyboardShowing = (notification.name == UIResponder.keyboardWillShowNotification)

        if self.isKeyboardShowing {

        } else {
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.closeSuggetionView()
            }
        }
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
       

        self.navigationController?.navigationBar.isHidden = true
        self.navigationController?.isNavigationBarHidden = true
        
        self.tabBarController?.tabBar.isHidden = false
        self.stackBottomHT.constant = (44 + 15)
        
        IQKeyboardManager.shared().isEnabled = false
        IQKeyboardManager.shared().isEnableAutoToolbar = false
        
        self.tblView.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
        
        APP_DEL.getUnreadCount { (res) in
            
            self.updateNotificationCount()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
         APP_DEL.strFilterWithDeeplink = ""
        
        IQKeyboardManager.shared().isEnabled = true
        IQKeyboardManager.shared().isEnableAutoToolbar = true
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?)
    {
        self.tblView.layer.removeAllAnimations()

        self.htTbl.constant = self.tblView.contentSize.height
        UIView.animate(withDuration: 0.5) {
            self.updateViewConstraints()
            self.loadViewIfNeeded()
        }
    }
    
    @objc func holeSwiped(gesture: UISwipeGestureRecognizer){
        
        switch gesture.direction {
        case .down :
            print("Down swipe")
            
//            UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.transitionCrossDissolve, animations: {
//
//                //self.constraintCollectionCategoryHeight.constant = 40
//                self.constraintCollTopCategory.constant = 0
//                self.stackBottomHT.constant = (44 + 10)
//                self.tabBarController?.tabBar.isHidden = false
//                self.view.layoutIfNeeded()
//
//            })
            
            UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.transitionCrossDissolve, animations: {
                
                self.constraintImageBannerHeight.constant = self.BannerHeight
                self.constraintCollectionCategoryHeight.constant = 40

                self.closeSuggetionView()
                
                self.view.layoutIfNeeded()
                
            })
            
        case .up :
            
            print("Up swipe")
            
//            UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.transitionCrossDissolve, animations: {
//
//                //self.constraintCollectionCategoryHeight.constant = 0
//                self.constraintCollTopCategory.constant = -40
//                self.stackBottomHT.constant = (10)
//                self.tabBarController?.tabBar.isHidden = true
//                self.view.layoutIfNeeded()
//
//            })
            
            UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.transitionCrossDissolve, animations: {
                       
                self.constraintImageBannerHeight.constant = 0
                self.constraintCollectionCategoryHeight.constant = 0

                self.closeSuggetionView()
                
                self.view.layoutIfNeeded()
                       
            })
        default:
            print("other swipe")
        }

    }
    
    func closeSuggetionView() {
        
         self.isSearchOpen = false
        self.txtSearch.text = ""
        self.searchText(text: "")
        self.view.endEditing(true)
        self.btnClearAction(self.btnClear)
    }
    
    @IBAction func btnNotificationAction(_ sender: UIButton) {
        
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: false, completion: nil)
        
    }
    
    func updateNotificationCount() {
        
        if let count = UserDefaults.standard.value(forKey: "NejreeUnreadCount") as? Int, (count > 0) {
            
            self.lblNotificCount.text = "\(count)"
            self.lblNotificCount.isHidden = false
            
        } else {
            
            self.lblNotificCount.text = ""
            self.lblNotificCount.isHidden = true

        }
    }
    
    @IBAction func btnSearchBottomAction(_ sender: Any) {
        
        if let viewController =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "cedMageSearchPage") as?  cedMageSearchPage {
            
            viewController.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(viewController, animated: true)
            
        }
    }
    
    @IBAction func btnFilterAction(_ sender: Any) {
        
        self.viewFilter.isHidden = true
        self.lblAppliedFilter.isHidden = true
        
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NejreeCategoryFilterVC") as! NejreeCategoryFilterVC
        vc.hidesBottomBarWhenPushed = true
        vc.categoryId = APP_DEL.isFromHomeBannerID
        vc.filterDT = self.filterDT
        vc.isCategoryFilter = true
        vc.isGenderHide = true
        vc.arrFilter = self.arrFilter
        vc.modalPresentationStyle = .overFullScreen
        vc.delegateFilterApply = self
        self.present(vc, animated: true, completion: nil)
    }
    
    func setSubCategory(index: Int) {
        
        if APP_DEL.arrCategoriesTopCat.count > index {
            
        } else {
            return;
        }
        
        
        let cat = APP_DEL.arrCategoriesTopCat[index]
        
        if APP_DEL.isFromHomeBannerTopID != (cat.main_category_id ?? "") {
            
            APP_DEL.isFromHomeBannerTopID = (cat.main_category_id ?? "")
            if APP_DEL.isFromHomeBannerID == ""{
                
                let temp = cat.subcategories?.first(where: { (temp_check) -> Bool in
                    return (temp_check.selected == true)
                })
                
                if temp != nil {
                    APP_DEL.isFromHomeBannerID = temp?.main_category_id ?? ""
                } else {
                    APP_DEL.isFromHomeBannerID = ""
                }
                
            }
            
            
        }
        
        
       
        
        var tempHomeCate = APP_DEL.arrCategoriesTopCat[index].subcategories ?? []
        
      //  let value=UserDefaults.standard.value(forKey: "AppleLanguages") as! [String]
        
        tempHomeCate = tempHomeCate.sorted { (one, two) -> Bool in
            
            if (one.sort_order == "" || one.sort_order == nil || two.sort_order == "" || two.sort_order == nil) {
                return false;
            } else {
                return ((Int(one.sort_order ?? "0")!) < (Int(two.sort_order ?? "0")!));
            }
        }
                
//        if value[0] == "ar" {
//            tempHomeCate = tempHomeCate.reversed()
//        }
        
        APP_DEL.arrCategoriesCat = tempHomeCate
        
        self.getCategoryProduct()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            APP_DEL.delegateUpdateHome?.updateHome(res: true)
        }
        
    }

    func getAllCategory() {
        
        if APP_DEL.arrCategoriesCat.count == 0{
            
            guard let storeId = UserDefaults.standard.value(forKey: "storeId") as? String else {return}
            let param=["theme":"5","store_id":storeId]

            cedMageLoaders.removeLoadingIndicator(me: self);
            cedMageLoaders.addDefaultLoader_withlockedbackground(me: self)

            API().callAPI(endPoint: "mobiconnectadvcart/category/getallcategorieslist", method: .POST, param: param) { (json_res, err) in

                DispatchQueue.main.async {

                    cedMageLoaders.removeLoadingIndicator(me: self);

                    if err == nil {
                        
                        var tempHomeTopCate : [TopCategory] = []

                        if (json_res[0]["status"].stringValue == "success")
                        {
                            for cat in json_res[0]["data"]["categories"].arrayValue {
                                
                                var tempSubCat: [HomeCategory] = []
                                for subCat in cat["subcategories"].arrayValue {
                                    
                                    tempSubCat.append(HomeCategory(main_category_id: subCat["main_category_id"].stringValue,
                                                                     main_category_name: subCat["main_category_name"].stringValue,
                                                                     main_category_image: subCat["main_category_image"].stringValue,
                                                                     sort_order: subCat["sort_order"].stringValue,
                                                                     has_child: subCat["has_child"].stringValue,
                                                                     selected: subCat["selected"].boolValue))
                                    
                                    if subCat["selected"].boolValue == true {
                                        
                                        if APP_DEL.isFromHomeBannerID == ""{
                                            APP_DEL.isFromHomeBannerID = subCat["main_category_id"].stringValue
                                        }
                                        
                                    }
                                }
                                
                                if cat["selected"].boolValue == true {
                                    
                                    if APP_DEL.isFromHomeBannerTopID == ""{
                                        APP_DEL.isFromHomeBannerTopID = cat["main_category_id"].stringValue
                                    }
                                    
                                }
                                
                                tempHomeTopCate.append(TopCategory(main_category_id: cat["main_category_id"].stringValue,
                                                                 main_category_name: cat["main_category_name"].stringValue,
                                                                 main_category_image: cat["main_category_image"].stringValue,
                                                                 sort_order: cat["sort_order"].stringValue,
                                                                 has_child: cat["has_child"].stringValue,
                                                                 subcategories: tempSubCat))
                            }
                        }
                        
                        

                        let value = UserDefaults.standard.value(forKey: "AppleLanguages") as! [String]
                        tempHomeTopCate = tempHomeTopCate.sorted { (one, two) -> Bool in
                            
                            if (one.sort_order == "" || one.sort_order == nil || two.sort_order == "" || two.sort_order == nil) {
                                return false;
                            } else {
                                return ((Int(one.sort_order ?? "0")!) < (Int(two.sort_order ?? "0")!));
                            }
                        }

//                       var finalHomeCategory : [HomeCategory] = []
//                        for cat in tempHomeCate {
//
//                            if APP_DEL.home_featured_category.contains(cat.main_category_id ?? "") {
//
//                                finalHomeCategory.append(cat)
//                            }
//                        }
                                
//                        if value[0] == "ar" {
//                            tempHomeTopCate = tempHomeTopCate.reversed()
//                        }
                        
                       
                        
                      APP_DEL.arrCategoriesTopCat = tempHomeTopCate
                 
                        let detail : [AnalyticKey:String] = [
                            .ItemCategory : APP_DEL.isFromHomeBannerID]
                        API().setEvent(eventName: .ViewItemList, eventDetail: detail) { (json, err) in }
                        Analytics.logEvent(AnalyticsEventViewItemList, parameters: [
                            AnalyticsParameterItemCategory : APP_DEL.isFromHomeBannerID,
                        ])

                        if APP_DEL.isFromHomeBannerTopID != ""{
                            let index_row = APP_DEL.arrCategoriesTopCat.firstIndex { (item_category) -> Bool in
                                return (item_category.main_category_id == APP_DEL.isFromHomeBannerTopID)
                            }
                            if let tempIndexRow = index_row {
                                self.setSubCategory(index: tempIndexRow)
                            }
                        }
                        else
                        {
                            self.setSubCategory(index: 0)
                        }
                        
                        
//                        self.setSubCategory(index: 0)
                        //self.getCategoryProduct()
                        self.setData()
                        
                        
                       // self.getFilter()
                    }
                }
            }
            
        }
        else
        {
            //self.getCategoryProduct()
            if APP_DEL.isFromHomeBannerTopID != ""{
                let index_row = APP_DEL.arrCategoriesTopCat.firstIndex { (item_category) -> Bool in
                    return (item_category.main_category_id == APP_DEL.isFromHomeBannerTopID)
                }
                if let tempIndexRow = index_row {
                    self.setSubCategory(index: tempIndexRow)
                }
            }
            else
            {
                self.setSubCategory(index: 0)
            }
        }
        
    }
    
    func filterApply(filter: String, filterDT: FilterData, isForShowFilterView: Bool) {
        
        //self.scrllView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        
        self.viewFilter.isHidden = false
       
        
        self.isFilterApplied = true
         if self.filterString == ""{
             self.lblAppliedFilter.isHidden = true
        }
        else
         {
             self.lblAppliedFilter.isHidden = false
        }
        
        
        if isForShowFilterView {
            return;
        }
        
        
        self.filterDT = filterDT
        self.currentPage = 1
        self.filterString = filter
        
        print(self.filterString)
        
        if self.filterString == ""{
            self.isFilterApplied = false
        }
        else
        {
            self.isFilterApplied = true
        }
        
        self.noProductCheck = false
        
        self.preselectedOption()
    }
    
    func preselectedOption() {
        
        if self.filterDT.selected_level_1_code != "" {
            
            if self.level_1.count > 0 {
                
                let filt_cat = self.level_1[0].filter_data?.first(where: { (item_category) -> Bool in (item_category.filter_code == self.filterDT.selected_level_1_code)})
                if filt_cat != nil {
                    
                    let index_row = self.level_1[0].filter_data?.firstIndex { (item_row) -> Bool in
                        return (filt_cat!.filter_code == item_row.filter_code)
                    }
                    
                    if let tempIndexRow = index_row {
                        
                        self.filterLevel1Index = tempIndexRow
                        filterLevel1 = [(level_1[0].filter_data?[tempIndexRow].filter_code ?? ""):(level_1[0].filter_data?[tempIndexRow].filter_value ?? "")]
                    }
                }
            }
            
        } else {
            
            self.filterLevel1Index = -1
            filterLevel1 = [:]
        }
        
        
        if self.filterDT.selected_level_2_code != "" {
            
            if self.level_2.count > 0 {
                
                let filt_type = self.level_2[0].filter_data?.first(where: { (item_type) -> Bool in (item_type.filter_code == self.filterDT.selected_level_2_code)})
                if filt_type != nil {
                    
                    let index_row = self.level_2[0].filter_data?.firstIndex { (item_row) -> Bool in
                        return (filt_type!.filter_code == item_row.filter_code)
                    }
                    
                    if let tempIndexRow = index_row {
                        
                        self.filterLevel2Index = tempIndexRow
                        filterLevel2 = [(level_2[0].filter_data?[tempIndexRow].filter_code ?? ""):(level_2[0].filter_data?[tempIndexRow].filter_value ?? "")]
                    }
                }
            }
            
        } else {
            
            self.filterLevel2Index = -1
            filterLevel2 = [:]
        }

        self.getCategoryProduct()
    }
    
    func showAllView() {
        
        UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.transitionCrossDissolve, animations: {
            
            //self.constraintCollectionCategoryHeight.constant = 40
            self.constraintCollTopCategory.constant = 0
            self.stackBottomHT.constant = (44 + 10)
            self.tabBarController?.tabBar.isHidden = false
            self.view.layoutIfNeeded()
            
        })
        
        UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.transitionCrossDissolve, animations: {
            
            self.constraintImageBannerHeight.constant = self.BannerHeight
            self.constraintCollectionCategoryHeight.constant = 40

            self.closeSuggetionView()
            
            self.view.layoutIfNeeded()
            
        })
    }
    
    var lastContentOffset: CGFloat = 0
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.lastContentOffset = scrollView.contentOffset.y
        
        if scrollView == self.collectionProduct {
            if self.isSearchOpen{
                
                self.txtSearch.text = ""
                self.searchText(text: "")
                self.view.endEditing(true)
                self.btnClearAction(self.btnClear)
                
            }
        }
    }
    
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
//        UIView.transition(with: btnBack, duration: 0.4,
//            options: .transitionCrossDissolve,
//            animations: {
//
//            self.btnBack.isHidden = false
//        })
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        
        let offset = scrollView.contentOffset.y
        if offset > 1 {
            
//            UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.transitionCrossDissolve, animations: {
//
//                //self.constraintCollectionCategoryHeight.constant = 0
//                self.constraintCollTopCategory.constant = -40
//                self.stackBottomHT.constant = (10)
//                self.tabBarController?.tabBar.isHidden = true
//                self.view.layoutIfNeeded()
//
//            })
            
            UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.transitionCrossDissolve, animations: {
                
                self.constraintImageBannerHeight.constant = 0

                
                self.view.layoutIfNeeded()
                
            })
           
            
        }else{
            
//            UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.transitionCrossDissolve, animations: {
//
//                //self.constraintCollectionCategoryHeight.constant = 40
//                self.constraintCollTopCategory.constant = 0
//                self.stackBottomHT.constant = (44 + 10)
//                self.tabBarController?.tabBar.isHidden = false
//                self.view.layoutIfNeeded()
//
//            })
            
            UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.transitionCrossDissolve, animations: {
                           
                    self.constraintImageBannerHeight.constant = self.BannerHeight
                           
                    self.view.layoutIfNeeded()
                           
                })
            
        }
        
        
        self.collectionProduct.performBatchUpdates({
            collectionProduct.layoutIfNeeded()
        }, completion: nil)
        
        
        if self.lastContentOffset < scrollView.contentOffset.y {
            // did move up

            UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.transitionCrossDissolve, animations: {
                
                //self.constraintCollectionCategoryHeight.constant = 0
                self.constraintCollTopCategory.constant = -80
                self.stackBottomHT.constant = (10)
                self.tabBarController?.tabBar.isHidden = true
                self.view.layoutIfNeeded()
                
            })

            
        } else if self.lastContentOffset > scrollView.contentOffset.y {
            // did move down
            UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.transitionCrossDissolve, animations: {
                         
                //self.constraintCollectionCategoryHeight.constant = 40
                self.constraintCollTopCategory.constant = 0
                self.stackBottomHT.constant = (44 + 10)
                self.tabBarController?.tabBar.isHidden = false
                self.view.layoutIfNeeded()
                         
                })

        } else {
            
        }

        if scrollView == self.collectionProduct{
            if  scrollView.contentOffset.y + scrollView.frame.size.height >= (scrollView.contentSize.height - 500) {
            
            if noProductCheck == false && self.loading == false {
                self.loading = true
                self.checkPagination()
                }
            }
        }
        

        
    }
    
    
}


extension CategoryVC {
    
    func setData(){
        
        let nib1 = UINib(nibName: "SubCategoryCell", bundle: nil)
        let nib2 = UINib(nibName: "SubCategoryTypeCell", bundle: nil)
        let nib3 = UINib(nibName: "CatProductCell", bundle: nil)
        let nib4 = UINib(nibName: "CategoryMainCell", bundle: nil)
        
        collSubCategory.register(nib1, forCellWithReuseIdentifier: "SubCategoryCell")
        collSubCatType.register(nib2, forCellWithReuseIdentifier: "SubCategoryTypeCell")
        collectionProduct.register(nib3, forCellWithReuseIdentifier: "CatProductCell")
        collType.register(nib4, forCellWithReuseIdentifier: "CategoryMainCell")
        
        collTopCategory.register(UINib(nibName: "CategoryTopCell", bundle: nil), forCellWithReuseIdentifier: "CategoryTopCell")
        collTopCategory.delegate = self
        collTopCategory.dataSource = self
        collTopCategory.reloadData()
        
        //scrllView.delegate = self
        
        collSubCatType.delegate = self
        collSubCatType.dataSource = self
        
        collSubCategory.delegate = self
        collSubCategory.dataSource = self
        
        collectionProduct.delegate = self
        collectionProduct.dataSource = self
        
        
        collType.delegate = self
        collType.dataSource = self
    
        collType.reloadData()
        view.layoutIfNeeded()

        if collType.contentSize.width <= SCREEN_WIDTH {
            isColScroll = false
        }else{
            isColScroll = true
        }
        view.layoutIfNeeded()
        collType.reloadData()
        

        RELOAD(isFromProduct: false)
    }
    
    func RELOAD(isFromProduct: Bool) {
        
        
        self.lblNoData.text = APP_LBL().no_product_found.uppercased()
        
        collectionProduct.reloadData()
        
        view.layoutIfNeeded()
        if self.products.count == 0 {
            
            self.lblNoData.isHidden = isFromProduct ? false : true
            
        } else {
                        
            self.lblNoData.isHidden = true
        }
        view.layoutIfNeeded()
        
        collectionProduct.reloadData()
        
        
        
        
        if self.level_1.count == 0 {
            self.viewTopCat.isHidden = true
            //self.viewBanner.isHidden = true
        } else {
            self.viewTopCat.isHidden = false
            //self.viewBanner.isHidden = false
        }
        self.viewBanner.isHidden = false

        if self.level_2.count == 0 {
            self.viewCatType.isHidden = true
        } else {
            self.viewCatType.isHidden = false
        }

        self.collSubCategory.reloadData()
        self.collSubCatType.reloadData()
        
        self.updateBannerImage()
        
        self.view.layoutIfNeeded()
        
        //showAllView()
    }
    
    func updateBannerImage() {

        if (level_1.count > 0) && ((level_1[0].filter_data?.count ?? 0) > (self.filterLevel1Index)) && (self.filterLevel1Index > -1) {
            self.imgBanner!.sd_setImage(with: URL(string: level_1[0].filter_data?[self.filterLevel1Index].filter_img ?? ""), placeholderImage: nil)
        } else {
            //self.imgBanner.sd_setImage(with: URL(string: self.categoryImage), placeholderImage: UIImage(named: "bannerplaceholder"))
            self.imgBanner.sd_setImage(with: URL(string: self.categoryImage), placeholderImage: nil)
        }
    }
    
    func updateFilterSTR() -> String {

        var tempDict : [String : [String : String]] = [:]
        
        if filterString != "" {
            
            let strData = filterString.data(using: String.Encoding.utf8)
            tempDict = try! JSONSerialization.jsonObject(with: strData!, options: []) as! [String : [String : String]]
        }
        
        
        if filterLevel1 != [:] {
            tempDict[self.level_1_key] = self.filterLevel1
        } else {
            
            if let index = tempDict.index(forKey: self.level_1_key) {
                tempDict.remove(at: index)
            }
        }
        
        if filterLevel2 != [:] {
            tempDict[self.level_2_key] = self.filterLevel2
        } else {
            
            if let index = tempDict.index(forKey: self.level_2_key) {
                tempDict.remove(at: index)
            }
        }
                        
        if tempDict == [:] {
            return ""
        }
        
        let data = try! JSONSerialization.data(withJSONObject: tempDict, options: [])
        return String(data: data, encoding: .utf8)!
    }
    
    func getCategoryProduct() {
        
        
       // let value = UserDefaults.standard.value(forKey: "AppleLanguages") as! [String]
        
        if APP_DEL.isFromHomeBannerTopID == "" {
            
//            if value[0] == "ar" {
//                APP_DEL.isFromHomeBannerTopID = APP_DEL.arrCategoriesTopCat.last?.main_category_id ?? ""
//            } else {
                APP_DEL.isFromHomeBannerTopID = APP_DEL.arrCategoriesTopCat.first?.main_category_id ?? ""
         //   }
            
        } else {
            
            let index_row = APP_DEL.arrCategoriesTopCat.firstIndex { (item_category) -> Bool in
                return (item_category.main_category_id == APP_DEL.isFromHomeBannerTopID)
            }
            
            if let tempIndexRow = index_row {
                
                APP_DEL.isFromHomeBannerTopID = APP_DEL.arrCategoriesTopCat[tempIndexRow].main_category_id ?? ""
                
            } else {
                
//                if value[0] == "ar" {
//                    APP_DEL.isFromHomeBannerTopID = APP_DEL.arrCategoriesTopCat.last?.main_category_id ?? ""
//                } else {
                    APP_DEL.isFromHomeBannerTopID = APP_DEL.arrCategoriesTopCat.first?.main_category_id ?? ""
       //         }
            }
         
        }
        
        if APP_DEL.isFromHomeBannerID == "" {
            
//            if value[0] == "ar" {
//                APP_DEL.isFromHomeBannerID = APP_DEL.arrCategoriesCat.last?.main_category_id ?? ""
//                self.categoryImage = APP_DEL.arrCategoriesCat.last?.main_category_image ?? ""
//            } else {
                APP_DEL.isFromHomeBannerID = APP_DEL.arrCategoriesCat.first?.main_category_id ?? ""
                self.categoryImage = APP_DEL.arrCategoriesCat.first?.main_category_image ?? ""
      //      }
            
        } else {
            
            let index_row = APP_DEL.arrCategoriesCat.firstIndex { (item_category) -> Bool in
                return (item_category.main_category_id == APP_DEL.isFromHomeBannerID)
            }
            
            
            if let tempIndexRow = index_row {
                
                APP_DEL.isFromHomeBannerID = APP_DEL.arrCategoriesCat[tempIndexRow].main_category_id ?? ""
                self.categoryImage = APP_DEL.arrCategoriesCat[tempIndexRow].main_category_image ?? ""
                
            } else {
                
//                if value[0] == "ar" {
//                    APP_DEL.isFromHomeBannerID = APP_DEL.arrCategoriesCat.last?.main_category_id ?? ""
//                    self.categoryImage = APP_DEL.arrCategoriesCat.last?.main_category_image ?? ""
//                } else {
                    APP_DEL.isFromHomeBannerID = APP_DEL.arrCategoriesCat.first?.main_category_id ?? ""
                    self.categoryImage = APP_DEL.arrCategoriesCat.first?.main_category_image ?? ""
         //       }
            }
            
            //isFromHomeBannerID = ""
        }
        
       
        
        
        self.collTopCategory.reloadData()
        self.collType.reloadData()
       
        guard let storeId = UserDefaults.standard.value(forKey: "storeId") as? String else {return}
        
        var postData = [String:String]()
        postData["page"] = "\(self.currentPage)"
        postData["id"] = APP_DEL.isFromHomeBannerID
        postData["theme"] = "1"
        postData["store_id"] = storeId
        if APP_DEL.strFilterWithDeeplink != ""{
            postData["multi_filter"] = APP_DEL.strFilterWithDeeplink
           
        }
        else
        {
            postData["multi_filter"] = self.updateFilterSTR()//filterString
        }
        
        
        if self.filterString == ""{
            
            self.lblAppliedFilter.isHidden = true
        }
        else
        {
            self.lblAppliedFilter.isHidden = false
        }
        
        
        print(postData["multi_filter"] ?? "")
        activityIndi.isHidden = false
        activityIndi.startAnimating()
        
        if !self.isCategoryLoadFromHome{
            cedMageLoaders.removeLoadingIndicator(me: self);
            cedMageLoaders.addDefaultLoader_withlockedbackground(me: self);
        }
        else
        {
            self.isCategoryLoadFromHome = false
        }
   
        var baseURL = ""
        if self.currentPage == 1 {
            
             baseURL = "mobiconnect/catalog/productwithattribute"
        }
        else
        {
           baseURL = "mobiconnect/catalog/productwithoutattribute/"
        }
        self.loading = true
        
        API().callAPI(endPoint: baseURL, method: .POST, param: postData) { (json, err) in
            
          //  DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                self.activityIndi.isHidden = true
                               self.activityIndi.stopAnimating()
                let json_0 = json[0]

            
            
                if err == nil {
                    
                   if (self.currentPage == 1) {
                    if json_0[0].stringValue == "NO_PRODUCTS" {
                        
//                        self.filterLevel2Index = -1
//                        self.filterLevel2 = [:]
//                        self.level_2 = []
//                        self.filterDT.selected_level_2_code = ""
                        self.products = []
                        self.noProductCheck = true;
                    }
                   }
                   else if json_0.stringValue == "NO_PRODUCTS"
                   {
                         self.noProductCheck = true;
                    
                   }
                    
                    if (self.currentPage == 1) {
                        
                        if self.noProductCheck == false {
                            
                            if !self.isFilterApplied{
                                
                                do {
                                    
                                    self.arrFilter = try JSONDecoder().decode([Filter].self, from: json[1][0]["filter"].rawData())
                                    
                                } catch { }
                                _ = self.arrFilter.filter { (fil) -> Bool in
                                    
                                    if fil.type == .Category_1 {
                                        
                                        if self.filterDT.selected_level_1_code == ""{
                                            
                                            self.level_1 = [fil]
                                            return true
                                        }
                                    }
                                    else if fil.type == .Category_2 {
                                        if self.filterDT.selected_level_2_code == ""{
                                            self.level_2 = [fil]
                                            return true
                                            
                                        }
                                        
                                        
                                    }
                                    
                                    return false
                                }
                                
                                
                                
                                
                                if (self.level_1.count > 0) && ((self.level_1[0].att_code ?? "") != "") {
                                    self.level_1_key = self.level_1[0].att_code ?? ""
                                }
                                
                                if (self.level_2.count > 0) && ((self.level_2[0].att_code ?? "") != "") {
                                    self.level_2_key = self.level_2[0].att_code ?? ""
                                }
                                
                                
                            }
                            
                            
                        }
                        
                        self.products = []
                    }
                    
                    do {
                        
                        let catData = try json_0["data"]["products"].rawData()
                        let temp = try JSONDecoder().decode([CategoryProduct].self, from: catData)
                        self.products.append(contentsOf: temp as [CategoryProduct])
                        
                    } catch {
                        
                    }
                          
                } else {
                    
                    self.products = []
                }
            
            
            if self.currentPage == 1{
                
                if (self.products.count > 0) && (self.collectionProduct.numberOfItems(inSection: 0) > 0) {
                    
                    self.collectionProduct?.scrollToItem(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
                }
                
                 self.RELOAD(isFromProduct: true)
            }
            else
            {
                
                self.collectionProduct.reloadData()
            }

            self.loading = false;
          //  }
        }
    }
    
    func getFilter() {
        
//        var postData = [String:String]()
//
//        guard let storeId = UserDefaults.standard.value(forKey: "storeId") as? String else {
//            return;
//        }
//        postData["store_id"] = storeId
//        postData["id"] = self.categoryId
//        postData["multi_filter"] = self.updateFilterSTR()
//
//        API().callAPI(endPoint: "mobiconnect/catalog/attributes", method: .POST, param: postData) { (json, err) in
//
//            DispatchQueue.main.async {
//
//                // cedMageLoaders.removeLoadingIndicator(me: self);
//
//                if err == nil {
//
//                    if json[0]["status"].stringValue == "true" {
//
//                        self.arrFilter = try! JSONDecoder().decode([Filter].self, from: json[0]["filter"].rawData())
//
//                        _ = self.arrFilter.filter { (fil) -> Bool in
//
//                            if fil.type == .Category_1 {
//
//                                if self.filterDT.selected_level_1_code == ""{
//
//                                    self.level_1 = [fil]
//                                    return true
//                                }
//                            }
//                            else if fil.type == .Category_2 {
//                                if self.filterDT.selected_level_2_code == ""{
//                                    self.level_2 = [fil]
//                                    return true
//
//                                }
//
//
//                            }
//
//                            return false
//                        }
//
//
//                        if (self.level_1.count > 0) && ((self.level_1[0].att_code ?? "") != "") {
//                            self.level_1_key = self.level_1[0].att_code ?? ""
//                        }
//
//                        if (self.level_2.count > 0) && ((self.level_2[0].att_code ?? "") != "") {
//                            self.level_2_key = self.level_2[0].att_code ?? ""
//                        }
//
//                        self.RELOAD(isFromProduct: false)
//
//                    }
//                }
//            }
//        }
    }
    
    
    func checkPagination() {
    
        currentPage += 1
        self.getCategoryProduct()
        
    }
    
}



extension CategoryVC : UICollectionViewDataSource , UICollectionViewDelegate ,UICollectionViewDelegateFlowLayout{
   
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collType{
            
            return APP_DEL.arrCategoriesCat.count
            
        }else if collectionView == collectionProduct {
            
            return products.count
            
        } else if collectionView == collSubCategory {
            
            if level_1.count > 0 {
                
                return (level_1[0].filter_data?.count ?? 0)
            }
                        
        } else if collectionView == collSubCatType {
            
            if level_2.count > 0 {
                
                return (level_2[0].filter_data?.count ?? 0)
            }
            
        } else if collectionView == collTopCategory {
            
            return (APP_DEL.arrCategoriesTopCat.count ?? 0)
        }
        
        
        
        return 0
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == collType{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryMainCell", for: indexPath) as! CategoryMainCell
            
              let dictdata = APP_DEL.arrCategoriesCat[indexPath.row]
            cell.lblTitle.text = dictdata.main_category_name?.uppercased()

            if APP_DEL.isFromHomeBannerID == (dictdata.main_category_id) {
                            
//                cell.lblTitle.font = UIFont(name: "Cairo-Bold", size: 18)!
//                cell.lblTitle.textColor = UIColor.init(hexString: "#FBAD18")!
//                cell.lblDevider.isHidden = true
                
                cell.lblTitle.font = UIFont(name: "Cairo-Bold", size: 18)!
                cell.lblDevider.isHidden = false
                
            } else {
                
//                cell.lblTitle.font = UIFont(name: "Cairo-Regular", size: 17)!
//                cell.lblTitle.textColor = UIColor.lightGray
//                cell.lblDevider.isHidden = true
                
                cell.lblTitle.font = UIFont(name: "Cairo-Regular", size: 17)!
                cell.lblDevider.isHidden = true
            }
            
            cell.lblTitle.textColor = UIColor.black
            cell.lblTitle.superview?.backgroundColor = UIColor.init(hexString: "#FBAD18")!
            cell.lblDevider.backgroundColor = UIColor.black
            
            return cell
            
        }else if collectionView == collectionProduct {
            
            let value=UserDefaults.standard.value(forKey: "AppleLanguages") as! [String]
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CatProductCell", for: indexPath) as! CatProductCell
            
            if self.products.count > indexPath.item{
                
                if (APP_DEL.isDiscountTagEnable == "0"){
                    cell.offerLabel.isHidden = true
                }
                else
                {
                    let offerString = products[indexPath.item].offer ?? ""
                    
                    if offerString != "" {
                        
                        let offer = " \(offerString)% \(APP_LBL().off.uppercased()) "
                        
                        cell.offerLabel.text = offer
                        cell.offerLabel.isHidden = false
                    } else {
                        cell.offerLabel.isHidden = true
                    }
                }
                
                cell.lblProductName.text = products[indexPath.item].product_name
                
                let tagString = products[indexPath.item].tag ?? ""
                if tagString != "" {
                    
                  
                    cell.lblTags.text = " " + tagString + " "
                    cell.lblTags.isHidden = false
                } else {
                    cell.lblTags.isHidden = true
                }
                
                
                let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: products[indexPath.item].regular_price!)
                attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 1, range: NSMakeRange(0, attributeString.length))
                
                cell.lblBrandName.text = products[indexPath.item].brands_name
                cell.lblRegularPrice.isHidden = true
                if products[indexPath.item].special_price != "no_special" {
                    
                    cell.lblPrice.text = products[indexPath.item].special_price;
                    
                    cell.lblRegularPrice.attributedText = attributeString
                    
                    cell.lblRegularPrice.isHidden = false
                    cell.lblRegularPrice.alpha = 1
                    
                } else {
                    
                    cell.lblPrice.text = products[indexPath.item].regular_price
                    
                    cell.lblRegularPrice.isHidden = true
                    cell.lblRegularPrice.alpha = 0
                }
                
                cell.imgProduct.image = nil
                
    //            if let downloadURL = SDImageCache.shared.imageFromCache(forKey: products[indexPath.item].product_image!){
    //                cell.imgProduct!.image = downloadURL
    //            } else {
                    cell.imgProduct!.sd_setImage(with: URL(string: products[indexPath.item].product_image!), placeholderImage: nil)
                //}
                
                if value[0] == "ar" {
                    cell.lblBrandName.textAlignment = .right
                    cell.lblPrice.textAlignment = .right
                    cell.lblRegularPrice.textAlignment = .left
                    cell.lblProductName.textAlignment = .right
                } else {
                    cell.lblBrandName.textAlignment = .left
                    cell.lblPrice.textAlignment = .left
                    cell.lblRegularPrice.textAlignment = .right
                    cell.lblProductName.textAlignment = .left
                }
                
                
                
            }
            
            return cell
            
        }else if collectionView == collSubCategory {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SubCategoryCell", for: indexPath) as! SubCategoryCell
                  
            cell.lblTitle.text = (level_1[0].filter_data?[indexPath.item].filter_value ?? "").uppercased()
            
            if self.filterLevel1Index == (indexPath.item) {
                cell.lblTitle.textColor = nejreeColor//UIColor.black
                cell.viewBg.backgroundColor = UIColor.black//nejreeColor
            } else {
                cell.lblTitle.textColor = UIColor.white
                cell.viewBg.backgroundColor = UIColor.black
            }

            return cell
            
        } else if collectionView == collSubCatType {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SubCategoryTypeCell", for: indexPath) as! SubCategoryTypeCell
            
            cell.lblTitle.text = (level_2[0].filter_data?[indexPath.item].filter_value ?? "").uppercased()
            cell.imgType.backgroundColor = UIColor.white
            //cell.imgType!.sd_setImage(with: URL(string: level_2[0].filter_data?[indexPath.item].filter_img ?? ""), placeholderImage: nil)
            
            cell.imgType.image = nil
            
//            if let downloadURL = SDImageCache.shared.imageFromCache(forKey: level_2[0].filter_data?[indexPath.item].filter_img ?? ""){
//                cell.imgType!.image = downloadURL
//            } else {
                cell.imgType!.sd_setImage(with: URL(string: level_2[0].filter_data?[indexPath.item].filter_img ?? ""), placeholderImage: nil)
//            }
            
            if self.filterLevel2Index == (indexPath.item) {
                cell.lblTitle.textColor = nejreeColor
            } else {
                cell.lblTitle.textColor = UIColor.white
            }
            
            return cell
        } else if collectionView == collTopCategory {
            
            let cell = self.collTopCategory.dequeueReusableCell(withReuseIdentifier: "CategoryTopCell", for: indexPath) as! CategoryTopCell
            
            let cat = APP_DEL.arrCategoriesTopCat[indexPath.row]
            
            cell.lblTitle.text = (cat.main_category_name ?? "").uppercased()
            
            if APP_DEL.isFromHomeBannerTopID == (cat.main_category_id ?? "") {
                            
                cell.lblTitle.font = UIFont(name: "Cairo-Bold", size: 17)!
                cell.lblTitle.textColor = UIColor.init(hexString: "#FBAD18")!
                cell.lblDevider.isHidden = true
                
            } else {
                
                cell.lblTitle.font = UIFont(name: "Cairo-Regular", size: 17)!
                cell.lblTitle.textColor = UIColor.white
                cell.lblDevider.isHidden = true
            }
            
            return cell;
        }
            
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == collSubCategory {
           
            self.filterLevel1Index = indexPath.item
            
            if (level_1.count > 0) && ((level_1[0].filter_data?.count ?? 0) > (indexPath.item)) {
                
                
                if self.filterDT.selected_level_1_code == (level_1[0].filter_data?[indexPath.item].filter_code ?? ""){
                    
                    self.filterLevel1Index = -1
                    self.filterDT.selected_level_1_code = ""
                    filterLevel1 = [:]
                }
                else
                {
                    filterLevel1 = [(level_1[0].filter_data?[indexPath.item].filter_code ?? ""):(level_1[0].filter_data?[indexPath.item].filter_value ?? "")]
                    self.filterDT.selected_level_1_code = (level_1[0].filter_data?[indexPath.item].filter_code ?? "")
                }
                
            }
            
            UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.transitionCrossDissolve, animations: {
                     
            //self.constraintCollectionCategoryHeight.constant = 40
            self.constraintCollTopCategory.constant = 0
            self.stackBottomHT.constant = (44 + 10)
            self.tabBarController?.tabBar.isHidden = false
            self.view.layoutIfNeeded()
                     
            })

            
            self.filterLevel2Index = -1
            filterLevel2 = [:]
            self.filterDT.selected_level_2_code = ""
            
         //   self.updateBannerImage()
            
            self.getFilter()
            
            self.filterApply(filter: self.filterString, filterDT: self.filterDT, isForShowFilterView: false)
            
        } else if collectionView == collSubCatType {
            
            self.filterLevel2Index = indexPath.item
            
            if (level_2.count > 0) && ((level_2[0].filter_data?.count ?? 0) > (indexPath.item)) {
                
                if self.filterDT.selected_level_2_code == (level_2[0].filter_data?[indexPath.item].filter_code ?? ""){
                    
                    self.filterLevel2Index = -1
                    self.filterDT.selected_level_2_code = ""
                    filterLevel2 = [:]
                }
                else
                {
                    filterLevel2 = [(level_2[0].filter_data?[indexPath.item].filter_code ?? ""):(level_2[0].filter_data?[indexPath.item].filter_value ?? "")]
                    self.filterDT.selected_level_2_code = (level_2[0].filter_data?[indexPath.item].filter_code ?? "")
                }
                
                
               
            }
            
            
           UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.transitionCrossDissolve, animations: {
                     
            //self.constraintCollectionCategoryHeight.constant = 40
            self.constraintCollTopCategory.constant = 0
            self.stackBottomHT.constant = (44 + 10)
            self.tabBarController?.tabBar.isHidden = false
            self.view.layoutIfNeeded()
                     
            })

            
            
            
            self.getFilter()
            self.filterApply(filter: self.filterString, filterDT: self.filterDT, isForShowFilterView: false)
            
        } else if collectionView == collectionProduct {
            
            let vc = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "productViewController") as! productViewController
            vc.product_id = products[indexPath.row].product_id ?? ""
             APP_DEL.productIDglobal = products[indexPath.row].product_id ?? ""
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
        } else if collectionView == collType {
            
            if APP_DEL.isFromHomeBannerID != (APP_DEL.arrCategoriesCat[indexPath.row].main_category_id ?? "") {
                
                print("Down swipe")
                UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.transitionCrossDissolve, animations: {
                               
                  self.constraintImageBannerHeight.constant = self.BannerHeight
                    self.constraintCollectionCategoryHeight.constant = 40

                    self.view.layoutIfNeeded()
                               
                })
                
                APP_DEL.isFromHomeBannerID = APP_DEL.arrCategoriesCat[indexPath.row].main_category_id ?? ""
                self.categoryImage = APP_DEL.arrCategoriesCat[indexPath.row].main_category_image ?? ""
               
                self.isFilterApplied = false
                
                self.filterLevel2Index = -1
                filterLevel2 = [:]
                self.filterDT.selected_level_2_code = ""

                self.filterLevel1Index = -1
                filterLevel1 = [:]
                self.filterDT.selected_level_1_code = ""
                
                 self.getFilter()
                
                self.filterApply(filter: "", filterDT: FilterData(), isForShowFilterView: false)
                self.collType.reloadData()
                
                self.level_1 = []
                self.level_2 = []
                self.products = []
       
                
                APP_DEL.delegateUpdateHome?.updateHome(res: true)
                
            }
        } else if collectionView == collTopCategory {
            
            self.filterLevel2Index = -1
            self.filterLevel2 = [:]
            self.level_2 = []
            self.filterDT.selected_level_2_code = ""
            
            self.filterLevel1Index = -1
            self.filterLevel1 = [:]
            self.level_1 = []
            self.filterDT.selected_level_1_code = ""
            
            self.isFilterApplied = false
            self.currentPage = 1
            self.noProductCheck = false
            
            APP_DEL.isFromHomeBannerID = ""
            self.setSubCategory(index: indexPath.row)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == collType{
            var width = CGFloat()
            let height : CGFloat = 40
            
            if !isColScroll {
                width = SCREEN_WIDTH/CGFloat(APP_DEL.arrCategoriesCat.count)
            }else{
               let fixSpaceToAdd : CGFloat = 10
                let dictData = APP_DEL.arrCategoriesCat[indexPath.row]
                if (dictData.main_category_name) != "" {
                let text  = dictData.main_category_name
                width =  self.textWidth(font:  UIFont(name: "Cairo-Bold", size: 17)!, text: text!) + fixSpaceToAdd
            } else {
                width = 100
            }
            }
            return CGSize(width: width, height: height)

            
        } else if collectionView == collTopCategory{
            
            return CGSize(width: (SCREEN_WIDTH / CGFloat(APP_DEL.arrCategoriesTopCat.count)), height: 40.0)

            
        } else if collectionView == collSubCategory {
            
            var width = CGFloat()
            let height : CGFloat = 30
            let fixSpaceToAdd : CGFloat = 20.0//35.0
            
            var text = ""
            
            if level_1.count > 0 {
                text = (level_1[0].filter_data?[indexPath.item].filter_value ?? "").uppercased()
            }
            
            if text != "" {
                width =  self.textWidth(font:  UIFont(name: "Cairo-Regular", size: 15)!, text: text) + fixSpaceToAdd
            } else {
                width = 100
            }
            
                return CGSize(width: width, height: height)
        }else if collectionView == collSubCatType {
            
            return CGSize(width: collSubCatType.frame.size.height , height: collSubCatType.frame.size.height)
            
        }else{
            return CGSize(width: (collectionProduct.frame.size.width/2)-3, height: (collectionProduct.frame.size.width/2)+70)
        }
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView == collectionProduct {
            return 0
        }else if collectionView == collSubCatType {
            return 10
        }else if collectionView == collType{
            return 0
        }else{
            return 0
        }

    }

    
   func textWidth(font: UIFont, text: String) -> CGFloat {
            let myText = text as NSString
            
            let rect = CGSize(width: CGFloat.greatestFiniteMagnitude, height: CGFloat.greatestFiniteMagnitude)
            let labelSize = myText.boundingRect(with: rect, options: .usesLineFragmentOrigin, attributes: [kCTFontAttributeName as NSAttributedString.Key: font], context: nil)
            
            return ceil(labelSize.width)
        }
    
}



//MARK:- SEARCH CODE
extension CategoryVC {
    
    
    func setSearchUI() {
                
        
        let value=UserDefaults.standard.value(forKey: "AppleLanguages") as! [String]
        if value[0] == "ar" {
            self.txtSearch.textAlignment = .right
        } else {
            self.txtSearch.textAlignment = .left
        }
        
        
        viewNotification.round(redius: 16)
        
        lblNotificCount.round(redius: 5)
        
        viewFilter.round(redius: 16)
        
        viewSearch.round(redius: 16)
        viewTxtSearch.round(redius: 16)
        viewTxtSearch.layer.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        viewTxtSearch.layer.borderWidth = 0.5
        viewTxtSearch.layer.shadowColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        viewTxtSearch.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        viewTxtSearch.layer.shadowRadius = 4
        viewTxtSearch.layer.shadowOpacity = 0.5
        viewTxtSearch.isHidden = true
        
        viewSearchList.round(redius: 16)

        
        
        self.tblView.rowHeight = UITableView.automaticDimension
        self.tblView.estimatedRowHeight = UITableView.automaticDimension
        self.tblView.register(UINib(nibName: "SearchProductCell", bundle: nil), forCellReuseIdentifier: "SearchProductCell")
        self.tblView.dataSource = self
        self.tblView.delegate = self
        self.tblView.reloadData()
        self.tblView.allowsSelection = true
        
        tblView.keyboardDismissMode = .onDrag
        
        self.txtSearch.delegate = self
        self.txtSearch.placeholder = APP_LBL().search
        self.txtSearch.autocorrectionType = .no
        
        self.htTbl.constant = 4.0
        self.viewSearchList.isHidden = true
        self.tblView.isHidden = true
                
    }
    
    @IBAction func btnSearchAction(_ sender: Any) {
        
        if self.viewTxtSearch.isHidden == true {
            
            self.viewTxtSearch.isHidden = true
            //self.viewSearch.isHidden = false
            self.txtSearch.becomeFirstResponder()
            
            UIView.animate(withDuration: 0.3, delay: 0.0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.7, options: [.curveLinear], animations: { () -> Void in

                self.viewTxtSearch.isHidden = false
                self.isSearchOpen = true
                //self.viewSearch.isHidden = true
                self.view.layoutIfNeeded()
                
            }, completion: nil)
            
        } else {
            
            self.view.endEditing(true)
            
            if txtSearch.text == ""{
                
                self.closeSuggetionView()
                return
            }
            
            let detail : [AnalyticKey:String] = [
                .SearchTerm : searchText]
            API().setEvent(eventName: .Search, eventDetail: detail) { (json, err) in }
            Analytics.logEvent(AnalyticsEventSearch, parameters: [AnalyticsParameterSearchTerm : searchText])

            if let viewController =  UIStoryboard(name: "categorylayouts", bundle: nil).instantiateViewController(withIdentifier: "cedMageDefaultCollection") as?  cedMageDefaultCollection {
                
                viewController.searchString = searchText
                viewController.hidesBottomBarWhenPushed = true
                viewController.selectedCategory = APP_DEL.isFromHomeBannerID
                
                if (self.txtSearch.text != "") {
                    
                    self.navigationController?.pushViewController(viewController, animated: true)
                }
            }
        }
            
        
    }
    
    @IBAction func btnClearAction(_ sender: Any) {
        
        if self.txtSearch.text == "" {
                
            self.view.endEditing(true)
            
            self.viewTxtSearch.isHidden = false
            self.viewSearch.isHidden = true
            self.isSearchOpen = false
            
            
            UIView.animate(withDuration: 0.3, delay: 0.0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.7, options: [.curveLinear], animations: { () -> Void in

                self.viewTxtSearch.isHidden = true
                self.viewSearch.isHidden = false
                self.view.layoutIfNeeded()

            }, completion: nil)
            
        } else {
            
            self.txtSearch.text = ""
            self.searchText(text: "")
        }
    }
    
    
    func searchText(text: String) {

        searchText = text
        
        if text == "" {
                        
            self.htTbl.constant = 4.0
            self.viewSearchList.isHidden = true
            self.tblView.isHidden = true
            
        } else {
            
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(getHint), object: nil)
            self.perform(#selector(getHint), with: nil, afterDelay: 1)
        }

        //checkNoData()
    }
    
    
    @objc func getHint() {
        
        if(searchText.count >= 3) {
            
            searchText = searchText.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            getAutocomplete()
        }
    }
    
    func getAutocomplete() {
        
        guard let storeId = UserDefaults.standard.value(forKey: "storeId") as? String else {return}
        
        var postData = [String:String]()
        postData["page"] = "1"
        postData["store_id"] = storeId
        postData["q"] = searchText
        postData["autosearch"] = "yes"
        let defaults = UserDefaults.standard
        if defaults.bool(forKey: "isLogin") {
            
            if let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] {
                
                postData["hashkey"] = userInfoDict["hashKey"]
                postData["customer_id"] = userInfoDict["customerId"]
            }
        }
        
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader_withlockedbackground(me: self);
        
        API().callAPI(endPoint: "mobiconnect/catalog/productsearch", method: .POST, param: postData) { (json, err) in
            
          //  DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                let json_0 = json[0]
                
                if err == nil {

                   if (json[0].stringValue != "NO_PRODUCTS")
                    {
                        self.suggentionsArray.removeAll()
                        for (_,value) in json_0["data"]["suggestion"]
                        {
                            
                            var data = [String:String]()
                            data["product_id"] = value["product_id"].stringValue
                            data["product_name"] = value["product_name"].stringValue
                            data["product_image"] = value["product_image"].stringValue
                            self.suggentionsArray.append(data)
                        }
                        
                        self.tblView.reloadData()
                        self.viewSearchList.isHidden = false
                        self.tblView.isHidden = false
                    }
                    else
                    {
                        self.htTbl.constant = 4.0
                        self.viewSearchList.isHidden = true
                        self.tblView.isHidden = true
                    }

                }
          //  }
        }
    }

}

extension CategoryVC: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if let str = textField.text, let swtRange = Range(range, in: str) {
            
            let fullString = str.replacingCharacters(in: swtRange, with: string)
            searchText(text: fullString)
        }
        
        return true;
    }
    
//    func textFieldDidEndEditing(_ textField: UITextField) {
//
//        self.displayClear(res: ((textField.text ?? "").count > 0))
//    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {

       // filter(text: textField.text ?? "")
        self.view.endEditing(true)
        
        let detail : [AnalyticKey:String] = [
            .SearchTerm : searchText]
        API().setEvent(eventName: .Search, eventDetail: detail) { (json, err) in }
        Analytics.logEvent(AnalyticsEventSearch, parameters: [AnalyticsParameterSearchTerm : searchText])

        if let viewController =  UIStoryboard(name: "categorylayouts", bundle: nil).instantiateViewController(withIdentifier: "cedMageDefaultCollection") as?  cedMageDefaultCollection {
            
            viewController.searchString = searchText
            viewController.hidesBottomBarWhenPushed = true
            viewController.selectedCategory = APP_DEL.isFromHomeBannerID
            
            if (self.txtSearch.text != "") {
                
                self.navigationController?.pushViewController(viewController, animated: true)
            }
        }
        
        return true
    }
}

extension CategoryVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return suggentionsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchProductCell") as! SearchProductCell
        cell.selectionStyle = .none
        
        cell.imgView.sd_setImage(with: URL(string: suggentionsArray[indexPath.row]["product_image"] ?? ""), placeholderImage: nil)
            
        cell.lblTitle.text = suggentionsArray[indexPath.row]["product_name"]
        
        cell.lblDevider.isHidden = (indexPath.row == (suggentionsArray.count - 1))
        
        let value=UserDefaults.standard.value(forKey: "AppleLanguages") as! [String]
        if value[0] == "ar" {
            cell.lblTitle.textAlignment = .right
            cell.imgArrow.image = UIImage(named: "arrows_ar")
        } else {
            cell.lblTitle.textAlignment = .left
            cell.imgArrow.image = UIImage(named: "arrows")
        }
        
        return cell;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
                
        self.btnClearAction(self.btnClear)
        
        self.closeSuggetionView()
        
        let vc = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "productViewController") as! productViewController
        APP_DEL.productIDglobal = suggentionsArray[indexPath.row]["product_id"]!
        vc.product_id = suggentionsArray[indexPath.row]["product_id"]!
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    
}
