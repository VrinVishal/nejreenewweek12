//
//  CollTypeCell.swift
//  MageNative Magento Platinum
//
//  Created by Vrinsoft on 03/09/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class CollTypeCell: UICollectionViewCell {

    
    @IBOutlet weak var lblTypeTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        lblTypeTitle.setFont(fontFamily: "Cairo-Bold", fontSize: 18)
        // Initialization code

    }

}
