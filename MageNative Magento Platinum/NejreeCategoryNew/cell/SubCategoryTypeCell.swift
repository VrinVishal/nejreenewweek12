//
//  SubCategoryTypeCell.swift
//  MageNative Magento Platinum
//
//  Created by Vrinsoft on 29/07/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class SubCategoryTypeCell: UICollectionViewCell {

    
    @IBOutlet weak var viewBG: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgType: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        
        lblTitle.setFont(fontFamily: "Cairo-Bold", fontSize: 13)
        
        viewBG.layer.cornerRadius = 5
        viewBG.clipsToBounds = true
        
    }

}
