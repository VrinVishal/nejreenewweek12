//
//  SubCategoryCell.swift
//  MageNative Magento Platinum
//
//  Created by Vrinsoft on 29/07/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class SubCategoryCell: UICollectionViewCell {

    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        viewBg.layer.cornerRadius = 5
        viewBg.clipsToBounds = true
        
        lblTitle.setFont(fontFamily: "Cairo-Bold", fontSize: 13)
        
    }

}
