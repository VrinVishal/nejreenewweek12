//
//  CatProductCell.swift
//  MageNative Magento Platinum
//
//  Created by Vrinsoft on 29/07/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class CatProductCell: UICollectionViewCell {
        
    
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var offerLabel: UILabel!
    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var lblBrandName: UILabel!
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblRegularPrice: UILabel!
 
    @IBOutlet weak var lblTags: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        //cell.offerLabel.round(redius: 18)
        offerLabel.round(redius: 5)
        // lblTags.round(redius: 5)
        
        imgProduct.layer.cornerRadius = 2
        imgProduct.clipsToBounds = true
        
        lblBrandName.setFont(fontFamily: "Cairo-Bold", fontSize: 13)
        lblProductName.setFont(fontFamily: "Cairo-Regular", fontSize: 13)
        lblPrice.setFont(fontFamily: "Cairo-Regular", fontSize: 13)
        lblRegularPrice.setFont(fontFamily: "Cairo-Regular", fontSize: 13)
        // Initialization code
        
       lblBrandName.textAlignment = .center
       lblProductName.textAlignment = .center
        lblPrice.textAlignment = .center
        lblRegularPrice.textAlignment = .center

    }

}
