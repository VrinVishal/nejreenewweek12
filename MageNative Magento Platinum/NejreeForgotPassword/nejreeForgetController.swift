//
//  nejreeForgetController.swift
//  MageNative Magento Platinum
//
//  Created by MacMini on 17/08/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class nejreeForgetController: MagenativeUIViewController {

  
    @IBOutlet weak var dismissButton: UIButton!
    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var emailtextField: UITextField!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var emailValidImage: UIImageView!
    @IBOutlet weak var sideView: UIView!
    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var emaiImage: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        sendButton.addTarget(self, action: #selector(sendButtonTappeD(_:)), for: .touchUpInside)
        emailtextField.delegate = self
        sideView.backgroundColor = .black
        emailValidImage.image = UIImage(named: "")
 
//        emailView.layer.borderColor = UIColor.init(hexString: "#FBAD18")?.cgColor
//        emailView.layer.borderWidth = 1.0
    
        topLabel.text = APP_LBL().password_reset.uppercased()
        topLabel.textColor = nejreeColor
        
        contentLabel.text = APP_LBL().please_enter_your_email_so_that_we_can_send_you_the_password_reset_steps
        contentLabel.numberOfLines = 0
        self.navigationController?.navigationBar.isHidden = true
        dismissButton.addTarget(self, action: #selector(dismissTapped(_:)), for: .touchUpInside)
       
        sendButton.setCornerRadius()
        sendButton.setBorder()
        sendButton.layer.borderColor = UIColor.white.cgColor
        sendButton.setTitleColor(UIColor.white, for: .normal)
        
        emailView.setCornerRadius()
        emailView.setBorder()
        
        emailtextField.placeholder = APP_LBL().email_star.uppercased()
        emailtextField.attributedPlaceholder = NSAttributedString(string: APP_LBL().email_star.uppercased(),
        attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        
        
        sendButton.setTitle(APP_LBL().send, for: .normal)
        
        if APP_DEL.selectedLanguage == Arabic {
            emailtextField.textAlignment = .right
            dismissButton.setImage(UIImage(named: "BackArrowNewArabic"), for: .normal)
            
            topLabel.font = UIFont(name: "Cairo-Bold", size: 15)!
        }else {
            emailtextField.textAlignment = .left
             dismissButton.setImage(UIImage(named: "BackArrowNew"), for: .normal)
            
            topLabel.font = UIFont(name: "Cairo-Bold", size: 17)!
        }
        
      
        
        // Do any additional setup after loading the view.
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @objc func dismissTapped(_ sneder: UIButton) {
        self.dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
        
    }
    
    func forgotPassword() {
            
            cedMageLoaders.removeLoadingIndicator(me: self);
            cedMageLoaders.addDefaultLoader(me: self);
            
            var postData = [String:String]()
            
            if let store_id = self.defaults.object(forKey: "storeId") as? String {
                postData["store_id"] = store_id;
            }
            postData["email"] = emailtextField.text!;
            
            
            API().callAPI(endPoint: "mobiconnect/customer/forgotpassword", method: .POST, param: postData) { (json_res, err) in
                
             //   DispatchQueue.main.async {
                    
                    cedMageLoaders.removeLoadingIndicator(me: self);
                    
                    if err == nil {
                        
                        let json = json_res[0]
                        
                        if json["data"]["customer"][0]["status"].stringValue == "success" {
                            
                            let str = APP_LBL().if_there_is_account_associated + " " + self.emailtextField.text! + " " + APP_LBL().you_will_get_email
                            
                            cedMageHttpException.showAlertView(me: self, msg: str, title: APP_LBL().success)
                            
                        } else {
                            
                            let title = APP_LBL().error
                            let msg = APP_LBL().this_email_address_not_found
                            cedMageHttpException.showAlertView(me: self, msg: msg, title: title)
                        }
                    }
              //  }
            }
        }
}

extension nejreeForgetController: UITextFieldDelegate {
    

    func textFieldDidEndEditing(_ textField: UITextField) {
        let str = emailtextField.text!
        
        if str != "" {
            if isValidEmail(email: str) {
                
                emailValidImage.image = UIImage(named: "iconCorrectCheckMark")
               // sideView.backgroundColor = UIColor.init(hexString: "#FBAD18")
                emailtextField.textColor = UIColor.white
                emailView.layer.borderColor = nejreeColor?.cgColor
                
                sendButton.setTitleColor(UIColor.white, for: .normal)
                sendButton.backgroundColor = UIColor(red: 251/255, green: 173/255, blue: 24/255, alpha: 1.0)
                
                
            } else {
                emailValidImage.image = UIImage(named: "IconIncorrectCheck")

                    emailtextField.textColor = UIColor.white
                
                emailtextField.textColor = nejreeColor
             
                  
                sendButton.setTitleColor(UIColor.white, for: .normal)
                sendButton.backgroundColor = UIColor.clear
            }
            
            
            
        } else {
            emailValidImage.image = UIImage(named: "IconIncorrectCheck")
          //  sideView.backgroundColor = UIColor.clear
            //                emailtextField.titleColor = UIColor.init(hexString: "#741A31")!
            emailView.layer.borderColor = UIColor.init(hexString: "#FFFFFF")?.cgColor
           // sideView.backgroundColor = .black
          //  emaiImage.tintColor = UIColor.init(hexString: "#741A31")
          sendButton.setTitleColor(UIColor.white, for: .normal)
          sendButton.backgroundColor = UIColor.clear
        }
        
        
    }
    
    
    @objc func sendButtonTappeD(_ sender: UIButton) {
        
        
        self.view.endEditing(true)
        let validEmail = EmailVerifier.isValidEmail(testStr: self.emailtextField.text!)
             
        
        if self.emailtextField.text == ""{
             cedMageHttpException.showAlertView(me: self, msg: APP_LBL().please_enter_email, title: APP_LBL().error)
             return;
         }
         else if !validEmail {
              cedMageHttpException.showAlertView(me: self, msg: APP_LBL().please_enter_valid_email, title: APP_LBL().error)
             return;
         }
        
        
        
//        if self.emailtextField.text! == "" {
//            self.view.makeToast("Please Enter Email", duration: 1.0, position: .center)
//            return
//        }
//
//        if !isValidEmail(email: emailtextField.text!) {
//            self.view.makeToast("Please enter a valid email", duration: 1.0, position: .center)
//            return
//        }
        self.forgotPassword()
    }
    
    
    
    
    
}
