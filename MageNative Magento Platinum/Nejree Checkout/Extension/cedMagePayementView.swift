//
//  cedMagePayementView.swift
//  MageNative Magento Platinum
//
//  Created by CEDCOSS Technologies Private Limited on 20/07/17.
//  Copyright © 2017 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit
import FirebaseAnalytics


struct ItemBlock : Codable {
    
    let productId : String?
    let productName : String?
    let productPrice : String?
    let productQuantity : String?
    
    enum CodingKeys: String, CodingKey {

        case productId = "productId"
        case productName = "productName"
        case productPrice = "productPrice"
        case productQuantity = "productQuantity"
    }
}


//class cedMagePayementView: MagenativeUIViewController,UITableViewDelegate,UITableViewDataSource {
class cedMagePayementView: MagenativeUIViewController {
    
    
    var isOrderReview = false;// to be used in case of order review
    var orderEmail = "";// to be used in case of order review
    var all_pro_qty=[String]()
    var cart_id = "";
    
    var applePaySuccessCheck = false;

    
    var products = [[String: String]]();
    var options_selected = [String:[String:String]]();
    var bundle_options = [String:[String:String]]();
    var bundle_options_values = [String:[String:[String]]]();
    var total = [String: String]();
    var items_count = "";
    var items_qty = "";
    var allowed_guest_checkout = "";
    var tokenstr = "";
    //PayMent method
    var selectedPaymentMethod = String()
    
     
    
    //var productsData = [cartProduct]()
    
    
    var orderStatusData = [String:String]()
    var flag=false
    //payment checkout
    let clientTokenUrl  =     "mobiconnect/mobibrain/generatetokenpost"
    let trasactionUrl   =     "mobiconnect/mobibrain/transaction"
    let finalOrderCheck =     "mobiconnect/checkout/additionalinfo"
    let saveOrder       =     "mobiconnect/checkout/saveorder"
    var previousMethods = [[String:String]]()
    
//    let paycontroller = PayFortController.init(enviroment: KPayFortEnviromentProduction)
    let paycontroller = PayFortController.init(enviroment: KPayFortEnviromentSandBox)
    
    var mainvc: UIViewController?
    var orderId = ""
    
    @IBOutlet weak var tableView: UITableView!
    var paymentsDetail = [[String:String]]()
    var clientToken = ""
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.tableView.delegate = self
//        self.tableView.dataSource = self
        self.view.makeToast(APP_LBL().order_is_processing, duration: 1, position: .center, title: nil, image: nil, style: nil, completion: nil)
        if(defaults.object(forKey: "cartId") != nil){
        cart_id = (defaults.object(forKey: "cartId") as? String)!;
        }
        
//      if(self.defaults.object(forKey: "userInfoDict") != nil){
//          var postData = [String:String]()
//          let userInfoDict = UserDefaults.standard.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
//          postData["hashkey"] = userInfoDict["hashKey"]!
//          postData["customer_id"] = userInfoDict["customerId"]!;
//          postData["store_id"] = UserDefaults.standard.value(forKey: "storeId") as? String
//          self.sendRequest(url: "mobiconnect/removeStoreCredit", params: postData)
//      }
//
        
        
        
        //self.loadData()
        

    }
    
    
//
//    func loadData() {
//        userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
//        var urlToRequest = "mobiconnect/checkout/saveorder";
//        let requestHeader = cedMage.getInfoPlist(fileName:"cedMage",indexString: "requestheader") as! String
//        let baseURL = cedMage.getInfoPlist(fileName:"cedMage",indexString: "cedBaseUrl") as! String;
//
//        urlToRequest = baseURL+urlToRequest;
//
//        var postString = "";
//        var postData = [String:String]()
//        if cart_id == ""{
//            postData["cart_id"] = "0";
//        }else{
//            postData["cart_id"] = cart_id;
//        }
//
//        postData["email"] = orderEmail;
//
//        if defaults.bool(forKey: "isLogin") {
//            if let hashKey = userInfoDict["hashKey"] {
//            postData["hashkey"] = hashKey
//            }
//
//            if let customId = userInfoDict["customerId"] {
//                postData["customer_id"] = customId
//            }
//        }
//        postString = ["parameters":postData].convtToJson() as String
//        print(postString)
//        var request = URLRequest(url: URL(string: "\(urlToRequest)")!);
//        request.httpMethod = "POST";
//        request.httpBody = postString.data(using: String.Encoding.utf8);
//        request.setValue(requestHeader, forHTTPHeaderField: "Mobiconnectheader")
//        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
//        cedMageLoaders.addDefaultLoader(me: self);
//        let task = URLSession.shared.dataTask(with: request){
//
//            // check for fundamental networking error
//            data, response, error in
//            guard error == nil && data != nil else{
//                print("error=\(error)")
//                DispatchQueue.main.async{
//                    print(error?.localizedDescription ?? "localizedDescription");
//                    cedMageLoaders.removeLoadingIndicator(me: self);
//                }
//                return;
//            }
//
//            // check for http errors
//            if let httpStatus = response as? HTTPURLResponse , httpStatus.statusCode != 200{
//
//                print("statusCode should be 200, but is \(httpStatus.statusCode)")
//                print("response = \(response)")
//                DispatchQueue.main.async{
//                    cedMageLoaders.removeLoadingIndicator(me: self);
//                    if let data = data {
//                        if let string = NSString(data:data,encoding:String.Encoding.utf8.rawValue) as? String {
//                        self.view.makeToast(string, duration: 2, position: .center, title: nil, image: nil, style: nil, completion: nil)
//                        }
//                    }
//
//                }
//
//                return;
//            }
//
//            // code to fetch values from response :: start
//            var jsonResponse = try!JSON(data: data!);
//            jsonResponse = jsonResponse[0]
//            if(jsonResponse != nil){
//
//                DispatchQueue.main.async{
//                    print(jsonResponse);
//                    cedMageLoaders.removeLoadingIndicator(me: self);
//
//
//                    if(jsonResponse["success"].stringValue == "true"){
//                        self.orderStatusData["orderId"] = jsonResponse["order_id"].stringValue
//                        self.orderStatusData["orderStatus"] = jsonResponse["success"].stringValue
//                        self.total["grandtotal"] = jsonResponse["grandtotal"].stringValue
//                        self.orderStatusData["order_date"] = jsonResponse["order_date"].stringValue
//
//                        print(self.selectedPaymentMethod)
//                        print(self.total)
//
//                        //total["amounttopay"]
//
//
//
//                        if self.selectedPaymentMethod.lowercased() == "Braintree".lowercased(){
//                            self.getClientToken()
//                        }
//                        else if (self.selectedPaymentMethod.lowercased() == "payfort") {
//                           // self.requestForPayfortPaymentInitialization(amount: self.total["grandtotal"]!, orderId: self.orderStatusData["orderId"]!, email: self.orderEmail)
//                            self.getPayfortTokeFromServer()
//                        }
//                        else if(self.selectedPaymentMethod.lowercased() == "applepayfort")
//                        {
//                             //self.requestForPaymentInitialization(amount: self.total["grandtotal"]!, orderId: self.orderStatusData["orderId"]!, email: self.orderEmail)
//                            self.getApplePayTokeFromServer()
//                        }
//                        //else if selectedPaymentMethod.lowercased() == "payfort".lowercased(){
//                            // code for checkout with payFort payment gateway goes here
//
//
//                       // }
//                        else{
//
//
//                            print("\(self.productsData)")
//
//                            var arrTempBlocks : [ItemBlock] = []
//                            for bl in self.productsData {
//                                arrTempBlocks.append(ItemBlock(productId: bl.product_id, productName: bl.productName, productPrice: bl.subTotal, productQuantity: bl.quantity))
//                            }
//                            let finalRes = self.getItemBlock(itemBlocks: arrTempBlocks)
//
//                            print(finalRes.jsonString)
//                            print(finalRes.json)
//
//
//                            let strCoupon : String = self.defaults.value(forKey: "appliedCoupon") as? String ?? ""
//
//                            Analytics.logEvent(AnalyticsEventEcommercePurchase, parameters: [
//                                AnalyticsParameterValue : self.total,
//                                AnalyticsParameterCurrency : "SAR",
//                                AnalyticsParameterCoupon : strCoupon,
//                                AnalyticsParameterTax : "",
//                                AnalyticsParameterItemID : finalRes.jsonString,
//                                AnalyticsParameterTransactionID : self.orderStatusData["orderId"] as Any,
//
//                            ])
//
//                            self.defaults.removeObject(forKey: "guestEmail");
//                            self.defaults.removeObject(forKey: "cartId");
//                            self.defaults.removeObject(forKey: "appliedCoupon");
//                            self.defaults.setValue("0", forKey: "items_count")
//                            //self.view.makeToast("Order Placed Successfully.".localized, duration: 2, position: .center, title: nil, image: nil, style: nil, completion: {
//                              //  Void in
//
//
//
//
//
//
//
//                            APP_DEL.isComesFromOrder = true
//
//                                let storyboard = UIStoryboard(name: "Main", bundle: nil);
//                                let viewController = storyboard.instantiateViewController(withIdentifier: "cedMagePaymentSuccess") as! cedMagePaymentSuccess;
//                                viewController.order_id = self.orderStatusData["orderId"]!
//                                viewController.order_date = self.orderStatusData["order_date"]!
//                                self.navigationController?.viewControllers = [viewController];
//                            //})
//                        }
//                        //self.navigationController?.present(viewController, animated: true, completion: {});
//
//                    }
//                }
//            }
//            else
//            {
//                 let strCoupon : String = self.defaults.value(forKey: "appliedCoupon") as? String ?? ""
//
//                print("\(self.productsData)")
//
//                                           var arrTempBlocks : [ItemBlock] = []
//                                           for bl in self.productsData {
//                                               arrTempBlocks.append(ItemBlock(productId: bl.product_id, productName: bl.productName, productPrice: bl.subTotal, productQuantity: bl.quantity))
//                                           }
//                                           let finalRes = self.getItemBlock(itemBlocks: arrTempBlocks)
//
//                                           print(finalRes.jsonString)
//                                           print(finalRes.json)
//
//
//                let Parameters = ["COUPON" : strCoupon as Any,
//                                  "CURRENCY" : "SAR" as Any,
//                                  "ITEMS" : finalRes.jsonString,
//                                  "VALUE" : "",
//                ]
//
//                Analytics.logEvent("failed_checkout", parameters: Parameters)
//
//            }
//        }
//        task.resume();
//    }
//
//    func getApplePayTokeFromServer() {
//
////        https://stagingapp.nejree.com/rest/V1/mobiconnect/catalog/applepay/
//
//        let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
//                            let hashKey = userInfoDict["hashKey"]!;
//                               let customerId = userInfoDict["customerId"]!;
//              let deviceID =   paycontroller?.getUDID()
//
//
//        let params = ["customer_id": customerId, "hashkey": hashKey, "store_id" : UserDefaults.standard.value(forKey: "storeId") as? String ?? "","device_id" : deviceID ?? ""]
//
//             let url = "mobiconnect/catalog/applepay/"
//            var httpUrl = cedMage.getInfoPlist(fileName:"cedMage",indexString: "cedBaseUrl") as! String
//            let reqUrl = httpUrl+url
//
//             var postString=Dictionary<String,Dictionary<String,String>>()
//             var postString1=""
//             print(reqUrl)
//             var makeRequest = URLRequest(url: URL(string: reqUrl)!)
//
//             _ = cedMage.getInfoPlist(fileName:"cedMage",indexString: "requestheader") as! String
//             let storeId = defaults.value(forKey: "storeId") as? String
//
//             if(params != nil){
//                 makeRequest.httpMethod = "POST"
//
//                     postString=["parameters":[:]]
//                     for (key,value) in params
//                     {
//                         _ = postString["parameters"]?.updateValue(value, forKey:key)
//                     }
//
//                     if storeId != nil {
//                         _ = postString["parameters"]?.updateValue(storeId!, forKey:"store_id")
//                     }
//
//                     postString1=postString.convtToJson() as String
//                     makeRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
//
//             }
//
//             print(reqUrl)
//             print(postString)
//             makeRequest.httpBody = postString1.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
//
//             let task = URLSession.shared.dataTask(with: makeRequest, completionHandler: {data,response,error in
//                 //cedMageLoaders.removeLoadingIndicator(me: self)
//           //      print(NSString(data: data!, encoding: String.Encoding.utf8.rawValue)!)
//                 // check for http errors
//                 if let httpStatus = response as? HTTPURLResponse , httpStatus.statusCode != 200
//                 {
//
//                     DispatchQueue.main.async
//                         {
//                             cedMageLoaders.removeLoadingIndicator(me: self)
//
//
//
//                             print("poststring=\(postString1)")
//                             print(NSString(data: data!, encoding: String.Encoding.utf8.rawValue) ?? "")
//                             print("statusCode should be 200, but is \(httpStatus.statusCode)")
//
//                             print("response = \(response)")
//
//                     }
//                     return;
//                 }
//
//                 // code to fetch values from response :: start
//
//
//                 guard error == nil && data != nil else
//                 {
//                     DispatchQueue.main.async
//                         {
//
//                             cedMageLoaders.removeLoadingIndicator(me: self)
//                             cedMageHttpException.showAlertView(me: self, msg: error?.localizedDescription, title: "Error".localized )
//                             print("error=\(error)")
//
//                     }
//                     return ;
//                 }
//
//                 DispatchQueue.main.async
//                     {
//                         cedMageLoaders.removeLoadingIndicator(me: self)
//
//                         print("response = \(response)")
//                        var json = try!JSON(data:data!)
//                        print(json)
//
//
////                            self.createPayfortPaymentApiRequest(response: json[0].rawValue, amount: self.total["grandtotal"]!, email: self.orderEmail, orderID: self.orderStatusData["orderId"]!)
//
//                        self.createPaymentApiRequest(response: json[0].rawValue, amount: self.total["grandtotal"]!, email: self.orderEmail, orderID: self.orderStatusData["orderId"]!)
//
//
//                        /// self.recieveResponse(data: data, requestUrl: url, response: response)
//
//                 }
//             })
//
//             task.resume()
//
//    }
//
//
//
//    func getPayfortTokeFromServer() {
//
//        let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
//                            let hashKey = userInfoDict["hashKey"]!;
//                               let customerId = userInfoDict["customerId"]!;
//              let deviceID =   paycontroller?.getUDID()
//
//
//        let params = ["customer_id": customerId, "hashkey": hashKey, "store_id" : UserDefaults.standard.value(forKey: "storeId") as? String ?? "","device_id" : deviceID ?? ""]
//
//             let url = "mobiconnect/catalog/payfort/"
//            var httpUrl = cedMage.getInfoPlist(fileName:"cedMage",indexString: "cedBaseUrl") as! String
//            let reqUrl = httpUrl+url
//
//             var postString=Dictionary<String,Dictionary<String,String>>()
//             var postString1=""
//             print(reqUrl)
//             var makeRequest = URLRequest(url: URL(string: reqUrl)!)
//
//             _ = cedMage.getInfoPlist(fileName:"cedMage",indexString: "requestheader") as! String
//             let storeId = defaults.value(forKey: "storeId") as? String
//
//             if(params != nil){
//                 makeRequest.httpMethod = "POST"
//
//                     postString=["parameters":[:]]
//                     for (key,value) in params
//                     {
//                         _ = postString["parameters"]?.updateValue(value, forKey:key)
//                     }
//
//                     if storeId != nil {
//                         _ = postString["parameters"]?.updateValue(storeId!, forKey:"store_id")
//                     }
//
//                     postString1=postString.convtToJson() as String
//                     makeRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
//
//             }
//
//             print(reqUrl)
//             print(postString)
//             makeRequest.httpBody = postString1.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
//
//             let task = URLSession.shared.dataTask(with: makeRequest, completionHandler: {data,response,error in
//                 //cedMageLoaders.removeLoadingIndicator(me: self)
//           //      print(NSString(data: data!, encoding: String.Encoding.utf8.rawValue)!)
//                 // check for http errors
//                 if let httpStatus = response as? HTTPURLResponse , httpStatus.statusCode != 200
//                 {
//
//                     DispatchQueue.main.async
//                         {
//                             cedMageLoaders.removeLoadingIndicator(me: self)
//
//
//
//                             print("poststring=\(postString1)")
//                             print(NSString(data: data!, encoding: String.Encoding.utf8.rawValue) ?? "")
//                             print("statusCode should be 200, but is \(httpStatus.statusCode)")
//
//                             print("response = \(response)")
//
//                     }
//                     return;
//                 }
//
//                 // code to fetch values from response :: start
//
//
//                 guard error == nil && data != nil else
//                 {
//                     DispatchQueue.main.async
//                         {
//
//                             cedMageLoaders.removeLoadingIndicator(me: self)
//                             cedMageHttpException.showAlertView(me: self, msg: error?.localizedDescription, title: "Error".localized )
//                             print("error=\(error)")
//
//                     }
//                     return ;
//                 }
//
//                 DispatchQueue.main.async
//                     {
//                         cedMageLoaders.removeLoadingIndicator(me: self)
//
//                         print("response = \(response)")
//                        var json = try!JSON(data:data!)
//                        print(json)
//
//
//                          // self.requestForPaymentInitialization(amount: self.total["grandtotal"]!, orderId: self.orderStatusData["orderId"]!, email: self.orderEmail)
//
//                            self.createPayfortPaymentApiRequest(response: json[0].rawValue, amount: self.total["grandtotal"]!, email: self.orderEmail, orderID: self.orderStatusData["orderId"]!)
//
//
//                        /// self.recieveResponse(data: data, requestUrl: url, response: response)
//
//                 }
//             })
//
//             task.resume()
//
//    }
//
//
//
//    func getItemBlock(itemBlocks: [ItemBlock]) -> (json: [[String:String]], jsonString: String) {
//
//        do {
//            let data = try JSONEncoder().encode(itemBlocks)
//            let json : [[String : String]] = try JSONSerialization.jsonObject(with: data, options: []) as! [[String : String]]
//            return (json: json, jsonString: String(data: data, encoding: .utf8)!)
//        } catch {
//            print("ERROR.")
//            return (json: [], jsonString: "")
//        }
//    }
//
//
//    override func recieveResponse(data: Data?, requestUrl: String?, response: URLResponse?) {
//        if let data = data {
//            var json = try!JSON(data:data)
//            print(json)
//            json = json[0]
//            print(json)
//            if requestUrl == trasactionUrl {
//                let paymentId = json["message"].stringValue
//                print(NSString(data:data,encoding:String.Encoding.utf8.rawValue))
//                if json["success"].stringValue == "true"{
//                    self.afterPayment(payment_id: paymentId, failure: "false")
//                }else{
//
//                    self.afterPayment(payment_id: paymentId, failure: "true")
//
//                }
//
//            }
//            else if requestUrl == "mobiconnect/catalog/payfort/"{
//
//                 print(json)
//            }
//
//            else if requestUrl == "mobiconnect/removeStoreCredit" {
//                UserDefaults.standard.set(false, forKey: "isStoreCredit")
//            } else if requestUrl == "mobiconnect/checkout/coupon" {
//                print("hellooo")
//                if json["cart_id"]["success"].stringValue == "true" {
//
//                    UserDefaults.standard.removeObject(forKey: "appliedCoupon");
//
//                }
//                let view = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "mainTabbar") as! cedMageTabbar
//                view.modalPresentationStyle = .fullScreen
//                self.present(view, animated: true, completion: nil)
//            }
//
//            else if requestUrl == clientTokenUrl {
//
//                if json["success"].stringValue == "true" {
//                    let clientToken = json["message"].stringValue
//                    if json["creditCards"].arrayValue.count > 0 {
//                        for creditCard in json["creditCards"].arrayValue {
//                            let maskedNumber = creditCard["maskedNumber"].stringValue
//                            let imgUrl = creditCard["imageUrl"].stringValue
//                            let token  = creditCard["token"].stringValue
//                            self.previousMethods.append(["maskedNumber":maskedNumber,"imgUrl":imgUrl,"token":token])
//                        }
//                    }
//                    self.clientToken = clientToken
//                    if previousMethods.count > 0 {
//                        self.tableView.reloadData()
//                        showDropIn(clientTokenOrTokenizationKey: clientToken)
//                    }else{
//                        showDropIn(clientTokenOrTokenizationKey: clientToken)
//                    }
//                }
//
//            }
//            else if requestUrl == finalOrderCheck{
//                if json["success"].stringValue != "false" {
//                    self.defaults.removeObject(forKey: "guestEmail");
//                    self.defaults.removeObject(forKey: "cartId");
//                    self.defaults.setValue("0", forKey: "items_count")
//                    self.view.makeToast("Payment Successful.", duration: 2, position: .center, title: nil, image: nil, style: nil, completion: {
//                        Void in
//                        let storyboard = UIStoryboard(name: "Main", bundle: nil);
//
//                        let viewController = storyboard.instantiateViewController(withIdentifier: "cedMagePaymentSuccess") as! cedMagePaymentSuccess;
//                        viewController.order_id = self.orderStatusData["orderId"]!
//                        viewController.mainvc = self.mainvc
//                        self.navigationController?.viewControllers = [viewController];
//
//                       /* let storyboard = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil);
//                        let viewController = storyboard.instantiateViewController(withIdentifier: "orderPlacedViewController") as! OrderPlacedViewController;
//                        viewController.order_id = self.orderStatusData["orderId"]!
//                        self.navigationController?.viewControllers = [viewController];*/
//                    })
//                }else if json["success"].stringValue == "false"{
//
////                    appliedCoupon
////                    "mobiconnect/checkout/coupon";
////                    let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
////                    postData["hashkey"] = userInfoDict["hashKey"]!
////                    postData["customer_id"] = userInfoDict["customerId"]!;
////                    postData["cart_id"] = cart_id;
////                     postData["remove"] = "1";
//
//                    self.view.makeToast("Order Payment Failure.", duration: 2, position: .center, title: nil, image: nil, style: nil, completion: {
//                        Void in
//                        self.removeCoupon()
//
//
//                    })
//                }
//
//            }
//        }
//
//    }
//
//    func removeCoupon() {
//        let userInfoDict = self.defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
//        var param = [String:String]()
//        param["hashkey"] = userInfoDict["hashKey"]!
//        param["customer_id"] = userInfoDict["customerId"]!;
//        param["cart_id"] = self.cart_id
//        param["remove"] = "1";
//        self.sendRequest(url: "mobiconnect/checkout/coupon", params: param)
//    }
//
//
//
//
//
//
//    func userDidCancelPayment() {
//        self.dismiss(animated: true, completion: nil)
//        self.afterPayment(payment_id: "", failure: "true")
//    }
//
//    func drop(_ viewController: BTDropInViewController, didSucceedWithTokenization paymentMethodNonce: BTPaymentMethodNonce) {
//
//    }
//
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return paymentsDetail.count + 1
//    }
//
//    func numberOfSections(in tableView: UITableView) -> Int {
//        return 1
//    }
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        if indexPath.row > paymentsDetail.count {
//            let cell = UITableViewCell()
//            cell.textLabel?.text = "ADD NEW PAYMENT METHOD"
//            cell.textLabel?.fontColorTool()
//            return cell
//        }else{
//            if let cell = tableView.dequeueReusableCell(withIdentifier: "mageWooProductCell1") as? cedMagepaymentmethodCell {
//                if let imgUrl = paymentsDetail[indexPath.row]["imgUrl"] {
////                    cell.paymentImage.sd_setImage(with: URL(string: imgUrl), placeholderImage: UIImage(named: "placeholder"))
//
//                     cell.paymentImage.sd_setImage(with: URL(string: imgUrl), placeholderImage: nil)
//                }
//                cell.title.text = paymentsDetail[indexPath.row]["maskedNumber"]
//                cell.title.fontColorTool()
//                return cell
//            }
//        }
//        return UITableViewCell()
//    }
//
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        if indexPath.row < previousMethods.count {
//         let paymethod = previousMethods[indexPath.row]
//            if let token = paymethod["token"] {
//                self.postNonceToServer(paymentMethodNonce: "",user:true,token:token)
//            }
//        }else{
//            self.showDropIn(clientTokenOrTokenizationKey: clientToken)
//        }
//    }
//
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 45
//    }
//
//    override func failedWithError(data:Data?,requestUrl:String?,response:URLResponse?){
//        if requestUrl == finalOrderCheck{
//            self.afterPayment(payment_id: "", failure: "true")
//        }else if requestUrl == clientTokenUrl {
//            self.showDropIn(clientTokenOrTokenizationKey: clientToken)
//        }
//    }


}
