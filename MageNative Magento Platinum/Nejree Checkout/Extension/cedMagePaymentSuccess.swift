//
//  cedMagePaymentSuccess.swift
//  MageNative Magento Platinum
//
//  Created by Manohar Singh Rawat on 20/08/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class cedMagePaymentSuccess: cedMageViewController {

    @IBOutlet weak var orderNumberLabel: UILabel!
    
    @IBOutlet weak var orderDateLabel: UILabel!
    
    @IBOutlet weak var trackOrderButton: UIButton!
    
    @IBOutlet weak var continueButton: UIButton!
    
    
    @IBOutlet weak var paymentSuccessLabel: UILabel!
    
    @IBOutlet weak var congratsLabel: UILabel!
    
    
    var order_id = ""
    var order_date = ""
    var mainvc: UIViewController?;
    var orderProduct = [orderItems]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        orderNumberLabel.text = APP_LBL().order_number + " " + order_id;
        orderDateLabel.text = APP_LBL().order_date + " " + order_date//"\(Date())"
        self.navigationItem.hidesBackButton = true;
        self.navigationItem.title = "";
        continueButton.addTarget(self, action: #selector(continueButtonClicked(_:)), for: .touchUpInside);
        trackOrderButton.addTarget(self, action: #selector(trackOrderTapped(_sender:)), for: .touchUpInside)
        
        paymentSuccessLabel.text = APP_LBL().order_received.uppercased()
        congratsLabel.text = APP_LBL().congratulations_your_order_is_placed.uppercased()
        
        
        continueButton.setTitle(APP_LBL().continue_shopping.uppercased(), for: .normal)
        trackOrderButton.setTitle(APP_LBL().track_your_order.uppercased(), for: .normal)
        
     //   continueButton.roundCorners()
        continueButton.setBorder()
        
    //    trackOrderButton.roundCorners()
        trackOrderButton.setBorder()
        
        APP_DEL.isComesFromOrder = true
        
        let defaults = UserDefaults.standard
        defaults.set(minimumReviewWorthyActionCount, forKey: "appOpenCount")
             
        
        // Do any additional setup after loading the view.
    }
    
    @objc func continueButtonClicked(_ sender: UIButton)
    {
    
        APP_DEL.isComesFromOrder = false
        
         (UIApplication.shared.delegate as! AppDelegate).changeLanguage()
//        self.navigationController?.popToRootViewController(animated: true)
//        self.navigationController?.dismiss(animated: true, completion: {
//            if let viewController = UIStoryboard(name: "homeLayouts", bundle: nil).instantiateViewController(withIdentifier: "nejreeHomeController") as? nejreeHomeController {
//                cedMageViewController().setCartCount(view: viewController, items: "0")
//                self.defaults.set("0", forKey: "items_count")
//                //    self.tabBarController?.selectedViewController = viewController
//                self.tabBarController?.selectedIndex = 0
//            }
//        })

    }
    
    @objc func trackOrderTapped(_sender: UIButton) {
        let vc = UIStoryboard(name: "cedMageAccounts", bundle: nil).instantiateViewController(withIdentifier: "nejreeTrackController") as! nejreeTrackController
        vc.orderDate = self.order_date
        vc.orderNumber = self.order_id
        vc.orderProduct = self.orderProduct
        vc.status = "0"
        self.navigationController?.pushViewController(vc, animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
