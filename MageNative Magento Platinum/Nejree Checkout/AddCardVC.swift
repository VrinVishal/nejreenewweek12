//
//  AddCardVC.swift
//  CleanITCustomer
//
//  Created by vishal.n on 07/11/19.
//  Copyright © 2019 vishal.n. All rights reserved.
//

import UIKit
import Foundation
import FirebaseAnalytics
import WebKit

//import Stripe
//import ACFloatingTextfield_Swift

protocol PaymentFail {
    func PaymentFailForCheckout(index: Int)
}


class AddCardVC: MagenativeUIViewController, WKNavigationDelegate {

//    @IBOutlet weak var txtCreditCardNumber: SkyFloatingLabelTextField!
//    @IBOutlet weak var txtMonthYear: SkyFloatingLabelTextField!
//    @IBOutlet weak var txtCvv: SkyFloatingLabelTextField!
    
     var deleagteCheckoutFail: PaymentFail?
    
    
    @IBOutlet weak var btnSave: UIButton!
    
    @IBOutlet weak var viewName: UIView!
    @IBOutlet weak var txtName: SkyFloatingLabelTextField!
    
    @IBOutlet weak var viewCreditCardNumber: UIView!
    @IBOutlet weak var txtCreditCardNumber: SkyFloatingLabelTextField!
    
    @IBOutlet weak var viewMonthYear: UIView!
    @IBOutlet weak var txtMonthYear: SkyFloatingLabelTextField!
    
    @IBOutlet weak var viewCvv: UIView!
    @IBOutlet weak var lblCardDetails: UILabel!
    @IBOutlet weak var lblRequiredFields: UILabel!
    
    
    @IBOutlet weak var validationimageName: UIImageView!
    @IBOutlet weak var validationimageCardNumber: UIImageView!
    @IBOutlet weak var validationimageExpiry: UIImageView!
    @IBOutlet weak var validationimageCVVNumber: UIImageView!
    
    @IBOutlet weak var sideViewName: UIView!
    @IBOutlet weak var sideViewCardNumber: UIView!
    @IBOutlet weak var sideViewExpiry: UIView!
    @IBOutlet weak var sideViewCVV: UIView!
    
    @IBOutlet weak var txtCvv: SkyFloatingLabelTextField!
    
    @IBOutlet weak var WebPayment: WKWebView!
    
    var isBackFromSuccess : Bool = false
    

    var strGrandTotal = ""
    var strOrderId = ""
    var strOrderDate = ""
    
    var strPublicKey = ""
    var strSecretKey = ""
    var strCurrentCheckoutMode = ""
    var strCheckoutBaseURL = ""
    
    var PaymentSuccessURL = ""
    var PaymentFailURL = ""
    
    var productsData = [CartProduct]()

    var OrderTotal : Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationBar()
        
//        viewName.layer.cornerRadius = 5.0
//        viewCvv.layer.cornerRadius = 5.0
//        viewCreditCardNumber.layer.cornerRadius = 5.0
//        viewMonthYear.layer.cornerRadius = 5.0

         WebPayment.navigationDelegate = self
        print(strGrandTotal)
        
        
       // OrderTotal = (Int(round(Double(strGrandTotal) ?? 0.0)) * 100)

        OrderTotal = (Int((Double(strGrandTotal) ?? 0.0) * 100))
        print(OrderTotal)
//        let roundPrice = (Int(round(Double("552.58") ?? 0.0)) * 100)
//        print(roundPrice) = 55300
        
        
        setData()
    }
    
    
    override func viewDidDisappear(_ animated: Bool) {
        
        if self.isBackFromSuccess == false
        {
         
            self.deleagteCheckoutFail?.PaymentFailForCheckout(index: 0)
        }
        
         
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    

    @IBAction func actionBtnSave(_ sender: UIButton) {
        let title = APP_LBL().error
        var msg = "Email Or Password is Empty."
        if !txtName.hasText {
            msg = APP_LBL().enter_card_holder_name
            cedMageHttpException.showAlertView(me: self, msg: msg, title: title)

        } else if (!txtCreditCardNumber.hasText || (txtCreditCardNumber.text?.count != 19)){

            msg = APP_LBL().enter_valid_card_number
            cedMageHttpException.showAlertView(me: self, msg: msg, title: title)

        } else if (!txtMonthYear.hasText || (txtMonthYear.text!.components(separatedBy: "/").count != 2)) {

            msg = APP_LBL().enter_expiry_date
            cedMageHttpException.showAlertView(me: self, msg: msg, title: title)

       }  else if !txtCvv.hasText {

            msg = APP_LBL().enter_cvv_number
            cedMageHttpException.showAlertView(me: self, msg: msg, title: title)

       } else {
            
            txtName.resignFirstResponder()
            txtCreditCardNumber.resignFirstResponder()
            txtMonthYear.resignFirstResponder()
            txtCvv.resignFirstResponder()
            
            self.getTokenForPayment()
            
        }
    }
    
    func webLoad(_ strUrl: String) {
//        Utility.StartLoder()
        let urlload = strUrl
        let url = URL(string: urlload)
        DispatchQueue.main.async {
            
           // self.WebPayment.delegate = self
           
            self.WebPayment.load(URLRequest(url: url!))
        }
                
    }
    
    func getTokenForPayment() {
        cedMageLoaders.addDefaultLoader(me: self);
        let semaphore = DispatchSemaphore (value: 0)
        
        let strMonthYear = txtMonthYear.text!
        let month = strMonthYear.components(separatedBy: "/").first!
        let year = strMonthYear.components(separatedBy: "/").last!
        var cardNumber = txtCreditCardNumber.text!
        cardNumber = cardNumber.replacingOccurrences(of: " ", with: "")
        let cvvNumber = txtCvv.text!
        
         let cardHolderName = txtName.text!
        
        let finalCardNumber = cardNumber.getArToEnDigit()
        let finalMonth = month.getArToEnDigit()
        let finalYear = year.getArToEnDigit()
        let finalCvvNumber = cvvNumber.getArToEnDigit()
        
        
        //let parameters = "{\n \"type\": \"card\", \n \"name\": \"\(cardHolderName)\", \n\"number\":\"\(cardNumber)\",\n\"expiry_month\":\"\(month)\",\n\"expiry_year\":\"\(year)\",\n\"cvv\":\"\(cvvNumber)\"\n}"
        let parameters = "{\n \"type\": \"card\", \n \"name\": \"\(cardHolderName)\", \n\"number\":\"\(finalCardNumber)\",\n\"expiry_month\":\"\(finalMonth)\",\n\"expiry_year\":\"\(finalYear)\",\n\"cvv\":\"\(finalCvvNumber)\"\n}"
        
        print(parameters)
        
        let postData = parameters.data(using: .utf8)

        
        if strCurrentCheckoutMode == "1" {
            //Sandbox Mode
            strCheckoutBaseURL = "https://api.sandbox.checkout.com/tokens"
        } else {
            //Live Mode
            strCheckoutBaseURL = "https://api.checkout.com/tokens"
        }
        
        
        
        var request = URLRequest(url: URL(string: strCheckoutBaseURL)!,timeoutInterval: Double.infinity)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue(strPublicKey, forHTTPHeaderField: "Authorization")

        request.httpMethod = "POST"
        request.httpBody = postData

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
          guard let data = data else {
            print(String(describing: error))
            cedMageLoaders.removeLoadingIndicator(me: self);

            return
          }
          print(String(data: data, encoding: .utf8)!)
            let strResponse = String(data: data, encoding: .utf8)!
            let strPostData = strResponse.data(using: .utf8)

            
            
            
               var token = ""
            //            do {
            let json = try? JSONSerialization.jsonObject(with: strPostData!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary

            
            print(json)
            
            if (json?["token"] != nil){
                       // Parse JSON data
                       token = json?["token"] as! String
                print(token)
                DispatchQueue.main.async {

                    self.getWebviewUrl(json?["token"] as! String)
                }
            } else {
                DispatchQueue.main.async {
                   //  cedMageLoaders.removeLoadingIndicator(me: self);
                  //  cedMageHttpException.showAlertView(me: self, msg: "Something went wrong".localized, title: "Error".localized)
                    
                    self.FailedPayment()
                }
            }

            
          semaphore.signal()
        }

        task.resume()
        semaphore.wait()

    }
    
    func getWebviewUrl(_ strToken : String) {
        cedMageLoaders.addDefaultLoader(me: self);

         let cardHolderName = txtName.text!
        let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
        
        
         let parameters = "{\n  \"source\": {\n    \"type\": \"token\",\n    \"token\": \"\(strToken)\" \n  },\n  \"3ds\": {\n    \"enabled\": true\n  } , \n  \"customer\": {\n\"email\": \"\(userInfoDict["email"]!)\",\n\"name\": \"\(cardHolderName)\" \n},\n  \"amount\": \(OrderTotal ?? 0), \n  \"email\": \"\(userInfoDict["email"]!)\",\n  \"currency\": \"\(APP_DEL.selectedCountry.currency_code_en!)\",\n  \"reference\": \"\(strOrderId)\",\n\"success_url\": \"\(PaymentSuccessURL)\", \n\"failure_url\": \"\(PaymentFailURL)\",\n}"
        
        
         print(parameters)
        
        let postData = parameters.data(using: .utf8)
        print("parameters = \(parameters)")
        
        var PaymentAPIURL = ""
        if strCurrentCheckoutMode == "1" {
            //Sandbox Mode
            PaymentAPIURL = "https://api.sandbox.checkout.com/payments"
        }
        else
        {
            //Live Mode
            PaymentAPIURL = "https://api.checkout.com/payments"
        }
        
        
        var request = URLRequest(url: URL(string: PaymentAPIURL)!,timeoutInterval: Double.infinity)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue(strSecretKey, forHTTPHeaderField: "Authorization")
    //    request.addValue("text/plain", forHTTPHeaderField: "Content-Type")

        request.httpMethod = "POST"
        request.httpBody = postData

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            
          guard let data = data else {
            print(String(describing: error))
            cedMageLoaders.removeLoadingIndicator(me: self);

            return
          }
          print(String(data: data, encoding: .utf8)!)
        //  semaphore.signal()
//            let strResponse = String(data: data, encoding: .utf8)!
//            let strPostData = strResponse.data(using: .utf8)
//            let json = try? JSONSerialization.jsonObject(with: strPostData!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
//
//            let redirectionUrl = (((json?["_links"] as! [String : Any])["redirect"] as! [String : Any])["href"] as! String)
//            print("Redirection URl = \(redirectionUrl)")
//            self.webLoad(redirectionUrl)
            
            let strResponse = String(data: data, encoding: .utf8)!
            let strPostData = strResponse.data(using: .utf8)

            var isApproved = false
//            do {
            let json = try? JSONSerialization.jsonObject(with: strPostData!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary

                   // Parse JSON data
            if (json?["customer"] != nil){
                let strResponse = String(data: data, encoding: .utf8)!
                let strPostData = strResponse.data(using: .utf8)
                let json = try? JSONSerialization.jsonObject(with: strPostData!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                
                let redirectionUrl = (((json?["_links"] as! [String : Any])["redirect"] as? [String : Any])?["href"] as? String) ?? ""
                
                if redirectionUrl == ""{
                    
                    self.FailedPayment()
                    return
                    
                }
                
                print("Redirection URl = \(redirectionUrl)")
                self.webLoad(redirectionUrl)
            } else {
                
                 self.FailedPayment()
                
            }
            
        }

        task.resume()
      //  semaphore.wait()

    }
    
    override func recieveResponse(data: Data?, requestUrl: String?, response: URLResponse?) {
           guard let data = data else {return}
           do {
               
               let jsonTemp = try JSON(data: data)
               var json = jsonTemp[0]
               
               print(json)
               

            
            print("\(self.productsData)")

                    var arrTempBlocks : [ItemBlock] = []
                    for bl in self.productsData {
                        arrTempBlocks.append(ItemBlock(productId: bl.product_id, productName: bl.product_name, productPrice: bl.sub_total, productQuantity: "\(bl.quantity ?? 0)"))
                    }
                    let finalRes = self.getItemBlock(itemBlocks: arrTempBlocks)

                    print(finalRes.jsonString)
                    print(finalRes.json)


                    let strCoupon : String = self.defaults.value(forKey: "appliedCoupon") as? String ?? ""

                    Analytics.logEvent(AnalyticsEventPurchase, parameters: [
                       AnalyticsParameterValue : self.strGrandTotal,
                        AnalyticsParameterCurrency : APP_DEL.selectedCountry.currency_code_en!,
                        AnalyticsParameterCoupon : strCoupon,
                        AnalyticsParameterTax : "",
                        AnalyticsParameterItemID : finalRes.jsonString,
                        AnalyticsParameterTransactionID : self.strOrderId as Any,

                    ])

                    self.defaults.removeObject(forKey: "guestEmail");
                    self.defaults.removeObject(forKey: "cartId");
                    self.defaults.removeObject(forKey: "appliedCoupon");
                    self.defaults.setValue("0", forKey: "items_count")
                    //self.view.makeToast("Order Placed Successfully.".localized, duration: 2, position: .center, title: nil, image: nil, style: nil, completion: {
                      //  Void i
            
                    APP_DEL.isComesFromOrder = true

                        let storyboard = UIStoryboard(name: "Main", bundle: nil);
                        let viewController = storyboard.instantiateViewController(withIdentifier: "cedMagePaymentSuccess") as! cedMagePaymentSuccess;
                       viewController.order_id = self.strOrderId
                        viewController.order_date = strOrderDate
                        self.navigationController?.viewControllers = [viewController];
               
               
               
              
                           
           } catch {print("error occured in catch section")}
       }
}
extension AddCardVC: UITextFieldDelegate{
    

       func textFieldDidEndEditing(_ textField: UITextField) {

           let str = textField.text!

           switch textField {

           case txtName:

            validationimageName.isHidden = false

               print(str)
               if str != "" {
                   let decimalCharacters = CharacterSet.decimalDigits

                   let decimalRange = str.rangeOfCharacter(from: decimalCharacters)
                
                   if decimalRange != nil {
                       validationimageName.image = UIImage(named: "wrongname")
                       viewName.layer.borderWidth = 1
                       viewName.layer.borderColor = UIColor.init(hexString: "#741A31")!.cgColor
                       txtName.titleColor = UIColor.init(hexString: "#741A31")!
                       sideViewName.backgroundColor = UIColor.black

                   }else {
                       validationimageName.image = UIImage(named: "verified")
                       txtName.titleColor = .white
                       sideViewName.backgroundColor = UIColor.init(hexString: "#fcb215")
                       sideViewName.isHidden = false
                       viewName.layer.borderColor = nejreeColor?.cgColor
                   }
               } else {
                   validationimageName.image = UIImage(named: "wrongname")
                   viewName.layer.borderWidth = 1
                   viewName.layer.borderColor = UIColor.init(hexString: "#741A31")!.cgColor
                   txtName.titleColor = UIColor.init(hexString: "#741A31")!
                   sideViewName.backgroundColor = UIColor.black
                   return
               }
            
            
            
            
            case txtCreditCardNumber:

            validationimageCardNumber.isHidden = false

            print(str.count)
               if str != "" {
                
                
                let newLength = str.count
                
                   if newLength < 19 {
                       validationimageCardNumber.image = UIImage(named: "wrongname")
                       viewCreditCardNumber.layer.borderWidth = 1
                       viewCreditCardNumber.layer.borderColor = UIColor.init(hexString: "#741A31")!.cgColor
                       txtCreditCardNumber.titleColor = UIColor.init(hexString: "#741A31")!
                       sideViewCardNumber.backgroundColor = UIColor.black

                   }
                   else {
                       validationimageCardNumber.image = UIImage(named: "verified")
                       txtCreditCardNumber.titleColor = .white
                       sideViewCardNumber.backgroundColor = UIColor.init(hexString: "#fcb215")
                       sideViewCardNumber.isHidden = false
                       viewCreditCardNumber.layer.borderColor = nejreeColor?.cgColor
                   }
               } else {
                   validationimageCardNumber.image = UIImage(named: "wrongname")
                   viewCreditCardNumber.layer.borderWidth = 1
                   viewCreditCardNumber.layer.borderColor = UIColor.init(hexString: "#741A31")!.cgColor
                   txtCreditCardNumber.titleColor = UIColor.init(hexString: "#741A31")!
                   sideViewCardNumber.backgroundColor = UIColor.black
                   return
               }
            
            
            case txtMonthYear:

                    validationimageExpiry.isHidden = false

                    print(str.count)
                  if str != "" {
                      let newLength = str.count
                   
                       if newLength < 5 {
                          validationimageExpiry.image = UIImage(named: "wrongname")
                          viewMonthYear.layer.borderWidth = 1
                          viewMonthYear.layer.borderColor = UIColor.init(hexString: "#741A31")!.cgColor
                          txtMonthYear.titleColor = UIColor.init(hexString: "#741A31")!
                          sideViewExpiry.backgroundColor = UIColor.black

                      }else {
                          validationimageExpiry.image = UIImage(named: "verified")
                          txtMonthYear.titleColor = .white
                          sideViewExpiry.backgroundColor = UIColor.init(hexString: "#fcb215")
                          sideViewExpiry.isHidden = false
                          viewMonthYear.layer.borderColor = nejreeColor?.cgColor
                      }
                  } else {
                      validationimageExpiry.image = UIImage(named: "wrongname")
                      viewMonthYear.layer.borderWidth = 1
                      viewMonthYear.layer.borderColor = UIColor.init(hexString: "#741A31")!.cgColor
                      txtMonthYear.titleColor = UIColor.init(hexString: "#741A31")!
                      sideViewExpiry.backgroundColor = UIColor.black
                      return
                  }
            
            
            case txtCvv:

              validationimageCVVNumber.isHidden = false

            print(str)
            if str != "" {
                
                let newLength = str.count
            
                if newLength < 3 {
                    validationimageCVVNumber.image = UIImage(named: "wrongname")
                    viewCvv.layer.borderWidth = 1
                    viewCvv.layer.borderColor = UIColor.init(hexString: "#741A31")!.cgColor
                    txtCvv.titleColor = UIColor.init(hexString: "#741A31")!
                    sideViewCVV.backgroundColor = UIColor.black

                }else {
                    validationimageCVVNumber.image = UIImage(named: "verified")
                    txtCvv.titleColor = .white
                    sideViewCVV.backgroundColor = UIColor.init(hexString: "#fcb215")
                    sideViewCVV.isHidden = false
                    viewCvv.layer.borderColor = nejreeColor?.cgColor
                }
            } else {
                validationimageCVVNumber.image = UIImage(named: "wrongname")
                viewCvv.layer.borderWidth = 1
                viewCvv.layer.borderColor = UIColor.init(hexString: "#741A31")!.cgColor
                txtCvv.titleColor = UIColor.init(hexString: "#741A31")!
                sideViewCVV.backgroundColor = UIColor.black
                return
            }




           default:
               print("hellodefault")
           }


            self.checkAllFieldsAreFilledWithvalidation()
        
        
       }
    
    
    @IBAction func textFieldValueDidChanged (_ textField: UITextField){
      checkAllFieldsAreFilledWithvalidation()
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        checkAllFieldsAreFilledWithvalidation()

        if (textField.tag == 1002){
            //textfield credit card
            let newLength = (textField.text ?? "").count + string.count - range.length
                if(textField == txtCreditCardNumber) {
                    if (newLength > 19){
                        txtMonthYear.becomeFirstResponder()
                        txtCreditCardNumber.resignFirstResponder()
                        return newLength <= 19
                    }else {
                        return newLength <= 19
                    }
                  
                }
                return true
            
            
        }
        
        else if (textField.tag == 3390){
            if let x = string.rangeOfCharacter(from: NSCharacterSet.decimalDigits) {
                         return false
              } else {
              return true
              }
            
        }
        
        
            else if (textField.tag == 1001){
                //textfield month/year
                     let currentText = textField.text! as NSString
                textField.textColor = UIColor.black

                    let updatedText = currentText.replacingCharacters(in: range, with: string)

                    if string == "" {

                        if textField.text?.count == 3
                        {
                            textField.text = "\(updatedText.prefix(1))"
                            return false
                        }
                        if (updatedText == ""){
                            textField.text = ""
                            txtMonthYear.resignFirstResponder()
                            txtCreditCardNumber.becomeFirstResponder()
                             return true
                        }

                    return true
                    }

                    if updatedText.count == 5
                    {
                        expDateValidation(dateStr:updatedText)
                        return updatedText.count <= 5
                    } else if updatedText.count > 5
                    {
                        txtMonthYear.resignFirstResponder()
                        txtCvv.becomeFirstResponder()
                        return updatedText.count <= 5
                    } else if updatedText.count == 1{
                        if updatedText > "1"{
                            return updatedText.count < 1
                        }
                    }  else if updatedText.count == 2{   //Prevent user to not enter month more than 12
                        if updatedText > "12"{
                            return updatedText.count < 2
                        }
                    }

                    textField.text = updatedText


                if updatedText.count == 2 {

                       textField.text = "\(updatedText)/"   //This will add "/" when user enters 2nd digit of month
                }

                   return false
                
            }
            
//        else if (textField.tag == 1001){
//            //textfield month/year
//
//
//           let NewStringEnglish = textField.text!.getArToEnDigit()
//
//            textField.textColor = UIColor.black
//            let currentText = textField.text! as NSString
//          //  let updatedText = currentText.replacingCharacters(in: range, with: string)
//
//                if string == "" {
//
////                    if textField.text?.count == 3
////                    {
////                       // textField.text = "\(updatedText.prefix(1))"
////                        return false
////                    }
////                    if (updatedText == ""){
////                        textField.text = ""
////                        txtMonthYear.resignFirstResponder()
////                        txtCreditCardNumber.becomeFirstResponder()
////                         return true
////                    }
//
//                return true
//                }
//
//                if textField.text!.count == 4
//                {
//                    expDateValidation(dateStr:NewStringEnglish)
//                    return textField.text!.count <= 5
//                }
//                else if textField.text!.count > 4
//                {
//                    txtMonthYear.resignFirstResponder()
//                    txtCvv.becomeFirstResponder()
//                    return textField.text!.count <= 5
//                }
//
////                else if updatedText.count == 1{
////                    if updatedText > "1"{
////                        return updatedText.count < 1
////                    }
////                }  else if updatedText.count == 2{   //Prevent user to not enter month more than 12
////                    if updatedText > "12"{
////                        return updatedText.count < 2
////                    }
////                }
//
//               // textField.text = updatedText
//
//
//            if textField.text!.count == 2 {
//
//                textField.text = "\(textField.text ?? "")/"   //This will add "/" when user enters 2nd digit of month
//            }
//
//               return true
//
//        }
        else if (textField.tag == 1000){
            //textfield cvv561

            var totalString = String(format: "%@%@", textField.text!,string)
            if (range.length == 1){
                textField.text = formatPhoneNumber(totalString, deleteLastChar: true)
            } else  {
                 textField.text = formatPhoneNumber(totalString, deleteLastChar: false)
            }
            return false;
        }
        return true;
    }
    
    func checkAllFieldsAreFilledWithvalidation(){
        
        if (!txtName.hasText || (!txtCreditCardNumber.hasText) || (txtCreditCardNumber.text?.count != 19) || (txtMonthYear.text?.count != 5) || (txtCvv.text?.count != 3) || !txtCreditCardNumber.hasText || !txtCvv.hasText){
            
            btnSave.setTitleColor(.lightGray, for: .normal)
            btnSave.isUserInteractionEnabled = false
         } else {
            btnSave.setTitleColor(.white, for: .normal)
            btnSave.isUserInteractionEnabled = true
        }
        
    }
    
    func expDateValidation(dateStr:String) {

        let currentYear = Calendar.current.component(.year, from: Date()) % 100   // This will give you current year (i.e. if 2019 then it will be 19)
        let currentMonth = Calendar.current.component(.month, from: Date()) // This will give you current month (i.e if June then it will be 6)
        let enterdYr = Int(dateStr.suffix(2)) ?? 0   // get last two digit from entered string as year
        let enterdMonth = Int(dateStr.prefix(2)) ?? 0  // get first two digit from entered string as month
        print(dateStr) // This is MM/YY Entered by user

        if enterdYr > currentYear
        {
            if (1 ... 12).contains(enterdMonth){
                print("Entered Date Is Right")
                txtMonthYear.textColor = UIColor.black

            } else
            {
                print("Entered Date Is Wrong")
                txtMonthYear.textColor = UIColor.red

            }

        } else  if currentYear == enterdYr {
            if enterdMonth >= currentMonth
            {
                if (1 ... 12).contains(enterdMonth) {
                   print("Entered Date Is Right")
                    txtMonthYear.textColor = UIColor.black

                }  else
                {
                   print("Entered Date Is Wrong")
                    txtMonthYear.textColor = UIColor.red

                }
            } else {
                print("Entered Date Is Wrong")
                txtMonthYear.textColor = UIColor.red

            }
        } else
        {
           print("Entered Date Is Wrong")
            txtMonthYear.textColor = UIColor.red

        }

    }
    
    func formatPhoneNumber(_ simpleNumber: String?, deleteLastChar: Bool) -> String? {
        var simpleNumber = simpleNumber
        print(simpleNumber)
        if (simpleNumber?.count == 0){
            return ""
        }
        var error: Error? = nil
        var regex: NSRegularExpression? = nil
        do {
            regex = try NSRegularExpression(pattern: "[\\s-\\(\\)]", options: .caseInsensitive)
        } catch {
        }
        simpleNumber = regex?.stringByReplacingMatches(in: simpleNumber!, options: [], range: NSRange(location: 0, length: simpleNumber!.count), withTemplate: "")
         if (txtCvv.isFirstResponder) {
            if(simpleNumber!.count > 3) {
                // remove last extra chars.
                simpleNumber = (simpleNumber! as NSString).substring(to: 3)
            }
            if (deleteLastChar){
                // should we delete the last digit?
                simpleNumber = (simpleNumber! as NSString).substring(to: (simpleNumber!.count)-1)
                if (simpleNumber == ""){
                    txtCvv.resignFirstResponder()
                    txtMonthYear.becomeFirstResponder()
                }
            }
            simpleNumber = simpleNumber?.replacingOccurrences(of: "((\\d{5})", with: "$1", options: .regularExpression, range: Range<String.Index>(NSRange(location: 0, length: simpleNumber!.count), in: simpleNumber!))
        }

        return simpleNumber;
    }
    @objc func didChangeText(textField:UITextField) {
        txtCreditCardNumber.text = self.modifyCreditCardString(creditCardString: textField.text!)
        
        checkAllFieldsAreFilledWithvalidation()
    }
    
    func modifyCreditCardString(creditCardString : String) -> String {
        let trimmedString = creditCardString.components(separatedBy: .whitespaces).joined()

        let arrOfCharacters = Array(trimmedString)
        var modifiedCreditCardString = ""

        if(arrOfCharacters.count > 0) {
            for i in 0...arrOfCharacters.count-1 {
                modifiedCreditCardString.append(arrOfCharacters[i])
                if((i+1) % 4 == 0 && i+1 != arrOfCharacters.count){
                    modifiedCreditCardString.append(" ")
                }
            }
        }
        return modifiedCreditCardString
    }
}

extension AddCardVC {
    
    func setNavigationBar(){
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = UIColor.black
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
        self.navigationItem.hidesBackButton = true
        if APP_DEL.selectedLanguage == Arabic{
           navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:"back-Right"), style: .plain, target: self, action: #selector(actionBtnBack(_:)))
       } else {
           navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:"backArrow"), style: .plain, target: self, action: #selector(actionBtnBack(_:)))
       }
        
        
        
        navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        
    }
    
    //MARK:- Action Btn Menu
    @IBAction func actionBtnBack(_ sender: UIButton){
       // self.navigationController?.popViewController(animated: true)

        
        self.deleagteCheckoutFail?.PaymentFailForCheckout(index: 0)
//
//        if isModal() {
//            self.dismiss(animated: true, completion: nil)
//        } else {
            self.navigationController?.popViewController(animated: true)
        //}
        
        
    }
    func getCornerRound(sender:UIButton){
          sender.layer.borderWidth = 1
          sender.layer.borderColor = UIColor.gray.cgColor
      }
    
    func setData(){
        btnSave.setTitleColor(.lightGray, for: .normal)
        btnSave.isUserInteractionEnabled = false
        
//        viewName.roundCorners()
//        viewMonthYear.roundCorners()
//        viewCvv.roundCorners()
//        viewCreditCardNumber.roundCorners()
        
        viewName.setBorder()
        viewMonthYear.setBorder()
        viewCvv.setBorder()
        viewCreditCardNumber.setBorder()
        
       // btnSave.roundCorners()
        
        btnSave.backgroundColor = .black
//        btnSave.roundCorners()
//        btnSave.setBorder()
        //        btnSave.layer.cornerRadius = btnSave.frame.size.height/2
        //        btnSave.clipsToBounds = true
       // getCornerRound(sender: btnSave)
            
        
//        txtCreditCardNumber.placeholder = ConstantText.credit_card_number
//        txtCvv.placeholder = ConstantText.cvv
//        txtMonthYear.placeholder = ConstantText.mm_yy
//        txtCreditCardNumber.font = Utility.SourceSansProRegular(Utility.TextField_Font_size()+3)
//        txtCvv.font = Utility.SourceSansProRegular(Utility.TextField_Font_size()+3)
//        txtMonthYear.font = Utility.SourceSansProRegular(Utility.TextField_Font_size()+3)
//        btnSave.titleLabel?.font = Utility.SourceSansProSemibold(Utility.TextField_Font_size()+5)

        txtCreditCardNumber.delegate = self;
        txtCvv.delegate = self;
        txtMonthYear.delegate = self;
        self.txtCreditCardNumber.addTarget(self, action: #selector(didChangeText(textField:)), for: .editingChanged)
        
        self.txtName.addTarget(self, action: #selector(textFieldValueDidChanged(_:)), for: .editingChanged)

        self.txtMonthYear.addTarget(self, action: #selector(textFieldValueDidChanged(_:)), for: .editingChanged)

//        self.txtCreditCardNumber.addTarget(self, action: #selector(textFieldValueDidChanged(_:)), for: .editingChanged)

        self.txtCvv.addTarget(self, action: #selector(textFieldValueDidChanged(_:)), for: .editingChanged)

        
        txtName.placeholder = APP_LBL().cardholder_name
        txtCreditCardNumber.placeholder = APP_LBL().card_number
        txtMonthYear.placeholder = APP_LBL().expiry_date
        txtCvv.placeholder = APP_LBL().cvv_star
        
        lblCardDetails.text = APP_LBL().card_details
        lblRequiredFields.text = APP_LBL().star_required_fields
        
        if APP_DEL.selectedLanguage == Arabic{
            txtName.textAlignment = .right
            txtCreditCardNumber.textAlignment = .right
            txtMonthYear.textAlignment = .right
            txtCvv.textAlignment = .right
        } else {
            txtName.textAlignment = .left
            txtCreditCardNumber.textAlignment = .left
            txtMonthYear.textAlignment = .left
            txtCvv.textAlignment = .left
        }
        
        
        
//        txtCreditCardNumber.text = "4242 4242 4242 4242"
//        txtCvv.text = "100"
//        txtName.text = "Ashish"
//        txtMonthYear.text = "06/20"
        
        txtMonthYear.keyboardType = .asciiCapableNumberPad
       // txtName.keyboardType = .asciiCapable
        txtCreditCardNumber.keyboardType = .asciiCapableNumberPad
        txtCvv.keyboardType = .asciiCapable
        
        btnSave.setTitle(APP_LBL().pay_now, for: .normal)

    }
    
}

//MARK:- UIWebViewDelegate
extension AddCardVC:UIWebViewDelegate{
    
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
             self.WebPayment.isHidden = false

       }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
           cedMageLoaders.removeLoadingIndicator(me: self);
       }
    
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
           cedMageLoaders.removeLoadingIndicator(me: self);
       }
    
//    func webViewDidStartLoad(_ webView: UIWebView) {
////        Utility.StartLoder()
////        cedMageLoaders.addDefaultLoader(me: self);
//        self.WebPayment.isHidden = false
//
//    }
    
//    func webView(_ webView: UIWebView, didFailLoadWithError error: Error){
//        cedMageLoaders.removeLoadingIndicator(me: self);
//
//    }
    
    
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
              
              
             if navigationAction.request != nil{

                      DispatchQueue.main.async {

                            if ((navigationAction.request.url?.absoluteString.range(of: self.PaymentSuccessURL)) != nil) {
                                self.WebPayment.isHidden = false
                                self.successPayment()
                            } else if ((navigationAction.request.url?.absoluteString.range(of: self.PaymentFailURL)) != nil) {
                                self.WebPayment.isHidden = false
                                self.FailedPayment()
                                // self.successPayment()
                            }
    
                        }
                      cedMageLoaders.removeLoadingIndicator(me: self);
                      
                  }

 
              decisionHandler(WKNavigationActionPolicy.allow)

          }
    
    
//    func webViewDidFinishLoad(_ webView: UIWebView)
//    {
//        DispatchQueue.main.async {
//
//
//
//            if ((webView.request?.url?.absoluteString.range(of: self.PaymentSuccessURL)) != nil) {
//                self.WebPayment.isHidden = false
//                self.successPayment()
//            } else if ((webView.request?.url?.absoluteString.range(of: self.PaymentFailURL)) != nil) {
//                self.WebPayment.isHidden = false
//
//                self.FailedPayment()
//            }
//
//
//
//        }
//      cedMageLoaders.removeLoadingIndicator(me: self);
//
//    }
//
    
    func successPayment(){
        
        
        self.isBackFromSuccess = true
        self.deleagteCheckoutFail?.PaymentFailForCheckout(index: 1)
        self.navigationController?.popViewController(animated: true)
        
      
        
      
    }
    
    func FailedPayment(){
        
        self.deleagteCheckoutFail?.PaymentFailForCheckout(index: 0)
        self.navigationController?.popViewController(animated: true)
        
    }
    

    func getItemBlock(itemBlocks: [ItemBlock]) -> (json: [[String:String]], jsonString: String) {
        
        do {
            let data = try JSONEncoder().encode(itemBlocks)
            let json : [[String : String]] = try JSONSerialization.jsonObject(with: data, options: []) as! [[String : String]]
            return (json: json, jsonString: String(data: data, encoding: .utf8)!)
        } catch {
            print("ERROR.")
            return (json: [], jsonString: "")
        }
    }
}
