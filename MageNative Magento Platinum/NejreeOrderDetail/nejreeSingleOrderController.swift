//
//  nejreeSingleOrderController.swift
//  MageNative Magento Platinum
//
//  Created by MacMini on 22/08/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

struct GiftRedem : Codable {
    
    let order_id : String?
    let product_id : String?
    let child_product_id : String?
    let customer_id : String?
    let name : String?
    let email : String?
    let phone : String?
    let description : String?
    let status : String?
    let redemestatus : String?
    let price : String?
    let redemecode : String?

    enum CodingKeys: String, CodingKey {

        case order_id = "order_id"
        case child_product_id = "child_product_id"
        case product_id = "product_id"
        case customer_id = "customer_id"
        case name = "name"
        case email = "email"
        case phone = "phone"
        case description = "description"
        case status = "status"
        case redemestatus = "redemestatus"
        case price = "price"
        case redemecode = "redemecode"
    }
}

struct singleOrderData {
    var orderdate: String?
    var method_title: String?
    var mobile: String?
    var method_code: String?
    var credit_card_number: String?
    var country: String?
    var orderlabel: String?
    var ship_to: String?
    var tax_amount: String?
    var discount: String?
    var city: String?
    var credit_card_type: String?
    var name_on_card: String?
    var state: String?
    var grandtotal: String?
    var shipping_method: String?
    var pincode: String?
    var street: String?
    var shipping: String?
    var subtotal: String?
    var neighbour_name: String?
    var shipment_number: String?
    var tracking_id: String?
    var tracking_url: String?
    var returned: String?
    var store_credit: String?
}
struct orderItems {
    var product_type: String?
    var product_name: String?
    var product_price: String?
    var product_id: String?
    var rowsubtotal: String?
    var product_qty: String?
    var product_image: String?
    var selected: Bool?
    var itemId: String?
    var optionSize : String?
    var child_product_id: String?
}
struct returnReason {
    var value: String?
    var key: String?
}


enum OrderDetailSequenceType : Int {
    
    case ImageHeader = 0
    case AddressSection = 2
    case ProductListing = 1
    case PaymentSummary = 3
}

var arrGiftRedem : [GiftRedem] = []

class nejreeSingleOrderController: MagenativeUIViewController, UIGestureRecognizerDelegate {

//    @IBOutlet weak var myOrderHeadingLabel: UILabel!
    @IBOutlet weak var orderSingleTable: UITableView!
//    @IBOutlet weak var orderIdLabel: UILabel!
//    @IBOutlet weak var orderDateLabel: UILabel!
    
    
    var orderId: String?
    var number: String?
    var orderData = singleOrderData()
    var orderProducts = [orderItems]()
    var itemsToReturn = [orderItems]()
    var reasons = [returnReason]()
    var rmaStatus = String()
    var productStatus = String()
    var orderStatus = String()
    var isOrderCompleted = String()
    var codFee = String()
    
    var isOrderCancelAvalilable = String()
    
    var isEnableReturnOrder = false
    var isEnableReturnOrderCountry = false
    var isDisplayReturnOrder = false
    var isReturnOrderPeriodElapsed = true
    
    override func viewDidLoad() {
        super.viewDidLoad()

//        myOrderHeadingLabel.text = APP_LBL().my_orders.uppercased()
        self.orderView()
//        orderIdLabel.isHidden = true
//        orderDateLabel.isHidden = true
        orderSingleTable.isHidden = true
        self.navigationItem.title = APP_LBL().orders
        orderSingleTable.allowsMultipleSelection = true
        // Do any additional setup after loading the view.
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        
        if navigationController?.responds(to: #selector(getter: UINavigationController.interactivePopGestureRecognizer)) ?? false {
                   
                   self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
                   self.navigationController?.interactivePopGestureRecognizer?.delegate = self
               }

        
       
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

    }
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
           
           return true
       }
    
    func orderView() {
        
        var postData = [String:String]()
        
        if let userInfoDict = UserDefaults.standard.object(forKey: "userInfoDict") as? [String: String] {
            
            postData["hashkey"] = userInfoDict["hashKey"]
            postData["customer_id"] = userInfoDict["customerId"]
        }
        
        if let storeID =  UserDefaults.standard.value(forKey: "storeId") as? String {
            postData["store_id"] = storeID
        }
        
        postData["order_id"] = orderId!
        postData["country_id"] = APP_DEL.selectedCountry.country_code ?? "SA"
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);
        
        API().callAPI(endPoint: "mobiconnect/customer/orderview", method: .POST, param: postData) { (json_res, err) in
            
          //  DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                if err == nil {
                    
                    let json = json_res[0]
                    
                    if json["data"]["status"].stringValue == "success" {
                        
                        let js = json["data"]["orderview"][0]
                        
                        for reason in json["data"]["resaon_arr"].arrayValue {
                            
                            let temp = returnReason(value: reason["value"].stringValue, key: reason["key"].stringValue)
                            self.reasons.append(temp)
                        }
                        
                        self.orderStatus = js["order_status"].stringValue
                        self.isOrderCompleted = js["is_order_complete"].stringValue

                        if self.orderStatus == "4" {
                            self.orderStatus = "0"
                        }
                        
                        self.isEnableReturnOrder = (is_return_order_allow == "1")
                        
                        if self.orderStatus == "3" && self.isOrderCompleted == "1" {
                            
                            self.isDisplayReturnOrder = true
                            
                            if (js["return_time_elapsed"].stringValue == "1") && (js["is_return_enable_for_country"].stringValue == "1") {
                                
                                self.isEnableReturnOrderCountry = true
                                self.isReturnOrderPeriodElapsed = false
                                
                            } else {

                                self.isEnableReturnOrderCountry = false
                                self.isReturnOrderPeriodElapsed = true
                            }
                            
                        } else {
                            self.isDisplayReturnOrder = false
                        }
                                                
                        self.orderData = singleOrderData.init(orderdate: js["orderdate"].stringValue,
                                                              method_title: js["method_title"].stringValue,
                                                              mobile: js["mobile"].stringValue,
                                                              method_code: js["method_code"].stringValue,
                                                              credit_card_number: js["credit_card_number"].stringValue,
                                                              country: js["country"].stringValue,
                                                              orderlabel: js["orderlabel"].stringValue,
                                                              ship_to: js["ship_to"].stringValue,
                                                              tax_amount: js["tax_amount"].stringValue,
                                                              discount: js["discount"].stringValue,
                                                              city: js["city"].stringValue,
                                                              credit_card_type: js["credit_card_type"].stringValue,
                                                              name_on_card: js["name_on_card"].stringValue,
                                                              state: js["state"].stringValue,
                                                              grandtotal: js["grandtotal"].stringValue,
                                                              shipping_method: js["shipping_method"].stringValue,
                                                              pincode: js["pincode"].stringValue,
                                                              street: js["street"].stringValue,
                                                              shipping: js["shipping"].stringValue,
                                                              subtotal: js["subtotal"].stringValue,
                                                              neighbour_name: js["neighbour_name"].stringValue,
                                                              shipment_number: js["shipment_number"].stringValue,
                                                              tracking_id: js["tracking_id"].stringValue,
                                                              tracking_url: js["tracking_url"].stringValue,
                                                              returned:js["returned"].stringValue,
                                                              store_credit: js["store_credit"].stringValue)
                        
                        self.codFee = js["cod_fee"].stringValue
                        self.rmaStatus = js["rma_status"].stringValue
                        self.number = js["number"].stringValue

                        self.isOrderCancelAvalilable = js["is_cancel"].stringValue
                        
                        
                        for item in json["data"]["orderview"][0]["ordered_items"].arrayValue {
                            
                            let temp = orderItems.init(product_type: item["product_type"].stringValue,
                                                       product_name: item["product_name"].stringValue,
                                                       product_price: item["product_price"].stringValue,
                                                       product_id: item["product_id"].stringValue,
                                                       rowsubtotal: item["rowsubtotal"].stringValue,
                                                       product_qty: item["product_qty"].stringValue,
                                                       product_image: item["product_image"].stringValue,
                                                       selected: false,
                                                       itemId: item["item_id"].stringValue,
                                                       optionSize : item["option"][0]["option_value"].stringValue,
                                                       child_product_id : item["child_product_id"].stringValue
                                                       )
                            
                            self.orderProducts.append(temp)
                        }
                        
                        //number
                        
                        self.orderSingleTable.separatorStyle = .none
                        self.orderSingleTable.delegate = self
                        self.orderSingleTable.dataSource = self
                        self.orderSingleTable.reloadData()
                        self.orderSingleTable.isHidden = false
                        
                        self.getGiftRedeem()
                    }
                }
        //    }
        }
    }
    
    @objc func dismissButtonTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func btnReturnOrderAction(_ sender: UIButton) {
        
        let vc_1 = UIStoryboard(name: "cedMageAccounts", bundle: nil).instantiateViewController(withIdentifier: "NejreeReturnOrderVC") as! NejreeReturnOrderVC
        vc_1.orderId = self.number ?? ""
        self.navigationController?.pushViewController(vc_1, animated: true)
        return;
    }
    
    @objc func btnReturnOrderInfoAction(_ sender: UIButton) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NejreeCmsVC") as! NejreeCmsVC
        vc.contentText = "returns"
        vc.strTitle = APP_LBL().return_policy.uppercased()
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func getGiftRedeem() {

        
        let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
        
        var postData = [String:String]()
        postData["order_id"] = number ?? ""
        postData["customer_id"] = userInfoDict["customerId"]!;

        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);
        
        API().callAPI(endPoint: "giftredeem", method: .POST, param: postData) { (json_res, err) in
            
         //   DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                if err == nil {
                    
                    arrGiftRedem.removeAll()
                    
                    do {
                        
                        let catData = try json_res[0]["result"].rawData()
                        arrGiftRedem = try JSONDecoder().decode([GiftRedem].self, from: catData)
                        
                        self.orderSingleTable.reloadData()
                        
                    } catch {
                        
                    }
                    
                }
          //  }
        }
    }
    
}

extension nejreeSingleOrderController: UITableViewDelegate,UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let orderByType = OrderDetailSequenceType(rawValue: section)

        switch orderByType {
        case .ImageHeader:
                return 1
        case .ProductListing:
                return 1
        case .AddressSection:
                return 1
        case .PaymentSummary:
            return 1
            default:
                return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let orderByType = OrderDetailSequenceType(rawValue: indexPath.section)
        switch orderByType {
            
        case .ImageHeader:

            let cell = tableView.dequeueReusableCell(withIdentifier: "nejreeMyOrderImageCell", for: indexPath) as! nejreeLoginImageCell
            cell.btnBack.addTarget(self, action: #selector(dismissButtonTapped(_:)), for: .touchUpInside)

            if(APP_DEL.isLoginFromWishList){

                cell.btnBack.isHidden = true

            }
            else{
                cell.btnBack.isHidden = false
            }

            if APP_DEL.selectedLanguage == Arabic {

                cell.btnBack.setImage(UIImage(named: "icon_back_white_Arabic"), for: .normal)

            } else {

                cell.btnBack.setImage(UIImage(named: "icon_back_white_round"), for: .normal)
            }



            cell.lblSignIn.text = self.orderData.orderlabel?.uppercased()

            return cell
            
        case .AddressSection:
            let cell = tableView.dequeueReusableCell(withIdentifier: "orderAddressAndPaymentCell", for: indexPath) as! orderAddressAndPaymentCell
            
            var name = orderData.ship_to!
            name = name.replacingOccurrences(of: "Lastname", with: "")
            name = name.replacingOccurrences(of: "gigabyte", with: "")
            
            if orderData.method_code == "applepayfort" {
                
                cell.imgViewPayment.image = UIImage(named: "newapple-pay")
                
            } else if orderData.method_code == "payfort" {
                
                cell.imgViewPayment.image = UIImage(named: "mada")
                
            } else if orderData.method_code == "cashondelivery" {
                
                cell.imgViewPayment.image = UIImage(named: "cod")
                
            } else if orderData.method_code == "stcpayment" {
                
                cell.imgViewPayment.image = UIImage(named: "mada")
               
                
            } else if orderData.method_code == "onlinepayment" {
                
                cell.imgViewPayment.image = UIImage(named: "newapple-pay")
                
            } else if orderData.method_code == "applepaycheckout" {
                
                cell.imgViewPayment.image = UIImage(named: "newapple-pay")
            }
            
            else if orderData.method_code == "tamara_pay_later" {
                
                cell.imgViewPayment.image = UIImage(named: "tamara_pay_by_instalments")
                
                //tamara_pay_by_instalments
            } else if orderData.method_code == "tamara_pay_by_instalments" {
                
                cell.imgViewPayment.image = UIImage(named: "tamara_pay_by_instalments")
            }
            
            
            else if orderData.method_code == "checkoutcom_card_payment" {
                
                cell.imgViewPayment.image = UIImage(named: "mada")
               // cell.paymentMethodLabel.text = self.arrFinalPAY[indexPath.row].title!
                
            }
        
            cell.addressHeading.text = APP_LBL().address.uppercased()
//            cell.addressLabel.numberOfLines = 0
            cell.countryName.text = orderData.country
//            cell.addressLabel.text = AddMultiple
            cell.lblCity.text = orderData.city
            cell.lblNeighbourName.text = orderData.neighbour_name
            cell.lblPhone.text = orderData.mobile
            cell.lblName.text = orderData.ship_to
            
            if APP_DEL.selectedLanguage == Arabic {
                
                cell.addressHeading.textAlignment = .right
               cell.countryName.textAlignment = .right
                cell.lblCity.textAlignment = .right
                cell.lblNeighbourName.textAlignment = .right
                cell.lblPhone.textAlignment = .right
                cell.lblName.textAlignment = .right
                
                cell.paymentMethodHeading.textAlignment = .right
                cell.paymentMethodLabel.textAlignment = .right
                
            }
            else
            {
                cell.addressHeading.textAlignment = .left
              cell.countryName.textAlignment = .left
               cell.lblCity.textAlignment = .left
               cell.lblNeighbourName.textAlignment = .left
               cell.lblPhone.textAlignment = .left
               cell.lblName.textAlignment = .left
                
                cell.paymentMethodHeading.textAlignment = .left
                cell.paymentMethodLabel.textAlignment = .left
            }
            
            
            
            
            
            cell.paymentMethodHeading.text = APP_LBL().payment.uppercased()
            cell.paymentMethodLabel.text = orderData.method_title!.uppercased()
            cell.selectionStyle = .none
            return cell
            
        case .ProductListing:
            let cell = tableView.dequeueReusableCell(withIdentifier: "orderItemsCell", for: indexPath) as! orderItemsCell

            if APP_DEL.selectedLanguage == Arabic {
                cell.lblOrderItem.textAlignment = .right
                cell.lblOrderDate.textAlignment = .right
            } else {
                cell.lblOrderItem.textAlignment = .left
                cell.lblOrderDate.textAlignment = .left
            }
            cell.lblOrderItem.text = APP_LBL().items.uppercased()
            cell.selectionStyle = .none
            cell.productList = orderProducts
            cell.rmaStatus = rmaStatus
            cell.manageCarousel()
            cell.parentVC = self
            cell.lblOrderDate.text = self.orderData.orderdate

            return cell
            
        case .PaymentSummary:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "orderTotalCell", for: indexPath) as! orderTotalCell
            cell.valueHeading.text = APP_LBL().value.uppercased()
            cell.discountHeading.text = APP_LBL().discount.uppercased()
            cell.shippingHeading.text = APP_LBL().shipping.uppercased()
            cell.subtotalHeading.text = APP_LBL().subtotal.uppercased()
            cell.storeCreditHeading.text = APP_LBL().store_credit.uppercased()
            cell.totalHeading.text = APP_LBL().total.uppercased()
            cell.taxPriceHeading.text = APP_LBL().tax_price.uppercased()
            cell.codFeeHeading.text = APP_LBL().cod_fee_c.uppercased()
            cell.discountValue.text = orderData.discount
            cell.shippingValue.text = orderData.shipping
            cell.subtotalValue.text = orderData.subtotal
            cell.storeCreditValue.text = orderData.store_credit
            cell.totalValue.text = orderData.grandtotal
            cell.taxPriceValue.text = orderData.tax_amount
            cell.codFeeValue.text = self.codFee
            
            cell.lblReturnOrder.text = APP_LBL().return_order.uppercased()
            cell.lblReturnPeriod.text = APP_LBL().return_period
            cell.lblMoreInfo.text = APP_LBL().more_info.uppercased()
            cell.btnReturnOrder.setTitle(APP_LBL().return_order.uppercased(), for: .normal)
            
            if orderData.store_credit == "" {
                cell.storeCreditHeading.isHidden = true
                cell.storeCreditHeadingHeight.constant = 0
                cell.storeCreditValue.isHidden = true
                cell.storeCreditValueHeight.constant = 0
            }
            
            if self.codFee == "" {
                cell.codFeeHeadingHeight.constant = 0
                cell.codFeeValueHeight.constant = 0
                cell.codFeeHeading.isHidden = true
                cell.codFeeValue.isHidden = true
            }
            
            
            if orderData.returned == "" && orderStatus == "" && rmaStatus == ""
            {
                cell.trackOrderButton.isHidden = false
                
            }
            else if orderData.returned != "" && orderStatus != "" {
                
                cell.trackOrderButton.isHidden = false
            }
            else
            {
                cell.trackOrderButton.isHidden = true
                
            }
            
            if rmaStatus == ""  {
                cell.trackOrderButton.setTitle(APP_LBL().return_.uppercased(), for: .normal)

            }
            else {
                cell.trackOrderButton.setTitle(APP_LBL().track_your_order.uppercased(), for: .normal)
                
            }

            if self.orderStatus == "0" {
                cell.trackOrderButton.isHidden = false
                cell.trackOrderButton.setTitle(APP_LBL().track_your_order.uppercased(), for: .normal)

            }
            
            if orderStatus == "3"{
                //Cancel Order
                 cell.trackOrderButton.isHidden = true
            }
  
            cell.btnCancelOrder.isHidden = true
           if self.isOrderCancelAvalilable != "false"{
                cell.btnCancelOrder.isHidden = false
            }
            
            cell.btnReturnOrder.isUserInteractionEnabled = false
            cell.btnReturnOrder.backgroundColor = UIColor.lightGray
            if isEnableReturnOrder {
                if isDisplayReturnOrder {
                    
                    if isReturnOrderPeriodElapsed {
                        cell.btnReturnOrder.isUserInteractionEnabled = false
                        cell.btnReturnOrder.backgroundColor = UIColor.lightGray
                        cell.lblReturnOrder.superview?.isHidden = false
                    } else {
                        cell.btnReturnOrder.isUserInteractionEnabled = true
                        cell.btnReturnOrder.backgroundColor = UIColor.black
                        cell.lblReturnOrder.superview?.isHidden = true
                    }
                    
                } else {
                    cell.lblReturnOrder.superview?.isHidden = true
                    cell.btnReturnOrder.superview?.isHidden = true
                }
            } else {
                cell.lblReturnOrder.superview?.isHidden = true
                cell.btnReturnOrder.superview?.isHidden = true
            }
            
            if APP_DEL.selectedLanguage == Arabic {
                cell.lblReturnOrder.textAlignment = .right
                cell.lblReturnPeriod.textAlignment = .right
            } else {
                cell.lblReturnOrder.textAlignment = .left
                cell.lblReturnPeriod.textAlignment = .left
            }
     
            cell.trackOrderButton.addTarget(self, action: #selector(trackOrderTapped(_:)), for: .touchUpInside)
            cell.btnReturnOrder.addTarget(self, action: #selector(self.btnReturnOrderAction(_:)), for: .touchUpInside)
            cell.btnMoreInfo.addTarget(self, action: #selector(self.btnReturnOrderInfoAction(_:)), for: .touchUpInside)
            
            
            cell.btnCancelOrder.setTitle(APP_LBL().cancel_order.uppercased(), for: .normal)
            cell.btnCancelOrder.addTarget(self, action: #selector(btnCancelOrder(_:)), for: .touchUpInside)
            
            cell.selectionStyle = .none
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let orderByType = OrderDetailSequenceType(rawValue: indexPath.section)

        switch orderByType {
        case .ImageHeader:
            return UITableView.automaticDimension
        case .AddressSection:
            return UITableView.automaticDimension
        case .ProductListing:
            return UITableView.automaticDimension
        case .PaymentSummary:
             return UITableView.automaticDimension
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.rmaStatus == "" {
            if indexPath.section == 1 {
                orderProducts[indexPath.row].selected = !orderProducts[indexPath.row].selected!
                
                if orderProducts[indexPath.row].selected! {
                    self.itemsToReturn.append(orderProducts[indexPath.row])
                } else {
                    self.itemsToReturn.removeLast()
                }
                
                orderSingleTable.reloadRows(at: [indexPath], with: .automatic)
            }
        }
    }
    
    @objc func trackOrderTapped(_ sender: UIButton) {
        
        if sender.currentTitle == APP_LBL().track_your_order.uppercased() {
        let vc = UIStoryboard(name: "cedMageAccounts", bundle: nil).instantiateViewController(withIdentifier: "nejreeTrackController") as! nejreeTrackController
            vc.status = self.orderStatus//productStatus
            vc.orderNumber = self.orderData.orderlabel!
            vc.trackingId = orderData.tracking_id!
            vc.trackingUrl = orderData.tracking_url!
            vc.shipmentNumber = orderData.shipment_number!
            vc.orderDate = orderData.orderdate!
            vc.rmaStatus = rmaStatus
            vc.orderProduct = orderProducts
        self.navigationController?.pushViewController(vc, animated: true)
        } else {
            if itemsToReturn.count == 0 {
                self.view.makeToast(APP_LBL().please_select_a_product_to_return, duration: 1.0, position: .center)
                return
            }
//            let vc = UIStoryboard(name: "cedMageAccounts", bundle: nil).instantiateViewController(withIdentifier: "nejreeReturnSingleController") as! nejreeReturnSingleController
//            vc.itemsToReturn = self.itemsToReturn
//            vc.reasons = self.reasons
//            vc.orderId = self.orderId!
//            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @objc func btnCancelOrder(_ sender: UIButton) {
        
        // here call cancel order api
        
        let showTitle = APP_LBL().confirmation
        let showMsg = APP_LBL().are_you_sure_to_cancel_order
        
        let confirmationAlert = UIAlertController(title: showTitle, message: showMsg, preferredStyle: UIAlertController.Style.alert);
        confirmationAlert.addAction(UIAlertAction(title: APP_LBL().yes.uppercased(), style: .default, handler: { (action: UIAlertAction!) in
            
            self.cancelOrderAPI()
        }));
        
        confirmationAlert.addAction(UIAlertAction(title: APP_LBL().no, style: .default, handler: { (action: UIAlertAction!) in
           
            
        }));
        
        self.present(confirmationAlert, animated: true, completion: nil)
        
    }
    
    func cancelOrderAPI() {

        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);
        
        var postData = [String:String]()
        postData["cancelOrderId"] = orderId!
        
        
        API().callAPI(endPoint: "mobiconnect/customer/cancelOrder", method: .POST, param: postData) { (json_res, err) in
            
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                if err == nil {
                    
                    if (json_res[0]["success"].stringValue == "true"){
                        APP_DEL.isBackFromCancelOrder = true
                        
                        let showTitle = APP_LBL().success
                        let showMsg = APP_LBL().order_cancelled_successfully
                        
                        let confirmationAlert = UIAlertController(title: showTitle, message: showMsg, preferredStyle: UIAlertController.Style.alert);
                        confirmationAlert.addAction(UIAlertAction(title: APP_LBL().ok.uppercased(), style: .default, handler: { (action: UIAlertAction!) in
                            
                            self.navigationController?.popViewController(animated: true);
                        }));
                        
                        self.present(confirmationAlert, animated: true, completion: nil)
                        
                        
                    }
                    
                }
        
        }
        
        
//        API().callAPI(endPoint: "mobiconnect/customer/cancelOrder/\(orderId!)", method: .GET, param: [:]) { (json_res, err) in
//
//                cedMageLoaders.removeLoadingIndicator(me: self);
//
//                if err == nil {
//
//                    if (json_res[0]["success"].stringValue == "true"){
//                      APP_DEL.isBackFromCancelOrder = true
//
//                        let showTitle = APP_LBL().success
//                        let showMsg = APP_LBL().order_cancelled_successfully
//
//                        let confirmationAlert = UIAlertController(title: showTitle, message: showMsg, preferredStyle: UIAlertController.Style.alert);
//                        confirmationAlert.addAction(UIAlertAction(title: APP_LBL().ok.uppercased(), style: .default, handler: { (action: UIAlertAction!) in
//
//                           self.navigationController?.popViewController(animated: true);
//                        }));
//
//                        self.present(confirmationAlert, animated: true, completion: nil)
//
//
//                    }
//
//
//                }
//
//        }
    }
    
}
