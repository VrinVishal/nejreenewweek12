/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */


import UIKit

class cedMageMyOrders: cedMageViewController,UITableViewDelegate,UITableViewDataSource , UIGestureRecognizerDelegate {
    
//    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var emptyOrders: UIImageView!
    @IBOutlet weak var myOrders: UITableView!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var lblNoData: UILabel!
    
    var ordersData = [[String:String]]()
    
    var noOrder = false
    var currentPage = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        APP_DEL.isOrderFromPushNotification = false
        
        myOrders.isHidden = true
        lblNoData.isHidden = true
        
        myOrders.delegate = self
        myOrders.dataSource = self
        
       // self.navigationController?.navigationBar.isHidden = true
        
        self.getOrder()
        
        myOrders.rowHeight = UITableView.automaticDimension
        myOrders.estimatedRowHeight = 400
        
        self.btnBack.addTarget(self, action: #selector(dismissButtonTapped(_:)), for: .touchUpInside)
        
        if APP_DEL.selectedLanguage == Arabic {

            btnBack.setImage(UIImage(named: "icon_back_white_Arabic"), for: .normal)

        } else {

            btnBack.setImage(UIImage(named: "icon_back_white_round"), for: .normal)
        }
        
        // Do any additional setup after loading the view.
        lblNoData.text = APP_LBL().you_have_no_orders.uppercased()
        lblNoData.textColor = UIColor.darkGray
        
         NotificationCenter.default.addObserver(self, selector: #selector(reloadOrderListAPI), name: NSNotification.Name(rawValue: "reloadAPIOnOrderCancel"), object: nil)
        
         
        
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    override func viewWillAppear(_ animated: Bool) {
           super.viewWillAppear(animated)
          self.navigationController?.setNavigationBarHidden(true, animated: true)
          
        
        if navigationController?.responds(to: #selector(getter: UINavigationController.interactivePopGestureRecognizer)) ?? false {
                   
                   self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
                   self.navigationController?.interactivePopGestureRecognizer?.delegate = self
               }
      
        if APP_DEL.isBackFromCancelOrder{
                  
            APP_DEL.isBackFromCancelOrder = false
            self.ordersData = []
            self.reloadOrderListAPI()
            
        }
               
       }
    
     @objc func reloadOrderListAPI(){
        currentPage = 1
         self.getOrder()
    }
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        
        return true
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //  MARK:- Pagination
    func checkPagination() {
    
        if noOrder {
           return
        }
       
        currentPage += 1
        
        self.getOrder()
    }
    
    func getOrder() {
        
        var postData = [String: String]()
        
        if let userInfoDict = UserDefaults.standard.object(forKey: "userInfoDict") as? [String: String] {
            
            postData["hashkey"] = userInfoDict["hashKey"]
            postData["customer_id"] = userInfoDict["customerId"]
        }
        
        if let storeID =  UserDefaults.standard.value(forKey: "storeId") as? String {
            postData["store_id"] = storeID
        }
        
        postData["page"] = "\(currentPage)"
        
        postData["country_id"] = APP_DEL.selectedCountry.country_code ?? "SA"
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);
        
        API().callAPI(endPoint: "mobiconnect/customer/order", method: .POST, param: postData) { (json_res, err) in
            
            DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                if err == nil {
                    
                    let json = json_res[0]
                    
                    if (json["data"]["status"].stringValue == "no_order") {
                        
                        self.noOrder = true
                    }
                    
                    let jsonCart = json["data"]["orderdata"];
                    if (json["data"]["status"].stringValue == "success") {
                        
                        for c in jsonCart.arrayValue {
                            
                            let shipTo=c["ship_to"].stringValue;
                            let number=c["number"].stringValue;
                            let orderId=c["order_id"].stringValue;
                            let totalAmount=c["total_amount"].stringValue;
                            let orderStatus=c["order_status"].stringValue;
                            let date=c["date"].stringValue;
                            let v = ["shipTo":shipTo,"number":number,"orderId":orderId,"totalAmount":totalAmount,"orderStatus":orderStatus,"date":date];
                            
                            self.ordersData.append(v);
                        }
                        
                        self.myOrders.reloadData()
                    }
                    
                    if (self.ordersData.count == 0) {
                        
                        self.myOrders.isHidden = true
                        self.lblNoData.isHidden = false
                        
                        return;
                        
                    } else {
                        
                        self.myOrders.isHidden = false
                        self.lblNoData.isHidden = true
                    }
                }
            }
        }
    }
    
    @objc func dismissButtonTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
            case 0:
                return 1
        
            default:
                return ordersData.count
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "myOrders") as! cedMageMyOrdersCell
//        let shipTo = ordersData[indexPath.row]["shipTo"];
//        let orderId = ordersData[indexPath.row]["orderId"];
//        let totalAmmount = ordersData[indexPath.row]["totalAmount"];
//        cell.shipTo?.text = shipTo
//        cell.orderId?.text = orderId
//        cell.cost?.text = totalAmmount
//        cell.calenderButton.setTitle(ordersData[indexPath.row]["date"], for: UIControlState.normal);
//        cell.calenderButton.fontColorTool()
//        cell.statusButton.setTitle(ordersData[indexPath.row]["orderStatus"], for: UIControlState.normal);
//        cell.statusButton.fontColorTool()
//        return cell
        
        switch indexPath.section {
                    
        case 0:

            let cell = tableView.dequeueReusableCell(withIdentifier: "nejreeMyOrdersImageCell", for: indexPath) as! nejreeLoginImageCell
            cell.btnBack.addTarget(self, action: #selector(dismissButtonTapped(_:)), for: .touchUpInside)

            cell.lblSignIn.text = APP_LBL().my_orders.uppercased()

            return cell
            
        default:
            
            if indexPath.row == ordersData.count - 3 {
                self.checkPagination()
            }
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "nejreeOrderListingCell", for: indexPath) as! nejreeOrderListingCell
            cell.datelabel.text = ordersData[indexPath.row]["date"]?.uppercased()
            cell.orderLabel.text = APP_LBL().order+" #:".uppercased()
            cell.orderNumber.text = ordersData[indexPath.row]["number"]
            cell.totalText.text = APP_LBL().total+":".uppercased()
            cell.totalLabel.text = ordersData[indexPath.row]["totalAmount"]
            cell.statusLabel.text = ordersData[indexPath.row]["orderStatus"]
            
            if ordersData[indexPath.row]["orderStatus"]?.lowercased() == "processing".lowercased() {
                cell.statusLabel.textColor = UIColor.init(hexString: "#990000")
            } else if ordersData[indexPath.row]["orderStatus"]?.lowercased() == "received".lowercased() {
                cell.statusLabel.textColor = UIColor.init(hexString: "#7d7d7d")
            } else if ordersData[indexPath.row]["orderStatus"]?.lowercased() == "shipped".lowercased() {
                cell.statusLabel.textColor = UIColor.init(hexString: "#FF33B5E5")
            } else if ordersData[indexPath.row]["orderStatus"]?.lowercased() == "delivered".lowercased() {
                cell.statusLabel.textColor = UIColor.init(hexString: "#FF99CC00")
            }
            
            cell.orderNumber.textColor = .black
            cell.statusLabel.textColor = .black
            
            cell.insideView.cardView()
            cell.insideView.backgroundColor = .white
            cell.selectionStyle = .none
            return cell
        
        
        }
        
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
           case 0:
            return UITableView.automaticDimension
       
           default:
            return UITableView.automaticDimension
       }
        return 100
        // return UITableView.UITableViewAutomaticDimension
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let orderId = ordersData[indexPath.row]["orderId"]
        /*let viewcontroll = UIStoryboard(name: "cedMageAccounts", bundle: nil).instantiateViewController(withIdentifier: "cedMageSingleOrder") as! cedMageSingleOrder */
        let viewcontroll = UIStoryboard(name: "cedMageAccounts", bundle: nil).instantiateViewController(withIdentifier: "nejreeSingleOrderController") as! nejreeSingleOrderController
//        viewcontroll.isStatusProccessed = false
        viewcontroll.productStatus = ordersData[indexPath.row]["orderStatus"]!
        viewcontroll.orderId = orderId
        viewcontroll.number = ordersData[indexPath.row]["number"]!
        
        self.navigationController?.pushViewController(viewcontroll, animated: true)
        
    }
    
}
