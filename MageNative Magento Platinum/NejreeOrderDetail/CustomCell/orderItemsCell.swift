//
//  orderItemsCell.swift
//  MageNative Magento Platinum
//
//  Created by MacMini on 23/08/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class orderItemsCell: UITableViewCell {

    
    @IBOutlet weak var lblOrderItem: UILabel!
    @IBOutlet weak var lblOrderDate: UILabel!

    @IBOutlet weak var viewCarousel: iCarousel!

    var parentVC: UIViewController?
    var productList =  [orderItems]()
    var nejreeColor = UIColor.init(hexString: "#FBAD18")
    var rmaStatus = String()


//    @IBOutlet weak var productImage: UIImageView!
//    @IBOutlet weak var productName: UILabel!
//    @IBOutlet weak var quantityLabel: UILabel!
//    @IBOutlet weak var priceLabel: UILabel!
//    @IBOutlet weak var insideView: UIView!
//    @IBOutlet weak var selectionOuterView: UIView!
//    @IBOutlet weak var selectionInnerView: UIView!
//    @IBOutlet weak var selectionViewWidth: NSLayoutConstraint!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        manageCarousel()
    }

  func manageCarousel() {
      
      self.viewCarousel.dataSource = self
      self.viewCarousel.delegate = self
      
      self.viewCarousel.type = iCarouselType.linear
      self.viewCarousel.isPagingEnabled = true
      self.viewCarousel.centerItemWhenSelected = true
      self.viewCarousel.bounces = false
      self.viewCarousel.currentItemIndex = 0
      self.viewCarousel.decelerationRate = 0.6
      self.viewCarousel.autoresizingMask = [.flexibleWidth, .flexibleHeight]
      self.viewCarousel.reloadData()
  }

    @objc func btnReedemCodeAction(btn: UIButton) {
        
        let pasteboard = UIPasteboard.general
        pasteboard.string = (btn.accessibilityLabel ?? "")
        
        parentVC?.view.makeToast(APP_LBL().copied_to_clipboard, duration: 1.0, position: .center)
    }
}

extension orderItemsCell: iCarouselDelegate, iCarouselDataSource {
    
    func numberOfItems(in carousel: iCarousel) -> Int {
        return self.productList.count
    }
    
    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView {
        
        let w = (self.viewCarousel.frame.size.width - 80.0)
        let h = self.viewCarousel.frame.size.height
        let carView = UIView(frame: CGRect(x: 0, y: 0, width: w, height: h))
//        carView.layer.cornerRadius = 20.0
//        carView.clipsToBounds = true
        
        let vi = Bundle.main.loadNibNamed("ViewProduct", owner: self, options: nil)?.first as! ViewProduct
        vi.frame = carView.bounds
        
        vi.productImage.sd_setImage(with: URL(string: productList[index].product_image!), placeholderImage: nil)

        vi.productName.text = productList[index].product_name
        vi.quantityLabel.text = APP_LBL().quantity_semicolon + " " + productList[index].product_qty!
        vi.sizeLabel.text = ""//APP_LBL().size + ": " + productList[index].optionSize!
        vi.productImage.imageShadow()

        vi.priceLabel.text = productList[index].rowsubtotal
//        vi.selectionStyle = .none
         
//        vi.insideView.roundCorners()
//        vi.insideView.layer.borderWidth = 1.0
//        vi.insideView.layer.borderColor = UIColor.init(hexString: "FBAD18")?.cgColor
        vi.selectionViewWidth.constant = 0
        if APP_DEL.selectedLanguage == Arabic {
            
            vi.productName.textAlignment = .right
            vi.quantityLabel.textAlignment = .right
            vi.sizeLabel.textAlignment = .right
            
        }
        else
        {
            vi.productName.textAlignment = .left
            vi.quantityLabel.textAlignment = .left
            vi.sizeLabel.textAlignment = .left
        }
        
        
         
         if self.rmaStatus == "" {
            vi.selectionOuterView.layer.cornerRadius = 5
            vi.selectionInnerView.layer.cornerRadius = 5
            vi.selectionOuterView.layer.borderWidth = 1.0
            vi.selectionOuterView.layer.borderColor = nejreeColor?.cgColor
            vi.selectionViewWidth.constant = 0
             
             if !productList[index].selected! {
                vi.selectionInnerView.backgroundColor = .clear
             } else {
                vi.selectionInnerView.backgroundColor = nejreeColor
             }
         }
        
        
        vi.lblReedem.font = UIFont(name: "Cairo-Regular", size: 10)!
        vi.btnReedemCode.titleLabel?.font = UIFont(name: "Cairo-Regular", size: 10)!
        vi.btnReedemCode.addTarget(self, action: #selector(self.btnReedemCodeAction(btn:)), for: .touchUpInside)
        
        
        let filt = arrGiftRedem.first(where: { (item) -> Bool in ((item.child_product_id ?? "") == (productList[index].child_product_id ?? ""))})

        if filt != nil {

            var one = ""
            one = APP_LBL().to + (filt?.name ?? "")
            one = one + " | " + (filt?.phone ?? "") + " "
            one = one + (filt?.description ?? "")


            vi.sizeLabel.text = one

            let comp = productList[index].rowsubtotal?.components(separatedBy: " ")
            vi.lblPrice.text = String(format: "%.2f", Double(comp?[1] ?? "0.0")!)

            if APP_DEL.selectedLanguage == Arabic{
                vi.productImage.sd_setImage(with: URL(string: productList[index].product_image!), placeholderImage:  UIImage(named: "gift_card_ar"))
            } else {
                vi.productImage.sd_setImage(with: URL(string: productList[index].product_image!), placeholderImage:  UIImage(named: "gift_card_eng"))
            }
           
             
            
            vi.lblPrice.isHidden = false
            vi.lblPriceSAR.isHidden = false
            vi.lblPriceSAR.text = APP_DEL.selectedCountry.getCurrencySymbol().uppercased()
            
            vi.lblReedem.text = APP_LBL().reedem_code + ":"
            vi.btnReedemCode.accessibilityLabel = (filt?.redemecode ?? "")
            vi.btnReedemCode.setTitle((filt?.redemecode ?? ""), for: .normal)
            
        } else {
            vi.sizeLabel.text = APP_LBL().size + ": " + productList[index].optionSize!
            vi.lblPrice.isHidden = true
            vi.lblPriceSAR.isHidden = true
            vi.lblPriceSAR.text = ""
            
            vi.lblReedem.text = ""
            vi.btnReedemCode.setTitle((""), for: .normal)
        }
        
        
        
        vi.viewBg.cardView()
        vi.viewBg.layer.cornerRadius = 10.0

        
        vi.layoutSubviews()
        vi.layoutIfNeeded()
        carView.addSubview(vi)
        
        return carView
    }
    
    func carousel(_ carousel: iCarousel, valueFor option: iCarouselOption, withDefault value: CGFloat) -> CGFloat {
        
        switch (option) {
            
            case .spacing:
                return (1.0 + (20.0 / carousel.itemWidth))
            
            default:
                return value
        }
    }
    
    func carousel(_ carousel: iCarousel, didSelectItemAt index: Int) {
        
        let filt = arrGiftRedem.first(where: { (item) -> Bool in ((item.child_product_id ?? "") == (productList[index].child_product_id ?? ""))})
        
        if filt != nil {
            return;
        }
        
        
        
        let vc = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "productViewController") as! productViewController
        vc.product_id = productList[index].product_id!
        APP_DEL.productIDglobal = productList[index].product_id!
        vc.hidesBottomBarWhenPushed = true
        parentVC?.navigationController?.pushViewController(vc, animated: true)
        
        
    }
}
