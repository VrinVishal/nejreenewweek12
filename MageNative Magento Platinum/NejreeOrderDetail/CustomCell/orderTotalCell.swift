//
//  orderTotalCell.swift
//  MageNative Magento Platinum
//
//  Created by MacMini on 23/08/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class orderTotalCell: UITableViewCell {

    
    @IBOutlet weak var valueHeading: UILabel!
    @IBOutlet weak var subtotalHeading: UILabel!
    @IBOutlet weak var shippingHeading: UILabel!
    @IBOutlet weak var discountHeading: UILabel!
    @IBOutlet weak var totalHeading: UILabel!
    @IBOutlet weak var storeCreditHeading: UILabel!
    
    @IBOutlet weak var subtotalValue: UILabel!
    @IBOutlet weak var shippingValue: UILabel!
    @IBOutlet weak var discountValue: UILabel!
    @IBOutlet weak var totalValue: UILabel!
    @IBOutlet weak var storeCreditValue: UILabel!
    
    @IBOutlet weak var trackOrderButton: UIButton!
    @IBOutlet weak var btnCancelOrder: UIButton!
    @IBOutlet weak var storeCreditHeadingHeight: NSLayoutConstraint!
    @IBOutlet weak var storeCreditValueHeight: NSLayoutConstraint!
    
    @IBOutlet weak var taxPriceHeading: UILabel!
    @IBOutlet weak var taxPriceValue: UILabel!
    
    @IBOutlet weak var codFeeHeading: UILabel!
    @IBOutlet weak var codFeeValue: UILabel!
    @IBOutlet weak var codFeeHeadingHeight: NSLayoutConstraint!
    @IBOutlet weak var codFeeValueHeight: NSLayoutConstraint!
    
    @IBOutlet weak var btnReturnOrder: UIButton!
    @IBOutlet weak var lblReturnOrder: UILabel!
    @IBOutlet weak var lblReturnPeriod: UILabel!
    @IBOutlet weak var lblMoreInfo: UILabel!
    @IBOutlet weak var btnMoreInfo: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    

}
