//
//  orderAddressAndPaymentCell.swift
//  MageNative Magento Platinum
//
//  Created by MacMini on 22/08/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class orderAddressAndPaymentCell: UITableViewCell {

   @IBOutlet weak var imgViewPayment: UIImageView!

    @IBOutlet weak var addressHeading: UILabel!
    @IBOutlet weak var countryName: UILabel!
//    @IBOutlet weak var lblCountryName: UILabel!
    @IBOutlet weak var lblNeighbourName: UILabel!
    @IBOutlet weak var lblPhone: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblCity: UILabel!


    @IBOutlet weak var paymentMethodHeading: UILabel!
    @IBOutlet weak var paymentMethodLabel: UILabel!
    @IBOutlet weak var itemsHeading: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

   

}
