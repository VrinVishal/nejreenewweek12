//
//  ViewProduct.swift
//  MageNative Magento Platinum
//
//  Created by Jayesh Dabhi on 16/07/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class ViewProduct: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblPriceSAR: UILabel!
    
    @IBOutlet weak var viewBg: UIView!

    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var quantityLabel: UILabel!
    @IBOutlet weak var sizeLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var insideView: UIView!
    @IBOutlet weak var selectionOuterView: UIView!
    @IBOutlet weak var selectionInnerView: UIView!
    @IBOutlet weak var selectionViewWidth: NSLayoutConstraint!
    @IBOutlet weak var lblReedem: UILabel!
    @IBOutlet weak var btnReedemCode: UIButton!
    
}
