//
//  NejreeSortVC.swift
//  MageNative Magento Platinum
//
//  Created by vishal.n on 18/02/21.
//  Copyright © 2021 CEDCOSS Technologies Private Limited. All rights reserved.
//

//test merging

import UIKit

protocol DidSelectSortType {
    func didSelectSortType(res: Bool, type: SortType)
}

class NejreeSortVC: UIViewController {

    @IBOutlet weak var viewMainSort: UIView!
    @IBOutlet weak var btnCancel: UIButton!
    
    @IBOutlet weak var collView: UICollectionView!
    @IBOutlet weak var collViewHT: NSLayoutConstraint!
    
    var delegateDidSelectSortType: DidSelectSortType?
   
    var arrSort: [SortType] = [SortType]()
    var preSort: SortType? = nil
    var selectedIndex = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let preS = preSort {
            
            let tempIndex = arrSort.firstIndex { (sort) -> Bool in
                return (sort.id == preS.id)
            }
            
            if let defIndex = tempIndex {
                selectedIndex = defIndex
            }
            
        } else if selectedIndex == -1 {
            
            let tempIndex = arrSort.firstIndex { (sort) -> Bool in
                return (sort.code == "default")
            }
            
            if let defIndex = tempIndex {
                selectedIndex = defIndex
            }
        }
        
        
        setUI()
    }
    
    @IBAction func btnCancelAction(_ sender: UIButton) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.collView.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
    }

    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?)
    {
        if let tbl = object as? UICollectionView {
            
            if tbl == self.collView {
             
                self.collView.layer.removeAllAnimations()
                self.collViewHT.constant = self.collView.contentSize.height
                
            }
        }
        
        UIView.animate(withDuration: 0.5) {
            self.updateViewConstraints()
            self.loadViewIfNeeded()
        }
    }

    func setUI() {
                
        viewMainSort.round(redius: 10)
        viewMainSort.layer.borderWidth = 0.5
        viewMainSort.layer.borderColor = UIColor.lightGray.cgColor
        
        btnCancel.setTitle(APP_LBL().cancel.uppercased(), for: .normal)
        
        collView.register(UINib(nibName: "NejreeSortCell", bundle: nil), forCellWithReuseIdentifier: "NejreeSortCell")
        
        collView.delegate = self
        collView.dataSource = self
        //collView.contentInset = UIEdgeInsets(top: 0, left: 15, bottom: 15, right: 15)
        
        collView.reloadData()
    }
}

extension NejreeSortVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return arrSort.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NejreeSortCell", for: indexPath) as! NejreeSortCell
        
        let sort = arrSort[indexPath.section]
        
        cell.lblTitle.text = ((APP_DEL.selectedLanguage == Arabic) ? sort.label_ar : sort.label_en)
        cell.lblTitle.font = UIFont(name: "Cairo-Regular", size: 15)!
        
        if selectedIndex == indexPath.section {
            cell.imgSelect.image = UIImage(named: "sort_sel")//UIImage(named: "checkboxFilled")
        } else {
            cell.imgSelect.image = UIImage(named: "sort_unsel")//UIImage(named: "checkboxEmpty")
        }
        
        cell.lblDevider.isHidden = (indexPath.section == (arrSort.count - 1))
        
        if APP_DEL.selectedLanguage == Arabic {
            cell.lblTitle.textAlignment = .right
        } else {
            cell.lblTitle.textAlignment = .left
        }
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if selectedIndex == indexPath.section {
            selectedIndex = -1
        } else {
            selectedIndex = indexPath.section
        }
        
        self.collView.reloadData()
        
        self.delegateDidSelectSortType?.didSelectSortType(res: true, type: arrSort[indexPath.section])
        self.dismiss(animated: true, completion: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: SCREEN_WIDTH - 75, height: 60)
    }
 
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {

        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        
        return CGSize(width: SCREEN_WIDTH - 75, height: 0.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        
        return CGSize(width: SCREEN_WIDTH - 75, height: 0.0)
    }
}
