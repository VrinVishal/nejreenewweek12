//
//  APICommon.swift
//  MageNative Magento Platinum
//
//  Created by vishal.n on 04/04/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import Foundation
import Alamofire
import Adjust



//    https://nejree.com/
//    https://nejree.com/rest/V1/


//    https://stagingapp.nejree.com/
//    https://stagingapp.nejree.com/rest/V1/

enum httpMethod: String {
    
    case GET = "GET"
    case POST = "POST"
}



class API
{
    static let JWT_AUTH_KEY = "JWT_AUTH_KEY"
    
    func setJWT(jwt: String) {
        
        
        print("Saved JWT Token to \(jwt)")
        
        if jwt != "" {
            UserDefaults.standard.setValue(jwt, forKey: API.JWT_AUTH_KEY)
        }
    }
    
    func setJWTBlank(jwt: String) {
        
            UserDefaults.standard.setValue(jwt, forKey: API.JWT_AUTH_KEY)
        
    }
    
    
    func getAuthorization() -> String {
        
        let baseURL = cedMage.getInfoPlist(fileName:"cedMage",indexString: "cedBaseUrl") as! String;
        
        if baseURL == "https://stagingapp.nejree.com/rest/V1/" {
            return "Bearer kg1a1wtlvoogth4fpo0hk2t2ljfmzn8h";
            
        } else if baseURL == "https://nejree.com/rest/V1/" {
            return "Bearer q7azizhuvd85scmminj426ssvs0kktl6";
            
        } else if baseURL == "https://stagenew.nejree.com/rest/V1/" {
            return "Bearer q7azizhuvd85scmminj426ssvs0kktl6";
            
        } else if baseURL == "https://dev05.nejree.com/rest/V1/" {
            return "Bearer pgpdmn8i5t1kb2g8em3tc48bxwr1ojtq";
        } else {
            return "";
        }
    }
    
    
    func setHeader(request: URLRequest) -> URLRequest {
        
        var tempReq = request
        let dictHeader : [String:String] = [
            "Content-Type":"application/json",
            "Authorization":getAuthorization(),
            //"timestamp":"\(Date().timeIntervalSince1970)",
            "cache-control": "no-cache",
        ]
        
        for (k,v) in dictHeader {
            tempReq.setValue(v, forHTTPHeaderField: k)
        }
        return tempReq
    }
    
    func getParameterDefault(param: [String : String]) -> [String : String] {
        var paramNew = param
//        paramNew["country_id"] = APP_DEL.selectedCountry.value ?? ""
        return paramNew
    }
    
    
    
    func callAPI(endPoint: String, method: httpMethod, param: [String : String], completion: @escaping (JSON, String?) -> ()) {
        let postString = ["parameters":self.getParameterDefault(param: param)].convtToJson() as String
        let baseURL = cedMage.getInfoPlist(fileName:"cedMage",indexString: "cedBaseUrl") as! String;
        
        let url = baseURL + endPoint
        //var request = URLRequest(url: URL(string: url)!)
        
        var request = URLRequest(url: URL(string: url)!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 50.0)
        
        request.httpMethod = method.rawValue
        
        print(url);
        
        if method != .GET {
            request.httpBody = postString.data(using: String.Encoding.utf8);
            print(postString);
        }
        
        request = API().setHeader(request: request)
        
        print(request.allHTTPHeaderFields ?? "--");
        
        AF.request(request).responseJSON { (response) in

            switch response.result {

            case .success:

                guard let json = try? JSON(data: response.data!) else {
                    return completion(JSON(), "Invalid json formate")
                }

                print(json)

                return completion(json, nil)

            case let .failure(error):

                 print(error.localizedDescription)
                
                return completion(JSON(), error.localizedDescription)
            }
        }

    }
    
    func callDataAPI(endPoint: String, method: httpMethod, param: [String : String], completion: @escaping (Data?, String?) -> ()) {
        
        let postString = ["parameters":self.getParameterDefault(param: param)].convtToJson() as String
        let baseURL = cedMage.getInfoPlist(fileName:"cedMage",indexString: "cedBaseUrl") as! String;
        
        let url = baseURL + endPoint
        var request = URLRequest(url: URL(string: url)!)
        request.httpMethod = method.rawValue
        
        print(url);
        
        if method != .GET {
            request.httpBody = postString.data(using: String.Encoding.utf8);
            print(postString);
        }
        
        request = API().setHeader(request: request)
        
        print(request.allHTTPHeaderFields ?? "--");
        
        AF.request(request).responseJSON { (response) in

            switch response.result {

            case .success:

                do {
                    
                    let json = try JSON(data: response.data!)
                    print(json)
                    
                } catch {
                    
                    print(error.localizedDescription)
                }
                
                return completion(response.data!, nil)

            case let .failure(error):

                return completion(nil, error.localizedDescription)
            }
        }

    }
    
    func setToken(param: [String : String], completion: @escaping (JSON, String?) -> ()) {
        
        //let postString = ["parameters":param].convtToJson() as String
        let postString = param.convtToJson() as String
        
        let url = "https://us-central1-nejree-app.cloudfunctions.net/setToken"
        var request = URLRequest(url: URL(string: url)!)
        request.httpMethod = httpMethod.POST.rawValue
        
        print(url);
        
        request.httpBody = postString.data(using: String.Encoding.utf8);
        print(postString);
        
        
        let dictHeader : [String:String] = [
            "Content-Type":"application/json",
            "Accept":"application/json",
            "Authorization":"eyJhbGciOIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxNzE5OTciYW1lIjoiTkVKUkVFTk9USUZJQ0FUSU9OU05FSlJFRSIsImlhdCI6MTUxNjIzOTAyMn0.vgYycEt4qMujii0T2iUsUlPyfNE0tifzcKmug_Xoc",
            "timestamp":"\(Date().timeIntervalSince1970)"
        ]
        
        for (k,v) in dictHeader {
            request.setValue(v, forHTTPHeaderField: k)
        }
        
        print(request.allHTTPHeaderFields ?? "--");
        
        AF.request(request).responseJSON { (response) in

            switch response.result {

            case .success:

                guard let json = try? JSON(data: response.data!) else {
                    return completion(JSON(), "Invalid json formate")
                }

                print(json)

                return completion(json, nil)

            case let .failure(error):

                 print(error.localizedDescription)
                
                return completion(JSON(), error.localizedDescription)
            }
        }

    }
    
    func setAdjustEvent(eventName: AnalyticEvent, eventDetail: [AnalyticKey:String], completion: @escaping (JSON, String?) -> ()) {
        
        //MARK:- ADJUST
        var event: ADJEvent?
        
        //OLD EVENTS - which is already implemented with setEvent call
        
        if eventName == .Purchase {//Done
            
            event = ADJEvent(eventToken: "yegnks")
            let price = Double((((eventDetail[.PriceWithoutCurrency] ?? "") == "") ? "0.0" : (eventDetail[.PriceWithoutCurrency] ?? "0.0")))!
            event?.setRevenue(price, currency: ((APP_DEL.selectedCountry.currency_code_en ?? "SAR").uppercased()))
            event?.setTransactionId((eventDetail[.TransactionID] ?? ""))
            
        } else if eventName == .Search {//Done
            
            event = ADJEvent(eventToken: "25e4q0")
            
        } else if eventName == .ViewItemList {//Not found
            
            event = ADJEvent(eventToken: "5j3zff")
            
        } else if eventName == .SignUp {//Done
            
            event = ADJEvent(eventToken: "sypc8s")
            
        } else if eventName == .SelectContent {//Done
            
            event = ADJEvent(eventToken: "pyu7ge")
            
        } else if eventName == .BeginCheckout {//Done
            
            event = ADJEvent(eventToken: "vmzypg")
            let price = Double((((eventDetail[.PriceWithoutCurrency] ?? "") == "") ? "0.0" : (eventDetail[.PriceWithoutCurrency] ?? "0.0")))!
            event?.setRevenue(price, currency: ((APP_DEL.selectedCountry.currency_code_en ?? "SAR").uppercased()))
            
        } else if eventName == .FailedCheckout {//Done
            
            event = ADJEvent(eventToken: "t7km85")
            
        } else if eventName == .AddToCart {//Done
            
            event = ADJEvent(eventToken: "3ne2ja")
            let price = Double((((eventDetail[.PriceWithoutCurrency] ?? "") == "") ? "0.0" : (eventDetail[.PriceWithoutCurrency] ?? "0.0")))!
            event?.setRevenue(price, currency: ((APP_DEL.selectedCountry.currency_code_en ?? "SAR").uppercased()))
            
        } else if eventName == .ViewItem {//Done
            
            let event = ADJEvent(eventToken: "l8gq7s")
            let price = Double((((eventDetail[.PriceWithoutCurrency] ?? "") == "") ? "0.0" : (eventDetail[.PriceWithoutCurrency] ?? "0.0")))!
            event?.setRevenue(price, currency: ((APP_DEL.selectedCountry.currency_code_en ?? "SAR").uppercased()))
            
        } else if eventName == .Login {//Done
            
            event = ADJEvent(eventToken: "7o1h28")
        } else if eventName == .AddToWishlist {
            
            event = ADJEvent(eventToken: "l2gvig")
            let price = Double((((eventDetail[.PriceWithoutCurrency] ?? "") == "") ? "0.0" : (eventDetail[.PriceWithoutCurrency] ?? "0.0")))!
            event?.setRevenue(price, currency: ((APP_DEL.selectedCountry.currency_code_en ?? "SAR").uppercased()))
            
        }
        
        
        //NEW EVENTS
        else if eventName == .AppUpdate {
            
            event = ADJEvent(eventToken: "kkqde1")
            
        } else if eventName == .DeeplinkClicked {
            
            event = ADJEvent(eventToken: "cbq35p")
            
        } else if eventName == .OpenCart {//Done
            
            event = ADJEvent(eventToken: "pmeb0d")
            let price = Double((((eventDetail[.PriceWithoutCurrency] ?? "") == "") ? "0.0" : (eventDetail[.PriceWithoutCurrency] ?? "0.0")))!
            event?.setRevenue(price, currency: ((APP_DEL.selectedCountry.currency_code_en ?? "SAR").uppercased()))
            
        }
//        else if eventName == .SearchView {
//
//            event = ADJEvent(eventToken: "kgl4zn")
//
//        }
        else if eventName == .ViewSearchResult {
            
            event = ADJEvent(eventToken: "ey2gr8")
            
        }
        
        
        //For add common custom keys in adjust
        if event != nil {
            
            do {
                
                print(eventName.rawValue)
                
                for detail in eventDetail {
                    
//                    event?.setValue(detail.value, forKey: detail.key.rawValue)
                    event?.addPartnerParameter(detail.key.rawValue, value: detail.value)
                    
                    print(detail.key.rawValue)
                    print(detail.value)
                }
                
                Adjust.trackEvent(event)
                
            } catch {
                
                print("trackEvent error")
                print(error.localizedDescription)
            }
        }

    }
    
    func setEvent(eventName: AnalyticEvent, eventDetail: [AnalyticKey:String], completion: @escaping (JSON, String?) -> ()) {
        
        self.setAdjustEvent(eventName: eventName, eventDetail: eventDetail) { (json, str) in
            
        }
        
        
        //MARK:- setEvent
        var postData = [String:Any]()
        for (k,v) in eventDetail {
            postData[k.rawValue] = v
        }
        
        var param = [String:Any]()
        param["eventName"] = eventName.rawValue
                
        let userInfoDict = UserDefaults.standard.object(forKey: "userInfoDict") as? [String: String]
        param["customerId"] = userInfoDict?["customerId"] ?? "";

        param["token"] = APP_DEL.strFirebaseToken
        param["eventDetails"] = postData
        
        let postString = param.convtToJson() as String
        
        let url = "https://us-central1-nejree-app.cloudfunctions.net/setEvent"
        var request = URLRequest(url: URL(string: url)!)
        request.httpMethod = httpMethod.POST.rawValue
        
        print(url);
        
        request.httpBody = postString.data(using: String.Encoding.utf8);
        print(postString);
        
        
        let dictHeader : [String:String] = [
            "Content-Type":"application/json",
            "Accept":"application/json",
            "Authorization":"eyJhbGciOIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxNzE5OTciYW1lIjoiTkVKUkVFTk9USUZJQ0FUSU9OU05FSlJFRSIsImlhdCI6MTUxNjIzOTAyMn0.vgYycEt4qMujii0T2iUsUlPyfNE0tifzcKmug_Xoc",
            "timestamp":"\(Date().timeIntervalSince1970)"
        ]
        
        for (k,v) in dictHeader {
            request.setValue(v, forHTTPHeaderField: k)
        }
        
        print(request.allHTTPHeaderFields ?? "--");
        
        AF.request(request).responseJSON { (response) in

            switch response.result {

            case .success:

                guard let json = try? JSON(data: response.data!) else {
                    return completion(JSON(), "Invalid json formate")
                }

                print(json)

                return completion(json, nil)

            case let .failure(error):

                 print(error.localizedDescription)
                
                return completion(JSON(), error.localizedDescription)
            }
        }

    }
    
    func randomAlphaNumericString(length: Int) -> String {
        
        let allowedChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let allowedCharsCount = UInt32(allowedChars.count)
        var randomString = ""

        for _ in 0..<length {
            
            let randomNum = Int(arc4random_uniform(allowedCharsCount))
            let randomIndex = allowedChars.index(allowedChars.startIndex, offsetBy: randomNum)
            let newCharacter = allowedChars[randomIndex]
            randomString += String(newCharacter)
        }

        return randomString
    }
}



class CHAT {
        
    func convertServerDateTime(toLocalDeviceDateTime strDateTime: String, isFromChatDetails: Bool) -> String? {
        
        var strDateTime = strDateTime
        
        let dateFormatter1 = DateFormatter()
        dateFormatter1.dateFormat = "yyyy-MM-dd HH:mm:ss"
        guard let date = dateFormatter1.date(from: strDateTime) else {
            return "";
        }
        
        let currentTimeZone = NSTimeZone.local as NSTimeZone
        let utcTimeZone = NSTimeZone(abbreviation: "UTC")!
        
        let currentGMTOffset = currentTimeZone.secondsFromGMT(for: date)
        let gmtOffset = utcTimeZone.secondsFromGMT(for: date)
        let gmtInterval = TimeInterval((currentGMTOffset) - (gmtOffset))
        
        let destinationDate = Date(timeInterval: gmtInterval, since: date)
        let currentDate = Date()
        let timeDifference = currentDate.timeIntervalSince(destinationDate)
        let time = Int(round(timeDifference))
        
        
        let daycount = (Double(time) / 86400.00)
        if daycount <= 0.50 {
            
            let dateFormatters = DateFormatter()
            dateFormatters.dateFormat = "hh:mm a"
            
            dateFormatters.timeZone = NSTimeZone.system
            strDateTime = dateFormatters.string(from: destinationDate)
            
            
            if isFromChatDetails {
                strDateTime = "Today \(strDateTime)"
            } else {
                strDateTime = "\(strDateTime)"
            }
            
            return strDateTime
        } else if daycount > 0 && daycount < 2 {
            
            let dateFormatters = DateFormatter()
            dateFormatters.dateFormat = "hh:mm a"
            
            dateFormatters.timeZone = NSTimeZone.system
            strDateTime = dateFormatters.string(from: destinationDate)
            
            
            if isFromChatDetails {
                strDateTime = "Yesterday \(strDateTime)"
            } else {
                strDateTime = "Yesterday"
            }
            
            return strDateTime
        } else {
            
            
            let dateFormatters = DateFormatter()
            
            if isFromChatDetails {
                dateFormatters.dateFormat = "dd MMM yy hh:mm a"
            } else {
                dateFormatters.dateFormat = "dd MMM yy"
            }
            
            dateFormatters.timeZone = NSTimeZone.system
            strDateTime = dateFormatters.string(from: destinationDate)
            
            
            return strDateTime
        }
    }

}



enum AnalyticEvent: String {
    
    case Purchase = "Purchase"
    case Search = "Search"
    case ViewItemList = "ViewItemList"
    case SignUp = "SignUp"
    case SelectContent = "SelectContent"
    case BeginCheckout = "BeginCheckout"
    case FailedCheckout = "FailedCheckout"
    case AddToCart = "AddToCart"
    case ViewItem = "ViewItem"
    case Login = "Login"
    case AddToWishlist = "AddToWishlist"
    
    
    case AppUpdate = "AppUpdate"
    case DeeplinkClicked = "DeeplinkClicked"
    case OpenCart = "OpenCart"
    case SearchView = "SearchView"
    case ViewSearchResult = "ViewSearchResult"
}

enum AnalyticKey: String {
    
    case Value = "Value"
    case Currency = "Currency"
    case Coupon = "Coupon"
    case Tax = "Tax"
    case ItemID = "ItemID"
    case TransactionID = "TransactionID"
    case SearchTerm = "SearchTerm"
    case ItemCategory = "ItemCategory"
    case Email = "Email"
    case PhoneNumber = "PhoneNumber"
    case ContentType = "ContentType"
    case Quantity = "Quantity"
    case Price = "Price"
    case ItemName = "ItemName"
    case Items = "Items"
    case ItemLocationID = "ItemLocationID"
    case PriceWithoutCurrency = "PriceWithoutCurrency"
    
    
    
}
