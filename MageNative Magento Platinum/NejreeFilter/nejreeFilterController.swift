//
//  nejreeFilterController.swift
//  MageNative Magento Platinum
//
//  Created by MacMini on 03/09/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit
import TTRangeSlider

struct filter {
    var filterName: String?
    var filterCode: String?
    var isExpanded: Bool?
}
struct filterData {
    var filterKey: String?
    var filterData: [String]?
    var filterDataCode: [String]?
}
protocol filterResultDelegate {
    func filterResult(data: String, applyFilter: Bool)
}

var appliedFilter = ""


class nejreeFilterController: MagenativeUIViewController {

    @IBOutlet weak var filterTable: UITableView!
    var isExpanded = false
    var searchString = String()
    var dataCheck = false
    var filters = [filter]()
    var filterDatas = [filterData]()
    var selectedFilters = [String:[String]]()
    

    var isEmpty = false
    var isCategoryFilter = false
    var categoryId = String()
    var currentPage = 1
    var delegate: filterResultDelegate!
    var vc = cedMageDefaultCollection()
    var vc2 = nejreeCategoryController()
    var priceText = "";
    var priceField = UILabel()
    
   
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "Filters"
        self.filterTable.isHidden = true
        if !isCategoryFilter{  self.sendRequest(url: "mobiconnect/catalog/productsearch", params: ["q":searchString,"page":"0","theme":"1","store_id":UserDefaults.standard.value(forKey: "storeId") as! String]) }
        else {
            self.sendRequest(url: "mobiconnect/catalog/productwithoutattribute/", params: ["id":categoryId, "page":String(currentPage),"theme":"1","store_id":UserDefaults.standard.value(forKey: "storeId") as! String,"country_id" : APP_DEL.selectedCountry.country_code ?? "SA"])
        }
        
        
       print("\(appliedFilter)")
       
        // Do any additional setup after loading the view.
        
        appliedFilter = ""
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }

    override func recieveResponse(data: Data?, requestUrl: String?, response: URLResponse?) {
        guard let data = data else {return}
        do {
            var json = try JSON(data: data)
            json = json[0]
            print(json)
            
            for fil in json["data"]["filter"].arrayValue {
                let temp = filter.init(filterName: fil["att_label"].stringValue, filterCode: fil["att_code"].stringValue, isExpanded: false)
                self.filters.append(temp)
            }
            
            print(filters.count)
            
            for (_,val) in json["data"]["filter_label"] {
                var ar = [String]()
                var arCd = [String]()
                var k = String()
                for (ke,va) in val {
                    for var v in va.arrayValue {
                        
                        
                        
                        
                        if ke == "price" {
                            
                            print("\(v.stringValue)")
                            
                            if v.stringValue == "0"{
                                v.stringValue = "1"
                            }
                            
                            
                            ar.append(v.stringValue)
                        } else {
                            let cat = v.stringValue
                            let catCode = cat.components(separatedBy: "#")
                            
                            print(catCode)
                            arCd.append(catCode[0])
                            if ke == "all_brands" || ke == "color" {
                                ar.append(catCode[1] + "#" + catCode[2])
                            }else {
                            ar.append(catCode[1])
                            }
                        }
                    }
                    k = ke
                }
                let temp = filterData.init(filterKey: k, filterData: ar, filterDataCode: arCd)
                
                print(temp)
                
                selectedFilters[k] = [String]()
                self.filterDatas.append(temp)
            }
            
            let temp = filterData.init(filterKey: "apply", filterData: ["hejg"], filterDataCode: ["dshgfahsd"])
            self.filterDatas.append(temp)
            //if !isCategoryFilter{ let temp = filterData.init(filterKey: "apply", filterData: ["hejg"], filterDataCode: ["dshgfahsd"])
           // self.filterDatas.append(temp)
            //}
            
             self.setupTableView()
        } catch {print("inside catch section error occured")}
        
    }
    
    func setupTableView() {
        filterTable.separatorStyle = .none
        filterTable.isHidden = false
        
        filterTable.delegate = self
        filterTable.dataSource = self
        filterTable.backgroundColor = .black
        
        filterTable.reloadData()
    }

}

extension nejreeFilterController: UITableViewDelegate,UITableViewDataSource {
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return filterDatas.count //+ 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        switch filterDatas[indexPath.section].filterKey{//filters[indexPath.section].filterCode {
            case "gender":
                let cell = tableView.dequeueReusableCell(withIdentifier: "genderFilterCell", for: indexPath) as! genderFilterCell
                cell.topLabel.text = APP_LBL().gender.uppercased()//filters[indexPath.section].filterName
                cell.topLabel.textColor = nejreeColor
                
                if APP_DEL.selectedLanguage == Arabic{
                
                     cell.topLabel.textAlignment = .right
                 }
                 else
                 {
                     cell.topLabel.textAlignment = .left
                 }
                
                
                
                cell.genderData = getFilterDataFrom(key: filterDatas[indexPath.section].filterKey!, indexPath: indexPath)
                cell.delegate = self
                cell.parent = self
                if filters[indexPath.section].isExpanded! {cell.expandedImage.image = UIImage(named: "minus ")}
                else {cell.expandedImage.image = UIImage(named: "plus-1")}
                cell.selectionStyle = .none
                
                return cell
            case "size":
                print("size")
                let cell = tableView.dequeueReusableCell(withIdentifier: "sizeFilterCell", for: indexPath) as! sizeFilterCell
                cell.sizeData = getFilterDataFrom(key: filterDatas[indexPath.section].filterKey!, indexPath: indexPath)
                cell.topLabel.text = APP_LBL().size.uppercased()//filters[indexPath.section].filterName
                cell.topLabel.textColor = nejreeColor
                
                if APP_DEL.selectedLanguage == Arabic{
                
                     cell.topLabel.textAlignment = .right
                 }
                 else
                 {
                     cell.topLabel.textAlignment = .left
                 }
                
                cell.parent = self
                cell.delegate = self
                if filters[indexPath.section].isExpanded! {cell.expandedImage.image = UIImage(named: "minus ")}
                else {cell.expandedImage.image = UIImage(named: "plus-1")}
                cell.selectionStyle = .none
                
                return cell
        case "all_brands":
                let cell = tableView.dequeueReusableCell(withIdentifier: "brandFilterCell", for: indexPath) as! brandFilterCell
                cell.brandData = getFilterDataFrom(key: filterDatas[indexPath.section].filterKey!, indexPath: indexPath)
                cell.topLabel.text = APP_LBL().brands.uppercased()//filters[indexPath.section].filterName
                cell.topLabel.textColor = nejreeColor
              
                if APP_DEL.selectedLanguage == Arabic{
                
                     cell.topLabel.textAlignment = .right
                 }
                 else
                 {
                     cell.topLabel.textAlignment = .left
                 }
                
                
                cell.delegate = self
                cell.parent = self
                cell.isManufacturer = false
                if filters[indexPath.section].isExpanded! {cell.expandedImage.image = UIImage(named: "minus ")}
                else {cell.expandedImage.image = UIImage(named: "plus-1")}
                cell.selectionStyle = .none
                return cell
        case "manufacturer":
            let cell = tableView.dequeueReusableCell(withIdentifier: "brandFilterCell", for: indexPath) as! brandFilterCell
            cell.brandData = getFilterDataFrom(key: filterDatas[indexPath.section].filterKey!, indexPath: indexPath)
            cell.topLabel.text = APP_LBL().brands.uppercased()//filters[indexPath.section].filterName
            cell.topLabel.textColor = nejreeColor
                                         
            if APP_DEL.selectedLanguage == Arabic{
            
                 cell.topLabel.textAlignment = .right
             }
             else
             {
                 cell.topLabel.textAlignment = .left
             }
            
            cell.delegate = self
            cell.parent = self
            cell.isManufacturer = true
            if filters[indexPath.section].isExpanded! {cell.expandedImage.image = UIImage(named: "minus ")}
            else {cell.expandedImage.image = UIImage(named: "plus-1")}
            cell.selectionStyle = .none
            return cell
            case "price":
                let data = getFilterDataFrom(key: filterDatas[indexPath.section].filterKey!, indexPath: indexPath)
                print(data.filterData)
                if data.filterData == nil {
                    self.isEmpty = true
                    return UITableViewCell()
                } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "priceFilterCell", for: indexPath) as! priceFilterCell
//                    cell.insideView.cardView()
                    cell.topLabel.text = APP_LBL().price.uppercased()//filters[indexPath.section].filterName
                    cell.topLabel.textColor = nejreeColor
                    
                    
                    
                    if(priceText == "")
                    {
                        priceText = data.filterData![0]  + " SAR" + " - " + data.filterData![1] + " SAR"
                    }
                    cell.priceRangeLabel.text = priceText
                    priceField = cell.priceRangeLabel
                    cell.rangeSlider.selectedHandleDiameterMultiplier = 1
                    cell.rangeSlider.tintColorBetweenHandles = UIColor.init(hexString: "#fcb215")
                    cell.rangeSlider.minValue = Float(data.filterData![0]) ?? 1
                    cell.rangeSlider.maxValue = Float(data.filterData![1]) ?? 111
                    cell.rangeSlider.selectedMinimum = Float(data.filterData![0]) ?? 1
                    cell.rangeSlider.selectedMaximum = Float(data.filterData![1]) ?? 111
                    cell.rangeSlider.handleDiameter = 30
                    cell.rangeSlider.handleColor = UIColor.init(hexString: "#fcb215")
                    cell.rangeSlider.lineHeight = 3
                    cell.rangeSlider.delegate = self
                    cell.backgroundColor = .black
                    cell.rangeSlider.backgroundColor = .black
                    if filters[indexPath.section].isExpanded! {cell.expandedImage.image = UIImage(named: "minus ")}
                        
                        
                        
                        
                    else {cell.expandedImage.image = UIImage(named: "plus-1")}
                    
                    if APP_DEL.selectedLanguage == Arabic{
                               
                        cell.topLabel.textAlignment = .right
                     cell.rangeSlider.semanticContentAttribute = .forceRightToLeft
                    }
                    else
                    {
                        cell.topLabel.textAlignment = .left
                     cell.rangeSlider.semanticContentAttribute = .forceLeftToRight
                    }
                    
                    
                    cell.selectionStyle = .none
                    return cell
                }
                
            case "type":
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "typeFilterCell", for: indexPath) as! typeFilterCell
                cell.typeData = getFilterDataFrom(key: filterDatas[indexPath.section].filterKey!, indexPath: indexPath)
                cell.topLabel.text = APP_LBL().type.uppercased()//filters[indexPath.section].filterName
                cell.topLabel.textColor = nejreeColor
              
                                                      
                if APP_DEL.selectedLanguage == Arabic{
                
                     cell.topLabel.textAlignment = .right
                 }
                 else
                 {
                     cell.topLabel.textAlignment = .left
                 }
                
                
                cell.delegate = self
                  cell.parent = self
                cell.backgroundColor = .black
                if filters[indexPath.section].isExpanded! {cell.expandedImage.image = UIImage(named: "minus ")}
                else {cell.expandedImage.image = UIImage(named: "plus-1")}
                cell.selectionStyle = .none
                
                return cell
            case "color":
                let cell = tableView.dequeueReusableCell(withIdentifier: "colorFilterCell", for: indexPath) as! colorFilterCell
                cell.colorData = getFilterDataFrom(key:  filterDatas[indexPath.section].filterKey!, indexPath: indexPath)
                cell.topLabel.text = APP_LBL().color.uppercased()//filters[indexPath.section].filterName
                cell.topLabel.textColor = nejreeColor
                
             
                                                      
                if APP_DEL.selectedLanguage == Arabic{
                
                     cell.topLabel.textAlignment = .right
                 }
                 else
                 {
                     cell.topLabel.textAlignment = .left
                 }
                
                cell.delegate = self
                 cell.parent = self
                if filters[indexPath.section].isExpanded! {cell.expandedImage.image = UIImage(named: "minus ")}
                else {cell.expandedImage.image = UIImage(named: "plus-1")}
                cell.selectionStyle = .none
                
                return cell
            case "family":
                let cell = tableView.dequeueReusableCell(withIdentifier: "familyFilterCell", for: indexPath) as! familyFilterCell
                cell.familyData = getFilterDataFrom(key:  filterDatas[indexPath.section].filterKey!, indexPath: indexPath)
                cell.topLabel.text = APP_LBL().family.uppercased()//filters[indexPath.section].filterName
                cell.topLabel.textColor = nejreeColor
                
                if APP_DEL.selectedLanguage == Arabic{
                
                     cell.topLabel.textAlignment = .right
                 }
                 else
                 {
                     cell.topLabel.textAlignment = .left
                 }
                
                
                cell.delegate = self
                cell.parent = self
                if filters[indexPath.section].isExpanded! {cell.expandedImage.image = UIImage(named: "minus ")}
                else {cell.expandedImage.image = UIImage(named: "plus-1")}
                cell.selectionStyle = .none
                
                return cell
            default:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "ClearAndApplyFilterCell", for: indexPath) as! ClearAndApplyFilterCell
               // cell.applyFilterButton.layer.cornerRadius = 10.0
                //cell.applyFilterButton.cardView()
                cell.applyFilterButton.setTitle(APP_LBL().apply_filter_c.uppercased(), for: .normal)
                cell.clearAllButton.setTitle(APP_LBL().clear_all.uppercased(), for: .normal)
                cell.applyFilterButton.roundCorners()
                cell.applyFilterButton.setBorder()
                
                cell.applyFilterButton.addTarget(self, action: #selector(applyFilterTapped(_:)), for: .touchUpInside)
                cell.clearAllButton.addTarget(self, action: #selector(clearFilterButtonTapped(_:)), for: .touchUpInside)
                cell.selectionStyle = .none
                
                return cell
            }
            
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch filterDatas[indexPath.section].filterKey{//filters[indexPath.section].filterCode {
        case "gender":
            filters[indexPath.section].isExpanded! = !filters[indexPath.section].isExpanded!
            filterTable.reloadSections([indexPath.section], with: .automatic)
        case "size":
            filters[indexPath.section].isExpanded! = !filters[indexPath.section].isExpanded!
            filterTable.reloadSections([indexPath.section], with: .automatic)
        case "all_brands","manufacturer":
            filters[indexPath.section].isExpanded! = !filters[indexPath.section].isExpanded!
            filterTable.reloadSections([indexPath.section], with: .automatic)
        case "price":
            filters[indexPath.section].isExpanded! = !filters[indexPath.section].isExpanded!
            filterTable.reloadSections([indexPath.section], with: .automatic)
        case "type":
            filters[indexPath.section].isExpanded! = !filters[indexPath.section].isExpanded!
            filterTable.reloadSections([indexPath.section], with: .automatic)
        case "color":
            filters[indexPath.section].isExpanded! = !filters[indexPath.section].isExpanded!
            filterTable.reloadSections([indexPath.section], with: .automatic)
        case "family":
            filters[indexPath.section].isExpanded! = !filters[indexPath.section].isExpanded!
            filterTable.reloadSections([indexPath.section], with: .automatic)
        default:
            print("helloo")
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
        switch filterDatas[indexPath.section].filterKey{//filters[indexPath.section].filterCode {
            case "gender":
                if filters[indexPath.section].isExpanded! {return 120} else {return 70}
            case "size":
                if filters[indexPath.section].isExpanded! {return 140} else {return 70}
            case "all_brands","manufacturer":
               if filters[indexPath.section].isExpanded! {return 140} else {return 70}
            case "price":
                if isEmpty{return 0}else{if filters[indexPath.section].isExpanded! {return 180} else {return 70}}
            case "type":
                if filters[indexPath.section].isExpanded! {return 140} else {return 70}
            case "color":
                if filters[indexPath.section].isExpanded! {return 120} else {return 70}
            case "family":
                if filters[indexPath.section].isExpanded! {return 150} else {return 70}
            default:
                return 150
            }
        
        
    }
    
    func getFilterDataFrom(key: String, indexPath: IndexPath) -> filterData {
        for data in filterDatas {
            if data.filterKey == key {
                return data
            }
        }
        return filterData()
        
    }
    
    @objc func applyFilterTapped(_ sender: UIButton) {
    
    print(self.selectedFilters)
        
        var innerString = String();
        
        innerString += "{"
        for (key,value) in selectedFilters {
            if(value != [String]())
            {
                
                var x = "{"
                for dt in value {
                    let dtr = dt.components(separatedBy: "#")
                    
                    if dtr.count > 2 {
                        //filter += "\""+"price"+"\":{\"from\":\""+value11+"\",\"to\":\""+value12+"\"},";
                        x += "\""+dtr[0]+"\":\""+dtr[2]+"\"";
                        
                        //x += dtr[0] + ":" + dtr[2]
                    } else {
                        x += "\""+dtr[0]+"\":\""+dtr[1]+"\"";
                         //x += dtr[0] + ":" + dtr[1]
                    }
                    x += ","
                }
                
                if(x.characters.last! == ","){
                    x = x.substring(to: x.index(before: x.endIndex));
                }
                x += "}"
                //x += "}"
                innerString += "\""+key+"\":" + x + ","
                //innerString += key + ":" + x
                
            }
            
            
        }
        if(innerString.characters.last! == ","){
            innerString = innerString.substring(to: innerString.index(before: innerString.endIndex));
        }
        innerString += "}"
        
     
        
        
        print(innerString)
        
        self.navigationController?.popViewController(animated: true, completion: {
            if !self.isCategoryFilter{
            self.vc.applyFilter = true
            self.vc.filterString = innerString
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "loadProductsAgainId"), object: nil)
            } else {
                self.delegate.filterResult(data: innerString, applyFilter: true)
                
            }
        })
        
        
    }
  
    
    @objc func clearFilterButtonTapped(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true, completion: {
            self.vc.applyFilter = false
//            self.vc.filterString = innerString
            self.vc.searchString = self.searchString
            if(self.isCategoryFilter)
            {
                self.delegate.filterResult(data: "", applyFilter: false)
            }
            else{
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "loadProductsAgainId"), object: nil)
            }
            
            
        })
        
    }
    
}


extension nejreeFilterController: sizeFilterDelegate,familyFilterDelegate,colorFilterDelegate,typeFilterDelegate,brandFilterDelegate,genderFilterDelegate,TTRangeSliderDelegate {
    
    func getSizeFilter(data: [String : String], mainKey: String, selection: Bool) {
        let keyss = data.keys.first! + "#" + data.values.first!
        if selection {
            
            if selectedFilters.count == 0 {
                
                appliedFilter = mainKey
                
                self.selectedFilters[mainKey] = [data.keys.first! + "#" + data.values.first!]
            }else{
                for(key,value) in selectedFilters {
                    if(key == mainKey)
                    {
                        
                        print("\(key)")
                        print("\(mainKey)")
                        print("\(value)")
                        
                        var val = value
                        val.append(data.keys.first! + "#" + data.values.first!)
                        self.selectedFilters[mainKey] = val
                        
                        appliedFilter = mainKey
                        
                    }
                    
                }
                
            }
            
        } else {
            for (key,val) in selectedFilters {
                if key == mainKey {
                    var val2 = val
                    let index = val2.index(of: keyss)
                    val2.remove(at: index!)
                    self.selectedFilters[key] = val2;
                    
                    
                    
                }
            }
            
            if let value = self.selectedFilters[mainKey] {
                if value.count == 0{
                    print("Got it")
                    appliedFilter = ""
                }
            }

            
            print(self.selectedFilters)
            print(self.selectedFilters.values)
            
        }
        
        
    }
    
    func getFamilyFilter(data: [String : String], mainKey: String, selection: Bool) {
      let keyss = data.keys.first! + "#" + data.values.first!
        
        print(data)
        
        
        if selection {
            if selectedFilters.count == 0 {
                 appliedFilter = mainKey
                self.selectedFilters[mainKey] = [data.keys.first! + "#" + data.values.first!]
            }else{
                for(key,value) in selectedFilters {
                    if(key == mainKey)
                    {
                        var val = value
                        val.append(data.keys.first! + "#" + data.values.first!)
                        self.selectedFilters[mainKey] = val
                         appliedFilter = mainKey
                    }
                    
                }
                
            }
            
        } else {
            for (key,val) in selectedFilters {
                if key == mainKey {
                    var val2 = val
                    let index = val2.index(of: keyss)
                    val2.remove(at: index!)
                    self.selectedFilters[key] = val2;
                }
            }
            
            if let value = self.selectedFilters[mainKey] {
               if value.count == 0{
                   print("Got it")
                   appliedFilter = ""
               }
           }

                       
           print(self.selectedFilters)
           print(self.selectedFilters.values)
            
        }
        
    }
    
    func getColorFilter(data: [String : String], mainKey: String, selection: Bool) {
        let keyss = data.keys.first! + "#" + data.values.first!
        if selection {
            if selectedFilters.count == 0 {
                 appliedFilter = mainKey
                self.selectedFilters[mainKey] = [data.keys.first! + "#" + data.values.first!]
            }else{
                for(key,value) in selectedFilters {
                    if(key == mainKey)
                    {
                        var val = value
                        val.append(data.keys.first! + "#" + data.values.first!)
                        self.selectedFilters[mainKey] = val
                         appliedFilter = mainKey
                    }
                    
                }
                
            }
            
        } else {
            for (key,val) in selectedFilters {
                if key == mainKey {
                    var val2 = val
                    let index = val2.index(of: keyss)
                    val2.remove(at: index!)
                    self.selectedFilters[key] = val2;
                }
            }
            
            if let value = self.selectedFilters[mainKey] {
                if value.count == 0{
                    print("Got it")
                    appliedFilter = ""
                }
            }

                        
            print(self.selectedFilters)
            print(self.selectedFilters.values)
            
        }
        
    }
    
    func getTypeFilter(data: [String : String], mainKey: String, selection: Bool) {
        
        let keyss = data.keys.first! + "#" + data.values.first!
        if selection {
            if selectedFilters.count == 0 {
                 appliedFilter = mainKey
                self.selectedFilters[mainKey] = [data.keys.first! + "#" + data.values.first!]
            }else{
                for(key,value) in selectedFilters {
                    if(key == mainKey)
                    {
                        
                        print("\(key)")
                         print("\(mainKey)")
                         print("\(value)")
                        
                        var val = value
                        val.append(data.keys.first! + "#" + data.values.first!)
                        self.selectedFilters[mainKey] = val
                         appliedFilter = mainKey
                    }
                    
                }
                
            }
            
        } else {
            for (key,val) in selectedFilters {
                if key == mainKey {
                    var val2 = val
                    let index = val2.index(of: keyss)
                    val2.remove(at: index!)
                    self.selectedFilters[key] = val2;
                }
            }
            
            if let value = self.selectedFilters[mainKey] {
               if value.count == 0{
                   print("Got it")
                   appliedFilter = ""
               }
           }

                       
           print(self.selectedFilters)
           print(self.selectedFilters.values)
            
        }
       
    }
    
    func getBrandFilter(data: [String : String], mainKey: String, selection: Bool) {
        let keyss = data.keys.first! + "#" + data.values.first!
        if selection {
            if selectedFilters.count == 0 {
                 appliedFilter = mainKey
                self.selectedFilters[mainKey] = [data.keys.first! + "#" + data.values.first!]
            }else{
                for(key,value) in selectedFilters {
                    if(key == mainKey)
                    {
                        var val = value
                        val.append(data.keys.first! + "#" + data.values.first!)
                        self.selectedFilters[mainKey] = val
                         appliedFilter = mainKey
                    }
                    
                }
                
            }
            
        } else {
            for (key,val) in selectedFilters {
                if key == mainKey {
                    var val2 = val
                    let index = val2.index(of: keyss)
                    val2.remove(at: index!)
                    self.selectedFilters[key] = val2;
                }
            }
            
            if let value = self.selectedFilters[mainKey] {
                if value.count == 0{
                    print("Got it")
                    appliedFilter = ""
                }
            }

                        
            print(self.selectedFilters)
            print(self.selectedFilters.values)
            
        }
    }
    
    func getGenderFilter(data: [String : String], mainKey: String, selection: Bool) {
        let keyss = data.keys.first! + "#" + data.values.first!
        if selection {
            if selectedFilters.count == 0 {
                 appliedFilter = mainKey
                self.selectedFilters[mainKey] = [data.keys.first! + "#" + data.values.first!]
            }else{
                for(key,value) in selectedFilters {
                    if(key == mainKey)
                    {
                        var val = value
                        val.append(data.keys.first! + "#" + data.values.first!)
                        self.selectedFilters[mainKey] = val
                         appliedFilter = mainKey
                    }
                    
                }
                
            }
            
        } else {
            for (key,val) in selectedFilters {
                if key == mainKey {
                    var val2 = val
                    let index = val2.index(of: keyss)
                    val2.remove(at: index!)
                    self.selectedFilters[key] = val2;
                }
            }
            
            if let value = self.selectedFilters[mainKey] {
                if value.count == 0{
                    print("Got it")
                    appliedFilter = ""
                }
            }

                        
            print(self.selectedFilters)
            print(self.selectedFilters.values)
            
        }
    }
    
    
//    func didStartTouches(in sender: TTRangeSlider!) {
//        print("start touch slider")
//        
//        if  appliedFilter != ""{
//
//            if appliedFilter != "price" {
//             
//                cedMageHttpException.showAlertView(me: self, msg: "you can only choose one filter".localized, title: "Error".localized)
//                return
//            }
//        }
//        
//    }
    
    
    
    func rangeSlider(_ sender: TTRangeSlider!, didChangeSelectedMinimumValue selectedMinimum: Float, andMaximumValue selectedMaximum: Float) {
        print("\(selectedMinimum) and \(selectedMaximum)")
        
        let minInt = Int(selectedMinimum)
        let maxInt = Int(selectedMaximum)
        
        
        
        priceText = "\(minInt) SAR" + " - " + "\(maxInt) SAR"
        priceField.text = priceText;
        self.selectedFilters["price"] = [ "0.0-" + "0.0" + "#" + String(minInt) + "-" + String(maxInt)]
        
        
    }
    
    
}
