//
//  familyFilterCollectionCell.swift
//  MageNative Magento Platinum
//
//  Created by MacMini on 04/09/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class familyFilterCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var familyLabel: UILabel!
    
    override var isSelected: Bool {
        didSet {
            self.backgroundColor = isSelected ? UIColor.init(hexString: "#fcb215") : .black
            self.familyLabel.textColor = isSelected ? .white : .darkGray
        }
    }
    
}
