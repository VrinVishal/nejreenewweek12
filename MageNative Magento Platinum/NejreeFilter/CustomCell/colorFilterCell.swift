//
//  colorFilterCell.swift
//  MageNative Magento Platinum
//
//  Created by MacMini on 04/09/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit


protocol colorFilterDelegate {
    func getColorFilter(data: [String:String], mainKey: String, selection: Bool)
}


class colorFilterCell: UITableViewCell {

    @IBOutlet weak var insideView: UIView!
    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var expandedImage: UIImageView!
    @IBOutlet weak var colorCollection: UICollectionView!
    
    var colorData = filterData()
    var delegate: colorFilterDelegate!
    
    var parent = UIViewController()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
//        insideView.cardView()
        colorCollection.delegate = self
        colorCollection.dataSource = self
        colorCollection.allowsMultipleSelection = true
        // Initialization code
    }

   

}

extension colorFilterCell: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (colorData.filterData?.count)!
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "colorFilterCollectionCell", for: indexPath) as! colorFilterCollectionCell
        cell.layer.cornerRadius = 5.0
        cell.backgroundColor = .black
        
        let bName = colorData.filterData![indexPath.item]
        let bn = bName.components(separatedBy: "#")
        
        
//        cell.colorImage.sd_setImage(with: URL(string: colorData.filterData![indexPath.item]), placeholderImage: UIImage(named: "placeholder"))
        
        cell.colorImage.sd_setImage(with: URL(string: colorData.filterData![indexPath.item]), placeholderImage: nil)
        
//        cell.colorName.text = bn[1]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 30, height: 30)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        var dt = [String:String]()
        dt[colorData.filterDataCode![indexPath.row]] = colorData.filterData![indexPath.row]
        
        self.delegate.getColorFilter(data: dt, mainKey: colorData.filterKey!, selection: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        var dt = [String:String]()
        dt[colorData.filterDataCode![indexPath.row]] = colorData.filterData![indexPath.row]
        
        self.delegate.getColorFilter(data: dt, mainKey: colorData.filterKey!, selection: false)
    }
//    func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
//           
//           if  appliedFilter != ""{
//               
//               if appliedFilter == colorData.filterKey! {
//                
//                for i in 0..<(colorData.filterData?.count ?? 0){
//                                    
//                                    
//                    if let cell = collectionView.cellForItem(at: IndexPath(item: i, section: 0)){
//                        
//                        if cell.isSelected == true{
//                            
//                             cedMageHttpException.showAlertView(me: parent, msg: "you can only choose one filter".localized, title: "Error".localized)
//                            return false
//                        }
//                        
//                        
//                    }
//                }
//                
//                   return true
//               }
//               else{
//                   
//                   cedMageHttpException.showAlertView(me: parent, msg: "you can only choose one filter".localized, title: "Error".localized)
//                   
//                   return false
//               }
//               
//           }
//           return true
//       }
//    
    
}
