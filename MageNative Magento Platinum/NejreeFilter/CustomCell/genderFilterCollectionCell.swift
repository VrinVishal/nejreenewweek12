//
//  genderFilterCollectionCell.swift
//  MageNative Magento Platinum
//
//  Created by MacMini on 05/09/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class genderFilterCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var checkboxImage: UIImageView!
    @IBOutlet weak var genderLabel: UILabel!
    
    
    override var isSelected: Bool {
        didSet {
            checkboxImage.image = isSelected ? UIImage(named: "checkboxFilled") : UIImage(named: "checkboxEmpty")
        }
    }
    
    
    
    
}
