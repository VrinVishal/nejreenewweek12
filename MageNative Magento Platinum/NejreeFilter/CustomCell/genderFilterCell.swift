//
//  genderFilterCell.swift
//  MageNative Magento Platinum
//
//  Created by MacMini on 03/09/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

protocol genderFilterDelegate {
    func getGenderFilter(data: [String:String], mainKey: String, selection: Bool)
}


class genderFilterCell: UITableViewCell {

   
    @IBOutlet weak var insideView: UIView!
    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var expandedImage: UIImageView!
    @IBOutlet weak var genderCollection: UICollectionView!
    var genderData = filterData()
    var delegate: genderFilterDelegate!
    
     var parent = UIViewController()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
//        insideView.cardView()
        genderCollection.delegate = self
        genderCollection.dataSource = self
        genderCollection.allowsMultipleSelection = true
       
        // Initialization code
    }

  
}

extension genderFilterCell: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (genderData.filterData?.count)!
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "genderFilterCollectionCell", for: indexPath) as! genderFilterCollectionCell
//        cell.insideView.layer.borderColor = UIColor.init(hexString: "#fcb215")?.cgColor
//        cell.insideView.layer.borderWidth = 2.0
//        cell.insideView.layer.cornerRadius = 5.0
        cell.genderLabel.text = genderData.filterData![indexPath.item]
        cell.genderLabel.textColor = .lightGray//UIColor.init(hexString: "FBAD18")
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 130, height: 50)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        var dt = [String:String]()
        dt[genderData.filterDataCode![indexPath.row]] = genderData.filterData![indexPath.row]
        
        self.delegate.getGenderFilter(data: dt, mainKey: genderData.filterKey!, selection: true)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        
        var dt = [String:String]()
        dt[genderData.filterDataCode![indexPath.row]] = genderData.filterData![indexPath.row]
        
        self.delegate.getGenderFilter(data: dt, mainKey: genderData.filterKey!, selection: false)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        
        if  appliedFilter != ""{
            
            if appliedFilter != genderData.filterKey! {
                
                print("genderData.filterKey");
                print(appliedFilter)
                
                for i in 0..<(genderData.filterData?.count ?? 0){
                    
                    
                    if let cell = collectionView.cellForItem(at: IndexPath(item: i, section: 0)){
                        
                        if cell.isSelected == true{
                            
                             cedMageHttpException.showAlertView(me: parent, msg: APP_LBL().only_one_filter_allowed, title: APP_LBL().error)
                            return false
                        }
                        
                        
                    }
                }
                
                
                return true
            }
            else{
                
                cedMageHttpException.showAlertView(me: parent, msg: APP_LBL().only_one_filter_allowed, title: APP_LBL().error)
                
                return false
            }
            
        }
        return true
    }
    
}
