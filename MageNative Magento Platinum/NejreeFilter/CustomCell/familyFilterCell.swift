//
//  familyFilterCell.swift
//  MageNative Magento Platinum
//
//  Created by MacMini on 04/09/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit


protocol familyFilterDelegate {
    func getFamilyFilter(data: [String:String], mainKey: String, selection: Bool)
}



class familyFilterCell: UITableViewCell {

    
    @IBOutlet weak var insideView: UIView!
    @IBOutlet weak var expandedImage: UIImageView!
    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var familyCollection: UICollectionView!
    
    var familyData = filterData()
    var delegate: familyFilterDelegate!
    
    var parent = UIViewController()
    
    override func awakeFromNib() {
        super.awakeFromNib()
//        insideView.cardView()
        familyCollection.delegate = self
        familyCollection.dataSource = self
        familyCollection.allowsMultipleSelection = true
        // Initialization code
    }

   

}

extension familyFilterCell: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (familyData.filterData?.count)!
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "familyFilterCollectionCell", for: indexPath) as! familyFilterCollectionCell
        cell.layer.borderColor = UIColor.init(hexString: "#fcb215")!.cgColor
        cell.layer.borderWidth = 2.0
        cell.layer.cornerRadius = 5.0
        
        cell.familyLabel.text = familyData.filterData![indexPath.item]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 120, height: 60)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        var dt = [String:String]()
        dt[familyData.filterDataCode![indexPath.row]] = familyData.filterData![indexPath.row]
        
        self.delegate.getFamilyFilter(data: dt, mainKey: familyData.filterKey!, selection: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        var dt = [String:String]()
        dt[familyData.filterDataCode![indexPath.row]] = familyData.filterData![indexPath.row]
        
        self.delegate.getFamilyFilter(data: dt, mainKey: familyData.filterKey!, selection: true)
    }
//    func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
//        
//        if  appliedFilter != ""{
//            
//            if appliedFilter == familyData.filterKey! {
//                for i in 0..<(familyData.filterData?.count ?? 0){
//                                    
//                                    
//                    if let cell = collectionView.cellForItem(at: IndexPath(item: i, section: 0)){
//                        
//                        if cell.isSelected == true{
//                            
//                             cedMageHttpException.showAlertView(me: parent, msg: "you can only choose one filter".localized, title: "Error".localized)
//                            return false
//                        }
//                        
//                        
//                    }
//                }
//                
//                return true
//            }
//            else{
//                
//                cedMageHttpException.showAlertView(me: parent, msg: "you can only choose one filter".localized, title: "Error".localized)
//                
//                return false
//            }
//            
//        }
//        return true
//    }
    
}
