//
//  sizeFilterCollectionCell.swift
//  MageNative Magento Platinum
//
//  Created by MacMini on 04/09/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class sizeFilterCollectionCell: UICollectionViewCell {
 
    @IBOutlet weak var sizeLabel: UILabel!
    @IBOutlet weak var insideView: UIView!
    
    
   
    
    override var isSelected: Bool {
        didSet{
            insideView.layer.borderColor = isSelected ? UIColor.init(hexString: "#fcb215")?.cgColor : UIColor.lightGray.cgColor
            sizeLabel.textColor = isSelected ? UIColor.init(hexString: "#fcb215") : .lightGray }
    }
    
    
    
    
}
