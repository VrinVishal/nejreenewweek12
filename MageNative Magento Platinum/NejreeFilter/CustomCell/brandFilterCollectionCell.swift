//
//  brandFilterCollectionCell.swift
//  MageNative Magento Platinum
//
//  Created by MacMini on 04/09/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class brandFilterCollectionCell: UICollectionViewCell {
   
    @IBOutlet weak var insideView: UIView!
    @IBOutlet weak var brandImage: UIImageView!
    @IBOutlet weak var brandName: UILabel!
    
    
   override var isSelected: Bool {
        didSet{
            
            self.layer.borderColor = isSelected ? UIColor.init(hexString: "#fcb215")?.cgColor : UIColor.lightGray.cgColor
            brandName.textColor = isSelected ? .white : .lightGray
        }
    }

    
    
}
