//
//  sizeFilterCell.swift
//  MageNative Magento Platinum
//
//  Created by MacMini on 04/09/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit


protocol sizeFilterDelegate {
    func getSizeFilter(data: [String:String], mainKey: String, selection: Bool)
}



class sizeFilterCell: UITableViewCell {

  
    @IBOutlet weak var insideView: UIView!
    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var sizeCollection: UICollectionView!
    @IBOutlet weak var expandedImage: UIImageView!
    
     var parent = UIViewController()
    
    
     var delegate: sizeFilterDelegate!
    var sizeData = filterData()
    override func awakeFromNib() {
        super.awakeFromNib()
        
//        insideView.cardView()
        sizeCollection.delegate = self
        sizeCollection.dataSource = self
        sizeCollection.allowsMultipleSelection = true
        // Initialization code
        
       
    }

   

}

extension sizeFilterCell: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (sizeData.filterData?.count)!
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "sizeFilterCollectionCell", for: indexPath) as! sizeFilterCollectionCell
        cell.insideView.layer.borderColor = UIColor.lightGray.cgColor
        cell.insideView.layer.borderWidth = 2.0
        cell.insideView.layer.cornerRadius = 5.0
        cell.sizeLabel.text = sizeData.filterData![indexPath.item]
        cell.insideView.backgroundColor = .darkGray
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 45, height: 45)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        var dt = [String:String]()
        dt[sizeData.filterDataCode![indexPath.row]] = sizeData.filterData![indexPath.row]
        
        self.delegate.getSizeFilter(data: dt, mainKey: sizeData.filterKey!, selection: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        
        var dt = [String:String]()
        dt[sizeData.filterDataCode![indexPath.row]] = sizeData.filterData![indexPath.row]
        
        self.delegate.getSizeFilter(data: dt, mainKey: sizeData.filterKey!, selection: false)
    }
    func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        
        if  appliedFilter != ""{
            
            if appliedFilter == sizeData.filterKey! {
                
                for i in 0..<(sizeData.filterData?.count ?? 0){
                    
                    
                    if let cell = collectionView.cellForItem(at: IndexPath(item: i, section: 0)){
                        
                        if cell.isSelected == true{
                            
                             cedMageHttpException.showAlertView(me: parent, msg: APP_LBL().only_one_filter_allowed, title: APP_LBL().error)
                            return false
                        }
                        
                        
                    }
                }
                
                
                
                return true
            }
            else{
                
                cedMageHttpException.showAlertView(me: parent, msg: APP_LBL().only_one_filter_allowed, title: APP_LBL().error)
                
                return false
            }
            
        }
        return true
    }
}
