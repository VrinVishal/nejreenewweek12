//
//  typeFilterCell.swift
//  MageNative Magento Platinum
//
//  Created by MacMini on 04/09/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

protocol typeFilterDelegate {
    func getTypeFilter(data: [String:String], mainKey: String, selection: Bool)
}


class typeFilterCell: UITableViewCell {

 
    @IBOutlet weak var insideView: UIView!
    @IBOutlet weak var expandedImage: UIImageView!
    @IBOutlet weak var typeCollection: UICollectionView!
    @IBOutlet weak var topLabel: UILabel!
    
     var parent = UIViewController()
    
    var typeData = filterData()
    var delegate: typeFilterDelegate!
    
    override func awakeFromNib() {
        super.awakeFromNib()
//        insideView.cardView()
        typeCollection.delegate = self
        typeCollection.dataSource = self
        typeCollection.allowsMultipleSelection = true
        typeCollection.backgroundColor = .black
        // Initialization code
    }

    

}

extension typeFilterCell: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (typeData.filterData?.count)!
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "typeFilterCollectionCell", for: indexPath) as! typeFilterCollectionCell
        cell.layer.borderColor = UIColor.lightGray.cgColor
        cell.layer.borderWidth = 1.0
        cell.layer.cornerRadius = 5.0
        cell.backgroundColor = .darkGray
        cell.typeLabel.textColor = .lightGray
        cell.typeLabel.text = typeData.filterData![indexPath.item]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 120, height: 40)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        var dt = [String:String]()
        dt[typeData.filterDataCode![indexPath.row]] = typeData.filterData![indexPath.row]
        
        self.delegate.getTypeFilter(data: dt, mainKey: typeData.filterKey!, selection: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        var dt = [String:String]()
        dt[typeData.filterDataCode![indexPath.row]] = typeData.filterData![indexPath.row]
        
        self.delegate.getTypeFilter(data: dt, mainKey: typeData.filterKey!, selection: false)
    }
    
    func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        
        if  appliedFilter != ""{
            
            if appliedFilter != typeData.filterKey! {
                
                for i in 0..<(typeData.filterData?.count ?? 0){
                                    
                                    
                    if let cell = collectionView.cellForItem(at: IndexPath(item: i, section: 0)){
                        
                        if cell.isSelected == true{
                            
                             cedMageHttpException.showAlertView(me: parent, msg: APP_LBL().only_one_filter_allowed, title: APP_LBL().error)
                            return false
                        }
                        
                        
                    }
                }
                
                return true
            }
            else{
                
                cedMageHttpException.showAlertView(me: parent, msg: APP_LBL().only_one_filter_allowed, title: APP_LBL().error)
                
                return false
            }
            
        }
        return true
    }
    
    
}
