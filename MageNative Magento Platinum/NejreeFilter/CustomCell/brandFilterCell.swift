//
//  brandFilterCell.swift
//  MageNative Magento Platinum
//
//  Created by MacMini on 04/09/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

protocol brandFilterDelegate {
    func getBrandFilter(data: [String:String], mainKey: String, selection: Bool)
}



class brandFilterCell: UITableViewCell {

  
    @IBOutlet weak var insideView: UIView!
    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var brandCollection: UICollectionView!
    @IBOutlet weak var expandedImage: UIImageView!
    
    var parent = UIViewController()
    
    var brandData = filterData()
    var delegate: brandFilterDelegate!
    var isManufacturer = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
//        insideView.cardView()
        brandCollection.delegate = self
        brandCollection.dataSource = self
        brandCollection.allowsMultipleSelection = true
        // Initialization code
    }

 
}

extension brandFilterCell: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (brandData.filterData?.count)!
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "brandFilterCollectionCell", for: indexPath) as! brandFilterCollectionCell
            
            cell.layer.borderColor = UIColor.lightGray.cgColor
            cell.layer.borderWidth = 1.0
            cell.layer.cornerRadius = 5.0
            cell.backgroundColor = .darkGray
            cell.brandName.textColor = .lightGray
            
    //        cell.insideView.layer.borderColor = UIColor.lightGray.cgColor
    //        cell.insideView.layer.borderWidth = 2.0
    //        cell.insideView.layer.cornerRadius = 5.0
    //        cell.backgroundColor = .black
            
            cell.brandImage.clipsToBounds = true
            cell.brandImage.layer.cornerRadius = 5.0
            //cell.brandImage.backgroundColor = .darkGray
            let bName = brandData.filterData![indexPath.item]
            let bn = bName.components(separatedBy: "#")
            
//            cell.brandImage.sd_setImage(with: URL(string: brandData.filterData![indexPath.item]), placeholderImage: UIImage(named: "placeholder"))
        
        cell.brandImage.sd_setImage(with: URL(string: brandData.filterData![indexPath.item]), placeholderImage: nil)
            
            if isManufacturer {
                cell.brandName.text = bName
                cell.brandName.isHidden = false
                cell.brandImage.isHidden = true
                cell.insideView.backgroundColor = .darkGray
            } else {
                 cell.brandName.text = bn[1]
                cell.brandName.isHidden = true
                cell.brandImage.isHidden = false
            }
           
            
            return cell
        }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 120, height: 40)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        var dt = [String:String]()
        dt[brandData.filterDataCode![indexPath.row]] = brandData.filterData![indexPath.row]
        
        self.delegate.getBrandFilter(data: dt, mainKey: brandData.filterKey!, selection: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        var dt = [String:String]()
        dt[brandData.filterDataCode![indexPath.row]] = brandData.filterData![indexPath.row]
        
        self.delegate.getBrandFilter(data: dt, mainKey: brandData.filterKey!, selection: false)
    }
    
    func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        
        if  appliedFilter != ""{
            
            if appliedFilter == brandData.filterKey! {
                
                for i in 0..<(brandData.filterData?.count ?? 0){
                                   
                                   
                   if let cell = collectionView.cellForItem(at: IndexPath(item: i, section: 0)){
                       
                       if cell.isSelected == true{
                           
                            cedMageHttpException.showAlertView(me: parent, msg: APP_LBL().only_one_filter_allowed, title: APP_LBL().error)
                           return false
                       }
                       
                       
                   }
               }
                
                
                return true
            }
            else{
                
                cedMageHttpException.showAlertView(me: parent, msg: APP_LBL().only_one_filter_allowed, title: APP_LBL().error)
                
                return false
            }
            
        }
        return true
    }
    
    
    
}
