//
//  typeFilterCollectionCell.swift
//  MageNative Magento Platinum
//
//  Created by MacMini on 04/09/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class typeFilterCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var typeLabel: UILabel!

    override var isSelected: Bool {
        didSet{
            self.layer.borderColor = isSelected ? UIColor.init(hexString: "#fcb215")?.cgColor : UIColor.lightGray.cgColor
            typeLabel.textColor = isSelected ? .white : .lightGray
        }
    }

}


