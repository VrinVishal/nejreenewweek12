//
//  colorFilterCollectionCell.swift
//  MageNative Magento Platinum
//
//  Created by MacMini on 04/09/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class colorFilterCollectionCell: UICollectionViewCell {
    
    
    @IBOutlet weak var colorImage: UIImageView!
    @IBOutlet weak var colorName: UILabel!
    
    override var isSelected: Bool{
        didSet{
            self.layer.borderColor = UIColor.init(hexString: "#fcb215")?.cgColor
            self.layer.borderWidth = isSelected ? 1 : 0
        
        }
    }
    
}
