//
//  nejreeHomeController.swift
//  MageNative Magento Platinum
//
//  Created by cedcoss on 11/09/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

struct product {
    var offer: String?
    var product_name: String?
    var stock_status: String?
    var product_id: String?
    var type: String?
    var special_price: String?
    var product_url: String?
    var product_image: String?
    var wishlist_item_id: String?
    var Inwishlist: String?
    var description: String?
    var review: String?
    var regular_price: String?
    var brands_name: String?
    var tag: String?
}

struct nejreeBannerData {
    
    var id: String?
    var title: String?
    var banner_image: String?
    var type: String?
    var link_to: String?
    var product_id: String?
}


import Firebase
//import FirebaseInAppMessaging
//import FirebaseMessaging
//import FirebaseInstanceID
import Alamofire

class nejreeHomeController: MagenativeUIViewController {

    var currentPage = 1
    var banners = [String]()
    var bannerData = [nejreeBannerData]()
    var noProduct = false
    
    var products: [product] = []
    @IBOutlet weak var homeCollection: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

//        self.navigationItem.title = "Home".localized
        sendRequestForHomepage()
        homeCollection.delegate = self
        homeCollection.dataSource = self
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        NotificationCenter.default.addObserver(self, selector: #selector(willEnterForeground), name: UIApplication.willEnterForegroundNotification, object: nil)
        
        willEnterForeground()
    }
    
    func isKeyPresentInUserDefaults(key: String) -> Bool {
        return UserDefaults.standard.object(forKey: key) != nil
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
//        let cartCount = self.defaults.value(forKey: "items_count") as! String;
//        print("cartcount")
//        print(cartCount)
//        cedMageViewController().setCartCount(view:self,items: cartCount)
        
        
//        InstanceID.instanceID().instanceID { (result, error) in
//                      if let error = error {
//                          print("Error fetching remote instange IXD: \(error)")
//                      } else if let result = result {
//                       print("Remote instance ID tokenX: \(result.instanceID)")
//
//
//                       if(self.isKeyPresentInUserDefaults(key: "EmailId")){
//                           let sEmail = UserDefaults.standard.value(forKey: "EmailId");
//                           print("Email Check : \(sEmail)")
//
//
//
//                           let urlString = "http://15.185.60.39:3000/saveFirebaseInstance"
//                           let headersNew = ["Authorization" : "AIzaSyd7XPB1z5QA6M_-DzWXeVJPNiGBqo-vQt8",
//                           "Content-Type": "application/json"]
//
//                           let emailExists = UserDefaults.standard.value(forKey: "EmailId") as? String;
//                           print("Email Fetch Success2");
//                           print(emailExists);
//                           var eme = emailExists!;
//
//                           if(eme != ""){
//                               print("Email Fetch Success");
//                               print(eme);
//
//
//                               Alamofire.request(urlString, method: .post, parameters: ["email": eme, "device": "ios", "version": "2.0.3", "instanceId": result.instanceID],encoding: JSONEncoding.default, headers: headersNew).responseString {
//                                              response in
//
//                                                  switch response.result {
//                                                              case .success:
//                                                                  print("response112233")
//
//                                                                  print(response)
//                                                                  print("response112233")
//
//
//
//                                                                  break
//                                                              case .failure(let error):
//
//                                                                  print(error)
//                                                                  print("error112233")
//                                                              }
//                                              }
//
//
//                           }
//
//
//
//                       }else{
//                            print("E N R")
//                       }
//
//                  }
//               }
    }
    
    @objc func willEnterForeground() {

        if isCheckForUpdate {
            
            let alert = UIAlertController(title: "Nejree", message: APP_LBL().please_update_new_version_from_the_store, preferredStyle: .alert)
            let ok = UIAlertAction(title: APP_LBL().update_now, style: .default) { (ok) in
                
                alert.dismiss(animated: true, completion: nil)
                
                DispatchQueue.main.async {
                    
                    let APP_ID = "1454057624"
                    
                    if let url = URL(string: "itms-apps://itunes.apple.com/app/id" + APP_ID )
                    {
                        if #available(iOS 10.0, *) {
                            
                            UIApplication.shared.open(url, options: [:]) { (opend) in
                                print("version Popup:- ",opend)
                            }
                            
                        } else {
                            
                            if UIApplication.shared.canOpenURL(url as URL) {
                                UIApplication.shared.openURL(url as URL)
                            } else {
                                print("Can not open")
                            }
                        }
                        
                    } else {
                        
                        //Just check it on phone not simulator!
                        print("Can not open")
                    }
                }
            }
            alert.addAction(ok)
            self.present(alert, animated: true, completion: nil)
        }
        
    }

    func sendRequestForHomepage() {
        self.getRequest(url: "mobiconnect/module/gethomepage/1/",store:true)
        self.send_req_pagination(url: "mobiconnect/home/featured/page/\(currentPage)/store/", store: true)
    }
    
    override func recieveResponse(data: Data?, requestUrl: String?, response: URLResponse?) {
        guard let data = data else {return}
        do {
            var json = try JSON(data: data)
            json = json[0]
            
            
            if requestUrl == "mobiconnect/module/gethomepage/1/"{
                print(json)
                if json["data"]["status"].stringValue == "enabled"{
                    for image in json["data"]["banner"].arrayValue {
                        
                        let sBan = nejreeBannerData(id: image["id"].stringValue, title: image["title"].stringValue, banner_image: image["banner_image"].stringValue, type: image["type"].stringValue, link_to: image["link_to"].stringValue, product_id: image["product_id"].stringValue)
                        
                        self.bannerData.append(sBan)
                        
                        self.banners.append(image["banner_image"].stringValue)
                    }
                    
                }
            }
            else {
                print(json)
                
                if json.stringValue == "NO_PRODUCTS" {
                    self.noProduct = true;return
                }
                
                
                if json["success"].stringValue == "true" {
                    for prod in json["featured_products"].arrayValue {
                        let temp = product(offer: prod["offer"].stringValue,
                                           product_name: prod["product_name"].stringValue,
                                           stock_status: prod["stock_status"].stringValue,
                                           product_id: prod["product_id"].stringValue,
                                           type: prod["type"].stringValue,
                                           special_price: prod["special_price"].stringValue,
                                           product_url: prod["product-url"].stringValue,
                                           product_image: prod["product_image"].stringValue,
                                           wishlist_item_id: prod["wishlist_item_id"].stringValue,
                                           Inwishlist: prod["Inwishlist"].stringValue,
                                           description: prod["description"].stringValue,
                                           review: prod["review"].stringValue,
                                           regular_price: prod["regular_price"].stringValue,
                                           brands_name: prod["brands_name"].stringValue)

                        self.products.append(temp)
                    }
                }
            }

            self.homeCollection.reloadData()

            if isFromWishList != "" {
                
                let vc = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "productViewController") as! productViewController
                vc.product_id = isFromWishList
                APP_DEL.productIDglobal = isFromWishList
                vc.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(vc, animated: true)
                isFromWishList = ""
            }
            
//            if currentPage > 1 {
//                self.homeCollection.reloadSections([1])
//            } else {
//                UIView.transition(with: homeCollection, duration: 0.2, options: .transitionCrossDissolve, animations: {
//                    self.homeCollection.reloadData()
//                }, completion: nil)
//            }
           
        }catch{
            self.view.makeToast(APP_LBL().please_check_your_internet_connection, duration: 1.0, position: .center)
        }
    }

    func sendReqest(){
        currentPage += 1
        self.send_req_pagination(url: "mobiconnect/home/featured/page/\(currentPage)/store/", store: true)
    }
    
}


extension nejreeHomeController: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        default:
            return products.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch indexPath.section {
        case 0:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "nejreeBannerCell", for: indexPath) as! nejreeBannerCell
            cell.banners = self.banners
            cell.bannerData = self.bannerData
            cell.parent = self
            return cell
        default:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "nejreeProductCell", for: indexPath) as! nejreeProductCell
            
            if !noProduct {
                if indexPath.item == products.count - 1 {
                    self.sendReqest()
                }
            }
            
            cell.dataCheck = false
            let pro = products[indexPath.item]
            
            if pro.offer != "" {
                //cell.offerLabel.text = pro.offer! + APP_LBL().percent_off.uppercased()
                let offer =
                """
                \(pro.offer!) %
                \(APP_LBL().off.uppercased())
                """
                
                cell.offerLabel.text = offer
                cell.offerLabel.isHidden = false
            } else {
                cell.offerLabel.isHidden = true
            }
            
            cell.offerLabel.round(redius: 18)
            
//            cell.productImage!.sd_setImage(with: URL(string: pro.product_image!), placeholderImage: UIImage(named: "placeholder"))
            
            
             cell.productImage.image = nil
            
//            if let downloadURL = SDImageCache.shared.imageFromCache(forKey: pro.product_image!){
//                 cell.productImage!.image = downloadURL
//            }
//            else{

            // cell.productImage.sd_setImage(with: URL(string: pro.product_image!), completed: nil)
//                 cell.productImage!.sd_setImage(with: URL(string: pro.product_image!), placeholderImage: UIImage(named: "placeholder"))
                 cell.productImage!.sd_setImage(with: URL(string: pro.product_image!), placeholderImage: nil)
                
            //}

            //cell.productImage!.sd_setImage(with: URL(string: pro.product_image!), placeholderImage: UIImage(named: "placeholder"))
            
            let name = pro.product_name
            let nam = name?.components(separatedBy: "-")
            
            var newName = nam?[0]
            if newName?.first == " " {
                newName?.removeFirst()
            }
            
           // cell.productName.text = newName//nam?.first
            cell.productName.text = pro.product_name
           // cell.productColor.text = nam?.last
            cell.productColor.text = ""
            
            cell.lblBrandName.text = pro.brands_name
             cell.lblBrandName.textColor = UIColor.lightGray
            // cell.lblBrandName.textAlignment = .center

            
            let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: pro.regular_price!)
            attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 1, range: NSMakeRange(0, attributeString.length))
            
            if pro.special_price != "no_special"{
                cell.priceLabel.text = pro.special_price;
                cell.oldPriceLabel.attributedText = attributeString
                cell.oldPriceLabel.isHidden = false
            } else {
                cell.priceLabel.text = pro.regular_price;
                cell.oldPriceLabel.isHidden = true
            }
            
            cell.insideView.cardView()
            
            return cell
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch indexPath.section {
        case 0:
            if self.banners.count > 0
            {
                return CGSize(width: collectionView.frame.width, height: (self.view.frame.size.width/2) + 20 )
                
            }
            else {
                return CGSize()
                
            }
        default:
            return CGSize(width: collectionView.frame.width/2, height: 365)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        switch indexPath.section {
        case 0:
            return
        default:
            let pro = products[indexPath.item]
            let vc = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "productViewController") as! productViewController
            vc.product_id = pro.product_id!
            APP_DEL.productIDglobal = pro.product_id!
             vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
    
    
    
    
}
