//
//  nejreeProductCell.swift
//  MageNative Magento Platinum
//
//  Created by cedcoss on 11/09/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class nejreeProductCell: UICollectionViewCell {
    
    @IBOutlet weak var insideView: UIView!
    @IBOutlet weak var offerLabel: UILabel!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var productColor: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var oldPriceLabel: UILabel!
    
    @IBOutlet weak var lblBrandName: UILabel!
    
    var dataCheck = true
    
    
    override func prepareForReuse() {
        productImage.image = nil;
    }
    
    
    
    
}
