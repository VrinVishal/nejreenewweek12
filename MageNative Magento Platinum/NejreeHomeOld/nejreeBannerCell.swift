//
//  nejreeBannerCell.swift
//  MageNative Magento Platinum
//
//  Created by cedcoss on 11/09/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class nejreeBannerCell: UICollectionViewCell {
    
    @IBOutlet weak var widgetView: FSPagerView! {
        didSet {
            self.widgetView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")
           // self.widgetView.itemSize = CGSize.zero
             widgetView.itemSize = FSPagerView.automaticSize
        }
    }
    
    var banners: [String] = []
    var bannerData = [nejreeBannerData]()
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        widgetView.dataSource = self
        widgetView.delegate = self
        widgetView.automaticSlidingInterval = 3.0
        widgetView.isInfinite = true
       // widgetView.itemSize = CGSize.zero
        widgetView.itemSize = FSPagerView.automaticSize
    }
    
    var parent = UIViewController()
    
}

extension nejreeBannerCell: FSPagerViewDelegate,FSPagerViewDataSource {
    
    func numberOfItems(in pagerView: FSPagerView) -> Int {
        self.banners.count
    }
    
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index)
       // cell.imageView?.clipsToBounds = true
        if let url = URL(string: banners[index]){
            //cell.imageView?.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "placeholder"))
            
            cell.imageView?.sd_setImage(with: url)
            cell.imageView?.contentMode = .scaleAspectFill
            
        }
        
        return cell
    }
    
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {

        if bannerData[index].link_to == "category"{
//            let viewcontoller = UIStoryboard(name: "categorylayouts", bundle: nil).instantiateViewController(withIdentifier: "cedMageDefaultCollection") as? cedMageDefaultCollection
//            viewcontoller?.selectedCategory = bannerData[index].product_id!
//            parent.navigationController?.pushViewController(viewcontoller!, animated: true)
            
            APP_DEL.isFromHomeBannerID = bannerData[index].product_id ?? ""
            self.parent.tabBarController?.selectedIndex = 2
            
            
        }else if (bannerData[index].link_to == "product"){
            let productview = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "productViewController") as! productViewController
//            productview.pageData = [["product_id":bannerData[index].link_id!]]
//            let instance = cedMage.singletonInstance
//            instance.storeParameterInteger(parameter: 0)
            productview.product_id = bannerData[index].product_id!
            APP_DEL.productIDglobal = bannerData[index].product_id!
            productview.hidesBottomBarWhenPushed = true
            parent.navigationController?.pushViewController(productview, animated: true)
        }
        else if (bannerData[index].link_to == "website"){
//            let view = UIStoryboard(name: "cedMageAccounts", bundle: nil)
//            let viewControl = view.instantiateViewController(withIdentifier: "cmsWebView") as! cedMageCmsWebView
//            let url = bannerData[index].product_id ?? ""
//            viewControl.pageUrl = url
//            parent.navigationController?.pushViewController(viewControl, animated: true)
        }

    }
    
    
    
}
