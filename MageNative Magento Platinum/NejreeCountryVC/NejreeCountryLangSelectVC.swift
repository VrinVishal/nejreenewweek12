//
//  NejreeCountryLangSelectVC.swift
//  MageNative Magento Platinum
//
//  Created by Jayesh Dabhi on 10/02/21.
//  Copyright © 2021 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class NejreeCountryLangSelectVC: MagenativeUIViewController, UIGestureRecognizerDelegate {

    @IBOutlet var lblTitle : UILabel!
    @IBOutlet var tblView : UITableView!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var imgViewBackArrow: UIImageView!
    @IBOutlet weak var btnApply: UIButton!

    var selectedGenderIndex = -1
    var selectedCountryIndex = -1
    var selectedLangaugeIndex = -1
    var Stores = [[String:String]]()
    var isFromSplash : Bool = false
    var countryList = [countryData]()
//    var selectedIndex = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.btnApply.isHidden = true
        self.btnApply.titleLabel?.font = UIFont(name: "Cairo-Regular", size: 17)
        self.btnApply.setTitle(APP_LBL().apply.uppercased(), for: .normal)

//        self.tabBarController?.tabBar.isHidden = true

        lblTitle.text = APP_LBL().langauge_and_country.uppercased()

        self.getAllCountryAPI()
        // Do any additional setup after loading the view.
        if (isFromSplash){
            self.btnBack.isHidden = true
        } else {
            self.btnBack.isHidden = false
        }
        
        // get selected gender index
        for i in 0...arrGender.count-1{
            let rec = arrGender[i]
            if let genderId = UserDefaults.standard.value(forKey: KEY_GENDER) as? String  {
                if (rec.id == genderId){
                    self.selectedGenderIndex = i
                }
            }
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override func viewDidDisappear(_ animated: Bool) {
       self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
   }
   
//   override func viewDidAppear(_ animated: Bool) {
//       self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
//   }
   
   override func viewWillAppear(_ animated: Bool) {
      // self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        
        UIApplication.shared.statusBarStyle = .default
      
        self.navigationController?.navigationBar.isHidden = true
        self.navigationController?.isNavigationBarHidden = true
        
    self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    
    if navigationController?.responds(to: #selector(getter: UINavigationController.interactivePopGestureRecognizer)) ?? false {
        
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
    }
        
       
        if APP_DEL.selectedLanguage == Arabic{
            imgViewBackArrow.image = UIImage(named: "icon_back_white_Arabic")
        }
        else{
            imgViewBackArrow.image = UIImage(named: "icon_back_white_round")
        }
    }
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        
        return true
    }
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func setUI() {
        tblView.register(UINib(nibName: "CustomLabelHorizontalCollViewCell", bundle: nil), forCellReuseIdentifier: "CustomLabelHorizontalCollViewCell")
//        lblTitle.text = APP_LBL().select_country.uppercased()
        tblView.register(UINib(nibName: Constants().CELL_NAME_CUSTOMSINGLECOUNTRYCELL, bundle: nil), forCellReuseIdentifier: Constants().CELL_NAME_CUSTOMSINGLECOUNTRYCELL)
        tblView.delegate = self
        tblView.dataSource = self
        tblView.reloadData()
        self.btnApply.isHidden = false

    }
    
    @IBAction func btnCountineClicked(sender : UIButton) {
        print("btnCountineClicked \(selectedGenderIndex)-\(selectedLangaugeIndex)-\(selectedCountryIndex)")
        if (selectedGenderIndex < 0){
            APP_DEL.showAlertWithOkButton(parentVC: self, strTitle: "", strMsg: APP_LBL().please_select_gender)
        } else if (selectedLangaugeIndex < 0) {
            APP_DEL.showAlertWithOkButton(parentVC: self, strTitle: "", strMsg: APP_LBL().please_select_language)
        } else if (selectedCountryIndex < 0){
            APP_DEL.showAlertWithOkButton(parentVC: self, strTitle: "", strMsg: APP_LBL().please_select_country)
        } else {
            self.setValues()
        }
    }
    
    func setValues() {
        
        guard let storeId = UserDefaults.standard.value(forKey: "storeId") as? String  else {return}
        if (((APP_DEL.selectedCountry.country_code ?? "SA") == (countryList[selectedCountryIndex].country_code ?? "SA")) && (self.Stores[self.selectedLangaugeIndex]["store_id"] == storeId)){
            UserDefaults.standard.setValue((arrGender[self.selectedGenderIndex]).id, forKey: KEY_GENDER)

//            APP_DEL.selectedCountry = self.countryList[self.selectedCountryIndex]
//            APP_DEL.selectedCountry.syncronize()
//            self.selectStore(store: self.Stores[self.selectedLangaugeIndex]["store_id"])
            self.navigationController?.popViewController(animated: true)
            return
        } else if ((APP_DEL.selectedCountry.country_code ?? "SA") == (countryList[selectedCountryIndex].country_code ?? "SA")) {
            UserDefaults.standard.setValue((arrGender[self.selectedGenderIndex]).id, forKey: KEY_GENDER)

            APP_DEL.selectedCountry = self.countryList[self.selectedCountryIndex]
            APP_DEL.selectedCountry.syncronize()
            self.selectStore(store: self.Stores[self.selectedLangaugeIndex]["store_id"])
            return
        }
        var countryAlert = APP_LBL().change_country_alert_description
        countryAlert = countryAlert.replacingOccurrences(of: "XYZ", with: countryList[selectedCountryIndex].getCountryName())
        countryAlert = countryAlert.replacingOccurrences(of: "ABC", with: countryList[selectedCountryIndex].getCurrencySymbol())

        let confirmationAlert = UIAlertController(title: APP_LBL().change_country, message: countryAlert, preferredStyle: UIAlertController.Style.alert);
       
        confirmationAlert.addAction(UIAlertAction(title: APP_LBL().ok, style: .default, handler: { (action: UIAlertAction!) in
            UserDefaults.standard.setValue((arrGender[self.selectedGenderIndex]).id, forKey: KEY_GENDER)

            APP_DEL.selectedCountry = self.countryList[self.selectedCountryIndex]
            APP_DEL.selectedCountry.syncronize()
            self.selectStore(store: self.Stores[self.selectedLangaugeIndex]["store_id"])

//            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
//                // your code here
//                APP_DEL.setTabBarAfterAPIsCalling()
//            }
        }));
        
        confirmationAlert.addAction(UIAlertAction(title: APP_LBL().cancel, style: .destructive, handler: { (action: UIAlertAction!) in
            self.navigationController?.popViewController(animated: true)
            
        }));

        self.present(confirmationAlert, animated: true, completion: nil)
       
    }
    
    func selectStore(store:String?){
        //let params = ["store_id":store! as String]
        UserDefaults.standard.setValue(store, forKey: "storeId")
        self.setStore(url: "mobiconnectstore/setstore/"+store!)
    }
    
    func setStore(url: String) {
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);
        
        API().callAPI(endPoint: url, method: .GET, param: [:]) { (json_res, err) in
            
            //    DispatchQueue.main.async {
            
            cedMageLoaders.removeLoadingIndicator(me: self);
            
            if err == nil {
                
                let lang = json_res[0]["locale_code"].stringValue
                
                let language=lang.components(separatedBy: "_")
                if language[0]=="ar"
                {
                    UserDefaults.standard.set(["ar","en","fr"], forKey: "AppleLanguages")
                    UIView.appearance().semanticContentAttribute = .forceRightToLeft
                }
                else if language[0]=="en"
                {
                    UserDefaults.standard.set(["en","ar","fr"], forKey: "AppleLanguages")
                    UIView.appearance().semanticContentAttribute = .forceLeftToRight
                }
                
                APP_DEL.changeLanguage()
            }
            // }
        }
    }
    
    func getStoreList() {
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);
        
        API().callAPI(endPoint: "mobiconnectstore/getlist", method: .GET, param: [:]) { (json_res, err) in
            
            //  DispatchQueue.main.async {
            
            cedMageLoaders.removeLoadingIndicator(me: self);
            
            if err == nil {
                
                let json = json_res[0]
                
                self.Stores.removeAll()
                var index = 0
                for store in json["store_data"].arrayValue{
                    
                    let group_id = store["group_id"].stringValue
                    
                    let code = store["code"].stringValue
                    let store_id = store["store_id"].stringValue
                    let name = store["name"].stringValue
                    let is_active = store["is_active"].stringValue
                    let storeobject = ["group_id":group_id,"code":code,"store_id":store_id,"name":name,"is_active":is_active]
                    self.Stores.append(storeobject)
                    if let storeId = UserDefaults.standard.value(forKey: "storeId") as? String  {
                        if (store["store_id"].stringValue == storeId){
                            self.selectedLangaugeIndex = index
                        }
                    }

                    
                    index = index + 1
                }
                self.setUI()
            }
            //   }
        }
    }
    
    func getAllCountryAPI(){
        APP_DEL.activityIndicator = NVActivityIndicatorView(frame: CGRect(x: (UIScreen.main.bounds.width/2) - 25.0, y: (UIScreen.main.bounds.height/2) - 25.0, width: 50.0, height: 50.0), type: .ballPulse, color: UIColor.init(hexString: "#fcb215"), padding: 0)
        
        APP_DEL.window?.addSubview(APP_DEL.activityIndicator)
        APP_DEL.window?.bringSubviewToFront(APP_DEL.activityIndicator)
        APP_DEL.activityIndicator.isHidden = true
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);
        
        let storeID = UserDefaults.standard.value(forKey: "storeId") as! String?
        var postData = [String:String]()
        postData["store_id"] = "2"
        API().callAPI(endPoint: "mobiconnect/getcountrybystore", method: .POST, param: postData) { (json_res, err) in

            cedMageLoaders.removeLoadingIndicator(me: self);


            let catData = try! json_res[0]["country"].rawData()
            self.countryList = try! JSONDecoder().decode([countryData].self, from: catData)
            
            for i in 0...self.countryList.count-1 {
                if ((self.countryList[i].country_code ?? "") == (APP_DEL.selectedCountry.country_code ?? "")) {
                    self.selectedCountryIndex = i
                }
            }
            self.getStoreList()

        }
//        if let path = Bundle.main.path(forResource: "CountryList", ofType: "json") {
//            do {
//                  let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
//                guard let json_res = try? JSON(data: data) else {
//                    return
//                }
//                  let catData = try! json_res[0]["Country"].rawData()
//                 self.countryList = try! JSONDecoder().decode([countryData].self, from: catData)
//                 self.setUI()
//              } catch {
//                   // handle error
//              }
//        }
    }
    
    
}


extension NejreeCountryLangSelectVC : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (is_country_selection_allow == "1") {
            return 3
        } else {
            return 2
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomLabelHorizontalCollViewCell") as! CustomLabelHorizontalCollViewCell
        cell.selectionStyle = .none
        cell.collType = CustomLabelHorizontalCollType(rawValue: indexPath.row)
        cell.arrData = NSMutableArray(array: arrGender)
        cell.lblTitle.textColor = .black
        cell.theme = 1
        switch cell.collType {
            case .kCountry:
                if (is_country_selection_allow == "1") {
                    cell.arrData = NSMutableArray(array:self.countryList)
                    cell.selectedIndex = selectedCountryIndex
                    cell.lblTitle.text = APP_LBL().choose_country.uppercased()
                }
                break;
            case .kGender:
                cell.arrData = NSMutableArray(array: arrGender)
                cell.selectedIndex = selectedGenderIndex
                cell.store_id = self.Stores[selectedLangaugeIndex]["store_id"] ?? "2"
                cell.lblTitle.text = APP_LBL().choose_shopping_for.uppercased()
                break;

            case .kLangauge:
                cell.arrData = NSMutableArray(array: self.Stores)
                cell.selectedIndex = selectedLangaugeIndex
                cell.lblTitle.text = APP_LBL().language.uppercased()
                
                
                
                    break;
                
            default:
                break;
        }
        cell.delegate = self
        cell.reloadWholeCellData()
        return cell;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

//    func numberOfSections(in tableView: UITableView) -> Int {
//        return 1
//    }
//
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return countryList.count
//    }
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: Constants().CELL_NAME_CUSTOMSINGLECOUNTRYCELL, for: indexPath) as! CustomSingleCountryCell
//        cell.selectionStyle = .none
//
//        let rec = countryList[indexPath.row]
//        cell.lblTitle.text = rec.getCountryName()
//
//        let imgStr = (rec.country_flag_image ?? "")
//        if let imgUrl = URL(string: rec.country_flag_image ?? ""), imgStr != "" {
//            cell.imgView.sd_setImage(with: imgUrl, placeholderImage: nil)
//        } else {
//            cell.imgView.backgroundColor = UIColor(hex: cat_bg_default_color_code)
//        }
//        if (selectedIndex == indexPath.row){
//            cell.imgSelection.image = UIImage(named: "MyAccountTick")
//        } else {
//            cell.imgSelection.image = nil
//        }
//
//        return cell
//    }
//
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return UITableView.automaticDimension
//    }
//
//
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        if (isFromSplash){
//            self.setCountryAndRedirect(indexPath: indexPath)
//            return
//        }
//        var countryAlert = APP_LBL().change_country_alert_description
//        countryAlert = countryAlert.replacingOccurrences(of: "XYZ", with: countryList[indexPath.row].getCountryName())
//        countryAlert = countryAlert.replacingOccurrences(of: "ABC", with: countryList[indexPath.row].getCurrencySymbol())
//
//        let confirmationAlert = UIAlertController(title: APP_LBL().change_country, message: countryAlert, preferredStyle: UIAlertController.Style.alert);
//
//        confirmationAlert.addAction(UIAlertAction(title: APP_LBL().ok, style: .default, handler: { (action: UIAlertAction!) in
//            self.setCountryAndRedirect(indexPath: indexPath)
//
//        }));
//
//        confirmationAlert.addAction(UIAlertAction(title: APP_LBL().cancel, style: .destructive, handler: { (action: UIAlertAction!) in
//
//
//        }));
//
//        self.present(confirmationAlert, animated: true, completion: nil)
//
//    }
    
    func setCountryAndRedirect(indexPath : IndexPath){
        let cell :CustomSingleCountryCell = self.tblView.cellForRow(at: indexPath) as! CustomSingleCountryCell
        self.tblView.reloadData()
        self.selectedCountryIndex = indexPath.row
        let rec = self.countryList[indexPath.row]
       
        APP_DEL.selectedCountry = rec
        APP_DEL.selectedCountry.syncronize()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            // your code here
            APP_DEL.setTabBarAfterAPIsCalling()
        }
    }
}

extension NejreeCountryLangSelectVC : CustomLabelHorizontalCollViewCellDelegate {
    func anyDataTappedCallBack(index: Int, type: CustomLabelHorizontalCollType) {
        switch type {
            case .kCountry:
                selectedCountryIndex = index
                break;
                
            case .kGender:
                selectedGenderIndex = index
                break;

            case .kLangauge:
                    selectedLangaugeIndex = index
                var lang = "ar"
                if (self.Stores[selectedLangaugeIndex]["store_id"] == "2"){
                    lang = "ar"
                } else {
                    lang = "en"
                }
                
                let language=lang.components(separatedBy: "_")
                if language[0] == "ar"
                {
                    APP_DEL.selectedLanguage =  Arabic
                    UserDefaults.standard.set(["ar","en","fr"], forKey: "AppleLanguages")
                    UIView.appearance().semanticContentAttribute = .forceRightToLeft
                    self.view.semanticContentAttribute = .forceRightToLeft
                }
                else if language[0] == "en"
                {
                    APP_DEL.selectedLanguage =  English
                    UserDefaults.standard.set(["en","ar","fr"], forKey: "AppleLanguages")
                    UIView.appearance().semanticContentAttribute = .forceLeftToRight
                    self.view.semanticContentAttribute = .forceLeftToRight
                }
                lblTitle.text = APP_LBL().langauge_and_country.uppercased()

                self.btnApply.setTitle(APP_LBL().apply.uppercased(), for: .normal)

                
            
                break;
            
            default:
                break;
        }
        tblView.reloadData()
    }
}
