//
//  CustomSingleCountryCell.swift
//  MageNative Magento Platinum
//
//  Created by Jayesh Dabhi on 10/02/21.
//  Copyright © 2021 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class CustomSingleCountryCell: UITableViewCell {

    @IBOutlet var lblTitle : UILabel!
    @IBOutlet var imgView : UIImageView!
    @IBOutlet var imgSelection : UIImageView!
    @IBOutlet var lblBottomLine : UILabel!
    @IBOutlet var viewBg : UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        lblTitle.font = UIFont(name: "Cairo-Regular", size: 16)!
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
