//
//  NotificationVC.swift
//  MageNative Magento Platinum
//
//  Created by Vrinsoft on 31/07/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit
import SKPhotoBrowser

struct AppNotif : Codable {
    
    let title: String?
    let body: String?
    let image: String?
    
    enum CodingKeys: String, CodingKey {
        
        case title = "title"
        case body = "body"
        case image = "image"
    }
}

protocol MenuDidSelectCall {
    func NavigationFromMenu(index:Int)
}

class NotificationVC: UIViewController {
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var btnSetting: UIButton!
    @IBOutlet weak var vwMenu: UIView!
    
    @IBOutlet weak var imgNotification: UIImageView!
    @IBOutlet weak var lblCount: UILabel!
    
    @IBOutlet weak var viewNotification: UIView!
    @IBOutlet weak var lblNoData: UILabel!

    
    var NavDelegate : MenuDidSelectCall?
    //var arrNotifica: [Notific] = []

    
    override func viewDidLoad(){
        super.viewDidLoad()

        setData()
         self.vwMenu.isHidden = true
        
        UserDefaults.standard.set(0, forKey: "NejreeUnreadCount")
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
            
        self.updateNotificationCount()
        
        self.getNotification()
    }
    
    func updateNotificationCount() {
        
        if let count = UserDefaults.standard.value(forKey: "NejreeUnreadCount") as? Int, (count > 0) {
            
            self.lblCount.text = "\(count)"
            self.lblCount.isHidden = false
        } else {
            
            self.lblCount.text = ""
            self.lblCount.isHidden = true
        }
    }
    
    @IBAction func actionBtnDismiss(_ sender: UIButton) {
        
        let transition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: .easeInEaseOut)
     
        if APP_DEL.selectedLanguage == Arabic{
            transition.subtype = .fromLeft
        }else{
            transition.subtype = .fromRight
        }
        
        transition.type = .push
        self.vwMenu.layer.add(transition, forKey: nil)
        self.vwMenu.isHidden = true
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.dismiss(animated: false, completion: nil)
        }

    }
    
    @IBAction func actionBtnSetting(_ sender: UIButton) {
        
    }
    
    func checkNoData() {
        
        if APP_DEL.arrNotification.count == 0 {
            self.tblView.isHidden = true
            self.lblNoData.isHidden = false
        } else {
            self.tblView.isHidden = false
            self.lblNoData.isHidden = true
        }
    }

    
    func getNotification() {
        
//        var postData = [String:String]()
//
//        if let userInfoDict = UserDefaults.standard.object(forKey: "userInfoDict") as? [String: String] {
//
//            if let customId = userInfoDict["customerId"] {
//                postData["customer_id"] = customId
//            }
//        }
//
//        cedMageLoaders.removeLoadingIndicator(me: self);
//        cedMageLoaders.addDefaultLoader(me: self);
//
//        API().callAPI(endPoint: "mobiconnect/getnotification", method: .POST, param: postData) { (json, err) in
//
//          //  DispatchQueue.main.async {
//
//                cedMageLoaders.removeLoadingIndicator(me: self);
//
//                if err == nil {
//
//                    self.arrNotifica = try! JSONDecoder().decode([Notific].self, from: json[0]["result"].rawData())
//                }
                
                self.tblView.reloadData()
                
                self.checkNoData()
          //  }
//        }
    }
    
    func setData(){
        
        
        lblCount.layer.cornerRadius = lblCount.frame.size.height/2
        lblCount.clipsToBounds = true
        
        
        imgNotification.layer.cornerRadius = imgNotification.frame.size.height/2
        
        imgNotification.clipsToBounds = true

        viewNotification.layer.cornerRadius = viewNotification.frame.size.height/2
        
        viewNotification.clipsToBounds = true
        
            
             
             
             tblView.register(UINib(nibName:"NotificationCell" , bundle: nil), forCellReuseIdentifier: "NotificationCell")
             tblView.dataSource = self
             tblView.delegate = self
             tblView.tableFooterView = UIView()
             tblView.reloadData()
             
             lblNoData.text = APP_LBL().no_data_found.uppercased();
             
     
        let transition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: .easeInEaseOut)
        transition.type = .push
        if APP_DEL.selectedLanguage == Arabic{
                transition.subtype = .fromRight
              }else{
                  transition.subtype = .fromLeft
              }
              
        
        
        UIView.animate(withDuration: 0.1,
                       delay: 0.5,
                       options: UIView.AnimationOptions.curveEaseIn,
                       animations: { () -> Void in
                        
                        self.view?.layoutIfNeeded()
        }, completion: { (finished) -> Void in
            self.vwMenu.isHidden = false
            self.vwMenu.layer.add(transition, forKey: nil)
        })


        
    }
    
    @objc func btnImageAction(_ sender: UIButton) {
        
        let not = APP_DEL.arrNotification[sender.tag]
        
        SKPhotoBrowserOptions.displayAction = false
        var arrImages = [SKPhoto]()
        
        let photo = SKPhoto.photoWithImageURL((not.image ?? ""))
        photo.shouldCachePhotoURLImage = true
        arrImages.append(photo)
        
        let browser = SKPhotoBrowser(photos: arrImages)
        browser.modalPresentationStyle = .fullScreen
        browser.modalTransitionStyle = .crossDissolve
        self.present(browser, animated: true, completion: {})
        //APP_DEL.window?.rootViewController?.present(browser, animated: true, completion: {})
    }
    
    
}

extension NotificationVC : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return APP_DEL.arrNotification.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationCell", for: indexPath) as! NotificationCell
        cell.selectionStyle = .none
        
        let not = APP_DEL.arrNotification[indexPath.row]
        
        cell.lblTitle.text = (not.title ?? "").uppercased()
        cell.lblDescription.text = not.body ?? ""
        
        cell.btnImage.tag = indexPath.row
        cell.btnImage.addTarget(self, action: #selector(self.btnImageAction(_:)), for: .touchUpInside)
        
//        let tempTime = not.created_date ?? ""
//        if tempTime != "" {
//            let time = CHAT().convertServerDateTime(toLocalDeviceDateTime: tempTime, isFromChatDetails: false)
//            cell.lblDateTime.text = time
//        } else {
//            cell.lblDateTime.text = "-:-"
//        }
        
        if (not.image ?? "") == "" {
            cell.imgView.superview?.isHidden = true
        } else {
            cell.imgView.superview?.isHidden = false
            cell.imgView.sd_setImage(with: URL(string: (not.image ?? "")), placeholderImage: nil)
        }
    
        if APP_DEL.selectedLanguage == Arabic{
            
            cell.lblTitle.textAlignment = .right
            cell.lblDescription.textAlignment = .right
        }else{
            cell.lblTitle.textAlignment = .left
            cell.lblDescription.textAlignment = .left
        }

        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    
}
