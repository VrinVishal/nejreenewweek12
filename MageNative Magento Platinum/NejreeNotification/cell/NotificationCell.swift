//
//  NotificationCell.swift
//  MageNative Magento Platinum
//
//  Created by Vrinsoft on 31/07/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class NotificationCell: UITableViewCell {
    
    @IBOutlet weak var viewBG: UIView!
    
    @IBOutlet weak var imgView: UIImageView!
    
    @IBOutlet weak var btnImage: UIButton!
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblDateTime: UILabel!
    
    

    override func awakeFromNib() {
        super.awakeFromNib()

        imgView.round()
        
//        viewBG.layer.cornerRadius = 10
//        viewBG.clipsToBounds = true
        
        lblTitle.setFont(fontFamily: "Cairo-Bold", fontSize: 15)
        lblDescription.setFont(fontFamily: "Cairo-Regular", fontSize: 15)
        lblDateTime.setFont(fontFamily: "Cairo-Regular", fontSize: 15)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
