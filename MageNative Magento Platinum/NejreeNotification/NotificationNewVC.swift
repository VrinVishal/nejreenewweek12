//
//  NotificationNewVC.swift
//  MageNative Magento Platinum
//
//  Created by vishal.n on 16/08/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

struct Notific: Codable {
    
    var list_id: String?
    var message_title: String?
    var message_description: String?
    var topic_id: String?
    var user_id: String?
    var created_date: String?
}


class NotificationNewVC: UIViewController {

    
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var imgBackBG: UIImageView!

    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var lblNoData: UILabel!
    
    var arrNotifica: [Notific] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tblView.isHidden = true
        self.lblNoData.isHidden = true
        
        setUI()
        
        getNotification()
    }
    

    func setUI() {
        tblView.register(UINib(nibName:"NotificationCell" , bundle: nil), forCellReuseIdentifier: "NotificationCell")
        tblView.dataSource = self
        tblView.delegate = self
        tblView.tableFooterView = UIView()
        tblView.reloadData()
        
        lblNoData.text = APP_LBL().no_data_found.uppercased();
        lblHeader.text = APP_LBL().notification.uppercased();
        
        if APP_DEL.selectedLanguage == Arabic{
            imgBackBG.image = UIImage(named: "icon_back_white_Arabic")
        } else {
            imgBackBG.image = UIImage(named: "icon_back_white_round")
        }
        
    }

    func checkNoData() {
        
        if self.arrNotifica.count == 0 {
            self.tblView.isHidden = true
            self.lblNoData.isHidden = false
        } else {
            self.tblView.isHidden = false
            self.lblNoData.isHidden = true
        }
    }
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSettingAction(_ sender: UIButton) {
        
    }
    
    func getNotification() {
        
        var postData = [String:String]()

        if let userInfoDict = UserDefaults.standard.object(forKey: "userInfoDict") as? [String: String] {
            
            if let customId = userInfoDict["customerId"] {
                postData["customer_id"] = customId
            }
        }
        
        cedMageLoaders.removeLoadingIndicator(me: self);
        cedMageLoaders.addDefaultLoader(me: self);
        
        API().callAPI(endPoint: "mobiconnect/getnotification", method: .POST, param: postData) { (json, err) in
            
         //   DispatchQueue.main.async {
                
                cedMageLoaders.removeLoadingIndicator(me: self);
                
                if err == nil {
                    
                    self.arrNotifica = try! JSONDecoder().decode([Notific].self, from: json[0]["result"].rawData())
                }
                
                self.tblView.reloadData()
                
                self.checkNoData()
          //  }
        }
    }
}


extension NotificationNewVC : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.arrNotifica.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationCell", for: indexPath) as! NotificationCell
        cell.selectionStyle = .none
        
        let not = self.arrNotifica[indexPath.row]
        
        cell.lblTitle.text = (not.message_title ?? "").uppercased()
        cell.lblDescription.text = not.message_description ?? ""
        
        let tempTime = not.created_date ?? ""
        if tempTime != "" {
            let time = CHAT().convertServerDateTime(toLocalDeviceDateTime: tempTime, isFromChatDetails: false)
            cell.lblDateTime.text = time
        } else {
            cell.lblDateTime.text = "-:-"
        }

        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    
}
